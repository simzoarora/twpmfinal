$(function () {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }

    $('#client-name').on('keyup', function () {
        var $this = $(this),
                clientVal = $this.val();
        if (clientVal.length > 0) {
            $.ajax({
                url: searchClientUrl,
                type: 'POST',
                data: {
                    queryString: clientVal
                },
                success: function (response) {
                    $('#employee-ul').empty();
                    $this.next().empty();
                    if (response.length > 0) {
                        $.each(response, function (i, v) {
                            $this.next().append('<li data-id="' + v.id + '">' + (v.profile != null ? v.profile.first_name + ' ' + v.profile.last_name : '') + ' (' + v.email + ')' + '</li>');
                        });
                    }
                },
                error: function (err) {
                    $('#client-name').next().append('<li>There has been error. Please try again.</li>');
                }
            });
        } else {
            $('#employee-ul').empty();
            $('#client-ul').empty();
        }
    });
    $(document).on('click', '#client-ul li', function () {
        var $this = $(this);
        $('#client-id').val($this.attr('data-id'));
        $('#client-name').val($this.text());
        $('#client-ul').empty();
    });

    $('#employee-name').on('keyup', function () {
        var $this = $(this),
                empVal = $this.val();
        if (empVal.length > 0) {
            $.ajax({
                url: searchEmployeeUrl,
                type: 'POST',
                data: {
                    queryString: empVal
                },
                success: function (response) {
                    $('#client-ul').empty();
                    $this.next().empty();
                    if (response.length > 0) {
                        $.each(response, function (i, v) {
                            $this.next().append('<li data-id="' + v.id + '">' + v.name + ' (' + v.email + ')' + '</li>');
                        });
                    }
                },
                error: function (err) {
                    $('#employee-name').next().append('<li>There has been error. Please try again.</li>');
                }
            });
        } else {
            $('#client-ul').empty();
            $('#employee-ul').empty();
        }
    });
    $(document).on('click', '#employee-ul li', function () {
        var $this = $(this);
        $('#employee-id').val($this.attr('data-id'));
        $('#employee-name').val($this.text());
        $('#employee-ul').empty();
    });

});
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});  