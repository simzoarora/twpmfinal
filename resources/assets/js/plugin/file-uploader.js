! function(e) {
"use strict";
        e.FileDialog = function(a) {
        var o = e.extend(e.FileDialog.defaults, a),
                t = e(["<div class='modal fade file-uploader'>", "<div class='modal-dialog'>", "<div class='modal-content'>", "<div class='modal-header' style='background-color: #0972bd; color:#fff; border: none;'>", "<button type='button' class='close' data-dismiss='modal'>", "<span aria-hidden='true'>&times;</span>", "<span class='sr-only'>", o.cancelButton, "</span>", "</button>", "<h4 class='modal-title'>", o.title, "</h4>", "</div>", "<div class='modal-body col-sm-12'>", "<input type='file' style='display:none; border:0'  />", "<div class='container-fluid bfd-files'>", " </div>", "<div class='bfd-dropfield'>", "<div class='bfd-dropfield-inner'>", o.dragMessage, "</div>", "</div>", "</div>", "<div class='modal-footer'>", "<button type='button' class='btn btn-primary bfd-ok'>", o.okButton, "</button>", "<!--<button type='button' class='btn btn-default bfd-cancel'", " data-dismiss='modal'>", o.cancelButton, "</button> -->", "</div>", "</div>", " </div>", "</div>"].join("")),
                n = !1,
                i = e("input:file", t),
                r = e(".bfd-dropfield", t),
                s = e(".bfd-dropfield-inner", r);
                s.css({
                height: o.dropheight,
                        "padding-top": o.dropheight / 2 - 32
                }), i.attr({
        accept: o.accept,
                multiple: o.multiple
        }), r.on("click.bfd", function() {
        i.trigger("click")
        });
                var d = [],
                l = [],
                c = function(a) {
                var n, i, r = new FileReader;
                        l.push(r), r.onloadstart = function() {}, r.onerror = function(e) {
                e.target.error.code !== e.target.error.ABORT_ERR && n.parent().html(["<div class='bg-danger bfd-error-message'>", o.errorMessage, "</div>"].join("\n"))
                }, r.onprogress = function(a) {
                var o = Math.round(100 * a.loaded / a.total) + "%";
                        n.attr("aria-valuenow", a.loaded), n.css("width", o), e(".sr-only", n).text(o)
                }, r.onload = function(e) {
                a.content = e.target.result, d.push(a), n.removeClass("active")
                };
                        var s = e(["<div class='bfd-info'>", "    <span class='glyphicon glyphicon-remove bfd-remove hide'></span>", "    <span class='glyphicon glyphicon-file hide'></span>&nbsp; <span class='uploadedfile'>" + a.name, "</em></div>", "<div class='bfd-progress'>", "    <div class='progress'>", "        <div class='progress-bar  active' role='progressbar'", "            aria-valuenow='0' aria-valuemin='0' aria-valuemax='" + a.size + "'>", "            <span class='sr-only'>0%</span>", "        </div>", "    </div>", "</div>"].join(""));
                        n = e(".progress-bar", s), e(".bfd-remove", s).tooltip({
                container: "body",
                        html: !0,
                        placement: "top",
                        title: o.removeMessage
                }).on("click.bfd", function() {
                r.abort();
                        var e = d.indexOf(a);
                        e >= 0 && d.pop(e), i.fadeOut()
                }), i = e("<div class='row'></div>"), i.append(s), e(".bfd-files", t).append(i), r["readAs" + o.readAs](a)
                },
                f = function(e) {
                Array.prototype.forEach.apply(e, [c])
                };
                return i.change(function(e) {

                if ($('.progress-bar').length >= 1){
                $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
                        $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
                        return false;
                }
                e = e.originalEvent;
                        var a = e.target.files;
                        f(a);
                        var o = i.clone(!0);
                        i.replaceWith(o), i = o
                }), r.on("dragenter.bfd", function() {
        s.addClass("bfd-dragover")
        }).on("dragover.bfd", function(e) {
        e = e.originalEvent, e.stopPropagation(), e.preventDefault(), e.dataTransfer.dropEffect = "copy"
        }).on("dragleave.bfd drop.bfd", function() {
        s.removeClass("bfd-dragover")
        }).on("drop.bfd", function(e) {
            $('.fileNameSaver').removeClass('hide');
                if ($('.progress-bar').length >= 1){
        $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
                $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
                return false;
        }

        e = e.originalEvent, e.stopPropagation(), e.preventDefault();
                var a = e.dataTransfer.files;
                if ((a.length) > 1){
        return false;
        }
        0 === a.length, f(a)
        }), e(".bfd-ok", t).on("click.bfd", function() {
        var a = e.Event("files.bs.filedialog");
                // alert('hide');
                $('#myModal').modal({
        backdrop: 'static',
                keyboard: false
        })
                a.files = d, t.trigger(a), n = !0, t.modal("hide")
        }), t.on("hidden.bs.modal", function() {
        if (l.forEach(function(e) {
        e.abort()
        }), !n) {
        var a = e.Event("cancel.bs.filedialog");
                t.trigger(a)
        }
        t.remove()
        }), e(document.body).append(t), t.modal(), t
        }, e.FileDialog.defaults = {
accept: "jpg",
        cancelButton: "Close",
        dragMessage: "Drag files here or <em>browse<em>",
        dropheight: 400,
        errorMessage: "An error occured while loading file",
        multiple: !0,
        okButton: "OK",
        readAs: "DataURL",
        removeMessage: "Remove&nbsp;file",
        title: "UPLOAD FILE"
}
}(jQuery);