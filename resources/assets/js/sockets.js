import * as moment from 'moment';
window.Echo.private('private-event-' + document.getElementsByName("_user_id")[0].content)
        .listen('PrivateEvent', (e) => {
            toastr.success(e.notification.notification);
//            alert(e.notification.notification);
            $('#unreadNotifications, #allNotifications').prepend('<li  class="active"><p>' + e.notification.notification + '</p><span>Now</span></li>');
            $('.noti-red').removeClass('hide');

        });

$(document).ready(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    var userIdrCur = $("#auth_user_id").val();
    var connectionId = $("#notification_id").val();
    getNotifications();

    // Show/Hide Notification pane
    $("#noti-open , .out").click(function ()
    {
        $("#notificationContainer").fadeToggle(300);
        var formData = new FormData();
        var userId = $("#auth_user_id").val();
        formData.append('userId', userId);
        $.ajax({
            type: 'post',
            url: '/isReadTrue',
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false,
            error: function () {
                console.log("Request Failed");
            },
            success: function (response) {
                $('.noti-red').addClass('hide');
            }
        });
        return false;
    });
    //Document Click
    $(document).click(function ()
    {
//        $("#notificationContainer").hide();
//        getNotifications();
    });
    $(".out").click(function ()
    {
        $("#notificationContainer").hide();
        getNotifications();
    });
    //Popup Click
    $("#notificationContainer").click(function ()
    {
        return false;
    })
    $('#notificationsBody a.notification-type').on('click', function () {
        var $this = $(this);
        $this.addClass('active');
        $this.siblings().removeClass('active');
        var href = $this.attr('href');
        $('.notifications-list').hide(500);
        $(href).show(500);
    });
    function getNotifications() {
        var formData = new FormData();
        var userId = $("#auth_user_id").val();
        formData.append('id', userId);
        $.ajax({
            type: 'get',
            url: '/notification',
            data: {'userId': userId},
            error: function () {
                console.log("Request Failed");
            },
            success: function (response) {

                $('#allNotifications').html('');
                $('#unreadNotifications').html('');
                $.each(response.data, function (key, val) {
                    var activeClass = '';

                    if (val.is_read == 0) {
                        $('.noti-red').removeClass('hide');
                        activeClass = 'active'
                    }
                    $('#allNotifications').append('<li  class="' + activeClass + '" data-id="' + val.id + '" ><p>' + val.notification + '</p><span>' + moment.utc(val.created_at).local().fromNow() + '</span></li>');
                });
                $.each(response.unReadData, function (key, val) {
                    var activeClass = '';

                    if (val.is_read == 0) {
                        $('.noti-red').removeClass('hide');
                        activeClass = 'active'
                    }
                    $('#unreadNotifications').append('<li  class="' + activeClass + '" data-id="' + val.id + '" ><p>' + val.notification + '</p><span>' +moment.utc(val.created_at).local().fromNow() + '</span></li>');
                });
//               
            }
        });
    }

});