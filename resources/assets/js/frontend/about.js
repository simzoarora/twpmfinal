var navHeight = $('.home-navbar').height();
if (window.location.hash == '#ourTeam') {
    $("html, body").animate({scrollTop: $('#team-member-outer').offset().top - navHeight}, 500);
}
var animationInProgress = false;
$(document).ready(function () {
    $('.show-employee-details').on('mouseenter', function () {
        if (animationInProgress == false) {
            animationInProgress = true;
            var sel = $(this).closest('.single-employee').find('.employee-details');
            sel.addClass('show animated slideInUp');
            setTimeout(function () {
                sel.removeClass('animated slideInUp');
                animationInProgress = false;
            }, 1000);
        }
    });
    $('.employee-details').on('mouseleave', function () {
        if (animationInProgress == false) {
            animationInProgress = true;
            var $this = $(this);
            $this.addClass('show animated slideOutDown');
            setTimeout(function () {
                animationInProgress = false;
                $this.removeClass('show animated slideOutDown');
            }, 1000);
        }
    });
    $('.hide-employee-details').on('click', function () {
        if (animationInProgress == false) {
            animationInProgress = true;
            var sel = $(this).closest('.employee-details');
            sel.addClass('show animated slideOutDown');
            setTimeout(function () {
                animationInProgress = false;
                sel.removeClass('show animated slideOutDown');
            }, 1000);
        }
    });
    $('.full-bio-check').on('click', function () {
        var $this = $(this);
        $('#single-team-member').html($this.siblings('.full-employee-details').html()).animate({'left': '0%', opacity: 1}, 700, 'easeInOutCubic');
        $('#all-team-members').animate({'marginLeft': '-15%', opacity: 0.3}, {
            duration: 700,
            complete: function () {
                $('#all-team-members').css('height', '1px');
//                $('body').css('overflow-y', 'hidden');          
                $('.hide-content,#footer').hide();
                $("html, body").animate({scrollTop: 0}, 500, function () {
                    $('.home-navbar').addClass('blue-nav');
                    setTimeout(function(){
                        $('.home-navbar').addClass('blue-nav');
                    },100)
                });

            },
            easing: 'easeInOutCubic'
        });
    });
    $('#single-team-member').on('click', '.back-arrow', function () {
        $('#single-team-member').animate({'left': '100%', opacity: 0.3}, 700, 'easeInOutCubic');
//        $('body').css('overflow-y', 'auto');
        $('#all-team-members').css('height', 'auto').animate({'marginLeft': '0%', opacity: 1}, {
            duration: 700,
            complete: function () {
                $('#single-team-member').html('');
                $('.hide-content,#footer').show();
            },
            easing: 'easeInOutCubic'
        });
    });
}); 