$(document).ready(function () {
    //navbar color change on scroll  
    $(window).on('scroll', function () {
        if ($('.home-navbar').offset().top >= 30) {
            $('.home-navbar').addClass('blue-nav');
        } else {
            $('.home-navbar').removeClass('blue-nav');
        }
    });
    //navbar color change on page load  
    if ($('.home-navbar').offset().top >= 30) {
        $('.home-navbar').addClass('blue-nav');
    }
});