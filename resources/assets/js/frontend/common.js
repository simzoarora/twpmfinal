/**
 * Place the CSRF token as a header on all pages for access in AJAX requests
 */
$.ajaxSetup({ 
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//END
//Custom function variable defined
var twpmApp = {};
//END
$(document).ready(function () {
    setTimeout(function () {
        $('.alert-danger, .alert-success').fadeOut('fast');
    }, 5000);   
    
//Show contact modal
    $(document).on('click', '.contact-modal-show', function (e) {
        $('#contact-us').show();
        $('.contact-response').html('');
        $('#get-started-modal').modal();
        e.preventDefault();
    });
//END 
//Contact form submit
    $('#contact-us').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $this.serialize(),
            success: function (resp) {
                $this.parent().find('.contact-response').html("<p>Thank you for contacting us. We'll respond to you shortly.</p>");
                $this.hide();
            },
            error: function (err) {
                swal({
                    title: 'Oops!!',
                    text: 'Something went wrong. Please try again.',
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });
//END
//Contact form submit
    $('#investment-intake-form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $this.serialize(),
            success: function (resp) {
                location.href = resp.href;
            },
            error: function (err) {
                swal({
                    title: 'Oops!!',
                    text: 'Something went wrong. Please try again.',
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });
//END

    $('.select-input').each(function () {
        $(this).text($(this).next().children().first().text());
    });
    $('.select-input').on('click', function () {
        $(this).next().addClass('ul-border').show();
        $(this).next().children().show();
    });

    $(document).on('click', function (e) {
        var lists = $('.select-list');
        if (!lists.is(e.target) && !$('.select-input').is(e.target))
        {
            lists.hide();
        }
    });

    $('.select-list').each(function () {
        var li_lengths = [],
                list = $(this);   
        list.children('li').each(function () {
            li_lengths.push($(this).outerWidth());
        });
        var max_width = Math.max.apply(Math, li_lengths);
        list.parent('.investment_wrapper').width(max_width);
        list.parent('.investment_wrapper').find('.select-input').width(max_width);
    });

    $('#investment-intake-form').on('click', 'li', function () {
        $(this).parent().removeClass('ul-border');
        var selected_val = $(this).data('val'),
                input_class = $(this).parent().data('input');
        $('.' + input_class).text(selected_val);
        $(this).parent().children().hide();
    });

    twpmApp.ajaxInputError = function (error, formSel) {
        //removing all error classes
        formSel.find('.response-msg').remove();
        if ($.isEmptyObject(error)) {
            swal({
                title: 'Oops!!',
                text: 'Something went wrong. Please try again.',
                type: 'error',
                customClass: 'sweat-alert-confirm'
            });
        } else {
            if (!$.isEmptyObject(error.responseText)) {
                var responseText = JSON.parse(error.responseText);
                $.each(responseText, function (i, v) {
                    if ($.isArray(v)) {
                        var textValue = '';
                        $.each(v, function (i, value) {
                            textValue = textValue + ' ' + value;
                        });
                    } else {
                        var textValue = v;
                    }
                    formSel.find('[name="' + i + '"]').after('<div class="response-msg alert alert-danger">' + textValue + '</div>');
                });
            }
        }
    };

    //Make selects css better
    $(".selectboxit").selectBoxIt();
    
    $('.status a').each(function () {
        var $this = $(this);
        $this.css('text-decoration', 'none');
        var link = $.trim($this.text());
        if (link === 'Begin') {
            $this.css({'background-color': '#ffffff',
                'border': '1px solid #2279ee',
                'color': '#2279ee',
                'text-decoration': 'none'
            });
        }
        if (link === 'In progress') {
            $this.css({'background-color': '#2279ee',
                'border-radius': '4px',
                'color': '#ffffff',
                'text-decoration': 'none'
            });
        }


    });

    $(document).on('click', '.toastrForService', function () {
        toastr.warning('Please Complete the Service');
    });
    $(document).on('keypress', 'input[type=text]:not(.add-comma), textarea', function (e) {
        if (e.which === 32 && !this.value.trim().length) {
            e.preventDefault();
        }

        var inputValue = event.charCode;
        if (!(inputValue >= 48 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 && inputValue != 44 && inputValue != 46)) {
            event.preventDefault();
        }

    });
//   for decimal input 
    $(document).on('keydown', 'input[type=number]', function (event) {
        if (event.shiftKey == true) {
            event.preventDefault();
        }
        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });

});  