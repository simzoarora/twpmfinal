$(document).ready(function () {
//    }
    $('.continue-btn').hide();

    $(document).on('click', '.cross', function () {

        $('.modal-backdrop').remove();
    });

    $('.client-item:first-child').addClass('active');

    var EMPLOYEE_FUNCTIONS = {
        searchClients: function (searchText) {
            var route = getAllClientsRoute.replace('text', searchText);
            $.ajax({
                url: route,
                type: 'GET',
                success: function (response) {
                    $('#clients-list .list-unstyled').html(response).animate({scrollTop: 0}, 500);
                }
            });
        },
        openClientDetailsTab: function (clientId) {
            var route = getClientDetailsRoute.replace('text', clientId);
            $.ajax({
                url: route,
                type: 'GET',
                success: function (response) {
                    EMPLOYEE_FUNCTIONS.appendClientDetails(response);
                }
            });
        },
        appendClientDetails: function (clientDetails) {
            $('.profile_inner').html(clientDetails);
        }
    };
    //Search clients
    $("#clients-list input").keyup(function () {
        var searchText = $(this).val();
        if (searchText.length > 0) {
            EMPLOYEE_FUNCTIONS.searchClients(searchText);
        }
    });
    //Get clients data on right
    $(document).on('click', '#clients-list li.client-item', function (e) {
        var clientId = $(e.target).data('id');
        $(e.target).siblings().removeClass('active');
        $(e.target).addClass('active');
        EMPLOYEE_FUNCTIONS.openClientDetailsTab(clientId);
    });
    //Getting first client data to right
    if ($('#clients-list .client-item').first().length > 0) {
        var clientId = $('#clients-list .client-item').first().attr('data-id');
        EMPLOYEE_FUNCTIONS.openClientDetailsTab(clientId);
    }
    //Show/hide clients and financial profile
    $(document).on('click', '#all-profile li a', function (e) {
        var $this = $(this),
                href = $this.attr('href');
        $('.personal-profile').hide();
        $(href).show();
        $this.parent().siblings().removeClass('active');
        $this.parent().addClass('active');
        e.preventDefault();
    });
    //Show/hide Profile and documents
    $(document).on('click', '.profile-nav li a', function (e) {
        var $this = $(this),
                href = $this.attr('href');
        $('.pro-doc-full').hide();
        $(href).show();
        $this.parent().siblings().removeClass('active');
        $this.parent().addClass('active');
        e.preventDefault();
    });

    //Show previous notes by scrolling down
    $(document).on('click', '#notesModal', function (e) {
        $('#addNotesModal').scroll(function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

                if ($('#stopFunction').val() == '0') {
                    $('.loader').show();
                } else {
                }
                getPreviousNotes();
            }
        });
    });

    //Add a new note when you click on return/enter key
    $(document).on('keypress', '.add-note-input', function (e) {
        if (e.which == 13) {
            $(".employee-note-button").click();
            return e.which !== 13;
        }
    });


//    $(document).on('click', '.employee-note-button', function (e) {
//        e.preventDefault();
//        var $this = $(this);
//        //Remove error messages
//        console.log('keycode', e.keyCode);
//        console.log('shift', e.shiftKey);
//        console.log('ctrlKey', e.ctrlKey);
//        if ($('.add-note-input textarea').val()) {
//
//            $.ajax({
//                type: "POST",
//                url: $('#notesForm').attr('action'),
//                data: $('#notesForm').serialize(),
//                dataType: 'json',
//                success: function (resp) {
//                    var html = '';
//                    var html1 = '';
//                    $.each(resp.notes, function (key, value) {
//                        html += '<div class="notes-details"> <p class="date">' + value.date + '</p><p class="note-info">' + value.note + '</p><hr></div>';
//                        html1 += '<div class="modal-notes-details"> <p class="date">' + value.date + '</p><p class="employee-name">' + resp.employeeName + '</p><p class="note-info">' + value.note + '</p><hr></div>';
//
//                    });
//
//                    $('#addNotesModal form')[0].reset();
//                    $('.notes-data').html(html)
//                    setTimeout(function () {
//                    }, 1500);
//                    $('.previous-notes').html(html1);
//                    setTimeout(function () {
//                    }, 1500);
//                    $('.no-notes').hide();
//
//                },
//                error: function (err) {
//                    var error = JSON.parse(err.responseText);
//                    $('#addNotesModal form').find("#form-response").addClass('error').text(error.message);
//                    setTimeout(function () {
//                    }, 1500);
//                }
//            });
//        } else {
//            $('#addNotesModal').hide();
//            $('.modal-backdrop').remove();
//        }
//    });


    function getCaret(el) {
        if (el.selectionStart) {
            return el.selectionStart;
        } else if (document.selection) {
            el.focus();
            var r = document.selection.createRange();
            if (r == null) {
                return 0;
            }
            var re = el.createTextRange(), rc = re.duplicate();
            re.moveToBookmark(r.getBookmark());
            rc.setEndPoint('EndToStart', re);
            return rc.text.length;
        }
        return 0;
    }
    $(document).on('keyup', '.add-note-input textarea', function (event) {
//        event.preventDefault();
        var $this = $(this);
        //Remove error messages
        if (event.keyCode == 13) {
            this.after('<div class="lds-dual-ring"></div>');
            $this.hide();
            var content = this.value;
            var caret = getCaret(this);
            if (event.shiftKey) {
                this.value = content.substring(0, caret) + "\n" + content.substring(caret, content.length);
                event.stopPropagation();
            } else {
                this.value = content.substring(0, caret) + content.substring(caret, content.length);
                if ($('.add-note-input textarea').val()) {

                    $.ajax({
                        type: "POST",
                        url: $('#notesForm').attr('action'),
                        data: $('#notesForm').serialize(),
                        dataType: 'json',
                        success: function (resp) {
                            $('.lds-dual-ring').remove();       
                            $('.add-note-input textarea').show();
                            var html = '';
                            var html1 = '';
                            $.each(resp.notes, function (key, value) {

//                                var match = /\r|\n/.exec(value.note);
//                                if (match) {
//                                    value.note = value.note.substring(0, match.index) + "\n" + value.note.substring(match.index + 1, value.note.length);
//                                    console.log('value.note',value.note);
//                                }
//                                console.log('match', match);

                                html += '<div class="notes-details"> <p class="date">' + value.date + '</p><p class="note-info">' + value.note + '</p><hr></div>';
                                html1 += '<div class="modal-notes-details"> <p class="date">' + value.date + '</p><p class="employee-name">' + resp.employeeName + '</p><p class="note-info">' + value.note + '</p><hr></div>';

                            });

                            $('#addNotesModal form')[0].reset();
                            $('.notes-data').html(html)
                            setTimeout(function () {
                            }, 1500);
                            $('.previous-notes').html(html1);
                            setTimeout(function () {
                            }, 1500);
                            $('.no-notes').hide();

                        },
                        error: function (err) {
                            var error = JSON.parse(err.responseText);
                            $('#addNotesModal form').find("#form-response").addClass('error').text(error.message);
                            setTimeout(function () {
                            }, 1500);
                        }
                    });
                } else {
                    $('#addNotesModal').hide();
                    $('.modal-backdrop').remove();
                }
            }
        }



    });

    //Notes show more
    $(document).on('click', '#loadMore', function () {

        if ($('#stopFunction').val() == '0') {
            $('.load-more-btn').show();
        } else {
            $('.load-more-btn').hide();
        }
        showMoreNotes()
    });
    //END

    $(document).on('click', '.products-list', function () {

        $(this).addClass('selected').parent().parent().siblings().find('.selected').removeClass('selected')
    });

});

//Show/hide clients list
var ClientsListContainer = '#clients-list';
$(document).on('click', '.profile-nav a', function () {
    if (!$(ClientsListContainer).hasClass('hide-clients-list')) {
        $('.client-list-naviagtion').show();
        $(ClientsListContainer).animate({width: '0px'}).addClass('hide-clients-list');
        $('.right-content').css('width', '100%').css('width', '-=85px');
    }
});
$(document).on('click', '.client-list-naviagtion a', function () {
    if ($(ClientsListContainer).hasClass('hide-clients-list')) {
        $('.client-list-naviagtion').hide();
        $(ClientsListContainer).removeClass('hide-clients-list').animate({width: '270px'});
        $('.right-content').css('width', '100%').css('width', '-=355px');
    }
});

// To maintain responsiveness  when user resize screen
$(window).resize(function () {
    if ($('#clients-list').length != 0) {
        if ($(ClientsListContainer).hasClass('hide-clients-list')) {
            $('.right-content').css('width', '100%').css('width', '-=82px');
        }
        if (!$(ClientsListContainer).hasClass('hide-clients-list')) {
            $('.right-content').css('width', '100%').css('width', '-=352px');
        }
    }
})


function showMoreNotes() {
    var clientID = $('.notes-data').attr('data-clientid');
    var pageNo = $('.notes-data').attr('data-pageno');
    if ($('#stopFunction').val() == '0') {
        $.ajax({
            type: "GET",
            url: '/getNotesData?page=' + pageNo,
            data: {client_id: clientID},
            dataType: 'json',
            success: function (resp) {
                if (resp.notes.length < 15) {
                    $('#stopFunction').val('1');
                } else {
                }
                var html = '';
                $.each(resp.notes, function (key, value) {
                    html += '<div class="notes-details"> <p class="date">' + value.date + '</p><p class="note-info">' + value.note + '</p><hr></div>';

                });
                $('.notes-data').append(html);
                if (15 * pageNo < resp.totalRecords) {
                    pageNo++;

                    $('.notes-data').attr('data-pageno', pageNo);
                } else {
                    $('.load-more-btn').hide();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}


function getPreviousNotes() {
    var clientID = $('.previous-notes').attr('data-clientid');
    var pageNo = $('.previous-notes').attr('data-pageno');
    if ($('#stopFunction').val() == '0') {
        $.ajax({
            type: "GET",
            url: '/getNotesData?page=' + pageNo,
            data: {client_id: clientID},
            dataType: 'json',
            success: function (resp) {
                if (resp.notes.length < 15) {
                    $('#stopFunction').val('1');
                    $('.all-notes-message').show();
                } else {
                    $('.all-notes-message').hide();
                }
                var html = '';
                $.each(resp.notes, function (key, value) {
                    html += '<div class="modal-notes-details"> <p class="date">' + value.date + '</p><p class="employee-name">' + resp.employeeName + '</p><p class="note-info">' + value.note + '</p><hr></div>';

                });
                $('.previous-notes').append(html);
                if (15 * pageNo < resp.totalRecords) {
                    pageNo++;

                    $('.previous-notes').attr('data-pageno', pageNo);
                } else {
                    $('.loader').hide();
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    }
}
