$(document).ready(function () {
    $('#subscribe-newsletter').on('click', function (e) {
        $('#subscribe-modal').find('.server-response').empty();
        $('#subscribe-modal').modal('show');
        e.preventDefault();
    });
    $('#subscriptionForm,#shareForm').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $this.serialize(),
            success: function (resp) {
                $this.siblings('.server-response').html('<p class="success">' + resp.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            },
            error: function (err) {
                $this.siblings('.server-response').html('<p class="error">' + err.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            }
        });
    });
    $('.blog-isotope-container').imagesLoaded(function () {
        var blogList = $('.blog-isotope-container').isotope();

        $('.blog-isotope-links a').on('click', function (e) {
            var $this = $(this),
                    filterValue = $this.attr('data-filter');
            blogList.isotope({filter: filterValue});

            $this.parent().addClass('active');
            $this.parent().siblings().removeClass('active');
            e.preventDefault();
        });
    });

    $('.excerpt-handler').on('click', function () {
        var $this = $(this),
                currentValue = $this.attr('data-current');
        if (currentValue == 'more') {
            $this.text('less');
            $this.attr('data-current', 'less');
            $this.siblings('.excerpt-second').css({display: 'inline'});
        } else {
            $this.text('more');
            $this.attr('data-current', 'more');
            $this.siblings('.excerpt-second').css({display: 'none'});
        }
    });
});