$(document).ready(function () {

   

    $('#subscribe-newsletter').on('click', function () {
        $('#subscribe-modal').find('.server-response').empty();
        $('#subscribe-modal').modal();
    });
    $('#subscriptionForm,#shareForm').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        $.ajax({
            url: $this.attr('action'),
            type: 'POST',
            data: $this.serialize(),
            success: function (resp) {
                $this.siblings('.server-response').html('<p class="success">' + resp.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            },
            error: function (err) {
                $this.siblings('.server-response').html('<p class="error">' + err.message + '</p>');
                $this.slideUp();
                setTimeout(function () {
                    $this.slideDown().closest('.modal').modal('hide');
                    $this[0].reset();
                }, 3000);
            }
        });
    });
    $('#facebook-share  i > div').sharrre({
        share: {
            facebook: true
        },
        enableHover: false,
        enableTracking: true,
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('facebook');
        }
    });

    $('#twitter-share i > div').sharrre({
        share: {
            twitter: true
        },
        enableHover: false,
        enableTracking: true,
        buttons: {
            twitter: {
                via: '_TWPM'
            }
        },
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('twitter');
        }
    });
    $('#linkedin-share i > div').sharrre({
        share: {
            linkedin: true
        },
        enableHover: false,
        enableTracking: true,
        buttons: {
            linkedin: {
                via: '_TWPM'
            }
        },
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('linkedin');
        }
    });

    $('#google-share i > div').sharrre({
        share: {
            googlePlus: true
        },
        enableHover: false,
        enableTracking: true,
        click: function (api, options) {
            api.simulateClick();
            api.openPopup('googlePlus');
        }
    });
});
