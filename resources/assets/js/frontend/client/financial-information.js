$(document).ready(function () {    

    $("#tax_filing_status").selectBoxIt();

    $('#tax_filing_status').on('change', function () { 
        var $this = $(this);
        if ( $this.val() == 3 || $this.val() == 4 || $this.val() == 5 || $this.val() == 6) {
            $('.spouse_name, .spouse_annual_income').attr('required',true);
            $('form .two-options-auth').show();
            
        } else {
            $('.spouse_name, .spouse_annual_income').removeAttr('required');
            $('form .two-options-auth').hide();
        }
   
    });
    if ( $("#tax_filing_status").val() == 3 || $("#tax_filing_status").val() == 4 || $("#tax_filing_status").val() == 5 || $("#tax_filing_status").val() == 6) {
            $('.spouse_name, .spouse_annual_income').attr('required',true);
        $('form .two-options-auth').show();
        } else {
            $('.spouse_name, .spouse_annual_income').removeAttr('required');
            $('form .two-options-auth').hide();
        }
});