$(document).ready(function () {
    //checking if there are questions or not
    function isEmpty(el) {
        return !$.trim(el.html());
    }
    if (isEmpty($('#serviceQuestions-first'))) {
        $('#serviceQuestions-first').hide();
        $('#serviceQuestions-append').hide();
        $('#save-answer').hide();
        $('#ending-message').fadeIn();
        setTimeout(function () {
            $('#ending-message').remove();
            $('#settingup-dash').fadeIn();

            setTimeout(function () {
                endRegisterCall();
            }, 5000);
        }, 4000);
    } else {
        $('#save-answer').show();
    }
    //END
    //Questions submit event handler
    $('#step5Form').on('submit', function (e) {
        //Remove errors
        $('.each-question').each(function () {
            $(this).find('.err-messages').text('');
        });
        $('.each-question-input').each(function () {
            $(this).find('.err-messages').text('');
        });
        //Add loading
        $('#save-answer-out').addClass('spin').find('input').attr('disabled', true);

        $.ajax({
            type: "POST",
            url: saveAnswerUrl,
            data: new FormData($(this)[0]),
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (resp) {
                //Remove loading
                $('#save-answer-out').removeClass('spin').find('input').attr('disabled', false);

                //Append next five questions
                if (!$.isEmptyObject(resp.questionData.service_question)) {
                    var appendlocation = $('#serviceQuestions-append'),
                            template = $('#serviceQuestions-html').html();
                    template = _.template(template);
                    var data = {
                        serviceQuestion: resp.questionData.service_question
                    };
                    $('#serviceQuestions-first').empty();
                    appendlocation.empty().append(template(data));
                } else {
                    $('#serviceQuestions-first').hide();
                    $('#serviceQuestions-append').hide();
                    $('#save-answer').hide();
                    $('#ending-message').fadeIn();
                    setTimeout(function () {
                        location.href = paymentUrl;
                    }, 2000);

                }
            },
            error: function (err) {
                //Remove loading
                $('#save-answer-out').removeClass('spin').find('input').attr('disabled', false);
                ;
                var error = JSON.parse(err.responseText);
                $.each(error.message, function (i, v) {
                    $('#ques-' + i).find('.err-messages').text('This field is required.');
                });
            }
        });
        e.preventDefault();
    });
    //END
    
});