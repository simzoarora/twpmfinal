
var form;
$(document).ready(function () {
    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            current: "hello",
            finish: "finish",
            next: "next",
            previous: "back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
            var label = $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').attr('data-label');
            if (label) {      
                $('a[href="#next"]').text(label).append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } 
      

            if (currentIndex > previousIndex) {
                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');

                var callBackFunction = previousStepSection.data('callback');

                if (callBackFunction) {
                    window[callBackFunction]();
                }

                if (!jumpHappening) {
                    jump = previousStepSection.data('jump');
                    $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections').data('jump', 0);
                }
            }
            if ((currentIndex + 1) == $('#services-question-form fieldset').length) {
//                lastStep();
            }

        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                return form.valid();
            }
        },
        onFinishing: function (event, currentIndex)

        {
            if ($('.document-type:not(.hide) option:selected').val() == 0)
            {
                $('.document-type:not(.hide)').parent().append('<div class="error error-alert">Please select value</div>');
                return false; 
            }
            $('table tbody').find('label').remove();
            if ($('.add-child table tbody').find('tr').length === 0 && $('.table-confirmation:checked').val() == 'yes') {
                $(this).find('tbody').append('<label style="width: 100% !important;"  class="error error-alert">Please enter the details.</label>');
                return false;
            } else {
                $('table tbody').find('label').remove();
                $(this).remove('error error-alert');
                return form.valid();
            }
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });

    $('.family-info').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 1);
        } else {
            $(this).closest('.sections').data('jump', 0);
            $("#services-question-form-p-6 input").attr('disabled', false);
        }
    });
//    function lastStep() {
//        $('#homeValueFigure').text($('.house-value input').val());
//        $('#investments').text($('.investment-value input').val());
//        $('#mortgageFigure').text($('.total-mortgage input').val());
//        $('#otherDebtsFigure').text($('.total-other-debts input').val());
//        $('#dependentList,#documentList').html('');
//        $('.dependent-name').each(function (i, elem) {
//            $('#dependentList').append('<li>' + $(elem).text() + '</li>');
//        })
////        $('#firstDependent').text($('.dependent-name').text());
////            $('#secondDependent').text($('.student-name input').val());
//        $('.document-name').each(function (i, elem) {
//            $('#documentList').append('<li>' + $(elem).val() + '</li>');
//        })
////        $('#documentList').text($('.document-name').val());
//        $('#clientGrossIncome').text($('.gross-income input').val());
//        $('#spouseGrossIncome').text($('.spouse-income input').val());
//    }
    $(".actions").addClass('col-xs-offset-0 col-sm-offset-5 col-md-offset-5 col-lg-offset-5');
    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');


    $('a[href="#finish"]').css({'background': '#ffffff', 'color': '#1a88c8'}).append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    if ($("#services-question-form-p-0").is(':visible')) {
//        $('a[href="#previous"]').parent('li').css('display', 'inline').attr('aria-disabled', false);
    }


    $('a[href="#next"]').css({'background': '#ffffff', 'color': '#1a88c8'});
    $('a[href="#previous"]').addClass('prev-btn-link pull-left').append('<i style="background:#fff; font-size:15px; padding-right:6px; margin-top:6px;" class="fa fa-arrow-left pull-left"></i>');
    $('a[href="#next"]').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');

});

function addRemoveHref() {

    var cind = form.steps('getCurrentIndex');
    if (cind == 0) {
        $('a[href="#previous"]').attr('onclick', "window.location.href='" + backUrl + "'");
    } else {
        $('a[href="#previous"]').removeAttr('onclick');
    }
}

function deleteRow(index_id) {

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this information!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
//                    $('.fa-trash').closest('tr').remove();

                    $("[data-index=" + index_id + "]").remove();

                    swal("Your information has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Your information is safe!");
                }
            });

}


$("#services-question-form-p-1").append('<p id="return-later">Save and return later</p>');

