
var form;
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).on('keydown','input[type=number]',function (event) {


        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });
$(document).ready(function () {
    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });

    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "finish",
            next: "next",
            previous: "back"
        },

        onStepChanged: function (event, currentIndex, previousIndex) {

//            if (currentIndex > previousIndex) {
//                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');
//
//                var callBackFunction = previousStepSection.data('callback');
//                console.log(callBackFunction);
//                if (callBackFunction) {
//                    window[callBackFunction]();
//                }
//
//                if (!jumpHappening) {
//                    jump = previousStepSection.data('jump');
//                    $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections').data('jump', 0);
//                }
//            }
        },

        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                return form.valid();
            }
        },
        onFinishing: function (event, currentIndex)
        {
            if (($('.find-table-length tbody').find('tr').length === 0 && $('.table-confirmation:checked').val() == 'yes') || ($('#children-info1 tbody').find('tr').length === 0 && $('#educationalExpensesData').val() == '')) {
                if ($(this).find('tbody').find('.error-alert').length <= 0)
                    $(this).find('#children-info1').after('<label style="width: 30%;display: block;border-radius: 0;padding: 7px;font-weight: 400;padding-left: 15px;background-color: #f2dede;border-color: #ebccd1;color: #a94442;" class="error error-alert">This field is required.</label>');
                return false;
            } else {
                $('.find-table-length tbody').find('label').remove();
                $('.find-table-length tbody').remove('error error-alert');
                return true;
            }

            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });
//    if ($('#services-question-form-
//    p-0:visible').append('<input type="button" value="Back" class="footerBack col-md-offset-6" onclick="history.back(-1)" />')) 
    //    save and return later


    $(document).on('change', '.table-confirmation:checked', function () {
        if ($(this).val() == 'no') {
            $('.find-table-length tbody').find('label').remove();
            $('.find-table-length tbody').remove('error error-alert');
        } else {

        }
    });




    $(".actions").addClass('col-xs-offset-0 col-sm-offset-5 col-md-offset-5 col-lg-offset-5');
    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');

    $('a[href="#finish"]').css({'background': '#ffffff', 'color': '#1a88c8'}).append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    if ($("#services-question-form-p-0").is(':visible')) {
        $('a[href="#previous"]').parent('li').css('display', 'inline').attr('aria-disabled', false);
    }
//    $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    $('a[href="#next"]').css({'background': '#ffffff', 'color': '#1a88c8'});
    $('a[href="#previous"]').addClass('prev-btn-link pull-left').append('<i style="background:#fff; font-size:15px; padding-right:6px; margin-top:6px;" class="fa fa-arrow-left pull-left"></i>');
    $('a[href="#next"]').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');



});

function addRemoveHref() {

    var cind = form.steps('getCurrentIndex');
    if (cind == 0) {
        $('a[href="#previous"]').attr('onclick', "window.location.href='" + backUrl + "'");
    } else {
        $('a[href="#previous"]').removeAttr('onclick');
    }
}

function deleteRow(index_id) {

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this information!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
//                    $('.fa-trash').closest('tr').remove();

//                    $("[data-index="+index_id+"]").remove();

                    swal("Your information has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Your information is safe!");
                }
            });

}

$("#services-question-form-p-0").append('<p id="return-later">Save and return later</p>');
