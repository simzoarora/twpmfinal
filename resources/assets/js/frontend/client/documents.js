$(document).ready(function ($) {
    $(document).on('click', '.documents-nav li a', function (e) {
        var $this = $(this),
                href = $this.attr('href');
        href = href.toLowerCase().replace(/\s/g, "");
        $('.documents-type-table').css('display', 'none');
        $(href).css('display', 'block');
        $this.parent().siblings().removeClass('active');
        $this.parent().addClass('active');
        e.preventDefault();
    });


    $(document).on('dragover', '#file-dropzone', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
    $(document).on('dragenter', '#file-dropzone', function (e) {
        e.preventDefault();
        e.stopPropagation();
    })
    $(document).on('drop', '#file-dropzone', function (e) {
        e.preventDefault();
        e.stopPropagation();
        uploadDocument(e.originalEvent.dataTransfer.files[0]);
    });

    var isEditMode = false;
    var docFile = [];
    var docName = '';
    var notifyClient = false;
    var checkFileType = false;
    var notificationData = '';

    // Trigger click event of file input when click on dropzone area.
    $(document).on('click', "#file-dropzone", function () {
        $('#file_uploader').trigger('click');
    })

    // On browse document file
    $(document).on('change', '#file_uploader', function (event) {
        var fileData = event.target.files[0];
        uploadDocument(fileData);
    })

    // Upload file 
    function uploadDocument(fileData) {
        docFile = fileData;
        docName = fileData.name;

        var ext = docName.split('.').pop();
        if (ext != "pdf" && ext != "doc" && ext != "docx" && ext != "xls" && ext != "xlsx") {
            toastr.warning('You Have Uploaded ' + ext + ' file , Only .PDF, Doc/Docx, Xls/Xlsx files are allowed ');
            docName = '';
            $('#file_uploader').val(null);
            return;
        }
        checkFileType = true;
        $('#file-name-input').val(docName);
        $('.uploaded-doc-name').html(docName);
        $('#file-upload-box').hide();
        $('.file-preview-container').show();
        if (ext == 'pdf') {
            var fileSrc = window.URL.createObjectURL(docFile)
            previewPdfFile(fileSrc);
            $('#type-pdf').attr('data', fileSrc);
            $('#type-pdf').show();
            $('#type-doc, #type-docx, #type-xls').hide();
        } else if (ext == 'doc') {
            $('#type-doc').show();
            $('#type-pdf, #type-docx, #type-xls').hide();
        } else if (ext == 'docx') {
            $('#type-docx').show();
            $('#type-doc, #type-pdf, #type-xls').hide();
        } else {
            $('#type-xls').show();
            $('#type-doc, #type-docx, #type-pdf').hide();
        }

    }

    // show doc file name input box
    $(document).on('click', '#edit-file-name', function (event) {
        $('.file-info-container').show();
        $('.file-name-conatiner').hide();
    });
    // hide doc file name input box
    $(document).on('click', '#done-file-name', function (event) {
        $('.file-info-container').hide();
        $('.file-name-conatiner').show();
    });

//    Empty all modal fields 
    $(document).on('click', '#uploadDocs', function () {
        $('#file_uploader').val(null);
        $('#file-name-input').val('');
        $('textarea').val('');
//        $('#documentCategorySelect').val('1');
//        var selectBox = $("select").selectBoxIt();
//        $(selectBox).selectBoxIt('selectOption', 1);
        $('.uploaded-doc-name').html('');
        $('#file-upload-box').show();
        $('.file-preview-container').hide();
        $('.file-info-container').hide();
        $('.file-name-conatiner').show();
        notifyClient = false;
        docName = '';

    });

//    To get notify client checkbox value
    $(document).on('click', '#notify-client-check', function (event) {
        notifyClient = event.target.checked;
    });

    // call ajax for Initiate socket on upload
    $(document).on('click', '#uploadDocs', function (event) {
        var employeeId = $('#employee-id').val();
        var clientId = $('#client-id').val();

        var formData = new FormData();
        formData.append('client_id', clientId);
        formData.append('employee_id', employeeId);

        $.ajax({
            type: 'POST',
            url: '/getConnectionData',
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false,
            error: function () {
                console.log("Request Failed");
            },
            success: function (response) {
                if (response.notificationRoomData) {
                    notificationData = response.notificationRoomData;
                }
            }
        });
    });


    // Upload document file.
    $(document).on('click', '#upload-form', function (event) {
        let $this = $(this);      
        $this.after('<div class="lds-dual-ring"></div>');   
        $this.hide();
        var docCategory = $('#documentCategorySelect').val();
        var docDesc = $('#documentDesc').val();
        var employeeId = $('#employee-id').val();
        var clientId = $('#client-id').val();
        docName = $('#file-name-input').val();
        if (docName == '')
        {
            toastr.warning('Please add a document file!!');
             $('.lds-dual-ring').remove();
             $('#upload-form').show();
            return;
        }
        $this.attr('disabled', true);

        var formData = new FormData();
        formData.append('category', docCategory);
        formData.append('description', docDesc);
        formData.append('client_id', clientId);
        formData.append('employee_id', employeeId);
        formData.append('notify_client', notifyClient);
        formData.append('original_name', docName);


        if (notifyClient) {
            var senderId = notificationData.user_ids.split(',')[0];
            var receiverId = notificationData.user_ids.split(',')[1];
            var notificationRoomId = notificationData.id;

            formData.append('notification_room_id', notificationRoomId);
            formData.append('notification_sender_id', senderId);
            formData.append('notification_receiver_id', receiverId);
        }

        formData.append('filedata', docFile);

        // For edit document case;
        if (isEditMode) {
            var documentId = $('#document-id').val();
            formData.append('id', documentId);
            var ajaxUrl = '/updateDocument/' + documentId
        } else {
            var ajaxUrl = '/saveDocument'
        }

        $.ajax({
            type: 'POST',
            url: ajaxUrl,
            data: formData,
            processData: false, // tell jQuery not to process the data
            contentType: false,
            error: function () {
                alert("Request Failed");
            },
            success: function (response) {
                $('.lds-dual-ring').remove();
                $('#upload-form').show();
                var data = response.data;
                newRow = '<tr> \
                <td>' + data.updated_at + '</td>\
                <td>' + data.description + '</td> \
                <td><a href="/public/uploadedClientDocuments/' + data.document_path + '">' + data.original_name + '</a></td> \
                <td class="action-button"> \
                <a href="javascript:void(0)" class="editDocument" data-id="' + data.id + '" ><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a> \
                <span data-id="' + data.id + '" class="action-button delete-document"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span></td></tr>'
                $('#uploadDocsModal').modal('hide');
                $('#file-name-input').val('');
                $('.uploaded-doc-name').html('');
                $('#file-upload-box').show();
                $('.file-preview-container').hide();
                if (isEditMode) {
                    $("#doc-id" + data.id).replaceWith(newRow);
                } else {
                    var tableParentDiv = '';
                    console.log('docCategory', docCategory);
                    switch (docCategory) {
                        case '1':
                            tableParentDiv = 'statements';
                            break;
                        case '2':
                            tableParentDiv = 'taxforms';
                            break;
                        case '3':
                            tableParentDiv = 'insuranceforms';
                            break;
                        case '4':
                            tableParentDiv = 'otherdocs';
                            break;
                        default:
                            break;
                    }
                    console.log('tableParentDiv', tableParentDiv);
                    $('#' + tableParentDiv + ' #documents_table tbody').append(newRow);
                }
                $('#documentCategorySelect').val(1);
                $('#documentDesc').val('');
                $('#file-name-input').val('');
                $('#notify-client-check').prop('checked', false);
                $('#file-upload-box').show();
                $('.file-preview-container').hide();
                $('.file-info-container').hide();
                $('.file-name-conatiner').show();
                $this.attr('disabled', false);
                console.log('here');
                swal("Document has been uploaded successfully.", "", "success");
                isEditMode = false;
            }
        });
    })

    $(document).on('click', '#remove-file-button', function () {
        $('#file_uploader').val(null);
        $('#file-name-input').val('');
        $('.uploaded-doc-name').html('');
        $('#file-upload-box').show();
        $('.file-preview-container').hide();
        $('.file-info-container').hide();
        $('.file-name-conatiner').show();
        docName = '';
    })


    // Edit document file modal open.
    $(document).on('click', '.editDocument', function () {
        isEditMode = true;
        var docID = $(this).attr('data-id');
        $('#uploadDocsModal').modal('show');
        $('.file-info-container').hide();
        $('.file-name-conatiner').show();
        $.ajax({
            type: 'GET',
            url: '/editDocument/' + docID,
            processData: false, // tell jQuery not to process the data
            contentType: false,
            error: function () {
                console.log("Something went wrong!!");
            },
            success: function (response) {
                var data = response.data;
                $('#documentCategorySelect').val(data.document__categories_id);
                $('#documentDesc').val(data.description);
                $('#document-id').val(data.id);
                $('#file-name-input').val(data.original_name);
                $('.uploaded-doc-name').html(data.original_name);
                var docFilePath = data.document_path;
                var ext = docFilePath.split('.').pop();
                checkFileType = true;
                $('#file-upload-box').hide();
                $('.file-preview-container').show();
                if (ext == 'pdf') {
                    $('#type-pdf').attr('data', 'uploadedClientDocuments/' + docFilePath);
                    $('#type-pdf').show();
                    $('#type-doc, #type-docx, #type-xls').hide();
                } else if (ext == 'doc') {
                    $('#type-doc').show();
                    $('#type-pdf, #type-docx, #type-xls').hide();
                } else if (ext == 'docx') {
                    $('#type-docx').show();
                    $('#type-doc, #type-pdf, #type-xls').hide();
                } else {
                    $('#type-xls').show();
                    $('#type-doc, #type-docx, #type-pdf').hide();
                }
            }
        });
    })

    // Deleted document file.
    $(document).on('click', '.delete-document', function () {
        var docID = $(this).attr('data-id');
        $this = $(this);
        swal({
            title: "Are you sure?",
            text: "Please click on finish for final submission.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: 'POST',
                            url: '/deleteDocument/' + docID,
                            processData: false, // tell jQuery not to process the data
                            contentType: false,
                            error: function () {
                                alert("Something went wrong!!");
                            },
                            success: function (response) {
                                $this.parent().parent().remove();
                                swal("Selected document has been deleted!", {
                                    icon: "success",
                                });
                            }
                        });
                    }
                });
    });
    $(document).on('change', '#documentCategorySelect', function () {
        if ($(this).val() == '5') {
            $('#select_template_or_upload_new_div').show();
        } else {
            $('#select_template_or_upload_new_div').hide();
        }
    });

    $(document).on('change', 'input[type=radio][name=select_template_or_upload_new]', function () {
        let value = $(this).val();
        console.log('value', value);
        if (value == 'select_template') {
            $('#file_upload_div').hide();
            $('#template_dropdown').show();
        } else if (value == 'upload_new') {
            $('#file_upload_div').show();
            $('#template_dropdown').hide();
        }
    });



    // Preview Pdf file with only first page
    function previewPdfFile(fileUrl) {
        var __PDF_DOC,
                __PAGE_RENDERING_IN_PROGRESS = 0,
                __CANVAS = $('#pdf-canvas').get(0),
                __CANVAS_CTX = __CANVAS.getContext('2d');

        PDFJS.getDocument({url: fileUrl}).then(function (pdf_doc) {
            __PDF_DOC = pdf_doc;

            // Show the first page
            // Fetch the page
            __PDF_DOC.getPage(1).then(function (page) {
                // As the canvas is of a fixed width we need to set the scale of the viewport accordingly
                var scale_required = __CANVAS.width / page.getViewport(1).width;

                // Get viewport of the page at required scale
                var viewport = page.getViewport(scale_required);

                // Set canvas height
                __CANVAS.height = viewport.height;

                var renderContext = {
                    canvasContext: __CANVAS_CTX,
                    viewport: viewport
                };

                // Render the page contents in the canvas
                page.render(renderContext).then(function () {
                    __PAGE_RENDERING_IN_PROGRESS = 0;

                });
            });
        }).catch(function (error) {
            console.log(error.message);
        });
    }

});