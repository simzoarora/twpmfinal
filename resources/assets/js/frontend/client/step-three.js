$(document).ready(function () {
    //Send OTP ajax call
    $('#send-otp').on('click', function (e) {
        e.preventDefault();
        var userId = $('#user-id').val(),
                mobileNumber = $('#mobile-number').val();

        //Remove error messages
        $(".send-otp-success").text('').hide();
        $(".send-otp-error").text('').hide();
        
        $.ajax({
            type: "POST",
            url: sendOtpUrl,
            data: {
                user_id: userId,
                mobile_number: mobileNumber
            },
            dataType: 'json',
            success: function (resp) {
                $(".send-otp-success").text(resp.message).show();
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $(".send-otp-error").text(error.message).show();
            }
        });
    });
    //END
    //Verify OTP ajax call
    $('#verify-otp').on('click', function (e) {
        e.preventDefault();
        var userId = $('#user-id').val(),
                mobileNumber = $('#mobile-number').val(),
                otp = $('#otp-code').val(),
                profile_process_val = $('#profile_process').val();

        //Remove error messages
        $(".send-otp-success").text('').hide();
        $(".send-otp-error").text('').hide();

        $.ajax({
            type: "POST",
            url: verifyOtpUrl,
            data: {
                user_id: userId,
                mobile_number: mobileNumber,
                otp: otp,
                profile_process: profile_process_val,
            },
            dataType: 'json',
            success: function (resp) {
                $(".send-otp-success").text(resp.message).show();
                $("#step-three-submit").removeAttr('disabled');
                $('.client-verify-mobile').hide();
                $("#mobile-verify-mobile-success").show();

            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $(".send-otp-error").text(error.message).show();
            }
        });
    });
    //END
    
    $('#mobile-number').on('change', function(e){
        $('#step-three-submit').attr('disabled', 'disabled');
    });
});