$(document).ready(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

    //Make selects css better
    $(".selectboxit").selectBoxIt();
    $(".selectboxit").on({
        open: function () {
            $(this).data("selectBox-selectBoxIt").dropdown.addClass("dropup");
        },
        close: function () {
            $(this).data("selectBox-selectBoxIt").dropdown.removeClass("dropup");
        }
    });

    $('#change-password-btn').on('click', function (e) {
        e.preventDefault();
        $('#change-password-div').show();
        $('#changePasswordForm')[0].reset();
        $(".old-password-err").empty();
        $(".new-password-err").empty();
        $('#change-password-btn').hide();
    });

    $('#cancel-btn').on('click', function (e) {
        e.preventDefault();
        $('#change-password-div').hide();
        $('#change-password-btn').show();
    });

    $('#change-password-submit-btn').on('click', function (e) {
        e.preventDefault();
        var formData = new FormData($('#changePasswordForm')[0]);
        $(".old-password-err").empty();
        $(".new-password-err").empty();

        $.ajax({
            type: "POST",
            url: changePasswordRoute,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function (resp) {
                $(".change-pass-succ-msg").append(resp.message);
                $('#change-password-div').hide();
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                if (typeof error.message != 'undefined') {
                    $(".old-password-err").append(error.message + "<br/>");
                } else {
                    $.each(error, function (key, value) {
                        $.each(value, function (inKey, inValue) {
                            if (key == 'password') {
                                $(".new-password-err").append(inValue + "<br/>");
                            } else if (key == 'old_password') {
                                $(".old-password-err").append(inValue + "<br/>");
                            }
                        });
                    });
                }
            }
        });
    });

//    $('#two-factor-auth-btn').on('click', function (e) {
//        e.preventDefault();
//
//        //Remove error messages
//        $(".send-otp-success").text('').hide();
//        $(".send-otp-error").text('');
//        $(".mobile-verification-link").hide();
//        var change = $("#two-factor-auth").val();
//
//        $.ajax({
//            type: "POST",
//            url: changeTwoFactorAuthRoute,
//            data: {
//                change: change,
//            },
//            dataType: 'json',
//            success: function (resp) {
//                if (change == 1) {
//                    var statusVal = 'On';
//                } else {
//                    var statusVal = 'Off';
//                }
//                if (resp.status == 1) {
//                    $(".two-fact-success").text(resp.message);
//                    $(".status").text(statusVal);
//                    $("#two-factor-auth-btn").hide();
//                } else {
//                    $(".two-fact-error").text(resp.message);
//                    $(".mobile-verification-link").show();
//                }
//            },
//            error: function (err) {
//                var error = JSON.parse(err.responseText);
//                $(".two-fact-error").text(error.message);
//            }
//        });
//    });

    //opening setting page
    if (typeof location.hash != 'undefined') {
        $('a[href="' + location.hash + '"]').click();
    }
    //END

    //personal profile modal
    $('#personal-profile-open').on('click', function () {
        $('#personal-profile-modal').modal('show');
    });
    $('#personal-profile-modal form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        //Remove error messages
        $this.find("#form-response").text('').removeClass('success').removeClass('error');

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.find("#form-response").addClass('success').text(resp.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('#personal-profile-modal form').find("#form-response").addClass('error').text(error.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });
    //END
    //
    $('.edit-icon-profiles').click(function () {
        var id = this.id;
        $('#' + id + '-modal').modal('show');
    });

    $('.all-profile-modal form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        //Remove error messages
        $this.find(".form-response").text('').removeClass('success').removeClass('error');

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.find(".form-response").addClass('success').text(resp.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('.all-profile-modal form').find(".form-response").addClass('error').text(error.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });

    //User name edit 
    $('#name-edit-open').on('click', function () {
        $('#name-edit-modal').modal('show');
    });
    $('#name-edit-modal form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        //Remove error messages
        $this.find("#form-response").text('').removeClass('success').removeClass('error');

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.find("#form-response").addClass('success').text(resp.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('#name-edit-modal form').find("#form-response").addClass('error').text(error.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });
    //END

    //Employee modal
    $('#employee-info-open').on('click', function () {
        $('#employee-info-modal').modal('show');
    });
    $('#employee-info-modal form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        //Remove error messages
        $this.find("#form-response").text('').removeClass('success').removeClass('error');

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.find("#form-response").addClass('success').text(resp.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('#employee-info-modal form').find("#form-response").addClass('error').text(error.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });
    //END
    //Financial modal
//      $("#tax_filing_status").selectBoxIt();
    $('#tax_filing_status').on('change', function () {
        var $this = $(this);
        if ( $this.val() == 3 || $this.val() == 4 || $this.val() == 5 || $this.val() == 6) {
            $('.spouse_name, .spouse_annual_income').attr('required',true);
            $('form .two-options-auth').show();
            
        } else {
            $('.spouse_name, .spouse_annual_income').removeAttr('required');
            $('form .two-options-auth').hide();
        }
   
    });
     if ( $("#tax_filing_status").val() == 3 || $("#tax_filing_status").val() == 4 || $("#tax_filing_status").val() == 5 || $("#tax_filing_status").val() == 6) {
        $('.spouse_name, .spouse_annual_income').attr('required',true);    
        $('form .two-options-auth').show();
        } else {
            $('.spouse_name, .spouse_annual_income').removeAttr('required');
            $('form .two-options-auth').hide();
        }
    $('#financial-back-open').on('click', function () {
        $('#financial-back-modal').modal('show');
    });
    $('#financial-back-modal form').on('submit', function (e) {
        e.preventDefault();
        var $this = $(this);
        //Remove error messages
        $this.find("#form-response").text('').removeClass('success').removeClass('error'); 

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $this.find("#form-response").addClass('success').text(resp.message);
                setTimeout(function () {
                    location.reload();
                }, 3000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('#financial-back-modal form').find("#form-response").addClass('error').text('Error');
                setTimeout(function () {
                    location.reload();
                }, 3000);
            }
        });
    });
    //END

});