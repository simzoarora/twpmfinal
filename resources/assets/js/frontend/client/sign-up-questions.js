$(document).ready(function () {
    $("select-type").selectBoxIt();
    var form = $("#example-advanced-form");
    form.on('click', '.plans-wrapper', function () {
        $(this).addClass('selected').siblings().removeClass('selected');
        $('#customSelection').val($(this).attr('data-percentage'));
    });
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            if (element.closest('ul').length) {  
                element.find(error).remove();
                element.closest('ul').after(error.addClass('alert alert-danger').text('Please answer this question.'));
            } else {
                element.find(error).remove();
                element.after(error.addClass('alert alert-danger').text('Please answer this question.'));
            }
        },
        rules: {
            input: {
                required: true,
                min: 1
            },
            employeeno: {
                pattern: /^[0-9,]+$","/,
                minlength: 1,
                maxlength: 20
            }
        }
    });

    form.children("div").steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        labels: {
            finish: "Finish",
            next: "Continue",
            previous: "Back"
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (currentIndex > newIndex) {
                //going to previous
                return form;
            } else {
                //going to next
                if ($('#steps-uid-0-p-' + currentIndex).find('select').length > 0) {
                    var selectSel = $('#steps-uid-0-p-' + currentIndex);
                    var selectVal = selectSel.find('select').val();
                    if (selectVal == 0) {
                        selectSel.find('label').remove();
                        selectSel.find('h2').append('<label class="error alert alert-danger">Please answer this question.</label>');
                    } else {
                        selectSel.find('label').remove();
                        return form.valid();
                    }
                } else {
                    return form.valid();
                }

            }
        },
        onStepChanged: function (event, currentIndex, priorIndex)
        {
            if ($('#steps-uid-0-p-16').is(':visible')) {
                $('a[href="#previous"]').click(function () {

                });
            } else {
                $('.step,.question-heading').show();
            }
            if ($('#steps-uid-0-p-14').is(':visible')) {
                $('a[href="#next"]').click(function () {
                    $('.actions').hide();
                    setTimeout(function () {
                        form.children("div").steps('next');
                        $('#steps-uid-0-p-15').addClass('loaderStep1');
                        $('.actions').show();
                    }, 3000);
                });
            }
            if ($('.loaderStep1').is(':visible')) {
                form.children("div").steps('previous');
                $('.loaderStep').removeClass('loaderStep1');
            } else {
                $('.step,.question-heading').show();
            }


            if ($('#steps-uid-0-p-1').is(':visible')) {
                $('.step p').text('Step 2: Assess your risk tolerance');
            } else {
                $('.step').show();
            }

            if ($('#steps-uid-0-p-2, #steps-uid-0-p-4').is(':visible')) {
                $('.step p').text('Step 2: Assess your risk tolerance (continued)');
            } else {
                $('.step p').text('Step 2: Assess your risk tolerance');
                if ($('#steps-uid-0-p-0').is(':visible')) {
                    $('.step p').text('Step1: Define your investment goals');
                } else {
                    $('.step').show();
                }
            }
            if ($('#steps-uid-0-p-0').is(':visible')) {
                $('.blue-line-progress').css('width', ('0') + '%');
            } else {
                $('.blue-line').css('width', (currentIndex * 6.66) + '%');
            }
            $('.blue-line').css('width', (currentIndex * 6.66) + '%');
        },
        onFinishing: function (event, currentIndex)
        {
            event.preventDefault();
            $('a[href="#previous"]', form).hide(); 
            $('a[href="#finish"]', form).attr('href', '#finishx').text('Processing...');
            setTimeout(function(){ $('a[href="#finishx"]', form).text('Processing.... almost done....'); }, 4000);
            
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                dataType: 'json',
                success: function (resp) {
                    window.location = resp.url;
                },
                error: function (error) {
                    $('a[href="#previous"]', form).show(); 
                    $('a[href="#finishx"]', form).attr('href', '#finish').text('Finish');
                    twpmApp.ajaxInputError(error, $("#example-advanced-form"));
                }
            });
        },
        onFinished: function (event, currentIndex)
        {
            event.preventDefault();
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form.serialize(),
                dataType: 'json',
                success: function (resp) {
                    window.location = resp.url;
                },
                error: function (error) {
                    twpmApp.ajaxInputError(error, $("#example-advanced-form"));
                }
            });
        }
    });
    $("select").selectBoxIt();
    $("#steps-uid-0-p-0").append($('#debt-markup').html());
    $("#steps-uid-0-p-6").append('<div class="liquid-investments"> <p> (savings, CDs, mutual funds, IRAs, public stocks, etc) </p> </div>');
    $('.hideStep').removeClass('hideStep');
});
