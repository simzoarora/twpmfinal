$(document).ready(function () {
    $("select").selectBoxIt();

    $('#type').on('change', function () {
        ;
        if ($(this).val() == 1) {
            $('form .authentication').show();
        } else {
            $('.authentication').hide();
        }

    })

    $('#two-factor-authentication').on('change', function () {
        if ($(this).is(':checked')) {
            $.ajax({
                type: 'POST',
                url: "{{route('frontend.client.sendOtp')}}",
                data: {
                    phone_number: $('#phone_number').val()
                },
                dataType: 'json',
                success: function (resp) {
                    $('.one-time-password').append('<p>Enter OTP</p> <input type="number" class="otp" name="verify_otp" required/> <button type="button" class="btn" id="sendOtp">OK</button>');
                    $('.site-heading-1').hide();
                },
                error: function (error) {

                }
            });
        } else {
            $('.one-time-password').empty();
        }
    });

    var form = $("#contact-us");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('Please answer this question'));
        },
        ignore: '',
        rules: {
            input: {
                required: true,
                min: 1
            },
            select: {
                required: true,
                min: 1
            }
        }
    });
    $('#get-started-modal input[type="text"],#get-started-modal input[type="number"],#get-started-modal input[type="email"]').val('');
    $('.service-question-custom-select').val("").trigger('change');
    var form1 = $("#security-form");
    form1.validate({
        errorPlacement: function errorPlacement(error, element) {
            if (element.closest('ul').length) {
                element.closest('.inner-left').append(error.addClass('alert alert-danger').css({'margin-top': '0px', 'clear': 'both'}).text('Please answer this questions'));
            } else {
            }
            element.closest('.inner-left').append(error.addClass('alert alert-danger').css({'margin-top': '0px', 'clear': 'both'}).text('Please fill this field'));
        },
        rules: {
            input: {
                required: true,
                min: 1
            }
        }
    });

    $('form').on('click', '#sendOtp', function () {


        $.ajax({
            type: 'POST',
            url: "{{route('frontend.client.verifyOtp')}}",
            data: {
                phone_number: $('#phone_number').val(),
                verify_otp: $('.otp').val()
            },
            dataType: 'json',
            success: function (resp) {
                $('.site-heading-1').show();
            },
            error: function (error) {

            }
        });
    }
    );

});