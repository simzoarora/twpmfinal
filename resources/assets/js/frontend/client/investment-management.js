var form;
$(document).ready(function () {



    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        ignore: ".ignore, :hidden",
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "Finish",
            next: "Continue",
            previous: "Back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
            if (currentIndex > previousIndex) {
                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');

                var callBackFunction = previousStepSection.data('callback');

                if (callBackFunction) {
                    window[callBackFunction]();
                }
            }
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {

            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                return true;

            }
        },
        onFinishing: function (event, currentIndex)
        {
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });

    //select account type
    $('.acc-name').on('click', function () {
        var $this = $(this);
        $this.addClass('selected').siblings('.selected').removeClass('selected');
    });
    $('.marital_status').on('click', function () {
        var $this = $(this);
        $this.addClass('maritalselected').siblings('.maritalselected').removeClass('maritalselected');
    });
    $('.phone').on('click', function () {
        var $this = $(this);
        $this.addClass('phoneselected').siblings('.phoneselected').removeClass('phoneselected');
    });
    $('.doc').on('click', function () {
        var $this = $(this);
        $this.addClass('docselected').siblings('.docselected').removeClass('docselected');
    });

    $('.tax_type').on('click', function () {
        var $this = $(this);
        $this.addClass('taxselected').parents('.col-sm-12').siblings().find('.taxselected').removeClass('taxselected');

        if ($(this).attr('id') === 'other_tax') {
            $('#other_tax').show();
        } else {
            $('#other_tax').hide();
        }

    });

});
