var form;
$(document).on('keypress', 'input[type=text], textarea', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 48 && inputValue <= 122) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
//   for decimal input 
    $(document).on('keydown','input[type=number]',function (event) {
    if (event.shiftKey == true) {
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
    } else {
        event.preventDefault();
    }

    if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
        event.preventDefault();

});
$(document).ready(function () {

//   back button
    $(document).on('click', '.backBtn', function () {
        if ($(".modal-body .setup-content:visible").prev().length != 0) {
            $(".modal-body .setup-content:visible").prev().show().next().hide();
        } else {
            $(".modal-body .setup-content:visible").hide();
            $(".modal-body .setup-content:visible:last").show();
        }
        return false;
    });
    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').find('.error-alert').remove();
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "finish",
            next: "next",
            previous: "back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
            $("select").selectBoxIt();
            if (currentIndex > previousIndex) {
                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');
                var callBackFunction = previousStepSection.data('callback');
                if (callBackFunction) {
                    window[callBackFunction]();
                }
            }

        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            $("select").selectBoxIt();
            if ($('a[href="#next"]').text() == 'Finish' || $('a[href="#next"]').text() == 'finish') {
                $("form").steps("finish");
                return false;
            }
            var tableValidation = $('#services-question-form-p-' + currentIndex + ' .section-right table tbody');
            if ($(tableValidation).find('tr').length === 0 && $('#services-question-form-p-' + currentIndex + ' .table-confirmation:checked').val() == 'yes') {
                if ($(tableValidation).find('.error-alert').length <= 0)
                    $(tableValidation).append('<label style="width: 100% !important;"  class="error error-alert">This field is required.</label>');
                return false;
            } else {

                $('section.sections').closest('.section-right table tbody').find('label').remove();
                $('section.sections').closest('.section-right table tbody').remove('error error-alert');
            }

            if (newIndex < currentIndex) {
                return true;
            } else {
//                return true;
                return form.valid();
            }
            if (newIndex > currentIndex) {
                return true;
            } else {
//                return true;
                return true;
            }

        },
        onFinishing: function (event, currentIndex)
        {
            if ($('a[href="#finish"]').text() == 'Continue') {
                $("form").steps("next");
                return false;
            }
            if ($('a[href="#finish"]').text() == 'next') {
                $("form").steps("next");
                return false;
            }
            var tableValidation = $('#services-question-form-p-' + currentIndex + ' .section-right table tbody');
            if ($(tableValidation).find('tr').length === 0 && $('#services-question-form-p-' + currentIndex + ' .table-confirmation:checked').val() == 'yes') {
                if ($(tableValidation).find('.error-alert').length <= 0)
                    $(tableValidation).append('<label style="width: 100% !important;"  class="error error-alert">This field is required.</label>');
                return false;
            } else {

                $('section.sections').closest('.section-right table tbody').find('label').remove();
                $('section.sections').closest('.section-right table tbody').remove('error error-alert');
            }
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {

            form.submit();
        }
    });



    $(document).on('change', '.table-confirmation:checked', function () {
        if ($(this).val() == 'no') {
            $('.find-table-length tbody').find('label').remove();
            $('.find-table-length tbody').remove('error error-alert');
//            $('.find-table-length tbody tr').remove();
        } else {

        }
    });



    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    $(".actions").addClass('col-xs-offset-0 col-sm-offset-5 col-md-offset-5 col-lg-offset-5');
//    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');

    $('a[href="#finish"]').css({'background': '#ffffff', 'color': '#1a88c8'}).append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    if ($("#services-question-form-p-0").is(':visible')) {
        $('a[href="#previous"]').parent('li').css('display', 'inline').attr('aria-disabled', false);
    }
//    $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    $('a[href="#next"]').css({'background': '#ffffff', 'color': '#1a88c8'});
    $('a[href="#previous"]').addClass('prev-btn-link pull-left').append('<i style="background:#fff; font-size:15px; padding-right:6px; margin-top:6px;" class="fa fa-arrow-left pull-left"></i>');
    $('a[href="#next"]').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');

});

function addRemoveHref() {

    var cind = form.steps('getCurrentIndex');
    if (cind == 0) {
        $('a[href="#previous"]').attr('onclick', "window.location.href='" + backUrl + "'");
    } else {
        $('a[href="#previous"]').removeAttr('onclick');
    }
}

function deleteRow(index_id) {

    swal({
        title: "Are you sure?",
        text: "Once deleted, you will not be able to recover this information!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {
//                    $('.fa-trash').closest('tr').remove();
//                    oddEven(index_id);
//                    $("[data-index=" + index_id + "]").remove();
                    return true;
                    swal("Your information has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Your information is safe!");
                }
            });

}
//$("#services-question-form-p-1").append('<p id="return-later">Save and return later</p>');