var form;
       $(document).on('keypress', 'input[type=text]', function (e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode; 
       if(!(inputValue >= 65 && inputValue <= 124) && (inputValue != 32 && inputValue != 0)){ 
           event.preventDefault();
       }          
   });
   $(document).on('keydown','input[type=number]',function (event) {


        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });

$(document).ready(function () {
    $(document).on('click', '.contact-modal-show', function (e) {
        $('#contact-us').show();
        $('.contact-response').html('');
        $('#get-started-modal').modal();
        e.preventDefault();
    });


    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "Finish",
            next: "Continue",
            previous: "Back"
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {

            $(document).on('click', 'a[href="#finish"]', function () {
                if ($('table#student-info tbody').find('tr').length === 0) {
                    $('table#student-info tbody').append('<label class="error-alert">This field is required.</label>');
                    return false;
                } else {
                    $('table#student-info tbody').find('.error-alert').remove();
                    return form.valid();
                }
            });


            if (newIndex < currentIndex) {
                return true;

            } else {
                return form.valid();
            }
        },
        onFinishing: function (event, currentIndex)
        {
        if ($('#student-info tbody').find('tr').length === 0) {
            console.log($('.student-info tbody').find('tr').length);
                $(this).find('tbody').append('<label style="width: 100% !important;"  class="error error-alert">Please enter the details.</label>');
                return false;
            } else {
                $('#student-info tbody').find('label').remove();
                $(this).remove('error error-alert');
                return form.valid();
            }
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {

            form.submit();
        }
    });
    //    save and return later
    $(".actions").addClass('col-xs-offset-0 col-sm-offset-5 col-md-offset-5 col-lg-offset-5');
    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');

});
//$("#services-question-form-p-1").append('<p id="return-later">Save and return later</p>');
