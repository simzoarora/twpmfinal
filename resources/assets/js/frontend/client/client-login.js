$(document).ready(function () {
    //Login event handler
    $('#client-login-form').submit(function (e) {
        //Remove error
        $('#client-login-error').removeClass('box-error').html('');
        $('#otp-message').removeClass('box-success').html('');

        var $this = $(this);
        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                if (resp.status == 1) {
                    $('#verify-otp').slideDown();
                    $('.login-form').slideUp();
                    $('#otp-message').addClass('box-success').html(resp.message);
                    $('#mobile-number').val(resp.mobileNumber);
                    $('#user-id').val(resp.userId);
                } else {
                    location.href = resp.route;
                }
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $('#client-login-error').addClass('box-error').html(error.message);

            }
        });
        e.preventDefault();
    });
    //END
    //Verify otp event handler with login
    $('#verify-otp-form').submit(function (e) {
        //Remove error
        $('#otp-message').removeClass('box-success').removeClass('box-error').html('');

        var $this = $(this);
        //Remove error
        $this.find('.err-messages').text('');

        if ($.isNumeric($('#otp-input').val())) {
            $.ajax({
                type: "POST",
                url: $this.attr('action'),
                data: $this.serialize(),
                dataType: 'json',
                success: function (resp) {
                    location.href = resp.route;
                },
                error: function (err) {
                    var error = JSON.parse(err.responseText);
                    $('#otp-message').addClass('box-error').html(error.message);
                }
            });
        } else {
            $this.find('.err-messages').html('Invalid code. Digits are allowed.');
        }
        e.preventDefault();
    });
    //END
});