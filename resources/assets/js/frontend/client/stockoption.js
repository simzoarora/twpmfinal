var form;
$(document).ready(function () {
 
    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            current: "hello",
            finish: "Finish",
            next: "Continue",
            previous: "Back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
            var label = $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').attr('data-label');
            if (label) {
                $('a[href="#next"]').text(label);
            } else {
                $('a[href="#next"]').text('Continue');
            }

            if (currentIndex > previousIndex) {
                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');

                var callBackFunction = previousStepSection.data('callback');

                if (callBackFunction) {
                    window[callBackFunction]();
                }

                if (!jumpHappening) {
                    jump = previousStepSection.data('jump');
                    $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections').data('jump', 0);
                }
            }
            if ((currentIndex + 1) == $('#services-question-form fieldset').length) {
                lastStep();
            }

        },
        onStepChanging: function (event, currentIndex, newIndex)
        {
            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                return form.valid();
            }
        },
        onFinishing: function (event, currentIndex)
        {
             
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });

    $('.family-info').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 1);
        } else {
            $(this).closest('.sections').data('jump', 0);
            $("#services-question-form-p-6 input").attr('disabled', false);
        }
    });
    function lastStep() {
        $('#homeValueFigure').text($('.house-value input').val());
        $('#investments').text($('.investment-value input').val());
        $('#mortgageFigure').text($('.total-mortgage input').val());
        $('#otherDebtsFigure').text($('.total-other-debts input').val());
        $('#dependentList,#documentList').html('');
        $('.dependent-name').each(function (i, elem) {
            $('#dependentList').append('<li>' + $(elem).text() + '</li>');
        })
//        $('#firstDependent').text($('.dependent-name').text());
//            $('#secondDependent').text($('.student-name input').val());
        $('.document-name').each(function (i, elem) {
            $('#documentList').append('<li>' + $(elem).val() + '</li>');
        })
//        $('#documentList').text($('.document-name').val());
        $('#clientGrossIncome').text($('.gross-income input').val());
        $('#spouseGrossIncome').text($('.spouse-income input').val());
    }

});
$("#services-question-form-p-1").append('<p id="return-later">Save and return later</p>');

//annual portfolio review js

var form;
$(document).ready(function () {



    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
//        $('.custom-validation').rules("add", {regex: '^(100|[1-9][0-9]?)$'}); 

//    $('.file-manager-link').filemanager('file');

    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        ignore: ".ignore, :hidden",
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "Finish",
            next: "Continue",
            previous: "Back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
            var label = $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').attr('data-label');
            if (label) {
                $('a[href="#next"]').text(label);
            } else {
                $('a[href="#next"]').text('Continue');
            }

            if (currentIndex > previousIndex) {
                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');

                var callBackFunction = previousStepSection.data('callback');

                if (callBackFunction) {
                    window[callBackFunction]();
                }

                if (!jumpHappening) {
                    jump = previousStepSection.data('jump');
                    $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections').data('jump', 0);
                }
            }
//            if(currentIndex==6){
//                $('#services-question-form-p-5 input').prop("disabled","disabled");
////            }
//            if (jump > 0) {
//                jumpHappening = true;
//                $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').find('input, select').attr('disabled', true);
//                jump--;
//                if (jump == 0) {
//                    jumpHappening = false;
//                }
//                form.steps('next');
//            } else {
//                $(this).closest('.sections').data('jump', 0);
////                $("#services-question-form-p-4 input, #services-question-form-p-5 input, #services-question-form-p-6 input").attr('disabled', false);
////                $("#services-question-form-p-7 input").attr('disabled', false);
////                $("#services-question-form-p-9 input,#services-question-form-p-10 input").attr('disabled', false);
////                $("#services-question-form-p-12 input,#services-question-form-p-13 input").attr('disabled', false);
////                $("#services-question-form-p-15 input,#services-question-form-p-16 input").attr('disabled', false);
//            }
        },
        onStepChanging: function (event, currentIndex, newIndex)
        {

            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                  return form.valid();

            }
        },
        onFinishing: function (event, currentIndex)
        {
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });



//    $('.other-non-rental').on('change', function () {
//        if ($(this).val() == 'yes') {
//
//            var benFormMainTemplate = $('#new-data-entry-template').html();
//            benFormMainTemplate = _.template(benFormMainTemplate);
//            var benFromTemplateData = {
//                count: $('.new-data-entry-row').length
//            };
//            $('#otherNonRentalForm').append(benFormMainTemplate(benFromTemplateData));
//        } else {
//            $('#otherNonRentalForm').html('');
//        }
//        e.preventDefault();
//    });
    $('.other-non-rental').on('change', function () {
        if ($(this).val() == 'yes') {

            $('#otherNonRentalForm').html('');
        }
    });
    $('.non-rental').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 0);
        } else {
            $(this).closest('.sections').data('jump', 0);
//            $("#services-question-form-p-4 input, #services-question-form-p-5 input, #services-question-form-p-6 input").attr('disabled', false);
        }
    });
    $('.home-equity').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 2);
        } else {
            $(this).closest('.sections').data('jump', 0);
//            $("#services-question-form-p-7 input").attr('disabled', false);
        }
    });
    $('.vehicle-loan').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 2);
        } else {
            $(this).closest('.sections').data('jump', 0);
//            $("#services-question-form-p-9 input,#services-question-form-p-10 input").attr('disabled', false);
        }
    });
    $('.credit-card').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 2);
        } else {
            $(this).closest('.sections').data('jump', 0);
//            $("#services-question-form-p-12 input,#services-question-form-p-13 input").attr('disabled', false);
        }
    });
    $('.student-loan').on('change', function () {
        if ($(this).val() == 'no') {
            $(this).closest('.sections').data('jump', 2);
        } else {
            $(this).closest('.sections').data('jump', 0);
//            $("#services-question-form-p-15 input,#services-question-form-p-16 input").attr('disabled', false);
        }
    });

//    $('.non-rental').on('change', function () {
//        if ($(this).val() == 'no') {
//            form.steps("setStep", 8);
//        }
//    });
    $(document).on('blur', '.taxable-income', function () {

        event.preventDefault();
        var taxFilling = $('.taxBracket').find('input[name="data[23][answer][Estimated taxable income]"]').val();

        $.ajax({
            type: 'GET',
            url: ajaxUrl,
            data: {
                taxableIncome: taxFilling
            },
//            dataType: 'json',
            success: function (resp) {
                $('.tax-current-value').val(resp);
            },
            error: function (error) {
//                    twpmApp.ajaxInputError(error, $("#example-advanced-form"));
            }
        });

    });
});
$("#services-question-form-p-1").append('<p id="return-later">Save and return later</p>');
