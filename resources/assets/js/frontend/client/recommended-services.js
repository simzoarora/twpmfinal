$(document).ready(function () {




    if (typeof userFirstTime != 'undefined' && userFirstTime == true) {
        if (localStorage.getItem('popState') != 'shown') {
            $('#recommended-services-modal').modal('show');
            localStorage.setItem('popState', 'shown')
        }

    } else {
        $('#recommended-services-modal').modal('hide');
    }
    $(".service-options").owlCarousel({
        responsiveClass: true,
        navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            767: {
                items: 2,
                nav: false
            },
            991: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    });
    $('.products-list').on('click', function () {
        var $this = $(this);
        $this.addClass('selected').parent().siblings().find('.selected').removeClass('selected');
        $('.recommended-service').hide();
        var data_id = $this.attr('data-id');
        if($this.hasClass('dashboardDiv')){
        window.location.href = window.location.origin + "/recommendedServices/" + data_id;
        }
//        $('.rec-services .recommended-service[data-id=' + $this.attr('data-id') + ']').show();
    });
    $('.active-list').on('click', function () {
        var $this = $(this);
        $this.addClass('selected').parent().siblings().find('.selected').removeClass('selected');
        $('.recommended-service').hide();
        $('.active-services .recommended-service[data-id=' + $this.attr('data-id') + ']').show();
    });

    $(".your-services").hide();
//    $('.all-services .prcoducts-list').first().trigger('click');
    $("#your-services").click(function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $("#all-services").removeClass('active');
        $(".your-services").show();
        $(".all-services").hide();
        $('.your-services .active-list').first().trigger('click');
    });
    $(document).on('click', '#all-services', function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $("#your-services").removeClass('active');
        $(".all-services").show();
        $(".your-services").hide();
        $('.all-services .products-list').first().trigger('click');
    });
    $('.service-all-docu').each(function (i, v) {
        var $this = $(this);
        if ($this.find('a').length == 0) {
            $this.parent().find('.no-doc-message').show();
        }
    });

});

if ($(".all-services .service-options .owl-stage .owl-item:nth-child(1)").hasClass('active')) {
    $('.owl-stage-outer').next('.owl-nav').find('.owl-prev').css('opacity', '0')
}
$(document).on('click', '.owl-next', function () {
    $('.owl-stage-outer').next('.owl-nav').find('.owl-prev').css('opacity', '1');
});
if ($(".all-services .service-options .owl-stage .owl-item:nth-child(n)").hasClass('active')) {
    $('.owl-stage-outer').next('.owl-nav').find('.owl-next').css('opacity', '0')
}
$(document).on('click', '.owl-prev', function () {
    $('.owl-stage-outer').next('.owl-nav').find('.owl-next').css('opacity', '1');
});
