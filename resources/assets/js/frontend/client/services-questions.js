var form;
$(document).ready(function () {

//    left-div padding 
    $('.fa-angle-right').find('.addNewModal');
    $('.addNewModal').on('click', function () {
        if ($('.left-div').find('.service-preview')) {
            $('.left-div').css("padding:0");
        }
    });
    $(".select-type").selectBoxIt();
    $(document).on('click', '.backBtn', function () {
        if ($(".modal-body .setup-content:visible").prev().length != 0) {
            $(".modal-body .setup-content:visible").prev().show().next().hide();
        } else {
            $(".modal-body .setup-content:visible").hide();
            $(".modal-body .setup-content:visible:last").show();
        }
        return false;
    });



    $.validator.addMethod(
            "regex",
            function (value, element) {
                var re = new RegExp($(element).data('validation'));
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );
//        $('.custom-validation').rules("add", {regex: '^(100|[1-9][0-9]?)$'}); 

//    $('.file-manager-link').filemanager('file');

    var jumpHappening = false;
    var jump = 0;
    form = $("#services-question-form");
    form.validate({
        errorPlacement: function errorPlacement(error, element) {
            element.closest('.inner-left').append(error.addClass('error-alert').text('This field is required.'));
        }
    });
    form.steps({
        headerTag: "h3",
        bodyTag: "fieldset",
        transitionEffect: "none",
        labels: {
            finish: "finish",
            next: "continue",
            previous: "back"
        },
        onStepChanged: function (event, currentIndex, previousIndex) {
//            var label = $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').attr('data-label');
//            if (label) {
//                $('a[href="#next"]').text(label);
//            } else {
//                $('a[href="#next"]').text('Continue');
//            }
//            if (currentIndex > previousIndex) {
//                var previousStepSection = $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections');
//
//                var callBackFunction = previousStepSection.data('callback');
//
//                if (callBackFunction) {
//                    window[callBackFunction]();
//                }
//
//                if (!jumpHappening) {
//                    jump = previousStepSection.data('jump');
//                    $(event.target).find('#services-question-form-p-' + (currentIndex - 1) + ' >  .sections').data('jump', 0);
//                }
//            }
            $("html, body").animate({scrollTop: 10}, 100);
//            if(currentIndex==6){
//                $('#services-question-form-p-5 input').prop("disabled","disabled");
////            }
//            if (jump > 0) {
//                jumpHappening = true;
//                $(event.target).find('#services-question-form-p-' + (currentIndex) + ' >  .sections').find('input, select').attr('disabled', true);
//                jump--;
//                if (jump == 0) {
//                    jumpHappening = false;
//                }
//                form.steps('next');
//            } else {
//                $(this).closest('.sections').data('jump', 0);
////                $("#services-question-form-p-4 input, #services-question-form-p-5 input, #services-question-form-p-6 input").attr('disabled', false);
////                $("#services-question-form-p-7 input").attr('disabled', false);
////                $("#services-question-form-p-9 input,#services-question-form-p-10 input").attr('disabled', false);
////                $("#services-question-form-p-12 input,#services-question-form-p-13 input").attr('disabled', false);
////                $("#services-question-form-p-15 input,#services-question-form-p-16 input").attr('disabled', false);
//            }
//            if (currentIndex == currentIndex) {
//                changeLabel();
//            }
//            if ((currentIndex + 1) == $('#services-question-form fieldset').length) {
//                lastStep();
//            }


        },
        onStepChanging: function (event, currentIndex, newIndex)
        {

            if (newIndex < currentIndex) {
                return true;

            } else {
//                return true;
                return form.valid();
            }

            $('table tbody').find('label').remove();
            if ($('.find-table-length tbody').find('tr').length === 0 && $('.table-confirmation:checked').val() == 'yes') {
                if ($(this).find('tbody').find('.error-alert').length <= 0)
                    $(this).find('tbody').append('<label style="width: 100% !important;"  class="error error-alert">This field is required.</label>');
                return false;
            } else {
                $('.find-table-length tbody').find('label').remove();
                $('.find-table-length tbody').remove('error error-alert');
                return true;
            }

        },
        onFinishing: function (event, currentIndex)
        {
            $('table tbody').find('label').remove();
            if ($('.find-table-length tbody').find('tr').length === 0 && $('.table-confirmation:checked').val() == 'yes') {
                if ($(this).find('tbody').find('.error-alert').length <= 0)
                    $(this).find('tbody').append('<label style="width: 100% !important;"  class="error error-alert">This field is required.</label>');
                return false;
            } else {
                $('.find-table-length tbody').find('label').remove();
                $('.find-table-length tbody').remove('error error-alert');
                return true;
            }
            return form.valid();
        },
        onFinished: function (event, currentIndex)
        {
            form.submit();
        }
    });



//    $('.other-non-rental').on('change', function () {
//        if ($(this).val() == 'yes') {
//
//            var benFormMainTemplate = $('#new-data-entry-template').html();
//            benFormMainTemplate = _.template(benFormMainTemplate);
//            var benFromTemplateData = {
//                count: $('.new-data-entry-row').length
//            };
//            $('#otherNonRentalForm').append(benFormMainTemplate(benFromTemplateData));
//        } else {
//            $('#otherNonRentalForm').html('');
//        }
//        e.preventDefault();
//    });
    $('.other-non-rental').on('change', function () {
        if ($(this).val() == 'yes') {

            $('#otherNonRentalForm').html('');
        }
    });



    $(".actions").addClass('col-xs-offset-0 col-sm-offset-5 col-md-offset-5 col-lg-offset-5');
    $(".returnLater").addClass('col-xs-offset-0  col-lg-offset-0');



    $('a[href="#finish"]').css({'background': '#ffffff', 'color': '#1a88c8'}).append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    if ($("#services-question-form-p-0").is(':visible')) {
        $('a[href="#previous"]').parent('li').css('display', 'inline').attr('aria-disabled', false);
    }
//    $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    $('a[href="#next"]').css({'background': '#ffffff', 'color': '#1a88c8'});
    $('a[href="#previous"]').addClass('prev-btn-link pull-left').append('<i style="background:#fff; font-size:15px; padding-right:6px; margin-top:6px;" class="fa fa-arrow-left pull-left"></i>');
    $('a[href="#next"]').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');


});


function addRemoveHref() {

    var cind = form.steps('getCurrentIndex');
    if (cind == 0) {
        $('a[href="#previous"]').attr('onclick', "window.location.href='" + backUrl + "'");
    } else {
        $('a[href="#previous"]').removeAttr('onclick');
    }
}
function deleteRow(index_id) {

    swal({
        title: "Are you sure?",
        text: "Please click on finish for final submission.",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
            .then((willDelete) => {
                if (willDelete) {

//                        $("[data-index=" + index_id + "]").remove();

                    swal("Your information has been deleted!", {
                        icon: "success",
                    });
                } else {
                    swal("Your information is safe!");
                }
            });

}

  