$(document).ready(function () {
    $(".payment-form").submit(function (event) {
        var $this = $(this);
        $this.find('.add-card').attr('disabled', true);
        event.preventDefault();
        // do any validation checks or other actions here
        Stripe.createToken({
            name: $('.booking-name').val(),
            number: $('.number_on_card').val(),
            cvc: $('.cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val(),
            address_zip: $('.booking-billing_zip_code').val()

        }, stripeResponseHandler);
        return false;
    });
    function stripeResponseHandler(status, response) {
        if (response.error) {
            $(".payment-form .response").addClass('alert alert-danger').html(response.error.message);
            $(".payment-form").find('.add-card').attr('disabled', false);
        } else {
            //if form has no errors and Stripe has verified it too, go ahead with making payment
            formSubmission(response);
        }
    }
    function formSubmission(response) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(".payment-form").attr('action'),
            data: {stripe_token: response.id},
            dataType: 'json',
            success: function (resp) {
                $('#existingCards').append('<li data-id=' + response.card.id + '>xxxx xxxx xxxx ' + response.card.last4 + '<i class="fa fa-trash-o" aria-hidden="true"></i></li>');
                if ($('#existingCards li').length == 1) {
                    $('#existingCards li').first().addClass('active-card');
                }
                $('#add-card-modal').modal('hide');
                //reset form
                var paymentSel = $(".payment-form");
                paymentSel.find('.add-card').attr('disabled', false);
                paymentSel.find('.form-control').val('');
            },
            error: function (error) {
//                add error code
                $(".payment-form").find('.add-card').attr('disabled', false);
            }
        });
    }

    $('#existingCards').on('click', 'li', function () {
        $(this).addClass('active-card').siblings().removeClass('active-card');
    });

    $('#payNowBtn').on('click', function () {
        var activeCard = $('.active-card');
        if (user_confirmed != 1) {
            swal({
                title: 'Oops!!',
                text: 'Kindly Confirm your Account in order to Subscribe to Service.',
                type: 'error',
                customClass: ''
            });
            return;
        }
        if (!activeCard.length) {
            //no selected card
            swal({
                title: 'Oops!!',
                text: 'Please select a card.',
                type: 'error',
                customClass: ''
            });
            return;
        }
        $(this).attr('disabled', true);
        const button = document.querySelector('.submit-button'),
                stateMsg = document.querySelector('.pre-state-msg');

        button.classList.add('state-1', 'animated');
        button.classList.remove('state-0', 'animated');
        button.classList.remove('hide');


        const setInitialButtonState = function () {
            button.classList.remove('state-1', 'state-2', 'animated');
        };

        $.ajax({
            type: 'POST',
            url: servicePaymentLink,
            data: {
                card_id: activeCard.attr('data-id')
            },
            dataType: 'json',

            success: function (resp) {
                button.classList.add('state-2');
                button.classList.remove('state-1');

//            setTimeout(setInitialButtonState, 4000);

                swal({
                    title: 'Yay!!',
                    text: resp.message,
                    type: 'success',
                    customClass: '',
                    confirmButtonText: "Return to My Services"
                });
                $(this).hide();
                setTimeout(function () {
                    location.href = resp.link;
                }, 4000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                swal({
                    title: 'Oops!!',
                    text: error.message,
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });

    $(document).on('click', '#existingCards li .fa-trash-o', function () {
        var $this = $(this);
        $.ajax({
            type: 'POST',
            url: cardRemoveLink,
            data: {
                card_id: $this.parent().attr('data-id')
            },
            dataType: 'json',
            success: function (resp) {
                $this.parent().remove();
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                swal({
                    title: 'Oops!!',
                    text: error.message,
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });
});