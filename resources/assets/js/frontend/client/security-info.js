/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var form = $("#security-form");
form.validate({
    errorPlacement: function errorPlacement(error, element) {
        if (element.closest('ul').length) {
            element.closest('ul').after(error.addClass('alert alert-danger').text('Please answer this question'));
        } else {
            element.after(error.addClass('alert alert-danger').text('Please answer this question'));
        }
    },
    rules: {
        input: {
            required: true,
            min: 1
        }
    }
});