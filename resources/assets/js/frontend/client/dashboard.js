$(document).ready(function () {
        setTimeout(function () {
        $('.alert-danger, .alert-success').fadeOut('fast');
    }, 5000);
    $.ajaxSetup({ 
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    
//    var nData = JSON.parse(localStorage.getItem('notification-data')).notificationRoom;
//    var notificationRoomId = nData.id;
//    var senderId = nData.user_ids.split(',')[0];
//    var receiverId = nData.user_ids.split(',')[1];
    var userIdrCur = $("#auth_user_id").val();
    var connectionId = $("#notification_id").val();
//    var data = [];
//    if (connectionId) {
//        data = connectionId.split(',');
//    }
//    if(userIdrCur==parseInt(receiverId)){
//        alert('hie');
//    }
//    var socket = io.connect('http://client.twpmdemo.us:8890');
//
//    $.each(data, function (key, val) {
//        socket.emit('adduser', {'client': userIdrCur, 'notification': parseInt(val)});
//    });
//
//    socket.on('message', function (data) {
//        toastr.success(data.msg);
//        $('#unreadNotifications').prepend('<li  class="active"><p>' + data.msg + '</p><span>Now</span></li>');
//        $('#unreadNotifications').prepend('<li  class="active"><p>' + data.msg + '</p><span>Now</span></li>');
//        $('.noti-red').removeClass('hide');
//    });
//    getNotifications();

    //Profile nav links show/hide
    $('.profile-nav a').on('click', function (e) {
        var $this = $(this),
                href = $this.attr('href'),
                hrefNew = href.replace('#', '');

        $('.profile-lists').hide();
        $('.' + hrefNew).show();

        $this.parent().siblings().removeClass('active');
        $this.parent().addClass('active');

        e.preventDefault();
    });

//    // Show/Hide Notification pane
//    $("#noti-open , .out").click(function ()
//    {
//        getNotifications();
//        $("#notificationContainer").fadeToggle(300);
//        var formData = new FormData();
//        var userId = $("#auth_user_id").val();
//        formData.append('userId', userId);
//
//        $.ajax({
//            type: 'post',
//            url: '/isReadTrue',
//            data: formData,
//            processData: false, // tell jQuery not to process the data
//            contentType: false,
//            error: function () {
//            },
//            success: function (response) {
//                $('.noti-red').addClass('hide');
//            }
//        });
//        return false;
//    });
//
//    //Document Click
//    $(document).click(function ()
//    {
//        $("#notificationContainer").hide();
//    });
//    //Popup Click
//    $("#notificationContainer").click(function ()
//    {
//        return false;
//    })
//    $('#notificationsBody a.notification-type').on('click', function () {
//        var $this = $(this);
//        $this.addClass('active');
//        $this.siblings().removeClass('active');
//        var href = $this.attr('href');
//        $('.notifications-list').hide(500);
//        $(href).show(500);
//    });

    //Input add class  
    $(document).on('keyup change', 'input.input__field', function () {
        var $this = $(this);
        if ($this.val().trim() !== '') {
            $this.parent().addClass('input--filled');
        } else {
            $this.parent().removeClass('input--filled');
        }
    });
    $('input.input__field').each(function (i, v) {
        var $this = $(this);
        if ($this.val().trim() !== '') {
            $this.parent().addClass('input--filled');
        } else {
            $this.parent().removeClass('input--filled');
        }
    });
    //END
    //header search bar on focus
    $('#header-search+i').click(function () {
        $('#header-search').focus();
    });
    //END

    $('.docusign-send').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $docuSignMessage = $this.parent().find('.docu-sign-message');
        $docuSignMessage.html('Sending Digital Signature Request..');

        $.ajax({
            url: $this.data('url'),
            dataType: 'json',
            type: 'get',
            success: function (resp) {
                $docuSignMessage.html(resp);
            },
            error: function (err) {
                $docuSignMessage.html(err.responseJSON);
            }
        });
    });

    function getNotifications() {
        var formData = new FormData();
        var userId = $("#auth_user_id").val();
        formData.append('id', userId);
        $.ajax({
            type: 'get',
            url: '/notification',
            data: {'userId': userId},
            error: function () {
            },
            success: function (response) {

                $('#allNotifications').html('');
                $('#unreadNotifications').html('');
                $.each(response.data, function (key, val) {
                    var activeClass = '';
                    if (val.is_read == 0) {
                        $('.noti-red').removeClass('hide');
                        activeClass = 'active'
                    }
                    $('#allNotifications').append('<li  class="' + activeClass + '" data-id="' + val.id + '" ><p>' + val.text + '</p><span>' + val.created_at + '</span></li>');
                });
                $.each(response.unReadData, function (key, val) {
                    if (val.is_read == 0) {
                        $('.noti-red').removeClass('hide');
                        activeClass = 'active'
                    }
                    $('#unreadNotifications').append('<li  class="' + activeClass + '" data-id="' + val.id + '" ><p>' + val.text + '</p><span>' + val.created_at + '</span></li>');
                });
//               
            }
        });
    }
});

