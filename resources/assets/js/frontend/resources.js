$(document).ready(function () {
    $('#resources-outer').on('click', '.resources-box', function () {
        var $this = $(this);
        $this.parent().addClass('service-open-slide');
        $('#categoryName').text($this.attr('data-categoryname'));
//        $('body').animate({scrollTop: 0}, 500);
        $.ajax({
            url: $this.attr('data-url'),
            success: function (data) {
                var appendlocation = $('#append-resources-topics'),
                        topics_template = $('#all-resources-topics-html').html();
                topics_template = _.template(topics_template);
                var topics = {
                    topics: data
                };
                appendlocation.empty().append(topics_template(topics));
                alignTitles();
                $('.hide-content,#footer').hide();
                $("html, body").animate({scrollTop: 0}, 500);
                $('.home-navbar').addClass('blue-nav2');
//                setTimeout(function () {
//                    $('.home-navbar').addClass('blue-nav');
//                }, 100)
                $('#all-resources').animate({'marginLeft': '-16.666666%', opacity: 0.3}, 700, 'easeInOutCubic', function () {
                    $(this).css('height', '0px');
                });
                $('#all-resources-topics').animate({'marginLeft': '-16.666666%', opacity: 1}, 700, 'easeInOutCubic');
                $this.parent().removeClass('service-open-slide');
                return false;
            },
            error: function () {
                swal({
                    title: 'Oops!!',
                    text: 'Something went wrong. Please try again.',
                    type: 'error',
                    customClass: ''
                });
            }
        });
    });
    $('#resources-outer').on('click', '.resources-topics', function () {
        var $this = $(this);
        $this.parent().addClass('service-open-slide');
        $('body').animate({scrollTop: 0}, 500);
        $('#single-resources-topic .single-topic-content').html($this.find('.full-content').html()).promise().done(function () {
            $this.parent().removeClass('service-open-slide');
        });
        $('#all-resources').animate({'marginLeft': '-33.333333%', opacity: 0.3}, 700, 'easeInOutCubic', function () {
            $(this).css('height', '0px');
        });
        $('#all-resources-topics').animate({'marginLeft': '-16.666666%', opacity: 0.3}, 700, 'easeInOutCubic', function () {
            $(this).css('height', '0px');
        });
        $('#single-resources-topic').animate({'marginLeft': '-16.666666%', opacity: 1}, 700, 'easeInOutCubic');
    });
    $('#resources-outer').on('click', '.back-resources', function () {
        $('#all-resources,#all-resources-topics,#single-resources-topic').css('height', 'auto').animate({'marginLeft': '0%', opacity: 1}, 700, 'easeInOutCubic', function () {
            $('#append-resources-topics,#single-resources-topic .single-topic-content').html('');
        });
        $('.hide-content,#footer').show();
    });
    $('#resources-outer').on('click', '.back-categories', function () {
        $('#all-resources-topics,#single-resources-topic').css('height', 'auto').animate({'marginLeft': '0%', opacity: 1}, 700, 'easeInOutCubic', function () {
            $('#single-resources-topic .single-topic-content').html('');
        });
    });

    function alignTitles() {
        $('.resources-img h3').each(function () {
            var top = (($(this).closest('.resources-img').height()) / 2) - (($(this).height()) / 2);
            $(this).css('top', top);
        });
    }

    alignTitles();
});