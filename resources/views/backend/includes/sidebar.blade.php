<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/home') }}">
                <a href="{{ route('admin.home') }}">
                    <i class="fa fa-home"></i>
                    <span>{{ trans('menus.backend.home.title') }}</span>
                </a>
            </li>

            <li class="{{ Active::pattern('admin/about*') }} treeview">
                <a href="#">
                    <i class="fa fa-user"></i>
                    <span>About</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/employee*', 'menu-open') }}{{ Active::pattern('admin/about*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/employee*', 'display: block;') }}{{ Active::pattern('admin/about*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/about') }}">
                        <a href="{{ route('admin.about.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Who We Are</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/employee') }}">
                        <a href="{{ route('admin.employee.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Employees</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ Active::pattern('admin/services*') }} treeview">
                <a href="#">
                    <i class="fa fa-reorder"></i>
                    <span>Services</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/services*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/services*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/services/data') }}">
                        <a href="{{ route('admin.service.data') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Add Service data</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/services') }}">
                        <a href="{{ route('admin.services') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Services</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/services/categories') }}">
                        <a href="{{ route('admin.services.categories') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ Active::pattern('admin/resources*') }} treeview">
                <a href="#">
                    <i class="fa fa-gears"></i>
                    <span>Resources</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/resources*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/resources*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/resources/create') }}">
                        <a href="{{ route('admin.resources.create') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Create Resource</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/resources/topics') }}">
                        <a href="{{ route('admin.resources.topics') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Topics</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/resources/categories') }}">
                        <a href="{{ route('admin.resources.categories') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ Active::pattern('admin/blogs') }}">
                <a href="#">
                    <i class="fa fa-commenting"></i>
                    <span>Blogs</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/blogs*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/blogs*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/blogs/articles') }}">
                        <a href="{{ route('admin.blogs.articles') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Articles</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/blogs/categories') }}">
                        <a href="{{ route('admin.blogs.categories') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Categories</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="{{ Active::pattern('admin/footer') }}">
                <a href="{{ route('admin.footer') }}">
                    <i class="fa fa-file-o"></i>
                    <span>Footer/Navbar</span>
                </a>
            </li>
            <li class="hidden {{ Active::pattern('admin/seo') }}">
                <a href="{{ route('admin.seo') }}">
                    <i class="fa fa-area-chart"></i>
                    <span>SEO</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/contact') }}">
                <a href="{{ route('admin.contact') }}">
                    <i class="fa fa-phone"></i>
                    <span>Contact</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/legalDisclosures') }}">
                <a href="{{ route('admin.legalDisclosures') }}">
                    <i class="fa fa-legal"></i>
                    <span>Legal Disclosures</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/privacy') }}">
                <a href="{{ route('admin.privacy') }}">
                    <i class="fa fa-lock"></i>
                    <span>Privacy & Security</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/terms') }}">
                <a href="{{ route('admin.terms') }}">
                    <i class="fa fa-asterisk"></i>
                    <span>Terms of Use</span>
                </a>
            </li>
            <li class="{{ Active::pattern('admin/uploader') }} hidden">
                <a href="{{ route('admin.uploader') }}">
                    <i class="fa fa-asterisk"></i>
                    <span>Uploader</span>
                </a>
            </li>
            @permissions(['manage-users', 'manage-roles'])
            <li class="{{ Active::pattern('admin/access/*') }} treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu {{ Active::pattern('admin/access/*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/access/*', 'display: block;') }}">
                    @permission('manage-users')
                    <li class="{{ Active::pattern('admin/access/user*') }}">
                        <a href="{{ route('admin.access.user.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.users.management') }}</span>
                        </a>
                    </li>
                    @endauth

                    @permission('manage-roles')
                    <li class="{{ Active::pattern('admin/access/role*') }}">
                        <a href="{{ route('admin.access.role.index') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('labels.backend.access.roles.management') }}</span>
                        </a>
                    </li>
                    @endauth
                    @permission('manage-users')
                    <li class="{{ Active::pattern('admin/assignClients*') }}">
                        <a href="{{ route('admin.assignClients') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Assign Clients To Employee</span>
                        </a>
                    </li>
                    <li class="{{ Active::pattern('admin/allclientEmployeeList*') }}">
                        <a href="{{ route('admin.allclientEmployeeList') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>Clients To Employee List</span>
                        </a>
                    </li>
                    @endauth
                </ul>
            </li>
            @endauth
            <li></li>
            <li class="header">{{-- trans('menus.backend.sidebar.system') --}}</li>

            <!--<li class="{{ Active::pattern('admin/log-viewer*') }} treeview">-->
<!--                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>-->
                <!--<ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">-->
<!--                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{{ route('admin.log-viewer::dashboard') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>
                        </a>
                    </li>-->

<!--                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{{ route('admin.log-viewer::logs.list') }}">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.log-viewer.logs') }}</span>
                        </a>
                    </li>-->
                <!--</ul>-->
            <!--</li>-->
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>
