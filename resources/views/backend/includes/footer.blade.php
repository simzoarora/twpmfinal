<!-- Main Footer -->
<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{ date('Y') }} {{ app_name() }}.</strong> {{ trans('strings.backend.general.all_rights_reserved') }}
</footer>