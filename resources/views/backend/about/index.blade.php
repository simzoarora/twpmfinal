@extends('backend.layouts.master')

@section ('title', 'About' )

@section('page-header')
<h1>
    {{ app_name() }}
    <small>About</small>
</h1>
@endsection
@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
<style>
    .dropzone:after {
        bottom: 3%;
    }
    .deleteLogoBox {
        cursor: pointer;
    }
    .dropzone .tools {
        width: 30px;
        top: 0px;
        right: -30px;
    }
</style>
@stop

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(Session::has('updateMessage'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <span class="icon icon-info-circle icon-lg"></span>
            <small>{!! session('updateMessage') !!}</small>
        </div>
        @elseif(Session::has('saveMessage'))
        <div class="alert alert-success">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <span class="icon icon-info-circle icon-lg"></span>
            <small>{!! session('saveMessage') !!}</small>
        </div>
        @endif
    </div>
</div>

{{ Form::model($aboutPageContent,['id'=>'home_page_form','route'=>'admin.about.store','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        <h3 class="box-title">Slide 1</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('slide_1_heading')) echo ' has-error'; ?>">
            {{ Form::label('slide_1_heading','Heading', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('slide_1_heading', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Slide 1 Heading']) }}
                <span class="help-block">{{ $errors->first('slide_1_heading') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('slide_1_description')) echo ' has-error'; ?>">
            {{ Form::label('slide_1_description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('slide_1_description', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.sub_heading'),'rows'=>'4']) }}
                <span class="help-block">{{ $errors->first('slide_1_description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('slide_1_image')) echo ' has-error'; ?>">
            {{ Form::label('slide_1_image','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($aboutPageContent)
                        <input type="file" name="slide_1_image" />
                        @else
                        <input type="file" name="slide_1_image" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('slide_1_image') }}</span>
                    </div>
                    @if($aboutPageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/aboutpage/'.$aboutPageContent['slide_1_image'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-success"><!-- Slide-2 -->
    <div class="box-header with-border">
        <h3 class="box-title">Slide 2</h3>
    </div>
    <hr>
    <div class="form-group <?php if ($errors->first('slide_2_heading')) echo ' has-error'; ?>">
        {{ Form::label('slide_2_heading','Heading', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('slide_2_heading', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Slide 2 Heading']) }}
            <span class="help-block">{{ $errors->first('slide_2_heading') }}</span>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('slide_2_description')) echo ' has-error'; ?>">
        {{ Form::label('slide_2_description','Description', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('slide_2_description', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.sub_heading'),'rows'=>'4']) }}
            <span class="help-block">{{ $errors->first('slide_2_description') }}</span>
        </div>
    </div>
    <hr>
</div>
<div class="box box-success"><!-- Slide-3 -->
    <div class="box-header with-border">
        <h3 class="box-title">Slide 3</h3>
    </div>
    <div class="form-group <?php if ($errors->first('slide_3_heading')) echo ' has-error'; ?>">
        {{ Form::label('slide_3_heading','Heading', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('slide_3_heading', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Slide 3 Heading']) }}
            <span class="help-block">{{ $errors->first('slide_3_heading') }}</span>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('slide_3_description')) echo ' has-error'; ?>">
        {{ Form::label('slide_3_description','Description', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('slide_3_description', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Slide 3 Description']) }}
            <span class="help-block">{{ $errors->first('slide_3_description') }}</span>
        </div>
    </div>
    <hr>
</div>
<div class="box box-success"><!-- Slide-5 -->
    <div class="box-header with-border">
        <h3 class="box-title">Slide 5</h3>
    </div>
    <div class="form-group <?php if ($errors->first('slide_5_heading')) echo ' has-error'; ?>">
        {{ Form::label('slide_5_heading','Heading', ['class' => 'col-lg-2 control-label']) }}
        <div class="col-lg-10">
            {{ Form::textarea('slide_5_heading', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Slide 5 Heading']) }}
            <span class="help-block">{{ $errors->first('slide_5_heading') }}</span>
        </div>
    </div>
    <div class="form-group <?php if ($errors->first('slide_5_description')) echo ' has-error'; ?>">
        {{ Form::label('slide_5_description','Description', ['class' => 'col-lg-2 control-label']) }} 
        <div class="col-lg-10">
            {{ Form::textarea('slide_5_description', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.sub_heading')]) }}
            <span class="help-block">{{ $errors->first('slide_5_description') }}</span>
        </div>
    </div>
</div>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Sub Footer</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
            {{ Form::label('sponsor_logo','Sponsor Logo', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10 file">
                <div class="dropzone" data-width="120" data-height="120" data-save="false" data-button-edit="true" data-image="<?php echo $aboutPageContent
        ? URL::asset('img/backend/aboutpage/'.$aboutPageContent->sponsor_logo) : ''; ?>">
                    <input type="file" name="sponsor_logo_file" class="form-control"/>
                </div>
                <input type="hidden" id="filename" name="sponsor_logo[name]" value="<?php echo $aboutPageContent
        ? $aboutPageContent->sponsor_logo : ''; ?>" />
                <input type="hidden" id="filebase64" name="sponsor_logo[data]" />
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('sponsor_link','Sponsor Link', ['class' => 'col-lg-2 control-label']) }} 
            <div class="col-lg-10">
                {{ Form::text('sponsor_link', null, ['class' => 'form-control', 'placeholder' => 'Sponsor Link']) }}
            </div>
        </div>
    </div>
</div>
<!-- About -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">SEO</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('seo_title')) echo ' has-error'; ?>">
            {{ Form::label('seo_title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('seo_title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('seo_title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('seo_description')) echo ' has-error'; ?>">
            {{ Form::label('seo_description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('seo_description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('seo_description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('seo_img')) echo ' has-error'; ?>">
            {{ Form::label('seo_img','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($aboutPageContent)
                        <input type="file" name="seo_img" />
                        @else
                        <input type="file" name="seo_img" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('seo_img') }}</span>
                    </div>
                    @if($aboutPageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/aboutpage/'.$aboutPageContent['seo_img'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><!--box box-success-->
<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.dashboard', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Submit', ['class' => 'btn btn-success']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection
@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
    $(document).ready(function () {
        $('.dropzone').html5imageupload({
            onSave: function (data) {
                $('#filename').val(data.name);
                $('#filebase64').val(data.data);
            }
        });
    });
</script>
@endsection