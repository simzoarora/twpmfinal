@extends('backend.layouts.master')
@if(isset($tag))
@section ('title', 'Edit Profile Type Tag' )
@else
@section ('title', 'Create Profile Type Tag' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($tag))
    <small>Edit Profile Type Tag</small>
    @else
    <small>Create Profile Type Tag</small>
    @endif
</h1>
@endsection


@section('content')

@if(isset($tag))
{{ Form::model($tag,['route' =>['admin.profiletags.update',$tag->id],'class' => 'form-horizontal', 'role' => 'form', 'method' => 'PATCH']) }}
{{ Form::hidden('id',$tag->id) }}
@else
{{ Form::open(['route' =>'admin.profiletags.store','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
@endif
<div class="box box-success">
    <div class="box-header with-border">
        @if(isset($question))
        <h3 class="box-title">Edit Profile Type Tag</h3>
        @else
        <h3 class="box-title">Create Profile Type Tag</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            {{ Form::label('name','Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('name', null, ['class' => 'form-control', 'required','placeholder' => 'Write Profile type here']) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>

        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{{ route('admin.profiletags.index') }}" class="btn btn-danger">Cancel</a> 
                </div><!--pull-left-->

                <div class="pull-right">
                    @if(isset($question))
                    {{ Form::submit('Update Profile Tag', ['class' => 'btn btn-success submit_form']) }}
                    @else
                    {{ Form::submit('Save Profile Tag', ['class' => 'btn btn-success submit_form']) }}
                    @endif
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    </div>
</div>

{{ Form::close() }}
@endsection

@section('after-scripts')
@endsection