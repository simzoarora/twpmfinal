@extends ('backend.layouts.app')

@section ('title', 'Profile Type Tags')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Profile Type Tags</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Profile Type Tags</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.profiletags.create')}}"><span class="btn btn-primary btn-xs">Add New Profile Type Tags</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="service_categories_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Name</th>
                        <th class="w10">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($tags as $key => $tag)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th>{{ $tag->name }}</th>
                        <th>
                            <a href="{{route('admin.profiletags.edit',$tag->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <span data-target="#deleteModal" data-toggle="modal" data-id="{{$tag->id}}" class="btn btn-xs btn-danger deleteServiceCategory"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </th>
                    </tr>
                    @empty
                    No Profile Type Tags.
                    @endforelse
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteServiceCategoryForm']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Profile Type Tag</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Profile Type Tag?
                {{ Form::hidden('id',null,['id' => 'hiddenServiceCategoryId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('#service_categories_table').on('click', '.deleteServiceCategory', function () {
            $('#hiddenServiceCategoryId').val($(this).data('id'));
        });

        $('#deleteServiceCategoryForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("admin.profiletags.delete")}}',
                type: 'post',
                data: {
                    id: $('#hiddenServiceCategoryId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("admin.profiletags.index")}}');
                },
                error: function (err) {
                }
            });
        });

        $('#service_categories_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop
