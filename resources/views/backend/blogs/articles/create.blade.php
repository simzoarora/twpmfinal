@extends('backend.layouts.master')
@if(isset($article))
@section ('title', 'Edit Article' )
@else
@section ('title', 'Create Article' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($article))
    <small>Edit Article</small>
    @else
    <small>Create Article</small>
    @endif
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/select2/select2.min.css") }}
{{ Html::style("css/backend/plugin/datepicker/datepicker3.css") }}
<style>
    .dropzone:after {
        bottom: 3%;
    }
</style>
@stop

@section('content')
@if(isset($article))
{{ Form::model($article,['id'=>'articles_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$article->id) }}
@else
{{ Form::open(['id'=>'articles_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        @if(isset($article))
        <h3 class="box-title">Edit Article</h3>
        @else
        <h3 class="box-title">Create Article</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title', 'maxlength'=>'120']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('slug')) echo ' has-error'; ?>">
            {{ Form::label('slug','Slug', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                @if(isset($article))
                {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug','readonly'=>'readonly']) }}
                @else
                {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Slug']) }}
                @endif
                <span class="help-block">{{ $errors->first('slug') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('author')) echo ' has-error'; ?>">
            {{ Form::label('author','Author', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('author', $users, null, ['class' => 'form-control', 'placeholder' => 'Choose Author']) }}
                <span class="help-block">{{ $errors->first('author') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('blog_category_id')) echo ' has-error'; ?>">
            {{ Form::label('blog_category_id[]','Category', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                @if(isset($article))
                <select name="blog_category_id[]" class="select2 form-control" multiple="multiple">
                    @foreach($categories as $categoryId => $category)
                    <option value="{{$categoryId}}" <?php echo (in_array($categoryId, $article->blogCategories->pluck('id')->toArray())) ? 'selected' : ''; ?>>{{$category}}</option>
                    @endforeach
                </select>
                @else
                {{Form::select('blog_category_id[]', $categories , null ,['class'=>'select2 form-control','multiple'=>'multiple'])}}
                @endif
                <span class="help-block">{{ $errors->first('blog_category_id') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('image')) echo ' has-error'; ?>">
            {{ Form::label('image','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        <input type="file" name="image" />
                        <span class="help-block">{{ $errors->first('image') }}</span>
                    </div>
                    @if(isset($article))
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/blogs/articles/'.$article['image'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('excerpt')) echo ' has-error'; ?>">
            {{ Form::label('excerpt','Excerpt', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('excerpt', null, ['class' => 'form-control', 'placeholder' => 'Excerpt']) }}
                <span class="help-block">{{ $errors->first('excerpt') }}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">
                Upload PDF
            </label>
            <div class="col-lg-10">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="filepath">
                    <img class="hidden" id="holder" style="margin-top:15px;max-height:100px;">
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('body')) echo ' has-error'; ?>">
            {{ Form::label('body','Body', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('body', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Body']) }}
                <span class="help-block">{{ $errors->first('body') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('status')) echo ' has-error'; ?>">
            {{ Form::label('status','Publish', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                @if(isset($article) && $article->status)
                {{ Form::checkbox('status', config('constant.backend.blogs.active') , true) }}
                @else
                {{ Form::checkbox('status', config('constant.backend.blogs.active') ) }}
                @endif
                <span class="help-block">{{ $errors->first('status') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('publish_at')) echo ' has-error'; ?>">
            {{ Form::label('publish_at','Published On', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {{ Form::text('publish_at', null, ['class' => 'form-control pull-right', 'id' => 'datepicker']) }}
                </div>
                <span class="help-block">{{ $errors->first('publish_at') }}</span>
            </div>
        </div>

    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.blogs.articles', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($article))
            {{ Form::submit('Update Article', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Article', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection 

@section('after-scripts')
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
{{ Html::script("js/backend/plugin/select2/select2.min.js") }}
{{ Html::script("js/backend/plugin/datepicker/bootstrap-datepicker.js") }}
{{ Html::script("/vendor/laravel-filemanager/js/lfm.js") }}
<script>
    $(function () {
        $('.select2').select2({
            multiple: true
        });
        $('#title').on('keyup', function () {
            $.ajax({
                url: '{{route("admin.blogs.articles.generateSlug")}}',
                type: 'post',
                data: {
                    title: $('#title').val()
                },
                success: function (response)
                {
                    $('#slug').val(response.slug);
                },
                error: function (err) {
                }
            });
        });
        $('#slug').on('keyup', function () {
            var slug_form_group = $('#slug').closest('.form-group');
            if ($('#slug').val() !== '') {
                $.ajax({
                    url: '{{route("admin.blogs.articles.checkSlugDuplicacy")}}',
                    type: 'post',
                    data: {
                        slug: $('#slug').val()
                    },
                    success: function (response)
                    {
                        if (response.status == 'duplicate') {
                            slug_form_group.addClass('has-warning');
                            $('.submit_form').prop('disabled', true);
                        } else if (response.status == 'unique') {
                            slug_form_group.removeClass('has-warning').addClass('has-success');
                            $('.submit_form').prop('disabled', false);
                        }
                        slug_form_group.find('.help-block').text(response.message);
                    },
                    error: function (err) {
                    }
                });
            } else {
                slug_form_group.find('.help-block').text('');
                if (slug_form_group.hasClass('has-warning')) {
                    slug_form_group.removeClass('has-warning');
                }
                if (slug_form_group.hasClass('has-success')) {
                    slug_form_group.removeClass('has-success');
                }
            }
        });

        $('#datepicker').datepicker({
            todayHighlight: true,
            format: "yyyy-mm-dd",
            autoclose: true
        });

        $('#lfm').filemanager('Files');
    });
    
    function SetUrl(url) {
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        target_input.val(url);
        var filename = url.split('/').pop();
        $('.cke_button__link_icon').click();
        setTimeout(function(){ 
            $('#cke_86_textInput').val(url);
        }, 700);

        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
    }
</script>
@endsection