@extends ('backend.layouts.app')

@section ('title', 'Articles')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
    .w80 {
        width: 80px;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Articles</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Articles</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.blogs.articles.create')}}"><span class="btn btn-primary btn-xs">Add New Article</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="articles_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Title</th>
                        <th class="w80">Publish On</th>
                        <th class="w10">Published</th>
                        <th class="w10">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($articles as $key => $article)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th><?php echo (strlen($article->title) > 75) ? substr(strip_tags($article->title), 0, 75).'...' : $article->title ;?></th>
                        <th>{{$article->publish_at}}</th>
                        <th>
                            @if($article->status)
                            <small class="label pull-right bg-green">Yes</small>
                            @else
                            <small class="label pull-right bg-gray">No</small>
                            @endif
                        </th>
                        <th>
                            <a href="{{route('admin.blogs.articles.edit',$article->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <span data-target="#deleteModal" data-toggle="modal" data-id="{{$article->id}}" class="btn btn-xs btn-danger deleteArticle"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteArticleForm']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Article</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Article?
                {{ Form::hidden('id',null,['id' => 'hiddenArticleId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop 

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('#articles_table').on('click', '.deleteArticle', function () {
            $('#hiddenArticleId').val($(this).data('id'));
        });

        $('#deleteArticleForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("admin.blogs.articles.delete")}}',
                type: 'post',
                data: {
                    id: $('#hiddenArticleId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("admin.blogs.articles")}}');
                },
                error: function (err) {
                }
            });
        });

        $('#articles_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop
