@extends('backend.layouts.master')

@section ('title', trans('strings.backend.home.title') )

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Resource </small>
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
@stop
@section('content')
{{ Form::model($data,['id'=>'resource_form','class' => 'form-horizontal','role' => 'form']) }}

<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('Slide 1') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('heading_1')) echo ' has-error'; ?>">
            {{ Form::label('heading_1',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('heading_1', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.heading'),'required'=>'required']) }}
                <span class="help-block">{{ $errors->first('heading_1') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('subheading_1')) echo ' has-error'; ?>">
            {{ Form::label('subheading_1',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('subheading_1', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description'),'required'=>'required']) }}
                <span class="help-block">{{ $errors->first('subheading_1') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-2 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title2') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('heading_2')) echo ' has-error'; ?>">
            {{ Form::label('heading_2',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('heading_2', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.heading'),'required'=>'required']) }}
                <span class="help-block">{{ $errors->first('heading_2') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('subheading_2')) echo ' has-error'; ?>">
            {{ Form::label('subheading_2',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('subheading_2', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description'),'required'=>'required']) }}
                <span class="help-block">{{ $errors->first('description_2') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.resources.create', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection