@extends('backend.layouts.master')
@if(isset($category))
@section ('title', 'Edit Resource Category' )
@else
@section ('title', 'Create Resource Category' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($category))
    <small>Edit Resource Category</small>
    @else
    <small>Create Resource Category</small>
    @endif
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
{{ Html::style("css/backend/plugin/colorpicker/bootstrap-colorpicker.min.css") }}
@stop

@section('content')
@if(isset($category))
{{ Form::model($category,['id'=>'resource_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$category->id) }}
@else
{{ Form::open(['id'=>'resource_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        @if(isset($category))
        <h3 class="box-title">Edit Resource Category</h3>
        @else
        <h3 class="box-title">Create Resource Category</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title','Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('title_bg_color')) echo ' has-error'; ?>">
            {{ Form::label('title_bg_color','Background Color', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-3">
                {{ Form::text('title_bg_color', null, ['class' => 'form-control colorpicker']) }}
                <span class="help-block">{{ $errors->first('title_bg_color') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('order')) echo ' has-error'; ?>">
            {{ Form::label('order','Order', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('order', null, ['class' => 'form-control', 'placeholder' => 'Order', 'required'=>true]) }}
                <span class="help-block">{{ $errors->first('order') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.resources.categories', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($category))
            {{ Form::submit('Update Resource Category', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Resource Category', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/colorpicker/bootstrap-colorpicker.min.js") }}
<script>
    $(function () {
        $('#order').on('keyup', function () {
            var order = $('#order').val(),
                    category_id = $('input[name="id"]').val();

            $.ajax({
                url: '{{route("admin.resources.categories.order.edit")}}',
                type: 'post',
                data: {
                    order: order,
                    category_id: category_id,
                    form: 1
                },
                success: function (response)
                {
                    $('#order').closest('.form-group').find('.help-block').text(response.message);
                    if (response.status == 0) {
                        $('.submit_form').prop('disabled', true);
                        $('#order').closest('.form-group').addClass('has-error');
                    } else {
                        $('.submit_form').prop('disabled', false);
                        $('#order').closest('.form-group').removeClass('has-error');
                    }
                },
                error: function (err) {
                }
            });
        });
        $('.colorpicker').colorpicker();
    });
</script>
@endsection