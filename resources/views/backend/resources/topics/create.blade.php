@extends('backend.layouts.master')
@if(isset($topic))
@section ('title', 'Edit Resource Topic' )
@else
@section ('title', 'Create Resource Topic' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($topic))
    <small>Edit Resource Topic</small>
    @else
    <small>Create Resource Topic</small>
    @endif
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
{{ Html::style("css/backend/plugin/select2/select2.min.css") }}
{{ Html::style("css/backend/plugin/colorpicker/bootstrap-colorpicker.min.css") }}
<style>
    .change_file {
        margin: 0 10px;
    }
    .file_link {
        cursor: pointer;
    }
    .upload_file_group {
        display: none;
    }
</style>
@stop

@section('content')
@if(isset($topic))
{{ Form::model($topic,['id'=>'resource_topics_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$topic->id) }}
@else
{{ Form::open(['id'=>'resource_topics_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        @if(isset($topic))
        <h3 class="box-title">Edit Resource Topic</h3>
        @else
        <h3 class="box-title">Create Resource Topic</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title','Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('title_bg_color')) echo ' has-error'; ?>">
            {{ Form::label('title_bg_color','Background Color', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-3">
                {{ Form::text('title_bg_color', null, ['class' => 'form-control colorpicker']) }}
                <span class="help-block">{{ $errors->first('title_bg_color') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('order')) echo ' has-error'; ?>">
            {{ Form::label('order','Order', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('order', null, ['class' => 'form-control', 'placeholder' => 'Order']) }}
                <span class="help-block">{{ $errors->first('order') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('is_client')) echo ' has-error'; ?>">
            {{ Form::label('is_client','Display Type', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <?php 
                $is_client0 = isset($topic)&&$topic['is_client']==0?true:false;
                $is_client1 = isset($topic)&&$topic['is_client']==1?true:false;
                ?>
                {{ Form::radio('is_client', 0, $is_client0) }} For all<br>
                {{ Form::radio('is_client', 1, $is_client1) }} For Clients
                <span class="help-block">{{ $errors->first('is_client') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('resource_category_id')) echo ' has-error'; ?>">
            {{ Form::label('resource_category_id','Category', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{Form::select('resource_category_id', $categories , null ,['class'=>'select2 form-control','placeholder' => 'Choose Category'])}}
                <span class="help-block">{{ $errors->first('resource_category_id') }}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">
                Upload PDF
            </label>
            <div class="col-lg-10">
                <div class="input-group">
                    <span class="input-group-btn">
                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
                            <i class="fa fa-picture-o"></i> Choose
                        </a>
                    </span>
                    <input id="thumbnail" class="form-control" type="text" name="filepath">
                    <img class="hidden" id="holder" style="margin-top:15px;max-height:100px;">
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('content')) echo ' has-error'; ?>">
            {{ Form::label('title','Content', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('content', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Content']) }}
                <span class="help-block">{{ $errors->first('content') }}</span> 
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Files</label>
            <div class="files col-lg-10" id="resource_topics">
                @if(isset($topic) && !($topic->resourceTopicFiles->isEmpty()))
                @foreach($topic->resourceTopicFiles as $key => $resourceTopicFile)
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">File {{($key+1)}}</h3>
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('file_text['.($key+1).']','File Text',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::text('file_text['.($key+1).']',$resourceTopicFile->file_text,['class' => 'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('file_link','File Link',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                <a class="file_link" href="{{URL::asset('img/backend/resources/file_uploads/'.$resourceTopicFile->upload_file)}}" target="_blank"><i class="fa fa-file-o fa-lg text-success"></i></a>
                                <span class="btn btn-success btn-xs change_file">Change File</span>
                            </div>
                        </div>
                        <div class="form-group upload_file_group">
                            {{Form::label('upload_file['.($key+1).']','Upload File',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::file('upload_file['.($key+1).']')}}
                            </div>
                            {{Form::hidden('upload_file_old['.($key+1).']',$resourceTopicFile->upload_file)}}
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">File 1</h3>
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('file_text[1]','File Text',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::text('file_text[1]',null,['class' => 'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('upload_file[1]','Upload File',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::file('upload_file[1]')}}
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="text-center">
                <span class="btn btn-primary" id="add_more_files">Add More Files</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.resources.topics', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($topic))
            {{ Form::submit('Update Resource Topic', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Resource Topic', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/select2/select2.min.js") }}
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
{{ Html::script("js/backend/plugin/colorpicker/bootstrap-colorpicker.min.js") }}
<script>
    var fileCounter = $('.files').children().length;

    $('#add_more_files').on('click', function (e) {
        e.preventDefault();
        fileCounter++;
        var fileToAppend = appendTemplate(fileCounter);
        $('.files').append(fileToAppend);
    });

    $('.change_file').on('click', function () {
        $(this).closest('.box').find('.upload_file_group').toggle();
    });

    function appendTemplate(counter) {
        return '<div class="box box-default box-solid">\n\
                    <div class="box-header with-border">\n\
                        <h3 class="box-title">File ' + counter + '</h3>\n\
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>\n\
                    </div>\n\
                    <div class="box-body">\n\
                        <div class="form-group">\n\
                            <label for="file_text[' + counter + ']" class="col-lg-2 control-label">File Text</label>\n\
                            <div class="col-lg-10">\n\
                                <input type="text" name="file_text[' + counter + ']" class="form-control"/>\n\
                            </div>\n\
                        </div>\n\
                        <div class="form-group">\n\
                            <label for="upload_file[' + counter + ']" class="col-lg-2 control-label">Upload File</label>\n\
                            <div class="col-lg-10">\n\
                                <input type="file" name="upload_file[' + counter + ']"/>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>';
    }

    $(function () {
        $(".select2").select2();

        $('#order').on('keyup', function () {
            var order = $('#order').val(),
                    topic_id = $('input[name="id"]').val();

            $.ajax({
                url: '{{route("admin.resources.topics.order.edit")}}',
                type: 'post',
                data: {
                    order: order,
                    topic_id: topic_id,
                    form: 1
                },
                success: function (response)
                {
                    $('#order').closest('.form-group').find('.help-block').text(response.message);
                    if (response.status == 0) {
                        $('.submit_form').prop('disabled', true);
                        $('#order').closest('.form-group').addClass('has-error');
                    } else {
                        $('.submit_form').prop('disabled', false);
                        $('#order').closest('.form-group').removeClass('has-error');
                    }
                },
                error: function (err) {
                }
            });
        });

        $('#resource_topics_form').on('click', '.deleteFile', function () {
            $(this).closest('.box-solid').remove();
        });

        /*PDF Uploader*/
        $.fn.filemanager = function (type) {
            type = type || 'image';

            if (type === 'image' || type === 'images') {
                type = 'Images';
            } else {
                type = 'Files';
            }

            this.on('click', function (e) {
                localStorage.setItem('target_input', $(this).data('input'));
                localStorage.setItem('target_preview', $(this).data('preview'));
//                window.open('{{route("unisharp.lfm.show")}}?type=Files', 'FileManager', 'width=900,height=600'); 
                return false;
            });
        };
        $('#lfm').filemanager('file');
        $('.colorpicker').colorpicker();
    });

    function SetUrl(url) {
        //set the value of the desired input to image url
        var target_input = $('#' + localStorage.getItem('target_input'));
        target_input.val(url);
        var filename = url.split('/').pop();
        $('.cke_button__link_icon').click();
        setTimeout(function(){ 
            $('#cke_86_textInput').val(url);
        }, 700);

        //set or change the preview image src
        var target_preview = $('#' + localStorage.getItem('target_preview'));
        target_preview.attr('src', url);
    }
</script>
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script>
  $( function() {
    $( "#resource_topics" ).sortable();
    $( "#resource_topics" ).disableSelection();
  } );
</script>
@endsection