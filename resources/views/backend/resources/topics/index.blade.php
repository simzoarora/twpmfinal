@extends ('backend.layouts.app')

@section ('title', 'Resource Topics')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
    .w45 {
        width: 45px;
    }
    .w210 {
        width: 210px;
    }
    .success_message {
        color: #00a65a;
    }
    .error_message {
        color: #dd4b39;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Resource Topics</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Resource Topics</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.resources.topics.create')}}"><span class="btn btn-primary btn-xs">Add New Topic</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="resource_topics_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Name</th>
                        <th class="w210">Order</th>
                        <th class="w10">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($topics as $key => $topic)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th><?php echo (strlen($topic->title) > 70) ? substr(strip_tags($topic->title), 0, 70) . '...' : $topic->title; ?></th>
                        <th class="topic_order">
                            <span class="existing_order">{{$topic->order}}</span>
                            <span class="edit_order hidden">
                                <input type="number" name="order" class="w45 text-center" value="{{$topic->order}}"/>
                                <a href="" class="btn btn-xs btn-success save-order" data-id="{{$topic->id}}">
                                    <i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Save"></i>
                                </a>
                            </span>
                            <small class="edit-order-response"></small>
                        </th>
                        <th>
                            <a href="{{route('admin.resources.topics.edit',$topic->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <span data-target="#deleteModal" data-toggle="modal" data-id="{{$topic->id}}" class="btn btn-xs btn-danger deleteResourceTopic"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </th>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteResourceTopicForm']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Resource Topic</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Resource Topic?
                {{ Form::hidden('id',null,['id' => 'hiddenResourceTopicId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('#resource_topics_table').on('click', '.deleteResourceTopic', function () {
            $('#hiddenResourceTopicId').val($(this).data('id'));
        });

        $('#deleteResourceTopicForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("admin.resources.topics.delete")}}',
                type: 'post',
                data: {
                    id: $('#hiddenResourceTopicId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("admin.resources.topics")}}');
                },
                error: function (err) {
                }
            });
        });

        $('#resource_topics_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        
        $('#resource_topics_table').on('click', '.topic_order', function () {
            $(this).find('.existing_order').addClass('hidden');
            $(this).find('.edit_order').removeClass('hidden');
        });
        
        $('#resource_topics_table').on('click', '.save-order', function (e) {
            e.preventDefault();

            var edit_order = $(this).closest('.edit_order'),
                    input = edit_order.find('input').val(),
                    topic_id = $(this).data('id'),
                    response_holder = $(this).closest('.topic_order').find('.edit-order-response'),
                    old_value = $(this).closest('.topic_order').find('.existing_order');

            $.ajax({
                url: '{{route("admin.resources.topics.order.edit")}}',
                type: 'post',
                data: {
                    order: input,
                    topic_id: topic_id,
                    form: 0
                },
                success: function (response)
                {
                    response_holder.removeClass('hidden').text(response.message);
                    old_value.text(input);
                    
                    if (response.status == 1) {
                        old_value.removeClass('hidden');
                        response_holder.removeClass('error_message').addClass('success_message');
                        edit_order.addClass('hidden');
                        setTimeout(function () {
                            response_holder.addClass('hidden');
                        }, 1000);
                    } else {
                        response_holder.removeClass('success_message').addClass('error_message');
                    }
                },
                error: function (err) {
                }
            });
        });
    });
</script>
@stop
