@extends ('backend.layouts.app')

@section ('title', 'Employees')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
    .w45 {
        width: 45px;
    }
    .w210 {
        width: 210px;
    }
    .success_message {
        color: #00a65a;
    }
    .error_message {
        color: #dd4b39;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Employees</small>
</h1>
@endsection

@section('content')
<div class="row">
<div class="col-md-12">
    @if(Session::has('updateMessage'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="icon icon-info-circle icon-lg"></span>
        <small>{!! session('updateMessage') !!}</small>
    </div>
    @elseif(Session::has('saveMessage'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="icon icon-info-circle icon-lg"></span>
        <small>{!! session('saveMessage') !!}</small>
    </div>
    @endif
</div>
</div>
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Employees</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.employee.create')}}"><span class="btn btn-primary btn-xs">Add Another Employee</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="employees_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th style="max-width:300px;">Description</th>
                        <th class="w210">Order</th>
                        <th class="w10">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($employees as $key => $employee)
                    <tr>
                        <td>{{$employee->name}}</td>
                        <td>{{$employee->title}}</td>
                        <td><?php echo substr(strip_tags($employee->description), 0, 50);?>...</th>
                        <th class="employee_order">
                            <span class="existing_order">{{$employee->order}}</span>
                            <span class="edit_order hidden">
                                <input type="number" name="order" class="w45 text-center" value="{{$employee->order}}"/>
                                <a href="" class="btn btn-xs btn-success save-order" data-id="{{$employee->id}}">
                                    <i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Save"></i>
                                </a>
                            </span>
                            <small class="edit-order-response"></small>
                        </th>
                        <td>
                            <a href="{{route('admin.employee.edit',$employee->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <span data-target="#deleteModal" data-toggle="modal" data-route="{{route("admin.employee.destroy",$employee->id)}}" class="btn btn-xs btn-danger deleteEmployee"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </td>
                    </tr>
                    @empty
                    No Employees.
                    @endforelse
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteEmployeeForm','url' => route('admin.employee.destroy',0),'method'=>'DELETE']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete Employee</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Employee?
                {{ Form::hidden('id',null,['id' => 'hiddenServiceCategoryId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('.deleteEmployee').on('click',function(){
            $('#deleteEmployeeForm').attr('action',$(this).attr('data-route'));
        });
        $('#employees_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
        
        $('#employees_table').on('click', '.employee_order', function () {
            $(this).find('.existing_order').addClass('hidden');
            $(this).find('.edit_order').removeClass('hidden');
        });

        $('#employees_table').on('click', '.save-order', function (e) {
            e.preventDefault();

            var edit_order = $(this).closest('.edit_order'),
                    input = edit_order.find('input').val(),
                    employee_id = $(this).data('id'),
                    response_holder = $(this).closest('.employee_order').find('.edit-order-response'),
                    old_value = $(this).closest('.employee_order').find('.existing_order');

            $.ajax({
                url: '{{route("admin.employees.order.edit")}}',
                type: 'post',
                data: {
                    order: input,
                    employee_id: employee_id,
                    form: 0
                },
                success: function (response)
                {
                    response_holder.removeClass('hidden').text(response.message);
                    old_value.text(input);
                    
                    if (response.status == 1) {
                        old_value.removeClass('hidden');
                        response_holder.removeClass('error_message').addClass('success_message');
                        edit_order.addClass('hidden');
                        setTimeout(function () {
                            response_holder.addClass('hidden');
                        }, 1000);
                    } else {
                        response_holder.removeClass('success_message').addClass('error_message');
                    }
                },
                error: function (err) {
                }
            });
        });
    });
</script>
@stop
