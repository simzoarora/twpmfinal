@extends('backend.layouts.master')

@section ('title', 'Employees' )

@section('page-header')
<h1>
    {{ app_name() }}
    <small><?php echo $employee?'Edit':'Add New'; ?> Employee</small>
</h1>
@endsection
@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
<style>
    .dropzone:after {
        font-size: 20px;
        top: 50%;
        bottom: auto;
        transform: translate(0%,-50%);
    }
    .dropzone .tools {
        width: 30px;
        top: 0px;
        right: -30px;
    }
</style>
@stop
@section('content')
<div class="row">
    @if(Session::has('updateMessage'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="icon icon-info-circle icon-lg"></span>
        <small>{!! session('updateMessage') !!}</small>
    </div>
    @elseif(Session::has('saveMessage'))
    <div class="alert alert-success">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <span class="icon icon-info-circle icon-lg"></span>
        <small>{!! session('saveMessage') !!}</small>
    </div>
    @endif
</div>
<?php if($employee){ ?>
{{ Form::model($employee,['url'=>route('admin.employee.update',$employee->id),'method'=>'PUT','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$employee->id) }}
<?php }else{ ?>
{{ Form::open(array('route' => 'admin.employee.store','class' => 'form-horizontal','role' => 'form','files'=>'true')) }}
    <?php } ?>
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        <h3 class="box-title"><?php echo $employee?'Edit':'Add New'; ?> Employee</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            {{ Form::label('name','Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('order')) echo ' has-error'; ?>">
            {{ Form::label('order','Order', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('order', null, ['class' => 'form-control', 'placeholder' => 'Order']) }}
                <span class="help-block">{{ $errors->first('order') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title', 'Title',['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('description')) echo ' has-error'; ?>">
            {{ Form::label('description','Description', ['class' => 'col-lg-2 control-label']) }} 
            <div class="col-lg-10">
                {{ Form::textarea('description', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('phone')) echo ' has-error'; ?>">
            {{ Form::label('phone', 'Phone',['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) }}
                <span class="help-block">{{ $errors->first('phone') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('email')) echo ' has-error'; ?>">
            {{ Form::label('email', 'Email',['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) }}
                <span class="help-block">{{ $errors->first('email') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('linkedin')) echo ' has-error'; ?>">
            {{ Form::label('linkedin', 'Linked In',['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('linkedin', null, ['class' => 'form-control', 'placeholder' => 'Linked In']) }}
                <span class="help-block">{{ $errors->first('linkedin') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('twitter')) echo ' has-error'; ?>">
            {{ Form::label('twitter', 'Twitter',['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('twitter', null, ['class' => 'form-control', 'placeholder' => 'Twitter']) }}
                <span class="help-block">{{ $errors->first('twitter') }}</span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('image','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10 file">
                <div class="dropzone" data-width="300" data-height="300" data-save="false" data-button-edit="true" data-image="<?php echo $employee ? URL::asset('img/backend/aboutpage/employees/' . $employee->image) : ''; ?>">
                    <input type="file" name="sponsor_logo_file" class="form-control"/>
                </div>
                <input type="hidden" id="filename" name="image[name]" value="<?php echo $employee ? $employee->image : ''; ?>" />
                <input type="hidden" id="filebase64" name="image[data]" />
            </div>
        </div>
    </div>
</div><!--box box-success-->
<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.employee.index', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Submit', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
    $(document).ready(function () {
        $('.dropzone').html5imageupload({
            onSave: function (data) {
                $('#filename').val(data.name);
                $('#filebase64').val(data.data);
            }
        });
        
        $('#order').on('keyup', function () {
            var order = $('#order').val(),
                    employee_id = $('input[name="id"]').val();

            $.ajax({
                url: '{{route("admin.employees.order.edit")}}',
                type: 'post',
                data: {
                    order: order,
                    employee_id: employee_id,
                    form: 1
                },
                success: function (response)
                {
                    $('#order').closest('.form-group').find('.help-block').text(response.message);
                    if (response.status == 0) {
                        $('.submit_form').prop('disabled', true);
                        $('#order').closest('.form-group').addClass('has-error');
                    } else {
                        $('.submit_form').prop('disabled', false);
                        $('#order').closest('.form-group').removeClass('has-error');
                    }
                },
                error: function (err) {
                }
            });
        });
    });
</script>
@endsection