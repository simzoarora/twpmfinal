@extends('backend.layouts.master')  

@section ('title', 'Privacy & Security') 

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Privacy & Security</small>
</h1>
@endsection

@section('after-styles')
@stop

@section('content')

{{ Form::model($content,['id'=>'privacy_form','class' => 'form-horizontal','role' => 'form']) }}

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Privacy & Security</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('content')) echo ' has-error'; ?>">
            {{ Form::label('content','Content', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('content', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Content']) }}
                <span class="help-block">{{ $errors->first('content') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.privacy', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
</script>
@endsection