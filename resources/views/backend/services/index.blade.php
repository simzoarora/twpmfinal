@extends ('backend.layouts.app')

@section ('title', 'Services')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
    .w80 {
        width: 80px;
    }
    .w45 {
        width: 45px;
    }
    .w210 {
        width: 210px;
    }
    .success_message {
        color: #00a65a;
    }
    .error_message {
        color: #dd4b39;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Services</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Services</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.services.create')}}"><span class="btn btn-primary btn-xs">Add New Service</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="services_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Title</th>
                        <!--<th>Image</th>-->
                        <th class="w210">Order</th>
                        <th class="w80">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($services as $key => $service)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th><?php echo (strlen($service->title) > 70) ? substr(strip_tags($service->title), 0, 70) . '...' : $service->title; ?></th>
                        <!--<th></th>-->
                        <th class="service_order">
                            <span class="existing_order">{{$service->order}}</span>
                            <span class="edit_order hidden">
                                <input type="number" name="order" class="w45 text-center" value="{{$service->order}}"/>
                                <a href="" class="btn btn-xs btn-success save-order" data-id="{{$service->id}}">
                                    <i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="Save"></i>
                                </a>
                            </span>
                            <small class="edit-order-response"></small>
                        </th>
                        <th>
                            <a href="{{route('admin.services.edit',$service->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <!--<a href="{{ route('admin.services.questions', $service->id) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="View All Questions"></i></a>--> 
                            <span data-target="#deleteModal" data-toggle="modal" data-id="{{$service->id}}" class="btn btn-xs btn-danger deleteService"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </th>
                    </tr>
                    @empty
                    No services.
                    @endforelse
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteServiceForm']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Service</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Service?
                {{ Form::hidden('id',null,['id' => 'hiddenServiceId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('#services_table').on('click', '.deleteService', function () {
            $('#hiddenServiceId').val($(this).data('id'));
        });

        $('#deleteServiceForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("admin.services.delete")}}',
                type: 'post',
                data: {
                    id: $('#hiddenServiceId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("admin.services")}}');
                },
                error: function (err) {
                }
            });
        });

        $('#services_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });

        $('#services_table').on('click', '.service_order', function () {
            $(this).find('.existing_order').addClass('hidden');
            $(this).find('.edit_order').removeClass('hidden');
        });

        $('#services_table').on('click', '.save-order', function (e) {
            e.preventDefault();

            var edit_order = $(this).closest('.edit_order'),
                    input = edit_order.find('input').val(),
                    service_id = $(this).data('id'),
                    response_holder = $(this).closest('.service_order').find('.edit-order-response'),
                    old_value = $(this).closest('.service_order').find('.existing_order');

            $.ajax({
                url: '{{route("admin.services.order.edit")}}',
                type: 'post',
                data: {
                    order: input,
                    service_id: service_id,
                    form: 0
                },
                success: function (response)
                {
                    response_holder.removeClass('hidden').text(response.message);
                    old_value.text(input);

                    if (response.status == 1) {
                        old_value.removeClass('hidden');
                        response_holder.removeClass('error_message').addClass('success_message');
                        edit_order.addClass('hidden');
                        setTimeout(function () {
                            response_holder.addClass('hidden');
                        }, 1000);
                    } else {
                        response_holder.removeClass('success_message').addClass('error_message');
                    }
                },
                error: function (err) {
                }
            });
        });

    });
</script>
@stop
