@extends('backend.layouts.master')
@if(isset($service))
@section ('title', 'Edit Service' )
@else
@section ('title', 'Create Service' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($service))
    <small>Edit Service</small>
    @else
    <small>Create Service</small>
    @endif
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
{{ Html::style("css/backend/plugin/colorpicker/bootstrap-colorpicker.min.css") }}
<style>
    .dropzone:after {
        bottom: 3%;
    }
    .change_file {
        margin: 0 10px;
    }
    .file_link {
        cursor: pointer;
    }
    .upload_file_group {
        display: none;
    }
</style>
@stop

@section('content')
@if(isset($service))
{{ Form::model($service,['id'=>'services_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$service->id) }}
@else
{{ Form::open(['id'=>'services_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        @if(isset($service))
        <h3 class="box-title">Edit Service</h3>
        <div class="pull-right">
            <a href="{{ route('admin.services.questions.create', $service->id) }}" class="btn btn-info">Add Service Question</a>
            <a href="{{ route('admin.services.questions', $service->id) }}" class="btn btn-warning">View All Service Questions</a>
        </div>
        @else
        <h3 class="box-title">Create Service</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('title')) echo ' has-error'; ?>">
            {{ Form::label('title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('title_bg_color')) echo ' has-error'; ?>">
            {{ Form::label('title_bg_color','Background Color', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-3">
                {{ Form::text('title_bg_color', null, ['class' => 'form-control colorpicker']) }}
                <span class="help-block">{{ $errors->first('title_bg_color') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('order')) echo ' has-error'; ?>">
            {{ Form::label('order','Order', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::number('order', null, ['class' => 'form-control', 'placeholder' => 'Order']) }}
                <span class="help-block">{{ $errors->first('order') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('image')) echo ' has-error'; ?>">
            {{ Form::label('image','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if(isset($service))
                        <input type="file" name="image" />
                        @else
                        <input type="file" name="image" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('image') }}</span>
                    </div>
                    @if(isset($service))
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/services/'.$service['image'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('description')) echo ' has-error'; ?>">
            {{ Form::label('description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('description', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('recommended_for')) echo ' has-error'; ?>">
            {{ Form::label('recommended_for','Recommended For', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('recommended_for', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Recommended For']) }}
                <span class="help-block">{{ $errors->first('recommended_for') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('cost')) echo ' has-error'; ?>">
            {{ Form::label('cost','Cost', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('cost', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Cost']) }}
                <span class="help-block">{{ $errors->first('cost') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('actual_cost')) echo ' has-error'; ?>">
            {{ Form::label('amount','Amount', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('amount', null, ['class' => 'form-control actual_cost', 'placeholder' => 'Amount','id'=>'amount']) }}
                <span class="help-block">{{ $errors->first('amount') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('fee_schedule')) echo ' has-error'; ?>">
            {{ Form::label('fee_schedule','Fee Schedule', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('fee_schedule', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Fee Schedule']) }}
                <span class="help-block">{{ $errors->first('fee_schedule') }}</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">
                Category
            </label>
            <div class="col-lg-10">
                @foreach($serviceCategories as $key => $serviceCategory)
                <div class="col-lg-12">
                    <img src="{{URL::asset('img/backend/services/categories/'.$serviceCategory->logo)}}" style="max-width:25px; max-heigh:25px;"/>
                    @if(isset($selectedServiceCategories) && in_array($serviceCategory->id,$selectedServiceCategories))
                    {{ Form::checkbox('service_category_id['.($key).']',$serviceCategory->id,true) }}
                    @else
                    {{ Form::checkbox('service_category_id['.($key).']',$serviceCategory->id) }}
                    @endif
                    {{ Form::label('service_category_id['.($key).']',$serviceCategory->name, ['class' => 'control-label']) }}
                </div>
                @endforeach
                <span class="help-block service_category_id_error"></span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-lg-2 control-label">Files</label>
            <div class="files col-lg-10">
                @if(isset($service) && !($service->serviceFiles->isEmpty()))
                @foreach($service->serviceFiles as $key => $serviceFile)
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">File {{($key+1)}}</h3>
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('file_text['.($key+1).']','File Text',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::text('file_text['.($key+1).']',$serviceFile->file_text,['class' => 'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('file_link','File Link',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                <a class="file_link" href="{{URL::asset('img/backend/services/file_uploads/'.$serviceFile->upload_file)}}" target="_blank"><i class="fa fa-file-o fa-lg text-success"></i></a>
                                <span class="btn btn-success btn-xs change_file">Change File</span>
                            </div>
                        </div>
                        <div class="form-group upload_file_group">
                            {{Form::label('upload_file['.($key+1).']','Upload File',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::file('upload_file['.($key+1).']')}}
                            </div>
                            {{Form::hidden('upload_file_old['.($key+1).']',$serviceFile->upload_file)}}
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="box box-default box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">File 1</h3>
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                    </div>
                    <div class="box-body">
                        <div class="form-group">
                            {{Form::label('file_text[1]','File Text',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::text('file_text[1]',null,['class' => 'form-control'])}}
                            </div>
                        </div>
                        <div class="form-group">
                            {{Form::label('upload_file[1]','Upload File',['class' => 'col-lg-2 control-label'])}}
                            <div class="col-lg-10">
                                {{Form::file('upload_file[1]')}}
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="text-center">
                <span class="btn btn-primary btn-" id="add_more_files">Add More Files</span>
            </div>
        </div>

    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.services', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($service))
            {{ Form::submit('Update Service', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Service', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
{{ Html::script("js/backend/plugin/colorpicker/bootstrap-colorpicker.min.js") }}
<script>
    var fileCounter = $('.files').children().length;

    $('#add_more_files').on('click', function (e) {
        e.preventDefault();
        fileCounter++;
        var fileToAppend = appendTemplate(fileCounter);
        $('.files').append(fileToAppend);
    });

    $(function () {
        $('#services_form').on('click', '.deleteFile', function () {
            $(this).closest('.box-solid').remove();
        });

        $('#order').on('keyup', function () {
            var order = $('#order').val(),
                    service_id = $('input[name="id"]').val();

            $.ajax({
                url: '{{route("admin.services.order.edit")}}',
                type: 'post',
                data: {
                    order: order,
                    service_id: service_id,
                    form: 1
                },
                success: function (response)
                {
                    $('#order').closest('.form-group').find('.help-block').text(response.message);
                    if (response.status == 0) {
                        $('.submit_form').prop('disabled', true);
                        $('#order').closest('.form-group').addClass('has-error');
                    } else {
                        $('.submit_form').prop('disabled', false);
                        $('#order').closest('.form-group').removeClass('has-error');
                    }
                },
                error: function (err) {
                }
            });
        });
        $('.colorpicker').colorpicker();
    });

    $('#services_form').on('submit', function (e) {
        e.preventDefault();
        var checkedBoxes = $("input[type='checkbox']:checked").length,
                categoryErrorSpan = $('.service_category_id_error');
        if (checkedBoxes > 0) {
            $('.submit_form').prop('disabled', false);
            $('#services_form').unbind().submit();
        } else {
            categoryErrorSpan.closest('.form-group').addClass('has-error');
            categoryErrorSpan.text('Atleast 1 Service Category is required.');
            $('.submit_form').prop('disabled', true);
        }
    });

    $("input[type='checkbox']").on('change', function () {
        $('.submit_form').prop('disabled', false);
    });

    $('.change_file').on('click', function () {
        $(this).closest('.box').find('.upload_file_group').toggle();
    });

    function appendTemplate(counter) {
        return '<div class="box box-default box-solid">\n\
                    <div class="box-header with-border">\n\
                        <h3 class="box-title">File ' + counter + '</h3>\n\
                        <i class="fa fa-window-close pull-right deleteFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>\n\
                    </div>\n\
                    <div class="box-body">\n\
                        <div class="form-group">\n\
                            <label for="file_text[' + counter + ']" class="col-lg-2 control-label">File Text</label>\n\
                            <div class="col-lg-10">\n\
                                <input type="text" name="file_text[' + counter + ']" class="form-control"/>\n\
                            </div>\n\
                        </div>\n\
                        <div class="form-group">\n\
                            <label for="upload_file[' + counter + ']" class="col-lg-2 control-label">Upload File</label>\n\
                            <div class="col-lg-10">\n\
                                <input type="file" name="upload_file[' + counter + ']"/>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>';
    }
</script>
@endsection