@extends('backend.layouts.master')
@if(isset($question))
@section ('title', 'Edit Service Question' )
@else
@section ('title', 'Create Service Question' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($question))
    <small>Edit Service Question</small>
    @else
    <small>Create Service Question</small>
    @endif
</h1>
@endsection

@section('after-styles')

@stop

@section('content')

@php($questionHaveOptions = false)


@if(isset($question))
{{ Form::model($question,['route' =>'admin.services.questions.updateQuestion','id'=>'service_questions_form','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
{{ Form::hidden('id',$question->id) }}
@else
{{ Form::open(['route' =>'admin.services.questions.saveQuestion','id'=>'service_questions_form','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
@endif
<div class="box box-success">
    <div class="box-header with-border">
        @if(isset($question))
        <h3 class="box-title">Edit Service Question</h3>
        @else
        <h3 class="box-title">Create Service Question</h3>
        @endif
    </div>
    <div class="box-body">
        {{ Form::hidden('service_id',$serviceId) }}
        <div class="form-group <?php if ($errors->first('question')) echo ' has-error'; ?>">
            {{ Form::label('question','Question', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('question', null, ['class' => 'form-control', 'required','placeholder' => 'Write your question here']) }}
                <span class="help-block">{{ $errors->first('question') }}</span>
            </div>
        </div>


        <div class="form-group <?php if ($errors->first('profile_tag_id')) echo ' has-error'; ?>">
            {{ Form::label('profile_tag_id','Profile Tag Type', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('profile_tag_id', array_merge([''=>'Choose Profile Tag Type'],$profileTags), null, ['class' => 'form-control','id' => 'profile_tag_type', 'placeholder' => 'Choose Profile Tag Type']) }}
                <span class="help-block">{{ $errors->first('type') }}</span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('type')) echo ' has-error'; ?>">
            {{ Form::label('question_type','Question Type', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::select('type', config('constant.service_question_types'), null, ['class' => 'form-control','id' => 'question_type', 'required','placeholder' => 'Choose Question Type']) }}
                {{ Form::hidden('type', null, ['class' => 'form-control','id' => 'question_type_hidden', 'required','disabled']) }}
                <span class="help-block">{{ $errors->first('type') }}</span>
            </div>
        </div>

        <div class="form-group option-box-div" style="display:none">
            <label class="col-lg-2 control-label">Options</label>
            <div class="question_options col-lg-10">
                @if(isset($question) && !($question->serviceQuestionOption->isEmpty()))
                @php($questionHaveOptions = true)
                @foreach($question->serviceQuestionOption as $key => $answer_option)
                <div class="box-body option-box">
                    <div class="form-group">
                        <div class="col-lg-9">
                            {{ Form::hidden('option_name['.($key+1).'][id]',$answer_option->id) }}
                            {{Form::text('option_name['.($key+1).'][name]',$answer_option->name,['class' => 'form-control'])}}
                            {{Form::hidden('option_name['.($key+1).'][is_deleted]',true,['class' => 'form-control question-deleted','disabled' => 'disabled'])}}

                        </div>
                        <div class="col-lg-1">
                            <i class="fa fa-window-close pull-right deleteOption" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="box-body option-box">
                    <div class="form-group">
                        <div class="col-lg-9">
                            {{Form::text('option_name[1]',null,['class' => 'form-control option-disabled','disabled' => 'disabled','required'])}}
                        </div>
                        <div class="col-lg-1">
                            <i class="fa fa-window-close pull-right deleteOption" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="text-center">
                <span class="btn btn-primary pull-right" id="add_more_options" style="margin-right: 45px;"><i class="fa fa-plus" aria-hidden="true"></i></span>
            </div>
        </div>

        <div class="form-group <?php if ($errors->first('order')) echo ' has-error'; ?>">
            {{ Form::label('order','Order', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('order', null, ['class' => 'form-control question-order-value', 'required','placeholder' => 'Order in which question will be asked from user']) }}
                <span class="help-block question-order-status">{{ $errors->first('order') }}</span>
            </div>
        </div>


        <div class="box box-info">
            <div class="box-body">
                <div class="pull-left">
                    <a href="{{ route('admin.services.questions', $serviceId) }}" class="btn btn-danger">Cancel</a> 
                </div><!--pull-left-->

                <div class="pull-right">
                    @if(isset($question))
                    {{ Form::submit('Update Question', ['class' => 'btn btn-success submit_form']) }}
                    @else
                    {{ Form::submit('Save Question', ['class' => 'btn btn-success submit_form']) }}
                    @endif
                </div><!--pull-right-->

                <div class="clearfix"></div>
            </div><!-- /.box-body -->
        </div><!--box-->
    </div>
</div>

{{ Form::close() }}
@endsection

@section('after-scripts')
<script>
    $(document).ready(function () {

        $('#profile_tag_type').on('change select', function () {
            if ($('#profile_tag_type').val() != '') {
                var questionType = "<?php echo config('constant.service_question_types_options_inverse.sla'); ?>";
                $('#question_type').attr('disabled', 'disabled');
                $('#question_type_hidden').removeAttr('disabled');
                $('#question_type_hidden').val(questionType);
            } else {
                $('#question_type').val();
                $('#question_type').removeAttr('disabled');
                $('#question_type_hidden').val('');
                $('#question_type_hidden').attr('disabled', 'disabled');
            }
        });


        $('#question_type').on('change select', function () {
            var type = $('#question_type').val();
            var mcma = "<?php echo config('constant.service_question_types_options.mcma'); ?>";
            var mcsa = "<?php echo config('constant.service_question_types_options.mcsa'); ?>";
            if (type == mcma || type == mcsa) {
                $('.option-box-div').show();
                $('.option-disabled').removeAttr('disabled');

            } else {
                $('.option-box-div').hide();
                $('.option-disabled').attr('disabled', 'disabled');
            }
        });
        $('#question_type').trigger('change');

        var optionCounter = $('.question_options').children().length;

        $('#add_more_options').on('click', function (e) {
            e.preventDefault();
            optionCounter++;
            var optionToAppend = appendTemplate(optionCounter);
            $('.question_options').append(optionToAppend);
        });


        function appendTemplate(counter) {
            return '<div class="box-body option-box">\n\
                <div class="form-group">\n\
                    <div class="col-lg-9">\n\
                        <input type="text" name="option_name[' + counter + ']" class="form-control option-disabled"/>\n\
                    </div>\n\
                   <div class="col-lg-1">\n\
                    <i class="fa fa-window-close pull-right deleteOption" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>\n\
                </div>\n\
                </div>\n\
            </div>\n\
        ';
        }

        $('#service_questions_form').on('click', '.deleteOption', function () {
            var questionHaveOptions = "<?php echo $questionHaveOptions; ?>";
            if (questionHaveOptions) {
                $(this).closest('.option-box').hide();
                $(this).closest('.form-group').find('.question-deleted').removeAttr('disabled');

            } else {
                $(this).closest('.option-box').remove();
            }

        });

        $('.question-order-value').on('change focusout keyup', function () {
            var questionId = "<?php echo isset($question) ? $question->id : NULL; ?>";
            var serviceId = "<?php echo $serviceId; ?>";
            var orderVal = $('.question-order-value').val();
            if (orderVal) {
                $.ajax({
                    url: '{{route("admin.services.questions.order.check")}}',
                    type: 'POST',
                    data: {
                        question_id: questionId,
                        order: orderVal,
                        service_id: serviceId,
                        form: 1
                    },
                    success: function (response)
                    {
                        $('#order').closest('.form-group').find('.help-block').text(response.message);
                        if (response.status == 0) {
                            $('.submit_form').prop('disabled', true);
                            $('#order').closest('.form-group').addClass('has-error');
                        } else {
                            $('.submit_form').prop('disabled', false);
                            $('#order').closest('.form-group').removeClass('has-error');
                        }
                    },
                    error: function (err) {
                    }

                });
            }

        });

    });

</script>
@endsection