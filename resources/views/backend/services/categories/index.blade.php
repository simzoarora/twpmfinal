@extends ('backend.layouts.app')

@section ('title', 'Service Categories')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Service Categories</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Service Categories</h3>

        <div class="box-tools pull-right">
            <a href="{{route('admin.services.categories.create')}}"><span class="btn btn-primary btn-xs">Add New Category</span></a>
        </div><!--box-tools pull-right-->
    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="service_categories_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Name</th>
                        <th class="w10">Logo</th>
                        <th class="w10">{{ trans('labels.general.actions') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($categories as $key => $category)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th><?php echo (strlen($category->name) > 100) ? substr(strip_tags($category->name), 0, 100).'...' : $category->name ;?></th>
                        <th><img src="{{URL::asset('img/backend/services/categories/'.$category->logo)}}" style="max-width:25px; max-height:25px;"/></th>
                        <th>
                            <a href="{{route('admin.services.categories.edit',$category->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                            <span data-target="#deleteModal" data-toggle="modal" data-id="{{$category->id}}" class="btn btn-xs btn-danger deleteServiceCategory"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                        </th>
                    </tr>
                    @empty
                    No service categories.
                    @endforelse
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->
<!-- Modals -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteServiceCategoryForm']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="deleteModalLabel">Delete Service Category</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Service Category?
                {{ Form::hidden('id',null,['id' => 'hiddenServiceCategoryId']) }}
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>
    $(function () {
        $('#service_categories_table').on('click', '.deleteServiceCategory', function () {
            $('#hiddenServiceCategoryId').val($(this).data('id'));
        });

        $('#deleteServiceCategoryForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("admin.services.categories.delete")}}',
                type: 'post',
                data: {
                    id: $('#hiddenServiceCategoryId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("admin.services.categories")}}');
                },
                error: function (err) {
                }
            });
        });

        $('#service_categories_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>
@stop
