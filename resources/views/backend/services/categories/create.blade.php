@extends('backend.layouts.master')
@if(isset($category))
@section ('title', 'Edit Service Category' )
@else
@section ('title', 'Create Service Category' )
@endif

@section('page-header')
<h1>
    {{ app_name() }}
    @if(isset($category))
    <small>Edit Service Category</small>
    @else
    <small>Create Service Category</small>
    @endif
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
<style>
    .dropzone:after {
        bottom: 3%;
    }
    .dropzone .tools {
        width: 30px;
        top: 0px;
        right: -30px;
    }
    .dropzone:after {
        content: 'Drop Here';
        font-size: 16px !important;
        bottom: 1%;
    }
</style>
@stop

@section('content')
@if(isset($category))
{{ Form::model($category,['id'=>'service_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
{{ Form::hidden('id',$category->id) }}
@else
{{ Form::open(['id'=>'service_categories_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
@endif
<div class="box box-success">
    <div class="box-header with-border">
        @if(isset($category))
        <h3 class="box-title">Edit Service Category</h3>
        @else
        <h3 class="box-title">Create Service Category</h3>
        @endif
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('name')) echo ' has-error'; ?>">
            {{ Form::label('name','Name', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) }}
                <span class="help-block">{{ $errors->first('name') }}</span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('logo','Logo', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10 file">
                <div class="dropzone" data-width="50" data-height="50" data-save="false" data-button-edit="true" data-image="{{(isset($category)) ? URL::asset('img/backend/services/categories/'.$category['logo']) : ''}}">
                    @if(isset($category))
                    <input type="file" name="logo" class="form-control"/>
                    @else
                    <input type="file" name="logo" class="form-control" required/>
                    @endif
                </div>
                @if(isset($category))
                <input type="hidden" name="logo_old" value="{{$category->logo}}"/>
                @endif
                <input type="hidden" name="logo[name]" />
                <input type="hidden" name="logo[data]" />
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.services.categories', 'Cancel', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            @if(isset($category))
            {{ Form::submit('Update Category', ['class' => 'btn btn-success submit_form']) }}
            @else
            {{ Form::submit('Save Category', ['class' => 'btn btn-success submit_form']) }}
            @endif
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
<script>
    $(function () {
        $('.dropzone').html5imageupload({
            onSave: function (data) {
                var $this = $('.dropzone');
                $this.closest('.file').find('[name="logo[name]"]').val(data.name);
                $this.closest('.file').find('[name="logo[data]"]').val(data.data);
            }
        });
    });
</script>
@endsection