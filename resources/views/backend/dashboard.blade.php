@extends('backend.layouts.master')

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.dashboard.title') }}</small>
</h1>
@endsection

@section('after-styles') 
<style type="text/css">
    .history-box p{
        border-bottom: 1px solid #f4f4f4;
        padding: 10px 0;
        margin: 0;
    }
    .history-box p span{
        float: right;
    }
</style>
@stop

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {{ $logged_in_user->name }}!</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box tools -->
    </div>
    <div class="box-body">
        Customize your site by clicking on any feature on the left sidebar.
        Add, edit or delete any feature. Enjoy!!
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Client Notifications</h3>
        <div class="box-tools pull-right">
            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div><!-- /.box tools -->
    </div><!-- /.box-header -->
    <div class="box-body history-box">
        @foreach ($subscriptionHistory as $history)
        <p>{{ $history->user->email }} subscribed to {{ $history->service->title }}.  <span>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($history->updated_at))->diffForHumans() }}</span></p>
        @endforeach
    </div><!-- /.box-body -->
</div><!--box box-success-->
@endsection