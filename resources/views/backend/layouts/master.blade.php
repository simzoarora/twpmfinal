<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('title', app_name())</title>
        <!-- favicons start -->
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />
        <link href="img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
        <link href="img/apple-touch-icon-167x167.png" rel="apple-touch-icon" sizes="167x167" />
        <link href="img/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
        <link href="img/icon-hires.png" rel="icon" sizes="192x192" />
        <link href="img/icon-normal.png" rel="icon" sizes="128x128" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset("img/favicon.png")}}" class="favicon">    
        <!-- favicons end -->

        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', '')">
        <meta name="author" content="@yield('meta_author', '')">
        @yield('meta')

        <!-- Styles -->
        @yield('before-styles')

        <!-- Check if the language is set to RTL, so apply the RTL layouts -->
        <!-- Otherwise apply the normal LTR layouts -->
        @langRTL
            {{ Html::style(elixir('css/backend-rtl.css')) }}
            {{ Html::style(elixir('css/rtl.css')) }}
        @else
            {{ Html::style(asset('css/backend.css')) }}
        @endif
 
        @yield('after-styles')

        <!-- Html5 Shim and Respond.js IE8 support of Html5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        {{ Html::script('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}
        {{ Html::script('https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}
        <![endif]-->

        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body class="skin-{{ config('backend.theme') }} {{ config('backend.layout') }}">
        @include('includes.partials.logged-in-as')

        <div class="wrapper">
            @include('backend.includes.header')
            @include('backend.includes.sidebar')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    @yield('page-header')

                    {{-- Change to Breadcrumbs::render() if you want it to error to remind you to create the breadcrumbs for the given route --}}
                    {!! Breadcrumbs::renderIfExists() !!}
                </section>

                <!-- Main content -->
                <section class="content">
                    @include('includes.partials.messages')
                    @yield('content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            @include('backend.includes.footer')
        </div><!-- ./wrapper -->

        <!-- JavaScripts -->
        @yield('before-scripts')
        <script src="{{ asset('js/backend.js') }}"></script> 
        @yield('after-scripts')
    </body>
</html>