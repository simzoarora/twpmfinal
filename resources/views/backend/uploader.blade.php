@extends('backend.layouts.master')

@section('content')
<div class="input-group">
    <span class="input-group-btn">
        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary">
            <i class="fa fa-picture-o"></i> Choose
        </a>
    </span>
    <input id="thumbnail" class="form-control" type="text" name="filepath">
</div>
<span class="btn btn-primary submit_form">get link</span>
<input type="text" id="link"/>
<img id="holder" style="margin-top:15px;max-height:100px;">
@stop

@section('after-scripts')
<script>
$(document).ready(function () {
    $.fn.filemanager = function (type) {
        type = type || 'image';

        if (type === 'image' || type === 'images') {
            type = 'Images';
        } else {
            type = 'Files';
        }

        this.on('click', function (e) {
            localStorage.setItem('target_input', $(this).data('input'));
            localStorage.setItem('target_preview', $(this).data('preview'));
            window.open('{{route("unisharp.lfm.show")}}?type=Files', 'FileManager', 'width=900,height=600');
            return false;
        });
    }
    $('#lfm').filemanager('file');
    
    $('.submit_form').on('click',function(){
        $.ajax({
                url: '{{route("admin.saveUploader")}}',
                type: 'post',
                data: {
                    filepath: $('#thumbnail').val()
                },
                success: function (response)
                {
                    $('#link').val(response.link);
                    $('#holder').after(response.link);
                },
                error: function (err) {
                }
            });
    });
});
function SetUrl(url) {
    //set the value of the desired input to image url
    var target_input = $('#' + localStorage.getItem('target_input'));
    target_input.val(url);

    //set or change the preview image src
    var target_preview = $('#' + localStorage.getItem('target_preview'));
    target_preview.attr('src', url);
}

</script>
@stop