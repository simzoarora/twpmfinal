@extends('backend.layouts.master')
@section ('title', 'Assign Clients To Employee' )

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Assign Clients To Employee</small>
</h1>
@endsection

@section('after-styles')
<style type="text/css">
    #employee-name+ul, #client-name+ul{
        padding: 0;
        list-style: none;
    }
    #employee-name+ul li, #client-name+ul li{
        padding: 5px 12px;
        background: #ecf0f5;
        border-bottom: 1px solid #d2d6de;
        border-left: 1px solid #d2d6de;
        border-right: 1px solid #d2d6de;  
        cursor: pointer;
    }
    #employee-name+ul li:hover, #client-name+ul li:hover{
        background: #fff;   
    }

</style>
@stop

@section('content')


{{ Form::open(['route' =>'admin.storeClientsToEmployee','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Assign Clients To Employee</h3>
    </div>
    <div class="box-body">
        <div class="form-group ">
            {{ Form::label('client','Client', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-8">
                {{ Form::hidden('client_user_id', null, ['id' => 'client-id']) }}
                {{ Form::text('client_user_text', null, ['class' => 'form-control','id'=>'client-name' , 'required','placeholder' => 'Type Client name or email']) }}
                <ul id="client-ul"></ul>
                <span class="help-block">{{ $errors->first('client_user_id') }}</span>
            </div>
        </div>
        <div class="form-group ">
            {{ Form::label('employee','Employee', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-8">
                {{ Form::hidden('employee_user_id', null, ['id'=>'employee-id']) }}
                {{ Form::text('employee_user_text', null, ['class' => 'form-control','id'=>'employee-name', 'required','placeholder' => 'Type Employee name or email']) }}
                <ul id="employee-ul"></ul>
                <span class="help-block">{{ $errors->first('employee_user_id') }}</span>
            </div>
        </div>
    </div>


    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                <a href="{{ route('admin.allclientEmployeeList') }}" class="btn btn-danger">Cancel</a> 
            </div><!--pull-left-->

            <div class="pull-right">
                {{ Form::submit('Assign', ['class' => 'btn btn-success submit_form']) }}
            </div><!--pull-right-->

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->
</div>
</div>

{{ Form::close() }}
@endsection

@section('after-scripts')
<script>
var searchClientUrl = '{{route("admin.searchClient")}}',
 searchEmployeeUrl = '{{route("admin.searchEmployee")}}';
</script>
@endsection