@extends ('backend.layouts.app')

@section ('title', 'Client To Employee List')

@section('after-styles')
{{ Html::style("css/backend/plugin/datatables/dataTables.bootstrap.min.css") }}
<style>
    .w10 {
        width: 10px;
    }
    .w45 {
        width: 45px;
    }
    .w210 {
        width: 210px;
    }
    .success_message {
        color: #00a65a;
    }
    .error_message {
        color: #dd4b39;
    }
</style>
@stop

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Client To Employee List</small>
</h1>
@endsection

@section('content')
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Client To Employee List</h3>

    </div><!-- /.box-header -->

    <div class="box-body">
        <div class="table-responsive">
            <table id="service_questions_table" class="table table-bordered table-hover dataTable">
                <thead>
                    <tr>
                        <th class="w10">S.No.</th>
                        <th>Client</th>
                        <th>Employee</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $key => $user)
                    <tr>
                        <th>{{($key+1)}}</th>
                        <th>{{ @$user->clientUsers->profile->first_name }} {{ @$user->clientUsers->profile->middle_name }} {{ @$user->clientUsers->profile->last_name }} ( {{ @$user->clientUsers->email }} ) </th>
                        <th>{{  $user->employeeUser->name }} ( {{  $user->employeeUser->email }}  )</th>
                        <th><span data-target="#deleteModal" data-toggle="modal" class="btn btn-xs btn-danger deleteEmployee"><i title="" class="fa fa-trash" data-toggle="tooltip" data-placement="top" data-original-title="Delete"></i></span></th>
                    </tr>
                    @empty
                    No Clinets assigned to Employee Users.
                    @endforelse
                </tbody>
            </table>
        </div><!--table-responsive-->
    </div><!-- /.box-body -->
</div><!--box-->


<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['id'=>'deleteClientToEmployeeRelationForm','url' => route("admin.removeClientEmployeeRelation",$user->id),'method'=>'POST']) }}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
                Are you sure you want to delete this Client to Employee Relation?
              
            </div>
            <div class="modal-footer text-center">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop

@section('after-scripts')
{{ Html::script("js/backend/plugin/datatables/jquery.dataTables.min.js") }}
{{ Html::script("js/backend/plugin/datatables/dataTables.bootstrap.min.js") }}
<script>

    $(function () {

        $('#service_questions_table').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });


</script>
@stop
