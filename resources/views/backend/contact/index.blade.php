@extends('backend.layouts.master')  

@section ('title', 'Contact') 

@section('page-header')
<h1>
    {{ app_name() }}
    <small>Contact</small>
</h1>
@endsection

@section('after-styles')
@stop

@section('content')

{{ Form::model($contactContent,['id'=>'contact_form','class' => 'form-horizontal','role' => 'form']) }}

<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Contact</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('address')) echo ' has-error'; ?>">
            {{ Form::label('address','Address', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('address', null, ['class' => 'form-control ckeditor', 'placeholder' => 'Address']) }}
                <span class="help-block">{{ $errors->first('address') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('phone')) echo ' has-error'; ?>">
            {{ Form::label('phone','Phone', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) }}
                <span class="help-block">{{ $errors->first('phone') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.contact', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
</script>
@endsection