@extends('backend.layouts.master')

@section ('title', trans('strings.backend.home.title') )

@section('page-header')
<h1>
    {{ app_name() }}
    <small>{{ trans('strings.backend.home.title') }}</small>
</h1>
@endsection

@section('after-styles')
{{ Html::style("css/backend/plugin/html5imageupload/html5imageupload.css") }}
<style>
    .dropzone:after {
        bottom: 3%;
    }
    .deleteLogoBox {
        cursor: pointer;
    }
    .dropzone .tools {
        width: 30px;
        top: 0px;
        right: -30px;
    }
</style>
@stop
@section('content')
{{ Form::model($homePageContent,['id'=>'home_page_form','class' => 'form-horizontal','role' => 'form','files'=>'true']) }}
<div class="box box-success"><!-- Slide-1 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title1') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('bg_image_1')) echo ' has-error'; ?>">
            {{ Form::label('bg_image_1',trans('strings.backend.home.background_image'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($homePageContent)
                        <input type="file" name="bg_image_1" />
                        @else
                        <input type="file" name="bg_image_1" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('bg_image_1') }}</span>
                    </div>
                    @if($homePageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/homepage/'.$homePageContent['bg_image_1'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('heading_1')) echo ' has-error'; ?>">
            {{ Form::label('heading_1',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('heading_1', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.heading')]) }}
                <span class="help-block">{{ $errors->first('heading_1') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('subheading_1')) echo ' has-error'; ?>">
            {{ Form::label('subheading_1',trans('strings.backend.home.sub_heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('subheading_1', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.sub_heading')]) }}
                <span class="help-block">{{ $errors->first('subheading_1') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-2 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title2') }}</h3>
    </div>
    @if($homePageContent)
    <div class="box-body boxes">
        <?php foreach ($homePageContent['logoBox'] as $key => $logoBox) { ?>
            <div class="box box-default box-solid" id="box_{{$key+1}}">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('strings.backend.home.box') }} {{$key+1}}</h3>
                    <i class="fa fa-window-close pull-right deleteLogoBox" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        {{ Form::label('box_logo['.($key+1).']',trans('strings.backend.home.logo'), ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10 file">
                            <div class="dropzone box_logo_upload_<?php echo $key + 1; ?>" data-width="120" data-height="120" data-save="false" data-button-edit="true" data-image="{{URL::asset('img/backend/homepage/logobox/'.$logoBox['box_logo'])}}">
                                {{ Form::file('box_logo['.($key+1).']', null, ['class' => 'form-control']) }}
                            </div>
                            <input type="hidden" name="box_logo_old[{{$key+1}}]" value="{{$logoBox['box_logo']}}"/>
                            <input type="hidden" name="box_logo[{{$key+1}}][name]" />
                            <input type="hidden" name="box_logo[{{$key+1}}][data]" />
                        </div>
                    </div>
                    <div class="form-group <?php if ($errors->first('box_heading[' . ($key + 1) . ']')) echo ' has-error'; ?>">
                        {{ Form::label('box_heading['.($key+1).']',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            {{ Form::text('box_heading['.($key+1).']', $logoBox['box_heading'], ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.heading'),'required' => 'required']) }}
                            <span class="help-block">{{ $errors->first('box_heading['.($key+1).']') }}</span>
                        </div>
                    </div>
                    <div class="form-group <?php if ($errors->first('box_description[' . ($key + 1) . ']')) echo ' has-error'; ?>">
                        {{ Form::label('box_description['.($key+1).']',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
                        <div class="col-lg-10">
                            {{ Form::textarea('box_description['.($key+1).']', $logoBox['box_description'], ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description'),'required' => 'required']) }}
                            <span class="help-block">{{ $errors->first('box_description['.($key+1).']') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    @else
    <div class="box-body boxes">
        <div class="box box-default box-solid" id="box_1">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('strings.backend.home.box') }} 1</h3>
                <i class="fa fa-window-close pull-right deleteLogoBox" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>
            </div>
            <div class="box-body">
                <div class="form-group">
                    {{ Form::label('box_logo[1]',trans('strings.backend.home.logo'), ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10 file">
                        <div class="dropzone box_logo_upload_1" data-width="120" data-height="120" data-save="false" data-button-edit="true" data-image="">
                            <input type="file" name="box_logo[1]" class="form-control" required/>
                        </div>
                        <input type="hidden" name="box_logo[1][name]" />
                        <input type="hidden" name="box_logo[1][data]" />
                    </div>
                </div>
                <div class="form-group <?php if ($errors->first('box_heading[1]')) echo ' has-error'; ?>">
                    {{ Form::label('box_heading[1]',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::text('box_heading[1]', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.heading'),'required' => 'required']) }}
                        <span class="help-block">{{ $errors->first('box_heading[1]') }}</span>
                    </div>
                </div>
                <div class="form-group <?php if ($errors->first('box_description[1]')) echo ' has-error'; ?>">
                    {{ Form::label('box_description[1]',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
                    <div class="col-lg-10">
                        {{ Form::textarea('box_description[1]', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description'),'required' => 'required']) }}
                        <span class="help-block">{{ $errors->first('box_description[1]') }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="box-body text-center">
        <span class="btn btn-warning" id="add_more_boxes">{{ trans('strings.backend.home.add_more') }}</span>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-3 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title3') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('heading_3')) echo ' has-error'; ?>">
            {{ Form::label('heading_3',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('heading_3', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.heading')]) }}
                <span class="help-block">{{ $errors->first('heading_3') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('subheading_3')) echo ' has-error'; ?>">
            {{ Form::label('subheading_3',trans('strings.backend.home.sub_heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('subheading_3', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.sub_heading')]) }}
                <span class="help-block">{{ $errors->first('subheading_3') }}</span>
            </div>
        </div>
         <div class="form-group <?php if ($errors->first('image_3')) echo ' has-error'; ?>">
            {{ Form::label('image_3',trans('strings.backend.home.image'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($homePageContent)
                        <input type="file" name="image_3" />
                        @else
                        <input type="file" name="image_3" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('image_3') }}</span>
                    </div>
                    @if($homePageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/homepage/'.$homePageContent['image_3'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-4 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title4') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('image_4')) echo ' has-error'; ?>">
            {{ Form::label('image_4',trans('strings.backend.home.image'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($homePageContent)
                        <input type="file" name="image_4" />
                        @else
                        <input type="file" name="image_4" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('image_4') }}</span>
                    </div>
                    @if($homePageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/homepage/'.$homePageContent['image_4'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('heading_4')) echo ' has-error'; ?>">
            {{ Form::label('heading_4',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('heading_4', null, ['class' => 'form-control ckeditor', 'placeholder' => trans('strings.backend.home.heading')]) }}
                <span class="help-block">{{ $errors->first('heading_4') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('description_4')) echo ' has-error'; ?>">
            {{ Form::label('description_4',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('description_4', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description')]) }}
                <span class="help-block">{{ $errors->first('description_4') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('linktext_4')) echo ' has-error'; ?>">
            {{ Form::label('linktext_4',trans('strings.backend.home.link_text'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('linktext_4', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.link_text')]) }}
                <span class="help-block">{{ $errors->first('linktext_4') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-6 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title6') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('bg_image_6')) echo ' has-error'; ?>">
            {{ Form::label('bg_image_6',trans('strings.backend.home.background_image'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($homePageContent)
                        <input type="file" name="bg_image_6" />
                        @else
                        <input type="file" name="bg_image_6" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('bg_image_6') }}</span>
                    </div>
                    @if($homePageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/homepage/'.$homePageContent['bg_image_6'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('heading_6')) echo ' has-error'; ?>">
            {{ Form::label('heading_6',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('heading_6', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.heading')]) }}
                <span class="help-block">{{ $errors->first('heading_6') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('description_6')) echo ' has-error'; ?>">
            {{ Form::label('description_6',trans('strings.backend.home.description'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('description_6', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.description')]) }}
                <span class="help-block">{{ $errors->first('description_6') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('linktext_6')) echo ' has-error'; ?>">
            {{ Form::label('linktext_6',trans('strings.backend.home.link_text'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('linktext_6', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.link_text')]) }}
                <span class="help-block">{{ $errors->first('linktext_6') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-success"><!-- Slide-7 -->
    <div class="box-header with-border">
        <h3 class="box-title">{{ trans('strings.backend.home.slides.title7') }}</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('heading_7')) echo ' has-error'; ?>">
            {{ Form::label('heading_7',trans('strings.backend.home.heading'), ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('heading_7', null, ['class' => 'form-control', 'placeholder' => trans('strings.backend.home.heading')]) }}
                <span class="help-block">{{ $errors->first('heading_7') }}</span>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<!-- Home Page SEO -->
<div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">SEO</h3>
    </div>
    <div class="box-body">
        <div class="form-group <?php if ($errors->first('seo_title')) echo ' has-error'; ?>">
            {{ Form::label('seo_title','Title', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::text('seo_title', null, ['class' => 'form-control', 'placeholder' => 'Title']) }}
                <span class="help-block">{{ $errors->first('seo_title') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('seo_description')) echo ' has-error'; ?>">
            {{ Form::label('seo_description','Description', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                {{ Form::textarea('seo_description', null, ['class' => 'form-control', 'placeholder' => 'Description']) }}
                <span class="help-block">{{ $errors->first('seo_description') }}</span>
            </div>
        </div>
        <div class="form-group <?php if ($errors->first('seo_img')) echo ' has-error'; ?>">
            {{ Form::label('seo_img','Image', ['class' => 'col-lg-2 control-label']) }}
            <div class="col-lg-10">
                <div class="row">
                    <div class="col-lg-12">
                        @if($homePageContent)
                        <input type="file" name="seo_img" />
                        @else
                        <input type="file" name="seo_img" required/>
                        @endif
                        <span class="help-block">{{ $errors->first('seo_img') }}</span>
                    </div>
                    @if($homePageContent)
                    <div class="col-lg-12">
                        <img src="{{URL::asset('img/backend/homepage/'.$homePageContent['seo_img'])}}" style="max-width:50%; max-height:200px;"/>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div><!--box box-success-->

<div class="box box-info">
    <div class="box-body">
        <div class="pull-left">
            {{ link_to_route('admin.home', 'Undo Changes', [], ['class' => 'btn btn-danger']) }}
        </div><!--pull-left-->

        <div class="pull-right">
            {{ Form::submit('Save Changes', ['class' => 'btn btn-success submit_form']) }}
        </div><!--pull-right-->

        <div class="clearfix"></div>
    </div><!-- /.box-body -->
</div><!--box-->
{{ Form::close() }}
@endsection

@section('after-scripts')
{{ Html::script("js/backend/plugin/html5imageupload/html5imageupload.min.js") }}
{{ Html::script("js/backend/plugin/ckeditor/ckeditor.js") }}
<script>
    var boxCounter = $('.boxes').children().length;
<?php if (!($homePageContent)) { ?>
        imageUploader(1);
<?php } ?>

    $(function () {
        $('#home_page_form').on('click', '.deleteLogoBox', function () {
            $(this).closest('.box-solid').remove();
        });
    });

    $('#add_more_boxes').on('click', function (e) {
        e.preventDefault();
        boxCounter++;
        var boxToAppend = appendTemplate(boxCounter);
        $('.boxes').append(boxToAppend);
        imageUploader(boxCounter);
    });


    function imageUploader(counter) {
        $('.box_logo_upload_' + counter).html5imageupload({
            onSave: function (data) {
                var $this = $('.box_logo_upload_' + counter);
                $this.closest('.file').find('[name="box_logo[' + counter + '][name]"]').val(data.name);
                $this.closest('.file').find('[name="box_logo[' + counter + '][data]"]').val(data.data);
            }
        });
    }
<?php if ($homePageContent) { ?>
    <?php foreach ($homePageContent['logoBox'] as $key => $logoBox) { ?>
            imageUploader(<?php echo $key + 1; ?>);
    <?php } ?>
<?php } ?>

    function appendTemplate(counter) {
        return '<div class="box box-default box-solid" id="box_' + counter + '">\n\
                <div class="box-header with-border">\n\
                    <h3 class="box-title">{{ trans("strings.backend.home.box") }} ' + counter + '</h3>\n\
                    <i class="fa fa-window-close pull-right deleteLogoBox" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"></i>\n\
                </div>\n\
                <div class="box-body">\n\
                    <div class="form-group">\n\
                        <label for="box_logo[' + counter + ']" class="col-lg-2 control-label">{{trans("strings.backend.home.logo")}}</label>\n\
                        <div class="col-lg-10 file">\n\
                            <div class="dropzone box_logo_upload_' + counter + '" data-width="120" data-height="120" data-save="false" data-button-edit="true" data-image="">\n\
                                <input type="file" name="box_logo[' + counter + ']" class="form-control" required/>\n\
                            </div>\n\
                            <input type="hidden" name="box_logo[' + counter + '][name]" />\n\
                            <input type="hidden" name="box_logo[' + counter + '][data]" />\n\
                        </div>\n\
                    </div>\n\
                    <div class="form-group">\n\
                        <label for="box_heading[' + counter + ']" class="col-lg-2 control-label">{{trans("strings.backend.home.heading")}}</label>\n\
                        <div class="col-lg-10">\n\
                            <input type="text" name="box_heading[' + counter + ']" class="form-control" placeholder="{{trans("strings.backend.home.heading")}}" required/>\n\
                            <span class="help-block">{{ $errors->first("phone") }}</span>\n\
                    </div>\n\
                    </div>\n\
                    <div class="form-group">\n\
                        <label for="box_description[' + counter + ']" class="col-lg-2 control-label">{{trans("strings.backend.home.description")}}</label>\n\
                        <div class="col-lg-10">\n\
                            <textarea name="box_description[' + counter + ']" cols="50" rows="10" class="form-control" placeholder="{{trans("strings.backend.home.description")}}" required></textarea>\n\
                            <span class="help-block">{{ $errors->first("phone") }}</span>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>';
    }
</script>
@endsection