@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/finish-account.css')) }}   
@stop
@section('content')
<div class="wrapper">
    <div class="col-sm-12 header-bar">
        <div class="container">
            <img src="img/twpm-blue-logo.png" class="logo" alt="">
            <p>Risk Tolerance</p>
        </div>
    </div>
    <div class="container-fluid wrapper">
        <div class="container">
            <div class="step">
                <p>Step2: More about you</p>
                <div class="blue-line col-sm-4">
                </div>
                <div class="grey-line col-sm-8">
                </div>
            </div>
            <div class="container-first">

                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1 sections ">
                        <div class="col-sm-6 section-left">
                            <h2>More about you</h2>
                            <p>Because of the sensitive nature of information being provided, we need to gather a bit more information about you.  </p>
                        </div>
                        {{ Form::open(['route' => 'frontend.client.saveIdentityVerification']) }}
                        <div class="col-sm-6 section-right">
                            <div class="col-sm-8 inner-left">   
                                <label> Date of birth </label>
                                {{ Form::input('date','dob', null,['class' =>'form-control']) }}
                                @if($errors->has('dob'))
                                <div class="alert alert-danger">{{ $errors->first('dob') }}</div>
                                @endif
                            </div>
                            <div class="col-sm-8 inner-left">
                                <p class="left-subheading"><b>Gender</b></p> 

                                <label class="radio-custom-label">
                                    female
                                    <input type="radio" name="sex" value="2">
                                    <span class="radio-icon"></span>
                                </label>
                                <label class="radio-custom-label">
                                    male
                                    <input type="radio" name="sex" value="1" class="male">
                                    <span class="radio-icon"></span>
                                </label>
                                @if($errors->has('sex'))
                                <div class="alert alert-danger">{{ $errors->first('sex') }}</div>
                                @endif
                            </div>

                            <div class="site-heading-1"> 
                                <div class="next-page align-left col-sm-8">
                                    <button type="submit" class="btn btn-info ">Continue</button>
                                    <!--<a> <p class="col-sm-8">Save and return later</p> </a>-->
                                </div>
                            </div>
                        </div> 
                        {{ Form::close() }} 
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @include('frontend.includes.footer')
        </div> 
    </div>
</div>



@endsection