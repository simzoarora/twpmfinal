@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; Basic Information 
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')

<div class="client">
    @include('frontend.includes.client_register_header')

    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 1: Basic Information</p>
            <div class="col-sm-3 left-bottom-border"></div> 
            <div class="col-sm-9 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Basic Information</h2>
                <p class='heading-info'>We use this information to create your account</p>

                {{ Form::open(['route' =>'frontend.client.stepOneRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                {{ Form::hidden('serviceId',$serviceId) }}
                <div class="form-group" id='basic-information'>
                    <div class='basic-information-content'>
                        <span class="input input--hoshi <?php
                        if ($errors->first('email')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::email('email', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Email Address</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                    </div>
                    <div class="password-head">
                        <span class="input input--hoshi <?php
                        if ($errors->first('password')) {
                            echo 'input-error';
                        }
                        ?>">
                            <input class="input__field input__field--hoshi" type="password" name="password" required>
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Password(longer password are better)</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button> 
                    </div>
                    <div class="terms-services">
                        <i>By joining, you agree to the<a href="#"> terms of service.</i></a>
                    </div>   
                </div>
                <div class="terms-services">
                    <i>Already have an account ? <a href="{{ route('frontend.client.login', [config('constant.subdomain'),$serviceId]) }}"> Sign In Here.</i></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/step-one.js') }}"></script>
@stop