@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop 

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
@stop

@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content client-resources">
                <?php $page_title = 'Resources'; ?>
                @if(Session::has('confirmation_message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">     {!! session()->get('confirmation_message')!!}      </p>
                @endif
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    <?php
                    if (!$resourceCategory->isEmpty()) {
                        foreach ($resourceCategory as $category) {
                            ?>
                            <a href="{{ route('frontend.client.resources', [config('constant.subdomain'), $category->id]) }}">
                                <div class="col-sm-3 service-outer">
                                    <div class="service-name">
                                        <p><?php echo $category->title; ?></p>
                                    </div>
                                </div>
                            </a>
                            <?php
                        }
                    } else {
                        ?>
                        <div>No resource categories present.</div>
                    <?php }
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop