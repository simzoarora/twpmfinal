@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop 

@section('after-styles') 
{{ Html::style(asset('css/finish-account.css')) }} 
@stop
@section('content')

<div class="wrapper">
    <div class="col-sm-12 header-bar">
        <div class="container">
            {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }}
            <p>Finish Setup</p>
        </div>
    </div>
    <div class="container-fluid wrapper">
        <div class="container">
            <div class="step">
                <p>Step1: Contact Information </p>
                <div class="blue-line col-sm-2">
                </div>
                <div class="grey-line col-sm-10">
                </div>
                <div class="container-first">
                </div>
            </div>


            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 sections">
                    <div class="col-sm-6 section-left">
                        <h2> Contact Information </h2>
                        <p>Enter your email address and choose a password, so you can <br> review this plan or change it at later date.</p>
                    </div> 
                    {{ Form::open(['route' => 'frontend.client.saveContactInfo' , 'id'=>'contact-form']) }}
                    <div class="col-sm-6 section-right">
                        <div class="col-sm-8 inner-left">
                            {{ Form::label('first_name', 'First name') }}
                            {{ Form::input('text','first_name', null, ['class'=> 'form-control']) }}
                            <!--<div>{{ $errors->has('first_name') }}</div>-->
                            @if($errors->has('first_name'))
                            <div class="alert alert-danger">{{ $errors->first('first_name') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-4 inner-right">
                            {{ Form::label('middle_name', 'MI') }}
                            {{ Form::input('text','middle_name', null, ['class'=> 'form-control']) }}
                            @if($errors->has('middle_name'))
                            <div class="alert alert-danger">{{ $errors->first('middle_name') }}</div>
                            @endif
                        </div>

                        <div class="col-sm-8 inner-left">
                            {{ Form::label('last_name', 'Last name') }}
                            {{ Form::input('text','last_name', null, ['class'=> 'form-control']) }}
                            @if($errors->has('last_name'))
                            <div class="alert alert-danger">{{ $errors->first('last_name') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-8 inner-left">
                            {{ Form::label('first_address', 'Address Line 1') }}
                            {{ Form::input('text','first_address', null, ['class'=> 'form-control']) }}
                            @if($errors->has('first_address'))
                            <div class="alert alert-danger">{{ $errors->first('first_address') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-8 inner-left">
                            {{ Form::label('last_address', 'Address Line 2') }}
                            {{ Form::input('text','last_address', null, ['class'=> 'form-control']) }}
<!--                            @if($errors->has('last_address'))
                            <div class="alert alert-danger">{{ $errors->first('last_address') }}</div>
                            @endif-->
                        </div>
                        <div class="col-sm-8 inner-left">  
                            {{ Form::label('city', 'City') }}
                            {{ Form::input('text','city', null,['class'=> 'form-control']) }}
                            @if($errors->has('city'))
                            <div class="alert alert-danger">{{ $errors->first('city') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-4 inner-right"> 
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                            {{ Form::label('state_id', 'State') }}
                            {{ Form::select('state_id', $states) }} 
                        </div> 
                        <div class="col-sm-8 inner-left">
                            {{ Form::label('zip', 'Zip code') }}
                            {{ Form::input('text','zip', null,['class'=> 'form-control']) }}
                            @if($errors->has('zip'))
                            <div class="alert alert-danger">{{ $errors->first('zip') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-8 inner-left">
                            {{ Form::label('phone_number', 'Phone number') }}
                            {{ Form::input('number','phone_number', null, ['id'=> 'phone_number', 'class'=> 'form-control']) }}
                            @if($errors->has('phone_number'))
                            <div class="alert alert-danger">{{ $errors->first('phone_number') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-4 inner-right"> 
                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                            {{ Form::label('phone_type', 'Type') }}
                            {{ Form::select('phone_type', config('constant.phone_type_inverse'),1, ['id' => 'type', 'class'=> 'form-control']) }}
                        </div>
                        <div class="checkbox hide">
                            <ul>
                                <li>
                                    <label class="col-sm-8 checkbox-custom-label authentication">
                                        {{ Form::input("checkbox",'name', null, ['id' => 'two-factor-authentication']) }}
                                        <span class="checkbox-icon"></span>
                                        <p>Enable two-factor authentication</p>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="one-time-password"></div>
                        <div class="site-heading-1"> 
                            <div class="next-page align-left col-sm-8">

                                <button type="submit" class="btn btn-info ">Continue</button>

                            <!--<a> <p class="col-sm-8">Save and return later</p> </a>-->
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}    
                </div>
            </div>
            <div class="container">
                @include('frontend.includes.footer')
            </div> 
        </div> 
    </div> 
</div> 
@section('after-scripts')
{{ Html::script(elixir('js/contact-information.js')) }} 
@endsection