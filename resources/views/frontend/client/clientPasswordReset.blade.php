@extends('frontend.layouts.client')

@section('title')
Client Login
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    <div id="body-content" class="container-fluid">
        <div class="container">
            <div class="inner-content">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
                
                <h2 class='content-heading' style="color: #030303">{{ trans('labels.frontend.passwords.reset_password_box_title') }}</h2>

                {{ Form::open(['route' =>'frontend.auth.password.reset','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                 <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group" id='basic-information'>
                    <div class='basic-information-content'>
                        <span class="input input--hoshi <?php
                        if ($errors->first('email')) {
                            echo 'input-error';
                        }
                        ?>">
                            <p style="margin-bottom: 20px;"></p>
                            <input class="input__field input__field--hoshi" type="text" name="email" value ="{{ $email }}" required disabled="disabled">
                            {{ Form::input('hidden', 'email', $email) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">{{ trans('validation.attributes.frontend.email') }}</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                    </div>
                    <div class="password-head" style="margin-bottom: 5px;">
                        <span class="input input--hoshi <?php
                        if ($errors->first('password')) {
                            echo 'input-error';
                        }
                        ?>">
                            <input class="input__field input__field--hoshi" type="password" name="password" required>
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">{{ trans('validation.attributes.frontend.password') }}</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
                    </div>
                    <div class="password-head" style="margin-bottom: 40px;">
                        <span class="input input--hoshi <?php
                        if ($errors->first('password')) {
                            echo 'input-error';
                        }
                        ?>">
                            <input class="input__field input__field--hoshi" type="password" name="password_confirmation" required>
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">{{ trans('validation.attributes.frontend.password_confirmation') }}</span>
                            </label>
                        </span>
                    </div>

                    <div class="submit-btn" id='button-color'>
                        {{ Form::submit(trans('labels.frontend.passwords.reset_password_button'), ['class' => 'btn contact-info-button']) }}
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts') 
<script src="{{ asset('js/client-login.js') }}"></script>
@stop