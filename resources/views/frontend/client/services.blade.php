@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/servicesQuestion.css')) }}

@stop   
@section('content')  

<div class="dashboard wrapper">
    <div id="dashboard-content">
        <div class="container-fluid get-started">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                <?php $page_title = 'Welcome ' . Session::get('loggedInUserName'); ?>
                @if(Session::has('confirmation_message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">     {!! session()->get('confirmation_message')!!}      </p>
                @endif
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">

                    <p class="grey-active-service">
                        <a href="#" id="your-services">My Services</a>
                        <span><a href= "#" id="all-services" class="active">All services</span></a>
                    </p>
                    <div class="row service-name all-services">
                        <div class="previous"> </div>
                        <div class="service-options owl-carousel"> 
                            <?php
                            if (isset($recommendedServices)) {
                                foreach ($recommendedServices as $service) { 
                                    ?>
                                    <div class='col-sm-12 products-list serviceList' data-id="{{$service['id']}}" data-service-is-availed="<?php echo ($service['service_users'] && $service['service_users'][0] && $service['service_users'][0]['status'] == 1) ? 1 : 0 ?>">
                                        <div class="nested-div">
                                            <div class="position-absolute">
                                                <?php
                                                if ($service['recommended'] == 1) {
                                                    ?>
                                                    <i>Recommended</i>
                                                <?php }
                                                ?>
                                                <p><?php echo $service['title']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class='col-sm-12 products-list empty'>
                                    <div class="nested-div">
                                        <div class="position-absolute">
                                            <p>No services present.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="next"> </div>
                    </div>
                    <div class="row service-name your-services">
                        <div class="previous"> </div>
                        <div class="service-options owl-carousel"> 
                            <?php
                            if ($activeServices->services->count()) {
                                foreach ($activeServices->services as $service) {
                                    ?>
                                    <div class='col-sm-12 active-list serviceList' data-id="{{$service->id}}" data-service-is-availed="<?php echo ($service->serviceUsers && $service->serviceUsers[0] && $service->serviceUsers[0]['status'] == 1) ? 1 : 0 ?>">
                                        <div class="nested-div">
                                            <div class="position-absolute">
                                                <i>Active</i>
                                                <p><?php echo $service->title; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class='col-sm-12 active-list empty'>
                                    <div class="nested-div">
                                        <div class="position-absolute">
                                            <p>No active services.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="next"> </div>
                    </div>

                    <div class="row open-service-name rec-services">
                        <div class="col-sm-12"> 
                            <div class="row"> 
                                <div class="left-div">  <div class="service-preview-file" style="display: none;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        if (isset($recommendedServices)) {
                            foreach ($recommendedServices as $key => $service) {
                                ?>
                                                                                                                                                                                        <!--<div class="col-sm-12 recommended-service" data-id="{{$service['id']}}" style="<?php echo $key ? 'display: none;' : ''; ?>">--> 
                                <div class="col-sm-12 recommended-service" data-id="{{$service['id']}}" style="display: none"> 
                                    <div class="row"> 
                                        <div class="left-div">
                                            <div class="recommended-service-div">
                                                <?php
                                                if ($service['recommended'] == 1) {
                                                    ?>  
                                                    <i>Recommended</i>
                                                <?php }
                                                ?>
                                                <h2><?php echo $service['title']; ?></h2>
                                                <h5>Description</h5>  
                                                <p><p><?php echo $service['description']; ?></p></p>
                                                <h5>Recommended for</h5>  
                                                <p><?php echo $service['recommended_for']; ?></p>
                                                <h5>Cost</h5>  
                                                <p><?php echo $service['cost']; ?></p>                      
                                                <a href="{{ route('frontend.client.servicesQuestion', [config('constant.subdomain'), $service['id']]) }}" class="btn btn-blue get-started-btn">Get Started</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="row open-service-name active-services">
                        <?php
                        if ($activeServices->services->count()) {
                            foreach ($activeServices->services as $key => $service) {
                                ?>
                                <div class="col-sm-12 recommended-service" data-id="{{$service->id}}" style="display: none;">
                                    <div class="col-sm-6 left-div">
                                        <div class="pending-items">
                                            <h2>Pending Items</h2>
                                            <?php if (!$submittedDocuments->isEmpty()) { ?>
                                                <ul>
                                                    <?php foreach ($submittedDocuments as $documentAnswer) { ?>
                                                        <li>
                                                            <a href="#" class="docusign-send" data-url="{{ route('frontend.client.docusign', [config('constant.subdomain'), $documentAnswer->attempted_question_id]) }}">
                                                                <?php echo basename($documentAnswer->answer); ?>
                                                            </a>
                                                            <span class="docu-sign-message">Need Digital Signature</span>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } else { ?>
                                                <ul>
                                                    <li>
                                                        <span class="docu-sign-message">No pending items left.</span>
                                                    </li>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                        <?php
                                        if (!$activeServices->services->isEmpty()) {
                                            ?>
                                            <div class="documents-list">
                                                <div class="border-bottom service-all-docu">
                                                    <h2>All Documents</h2>
                                                    <?php
                                                    foreach ($activeServices->services as $service) {
                                                        foreach ($service->questions as $question) {
                                                            if ($question['type'] == config('constant.service_question_types_options_inverse.usl')) {
                                                                foreach ($service->attemptedQuestions as $attemptedQuestion) {
                                                                    if ($attemptedQuestion['question_id'] == $question['id']) {
                                                                        ?>
                                                                        <a href="#">
                                                                            <span class="red-box">
                                                                                <?php
                                                                                echo pathinfo($attemptedQuestion->attemptedQuestionAnswer->answer, PATHINFO_EXTENSION);
                                                                                ?>
                                                                            </span>
                                                                            <span class="file-name">
                                                                                <?php
                                                                                echo basename($attemptedQuestion->attemptedQuestionAnswer->answer);
                                                                                ?>
                                                                            </span>
                                                                        </a>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                    <span class="file-name no-doc-message">This service has no documents.</span>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div> 
                                    <div class="col-sm-6 right-div">
                                        <div class="que-ans-list">
                                            {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11  col-sm-offset-1 col-xs-11 col-xs-offset-1']) }}
                                            @foreach($service->questions as $key2=>$singleQuestion)
                                            @if($singleQuestion->type == config('constant.service_question_types_options_inverse.mla'))
                                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading" role="tab" id="headingOne">
                                                        <div class='get-started'>
                                                            <h2 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $singleQuestion->id }}" aria-expanded="true" aria-controls="collapseOne">
                                                                    <?php
                                                                    foreach ($activeServices->services as $service) {
                                                                        
                                                                    }
                                                                    ?>
                                                                    {{$service->title}}
                                                                </a>
                                                            </h2>
                                                        </div>
                                                    </div>
                                                    <div id="collapse{{ $singleQuestion->id }}" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">

                                                            <div class="wrapper">
                                                                <div class="container-fluid wrapper">
                                                                    <?php
                                                                    foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                                                        ?>
                                                                        <fieldset>
                                                                            {{ Form::input('hidden','serviceId',$service->id) }}
                                                                            <section class="sections">
                                                                                <div class="col-sm-12 section-left p-0">
                                                                                    <h2> {{$singleQuestion->question}} </h2>
                                                                                    <p> {{$singleQuestion->description}}</p>
                                                                                </div>
                                                                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                                                                <?php
                                                                                $inputs = json_decode($singleInput->inputs);
                                                                                ?>
                                                                                <?php
                                                                                foreach ($service->attemptedQuestions as $que_ans) {
                                                                                    if ($que_ans->attemptedQuestionAnswer) {
                                                                                        if ($que_ans->question_id == $singleQuestion->id) {
                                                                                            $answer = json_decode($que_ans->attemptedQuestionAnswer->answer, true);
                                                                                        }
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <div class=" col-sm-12 <?php echo ($singleInput->count == 1) ? 'section-right' : 'horizontal'; ?> p-0">
                                                                                    <?php if ($singleInput->count == 1) { ?>
                                                                                        <?php foreach ($inputs as $key => $input) { ?>
                                                                                            <div class="col-sm-12 inner-left p-0">
                                                                                                {{ Form::label('label', $input->description) }}<br>
                                                                                                {{ Form::input('text','data['.$key2.'][answer]['.$input->description.']',$answer[$input->description],['data-validation'=> $input->validation , 'class'=>'custom-validation', 'data-rule-regex'=>"false", 'required'=>true, 'readonly'=>true]) }}
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                    <?php
                                                                                    if ($singleInput->count >= 2):
                                                                                        foreach (json_decode($singleInput->inputs) as $questionInput):
                                                                                            ?>
                                                                                            <div class=" col-sm-6 headings p-0">
                                                                                                <p class="input-title">{{$questionInput->description }}</p>
                                                                                            </div>
                                                                                            <?php
                                                                                        endforeach;
                                                                                        ?>
                                                                                        <?php foreach ($answer as $key => $listInput) { ?>
                                                                                            <div class="col-sm-12 p-0">
                                                                                                <?php foreach ($listInput as $inputName => $inputValue) { ?>
                                                                                                    {{ Form::input('text','data['.$key2.'][answer]['.$key.']['.$inputName.']',$inputValue,['required' => true, 'readonly'=>true]) }}
                                                                                                <?php } ?>
                                                                                            </div>
                                                                                            <?php
                                                                                        }
                                                                                    endif;
                                                                                    ?>
                                                                                </div>
                                                                            </section>
                                                                        </fieldset>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endforeach
                                            {{ Form::close() }}

                                            <!--<button type="submit" class="btn btn-submit ">Submit</button>-->
                                        </div>
                                    </div>  
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>   


<!-- recommended services modal -->
<div id="recommended-services-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Welcome</h4>
            </div>
            <div class="modal-body">
                <p><span>Welcome to your dashboard. Based on the information you provided, we've made some service recommendations to get you started.
                    </span>
                </p> 
            </div>
        </div>

    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/employee.js') }}"></script>

<script>
var userFirstTime = <?php echo ($user_first_time == true ? $user_first_time : 'false'); ?>;
var clientId = <?php echo Auth::user()->id; ?>;
var serviceIdsArray = <?php echo json_encode(config('constant.services_ids')) ?>;

</script>
<script src="{{ asset('js/recommended-services.js') }}"></script> 

<script>
$(document).ready(function () {
    var url = window.location.href;
    var isLastSlash = (url[url.length - 1] == "/") ? true : false;
    var url = url.split("/");
    var selectedServiceId = url[url.length - (isLastSlash ? 2 : 1)];
    if (!isNaN(selectedServiceId) && jQuery.inArray(selectedServiceId, serviceIdsArray) === -1)
    {
        window.location.href = window.location.origin;
    }
    if (isNaN(selectedServiceId)) {
        selectedServiceId = '<?php echo $recommendedServices[0]["id"] ?>';
    }

    var comprehensive_life_insurance_id = '<?php echo config('constant.questions.comprehensive_life_insurance_id') ?>';
    var comprehensive_spouse_life_insurance_id = '<?php echo config('constant.questions.comprehensive_spouse_life_insurance_id') ?>';
    var stockServiceId = '<?php echo config('constant.questions.stock_service_id') ?>';
    var annualServivceId = '<?php echo config('constant.questions.annual_service_id') ?>';
    var semiAnnualServiceId = '<?php echo config('constant.questions.semi_annual_service_id') ?>';
    var lifeInsuranceServiceId = '<?php echo config('constant.questions.life_insurance_service_id') ?>';
    var comprehensive_life_insurance_ids = [comprehensive_life_insurance_id, comprehensive_spouse_life_insurance_id];
    var your_liabilities = '<?php echo config('constant.questions.your_liabilities') ?>';
    var incomeTaxTopicId = "{{config('constant.questions.income_tax_topic_id') }}";
    var userName = "{{ Session::get('loggedInUserName')}}";
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";



    function serviceClick(clientId, serviceId, doNotAnimate = false) {
        $.ajax({
            type: 'get',
            url: "/getTopicsOrQuestions/" + serviceId + "/" + clientId,
            success: function (data) {
                if (data) {
                    $('.service-preview-file').show();

                    $('.service-preview-file').html(data);
                    if ($('.service-preview-file').find('.serviceQuestions').length == 1) {
                        if (doNotAnimate) {
                            $('.serviceQuestions').css({left: '0'});
                        } else {
                            $('.serviceQuestions').animate({left: '0'});
                        }
                    }
                    fixListHeight();
                    showHideQuestions();
                }

            }
        })
    }

//        function onServiceClick() {
//
//            $('.products-list').on('click', function () {
//                var serviceId = $(this).attr('data-id');
//                serviceClick(clientId, serviceId);
//                fixListHeight()
//            });
//        }
    function onServiceClick() {
        $('.products-list , .active-list').on('click', function () {
            var serviceId = $(this).attr('data-id');
            var $this = $(this);
            $this.addClass('selected').parent().siblings().find('.selected').removeClass('selected');
            $('.recommended-service').hide();
            if ($this.attr('data-service-is-availed') == 1) {
                $('.recommended-service').hide();
                serviceClick(clientId, serviceId);
                fixListHeight()
            } else {
                $('.service-preview-file').hide();
                $('.rec-services .recommended-service[data-id=' + $this.attr('data-id') + ']').show();

            }
        });
    }
    function onTopicClick() {
        $(document).on('click', '.single-topic', function (e) {
            e.stopImmediatePropagation();
            if ($('.pre-topic-questions').find('.active-inputs').length > 0) {
                $('.pre-topic-questions').find('.active-inputs').hide();
                $('.pre-topic-questions').find('.updateAnswers').hide();
            }
            var question = $(this).attr('data-question');
            var serviceId = $('.service-name:visible').find('.serviceList.selected').attr('data-id');
            var topicId = $(this).attr('data-topic');
//                var clientId = $('#client-id').val();
            var topicName = $(this).attr('data-topic-name');
            if ($(this).attr('data-documents')) {
                var url = "/getServiceDocuments/" + serviceId + "/" + clientId;
            } else {
                var url = "/getSubTopicsOrQuestions/" + serviceId + "/" + clientId + "/" + topicId;
            }
            TopicClick(serviceId, topicId, clientId, topicName, url, $(this));
        });
    }

    function TopicClick(serviceId, topicId, clientId, topicName, url, $this) {

        $.ajax({
            type: "post",
            data: {topicName: topicName},
            url: url,
            success: function (data) {
                if (data) {
                    $('.service-preview-file .serviceSubTopics').remove();
                    $('.service-preview-file .serviceQuestions').remove();
                    $('.service-preview-file').append(data);
                    if (!$('.TaxableIncome').is(':visible') && $('.HouseHoldIncome').is(':visible')) {
                        var houseHoldIncome = $('.HouseHoldIncome').val();
                        getTaxBracket(houseHoldIncome, $this);
                    } else if ($('.TaxableIncome').is(':visible')) {
                        var taxableIncome = $('.TaxableIncome').val();
                        getTaxBracket(taxableIncome, $this);
                    }
                    if ($('.service-preview-file').find('.serviceQuestions').length == 1) {
                        $('.serviceQuestions').animate({left: '0'});
                        $('.serviceTopicsWrapper').hide();
                        if (topicId == incomeTaxTopicId) {
                            if ($('tr.fileUploadRow').length != 0) {
                                $('.add-file-btn').hide();
                            }
                        }
                        $(document).off('click', '.back-to-sub-topics');
                        $(document).on('click', '.back-to-sub-topics', function () {
                            backToTopicsSubTopics();
                            return false;
                        });

                    } else {
                        showSlide();
                        hideSlide();
                    }
                    $('.preTopicQuestions').hide();
                    fixListHeight();
                    showHideQuestions();
                }
            }
            ,
            error: function (err) {
            }
        });
    }
    function backToTopicsSubTopics(divToBeShown = false) {
        if ($('.pre-topic-questions').find('.active-inputs').length > 0) {
            $('.pre-topic-questions').find('.active-inputs').show();
        }
        $('.serviceQuestions').animate({left: '200%'});
        if (divToBeShown) {
            $('.' + divToBeShown).show(300);
        } else {
            $('.serviceTopicsWrapper').show(300);
        }
        $('.preTopicQuestions').show();
    }
    function onSubTopicClick() {
        $(document).on('click', '.single-sub-topic', function (e) {
            e.stopImmediatePropagation();
            var copyTabCurrent = $(this);
            var serviceId = $('.products-list.selected').attr('data-id');
            var topicId = $(this).attr('data-topic');
//                var clientId = $('#client-id').val();
            var topicName = $(this).attr('data-topic-name');
            var copyIndex = $(this).attr('data-copy-index');
            var displayType = $(this).attr('data-display-type');
            subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType);
        });
    }
    function subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, doNotAnimate = false, divToBeShown = false) {
        var url = "/getQuestionsOfTopicOrSubTopic/" + serviceId + "/" + clientId + "/" + topicId;
        $.ajax({
            type: "post",
            data: {topicName: topicName, copyIndex: copyIndex, displayType: displayType},
            url: url,
            success: function (data) {
                if (data) {
                    $('.service-preview-file .serviceQuestions').remove();
                    $('.serviceTopics').append(data);
                    $(".copy-tabs[data-copy-index='" + copyIndex + "']").addClass('selected-tab').siblings().removeClass('selected-tab');
                    $(".main-tabset-item[data-display-type='" + displayType + "']").addClass('active').siblings().removeClass('active');
                    var isCopy = false;
                    if (copyTabCurrent.hasClass('copy-tabs')) {
                        isCopy = true;
                    }
                    showQuestionsSlide(isCopy, doNotAnimate);
                    hideQuestionsSlide(isCopy, doNotAnimate, divToBeShown);
                    fixListHeight();
                }
            }
            ,
            error: function (err) {
            }
        });
    }
    function showSlide() {
        $('.serviceSubTopics').animate({left: '0'});
        $('.serviceTopicsWrapper').hide();
    }
    function hideSlide() {
        $(document).off('click', '.back-to-topics');
        $(document).on('click', '.back-to-topics', function () {
            $('.service-preview-file .serviceSubTopics').animate({left: '200%'});
            $('.serviceTopicsWrapper').show(500);
            $('.preTopicQuestions').show();
        });
        showHideQuestions()
    }
    function showQuestionsSlide(isCopy = false, doNotAnimate = false) {
        if (!isCopy && !doNotAnimate) {
            $('.serviceQuestions').animate({left: '0'});
        } else {
            $('.serviceQuestions').css({left: '0'});
        }
        $('.serviceSubTopics').hide(300);
        fixListHeight()
        showHideQuestions()
    }
    function hideQuestionsSlide(isCopy = false, doNotAnimate = false, divToBeShown = false) {
        $(document).off('click', '.back-to-sub-topics');
        $(document).on('click', '.back-to-sub-topics', function () {
            if (doNotAnimate) {
                backToTopicsSubTopics(divToBeShown);
            } else {
                if (!isCopy) {
                    $('.serviceQuestions').animate({left: '200%'});
                } else {
                    $('.serviceQuestions').css({left: '200%'});
                }
                $('.serviceSubTopics').show(300);
                $('.serviceTopicsWrapper').hide();
//                $('.serviceTopics').show();  
            }
            fixListHeight();
        });
    }
    function showSubQuestionsSlide() {
        $('.serviceModalQuestions').animate({left: '0'});
        $('.serviceQuestions').hide(300);
        showHideQuestions();
        fixListHeight()
    }
    function hideSubQuestionsSlide(copyIndex = false) {
        $(document).off('click', '.back-to-questions');
        $(document).on('click', '.back-to-questions', function () {
            $('.serviceModalQuestions').animate({left: '200%'});
            $('.serviceSubTopics').hide();
            var serviceId = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-service-id');
            var topicId = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-topic-id');
            var copyIndex = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-previous-slide-copy-index');
            var displayType = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-display-type');
            if (!copyIndex) {
                var copyIndex = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-copy-index');
            }
            var topicIsChild = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-topic-is-child');
//                var clientId = $('#client-id').val();
            if (topicId) {


                if (topicIsChild == '1') {
                    var copyTabCurrent = $('.single-sub-topic[data-topic="' + topicId + '"]');
                    var divToBeShown = 'serviceSubTopics';
                } else {

                    var copyTabCurrent = $('.single-topic[data-topic="' + topicId + '"]');
                    var divToBeShown = 'serviceTopicsWrapper';
                }
                var topicName = copyTabCurrent.attr('data-topic-name');
                if (!copyIndex) {
                    var copyIndex = copyTabCurrent.attr('data-copy-index');
                }
                if (!displayType) {
                    var displayType = copyTabCurrent.attr('data-display-type');
                }
                subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, 'doNotAnimate', divToBeShown);
                fixListHeight();
            } else {
                serviceClick(clientId, serviceId, 'doNotAnimate');
            }
        });
    }
    function onModalClick() {

        $(document).on('click', '.showModalQuestions', function (e) {
            $(this).closest('.service-preview').find('.active-inputs').show();
            $(this).closest('.service-preview').find('.updateAnswers').hide();
            e.stopImmediatePropagation();
            var serviceId = $(this).parent().attr('data-service-id');
            var topicId = $(this).parent().attr('data-topic-id');
            var answerIndex = $(this).parent().attr('data-index');
            var questionId = $(this).parent().attr('data-question-id');
            var copyIndex = $(this).parent().attr('data-copy-index');
            var data_previous_slide_copy_index = $(this).parent().attr('data-previous-slide-copy-index');
            var displayType = $(this).parent().attr('data-display-type');
//                var clientId = $('#client-id').val();
            var url = "/getDependentQuestions/" + serviceId + "/" + clientId;
            if (topicId) {
                var url = "/getDependentQuestions/" + serviceId + "/" + clientId + "/" + topicId;
            }
            $.ajax({
                type: 'post',
                url: url,
                data: {answerIndex: answerIndex, questionId: questionId, copyIndex: copyIndex, data_previous_slide_copy_index: data_previous_slide_copy_index, displayType: displayType},
                success: function (data) {
                    if (data) {
                        $('.service-preview-file .serviceModalQuestions').remove();
                        if ($('.service-preview-file').find('.serviceTopics').length != 1) {
                            $('.service-preview-file').append(data);
                        } else {
                            $('.serviceTopics').append(data);
                        }
                        // Init datepicker
                        $('.question_datepicker').datetimepicker({
                            format: 'YYYY-MM-DD',
                            useCurrent: true,
                        })
                        // Init yearpicker
                        $(".question_yearpicker").datetimepicker({
                            viewMode: "years",
                            format: "YYYY",
                            useCurrent: true,
                        });

                        showSubQuestionsSlide();
                        hideSubQuestionsSlide(copyIndex);
                        showHideQuestions();
                        fixListHeight()

                        $(this).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                            let symbols = $(element).attr('data-symbols');
                            let symbolsArray = symbols.split(',');
                            let inputValue = $(element).find('input').val();
                            if (symbolsArray.length > 1) {
                                if (inputValue) {
                                    var symbolValue = symbolsArray[0]
                                    var newInputValue = ''
                                }
                                let blank = '';
                                let selected = 'selected="selected"';
                                const selectedFirst = symbolValue == symbolsArray[0] ? selected : blank;
                                const selectedSecond = symbolValue == symbolsArray[1] ? selected : blank;
                                let newHtml = '<div class="symbol-elements" style="display: flex">' +
                                        '<input type="text" class="new-input activeInputs"  value="' + newInputValue + '" required>' +
                                        '<select class="new-symbol-type activeInputs">' +
                                        '<option value="' + symbolsArray[0] + '" ' + selectedFirst + '>' + symbolsArray[0] + '</option>' +
                                        '<option value="' + symbolsArray[1] + '" ' + selectedSecond + '>' + symbolsArray[1] + '</option>' +
                                        '<select>' +
                                        '</div>'
                                $(element).find('input').hide();
                                $(element).find('.answer').append(newHtml);
                            }
                        })
                    }
                }
            })
        });
    }
    function onDeleteModalClick() {

        $(document).on('click', '.deleteModal', function (e) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Modal Data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $(this).closest('.service-preview').find('.active-inputs').show();
                    $(this).closest('.service-preview').find('.updateAnswers').hide();
                    var current_element = $(this);
                    e.stopImmediatePropagation();
                    var serviceId = $(this).parent().attr('data-service-id');
                    var topicId = $(this).parent().attr('data-topic-id');
                    var answerIndex = $(this).parent().attr('data-index');
                    var questionId = $(this).parent().attr('data-question-id');
                    var copyIndex = $(this).parent().attr('data-copy-index');
//                        var clientId = $('#client-id').val();
                    var url = "/deleteAnswers/" + serviceId + "/" + clientId;
                    if (topicId) {
                        var url = "/deleteAnswers/" + serviceId + "/" + clientId + "/" + topicId;
                    }
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {answerIndex: answerIndex, questionId: questionId, copyIndex: copyIndex},
                        success: function (data) {
                            if (data.success == true) {
                                current_element.parent().remove();
                                $('tr[data-question-id="' + questionId + '"]').each(function (index, childElement) {
                                    $(childElement).attr('data-index', index); // update table rows indexes
                                })
                                $('tr[data-question-id="' + questionId + '"]')
                                        .closest('.modal-table')
                                        .find('.addNewModal')
                                        .parent().attr('data-index', $('tr[data-question-id="' + questionId + '"]').length + '_new');
                            } else {
                                toastr.warning('Error Occured');
                            }
                        }
                    })
                } else {
                    swal("Your information is safe!");
                }
            });
        });
    }


    onServiceClick();
    onTopicClick();
    onSubTopicClick();
    onModalClick();
    onDeleteModalClick();
    fixListHeight()

    function updateDatepicker(element, value) {
        $(element).datepicker('update', new Date(value));
    }

    // Show hide dependent questions(visible) accroding to their parent values
    function showHideQuestions() {
        $('li.question-answer').each(function (index, data) {

            let element = $(this);
            let parentId = element.attr('data-parent-id');
            let questionId = element.attr('data-id');
            let parentKey = element.attr('data-parent-key');
            let topicId = element.attr('data-topic-id');
            let topicName = element.attr('data-topic-name');
            showHideSingleQuestion(element, parentId, parentKey, topicId, topicName);
        });
    }

    // to check value is string or json data.
    function isJson(item) {
        item = typeof item !== "string"
                ? JSON.stringify(item)
                : item;
        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    }

    // Divide list items in 2 coulumns if more then 6 items
    function fixListHeight() {
        $("select").selectBoxIt();
//            if ($('ul.questions-list li:visible').length > 6) {
//                $('ul.questions-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.questions-list').addClass('single-column').removeClass('double-column');
//            }
//            if ($('ul.topic-list li').length > 6) {
//                $('ul.topic-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.topic-list').addClass('single-column').removeClass('double-column');
//            }
//            if ($('ul.sub-topic-list li').length > 6) {
//                $('ul.sub-topic-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.sub-topic-list').addClass('single-column').removeClass('double-column');
//            }
    }


//        <-------EDITABLE-------->



//        Toggles edit and save buttons
    function editAndSave() {
        $(document).off('click', '.active-inputs');
        $(document).on('click', '.active-inputs', function () {
            $('.question_datepicker').datetimepicker({
                format: 'YYYY-MM-DD',
            });
            // Init yearpicker
            $(".question_yearpicker").datetimepicker({
                viewMode: "years",
                format: "YYYY",
                useCurrent: true,
            });
            $(this).closest('.service-preview').find('input[type="text"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill), input[type="year"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill)  , input[type="number"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , input[type="radio"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , input[type="checkbox"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill), select:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , textarea:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , span.placeholder-symbol')
                    .attr('disabled', false)
                    .css({"border-bottom": "1px solid #ccc", "background": "#fff"});
            $(this).closest('.service-preview').find('.active-inputs').hide();
            $(this).closest('.service-preview').find('.updateAnswers').show();
            $(this).closest('.service-preview').find('.selectboxit-btn').removeClass('selectboxit-disabled');
            $(this).closest('.service-preview').find('.upload-statement .hide').removeClass('hide');
            $(this).closest('.service-preview').find('.addMoreRow .hide').removeClass('hide');
            $(this).closest('.service-preview').find('.deleteRow .hide').removeClass('hide');
            $(this).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                let symbols = $(element).attr('data-symbols');
                let symbolsArray = symbols.split(',');
                let inputValue = $(element).find('input').val();
                if (symbolsArray.length > 1) {
                    if (inputValue) {
                        if (/^[a-zA-Z0-9- ]*$/.test(inputValue) == false) {
                            var symbolValue = inputValue.substr(inputValue.length - 1);
                            var newInputValue = inputValue.split(symbolValue)[0];
                        } else {
                            if (inputValue.indexOf('Years') != -1) {
                                var symbolValue = 'Years';
                                var newInputValue = $(element).find('input').val().split('Years')[0];

                            } else {
                                var symbolValue = 'Months';
                                var newInputValue = $(element).find('input').val().split('Months')[0];

                            }
                        }
                    } else {
                        var symbolValue = symbolsArray[0]
                        var newInputValue = ''
                    }
                    let blank = '';
                    let selected = 'selected="selected"';
                    const selectedFirst = symbolValue == symbolsArray[0] ? selected : blank;
                    const selectedSecond = symbolValue == symbolsArray[1] ? selected : blank;
                    let newHtml = '<div class="symbol-elements" style="display: flex">' +
                            '<input type="text" class="new-input activeInputs"  value="' + newInputValue + '" required>' +
                            '<select class="new-symbol-type activeInputs">' +
                            '<option value="' + symbolsArray[0] + '" ' + selectedFirst + '>' + symbolsArray[0] + '</option>' +
                            '<option value="' + symbolsArray[1] + '" ' + selectedSecond + '>' + symbolsArray[1] + '</option>' +
                            '<select>' +
                            '</div>'
                    $(element).find('input').hide();
                    $(element).find('.answer').append(newHtml);
                } else {
//                        var symbolValue = inputValue.substr(inputValue.length - 1);
//                        inputValue = inputValue.split(symbolValue)[0];
//                        let newHtml = '<div class="symbol-elements" style="display: flex">' +
//                                '<input type="number" class="new-input" style="' + styleForNewHtml + '" value="' + inputValue + '">' +
//                                '<span class="new-symbol-type" style="padding: 5px;"> ' + symbolsArray[0] + ' </span>' +
//                                '</div>'
//                        $(element).find('input').hide();
//                        $(element).find('.answer').append(newHtml);
                }
            })
        });
    }
//        Toggles edit and save buttons ENDS

    function updateAnswers($this) {
        $this.closest('.service-preview').find('input[type="text"], input[type="year"]  , input[type="number"] , input[type="radio"] ,input[type="number"] , input[type="radio"] , input[type="checkbox"], select , span.placeholder-symbol')
                .attr('disabled', true)
                .css({"border-bottom": "none", "background": "transparent"});
        $this.closest('.service-preview').find('textarea').attr('disabled', true).css({"background": "transparent"});
        $this.closest('.service-preview').find('.updateAnswers').hide();
        $this.closest('.service-preview').find('.active-inputs').show();
        $this.closest('.service-preview').find('.selectboxit-btn').addClass('selectboxit-disabled');
        $this.closest('.service-preview').find('.upload-statement .modal-upload-file-input').addClass('hide');
        $this.closest('.service-preview').find('.upload-statement .upload-statement-button').addClass('hide');
        $this.closest('.service-preview').find('.upload-statement .delete-modal-file').addClass('hide');
        $this.closest('.service-preview').find('.addMoreRow .btn').addClass('hide');
        $this.closest('.service-preview').find('.deleteRow .fa').addClass('hide');

    }

    editAndSave();
    //Validation
    $(document).off('click', '.validation-button');
    function validateForm() {
        $(document).on('click', '.validation-button', function (e) {
            var formId = $(this).closest('.service-preview:visible').find('form').attr('id');
            if (!formId) {
                return false;
            }
            var saveButtonClicked = $(this).closest('.service-preview:visible').find('.updateAnswers');
            var saveButtonClickedClass = $(this).closest('.service-preview:visible').find('.updateAnswers').attr('data-class');
            $('label.service-input-validate').remove();
            let filesArray = [];
            $('#' + formId).find('.service-files-table tr.fileUploadRow').each(function (index, childElement) {

                let $row = $(childElement)
                let filePath = $row.find('td.file-download a').attr('download');
                let fileName = $row.find('td.file-name-input input').val();
                let fileNameKey = $row.find('td.file-name-input').attr('data-key-name');
                let fileType = $row.find('td.file-select-type select.file-type-dropdown').val();
                let fileTypeKey = $row.find('td.file-select-type').attr('data-key-name');
                let fileData = {}
                fileData[fileNameKey] = fileName;
                fileData['file_path'] = filePath;
                if (fileType) {
                    fileData[fileTypeKey] = fileType;
                }
                filesArray.push(fileData);
            })

            if (filesArray.length > 0) {
                $('#fileParentHidden').val(JSON.stringify(filesArray));
            }


            // To Check validation for checkboxes.............................
            function checkboxValidations(formId) {
                let isErrorFound = false;
                $('.type-checkbox').each(function (index, childElement) {
                    let dataId = $(childElement).attr('data-parent-id');
                    let parent_answer = $('li[data-id="' + dataId + '"] input[type=radio]:checked').val();
                    let $this = $('li[data-parent-id="' + dataId + '"] input[type="checkbox"]');
                    let notCheckedCount = [];
                    $this.each(function (innerIndex, element) {

                        if ($(element).prop('checked') == false && $(childElement).attr('data-parent-key') == parent_answer) {
                            notCheckedCount.push(false);
                        }
                        if (innerIndex + 1 == $this.length) {
                            if (notCheckedCount.length == $this.length) {
                                isErrorFound = true;
                                $(element).parent('.answer').find('.error').remove();
                                $(element).parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                            }
                        }
                    });
                });
                return isErrorFound;
            }

            function contigentTableValidation() {
                let isErrorFound = false;
                $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
                    $(childElement).closest('tbody').find('.error').remove();
                    if ($(childElement).find('td input').val() == '' && $(childElement).is(':visible')) {
                        $(childElement).closest('tbody').append('<label class="error error-alert table-single-error">fields can not be blank.</label>')
                        isErrorFound = true;
                    }
                });
                return isErrorFound;
            }
            $('#' + formId).validate({
                submitHandler: function (form) {
                    $('#' + formId).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                        let inputValue = $(element).find('.symbol-elements input.new-input').val();
                        let inputSymbol = $(element).find('.symbol-elements select.new-symbol-type').val();
                        if (inputValue && inputSymbol) {
                            $(element).find('input:not(.new-input)').val(String(inputValue) + '' + String(inputSymbol)).show();
                            $(element).find('.symbol-elements').remove();
                        }
                    });
                    var inputs = $('#' + formId).serializeArray();



                    if (checkboxValidations(formId)) {
                        return false;
                    } // Because serializeArray() ignores unset checkboxes and radio buttons: /
                    if (contigentTableValidation()) {

                        return false;
                    } // To check inputs in contigent table validations
                    inputs = inputs.concat(
                            jQuery('#' + formId + ' input[type=checkbox]:not(:checked)').map(
                            function () {
                                return {"name": this.name, "value": 'unchecked'}
                            }).get()
                            );
                    // updated selected checkboxs value in data.
                    jQuery('#' + formId + ' input[type=checkbox]:checked').map(
                            function () {
                                let index = inputs.findIndex(el => el.name == this.name)
                                inputs[index] = {"name": this.name, "value": 'checked'}
                            }).get();

                    updateAnswer(saveButtonClicked, inputs, saveButtonClickedClass);
                },
                invalidHandler: function (form) {
                    console.log('invalid');
                    return false;
                },
                errorPlacement: function errorPlacement(error, element) {
                    var errorCount = $(element).closest('.answer').find('.service-input-validate').length
                    if (errorCount == 0) {
                        $(element).parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                        if ($('input[type="radio"]')) {
                            $(element).parent().parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                        }
                    } else {
//                          $('label.service-input-validate').css('display', 'block');
                    }

                },
                rules: {
                    required: true
                }
            });
            if ($('.service-input-validate').length > 0) {
                e.preventDefault();
            }
        });
    }
    validateForm();
    $(document).on('click', '.save-new-answers , .save-new-modal-answers , .save-new-topic-answers', function (e) {
        $(this).closest('.service-preview').find('.validation-button').trigger('click');
        e.stopImmediatePropagation();
    });
    function updateAnswer($this, inputs, $class) {
        var formObj = {};
        $.each(inputs, function (i, input) {
            if (input.name.indexOf("[]") != -1) {  // to check array type values
                let newKey = input.name.split('[]')[0];
                if (formObj[newKey] === undefined) {
                    formObj[newKey] = [input.value];
                } else {
                    formObj[newKey].push(input.value);
                }
            } else {
                formObj[input.name] = input.value; //  for normal values
            }
        });
        var serviceId = $this.attr('data-service-id');
        var topicId = $this.attr('data-topic-id');
        var answerIndex = $this.attr('data-index');
        var copyIndex = $this.attr('data-copy-index');
//            var clientId = $('#client-id').val();
        var url = "/updateAnswers/" + serviceId + "/" + clientId;
        if (topicId) {
            var url = "/updateAnswers/" + serviceId + "/" + clientId + "/" + topicId;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            data: {answerIndex: answerIndex, copyIndex: copyIndex, data: formObj},
            success: function (data) {
                if (data.success == true) {
                    toastr.success('Updated Successfully');
                    updateAnswers($this);
                    if (copyIndex && $class == 'save-new-answers') {
                        var copyTabCurrent = $('.single-sub-topic[data-topic="' + topicId + '"]');
                        var divToBeShown = 'serviceSubTopics';
                        var topicName = copyTabCurrent.attr('data-topic-name');
                        if ($this.closest('.service-preview').children().find('#main-tabset').length == 1) {
                            var displayType = $('.service-preview').children().find('#main-tabset').find('li.active').attr('data-display-type');
                        }
                        if (!displayType) {
                            var displayType = copyTabCurrent.attr('data-display-type');
                        }
                        subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, 'doNotAnimate', divToBeShown);
                        fixListHeight();
                    }
                    $('.updateAnswers').hide();
                } else {
                    toastr.warning('poor you');
                }
            }
        });
    }
    function onChangeShowHideQuestions() {
        $(document).on('change', 'input[type="radio"], select, input[type="checkbox"]', function () {
            var parent_li = $(this).closest('li');
            let element = $(parent_li);
            let questionId = element.attr('data-id');
            let parentValue = $(this).val();
            if (parentValue == null && $(this).attr('data-type') == 'select') {
                parentValue = $(this).find('option:first').attr('value');
            }
            if ($(this).attr('data-type') == 'checkbox') {
                if ($(this).is(':checked')) {
                    $(this).val(1);
                } else {
                    $(this).val(0);
                }
            }
            $("li.question-answer[data-parent-id='" + questionId + "']").each(function (index, childElement) {

                let parentKey = $(childElement).attr('data-parent-key');
//if (questionType == 'checkbox') {
//                    if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
//                        var val = [];
//                        $('.question-' + parentId + ':checked').each(function (i) {
//                            val[i] = $(this).val();
//                        });
//                    }
//                }
                if (isJson(parentKey)) {
                    parentKeyArray = Object.keys(JSON.parse(parentKey));
                    let keyIndex = parentKeyArray.findIndex(key => key == parentValue)
                    if (keyIndex > -1 && element.is(':visible')) {
                        $(childElement).show();
                        $(childElement).find('.answer').children(':first').attr('required', 'required');
                    } else {
                        $(childElement).hide();
                    }
                } else {
                    if (parentValue == parentKey && element.is(':visible')) {
                        $(childElement).show();
                        $(childElement).find('.answer').children(':first').attr('required', 'required');
                    } else {
                        $(childElement).hide();
                    }
//                    }
                }

                let dataInputType = $(childElement).attr('data-input-type');
                if (dataInputType == 'radio') {
                    $(childElement).find(':input').prop('checked', false).trigger('change');
                } else if (dataInputType == 'checkbox') {
                    $(childElement).find(':input').prop('checked', false).trigger('change');
                } else if (dataInputType == 'select') {
                    $(childElement).find('.answer select').val('').trigger('change');
                } else {
                    $(childElement).find(':input').val('');
                }
            });
        }
        );
    }

    onChangeShowHideQuestions();

    function showHideSingleQuestion(element, parentId, parentKey, topicId, topicName) {
        if (parentId && parentKey) {

//                    if ($('#question-' + parentId).parent().parent().css('display') != 'none') {
            let questionType = $('#question-' + parentId).attr('data-type')
            let parentValue = '';
            if (questionType == 'radio') {
                if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
                    parentValue = $('.question-' + parentId + ':checked').val();
                }
            } else if (questionType == 'checkbox') {
                if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
                    var val = [];
                    $('.question-' + parentId + ':checked').each(function (i) {
                        val[i] = $(this).val();
                    });
                }
            } else {
                if ($('#question-' + parentId).closest('li').is(':visible')) {
                    parentValue = $('#question-' + parentId).val();
                }
            }
            if (!parentValue && ($.inArray(topicId, comprehensive_life_insurance_ids) !== -1 || topicName.toLowerCase() == your_liabilities)) {
                parentValue = element.attr('data-parent-answer');
            }

            let parentKeyArray = [];
            if (isJson(parentKey)) {
                parentKeyArray = Object.keys(JSON.parse(parentKey));
                let keyIndex = parentKeyArray.findIndex(key => key == parentValue)
                if (keyIndex > -1) {
                    element.show();
                } else {
                    element.hide();
                    element.find('.answer').children(':first').removeAttr('required');
                }
            } else {
                if (val && $.isArray(val) && ($.inArray(parentKey, val) !== -1)) {
                    element.show();
                } else {
                    if (parentValue == parentKey) {
                        element.show();
                    } else {
                        element.hide();
                        element.find('.answer').children(':first').removeAttr('required');
                    }
                }

            }
//                var elementType = element.attr('data-input-type');
//                console.log('elementType', elementType);
//                if (elementType == 'radio') {
//                    console.log('triggered' + elementType);
//                    element.find(':input').trigger('change');
//                } else if (elementType == 'checkbox') {
//                    console.log('triggered' + elementType);
//                    element.find(':input').trigger('change');
//                } else if (elementType == 'select') {
//                    console.log('triggered' + elementType);
//                    element.find('.answer select').trigger('change');
//                }
//                    }

        }
    }



    // get file size while uploading file..................
    $(document).on('change', 'input[type=file]', function () {
        if ($('.bfd-files').find('.bfd-info').length == 1) {
            $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
            $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
        }
        $('.fileNameSaver').removeClass('hide');
        if ($(this)[0].files[0]) {
            var iSize = ($(this)[0].files[0].size / 1024);
            if (iSize / 1024 > 1)
            {
                if (((iSize / 1024) / 1024) > 1)
                {
                    iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
                    $("#lblSize").html(iSize + "Gb");
                } else
                {
                    iSize = (Math.round((iSize / 1024) * 100) / 100)
                    $("#lblSize").html(iSize + "Mb");
                }
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#lblSize").html(iSize + "kb");
            }
        }
    });
    // File name validation in file uploader modal.................
    $(document).on('keyup change', '.bfd-files input', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        $('.bfd-ok').removeAttr('disabled');
        var inputs = $(".file-name-input input");
        for (var i = 0; i < inputs.length; i++) {
            if ($(this).val() && $('#newName').val() == $(inputs[i]).val()) {
                $('.bfd-ok').prop("disabled", "true");
                $(this).closest('.inner-left').append('<label class="error error-alert modal-error">This name already exists.</label>');
                return;
            } else if (!$(this).val()) {
                $(this).closest('.inner-left').append('<label class="error error-alert modal-error">This field is required.</label>');
                $('.bfd-ok').prop("disabled", "true");
            }
        }
    });
    // check file name validation in files table..............
    $(document).off('keyup change', 'td.file-name-input input');
    $(document).on('keyup change', 'td.file-name-input input', function () {

        let rowIndex = $(this).closest('.fileUploadRow').attr('data-row-index')
        let $parentRow = $('tr[data-row-index="' + rowIndex + '"]');
        $parentRow.next('.file-name-error').remove();
        $parentRow.find('.save-file').css('pointer-events', 'all');
        $parentRow.closest('.service-preview').find('.save-new-answers').css('pointer-events', 'all');
        var inputs = $(".file-name-input input");
        for (var i = 0; i < inputs.length; i++) {
            if (i != rowIndex) {
                if ($(this).val() && $(this).val() == $(inputs[i]).val()) {
                    $parentRow.find('.file-name-error').remove();
                    $parentRow.find('.save-file').css("pointer-events", "none");
                    $parentRow.closest('.service-preview').find('.save-new-answers').css("pointer-events", "none");
                    $parentRow.after('<label class="error file-name-error">This name already exists.</label>');
                    return;
                } else if (!$(this).val()) {

                    $parentRow.next('.file-name-error').remove();
                    $parentRow.after('<label class="error file-name-error">This field is required.</label>');
                    $parentRow.closest('.service-preview').find('.save-new-answers').css("pointer-events", "none");
                    $parentRow.find('.save-file').css("pointer-events", "none");
                }
            }
        }
    });
    // File uploader.......................
    $(document).off('click', '.open_button');
    $(document).on('click', '.open_button', function () {


        let $this = $(this);

        let fileTypeKey = $this.closest('.question-answer').find('select.file-type-dropdown:last').parent().attr('data-key-name');
        let fileNameKey = $this.closest('.question-answer').find('table.service-files-table').find('tr:last td:first').attr('data-key-name');
        if (!fileNameKey) {
            fileNameKey = 'file_name';
        }
        let ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
        var topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');

        if (serviceId == annualServivceId || serviceId == semiAnnualServiceId) {
            var dropDownList = '<select data-key="file_name" disabled="disabled" class="file-type-dropdown" style="display: none;">' +
                    '<option value="401(k)">401(k)</option>' +
                    '<option value="SEP">SEP</option>' +
                    '<option value="SRA Account">SRA Account</option>' +
                    '</select>';
        } else if (serviceId == lifeInsuranceServiceId) {
            var dropDownList = '<select data-key="file_name" disabled="disabled" class="file-type-dropdown" style="display: none;">' +
                    '<option value="user_name">' + userName + '</option>' +
                    '<option value="spouse_name">Spouse Name</option>' +
                    '</select>';
        } else {
            var dropDownList = '';
        }
        var fileTypeKeyVar = '';
        if (fileTypeKey) {
            fileTypeKeyVar = 'data-key-name="' + fileTypeKey + '"';

        }
        $.FileDialog({multiple: false}).on('files.bs.filedialog', function (ev) {
            var numbersOfTr = $('.fileUploadRow').length,
                    files = ev.files,
                    formData = new FormData(),
                    i = 0;
            if (files.length != 0) {
                files.forEach(function (f) {
                    let dropDownEl = '';
                    if (serviceId != stockServiceId && dropDownList != '') {
                        dropDownEl = '<td class="file-select-type" ' + fileTypeKeyVar + '></td>'
                    }
                    let newTr = '<tr class="fileUploadRow" data-row-index="' + numbersOfTr + '"> \
                                    <td class="file-name-input" data-key-name="' + fileNameKey + '"><input type="text" value=""  disabled="disabled" ></td> \
                                    ' + dropDownEl + ' \
                                    <td class="file-download text-center"></td> \
                                    <td class="text-center file-action"><i class="fa fa-check save-file"></i></td> \
                                </tr>'
                    $this.closest('.question-answer').find('table.service-files-table').append(newTr);
                    formData.append("file", f, f.name);
                    i++;
                });
            } else {
                toastr.warning(' Please Upload Single File.');
                return false;
            }

            if (dropDownList.length > 0) {
                $('tr[data-row-index="' + numbersOfTr + '"] .file-select-type').append(dropDownList)
                $('tr[data-row-index="' + numbersOfTr + '"] .file-select-type select').show();
            }
//                var clientId = $('#client-id').val();
            $this.after('<div class="lds-dual-ring"></div>');

            $.ajax({
                type: "post",
                url: ajaxUploadDocument + '/' + clientId,
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                timeout: 60000,
                success: function (response) {
                    $('.lds-dual-ring').remove();

                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-name-input input')
                            .removeAttr("disabled", "")
                            .css({'border-bottom': '1px solid #ccc', 'background': '#fff'});
                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-select-type select')
                            .removeAttr("disabled", "")
                            .css({'width': '80%', 'background': '#fff'});
                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-action')
                            .append('<input type="hidden" name="file_path" class="new-file-path" value="' + response.message + '">')
                    $this.closest('.service-preview').find('.save-new-answers, .active-inputs').hide();
                },
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }).on('cancel.bs.filedialog', function (ev) {
//                     alert('here');
        });
        $('.bfd-dropfield-inner').attr('id', 'flUpload');
        $('.bfd-files').append("<span class='inner-left' style='position:relative;'><span class='col-xs-12 paddingLeft0 paddingRight0'> <input class='form-control fileNameSaver hide' id='newName' required='required' placeholder='Enter File Name'></span><label id='lblSize' class='file-size'></label></span>");
        if ($('.fileNameSaver').val() == "") {
            $('.bfd-ok').prop("disabled", "true");
        }
        $('.file-size').text('');
        $(document).on('click', '.modal-footer .bfd-ok', function () {
        $('.fileUploadRow:last td.file-name-input input').val($('.fileNameSaver').val());
        if (topicId == incomeTaxTopicId) {
            if ($('tr.fileUploadRow').length != 0) {
                $('.add-file-btn').hide();
            }
        }
    });
    });
    
    // File upload action.........
    $(document).off('click', '.save-file');
    $(document).on('click', '.save-file', function () {

        $this = $(this).closest('tr');
        let filePath = $this.find('td.file-action .new-file-path').val();
        let fileName = $this.find('td.file-name-input input').val();

        let fileNameKey = $this.find('td.file-name-input').attr('data-key-name');
        let fileType = $this.find('td.file-select-type select.file-type-dropdown').val();
        let fileTypeKey = $this.find('td.file-select-type').attr('data-key-name');
        let questionId = $('#fileParentHidden').attr('name');
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
        let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
//            let clientId = $('#client-id').val();
        let filesArray = [{}]


        if (topicId == incomeTaxTopicId) {
            filesArray[0]['file_name'] = fileName;

        } else {
            filesArray[0][fileNameKey] = fileName;
        }
        filesArray[0]['file_path'] = filePath;
        if (fileType) {
            filesArray[0][fileTypeKey] = fileType;
        }

        let ajaxUrl = "/addFiles/" + serviceId + "/" + clientId;
        if (topicId) {
            ajaxUrl = "/addFiles/" + serviceId + "/" + clientId + "/" + topicId;
        }

        // Ajax request init
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: ajaxUrl,
            data: {questionId: questionId, data: filesArray},
            success: function (data) {
                if (data.success == true) {
                    toastr.success('File uploaded successfully.');
                    if (fileType) {
                        filesArray[0][fileTypeKey] = fileType;
                        $this.find('td.file-select-type select.file-type-dropdown')
                                .attr('disabled', 'disabled')
                                .css('background', 'transparent').selectBoxIt();
                    }
                    $this.find('td.file-name-input input')
                            .attr('disabled', 'disabled')
                            .css({'border-bottom': 'none', 'background': 'transparent'});
                    $this.find('td.file-download')
                            .append('<a download="' + filePath + '" href="files/' + clientId + '/' + filePath + '"> <i class="fa fa-download"></i></a>')
                    $this.find('td.file-action .fa')
                            .removeClass('fa-check save-file')
                            .addClass('fa-trash delete-file');
                    $this.closest('.service-preview').find('.active-inputs').show();
                } else {
                    toastr.warning('Something went wrong.');
                }
            }
        });
    });
    // delete file action..............
    $(document).off('click', '.delete-file');
    $(document).on('click', '.delete-file', function () {
        $this = $(this).parent();
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                let dataIndex = $this.closest('tr').attr('data-row-index');
                let questionId = $('#fileParentHidden').attr('name');
                let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
                let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
//                    let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteFiles/" + serviceId + "/" + clientId;
                if (topicId) {
                    ajaxUrl = "/deleteFiles/" + serviceId + "/" + clientId + "/" + topicId;
                }

                // Ajax request init
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionId: questionId, fileIndex: dataIndex},
                    success: function (data) {
                        if (data.success == true) {
                            if (topicId == incomeTaxTopicId) {
                                if ($('tr.fileUploadRow').length != 0) {
                                    $('tr[data-row-index="' + dataIndex + '"]')
                                            .closest('.question-answer')
                                            .find('.add-file-btn').show();
                                }
                            }
                            toastr.success('File deleted successfully.');
                            $('tr[data-row-index="' + dataIndex + '"]').remove();
                            $('tr.fileUploadRow').each(function (index, childElement) {
                                $(childElement).attr('data-row-index', index); // update table rows indexes
                            })
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        });
    });
    $(document).off('change', '.upload-statement input[type=file]');
    $(document).on('change', '.upload-statement input[type=file]', function () {
        let $this = $(this);
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
        let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
//            let clientId = $('#client-id').val();
        let questionId = $this.closest('.question-answer').find('.fileParentHidden').attr('name');
        let uploadStatementFile = new FormData();
        uploadStatementFile.append('file', $this[0].files[0]);
        $this.after('<div class="lds-dual-ring"></div>');

        $.ajax({
            type: "post",
            url: ajaxUploadDocument + '/' + clientId,
            async: true,
            data: uploadStatementFile,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000,
            success: function (response) {
                $('.lds-dual-ring').remove();

                let filePath = "files/" + clientId + "/" + response.message;
                let filesArray = new Object();
                filesArray['file_name'] = $this[0].files[0].name;
                filesArray['file_path'] = response.message;
                $this.closest('.question-answer').find('.fileParentHidden').val(JSON.stringify(filesArray));
//                    $this.closest('.upload-statement').find('.upload-statement-button, .modal-upload-file-input').hide();
                let fileName = '<span>' + response.message + '</span>';
                $this.closest('.upload-statement').find('table.uploaded-modal-file').removeClass('hide')
                $this.closest('.upload-statement').find('table.uploaded-modal-file').find('td.modal-file-name').html(fileName);
            },
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            }

        });
    });
    $(document).off('click', '.upload-statement .delete-modal-file')
    $(document).on('click', '.upload-statement .delete-modal-file', function () {
        $this = $(this);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                let questionId = $this.closest('.question-answer').find('.fileParentHidden').attr('name');
                let modalIndex = $this.closest('.service-preview').find('.updateAnswers').attr('data-index')
                let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
                let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
//                    let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteSingleFile/" + serviceId + "/" + clientId;
                if (topicId) {
                    ajaxUrl = "/deleteSingleFile/" + serviceId + "/" + clientId + "/" + topicId;
                }
                // Ajax request init
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionId: questionId, answerIndex: modalIndex},
                    success: function (data) {
                        if (data.success == true) {
                            toastr.success('File deleted successfully.');
                            $this.closest('.upload-statement').find('table.uploaded-modal-file').addClass('hide')
                            $this.closest('.upload-statement').find('table.uploaded-modal-file').find('td.modal-file-name').html('');
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        })
    });

    $(document).off('click', '.addMoreRow')
    $(document).on('click', '.addMoreRow', function () {
        let tableElement = $(this).closest('.question-answer').find('.contingent-beneficiary-table');

        let tableRow = $(this).closest('.question-answer').find('.contingent-beneficiary-table tbody tr:first-child').clone();
        tableElement.find('tbody').append(tableRow);
        tableElement.find('tbody tr:last-child').find(':input').val('');
        $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
            $(childElement).find('td.deleteRow').attr('data-index', index); // update table rows indexes
        });
        $(".question_yearpicker").datetimepicker({
            viewMode: "years",
            format: "YYYY",
            useCurrent: true,
        });
    });

    $(document).off('click', '.deleteRow')
    $(document).on('click', '.deleteRow', function () {
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                var deleteIndex = $this.attr('data-index');
                let answerIndex = $this.closest('.service-preview').find('.updateAnswers').attr('data-index')

                var questionIds = $this.attr('data-question-ids');
//                    let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteTableData";
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionIds: questionIds, answerIndex: answerIndex, clientId: clientId, deleteIndex: deleteIndex},
                    success: function (data) {
                        if (data.success == true) {
                            toastr.success('Deleted successfully.');
                            $this.parent().remove();
                            $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
                                $(childElement).find('td.deleteRow').attr('data-index', index); // update table rows indexes
                            });
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        })
    });
    if (typeof selectedServiceId != "undefined" && selectedServiceId) {
        $('.all-services .products-list[data-id="' + selectedServiceId + '"]').first().trigger('click');
    }

    $(document).off('keyup', '.TaxableIncome');
    $(document).on('keyup', '.TaxableIncome', function () {
        $this = $(this);
        var taxFilling = $(this).val();
        if (taxFilling) {
            getTaxBracket(taxFilling, $this);
        }


    });
    function getTaxBracket(taxFilling, $this) {
        $.ajax({
            type: 'GET',
            url: 'taxBracket/',
            data: {
                taxableIncome: taxFilling
            },
            success: function (resp) {
                $this.closest('.questions-list').find('.TaxBracket').val(resp);
                $('.service-preview-file').find('.TaxBracket').val(resp);
                $('.lds-dual-ring').hide();
            }
        });
    }

});
</script>


@endsection