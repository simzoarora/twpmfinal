@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/servicesQuestion.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_2')
                        </section>
                        <h3></h3>
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_1')
                        </section>
                    </fieldset>
                     <h3></h3>
                    <fieldset>
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_3')
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@section('after-scripts')
<!--<script src="{{ asset('js/dashboard.js') }}"></script>-->
<script src="{{ asset('js/services-questions.js') }}"></script>
{{ Html::script('/vendor/laravel-filemanager/js/lfm.js') }}
<!--<script src="/vendor/laravel-filemanager/js/lfm.js"></script>-->
@endsection