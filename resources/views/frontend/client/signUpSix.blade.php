@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/signUpSix.css')) }}   
@stop

@section('content')
<div class="col-sm-12 header-bar">
    <div class="container">
        <div class="row">
            <img src="img/twpm-blue-logo.png" class="logo" alt="">
            <p>Account Setup</p>
        </div> 
    </div>
</div>

<div class="container-fluid wrapper">
    <div class="container">
        <div class="step">
            <p>Step2: Assess your risk tolerance</p>
             <div class="blue-line col-sm-4">
            </div>
            <div class="grey-line col-sm-8">
            </div>
        </div>
        <div class="col-sm-8 col-sm-offset-2 site-heading-1">
            <p>LET'S GET TO KNOW YOU</p>
            <h2>When deciding how to invest your money, <br> Which do you care about more?</h2>
            <ul>
                <li><label class="checkbox-custom-label">
                        <input type="checkbox">
                       Maximizing gains
                    </label></li>
                <li><label class="checkbox-custom-label">
                        <input type="checkbox">
                       Minimizing losses
                    </label></li>
                <li><label class="checkbox-custom-label">
                        <input type="checkbox">
                        Equal mix
                    </label></li>
            </ul>

            <div class="next-page step5-padding"> 
               
                <a href="#" class="btn btn-info" role="button">Continue</a>
            </div>
        </div>
        <div class="footer">
            <p class="description">FOOTER LINKS</p>
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div> 
    </div> 
</div> 

@endsection