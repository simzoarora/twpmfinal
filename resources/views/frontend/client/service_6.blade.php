@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_36')
                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script src="{{ asset('js/educational-consultation.js') }}"></script>
<script>
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).on('keydown', 'input[type=number]', function (event) {
    if (event.shiftKey == true) { 
        event.preventDefault();
    }
    if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
    } else {
        event.preventDefault();
    }
    if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
        event.preventDefault();

});
$(document).ready(function () {
    $("select").selectBoxIt();
    var studentList = [];
    if ($('#liabilitiesData').val() != '') {
        studentList = JSON.parse($('#liabilitiesData').val());
    }


    $('#studentform').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#myModal .student-name').val() && $('#myModal .student-age').val()) {
                    if ($('#studentform').hasClass('edit-form')) {
                        studentList[$(this).attr('data-index')] = {
                            name: $('#myModal .student-name').val(),
                            age: $('#myModal .student-age').val(),
                            student: $('#myModal .who-is-student').val(),
                            education: $('#myModal .level-of-education').val()
                        }
                        $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i>' + $('#myModal .student-name').val() + '</td>');
                    } else {
                        $('#student-info tbody').append('<tr data-index=' + studentList.length + '><td><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i>' + $('#myModal .student-name').val() + '</td></tr>');
                        studentList.push({
                            name: $('#myModal .student-name').val(),
                            age: $('#myModal .student-age').val(),
                            student: $('#myModal .who-is-student').val(),
                            education: $('#myModal .level-of-education').val()
                        });
                    }
                    $('#myModal').modal('hide');
                    $('#liabilitiesData').html(JSON.stringify(studentList));
                    $('#myModal .student-name,#myModal .student-age').val('');
                    $('#myModal select').val('').trigger('change');
                }
            }
        }
        return false;
    });

//        $('#studentform').on('click', function () {
//            $('#myModal .error-alert').remove();
//            if ($('#myModal .student-name').val() && $('#myModal .student-age').val() && $('#myModal .who-is-student').val() && $('#myModal .level-of-education').val()) {
//
//                if ($(this).hasClass('edit-form')) {
//                    studentList[$(this).attr('data-index')] = {
//                        name: $('#myModal .student-name').val(),
//                        age: $('#myModal .student-age').val(),
//                        student: $('#myModal .who-is-student').val(),
//                        education: $('#myModal .level-of-education').val()
//                    }
//                    $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>' + $('#myModal .student-name').val() + '</td>');
//                } else {
//
//                    $('#student-info tbody').append('<tr data-index=' + studentList.length + '><td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>' + $('#myModal .student-name').val() + '</td></tr>');
//                    studentList.push({
//                        name: $('#myModal .student-name').val(),
//                        age: $('#myModal .student-age').val(),
//                        student: $('#myModal .who-is-student').val(),
//                        education: $('#myModal .level-of-education').val()
//                    });
//                }
//                $('#myModal .student-name,#myModal .student-age').val('');
//                $('#myModal select').val('').trigger('change');
//            } else {
//                $('#myModal input, #myModal select').each(function (i, elem) {
//                    if (!$(elem).val()) {
//                        $(elem).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
//                    }
//                });
//            }
//            $('#myModal').modal('hide');
//            
//        });
//        $('#myModal input').on('keyup change', function () {
//            $(this).parent().find('.error-alert').remove();
//            if (!$(this).val()) {
//                $(this).parent().append('<label class="error-alert">This field is required.</label>');
//            }
//        });

    $('#student-info').on('click', '.fa-pencil', function () {
        $('#myModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        studentDetails = studentList[index];
        $('#myModal .student-name').val(studentDetails.name);
        $('#myModal .student-age').val(studentDetails.age);
        $('#myModal .who-is-student').val(studentDetails.student).trigger('change');
        $('#myModal .level-of-education').val(studentDetails.education).trigger('change');
        $('#studentform').addClass('edit-form').attr('data-index', index);
        $('#myModal .error-alert').remove();

    });
    $('.add-student').on('click', function () {
        $('#studentform').removeClass('edit-form');
        $('#myModal .error-alert').remove();
        $('#myModal select').val('').trigger('change');
        $('#myModal input[type="text"], #myModal input[type="number"]').val('');
    });
});





</script>


@endsection