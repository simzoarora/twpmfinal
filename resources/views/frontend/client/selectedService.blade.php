@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles')
{{ Html::style(asset('css/selectedService.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content back-div">
                <?php $page_title = $addedService->title; ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    <div class="col-sm-5 completed-information">
                        <p>Thank you! <br> We have the information needed for your</p>

                        <h2><?php echo $page_title; ?></h2>
                        <p> What would you like to do next?</p>

                        <a class="proceed-payment" href="{{route('frontend.client.serviceEnrollment','client')}}"> Make Payment and <br>Schedule  Appointment</a>

                        <p> or </p>
                        <p> Work on another recommended service </p>

                        <div class=" col-sm-12 col-xs-12 recommended-services">
                            <?php
                            foreach ($recommendedServices as $key => $service) {
                                if ($key < 2) {
                                    ?>
                                    <div class="col-sm-6 col-xs-6 plans-wrapper">
                                        <a href="{{ route('frontend.client.servicesQuestion', [config('constant.subdomain'), $service['id']]) }}"><div class="annual-return">  
                                                <p>Recommended</p>
                                                <h4>{{ $service['title'] }}</h4>
                                            </div></a>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
<script src="{{ asset('js/signUp.js') }}"></script>
@endsection