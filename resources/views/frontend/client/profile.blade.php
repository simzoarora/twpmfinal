@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/profile.css')) }}   
@stop

@section('content')
<div class="dashboard"> 

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                <?php
                $page_title = 'Your Profile';
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    <div class="profile-nav">
                        <ul class="list-inline">
                            <li class="active"><a href="#profile_inner">Profile</a></li>
                            <!--<li><a href="#settings-inner">Settings</a></li>-->
                            <li><a href="#security-inner">Security</a></li>
                        </ul>
                    </div>

                    <div class="profile_inner profile-lists">
                        @if(!empty($userData->Profile))
                        <div class="col-sm-12 user-details">
                            <h2>{{ $userData->Profile->first_name.' '.$userData->Profile->middle_name.' '.$userData->Profile->last_name }}</h2>
                            <div class="edit-symbol">
                                <span>{{ $userData->email }}</span>
                                <span class="edit-icon" id="name-edit-open"><i title="Edit" class="fa fa-pencil"></i></span>
                            </div>
                        </div>
                        @endif

                        <!-- name & email modal -->
                        @if($userData->Profile)
                        <div class="modal fade" id="name-edit-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Update Basic Profile</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['route' =>'frontend.client.userNameSave', 'method' => 'post']) }}
                                        <div id="form-response"></div>
                                        <span class="">
                                            {{ Form::label('first_name', 'First name') }}
                                            {{ Form::input('text','first_name',$userData->Profile->first_name, ['class'=> 'form-control']) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('middle_name', 'Middle name') }}
                                            {{ Form::input('text','middle_name',$userData->Profile->middle_name, ['class'=> 'form-control']) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('last_name', 'Last name') }}
                                            {{ Form::input('text','last_name',$userData->Profile->last_name, ['class'=> 'form-control']) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('email', 'Email') }}
                                            {{ Form::input('email','email',$userData->email, ['class'=> 'form-control','readonly'=>true]) }}
                                        </span>
                                        <div class="continue-btn">
                                            {{ Form::submit('Continue', ['class' => 'btn contact-info-button']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- END name & email modal -->

                        <div class="col-sm-6 padding-0">
                            <div id="all-profile" class="col-sm-12">
                                <h2>
                                    Personal Profile
                                    <span class="edit-icon" id="personal-profile-open"><i title="Edit" class="fa fa-pencil"></i></span>
                                </h2>
                                <div class="details">
                                    <p class="title">Address</p>
                                    <p class="value"> {{ $userData->Profile->first_address.' '.$userData->Profile->last_address }}</p>
                                </div>
                                <div class="details">
                                    <p class="title">City</p>
                                    <p class="value">{{ $userData->Profile->city }}</p>
                                </div>
                                <div class="details">
                                    <p class="title">State</p>
                                    <p class="value">{{ $userData->Profile->state->name }}</p>
                                </div>
                                <div class="details">
                                    <p class="title">Zip code</p>
                                    <p class="value">{{ $userData->Profile->zip }}</p>
                                </div>
                                <?php if (!empty($userData->Profile->phone_number)) { ?>
                                    <div class="details">
                                        <p class="title">Phone</p>
                                        <p class="value">{{ $userData->Profile->phone_number }}</p>
                                    </div>
                                <?php } ?>
                                <div class="details">
                                    <p class="title">Date of birth</p> 
                                    <p class="value">{{ date('d M Y', strtotime($userData->Profile->dob)) }}</p>
                                </div>
                                <div class="details">
                                    <p class="title">Gender</p>
                                    <p class="value">
                                        <?php
                                        if ($userData->Profile->sex == 1) {
                                            echo "Male";
                                        } else if ($userData->Profile->sex == 2) {
                                            echo "Female";
                                        }
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <!-- personal profile modal -->
                        <div class="modal fade" id="personal-profile-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Update Personal Profile</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['route' =>'frontend.client.personalProfileSave', 'method' => 'post']) }}
                                        <div id="form-response"></div>
                                        <span class="">
                                            {{ Form::label('first_address', 'Address Line 1') }}
                                            {{ Form::input('text','first_address',$userData->Profile->first_address, ['class'=> 'form-control','required'=>true]) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('last_address', 'Address Line 2') }}
                                            {{ Form::input('text','last_address',$userData->Profile->last_address, ['class'=> 'form-control','required'=>true]) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('city', 'City') }}
                                            {{ Form::input('text','city',$userData->Profile->city, ['class'=> 'form-control','required'=>true]) }}
                                        </span>
                                        <span class="input-span">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            {{ Form::label('state', 'State') }}
                                            {{ Form::select('state_id',$states, $userData->Profile->state_id,['class'=> 'form-control selectboxit','required'=>true]) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('zip', 'Zip code') }}
                                            {{ Form::input('text','zip',$userData->Profile->zip, ['class'=> 'form-control','required'=>true]) }}
                                        </span>
                                        <span class="">
                                            {{ Form::label('dob', 'Date of birth') }}
                                            {{ Form::input('date','dob', $userData->Profile->dob,['class' =>'form-control','pattern'=>'[0-9]{4}-[0-9]{2}-[0-9]{2}','required'=>true]) }}
                                        </span>
                                        <span class="input-span radio-input">
                                            <p class="left-subheading">Gender</p> 
                                            <label class="radio-custom-label">
                                                female
                                                <input type="radio" name="sex" value="2" <?php echo ($userData->Profile->sex == 2 ? 'checked' : ''); ?>>
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">
                                                male
                                                <input type="radio" name="sex" value="1" <?php echo ($userData->Profile->sex == 1 ? 'checked' : ''); ?> class="male">
                                                <span class="radio-icon"></span>
                                            </label>
                                        </span>
                                        <span class="input-span">
                                            <!--<div class="col-sm-8 inner-left">--> 
                                            {{ Form::label('phone_number', 'Phone Number') }}
                                            {{ Form::input('text','phone_number',$userData->Profile->phone_number, ['class'=> 'form-control','required'=>true]) }}
                                            <!--                                            </div>
                                                                                        <div class="col-sm-4 inner-right"> 
                                                                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                                            {{-- Form::label('phone_type', 'Type') --}}
                                                                                            {{-- Form::select('phone_type', config('constant.phone_type_inverse'),1, ['class'=> 'form-control']) --}}
                                                                                        </div>-->
                                        </span>
                                        <div class="continue-btn">
                                            {{ Form::submit('Continue', ['class' => 'btn contact-info-button']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </div>
                        <!-- END personal profile modal -->

                        <!--Employee-->
                        <div class="col-sm-6 padding-0">
                            <div id="all-profile" class="col-sm-12">
                                <h2>
                                    Employment profile
                                    <span class="edit-icon" id="employee-info-open"><i title="Edit" class="fa fa-pencil"></i></span>
                                </h2>
                                <div class="details">
                                    <p class="title">Employment status</p>
                                    <p class="value">
                                        <?php
                                        echo config('constant.employement_status')[$financialData->employment_status];
                                        ?>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade" id="employee-info-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Update employment profile</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['route' =>'frontend.client.employmentProfileSave', 'method' => 'post']) }}
                                        <div id="form-response"></div>
                                        <span class="input-span">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            {{ Form::label('employment_status', 'Employment status') }}
                                            {{ Form::select('employment_status', config('constant.employement_status'),$financialData->employment_status, ['class' => 'selectboxit']) }}
                                        </span>
                                        <div class="continue-btn">
                                            {{ Form::submit('Continue', ['class' => 'btn contact-info-button']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </div>
                        <!--END Employee-->

                        <!--Employee-->
                        <div class="col-sm-6 padding-0">
                            <div id="all-profile" class="col-sm-12">
                                <h2>
                                    Financial profile
                                    <span class="edit-icon" id="financial-back-open"><i title="Edit" class="fa fa-pencil"></i></span>
                                </h2>
                                <div class="details">
                                    <p class="title">Tax filing status</p>
                                    <p class="value">
                                        <?php
                                        echo config('constant.tax_filing_status')[$financialData->tax_filing_status];
                                        ?>
                                    </p>
                                </div>
                                <div class="details">
                                    <p class="title">Your annual income</p>
                                    <p class="value">
                                        <?php
                                        echo $financialData->annual_income;
                                        ?>
                                    </p>
                                </div>
                                <?php if ($financialData->spouse_annual_income > 0) { ?>
                                    <div class="details">
                                        <p class="title">Your spouse's annual income</p>
                                        <p class="value">
                                            <?php echo $financialData->spouse_annual_income; ?>
                                        </p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="modal fade" id="financial-back-modal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Update financial profile</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['route' =>'frontend.client.financeProfileSave', 'method' => 'post']) }}
                                        <div id="form-response"></div>
                                        <span class="input-span">
                                            <i class="fa fa-angle-down tax-filling-arrow" aria-hidden="true"></i>
                                            <label> Tax filing status </label>
                                            {{ Form::select('tax_filing_status', config('constant.tax_filing_status'),$financialData->tax_filing_status, ['class' => 'selectboxit', 'id'=>'tax_filing_status']) }}
                                        </span>

                                        <!--Used for spouse_has_twpm_account  visibilty-->
                                        @php $value_filled = 'display: none;';$checked = false  @endphp
                                        @if($financialData->spouse_has_twpm_account == 1)
                                        @php $value_filled = 'display: block;'; $checked = true @endphp
                                        @endif

                                        <div class="spouse-account-checkbox two-options-auth" style="{{ $value_filled }}">
                                            <label class="checkbox-custom-label input-span">
                                                {{ Form::input("checkbox",'spouse_has_twpm_account', 1, ['class'=>'form-control', 'checked'=>$checked]) }}
                                                <span class="checkbox-icon"></span> 
                                                <p>My spouse also has a Total Wealth account</p>
                                            </label>
                                        </div>
                                        <div class="clearfix"></div>
                                        <span class="input-span">
                                            <label> Your annual income </label>
                                            {{ Form::input('number','annual_income', $financialData->annual_income, ['placeholder' => '$', 'class'=>'form-control','required'=>true]) }}
                                        </span>

                                        <span class="input-span two-options-auth" style="{{ $value_filled }}">
                                            <label for="label"> Spouse/partner's first name </label>
                                            {{ Form::input('text','spouse_name', $financialData->spouse_name, ['placeholder' => '', 'class'=>'spouse_name form-control']) }}
                                           
                                        </span>
                                        <span class="input-span two-options-auth" style="{{ $value_filled }}">
                                            <label> Your spouse's annual income </label>
                                            {{ Form::input('number','spouse_annual_income', $financialData->spouse_annual_income, ['placeholder' => '$', 'class'=>'form-control spouse_annual_income']) }}
                                        </span>

                                       

                                        <div class="continue-btn">
                                            {{ Form::submit('Continue', ['class' => 'btn contact-info-button']) }}
                                            {!! Form::close() !!}
                                        </div>
                                    </div> 

                                </div>
                            </div>
                        </div>
                        <!--END Employee-->




                        @foreach ($profileData as $profile)
                        <div class="col-sm-6 padding-0">
                            <div id="all-profile" class="col-sm-12 right">
                                <h2>
                                    {{ $profile->name}}
                                    <span class="edit-icon edit-icon-profiles" id=<?php
                                    echo str_replace(' ', '-', strtolower($profile->name));
                                    ?>><i title="Edit" class="fa fa-pencil"></i></span>
                                </h2> 
                                @foreach ($profile->serviceQuestionWithAnswers as $serviceQuestion)
                                <div class="details">
                                    <p class="title">{{$serviceQuestion->question}}</p>
                                    <p class="value">{{ $serviceQuestion->serviceQuestionUser->isNotEmpty() ?$serviceQuestion->serviceQuestionUser[0]['value']:'..' }}</p>
                                </div>
                                @endforeach
                            </div>
                        </div>

                        <!-- financial profile modal -->
                        <div class="modal fade all-profile-modal" id="<?php
                        echo str_replace(' ', '-', strtolower($profile->name)) . "-modal";
                        ?>" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Update {{ $profile->name}}</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['route' =>'frontend.client.updateClientProfileData', 'method' => 'post']) }}
                                        <div class="form-response"></div>
                                        @foreach ($profile->serviceQuestionWithAnswers as $key => $serviceQuestion)
                                        <span class="">
                                            {{Form::text('userdata['.$key.'][value]', $serviceQuestion->serviceQuestionUser->isNotEmpty() ?$serviceQuestion->serviceQuestionUser[0]['value']:'',['class' => 'input__field input__field--hoshi'])}}
                                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                                <span class="input__label-content input__label-content--hoshi">{{$serviceQuestion->question}}</span>
                                            </label>
                                        </span>
                                        {{ Form::hidden('userdata['.$key.'][service_question_id]',  $serviceQuestion->id) }}
                                        @if($serviceQuestion->serviceQuestionUser->isNotEmpty())
                                        {{ Form::hidden('userdata['.$key.'][id]', $serviceQuestion->serviceQuestionUser[0]['id']) }}
                                        @endif
                                        @endforeach
                                        {{ Form::submit('Continue', ['class' => 'btn contact-info-button']) }}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="settings-inner profile-lists">
                        <div class="col-sm-12 settings-title">
                            <h2>Account Settings</h2>
                        </div>

                        <div class="settings-boxes">
                            <h2>Notifications</h2>
                            <div class="details">
                                <p class="title">New document added</p>
                                <div class="settings">
                                    <div>
                                        <span>SMS</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                    <div>
                                        <span>Email</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch2" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch2"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="details">
                                <p class="title">New document added</p>
                                <div class="settings">
                                    <div>
                                        <span>SMS</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch3" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch3"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                    <div>
                                        <span>Email</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch4" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch4"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="settings-boxes right">
                            <h2>Notifications</h2>
                            <div class="details">
                                <p class="title">New document added</p>
                                <div class="settings">
                                    <div>
                                        <span>SMS</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch5" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch5"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                    <div>
                                        <span>Email</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch6" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch6"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="details">
                                <p class="title">New document added</p>
                                <div class="settings">
                                    <div>
                                        <span>SMS</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch7" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch7"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                    <div>
                                        <span>Email</span>
                                        <div class="switch switch--horizontal">
                                            <input id="radio-a" type="radio" name="first-switch8" checked="checked"/>
                                            <input id="radio-b" type="radio" name="first-switch8"/>
                                            <span class="toggle-outside"><span class="toggle-inside"></span></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="security-inner profile-lists">
                        <div class="col-sm-12 security-title">
                            <h2>Security</h2>
                            <p>We take the security of your information very seriously. Keeping these settings up-to-date safeguards your account even more.</p>
                        </div>

                        <div class="col-sm-6 padding-0">
                            <div class="security-boxes">
                                <h2>Password</h2>
                                <p>Your password protects your account, so make sure you choose a
                                    strong password. A strong password contains a mix of numbers,
                                    letters, and symbols. It is hard to guess and is only used for this
                                    account.</p>
                                <a href="" class="btn btn-primary" id="change-password-btn">Change Password</a>
                                <p class="change-pass-succ-msg success-message"></p>

                                <div class="clearfix"></div>

                                <div id="change-password-div" style="display: none" >
                                    {{ Form::open(['id'=>'changePasswordForm', 'role' => 'form', 'method' => 'patch']) }}
                                    <div class="form-group">
                                        {{ Form::label('old_password', trans('validation.attributes.frontend.old_password'), ['class' => 'col-md-4 control-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::input('password', 'old_password', null, ['class' => 'form-control','required', 'placeholder' => trans('validation.attributes.frontend.old_password')]) }}
                                            <p class="err-messages old-password-err"></p>
                                        </div>
                                    </div>

                                    <div class=" clearfix botttom-margin-10"></div>

                                    <div class="form-group">
                                        {{ Form::label('password', trans('validation.attributes.frontend.new_password'), ['class' => 'col-md-4 control-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::input('password', 'password', null, ['class' => 'form-control','required', 'placeholder' => trans('validation.attributes.frontend.new_password')]) }}

                                        </div>
                                    </div>

                                    <div class="clearfix botttom-margin-10"></div>

                                    <div class="form-group">
                                        {{ Form::label('password_confirmation', 'Confirm Password', ['class' => 'col-md-4 control-label']) }}
                                        <div class="col-md-8">
                                            {{ Form::input('password', 'password_confirmation', null, ['class' => 'form-control', 'required', 'placeholder' => trans('validation.attributes.frontend.new_password_confirmation')]) }}
                                            <p class="err-messages new-password-err"></p>
                                        </div>
                                    </div>

                                    <div class=" clearfix botttom-margin-10"></div>
                                    <div class=" clearfix botttom-margin-10"></div>
                                    <div class="pull-left">
                                        <a href="" class="btn sm-btn btn-danger" id="cancel-btn">Cancel</a>
                                    </div>
                                    <div class="pull-right">
                                        {{ Form::submit(trans('labels.general.buttons.update'), ['class' => 'btn sm-btn btn-success', 'id' => 'change-password-submit-btn']) }}
                                    </div>
                                    {!! Form::close() !!}
                                </div>

                            </div>
                        </div>
                        <div class="col-sm-6 padding-0 hide">
                            <div class="security-boxes right">
                                <h2>Two Factor Authentication
                                    @if ($userData->two_factor_auth === 1)
                                    <span class="status">On</span></h2>
                                @else
                                <span class="status">Off</span></h2>
                                @endif

                                <p>You can add an additional layer of protection with Two-Factor
                                    Authentication. You will be asked to enter a single-use code delivered
                                    to your phone or generated by an authenticator app to sign in. Even if
                                    somebody knows your password, they cannot access your account.</p>

                                @if ($userData->two_factor_auth === 1)
                                <input type="hidden" name="two_factor_auth" id="two-factor-auth" value ="0"> 
                                <a href="" class="btn btn-primary" id="two-factor-auth-btn">Disable two-factor authentication</a>
                                @else
                                <input type="hidden" name="two_factor_auth" id="two-factor-auth" value ="1"> 
                                <a href="" class="btn btn-primary" id="two-factor-auth-btn">Enable two-factor authentication</a>
                                @endif
                                <p class="success-message two-fact-success"></p>
                                <p class="err-messages two-fact-error" ></p>
                                <div class='mobile-verification-link' style="display: none;">
                                    <a href="{{ route('frontend.client.mobileVerify', config('constant.subdomain')) }}">Verify Your Mobile Number</a> 
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script>
    var changePasswordRoute = '{{route("frontend.auth.password.change")}}';
    changeTwoFactorAuthRoute = '{{route("frontend.client.changeTwoFactorAuth")}}';
</script>
<script src="{{ asset('js/client-profile.js') }}"></script>
@stop