@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections" data-label='employer'>
                            @include('frontend.client.serviceQuestions.question_26')
                        </section>
                    </fieldset>   
                    <h3></h3>  
                    <fieldset>
                        <section class="sections" data-label='documents'>
                            @include('frontend.client.serviceQuestions.question_25')
                        </section>
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections" data-label='finish'>
                            @include('frontend.client.serviceQuestions.question_27')
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<!--<script src="{{ asset('js/dashboard.js') }}"></script>-->
<script src="{{ asset('js/life-insurance.js') }}"></script>
<script>
$(document).ready(function () {

    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";



    $(document).on('keypress', '.taxable-income', function () {

        $('.lds-dual-ring').show();

    });

    $("select").selectBoxIt();
    $('.tax_filling_status').css('pointer-events', 'none');
    $(document).on('click', '.contact-modal-show', function (e) {
        $('#contact-us').show();
        $('.contact-response').html('');
        $('#get-started-modal').modal();
        e.preventDefault();
    });

    if ($('#services-question-form-p-0').is(':visible')) {
        $('a[href="#next"]').text('employer').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    } else {
    }

    $(document).ready(function () {
        $('input[type="file"]').change(function (e) {
            $('.progress-bar').width('0%');
            if (e.target.files.length) {
                var fileName = e.target.files[0].name;
                $('.add-input').append('<tr><td> <input class="document-name" disabled value=' + fileName + '>  </td>  <td><i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash-o enable-input" aria-hidden="true"></i></td> </tr>');
            }
        });
        $(document).on('click', '.enable-input', function () {
            $(this).closest('tr').find('.document-name').prop('disabled', false).css("border", "1px solid #a7a7a7");
        });
        $('#saveFile').on('click', function () {
            $('#filesLabel,#files').attr('disabled', false).css('color', '#1d99d4', 'border', '1px solid #1d99d4');
        });
    });



    $('.taxable-income').keyup(function () {

        var taxFilling = $('.taxBracket').find('input[name="answer[26][estimated_taxable_income]"]').val();

        $.ajax({
            type: 'GET',
            url: ajaxUrl,
            data: {
                taxableIncome: taxFilling
            },
            success: function (resp) {
                $('.tax-current-value').val(resp);
                $('.lds-dual-ring').hide();
            },
            error: function (error) {
//                twpmApp.ajaxInputError(error, $("#example-advanced-form"));
            }
        });

    });

    $(document).on("change", ".upload-file", function (e) {
        var $this = $(this);
        var files = $this[0].files;
        var upload = new Upload(files);
        upload.doUpload();
    });
    //file upload code
    var allFiles = [];
    var Upload = function (files) {
        allFiles = [];
        $.each(files, function (i, file) {
            allFiles[i] = file;
        });
    };

    Upload.prototype.getType = function (i) {
        return allFiles[i].type;
    };
    Upload.prototype.getSize = function (i) {
        return allFiles[i].size;
    };
    Upload.prototype.getName = function (i) {
        return allFiles[i].name;
    };
    Upload.prototype.doUpload = function () {
        $('#filesLabel,#files').attr('disabled', true).css('color', '#a7a7a7', 'border', '1px solid #a7a7a7');
        $('.progress-bar').width('0%');
        var that = this;
        var formData = new FormData();
        var imageError = false;
        $.each(allFiles, function (i, file) {
            formData.append("file", file, allFiles[i].name);
        });
        $('.progress').show();
        // add assoc key values, this will be posts values
        $.ajax({
            type: "POST",
            url: ajaxUploadDocument,
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) {
                    myXhr.upload.addEventListener('progress', that.progressHandling, false);
                }
                return myXhr;
            },
            success: function (response) {
                $('.progress-bar').width('100%');
            },
            error: function (error) {
                $('.progress-bar').width('0%');
                // handle error
            },
            async: true,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000
        });
    };
    var fieldName;
    Upload.prototype.progressHandling = function (event) {
        var percent = 0;
        var position = event.loaded || event.position;
        var total = event.total;
        if (event.lengthComputable) {
            percent = Math.ceil(position / total * 100);
        }
        $('.progress-bar').width(percent);
        // update progressbars classes so it fits your code
    };
    $("select").selectBoxIt();


    $(document).on('blur', '.document-name', function () {
        $(this).prop('disabled', true).css("border", "0");
    });
    $(document).on('click', '.fa-trash-o', function () {
        $(this).closest('tr').remove();
        $('.progress-bar').width('0%');
        if ($('.add-input tr').length == 0) {
            $('.progress').hide();
        }
    });
});
</script>   
<script>
    $(document).ready(function () {
        //$("select").selectBoxIt();

        $(document).on('click', '.fa-pencil', function () {
            $(this).closest('.fileUploadRow').find('.fa-trash, .editUploadRow').hide();
            $(this).closest('.fileUploadRow').find('.fileSave').show();
            $(this).closest('.fileUploadRow').find("input, select").addClass('edit').removeAttr("readonly");
            $(this).closest('.fileUploadRow').find('.fa-angle-down').show();
        });
        var username = "{{ Session::get('loggedInUserName') }}";
        var trcount = 0;
        $(".open_button").on('click', function () {
            $.FileDialog({multiple: false})
                    .on('files.bs.filedialog', function (ev) {

                        var nooftr = $('.fileUploadRow').length,
                                files = ev.files,
                                formData = new FormData(),
                                i = 0;
                        files.forEach(function (f) {


                            var inputs = $(".hello");

                            for (var i = 0; i < inputs.length; i++) {
                                if ($('#newName').val() == $(inputs[i]).val()) {
                                }
                            }




                            $('.add-file table tbody').append('<tr data-attr= " ' + nooftr + ' " class="fileUploadRow appendrow col-xs-12 paddingLeft0" style="border-bottom:0"><td  class="col-xs-6 col-sm-6 paddingLeft0 pull-left">  \n\
        <input name="answer[27][files][' + nooftr + '][file_name]" readonly="readonly" type="text" class="hello" value=' + $('.file-uploader .fileNameSaver').val() + '><input name="answer[27][files][' + nooftr + '][file_path]" type="hidden" class="hello"> </td>  \n\
        <td class="col-xs-1 col-sm-6  paddingLeft0 pull-left">&nbsp;</td>\n\
        <td class="col-xs-4 col-sm-6 pull-left"><span class="pull-right"> <i class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i>    <i title="Edit" class="fa fa-pencil editUploadRow" style="display:none;" aria-hidden="true"></i>  \n\
        <i title="Delete" class="fa fa-trash deleteUploadRow" style="display:none;" aria-hidden="true"></i> </span></td></tr>');
                            $('.fileSave').closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events', 'none');
                            formData.append("file", f, f.name);
                            trcount++;
                            i++;
                        });



                        $.ajax({
                            type: "post",
                            url: ajaxUploadDocument,
                            async: true,
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            timeout: 60000,
                            success: function (response) {
                                $('input[name="answer[27][files][' + nooftr + '][file_path]"]').val(response.message);
                            },
                            xhr: function () {
                                var myXhr = $.ajaxSettings.xhr();
                                return myXhr;
                            }
                        });
                    })
                    .on('cancel.bs.filedialog', function (ev) {
                    });
        });
        // ----------------- File uploader js finish
        $(document).on('click', '.deleteUploadRow', function () {

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $(this).closest('tr').remove();

                            swal("Your file has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
        });
        var executorList = [];
        executorList[$(this).attr('data-index')] = {
            fileNameSaver: $('.file-uploader .fileNameSaver').val()
        };
        // for id increment finsih
        $(document).on('click', '.bfd-ok', function () {
            $(this).closest('.appendrow').find('td:last-child .fa-trash').hide();
            $(this).closest('.appendrow').find('td:last-child .fileSave').show();
            $('.file-uploader .fileNameSaver').val(executorList.fileNameSaver);
            $('.fileUploadRow').addClass('fileNotSaved');
            if ($('.bfd-files').find('.bfd-info').length > 0) {
                $('.uploaderBtn').show();
                $('.open_button').hide();
            } else {
                $('.uploaderBtn').hide();
                $('.open_button').show();
            }

            $(".fileNameSaver").validate({
                ignore: ".ignore, :hidden",
                errorPlacement: function errorPlacement(error, element) {
                }
            });
        });
        // for id increment finsih

        $(document).each(function () {
            $(this).on('click', '.fileSave', function () {
                $(this).closest('.fileUploadRow').find("input[type=text]").attr("readonly", "readonly").addClass("fileSaved").removeClass('edit').addClass('hello');
                $(this).closest('.fileUploadRow').find('.fileSave').hide();
                $(this).closest('.fileUploadRow').find('.fa-trash, .editUploadRow').show().css('opacity', '.7');
                $(this).closest('.fileUploadRow').find('.error-alert').remove();
                $(this).closest('.fileUploadRow .fileSaved').css('border', '0px ');
                $('.uploaderBtn').hide();
                $('.open_button').show();
                $(this).closest('.fileUploadRow').css('border-bottom', '1px solid #bbb3b3');
                $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events', 'auto');
            });
        });
        $(document).on('click', '.editUploadRow', function () {
            $(this).closest('.fileUploadRow').find('.fa-trash, .editUploadRow').hide();
            $(this).closest('.fileUploadRow').find('.fileSave').show();
            $(this).closest('.fileUploadRow').find("input[type=text], select").addClass('edit').removeAttr("readonly").removeClass('hello');
            $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events', 'none');
            $(this).closest('.fileUploadRow').find('.fa-angle-down').show();
        });
        // file uploader js starts 
        $(document).on('click', '.open_button', function () {
            $('.bfd-files').closest('.inner-left').find('.error-alert').remove();
            $('.bfd-files').append("<span class='inner-left'><span class='col-xs-12 paddingLeft0 paddingRight0'><input class='form-control fileNameSaver hide'  id='newName'required='required' placeholder='Enter File Name'></span></span>");
            if ($('.fileNameSaver').val() == "") {
                $('.bfd-ok').prop("disabled", "true");
            }
        });
        $('.editUploadRow, .deleteUploadRow').show();
        $('.fileSave').hide();
        // =----------- get file size while uploading file
        $(document).on('change', 'input[type=file]', function () {
            if ($('.bfd-files').find('.bfd-info').length == 1) {
                $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
                $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
            }

            $('.fileNameSaver').removeClass('hide');
            var iSize = ($('input[type=file]')[0].files[0].size / 1024);
            if (iSize / 1024 > 1)
            {
                if (((iSize / 1024) / 1024) > 1)
                {
                    iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
                    $("#lblSize").html(iSize + "Gb");
                } else
                {
                    iSize = (Math.round((iSize / 1024) * 100) / 100)
                    $("#lblSize").html(iSize + "Mb");
                }
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#lblSize").html(iSize + "kb");
            }



        });
        // ------------ get file size while uploading file finish 
    });

    $(document).on('keyup change', '.bfd-files input', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        $('.bfd-ok').removeAttr('disabled');

        var inputs = $(".hello");

        for (var i = 0; i < inputs.length; i++) {

            if ($(this).val() && $('#newName').val() == $(inputs[i]).val()) {
                $('.bfd-ok').prop("disabled", "true");
                $(this).closest('.inner-left').append('<label class="error error-alert">This name already exists.</label>');
                return;


            } else if (!$(this).val()) {
                $(this).closest('.inner-left').append('<label class="error error-alert">This field is required.</label>');
                $('.bfd-ok').prop("disabled", "true");
            }
        }

    });
    $(document).on('keyup change', '.edit', function () {
        $(this).closest('td').find('.parent-error-alert').remove();
        var inputs = $(".hello");
        for (var i = 0; i < inputs.length; i++) {
            if ($(this).val() == $(inputs[i]).val()) {
                $(this).addClass('col-sm-5');
                $(this).closest('td').find('.parent-error-alert').remove();
                $(this).closest('td').find('.error-alert').remove();
                $(this).closest('td').append('<div class="col-sm-7 parent-error-alert" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">name already exist</label></div>');
                $(this).closest('tr').find('#saveFile').css('pointer-events', 'none');
                $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'none');
                return;
            } else if (!$(this).val()) {
                $(this).closest('td').find('.parent-error-alert').remove();
                $(this).closest('td').find('.error-alert').remove();
                $(this).closest('td').append('<div class="col-sm-7" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">This field is required.</label></div> ');
            } else {
                $(this).closest('tr').find('#saveFile').css('pointer-events', 'all');
                $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'all');
            }
        }

    });

    $(document).on('change', '.document-type', function () {
        var selectedVal = $(this).closest('.fileUploadRow ').find('.document-type  option:selected ');
        $(this).closest('.fileUploadRow ').find(".document-selected").val($(selectedVal).text());
    });

</script>
<style type="text/css">
    .file-size{float: left;  width: 100%; text-align: right; color: #ABB1C9; font-size: 12px; margin: 0; font-weight: normal}
    .modal-header{border-radius: 5px 5px 0 0;}
    @media (max-width: 768px){
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow{
            display: inherit;
        }
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow td .error-alert {
            position: absolute;
            top: 29px; 
            left: 0;
            padding: 4px 10px !important;
            width: 150px !important;
        }
        .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{
            background: #fff;
            margin-right: 8px;
            padding-left: 8px;
            left: auto !important;
            right: 0;
            top: 13px !important;
        }
    }

    .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{ top: 13px !important;}
</style>
<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script> 
@endsection