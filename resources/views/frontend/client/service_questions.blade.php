@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
{{ Html::style(asset('css/service-questions.css')) }}   
@stop

@section('content')
<div class="dashboard">

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content client-services">
                @include('frontend.includes.client_header')

                <div class="col-sm-8 products-list">
                    {{ Form::open(['route' =>['frontend.client.updateServiceQuestionAnswer'],'class' => 'form-horizontal', 'id'=>'clientService-form', 'role' => 'form', 'method' => 'post', 'files'=>'true']) }}
                    <input type="hidden" name="user_id" id="user-id" value ="{{ $logged_in_user->id }}"> 
                    <input type="hidden" name="service_id"  value ="{{ $serviceId }}"> 

                    @foreach ($questions as $key => $questionArray)
                    @include('frontend.client.includes.serviceQuestionsClient')
                    @endforeach

                    <?php $key = 40; ?>
                    @foreach ($unAnsweredQuestions->serviceQuestion as $questionArray)
                    @include('frontend.client.includes.serviceQuestionsClient-novalue')
                    <?php $key = $key + 1; ?>
                    @endforeach

                    {{ Form::submit('Save', ['class' => 'btn contact-info-button-small', 'id' => 'save-answer']) }}
                    {!! Form::close() !!}
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop