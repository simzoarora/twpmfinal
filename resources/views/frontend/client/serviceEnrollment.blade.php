@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles')
{{ Html::style(asset('css/selectedService.css')) }} 
@stop
@section('content')

@if(Session::has('services'))
<?php $services = Session::get('services'); ?> 
@endif
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                <?php $page_title = 'Service Enrollment'; ?>
                @include('frontend.includes.client_header')

                <div class="bill-summary recommended-service-div">
                    <div class="col-sm-6 service-payment">

                        <h5>ADD A SERVICE</h5>
                        <p> <span>Selected Services</span></p>
                        <?php foreach ($services as $service) { ?>
                            <p class="grey-text">{{$service['title']}} </p>
                            <!--                            <div class="delete-service">
                                                            <i title="Delete" class="fa fa-trash-o" aria-hidden="true"></i>
                                                        </div> -->
                        <?php } ?>
                        <a class="italics" href="{{ route('frontend.client.recommendedServices', config('constant.subdomain')) }}">
                            <span>+ <i>Add another service</i> </span> </a>
                        <p> <span>Payment method</span></p>
                        <ul class="existing-cards" id="existingCards">
                            <?php if (isset($allCards) && !empty($allCards)) { ?>
                                <?php foreach ($allCards['data'] as $key => $card) { ?>
                                    <li data-id="{{$card['id']}}" class="<?php echo!$key ? 'active-card' : ''; ?>">xxxx xxxx xxxx {{$card['last4']}} <i title="Delete" class="fa fa-trash-o" aria-hidden="true"></i></li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                        <div class="payment-method">
                            <button type="button" class="btn btn-default"   data-toggle="modal" data-target="#add-card-modal">
                                <i class="fa fa-plus-circle" aria-hidden="true"></i>Add a payment method</button>
                        </div>
                    </div>

                    <div class="checkout-wrapper">
                        <div class="checkout">
                            <div class="zig-zag-top col-sm-4 col-sm-offset-1">
                                <h5>CHECKOUT</h5>

                                <div class="section-left">
                                    <div class="service-details">
                                        <?php foreach ($services as $service) { ?>
                                            <div class="row">
                                                <div class="service-name col-sm-6 col-xs-6">
                                                    <p>{{$service['title']}}</p>
                                                </div>
                                                <div class="amount col-sm-6 col-xs-6">
                                                    <p>${{$service['amount']}}</p>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="due-date">
                                        <div class="row">
                                            <div class="due-today col-sm-6 col-xs-6">
                                                <p>Due today</p>
                                            </div>
                                            <div class="due-amount col-sm-6 col-xs-6">
                                                <p>${{$dueAmount}}</p>
                                            </div>
                                        </div>
                                        <button  class="btn btn-primary submit-button " id="payNowBtn">
                                            <span class="pre-state-msg">PAY NOW!</span>
                                            <span class="current-state-msg ">Sending...</span>
                                            <span class="done-state-msg ">Done!</span>
                                        </button>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>    

<!-- Payment Modal -->
<div class="modal fade" id="add-card-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">List Your Credit/Debit Card</h4>
            </div>
            {{ Form::open(['route' => 'frontend.client.stripeCardPayment', 'class'=>'payment-form']) }}
            <div class="response"></div>
            <div class="details">
                {{ Form::label('name_on_card', 'Name on Card') }}
                {{ Form::input('text','name_on_card', null,['placeholder'=> 'Name on card' , 'class'=>'form-control booking-name']) }}

                {{ Form::label('number_on_card', 'Card Number') }}
                {{ Form::input('number','number_on_card', null,['placeholder'=> 'XXXX  XXXX  XXXX  XXXX' , 'class'=>'form-control number_on_card', 'required'=>true]) }}
            </div>
            <div class="inline-details col-sm-12">
                <div class="col-sm-4 expiration">
                    {{ Form::label('expiry', 'Expiration') }}
                    <div class="row">
                        {{ Form::input('number','exp_month', null,['placeholder'=> 'MM/', 'class'=>'form-control month card-expiry-month', 'required'=>true]) }}
                        {{ Form::input('number','exp_year', null,['placeholder'=> 'YY', 'class'=>'form-control year card-expiry-year', 'required'=>true]) }}
                    </div>
                </div>
                <div class="col-sm-3 ">
                    {{ Form::label('cvc', 'CVC') }}
                    {{ Form::input('number','CVC', null,['placeholder'=> '123' , 'class'=>'form-control cvc', 'required'=>true]) }}
                </div>
                <div class="col-sm-5">
                    <div class="row">
                        {{ Form::label('billing_zip_code', 'Billing Zip Code') }}
                        {{ Form::input('text','billing_zip_code', null,['placeholder'=> 'ZIP' , 'class'=>'form-control billing_zip_code']) }}
                    </div>
                </div>
                <button type="submit" class="btn btn-primary add-card">Add Card</button>
            </div>
            {{ Form::close() }}    
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script src="https://js.stripe.com/v2/"></script>
<script>
    Stripe.setPublishableKey("pk_test_tpnFppH4RyqLZO5EKsXHHzZ900MEknyECp");
    var servicePaymentLink = '{{route("frontend.client.stripePayment")}}',
     cardRemoveLink = '{{route("frontend.client.stripeCardRemove")}}';
     var user_confirmed='{{$user_confirmed}}';
</script>
 <script src="{{ asset('js/service-enrollment.js') }}"></script> 
@endsection