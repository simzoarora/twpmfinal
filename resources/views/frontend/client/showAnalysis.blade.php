@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; About You
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/signUpSixteen.css')) }}   
@stop

@section('content')
<?php
$stocks = str_replace('%', '', $result['Stocks']);
$bonds = str_replace('%', '', $result['Bonds']);
?>
<section class="bg-color"> 
    <div class="col-sm-12 header-bar"> 
        <div class="container">
            {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }}
            <p>Your Allocation</p>
        </div>    
    </div>
    <div class="container center">
        <p>Based on your answers,  this allocation may be a <br>good place to start your investment discussion with your Personal Advisor. </p>
    </div>
    <div class="container ">
        <div class="col-sm-12 contain-type">
            <div class="col-sm-3 left-side">
                <h4>INVESTMENT STRATEGY</h4>
                <p>{!!$result['portfolioType']?$result['portfolioType']:''!!}<i class="fa fa-question-circle-o que-font-awe" aria-hidden="true"></i></p>
                <h4 class="pd-top">TARGET ALLOCATION</h4>
                <p> <span class="count">{!!$result['Stocks']?$result['Stocks']:''!!}</span>  Stocks </p>

            </div>
            <div class="col-sm-6 daughnut-style">
                <div class="hover-show">
                    <p>{!!$result['portfolioDescription']?$result['portfolioDescription']:''!!}
                    </p>
                </div>
                <canvas id="myChart">
                </canvas>
                <div class="absolute">
                    <div class="stocks">
                        <h3>{!!$stocks!!}</h3>
                        <p>stocks</p>
                    </div>
                    <div class="bonds">
                        <h3>{!!$bonds!!}</h3>
                        <p>bonds</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-3 right-side hide">
                <h4>BREAKDOWN</h4>
                <p class="black-color">Stocks</p>
                <p>Cash<span class="count">{!!$result['Cash']?$result['Cash']:''!!}</span><p class="percentage">  </p> </p>
                <p> Alternative<span class="count">{!!$result['Alternative']?$result['Alternative']:''!!}</span><p class="percentage">  </p> </p>
<!--                <p>US Middle Cup<span class="count">5.5</span><p class="percentage"> % </p> </p>
                <p>US Small Cup<span class="count">4.5</span><p class="percentage"> % </p> </p>
                <p class="black-color pd-top-right">Bonds</p>
                <p> Muncipal Bonds<span class="count">5.5</span><p class="percentage"> % </p> </p>
                <p> US Corp.Bonds<span class="count">0.5</span><p class="percentage"> % </p> </p>
                <p> International Bonds<span class="count">2.5</span><p class="percentage"> % </p> </p>-->
            </div>
        </div>
        <div class="col-sm-12 grey-p">
            <div class="col-sm-3">
                <!--<a href="#">Change my answers</a>-->
            </div>
            <div class="col-sm-6 text-center">
                <a href="{{ route('frontend.client.contactInfo', [config('constant.subdomain')]) }}" class="btn btn-lg">Looks good. Let's Continue.</a> 
            </div>
<!--            <div class="col-sm-3">
                <a href="logout" class="float-right">Save and finish later</a>
            </div>-->
        </div>
    </div>
    <div class="container">
        @include('frontend.includes.footer')
    </div>
</section>

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.js"></script>
<script>
new Chart(document.getElementById("myChart"),
        {"type": "doughnut",
            "data": {
                "datasets": [{
                        "data": ['{{ $bonds }}', '{{ $stocks }}'],
                        "backgroundColor": ["#006fcc", "#0258a0"],
                        "borderColor": ["#006fcc", "#0258a0"],
                    }],
                labels: ['Bonds', 'Stocks']
            },
            "options": {
                "cutoutPercentage": 75,
                legend: {
                    display: false
                },
            }

        });
$('.count').each(function () {
    var $this = $(this),
            initVal = $this.text();
    $this.prop('Counter', 0).animate({
        Counter: $this.text()
    }, {
        duration: 1000,
        step: function (now) {
            $this.text(Math.ceil(now));
        },
        complete: function () {
            $this.text(initVal);
        }
    });
});
$(document).ready(function () {
    $(".que-font-awe").hover(function () {
        $(".hover-show").show();
    }, function () {
        $(".hover-show").hide();
    });
});

</script>
@stop