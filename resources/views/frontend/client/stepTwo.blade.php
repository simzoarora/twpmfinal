@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; Contact Information 
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    @include('frontend.includes.client_register_header')

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 1: Contact Information</p>
            <div class="col-sm-3 left-bottom-border"></div> 
            <div class="col-sm-9 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Contact Information</h2>
                <p class='heading-info'>Don't worry, we'll never sell your info to anyone.</p>

                {{ Form::open(['route' =>'frontend.client.stepTwoRegister','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                <input type="hidden" name="user_id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif">
                <div class="form-group" id='content-information'>
                    <div class="first-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('first_name')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('first_name', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">First name</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('first_name'); ?></p>
                    </div>

                    <div class="mi-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('middle_name')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('middle_name', null, ['class' => 'input__field input__field--hoshi']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">MI</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('middle_name'); ?></p>
                    </div>

                    <div class="last-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('last_name')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('last_name', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Last name</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('last_name'); ?></p>
                    </div>

                    <div class="">
                        <span class="input input--hoshi <?php
                        if ($errors->first('first_address')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('first_address', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Address line 1</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('first_address'); ?></p>
                    </div>

                    <div class="">
                        <span class="input input--hoshi <?php
                        if ($errors->first('last_address')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('last_address', null, ['class' => 'input__field input__field--hoshi']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Address line 2</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('last_address'); ?></p>
                    </div>

                    <div class="city-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('city')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('city', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">City</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('city'); ?></p>
                    </div>

                    <div class="st-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('state')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('state', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">ST</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('state'); ?></p>
                    </div>

                    <div class="zip-code-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('zip')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('zip', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Zip Code</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('zip'); ?></p>
                    </div>

                    <div class="phone-err">
                        <span class="input input--hoshi <?php
                        if ($errors->first('phone_number')) {
                            echo 'input-error';
                        }
                        ?>">
                            {{ Form::text('phone_number', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Phone</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php
                            if (!empty($errors->first('phone_number'))) {
                                echo $errors->first('phone_number');
                            } elseif (!empty($errors->first('mobile_number'))) {
                                echo $errors->first('mobile_number');
                            }
                            ?></p>
                    </div>

                    <div class="mobile-btn-err">
                        {{ Form::select('phone_type', config('constant.phone_type_inverse'), null, ['class' => 'selectboxit','required']) }}
                        <p class="err-messages msg-err"><?php echo $errors->first('phone_type'); ?></p>
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button>
                    </div>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/step-two.js') }}"></script>
@stop