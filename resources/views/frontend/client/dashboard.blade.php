@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/servicesQuestion.css')) }}
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop

@section('content')
<div class="dashboard wrapper">
    <div id="dashboard-content">
        <div class="container-fluid get-started">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content client-resources">
                <?php $page_title = 'Welcome ' . Session::get('loggedInUserName'); ?>
                @if(Session::has('confirmation_message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">     {!! session()->get('confirmation_message')!!}      </p>
                @endif

                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <p class="grey-active-service">
                        <a href="#" id="your-services">My Services</a>
                        <span><a href= "#" id="all-services" class="active">All services</span></a>
                    </p>
                    <div class="row service-name all-services">
                        <div class="previous"> </div>
                        <div class="service-options owl-carousel"> 
                            <?php
                            if (isset($recommendedServices)) {
                                foreach ($recommendedServices as $service) {
                                    ?>
                                    <div class='col-sm-12 products-list dashboardDiv serviceList' data-id="{{$service['id']}}" data-service-is-availed="<?php echo ($service['service_users'] && $service['service_users'][0] && $service['service_users'][0]['status'] == 1) ? 1 : 0 ?>">
                                        <div class="nested-div">
                                            <div class="position-absolute">
                                                <?php
                                                if ($service['recommended'] == 1) {
                                                    ?>
                                                    <i>Recommended</i>
                                                <?php }
                                                ?>
                                                <p><?php echo $service['title']; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class='col-sm-12 products-list dashboardDiv empty'>
                                    <div class="nested-div">
                                        <div class="position-absolute">
                                            <p>No services present.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="next"> </div>
                    </div>
                    <div class="row service-name your-services">
                        <div class="previous"> </div>
                        <div class="service-options owl-carousel"> 
                            <?php
                            if ($activeServices->services->count()) {
                                foreach ($activeServices->services as $service) {
                                    ?>
                                    <div class='col-sm-12 active-list dashboardDiv' data-id="{{$service->id}}">
                                        <div class="nested-div">
                                            <div class="position-absolute">
                                                <i>Active</i>
                                                <p><?php echo $service->title; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <div class='col-sm-12 active-list dashboardDiv empty'>
                                    <div class="nested-div">
                                        <div class="position-absolute">
                                            <p>No active services. <a href="{{ route('frontend.client.recommendedServices', config('constant.subdomain')) }}">Buy Services</a>.</p>
                                        </div>
                                    </div>
                                </div>
                            <?php }
                            ?>
                        </div>
                        <div class="next"> </div>
                    </div>
                    <div class="clearfix"></div>




                    <div class="col-sm-6 dashboard-inner2">
                        <div id="my-tasks-list">
                            <h2>Recent Document Activity</h2>
                            <ul class="list-unstyled">
                                <li>
                                    No recent activity.
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 dashboard-inner2-right">
                        <div id="commentry">
                            <h2>Recent Commentary</h2>
                            <?php if (!$commentaryData->isEmpty()) { ?>
                                @foreach ($commentaryData as $commentary)
                                <div class="comment">
                                    <p class="date">{{ $commentary->publish_at }}</p>
                                    <p class="title">{{ $commentary->title }}</p>
                                    <p class="desc">
                                        {{ $commentary->excerpt }}
                                    </p>
                                    <a href="{{ route('frontend.single-blog',$commentary->slug) }}" class="arrow" target="_blank">
                                        <i class="fa fa-chevron-right"></i>
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                                @endforeach
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<!--{{-- Html::script(elixir('js/dashboard.js')) --}}-->
{{-- Html::script(elixir('js/dashboard.js')) --}}
{{ Html::script(elixir('js/recommended-services.js')) }}
@stop