@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; About You
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    @include('frontend.includes.client_register_header')

    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 3: About You</p>
            <div class="col-sm-8 left-bottom-border"></div> 
            <div class="col-sm-4 right-bottom-border"></div>
            <div class="inner-content" style="text-align: inherit !important; width: 60% !important">
                {{ Form::open(['route' =>['frontend.client.signupStepSix', config('constant.subdomain')],'class' => 'form-horizontal','id'=>'step5Form', 'role' => 'form', 'method' => 'get', 'files'=>'true']) }}
                <input type="hidden" name="user_id" id="user-id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif"> 
                <div class="form-group" id='about-you-questionnaire'>
                    <div class="content-p-b">
                        @include('frontend.client.includes.serviceQuestions')
                    </div>
                   <div id="ending-message">Great!! Thanks for the answers.</div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script>
    var saveAnswerUrl = '{{route("frontend.client.stepFiveRegister")}}',
            paymentUrl = '{{route("frontend.client.signupStepPayment", config('constant.subdomain'))}}',
            questionList = <?php echo json_encode(config('constant.service_question_types_options_inverse')); ?>;
</script>
<script src="{{ asset('js/step-five.js') }}"></script>
@stop