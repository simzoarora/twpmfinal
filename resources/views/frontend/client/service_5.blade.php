@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop 
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                        <section class="sections" data-label='Liabilities'>
                            @include('frontend.client.serviceQuestions.semi-annual-preview')
                        </section>
                    <!--<a class="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->

                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div>
</div>

@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>

<script>
    $(document).ready(function () {
        $(".actions").addClass('col-xs-offset-0 col-sm-offset-4 col-md-offset-4 col-lg-offset-4');
        $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');
        $(document).on('click', '.contact-modal-show', function (e) {
            $('#contact-us').show();
            $('.contact-response').html('');
            $('#get-started-modal').modal();
            e.preventDefault();
        });
        $('.datetimepicker').datetimepicker({
            format: 'MM/DDYYYY'
        });


        $("select").selectBoxIt();
        $(document).on('change', '.mortgage-type', function () {
                    ;
            if ($(this).val() == 4) {
                $('.other-type').show();
            } else {
                $('.other-type').hide();
            }
            if ($(this).val() == 2) {
                $('.adjustable-type').show();
            } else {
                $('.adjustable-type').hide();
            }
            if ($(this).val() == 3) {
                $('.interest-type').show();
            } else {
                $('.interest-type').hide();
            }

        })
        $(document).on('change', '.interest-rate', function () {
            if ($(this).val() == 4) {
                $('.interest-other').show();
            } else {
                $('.interest-other').hide();
            }
        })
        $(document).on('change', '.mortgage-term', function () {
            if ($(this).val() == 3) {
                $('.mortgage-other').show();
            } else {
                $('.mortgage-other').hide();
            }
        })
        $(document).on('change', '.radio-value', function () {
            if ($(this).val() == 'yes') {
                $('.insurance-amount').show();
            } else {
                $('.insurance-amount').hide();
            }
        })
        $(document).on('change', '.other-amount', function () {
            if ($(this).val() > '0') {
                $('.other-amount-description').show();
            } else {
                $('.other-amount-description').hide();
            }
        })
        $(document).on('change', '.rate-based', function () {
            if ($(this).val() == 5) {
                $('.rate-based-other').show();
            } else {
                $('.rate-based-other').hide();
            }
        })
        $(document).on('change', '.card-type', function () {
            if ($(this).val() == 'yes') {
                $('.cash-back-card,.exchange-for-cash').show();
            } else {
                $('.cash-back-card,.exchange-for-cash').hide();
                $('.exchange').prop('checked', false)
                $('.exchange-rate').hide();
            }
        })
        $(document).on('change', '.exchange', function () {
            if ($(this).val() == 'yes') {
                $('.exchange-rate').show();
            } else {
                $('.exchange-rate').hide();
            }
        })
        $(document).on('change', '.loan-on-name', function () {
            if ($(this).val() == 1) {
                $('.their-name').hide();
            } else {
                $('.their-name').show();
            }
        })
        $(document).on('change', '.consolidated-loan', function () {
            if ($(this).val() == 'yes') {
                $('.consolidation-date').show();
            } else {
                $('.consolidation-date').hide();
            }
        })
        $(document).on('change', '.collateral-loan', function () {
            if ($(this).val() == 'yes') {
                $('.collateral-other-input').show();
            } else {
                $('.collateral-other-input').hide();
            }
        })
        $(document).on('change', '.debt-name', function () {
            if ($(this).val() == 4) {
                $('.debt-other-name').show();
            } else {
                $('.debt-other-name').hide();
            }
        })
        $(document).on('change', '.total-calc', function () {
            $('.checkCalculations').each(function () {
                var $i = 0;
                $(this).find('.total-calc').each(function (i, elem) {
                    if ($(elem).val()) {
                        $i = $i + parseInt($(elem).val());
                    }
                });
                $(this).find('#totalValue').val($i);
            });
        });

        if ($('#services-question-form-p-0').is(':visible')) {
            $('a[href="#next"]').text('Liabilities');
        } else {
        }



    });



    //Other debts form//
    $(document).on('change', '.other-non-business-debts', function (e) {
        var cind = form.steps('getCurrentIndex');
        if ($(this).val() == 'yes') {
            var Debts = $('#debtsDetails').html();
            var otherDebts = $('#otherDebtsDetails').html();
            html = _.template(Debts);
            html2 = _.template(otherDebts);
            $("form").steps("add", {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("add", {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("select").selectBoxIt();
            $('.debt-datetimepicker').datetimepicker({
                format: 'MM/dd/YYYY'
            });
        } else {
            $("form").steps("remove", cind + 1);
            $("form").steps("remove", cind + 1);
        }
        $(document).find('.other-non-business-debts:checked').parent().parent().find('.other-non-business-debts:enabled').prop('disabled', true);
        e.preventDefault();
    });
    // Other debts END//


//   window['otherDebtsFirst'] = function otherDebtsFirst() {
//        $('.other-non-business-debts').prop('disabled', true);
//        ;
//    }
//
//    window['appendOtherDebtsFields'] = function appendOtherDebtsFields() {
//        if ($(document).find('.other-non-business-debts:enabled:checked').val() == 'yes') {
//            var Debts = $('#debtsDetails').html();
//            var otherDebts = $('#otherDebtsDetails').html();
//            html = _.template(Debts);
//            html2 = _.template(otherDebts);
//            var cind = form.steps('getCurrentIndex');
//            $("form").steps("insert", cind, {
//                content: html({count: $('#services-question-form fieldset').length + 1000})
//            });
//            $("form").steps("insert", cind + 1, {
//                content: html2({count: $('#services-question-form fieldset').length + 1000})
//            });
//            $("form").steps('previous');
//            $("form").steps('previous');
//            $("select").selectBoxIt();
//            $('.debt-datetimepicker').datetimepicker({
//                format: 'MM/DD/YYYY',
//
//            });
//        }
//
//        $(document).find('.other-non-business-debts:enabled:checked').parent().parent().find('.other-non-business-debts:enabled').prop('disabled', true);
////        $('.non-rental:enabled').prop('disabled', true);
//    }











    window['mortgageQuestionFirst'] = function mortgageQuestionFirst() {
        $('.non-rental').prop('disabled', true);
        ;
    }

    window['appendMortgageFields'] = function appendMortgageFields() {
        if ($(document).find('.other-non-rental:enabled:checked').val() == 'yes' || $('.non-rental:enabled:checked').val() == 'yes') {
            var rentalProperty = $('#new-data-entry-template').html();
            var rentalProperty2 = $('#new-data-entry-template2').html();
            var rentalProperty3 = $('#new-data-entry-template3').html();
            html = _.template(rentalProperty);
            html2 = _.template(rentalProperty2);
            html3 = _.template(rentalProperty3);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 2, {
                content: html3({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY',
            });

        }

        $(document).find('.other-non-rental:enabled:checked').parent().parent().find('.other-non-rental:enabled').prop('disabled', true);
        $('.non-rental:enabled').prop('disabled', true);
    }

    //Home equity form//
    window['mortgagehelocFirst'] = function mortgagehelocFirst() {
        $('.home-equity').prop('disabled', true);
        ;
    }

    window['appendHelocFields'] = function appendHelocFields() {
        if ($(document).find('.heloc-non-rental:enabled:checked').val() == 'yes' || $('.home-equity:enabled:checked').val() == 'yes') {
            var homeEquity = $('#equityForm').html();
            var homeEquity2 = $('#equityForm2').html();
            html = _.template(homeEquity);
            html2 = _.template(homeEquity2);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.heloc-datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        }

        $(document).find('.heloc-non-rental:enabled:checked').parent().parent().find('.heloc-non-rental:enabled').prop('disabled', true);
        $('.home-equity:enabled').prop('disabled', true);
    }
    //Home equity form END//


    //auto loan form//


    window['vehicleLoanFirst'] = function vehicleLoanFirst() {
        $('.vehicle-loan').prop('disabled', true);
        ;
    }
    window['appendVehicleLoanFields'] = function appendVehicleLoanFields() {
        if ($(document).find('.other-vehicle-loan:enabled:checked').val() == 'yes' || $('.vehicle-loan:enabled:checked').val() == 'yes') {
            var autoLoan = $('#autoloanDetails').html();
            var other = $('#anotherAutoLoan').html();
            html = _.template(autoLoan);
            html2 = _.template(other);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.vehicle-loan-datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        }
        $(document).find('.other-vehicle-loan:enabled:checked').parent().parent().find('.other-vehicle-loan:enabled').prop('disabled', true);
        $('.vehicle-loan:enabled').prop('disabled', true);
    }
    //auto loan form END//

    //credit card form//
    window['creditCardFirst'] = function creditCardFirst() {
        $('.credit-card').prop('disabled', true);
        ;
    }
    window['appendCreditCardFields'] = function appendCreditCardFields() {
        if ($(document).find('.another-credit-card:enabled:checked').val() == 'yes' || $('.credit-card:enabled:checked').val() == 'yes') {
            var creditCard = $('#creditCardDetails').html();
            var otherCard = $('#anotherCreditCard').html();
            html = _.template(creditCard);
            html2 = _.template(otherCard);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
        }
        $(document).find('.another-credit-card:enabled:checked').parent().parent().find('.another-credit-card:enabled').prop('disabled', true);
        $('.credit-card:enabled').prop('disabled', true);

    }
    //other student loan//
    window['studentLoanFirst'] = function studentLoanFirst() {
        $('.student-loan').prop('disabled', true);
        ;
    }
    window['appendStudentLoanFields'] = function appendStudentLoanFields() {
        if ($(document).find('.other-student-loan:enabled:checked').val() == 'yes' || $('.student-loan:enabled:checked').val() == 'yes') {
            var studentLoanDetails = $('#studentLoanDetails').html();
            var otherstudentLoanDetails = $('#otherstudentLoanDetails').html();
            html = _.template(studentLoanDetails);
            html2 = _.template(otherstudentLoanDetails);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.student-loan-datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });
        }
        $(document).find('.other-student-loan:enabled:checked').parent().parent().find('.other-student-loan:enabled').prop('disabled', true);
        $('.student-loan:enabled').prop('disabled', true);
    }
</script>
@endsection