@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; Contact Information 
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/preview.css')) }}   
@stop

@section('content')

<div class="col-sm-12 header-bar">
    <div class="container p0">
        <div class="row">
            <img src="img/twpm-blue-logo.png" class="logo" alt="">
            <p>Finish Setup</p>
        </div>
    </div>
</div>

<div class="col-sm-12 first-container">
    <div class="container p0">
        <div class="row">
            <p>Step 5: Security information</p>
            <div class="border-blue"></div>
            <div class="col-sm-12 second-container">
                <div class="col-sm-6">
                    <h2>Security information</h2>
                    <p>As financial advisors, we need to ask
                        a bit more about your financial background so that we can be sure
                        we are giving you the best possible advice.</p>
                </div>
                <div class="col-sm-6 questions">
                    <form>
                        <p>Select a primary security question</p>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What is your best friend's name?</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What was the name of your childhood pet?</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What is your favorite holiday?</label>
                        </div>
                        <p>Primary security answer</p>
                        <div class="form-group">
                            <textarea class="form-control" id="answer"></textarea>
                        </div>
                    </form>
                    <form class="secondary-question">
                        <p>Select a secondary security question</p>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What is your best friend's name?</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What was the name of your childhood pet?</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" name="optradio">What is your favorite holiday?</label>
                        </div>
                        <p>Secondary security answer</p>
                        <div class="form-group">
                            <!--<label for="comment">Comment:</label>-->
                            <textarea class="form-control" id="answer"></textarea>
                        </div>
                    </form>
                    <div class="checkbox">
                        <label><input type="checkbox" value="">By checking this box
                            you agree to that you have read and agreed to the 
                            terms below.</label>
                    </div>
                    <p>Lorem Ipsum is simply dummy text of the printing and 
                        typesetting industry. Lorem Ipsum has been the industry's
                        standard dummy text ever since the 1500s, when an unknown 
                        printer took a galley of type and scrambled it to make a 
                        type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining 
                        essentially unchanged.</p>
                    <a href="#" class="btn btn-info" role="button">FINISH</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-sm-12 footer">
    <div class="container p0">
        <div class="row">
            <p class="description">FOOTER LINKS</p>
            <p> Lorem Ipsum is simply dummy text of the printing and
                typesetting industry. Lorem Ipsum has been the industry's
                standard dummy text ever since the 1500s, when an unknown 
                printer took a galley of type and scrambled it to make a 
                type specimen book. It has survived not only five centuries,
                but also the leap into electronic typesetting, remaining 
                essentially unchanged. It was popularised in the 1960s with 
                the release of Letraset sheets containing Lorem Ipsum passages,
                and more recently with desktop publishing software like Aldus 
                PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply 
                dummy text of the printing and typesetting industry. Lorem Ipsum 
                has been the industry's standard dummy text ever since the 1500s, 
                when an unknown printer took a galley of type and scrambled it to
                make a type specimen book. It has survived not only five centuries,
                but also the leap into electronic typesetting, remaining essentially 
                unchanged. It was popularised in the 1960s with the release of Letraset
                sheets containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

        </div>
    </div>
</div>



@endsection

@section('after-scripts')
@stop
