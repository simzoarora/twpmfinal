@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open(['method' => 'get','url' => route('frontend.client.recommendedServices',[config('constant.subdomain')]), 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    <!--{{ Form::input('hidden','serviceId',$currentService->id) }}-->
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.lifeInsurance-preview')
                        </section>
                    </fieldset>
                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')

<script src="{{ asset('js/life-insurance.js') }}"></script>

<script>
$(document).ready(function () {
    $(".actions").addClass('col-xs-offset-0 col-sm-offset-4 col-md-offset-4 col-lg-offset-4');
    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');
});
</script>
@stop