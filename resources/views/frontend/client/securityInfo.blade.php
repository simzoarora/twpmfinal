@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/finish-account.css')) }}
@stop
@section('content')
<div class="wrapper"> 
    <div class="col-sm-12 header-bar">
        <div class="container">
            {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }}
            <p>Finish Setup</p>
        </div>
    </div>
    <div class="container-fluid wrapper">
        <div class="container">
            <div class="step">
                <p>Step5: Security information</p>
                <div class="blue-line col-sm-10">
                </div>
                <div class="grey-line col-sm-2">
                </div>
                <div class="container-first">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 sections">
                    <div class="col-sm-6 section-left">
                        <h2> Security information </h2>
                        <p>As financial advisors, we need to ask a bit more about your <br> financial background so that we can be sure we are giving you <br> the best possible advice.</p>
                    </div>
                    <div class="col-sm-6 section-right">
                        {{ Form::open(['route' => 'frontend.client.saveSecurityInfo' ,'novalidate', 'id'=>'security-form']) }} 
                        <div class="security-questions">
                            <div class="col-sm-12 inner-left"> 
                                <p class="left-subheading"><b>Select a primary security question</b></p>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[0][question_id]', 1, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options">{{ config('constant.security_question_primary')[1] }}</p>
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[0][question_id]', 2, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options">{{config('constant.security_question_primary')[2]}}</p> 
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[0][question_id]', 3, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options">{{config('constant.security_question_primary')[3]}}</p>
                                </label>
                            </div> 

                            <div class="col-sm-12 security-answer inner-left">
                                <label> Primary security answer </label>
                                {{ Form::input('text','security_question[0][answer]', null,['class' =>'form-control', 'required'=>true]) }}
                                {{ Form::input('hidden','security_question[0][type]', 1) }}
                            </div>
                        </div>
                        <div class="security-questions"> 
                            <div class="col-sm-12 inner-left">
                                <p class="left-subheading"><b>Select a secondary security question</b></p>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[1][question_id]', 1, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options"> {{ config('constant.security_question_secondary')[1] }} </p> 
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[1][question_id]', 2, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options">{{ config('constant.security_question_secondary')[2] }}</p>
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::input('radio','security_question[1][question_id]', 3, ['placeholder' => '', 'required'=>true]) }}
                                    <span class="radio-icon"></span>
                                    <p class="questions-options">{{ config('constant.security_question_secondary')[3] }}</p>
                                </label>
                            </div>

                            <div class="col-sm-12 security-answer inner-left">
                                <label> Secondary security answer </label>
                                {{ Form::input('text','security_question[1][answer]', null,['class' =>'form-control', 'required'=>true]) }}
                                {{ Form::input('hidden','security_question[1][type]', 2) }}
                            </div>
                        </div> 
<!--                        <div class="terms-and-conditions">
                            <label class="col-sm-10 checkbox-custom-label ">
                                {{ Form::input("checkbox",'name', null, ['required'=>true]) }}
                                <span class="checkbox-icon"></span>
                                <p> <b>By checking this box you confirm that you have read and agreed to the <a href="{{route('frontend.terms')}}">Terms & Conditions.</a></b></p>
                            </label>
                        </div>-->
                        <div class="site-heading-1"> 
                            <div class="next-page align-left col-sm-8">
                                <button type="button" data-toggle="modal" data-target="#termsModal" class="btn btn-info"> Continue</button>
                            </div> 
                        </div>

                        <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#termsModal">Open Modal</button>--> 

                        <!-- Modal -->
                        <div id="termsModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content" style="overflow: hidden;">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Terms and Conditions</h4> 
                                    </div>
                                    <div class="modal-body terms-content" >
                                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                                        <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                                        <div class="terms-and-conditions inner-left">
                                            <label class="col-sm-12 col-xs-12 checkbox-custom-label ">
                                                <input type="checkbox" name="name" required="true" style="width: 21px;">
                                                <span class="checkbox-icon"></span> By checking this box you confirm that you have <br>read and agreed to the <a target="_blank" href="{{route('frontend.terms')}}">Terms & Conditions.</a>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="site-heading-1"> 
                                        <div class="next-page align-center col-sm-8">
                                            <button type="submit" class="btn btn-info ">Finish</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
            @include('frontend.includes.footer') 
        </div> 
    </div> 
</div> 
@endsection
@section('after-scripts')
{{ Html::script(elixir('js/contact-information.js')) }} 
@endsection
