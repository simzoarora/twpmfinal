@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
@stop

@section('content')
<?php
//dd($serviceData);
//die('ol');
?>
<div class="dashboard">

    <div id="dashboard-content">
        <div class="container-fluid">
            <?php $activeClass3 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content client-services">
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">


                    <div id="documents-inner" class="pro-doc-full">
                        <div class="col-sm-6 padding-0">
                            <div id="document-activity">
                                <h2>All Service Documents</h2>
                                <?php
                                if (!$userFiles->isEmpty()) {
                                    foreach ($userFiles as $fileArray) {
                                        ?>
                                        <div class="document">
                                            <div class="image">
                                                <i class="fa fa-file-pdf-o"></i>
                                            </div>

                                            <div class="details">
                                                <p class="title">
                                                    <a href="{{ route('frontend.client.downloadFile', ['client',$fileArray->pivot->sharefile_item_id,$fileArray->service_id]) }}" target="_blank">
                                                        {{ $fileArray->pivot->value}}</a></p>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <div class="document">
                                        No documents found.
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                </div>
                <div class = "clearfix"></div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop