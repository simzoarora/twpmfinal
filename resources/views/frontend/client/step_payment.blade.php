@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; Payment
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    @include('frontend.includes.client_register_header')

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 1: Payment</p>
            <div class="col-sm-3 left-bottom-border"></div> 
            <div class="col-sm-9 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Payment</h2>
                <p class='heading-info'>Pay for the service.</p>

                {{ Form::open(['route' =>'frontend.client.stripePayment', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'stripe-payment-form']) }}
                <input type="hidden" name="user_id" id="user-id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif"> 
                <div id="form-response"></div>
                <div class="form-group">
                    <span class="input input--hoshi">
                        {{ Form::text('card_number', null, ['class' => 'input__field input__field--hoshi','required']) }}
                        <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                            <span class="input__label-content input__label-content--hoshi">Card Number</span>
                        </label>
                    </span>
                    <div class="col-sm-4 expires-month">
                        <span class="input input--hoshi">
                            {{ Form::text('expires_month', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Expiry (MM)</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-sm-4 expires-year">
                        <span class="input input--hoshi">
                            {{ Form::text('expires_year', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Expiry (YY)</span>
                            </label>
                        </span>
                    </div>
                    <div class="col-sm-4 cvv">
                        <span class="input input--hoshi">
                            {{ Form::text('cvv', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">CVV</span>
                            </label>
                        </span>
                    </div>

                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="final-step">
                <div id="settingup-dash">
                    <i class="fa fa-circle-o-notch fa-spin"></i>
                    <p>Setting up the dashboard....</p>
                </div>
                <div id="end-register-error">
                    Ohh!! Unfortunately there has been a error. Please login again.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/step-payment.js') }}"></script>
<script type="text/javascript">
    var endRegister = '{{ route("frontend.client.stepSixRegister") }}',
            loginUrl = '{{ route("frontend.client.login", config('constant.subdomain')) }}',
            dashboardUrl = '{{route("frontend.client.dashboard", config('constant.subdomain'))}}';
</script>
@stop