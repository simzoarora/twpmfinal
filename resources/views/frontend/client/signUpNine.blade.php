@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/signUpOne.css')) }}   
@stop

@section('content')
<div class="col-sm-12 header-bar">
    <div class="container">
        <div class="row">
            <img src="img/twpm-blue-logo.png" class="logo" alt=""> 
            <p>Risk Tolerance</p>
        </div>
    </div>
</div>

<div class="container-fluid wrapper">
    <div class="container">
        <div class="step">
            <p>Step2: Assess your risk tolerance</p>
            <div class="blue-line col-sm-4">
            </div>
            <div class="grey-line col-sm-8"> 
            </div>
        </div>  
        <div class="col-sm-8 col-sm-offset-2 site-heading-1">
            <p>LET'S GET TO KNOW YOU</p>
<!--                <h2>With regard to my investment return, i am <br> <input class="step9-input" type='text' class='blank' value ="extremely"> concerned about short-term <br>
                losses.</h2>-->
            <div class="investment-intake">
                {{ Form::open(['route'=>'frontend.investmentIntake','id' => 'investment-intake-form']) }}
            <h2>With regard to my investment return, i am</h2> 
                <div class="investment_wrapper">
               <!--<input type="text" name="consent" class="select-input select-consent"/>-->
                    <span class="select-input select-consent"></span>
                    <ul class="consent select-list" data-input="select-consent">
                        <li data-val="extremely">extremely</li>
                        <li data-val="somewhat">somewhat</li>
                        <li data-val="mildy">mildy</li>
                        <li data-val="not">not</li>
                    </ul>
                </div>
            <h2> concerned about short-term <br>losses.</h2>
            </div>
            </form>
            <div class="next-page step9-padding ">
                <a> <p>Back</p> </a>
                <a href="#" class="btn btn-info" role="button">Continue</a>
            </div>
        </div>
        <div class="footer">
            <p class="description">FOOTER LINKS</p>
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div> 
    </div> 
</div> 
@section('after-scripts')
<script src="{{ asset('js/employee.js') }}"></script>
<script>
    $('.select-input').on('click', function () {
        $(this).next().addClass('ul-border').show();
        $(this).next().children().show();
    });
    $('#investment-intake-form').on('click', 'li', function () {
        $(this).parent().removeClass('ul-border');
        var selected_val = $(this).data('val'),
                input_class = $(this).parent().data('input');
        $('.' + input_class).text(selected_val);
        $(this).parent().children().hide();
    });

</script>
@stop
@endsection