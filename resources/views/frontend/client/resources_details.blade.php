@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
@stop

@section('content')
<?php
//dd($topic);
?>
<div class="dashboard ">

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content client-resources">
                  <?php $page_title = 'Resources'; ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 resources-details">

                    <h2><?php echo $topic->title; ?></h2>
                    <div class="details"><?php echo $topic->content; ?></div>

                    <?php if (!empty($topic->resourceTopicFiles)) { ?>
                        <p class="helpful-links">Helpful Links</p>

                        <ul class="files-list">
                            <?php foreach ($topic->resourceTopicFiles as $file) { ?>
                                <li><a href="<?php echo URL::asset('img/backend/resources/file_uploads/') . '/' . $file->upload_file; ?>/" download="<?php echo $file->upload_file; ?>" target="_blank"><?php echo $file->file_text; ?> </a></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop