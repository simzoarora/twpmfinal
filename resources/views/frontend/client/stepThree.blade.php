@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; Verify Mobile
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    @include('frontend.includes.client_register_header')
    <!--step 2 started-->

    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 2: Verify Mobile</p>
            <div class="col-sm-5 left-bottom-border"></div> 
            <div class="col-sm-7 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>Verify Mobile Number</h2>
                <p class='heading-info'>For added security and timely notifications.</p>
                @include('frontend.client.includes.mobileVerify')
                <input type="hidden" name="profile_process" value="true" id="profile_process">
                
                {{ Form::open(['route' =>'frontend.client.submitVerify', 'method' => 'post']) }}
                <input type="hidden" name="user_id" id="user-id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif"> 
                <div class="submit-btn" id="button-color">
                    {{ Form::submit('Continue', ['class' => 'btn contact-info-button', 'id' => 'step-three-submit','disabled'=>'disabled']) }}
                </div>
                
                <div class='skip'>
                   {{ Form::submit('Skip this for now', ['class' => 'skip-content']) }} 
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@endsection

@section('after-scripts')
<script>
    var sendOtpUrl = '{{route("frontend.client.sendOtp")}}',
            verifyOtpUrl = '{{route("frontend.client.verifyOtp")}}';
</script>
<script src="{{ asset('js/step-three.js') }}"></script>
@stop