
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections ">
                            @include('frontend.client.serviceQuestions.question_44')
                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')

<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>
    $(document).ready(function () {
        
        
        
        
        
        $('.actions').addClass('hide-pagination');
        $('.hide-pagination').hide();

//        $('a[href="#finish"]').text('continue');
        form = $("#services-question-form");

        $('.pay-for-expense-datetimepicker').datetimepicker({
            default: 'MMMM YYYY',
        });


        $("select").selectBoxIt();
        $(document).on('change', '.one-time-expense', function () {

            if ($(this).val() == 'annual') {
                $('.need-per-year').show();
                $('.number-of-years').show();
            } else {
                $('.need-per-year').hide();
                $('.number-of-years').hide();

            }
        });
        $(document).on('change', '.one-time-expense', function () {

            if ($(this).val() == 'one-time') {
                $('.expense-amount').show();
            } else {
                $('.expense-amount').hide();

            }
        });
        $(document).on('change', '.client-goal', function () {

            if ($(this).val() == '4') {
                $('.other-goal').show();
            } else {
                $('.other-goal').hide();
            }
            $("select").selectBoxIt();
        });
        $(document).on('change', '.status', function () {

            if ($(this).val() == '1') {
                $('.partner-age').hide();
            } else {
                $('.partner-age').show();
            }
            $("select").selectBoxIt();
        });
        $(document).on('change', '.children-confirmation', function () {

            if ($(this).val() == 'yes') {
                $('.children-details-table').show();
            } else {
                $('.children-details-table').hide();
            }
        });
        var studentList = [];
        $(document).on('click', '#studentform', function (e) {
            $('#myModal .error-alert').remove();
            if ($('#myModal .student-name').val() && $('#myModal .student-age').val() && $('#myModal .beginning-year').val() && $('#myModal .level').val() && $('#myModal .school-name').val()) {

                if ($(this).hasClass('edit-form')) {
                    studentList[$(this).attr('data-index')] = {
                        name: $('#myModal .student-name').val(),
                        age: $('#myModal .student-age').val(),
                        year: $('#myModal .beginning-year').val(),
                        level: $('#myModal .level').text(),
                        school: $('#myModal .school-name').val()
                    }

                    $('#children-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#myModal .student-name').val() + '</td> <td>' + $('#myModal .student-age').val() + '</td> <td>' + $('#myModal .beginning-year').val() + '</td> <td>' + $('#myModal .level').val() + '</td> <td>' + $('#myModal .school-name').val() + '</td> <td> <i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i> </td>');
                } else {
                    $('#children-info tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#myModal .student-name').val() + '</td> <td>' + $('#myModal .student-age').val() + '</td> <td>' + $('#myModal .beginning-year').val() + '</td> <td>' + $('#myModal .level option:selected').text() + '</td> <td>' + $('#myModal .school-name').val() + '</td> <td> <i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i> </td> </tr>');
                    studentList.push({
                        name: $('#myModal .student-name').val(),
                        age: $('#myModal .student-age').val(),
                        year: $('#myModal .beginning-year').val(),
                        level: $('#myModal .level').val(),
                        school: $('#myModal .school-name').val()});
                }
                $('#myModal .student-name,#mymodal .student-age ,#mymodal .beginning-year,#mymodal .level, #mymodal .school-name ').val('');
                $('#myModal').modal('hide');
            } else {
                //                alert('please fill values');
                $('#myModal input,#myModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
            e.preventDefault();
        });
        $(document).on('keypress change', '#myModal input', '#myModal select', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val() || $(this).val() == '1') {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $(document).on('click', '#children-info .fa', function () {
            $('#myModal').modal();
            $("select").selectBoxIt();
            var index = $(this).closest('tr').attr('data-index');
            studentDetails = studentList[index];
            $('#myModal .student-name').val(studentDetails.name);
            $('#myModal .student-age').val(studentDetails.age);
            $('#myModal .beginning-year').val(studentDetails.year);
            $('#myModal .level').val(studentDetails.level);
            $('#myModal .school-name').val(studentDetails.school);
            $('#studentform').addClass('edit-form');
            $('#studentform').attr('data-index', index);
        });
        $(document).on('click', '.add-dependent', function (e) {
            $("select").selectBoxIt();
            $('#studentform').removeClass('edit-form');
            $('#myModal .error-alert').remove();
            e.preventDefault();
        });



        var childrenList = [];
        $(document).on('click', '#childrenForm', function () {
            $('#childrenModal .error-alert').remove();
            if ($('#childrenModal .children-name').val() && $('#childrenModal .children-age').val() && $('#childrenModal input[type="radio"]').is(':checked')) {

                if ($(this).hasClass('edit-form')) {
                    childrenList[$(this).attr('data-index')] = {
                        name: $('#childrenModal .children-name').val(),
                        age: $('#childrenModal .children-age').val(),
                        needs: $('#childrenModal .educational-needs:checked').val()
                    }
                    $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#childrenModal .children-name').val() + '</td> <td>' + $('#childrenModal .children-age').val() + '</td> <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td>');
                } else {
                    $('#student-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#childrenModal .children-name').val() + '</td> <td>' + $('#childrenModal .children-age').val() + '</td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td></tr>');

                    childrenList.push({
                        name: $('#childrenModal .children-name').val(),
                        age: $('#childrenModal .children-age').val(),
                        needs: $('#childrenModal .educational-needs:checked').val()});
                }
                $('#childrenModal .children-name,#childrenModal .children-age').val('');
                $('#childrenModal .educational-needs').prop('checked', false);
                $('#childrenModal').modal('hide');
            } else {
                //                alert('please fill values');
                $('#childrenModal input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
                if (!$('#childrenModal input[type="radio"]').is(':checked')) {
                    $('#childrenModal input[type="radio"]').last().closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
                }
            }
        });

        $('#childrenModal input').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val() || $(this).val() == '0') {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $(document).on('click', '#student-info .fa', function () {
            $('#childrenModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            studentDetails = childrenList[index];
            $('#childrenModal .children-name').val(studentDetails.name);
            $('#childrenModal .children-age').val(studentDetails.age);
            $('#childrenModal .educational-needs[value=' + studentDetails.needs + ']').prop('checked', true);
            $('#childrenForm').addClass('edit-form');
            $('#childrenForm').attr('data-index', index);

        });
        $(document).on('click', '.add-dependent', function () {
            $('#childrenForm').removeClass('edit-form');
        });

    });

</script>

@endsection