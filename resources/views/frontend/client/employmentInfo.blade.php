@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop 

@section('after-styles') 
{{ Html::style(asset('css/finish-account.css')) }}
@stop
@section('content')
<div class="wrapper">
    <div class="col-sm-12 header-bar">
        <div class="container">
            {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }}
            <p>Finish Setup</p>
        </div>
    </div>

    <div class="container-fluid wrapper">
        <div class="container">
            <div class="step">
                <p>Step3: Employment Information</p>
                <div class="blue-line col-sm-6">
                </div>
                <div class="grey-line col-sm-6">
                </div>
                <div class="container-first">
                </div>
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 sections">
                    <div class="col-sm-6 section-left">
                        <h2>Employment Information</h2>
                        <p>Your employment helps us provide more <br> personal advice regarding the types of accounts more <br> relevant and beneficial to your situation.</p>
                    </div>
                    <div class="col-sm-6 section-right">
                        {{ Form::open(['route' => 'frontend.client.saveEmploymentInfo']) }} 
                        <div class="col-sm-8 inner-left">
                            <div class='inner-right'>
                                <i class="fa fa-angle-down employement-arrow" aria-hidden="true"></i>
                                <p>Employment status </p>
                                {{ Form::select('employment_status', config('constant.employement_status'),null, ['class' => 'field full-width']) }}
                            </div>
                        </div>
                        <div class="site-heading-1"> 
                            <div class="next-page align-left step8-padding col-sm-8">
                                <!--<a href="financialInfo" class="btn btn-info " role="button">Continue</a>-->
                                <button type="submit" class="btn btn-info ">Continue</button>
                                <!--<a> <p class="col-sm-8">Save and return later</p> </a>-->
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div> 
                </div>
            </div>

            @include('frontend.includes.footer')
        </div> 
    </div> 
</div> 

@section('after-scripts')
<script src="{{ asset('js/signUp.js') }}"></script>
<script>
    $("select").selectBoxIt();
</script>
@endsection