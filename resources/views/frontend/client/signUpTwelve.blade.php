@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/signUpTwelve.css')) }}   
@stop

@section('content')
<div class="col-sm-12 header-bar">
    <div class="container">
        <div class="row">
            <img src="img/twpm-blue-logo.png" class="logo" alt="">
            <p>Risk Tolerance</p>
        </div>
    </div>
</div>

<div class="container-fluid wrapper">
    <div class="container">
        <div class="step">
            <p>Step2: Assess your risk tolerance</p>
            <div class="blue-line col-sm-4">
            </div>
            <div class="grey-line col-sm-8"> 
            </div>
        </div>
        <div class="col-sm-8 col-sm-offset-2 site-heading-1">
            <p>LET'S GET TO KNOW YOU</p>
            <h2>Based on my investment philosophy <br> objectives, and threshold for risk, i would <br> choose this plan:</h2>

            <div class="custom-row">

                <div class="col-sm-4 plans-wrapper">
                    <a><div class="annual-return">
                            <h4>Average annual return <br> 5%</h4>
                            <p>Worst case one-year scenario: -5%</p>
                            <p>Best case one-year scenario: +15%</p>
                        </div></a>
                </div>
                <div class="col-sm-4 plans-wrapper">
                    <a><div class="annual-return">
                            <h4>Average annual return <br> 7.5%</h4>
                            <p>Worst case one-year scenario: -12%</p>
                            <p>Best case one-year scenario: +22%</p>
                        </div></a>
                </div>
                <div class="col-sm-4 plans-wrapper">
                    <a><div class="annual-return">
                            <h4>Average annual return <br> 10%</h4>
                            <p>Worst case one-year scenario: -25%</p>
                            <p>Best case one-year scenario: +40%</p>
                        </div></a>
                </div>
            </div>








            <div class="next-page">
                <a> <p>Back</p> </a>
                <a href="#" class="btn btn-info" role="button">Continue</a>
            </div>
        </div>
        <div class="footer">
            <p class="description">FOOTER LINKS</p>
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div> 
    </div> 
</div> 


@endsection