@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/servicesQuestion.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    @foreach($serviceQuestionsPages as $pageNumber => $pagewiseServiceQuestions)
                    <h3></h3>
                    <fieldset>
                        @foreach($pagewiseServiceQuestions as $key2 => $singleQuestion)
                        <section class="sections">
                            <?php
                            if ($singleQuestion->type == config('constant.service_question_types_options_inverse.mla')) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                <?php foreach ($singleQuestion->questionInputs as $key1 => $singleInput) { ?>
                                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                                    <?php
                                    if ($singleInput->count == 1) {
                                        ?>
                                        {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                        {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                        <?php
                                        $inputs = json_decode($singleInput->inputs);
                                        ?>
                                        <div class=" col-sm-6 col-sm-offset-1 section-right">
                                            <?php foreach ($inputs as $key => $input) { ?>
                                                <div class="col-sm-8 inner-left">
                                                    {{ Form::label('label', ucfirst($input->description)) }}
                                                    {{ Form::input('text','data['.$key2.'][answer]['.$input->description.']',null,['data-validation'=> $input->validation , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
                                                </div>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                        {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                        <?php
                                        $listInputs = json_decode($singleInput->inputs);
                                        ?>
                                        <div class="horizontal col-md-6 col-sm-offset-1">
                                            <div class="headings">
                                                <?php
                                                foreach ($listInputs as $key => $listInput) {
                                                    $sm = 12 / count($listInputs);
                                                    ?>
                                                    <div class="col-md-{{$sm}} nopadding">
                                                        {{ Form::label('description', ucfirst($listInput->description), array('class' => 'description')) }}
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <?php
                                            for ($i = 0; $i < $singleInput->count; $i++) {
                                                foreach ($listInputs as $key => $listInput) {
                                                    $sm = 12 / count($listInputs);
                                                    ?>
                                                    <div class="col-md-{{$sm}}">

                                                        {{ Form::input('text','data['.$key2.'][answer]['.$i.']['.$listInput->description.']',null,['data-validation'=>$listInput->validation,'placeholder'=>$listInput->description=='amount'?'$':'', 'class'=>'form-control']) }}
                                                    </div>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                <?php } ?>

                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.usl'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <?php
                                foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                    $inputs = json_decode($singleInput->inputs);
                                    ?>
                                    <div class="col-sm-6 col-sm-offset-1 section-right">
                                        {{ Form::label('label', $inputs->description) }}
                                        {{ Form::file('data['.$key2.'][answer]['.$inputs->description.']', ['class' => 'field']) }}
                                    </div>
                                <?php } ?>
                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.mcma'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    <?php
                                    foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                        $inputs = json_decode($singleInput->inputs);
                                        ?>

                                        {{ Form::label('label', $inputs->description) }}
                                        @if (array_key_exists('options', $inputs))
                                        <ul class = "options" style="list-style:none">
                                            @foreach($inputs->options as $option)
                                            <li class="col-sm-2"> <label>
                                                    {{ Form::checkbox('data['.$key2.'][answer]['.$inputs->description.']', $option->id, null, ['required' => true]) }}
                                                    <span class = "checkbox-icon"></span>
                                                    <span>
                                                        {{$option->value}}
                                                    </span>
                                                </label>
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    <?php } ?>
                                </div>
                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.mcsa'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>

                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <?php
                                foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                    $inputs = json_decode($singleInput->inputs);
                                    ?>
                                    <div class="col-sm-6 col-sm-offset-1 section-right">
                                        {{ Form::label('label', $inputs->description) }}
                                        <ul class = "options" style="list-style:none">
                                            @if (array_key_exists('options', $inputs))
                                            @foreach($inputs->options as $option)
                                            <li class="col-sm-2"> <label class = "">
                                                    {{ Form::radio('data['.$key2.'][answer]['.$inputs->description.']', $option->id, null, ['required' => true]) }}
                                                    <span>
                                                        {{$option->value}}
                                                    </span>
                                                </label>
                                            </li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                <?php } ?>

                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.dpdwn'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <?php
                                foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                    $inputs         = json_decode($singleInput->inputs);
                                    ?>
                                    <div class="col-sm-6 col-sm-offset-1 section-right">
                                        <?php
                                        $options_values = ["0" => "SELECT"];
                                        if (array_key_exists('options', $inputs)) {
                                            foreach ($inputs->options as $option) {
                                                $options_values[$option->id] = $option->value;
                                            }
                                        }
                                        ?>
                                        <div class="col-sm-8 inner-left">
                                            {{ Form::label('label', $inputs->description) }}
                                            {{ Form::select('data['.$key2.'][answer]['.$inputs->description.']',$options_values, null) }}
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.uml'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <?php
                                foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                    $inputs = json_decode($singleInput->inputs);
                                    ?>
                                    <div class="col-sm-6 col-sm-offset-1 section-right">
                                        {{ Form::label('label', $inputs->description) }}
                                        {{ Form::file('data['.$key2.'][answer]['.$inputs->description.']', ['class' => 'field','multiple'=>true]) }}
                                    </div>
                                <?php } ?>

                                <?php
                            } elseif (($singleQuestion->type == config('constant.service_question_types_options_inverse.sla'))) {
                                ?>
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2> {{$singleQuestion->question}} </h2>
                                    <p> {{$singleQuestion->description}}</p>
                                </div>
                                {{ Form::input('hidden','data['.$key2.'][questionid]',$singleQuestion->id) }}
                                {{ Form::input('hidden','data['.$key2.'][type]',$singleQuestion->type) }}
                                <?php
                                foreach ($singleQuestion->questionInputs as $key1 => $singleInput) {
                                    $inputs = json_decode($singleInput->inputs);
                                    if (!empty($inputs)) {
                                        foreach ($inputs as $key => $input) {
                                            ?>
                                            <div class="col-sm-6 col-sm-offset-1 section-right">
                                                {{ Form::label('label', $input->description) }}
                                                {{ Form::input('text','data['.$key2.'][answer]['.$input->description.']',null,['data-validation'=> $input->validation , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>
                                <?php } ?>

                            <?php } ?>

                        </section>
                        @endforeach
                    </fieldset>
                    @endforeach

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
{{ Html::script('/vendor/laravel-filemanager/js/lfm.js') }}
@endsection