
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}  
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',13) }}
                    {{ Form::input('hidden','subTopicId',4) }}
                    {{ Form::input('hidden','redirectPageName','spouse-employment-income-retirement-preview') }}
                    <?php
                    if (!empty($answer) && !empty($answer["copy"])) {
                        foreach ($answer["copy"] as $key => $data) {
                            ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{$spouse_name}} PENSION PLANS</h6>
                                            <h2>Pension plans(defined benefit plans)</h2>

                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            {{ Form::input('hidden','questionName[141]','Pension plans(defined benefit plans)') }}
                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Does your company offer a pension plan (also known as defined benefit plan)?</label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][company_offer_pension_plan_'.$key.']', 'yes',(array_key_exists('company_offer_pension_plan_'.$key,$data) && $data['company_offer_pension_plan_'.$key] == "yes") ? true : false,['class'=>'pension-plan' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][company_offer_pension_plan_'.$key.']', 'no',(array_key_exists('company_offer_pension_plan_'.$key,$data) && $data['company_offer_pension_plan_'.$key] == "no") ? true : false,['class'=>'pension-plan' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Annual Benefit Amount</h2>

                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'Estimated annual benefit amount') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input data-validation="" class="borderLeft0 full-width comprehensive-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][estimated_annual_benefit_amount_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                                    if (array_key_exists('estimated_annual_benefit_amount_' . $key, $data)) {
                                                        echo $data["estimated_annual_benefit_amount_" . $key];
                                                    }
                                                    ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Age & Estimated Benefits</h2>

                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'At what age do you expect to begin taking the pension?') }}
                                                <div class="input-group service-input-group">
                                                    <input style="text-align:right; padding-right: 0 !important;" data-validation="" class=" borderRight0 full-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][age_expect_taking_pension_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                            if (array_key_exists('age_expect_taking_pension_' . $key, $data)) {
                                                echo $data["age_expect_taking_pension_" . $key];
                                            }
                                                    ?>">
                                                    <span class="input-group-addon">years</span>
                                                </div>
                        <!--<p style="position: absolute; top: 57px; right: 37px; font-weight: 400;">years</p>-->
                                            </div>
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'At what minimum age would you be able to begin taking the pension?') }}
                                                <div class="input-group service-input-group">
                                                    <input style="text-align:right; padding-right: 0 !important;" data-validation="" class=" borderRight0 full-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][minimum_age_able_begin_taking_pension_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                            if (array_key_exists('minimum_age_able_begin_taking_pension_' . $key, $data)) {
                                                echo $data["minimum_age_able_begin_taking_pension_" . $key];
                                            }
                                                    ?>">
                                                    <span class="input-group-addon">years</span>
                                                </div>
                                                <!--<p style="position: absolute; top: 57px; right: 37px; font-weight: 400;">years</p>-->
                                            </div>
                                            <div class="col-sm-6 inner-left estimated-benefit" style="display: <?php
                                            if (array_key_exists("minimum_age_able_begin_taking_pension_" . $key, $data) && ($data["minimum_age_able_begin_taking_pension_" . $key] != '')) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                                    ?>">
                                                {{ Form::label('label', 'What would the estimated benefit be if you begin taking at that age?') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input style="text-align:right;" data-validation="" class=" borderLeft0 full-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][estimated_benefit_begin_taking_age_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                                 if (array_key_exists('estimated_benefit_begin_taking_age_' . $key, $data)) {
                                                     echo $data["estimated_benefit_begin_taking_age_" . $key];
                                                 }
                                                 ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Pension & Social Security</h2>

                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Is your benefit coordinated with SS until you reach a certain age?</label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][benefit_coordinatedSS_certain_age_'.$key.']', 'yes',(array_key_exists('benefit_coordinatedSS_certain_age_'.$key,$data) &&  $data['benefit_coordinatedSS_certain_age_'.$key] == "yes") ? true : false,['class'=>'pension-plan' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][benefit_coordinatedSS_certain_age_'.$key.']', 'no',(array_key_exists('benefit_coordinatedSS_certain_age_'.$key,$data) && $data['benefit_coordinatedSS_certain_age_'.$key] == "no") ? true : false,['class'=>'pension-plan' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>

                                            <div class="col-sm-6 inner-left social-security-details" style="display:<?php
                                                 if (array_key_exists('benefit_coordinatedSS_certain_age_' . $key, $data) && ($data['benefit_coordinatedSS_certain_age_' . $key] == 'yes')) {
                                                     echo 'block';
                                                 } else {
                                                     echo 'none';
                                                 }
                                                 ?>;">
                                                {{ Form::label('label', 'Amount of additional pension?') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon"> $ </span>
                                                    <input style="text-align: right; border-left: 0; width: 226px;" data-validation="" class="borderLeft0 custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][amount_of_additional_pension_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                            if (array_key_exists('amount_of_additional_pension_' . $key, $data)) {
                                                echo $data["amount_of_additional_pension_" . $key];
                                            }
                                            ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6 inner-left social-security-details" style="display:<?php
                                         if (array_key_exists('benefit_coordinatedSS_certain_age_' . $key, $data) && ($data['benefit_coordinatedSS_certain_age_' . $key] == 'yes')) {
                                             echo 'block';
                                         } else {
                                             echo 'none';
                                         }
                                            ?>;">
                                                {{ Form::label('label', 'Until what age?') }}
                                                <div class="input-group service-input-group">
                                                    <input style="text-align:right; padding-right:0!important" data-validation="" class="borderRight0 full-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][what_minimum_age_would_begin_taking_pension_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                            if (array_key_exists('what_minimum_age_would_begin_taking_pension_' . $key, $data)) {
                                                echo $data["what_minimum_age_would_begin_taking_pension_" . $key];
                                            }
                                            ?>">
                                                    <span class="input-group-addon">years</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                                                    <?php
                                                    if (Session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                        ?>
                                <h3></h3>
                                <fieldset>
                                    <section class="sections">
                                        <div class="col-sm-12 ">
                                            <div class="col-md-4 col-xs-11 section-left">
                                                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                                <h2>Spousal Benefit</h2>

                                            </div>
                                            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                                <div class="col-sm-6 inner-left ">
                                                    {{ Form::label('label', 'Spousal benefit') }}
                                                    <div class="input-group service-input-group">
                                                        <input style="text-align:right; padding-right:0 !important;" data-validation="" class="custom-validation borderRight0 full-width  form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<?php echo $key; ?>][spouse_estimated_annual_benefit_amount_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                            if (array_key_exists('spouse_estimated_annual_benefit_amount_' . $key, $data)) {
                                                echo $data["spouse_estimated_annual_benefit_amount_" . $key];
                                            }
                                            ?>">
                                                        <span class="input-group-addon">%</span>
                                                    </div>
                                                </div>
                                            </div>
                                    </section>
                                </fieldset>
        <?php } ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Beneficiaries & Trusts</h2>

                                            <p> We will address contingent beneficiaries later. </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left insurance-beneficiary-modal">

                                                {{ Form::label('label', 'Beneficiaries') }}
                                                <p style="width:100%;"><i class="here-open" >If a trust is named, click <a class="click-here-open1" data-toggle='modal' data-target='#clickHereModal1' data-count="{{$key}}">here</a></i></p>

                                                <table id='group-life-insurance-info' data-copy="{{$key}}">
                                                    <thead>
                                                        <tr>
                                                            <td style="width:30%;">Beneficiary name</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
        <?php
        if (array_key_exists('IBRecords_' . $key, $data)) {
            $details = json_decode($data["IBRecords_" . $key]);
            if (!empty($details)) {
                foreach ($details as $key1 => $datas) {
                    ?>
                                                                    <tr data-index="{{$key1}}">
                                                                        <td>{{$datas->InsuranceName}}<?php if ($datas->primary) { ?>
                                                                                <i class="fa fa-check" aria-hidden="true"></i>
                    <?php } ?></td>
                                                                        <td></td>
                                                                        <td align="right"><i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <button type='button' class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal" data-copy="<?php echo $key; ?>">Add beneficiary</button>
                                            </div>
                                            <div class="col-sm-6 inner-left insurance-beneficiary-modal">
                                                <table id ='group-life-insurance-info1'>
                                                    <thead>
                                                        <tr>
                                                            <td style="width:35%;">Trust Name</td>
                                                            <td style="width:35%;">Trustee Name </td>
                                                            <td style="width:25%;"></td>
                                                            <td style="width:25%;"></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
        <?php
        if (array_key_exists('clickHereFormRecordsC_' . $key, $data)) {
            $details1 = json_decode($data["clickHereFormRecordsC_" . $key]);
            if (!empty($details1)) {
                foreach ($details1 as $key11 => $datas) {
                    ?>
                                                                    <tr data-index="{{$key11}}">
                                                                        <td style="width:25%;">{{$datas->nameOfTrust}}</td>
                                                                        <td style="width:25%;">{{$datas->trustee}}</td>
                                                                        <td align="right" style="width:50%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count="{{$key}}" aria-hidden="true"></i>
                                                                            <i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i></td>
                                                                    </tr>
                    <?php
                }
            }
        }
        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[141][copy][<?php echo $key; ?>][IBRecords_<?php echo $key; ?>]"><?php
                                                if (array_key_exists('IBRecords_' . $key, $data)) {
                                                    echo $data["IBRecords_" . $key];
                                                }
                                                ?></textarea>
                                            <textarea id="clickHereFormData1" class="hidden" name="answer[141][copy][<?php echo $key; ?>][clickHereFormRecordsC_<?php echo $key; ?>]">
                                                        <?php
                                                        if (array_key_exists('clickHereFormRecordsC_' . $key, $data)) {
                                                            echo $data["clickHereFormRecordsC_" . $key];
                                                        }
                                                        ?>
                                            </textarea>

                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Contingent Beneficiaries </h2>

                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left ">

                                                {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'yes',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "yes") ? true : false,['class'=>'contingent-beneficiaries table-confirmation' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'no',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "no") ? true : false,['class'=>'contingent-beneficiaries table-confirmation' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>

                                            </div>
                                            <div class="col-sm-6 inner-left contingent-beneficiaries-modal " style="display:<?php
                                                if (array_key_exists('contingent_beneficiaries_' . $key, $data) && ($data['contingent_beneficiaries_' . $key] == 'yes')) {
                                                    echo 'block';
                                                } else {
                                                    echo 'none';
                                                }
                                                ?>">
                                                {{ Form::label('label', 'Contingent Beneficiaries') }}
                                                <table id='contingent-beneficiary-info'>
                                                    <thead>
                                                        <tr>
                                                            <td>Beneficiary name</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">

                                            <?php
                                            if (array_key_exists('CBRecords_' . $key, $data)) {
                                                $detailss = json_decode($data["CBRecords_" . $key]);
                                                if (!empty($detailss)) {
                                                    foreach ($detailss as $key2 => $datass) {
                                                        ?>
                                                                    <tr data-index='{{$key2}}'>
                                                                        <td style="width:50%;">{{$datass->contingentInsuranceName}}</td>
                                                                        <td align="right" style="width:50%;"> <i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i>
                                                                            <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                                    </tr>

                    <?php
                }
            }
        }
        ?>

                                                    </tbody>
                                                </table>
                                                <button type='button' class="contingent-beneficiary add-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal" data-copy="{{$key}}">Add beneficiary</button>
                                            </div>
                                            <textarea id="contingentBeneficiaryData" class="hidden" name="answer[141][copy][<?php echo $key; ?>][CBRecords_<?php echo $key; ?>]">
                                                        <?php
                                                        if (array_key_exists('CBRecords_' . $key, $data)) {
                                                            echo $data["CBRecords_" . $key];
                                                        }
                                                        ?>
                                            </textarea>

                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections" >
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                                            <h2>Additional pension plans</h2>


                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left ">

                                                {{ Form::label('label', 'Do you have a pension benefit from another employer?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][pension_benefit_from_another_employer_'.$key.']', 'yes',(array_key_exists('pension_benefit_from_another_employer_'.$key,$data) && $data['pension_benefit_from_another_employer_'.$key] == "yes") ? true : false,['class'=>'pension-benefit' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[141][copy]['.$key.'][pension_benefit_from_another_employer_'.$key.']', 'no',(array_key_exists('pension_benefit_from_another_employer_'.$key,$data) && $data['pension_benefit_from_another_employer_'.$key] == "no") ? true : false,['class'=>'pension-benefit' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
        <?php
    }
}
?>
                    <textarea id="clickHereFormData" class="hidden" name="answer[141][clickHereFormRecords]">{{(!empty($answer["clickHereFormRecords"]))? $answer["clickHereFormRecords"]:null}}</textarea>
                    <div class="sections" style="min-height:0px! important;">

                        <div id="insuranceBeneficiaryModal" class="modal fade add-student-modal" role="dialog" >
                            <div class="modal-dialog">
                                <div class="modal-content" style="overflow:hidden;">
                                    <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">ADD BENEFICIARY</h4>
                                    </div>
                                    <div class="modal-body" style='border:none; float: left;'>
                                        <div class="row">
                                            <div class="setup-content">
                                                <div class="section-right">
                                                    <div class="validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','First Name') }}
                                                            {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'first name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Last Name') }}
                                                            {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Date of birth') }}
                                                            {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Relationship') }}
                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                            <select style="position: absolute;" class="relationship" name="relationship" required="" aria-invalid="false" >
                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                <option value="1">Spouse</option><option value="2">Child</option>
                                                                <option value="3">Other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left other-relationship-input" style="display: none;" >
                                                            {{ Form::label('label','Other') }}
                                                            {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control beneficiary-other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'describe', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <div class="primary-option"> 
                                                                <ul> 
                                                                    <p class="primary-select">Primary ?</p>
                                                                    <li style="display:inline; float: left; width: 50%;"> 
                                                                        <label class="checkbox-custom-label">
                                                                            <input type="checkbox" class="primary-selected">
                                                                            <span class="checkbox-icon"></span>
                                                                            <span>
                                                                            </span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Percentage (max 100%)') }}
                                                            <div class="input-group">
                                                                {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0  custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3', 'style'=>' border-right:0px;']) }}
                                                                <span class="input-group-addon" style="height: 51px; border-left: 0px;"> % </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button data-copy="" id='insuranceBeneficiariesForm' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 20px;
                                                                    margin-top: 30px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CLICK HERE MODAL-->

                        <div id="clickHereModal1" class="modal fade add-student-modal" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content" style="overflow:hidden;">
                                    <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">ADD TRUST</h4>
                                    </div>
                                    <div class="modal-body" style='border:none; float: left;'>
                                        <div class="row">
                                            <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                <div class="col-sm-12 validation-alert">
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Name of trust') }}
                                                        {{ Form::input('text','name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Trustee') }}
                                                        {{ Form::input('text','trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Successor trustee') }}
                                                        {{ Form::input('text','successor_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control successor-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Primary beneficiary') }}
                                                        {{ Form::input('text','primary_beneficiary',null,['data-validation'=> '' , 'class'=>'custom-validation form-control primary-beneficiary', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full names', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        <a class='append-beneficiary-input1'>+Beneficiary</a>
                                                    </div>
                                                    <div class="col-sm-12 append-input1 inner-left">
                                                    </div>

                                                    <div class="col-sm-12 inner-left">
                                                        <button id='clickHereForm1' type="button" style="border-radius: 3px;
                                                                color: #fff;    
                                                                font-family: Lato;
                                                                font-size: 18px;
                                                                line-height: 24px;                                  
                                                                text-align: center;
                                                                width: 150px;
                                                                padding: 12px 35px;
                                                                background-color: #2179EE;
                                                                text-decoration: none;
                                                                border: none; 
                                                                margin-left: 62px;
                                                                margin-bottom: 40px;">
                                                            Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CLICK HERE MODAL END-->

                        <!-- BENEFICIARY MODAL END-->


                        <div id="contingentBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content" style="overflow:hidden;">
                                    <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">ADD CONTINGENT BENEFICIARIES</h4>
                                    </div>
                                    <div class="modal-body" style='border:none; float: left;'>
                                        <div class="row">
                                            <div class="setup-content">
                                                <div class="section-right">
                                                    {{ Form::input('hidden','questionName[896]','ADD a Student') }}
                                                    <div class="validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','First Name') }}
                                                            {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control contingent-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Last Name') }}
                                                            {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Date of birth') }}
                                                            {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Relationship') }}
                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                            <select style="position: absolute;" class="contingent-relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left contingent-other-relationship" style="display: none;">
                                                            {{ Form::label('label','Other') }}
                                                            {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Percentage (max 100%)') }}
                                                            <div class="input-group">
                                                                {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0  custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3', 'style'=>' border-right:0px;']) }}
                                                                <span class="input-group-addon" style="height: 51px; border-left: 0px;"> % </span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='contingentBeneficiaryForm' data-number=""  class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-------CONTINGENT BENEFICIARY MODAL END-------->


                    </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- BENEFICIARY MODAL-->
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <div class="sections">

                <!--first 3 modal were here--> 

            </div>  <!--section end-->
        </div>
    </div>
</div>


@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>

<script id="step1" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Pension plans(defined benefit plans)</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <input name="questionName[141]" type="hidden" value="">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Does your company offer a pension plan (also known as defined benefit plan)?</label>
                    <label class="radio-custom-label">
                        <input class="pension-plan" required="required" name="answer[141][copy][<%= copynum %>][company_offer_pension_plan_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="pension-plan" required="required" name="answer[141][copy][<%= copynum %>][company_offer_pension_plan_<%= copynum %>]" type="radio" value="no" aria-required="true">No 
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </section> 
</script>


<script id="step2" type="text/html">
    <section class="sections">                            
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Annual Benefit Amount</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left ">
                    {{ Form::label('label', 'Estimated annual benefit amount') }}
                    <div class="input-group service-input-group"> 
                        <span class="input-group-addon">$</span>
                        <input data-validation="" class="borderLeft0 full-width comprehensive-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<%= copynum %>][estimated_annual_benefit_amount_<%= copynum %>]" type="number" aria-required="true">
                    </div>
                </div>
            </div>
        </div>
    </section> 
</script>


<script id="step3" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Age & Estimated Benefits</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">  
                <div class="col-sm-6 inner-left ">
                    {{ Form::label('label', 'At what age do you expect to begin taking the pension?') }}
                    <div class="input-group service-input-group">
                        <input data-validation="" class="custom-validation form-control borderRight0 full-width " data-rule-regex="false" required="" placeholder="" style="padding-right: 0 !important; text-align:right;" name="answer[141][copy][<%= copynum %>][age_expect_taking_pension_<%= copynum %>]" type="number" aria-required="true">
                        <span class="input-group-addon">years</span>
                    </div>
                </div>
                <div class="col-sm-6 inner-left ">
                    {{ Form::label('label', 'At what minimum age would you be able to begin taking the pension?') }}
                    <div class="input-group service-input-group">
                        <input data-validation="" class="custom-validation form-control minimum-age borderRight0 full-width" data-rule-regex="false" required="" placeholder="" style="padding-right: 0 !important;  text-align:right;" name="answer[141][copy][<%= copynum %>][minimum_age_able_begin_taking_pension_<%= copynum %>]" type="number" aria-required="true">
                        <!--<p style="position: absolute; top: 57px; right: 37px; font-weight: 400;">years</p>-->
                        <span class="input-group-addon">years</span>
                    </div>
                </div>
                <div class="col-sm-6 inner-left estimated-benefit" style="display: none;">
                    {{ Form::label('label', 'What would the estimated benefit be if you begin taking at that age?') }}
                    <div class="input-group service-input-group">
                        <span class="input-group-addon">$</span>
                        <input data-validation="" class="custom-validation form-control borderLeft0 full-width " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<%= copynum %>][estimated_benefit_begin_taking_age_<%= copynum %>]" type="number" aria-required="true">
                    </div>
                </div>
            </div>
        </div>
    </section> 
</script>
<script id="step4" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Pension & Social Security</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Is your benefit coordinated with SS until you reach a certain age?</label>
                    <label class="radio-custom-label">
                        <input class="social-security" required="required" name="answer[141][copy][<%= copynum %>][benefit_coordinatedSS_certain_age_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="social-security" required="required" name="answer[141][copy][<%= copynum %>][benefit_coordinatedSS_certain_age_<%= copynum %>]" type="radio" value="no" aria-required="true">No 
                        <span class="radio-icon"></span>
                    </label>
                </div>

                <div class="col-sm-6 inner-left social-security-details" style="display: none;">
                    {{ Form::label('label', 'Amount of additional pension?') }}
                    <div class="input-group service-input-group" >
                        <span class="input-group-addon"> $ </span>
                        <input data-validation="" class="full-width borderLeft0 custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<%= copynum %>][amount_of_additional_pension_<%= copynum %>]" type="number" aria-required="true">
                    </div>
                </div>
                <div class="col-sm-6 inner-left social-security-details" style="display: none;">
                    {{ Form::label('label', 'Until what age?') }}
                    <div class="input-group service-input-group">
                        <input data-validation="" class="custom-validation borderRight0 full-width form-control minimum-age valid" data-rule-regex="false" required="" placeholder="" style="text-align:right; padding-right:0 !important;" name="answer[141][copy][<%= copynum %>][what_minimum_age_would_begin_taking_pension_<%= copynum %>]" type="number" aria-required="true" aria-invalid="false">
                        <span class="input-group-addon">years</span>
                    </div>
                </div>
            </div>
        </div>
    </section> 
</script>
<?php if (Session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
    ?>
    <script id="step5" type="text/html">
        <section class="sections">
            <div class="col-sm-12 ">
                <div class="col-md-4 col-xs-11 section-left">
                    <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                    <h2>Spousal Benefit</h2>

                </div>
                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                    <div class="col-sm-6 inner-left ">
                        {{ Form::label('label', 'Spousal benefit') }}
                        <div class="inner-left input-group service-input-group">
                            <input style="text-align:right; padding-right:0 !important;" data-validation="" class="comprehensive-width borderRight0 full-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[141][copy][<%= copynum %>][spouse_estimated_annual_benefit_amount_<%= copynum %>]" type="number" aria-required="true">

                            <span class="input-group-addon"> %  </span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </script>
<?php } ?>

<script id="step6" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Beneficiaries & Trusts </h2>
                <p> We will address contingent beneficiaries later. </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left insurance-beneficiary-modal">

                    {{ Form::label('label', 'Beneficiaries') }}
                    <p style="width:100%;"><i class="here-open" >If a trust is named, click <a class="click-here-open1" data-toggle='modal' data-target='#clickHereModal1' data-count="<%= copynum %>">here</a></i></p>

                    <table id='group-life-insurance-info'>
                        <thead>
                            <tr>
                                <td style="width:30%;">Beneficiary name</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type="button" class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal" data-copy="<%= copynum %>" >Add beneficiary</button>
                </div>
                <div class="col-sm-6 inner-left insurance-beneficiary-modal">
                    <table id='group-life-insurance-info1'>
                        <thead>
                            <tr>
                                <td style="width:35%;">Trust name</td>
                                <td style="width:35%;">Trustee Name </td>
                                <td style="width:25%;"></td>
                                <td style="width:25%;"></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                </div>
                <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[141][copy][<%= copynum %>][IBRecords_<%= copynum %>]"></textarea>
                <textarea id="clickHereFormData1" class="hidden" name="answer[141][copy][<%= copynum %>][clickHereFormRecordsC_<%= copynum %>]"></textarea>
            </div>
        </div>
    </section> 
</script>
<script id="step7" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left ">

                    {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries table-confirmation" required="required" name="answer[141][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries table-confirmation" required="required" name="answer[141][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>

                </div>
                <div class="col-sm-6 inner-left contingent-beneficiaries-modal " style="display:none;">
                    {{ Form::label('label', 'Contingent Beneficiaries') }}
                    <table id='contingent-beneficiary-info'>
                        <thead>
                            <tr>
                                <td>Beneficiary name</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type='button' class="contingent-beneficiary add-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal"  data-copy="<%= copynum %>">Add beneficiary</button>
                </div>
                <textarea id="contingentBeneficiaryData" class="hidden" name="answer[141][copy][<%= copynum %>][CBRecords_<%= copynum %>]"></textarea>

            </div>
        </div>
    </section> 
</script>

<script id="anotherPensionPlans" type="text/html">
    <div class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} PENSION PLANS</h6>
                <h2> Additional pension plans</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left ">

                    {{ Form::label('label', 'Do you have a pension benefit from another employer?') }}
                    <label class="radio-custom-label">
                        <input class="pension-benefit" required="required" name="answer[141][copy][<%= copynum %>][pension_benefit_from_another_employer_<%= copynum %>]" type="radio" value="yes">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="pension-benefit" required="required" name="answer[141][copy][<%= copynum %>][pension_benefit_from_another_employer_<%= copynum %>]" type="radio" value="no">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="clickHereBeneficiary1" type="text/html">
    <div class="primary-beneficiary-wrapper1">
        <label for="label" >Beneficiary <span class="beneficiary-count1">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" class="contingent-check" name="contingent1[<%= count%>]">
                        <span class="checkbox-icon"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[<%= count%>]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>
<script>
var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,13,'spouse-employment-income-retirement-preview'])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    var tempArray = <?php echo json_encode($answer); ?>;
    if (!($.isEmptyObject(tempArray.copy))) {
        var copynumber = tempArray.copy.length - 1;
    } else {
        var copynumber = 0;
        appendSteps(copynumber);
    }
    $('.fa-check').hide();
    $('a[href="#finish"]').on('click', function () {
        if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.pension-benefit:checked').val() == 'no' || $('a[href="#finish"]').text() == 'Continue') {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
    });

    $(document).on('change', '.pension-benefit', function () {
        var $this = $(this);
        if ($this.val() == 'yes' && $this.is('checked')) {
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
    });
    $(document).on('change', '.minimum-age', function () {
        if ($(this).val() > 0) {
            $('.estimated-benefit, .estimated-benefit input').show();
        } else {
            $('.estimated-benefit, .estimated-benefit input').hide().val('');
        }
    });

    $(".minimum-age").keyup(function () {
        if ($(this).val() > 0) {
            $('.estimated-benefit, .estimated-benefit input').show();
        } else {
            $('.estimated-benefit, .estimated-benefit input').hide().val('');
        }
    });

    $(document).on('change', '.social-security', function () {
        if ($(this).val() == 'yes') {
            $('.social-security-details, .social-security-details input').show();
        } else {
            $('.social-security-details, .social-security-details input').hide().val('');
        }
    });

    $('#clickHereModal .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            $('#clickHereModal').modal('hide');
        }
        return false;
    });

    $(document).on('change', '.contingent-beneficiaries', function () {
        if ($(this).val() == 'yes') {
            $('.contingent-beneficiaries-modal').show();
        } else {
            $('.contingent-beneficiaries-modal').hide();
        }

    });
    $(document).on('change', '.contingent-relationship', function () {
        if ($(this).val() == '3') {
            $('.contingent-other-relationship, .contingent-other-relationship input').show();
        } else {
            $('.contingent-other-relationship, .contingent-other-relationship input').hide().val('');
        }
    });
    $(document).on('change', '.relationship', function () {
        if ($(this).val() == '3') {
            $('.other-relationship-input, .other-relationship-input input').show();
        } else {
            $('.other-relationship-input, .other-relationship-input input').hide().val('');
        }
    });
    $(document).on('change', '.pension-benefit', function () {
        $("select").selectBoxIt();
        if ($(this).val() == 'yes') {
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            removeSteps();

        }
        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.pension-benefit:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                copynumber++;
                appendSteps(copynumber);
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });
    });
//append steps
    function appendSteps(copynumber)
    {
        var plan1 = $('#step1').html();
        var plan2 = $('#step2').html();
        var plan3 = $('#step3').html();
        var plan4 = $('#step4').html();
        var plan5 = $('#step5').html();
        var plan6 = $('#step6').html();
        var plan7 = $('#step7').html();
        var plan8 = $('#anotherPensionPlans').html();
        var taxFillingStatus = <?php echo Session::get('tax_filing_status'); ?>;
        var taxFillingStatusInverse = <?php echo config('constant.tax_filing_status_inverse.single'); ?>;
        html1 = _.template(plan1);
        html2 = _.template(plan2);
        html3 = _.template(plan3);
        html4 = _.template(plan4);
        if (taxFillingStatus != taxFillingStatusInverse) {
            html5 = _.template(plan5);
        }
        html6 = _.template(plan6);
        html7 = _.template(plan7);
        html8 = _.template(plan8);
        var cind = form.steps('getCurrentIndex');
        $("form").steps("add", {
            content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        $("form").steps("add", {
            content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        $("form").steps("add", {
            content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        $("form").steps("add", {
            content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        if (taxFillingStatus != taxFillingStatusInverse) {
            $("form").steps("add", {
                content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
        }
        $("form").steps("add", {
            content: html6({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        $("form").steps("add", {
            content: html7({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
        });
        $("form").steps("add", {
            content: html8({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),

        });
    }
    function removeSteps() {
        var copyind = form.steps('getCurrentIndex');
        var totalSteps = $('#services-question-form fieldset').length;
        if (totalSteps > copyind) {
            var len = totalSteps - copyind;
            for (var i = 1; i <= len; i++) {
                var index = copyind + 1;
                $("form").steps("remove", index);
            }
        }
    }
    //    /---------BENEFICIARY FORM ---------/

    $("select").selectBoxIt();
    $('#insuranceBeneficiariesForm').on('click', function () {
        var insuranceList = [];
        $('#insuranceBeneficiaryModal .error-alert').remove();
        var copyVal = $(this).attr('data-number');
        var insuranceBeneficiaryDataVal = $('textarea[name="answer[141][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').val();
        if (insuranceBeneficiaryDataVal.length > 0) {
            insuranceList = JSON.parse(insuranceBeneficiaryDataVal);
        }
        if ($('#insuranceBeneficiaryModal .insurance-first-name').val() && $('#insuranceBeneficiaryModal .last-name').val() && $('#insuranceBeneficiaryModal .percentage').val()) {

            if ($(this).hasClass('edit-form')) {
                insuranceList[$(this).attr('data-index')] = {
                    InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                    lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                    dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                    relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                    beneficiaryOtherRelationship: $('#insuranceBeneficiaryModal .beneficiary-other-relationship-description').val(),
                    primary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                    percentage: $('#insuranceBeneficiaryModal .percentage').val()
                }
                if ($('.primary-selected').is(':checked')) {
                    $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '<i class="fa fa-check" aria-hidden="true"></i></td><td></td> <td align="right"> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                } else {
                    $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td> <td></td><td align="right"> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                }
            } else {
                if ($('.primary-selected').is(':checked')) {
                    $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '<i class="fa fa-check" aria-hidden="true"></i></td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                } else {
                    $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                }
                insuranceList.push({
                    InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                    lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                    dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                    relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                    primary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                    beneficiaryOtherRelationship: $('#insuranceBeneficiaryModal .beneficiary-other-relationship-description').val(),
                    percentage: $('#insuranceBeneficiaryModal .percentage').val()
                });
            }
            $('.fa-check').hide();
            $('#insuranceBeneficiaryModal').modal('hide');
            $('textarea[name="answer[141][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').html(JSON.stringify(insuranceList));
        } else {
            $('#insuranceBeneficiaryModal input, #insuranceBeneficiaryModal select').each(function (i, elem) {
                if (!$(elem).val()) {
                    $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                }
            });
        }
    });

    $('#insuranceBeneficiaryModal input, #insuranceBeneficiaryModal select').on('keyup change', function () {
        $(this).parent().find('.error-alert').remove();
        if (!$(this).val()) {
            $(this).parent().append('<label class="error-alert">This field is required.</label>');
        }
    });

    $(document).on('click', '#group-life-insurance-info .group-insurance-edit', function () {
        $('#insuranceBeneficiaryModal').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#insuranceBeneficiariesForm").attr("data-number", copyArr);
        insuranceList = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = insuranceList[index];
        $('#insuranceBeneficiaryModal .insurance-first-name').val(insuranceDetails.InsuranceName);
        $('#insuranceBeneficiaryModal .last-name').val(insuranceDetails.lastName);
        $('#insuranceBeneficiaryModal .date-of-birth').val(insuranceDetails.dob);
        $('#insuranceBeneficiaryModal .relationship').val(insuranceDetails.relationship).trigger('change');
        $('#insuranceBeneficiaryModal .beneficiary-other-relationship-description').val(insuranceDetails.beneficiaryOtherRelationship);
        $('#insuranceBeneficiaryModal .percentage').val(insuranceDetails.percentage);
        if (insuranceDetails.primary) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', true);
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', false);
        }
        if ($(this).closest('tr').find('.fa-check').length || $('#group-life-insurance-info tbody[data-copy="' + copyArr + '"]').find('.fa-check').length == 0) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', false);
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', true);
        }
        $('#insuranceBeneficiariesForm').addClass('edit-form').attr('data-index', index);
        $('#insuranceBeneficiaryModal .error-alert').remove();
        $('.fa-check').hide();
    });

    $(document).on('click', '.add-beneficiary', function () {
        var copyArr = $(this).attr('data-copy');
        $('#insuranceBeneficiariesForm').removeClass('edit-form');
        $('#insuranceBeneficiaryModal input[type="text"],#insuranceBeneficiaryModal input[type="number"]').val('');
        $('#insuranceBeneficiaryModal select').val('').trigger('change');
        $('#insuranceBeneficiaryModal .primary-selected:checked').prop('checked', false);
        $('#insuranceBeneficiaryModal .error-alert').remove();
        $("#insuranceBeneficiariesForm").attr("data-number", $(this).attr('data-copy'));
        if ($('#group-life-insurance-info tbody[data-copy="' + copyArr + '"]').find('.fa-check').length > 0) {
            $('.primary-selected').prop('disabled', true);
        } else {
            $('.primary-selected').prop('disabled', false);
        }
    });

    $(document).on('click', '#group-life-insurance-info .fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index')
        deleteRow(index_id);
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        insuranceDetails = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').val());
        $('.swal-button--danger').click(function () {
            insuranceDetails.splice(index_id, 1);
            $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyArr + ']').empty();
            if (insuranceDetails.length != 0) {
                var tr = '';
                $.each(insuranceDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '" ><td>' + value.InsuranceName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyArr + ']').html(tr);
            }
            $('textarea[name="answer[141][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').html(JSON.stringify(insuranceDetails));
        });
        return false;

    });
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    //    /----CONTINGENT BENEFICIARY MODAL-----/

    $('#contingentBeneficiaryForm').on('click', function () {
        var contingentList = [];
        var copyVal = $(this).attr('data-number');
        var contingentBeneficiaryDataVal = $.trim($('textarea[name="answer[141][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').val());
        if (contingentBeneficiaryDataVal.length > 0) {
            contingentList = JSON.parse(contingentBeneficiaryDataVal);
        }
        $('#contingentBeneficiaryModal .error-alert').remove();
        if ($('#contingentBeneficiaryModal .contingent-first-name').val() && $('#contingentBeneficiaryModal .last-name').val() && $('#contingentBeneficiaryModal .percentage').val()) {

            if ($(this).hasClass('edit-form')) {
                contingentList[$(this).attr('data-index')] = {
                    contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                    contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                    contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                    contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                    otherInput: $('#contingentBeneficiaryModal .other-relationship-description').val(),
                    contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                }
                $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td style="width:50%;">' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td><td align="right" style="width:50%;"><i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
            } else {
                $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + contingentList.length + '><td style="width:50%;">' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td><td align="right" style="width:50%;"><i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                contingentList.push({
                    contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                    contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                    contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                    contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                    otherInput: $('#contingentBeneficiaryModal .other-relationship-description').val(),
                    contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                });
            }

            $('#contingentBeneficiaryModal').modal('hide');
            $('textarea[name="answer[141][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').html(JSON.stringify(contingentList));
//                $('#contingentBeneficiaryModal .contingent-first-name,#contingentBeneficiaryModal .last-name, #contingentBeneficiaryModal .percentage, #contingentBeneficiaryModal .date-of-birth').val('');
        } else {

            $('#contingentBeneficiaryModal input, #contingentBeneficiaryModal select').each(function (i, elem) {
                if (!$(elem).val()) {
                    $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                }
            });
        }

    });

    $('#contingentBeneficiaryModal input, #contingentBeneficiaryModal select').on('keyup change', function () {
        $(this).parent().find('.error-alert').remove();
        if (!$(this).val()) {
            $(this).parent().append('<label class="error-alert">This field is required.</label>');
        }
    });

    $(document).on('click', '.beneficiary-edit', function () {
        $('#contingentBeneficiaryModal').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#contingentBeneficiaryForm").attr("data-number", copyArr);
        contingentList = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        contingentDetails = contingentList[index];
        $('#contingentBeneficiaryModal .contingent-first-name').val(contingentDetails.contingentInsuranceName);
        $('#contingentBeneficiaryModal .last-name').val(contingentDetails.contingentLastName);
        $('#contingentBeneficiaryModal .date-of-birth').val(contingentDetails.contingentDateOfBirth);
        $('#contingentBeneficiaryModal .contingent-relationship').val(contingentDetails.contingentRelationship).trigger('change');
        $('#contingentBeneficiaryModal .contingent-other-relationship').val(contingentDetails.otherInput);
        $('#contingentBeneficiaryModal .percentage').val(contingentDetails.contingentPercentage);
        $('#contingentBeneficiaryForm').addClass('edit-form').attr('data-index', index);
        $('#contingentBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '.contingent-beneficiary', function () {
        $("#contingentBeneficiaryForm").attr("data-number", $(this).attr('data-copy'));
        $('#contingentBeneficiaryForm').removeClass('edit-form');
        $('#contingentBeneficiaryModal input[type="text"],#contingentBeneficiaryModal input[type="number"]').val('');
        $('#contingentBeneficiaryModal select').val('').trigger('change');
        $('#contingentBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '#contingent-beneficiary-info .fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index')
        deleteRow(index_id);
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $('.swal-button--danger').click(function () {
            contingentDetails = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').val());
            contingentDetails.splice(index_id, 1);
            $(".contingent-beneficiaries-modal #contingent-beneficiary-info tbody[data-copy=" + copyArr + "]").empty();
            if (contingentDetails.length != 0) {
                var tr = '';
                $.each(contingentDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.contingentInsuranceName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $(".contingent-beneficiaries-modal #contingent-beneficiary-info tbody[data-copy=" + copyArr + "]").html(tr);
            }
            $('textarea[name="answer[141][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').html(JSON.stringify(contingentDetails));
        });
    });
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });


//copy add trust
    $(document).on('click', '.append-beneficiary-input1', function () {
        var number1 = $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text();
        if (number1 == '') {
            number1 = 1;
        }
        var beneficiaryInput1 = $('#clickHereBeneficiary1').html();
        html = _.template(beneficiaryInput1);
        number1++;
        var countNumberr = {
            count: number1
        };
        $('.append-input1').append(html((countNumberr)));
        $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text(number1);
    });
    $(document).on('click', '.click-here-open1', function () {
        var count = $(this).attr('data-count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereForm1').removeClass('edit-form').removeAttr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(''),
                $('#clickHereModal1 .trustee').val(''),
                $('#clickHereModal1 .successor-trustee').val(''),
                $('#clickHereModal1 .primary-beneficiary').val('')

        $('#clickHereModal1 .error-alert').remove();

    });

    $(document).on('click', '#group-life-insurance-info1 .group-insurance-edit1', function () {

        var trustList1 = [];
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList1 = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        trustDetails1 = trustList1[index];

        var count = $(this).data('count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereModal1').modal();
        var index = $(this).closest('tr').attr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(trustDetails1.nameOfTrust),
                $('#clickHereModal1 .trustee').val(trustDetails1.trustee),
                $('#clickHereModal1 .successor-trustee').val(trustDetails1.sucessorTrustee),
                $('#clickHereModal1 .primary-beneficiary').val(trustDetails1.primaryBeneficiary)
        var html = '';
        var count = 1;
        for (var i = 0; i < trustDetails1.benficiary.length; i++) {
            count++;
            var checked = '';
            if (trustDetails1.contingent[i]) {
                checked = 'checked';
            }
            html += '<div class="primary-beneficiary-wrapper1"><label for="label" >Beneficiary <span class="beneficiary-count1">' + count + '</span></label><div class="primary-option"><ul>\n\
                     <p class="primary-select"><i>contingent</i></p><li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label"><input type="checkbox" class="contingent-check" name="contingent1[' + count + ']" ' + checked + '>\n\
<span class="checkbox-icon"></span><span></span> </label></li></ul></div><input data-validation="" value="' + trustDetails1.benficiary[i] + '" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[' + count + ']" type="text" aria-required="true" style="margin-top: 6px;"></div>';
        }
        $('.append-input1').append(html);
        $('#clickHereForm1').addClass('edit-form').attr('data-index', index);
        $('#clickHereModal1 .error-alert').remove();
    });



    $(document).on('click', '#clickHereForm1', function () {
        var count = $(this).attr('data-number');
        var extendedBeneficiary = $("[name^='extended_beneficiary1']")
                .map(function () {
                    return $(this).val();
                }).get();
        var contingentBeneficiary = $("[name^='contingent1']")
                .map(function () {
                    return $(this).prop('checked');
                }).get();
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {

            var trustListcopy = [];
            var trustDetails = $.trim($('textarea[name="answer[141][copy][' + count + '][clickHereFormRecordsC_' + count + ']"]').val());
            if (trustDetails.length > 0) {
                trustListcopy = JSON.parse(trustDetails);
            }



            $('#clickHereModal1 .error-alert').remove();
            if ($('#clickHereModal1 .name-of-trust').val()) {

                if ($(this).hasClass('edit-form')) {
                    trustListcopy[$(this).attr('data-index')] = {
                        nameOfTrust: $('#clickHereModal1 .name-of-trust').val(),
                        trustee: $('#clickHereModal1 .trustee').val(),
                        sucessorTrustee: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    }
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#clickHereModal1 .name-of-trust').val() + '</td> <td>' + $('#clickHereModal1 .trustee').val() + '</td> <td align="right" style="width:50%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count="' + count + '" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td>');

                } else {
                    trustListcopy.push({
                        nameOfTrust: $('#clickHereModal1 .name-of-trust').val(),
                        trustee: $('#clickHereModal1 .trustee').val(),
                        sucessorTrustee: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    });
                    var listlength = trustListcopy.length - 1;
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + ']').append('<tr data-index=' + listlength + '><td>' + $('#clickHereModal1 .name-of-trust').val() + '</td><td>' + $('#clickHereModal1 .trustee').val() + '</td><td align="right" style="width:50%;"> <i title="Edit" class="fa fa-pencil group-insurance-edit1 " data-count="' + count + '" aria-hidden="true"></i>  <i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td></tr>');

                }

            } else {
                $('#clickHereModal1 input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }

                });
            }
            $('textarea[name="answer[141][copy][' + count + '][clickHereFormRecordsC_' + count + ']"]').html(JSON.stringify(trustListcopy));
            $('#clickHereModal1').modal('hide');
        }
    });

    $(document).on('click', '.insurance-beneficiary-modal #group-life-insurance-info1 .group-insurance-trash1 ', function () {
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList = JSON.parse($('textarea[name="answer[141][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').val());
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            trustList.splice(index_id, 1);
            $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').empty();
            if (trustList.length != 0) {
                var tr = '';
                $.each(trustList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.nameOfTrust + '</td><td>' + value.trustee + '</td><td  align="right" style="width:50%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count=' + copyArr + ' aria-hidden="true"></i><i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td></tr>';
                });
                $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').html(tr);
            }


            $('textarea[name="answer[141][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').html(JSON.stringify(trustList));
            return false;
        });
    });
//end copy trust
});


//    /----CONTINGENT BENEFICIARY MODAL END-----/


</script>
<style type="text/css">
    .fa-pencil{margin-right: 10px;}
    .wrapper .sections .section-right .inner-left label.error{margin-top: 0 !important;}
</style>
@stop 
