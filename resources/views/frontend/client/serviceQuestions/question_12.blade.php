<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about this credit card.</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[12][questionName]','Details about this credit card.') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Give this card a name') }}
            {{ Form::input('text','data[12][answer][card-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Which bank inssues this card?') }}
            {{ Form::input('text','data[12][answer][card-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Bank name']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What type of card is it?') }}
            {{ Form::input('text','data[12][answer][card-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Visa,Discover,Nordstorm']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Brand (may be a store,airline etc.)') }}
            {{ Form::input('text','data[12][answer][airline-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'United Airlines, etc.']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is this a rewards point or cash back card?') }}
            <label class="radio-custom-label">  {{ Form::radio('data[12][answer][card-type]', 'yes',false,['class'=> 'card-type']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">   
                {{ Form::radio('data[12][answer][card-type]', 'no',false,['class'=> 'card-type']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left cash-back-card"  style="display: none;">
            {{ Form::label('label', 'What is the point balance?') }}
            {{ Form::input('text','data[12][answer][point-balance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Can points be exchanged for cash?') }}
            <label class="radio-custom-label">  {{ Form::radio('data[12][answer][exchange]', 'yes',false,['class'=> 'exchange']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">   {{ Form::radio ('data[12][answer][exchange]', 'no',false,['class'=> 'exchange']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left exchange-rate"  style="display: none;">
            {{ Form::label('label', 'What is the rate at which points may be exchanged for cash(example: 1 to 0.01 would equal one cent per point') }}
            {{ Form::input('number','data[12][answer][point-rate]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the interest rate?') }}
            {{ Form::input('text','data[12][answer][Interest-rate]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the current balance on the card?') }}
            {{ Form::input('text','data[12][answer][card-balance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the minimum monthly payment?') }}
            {{ Form::input('text','data[12][answer][monthly-payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What do you typically pay per month?') }}
            {{ Form::input('text','data[12][answer][pay-per-month]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Approximately how much in new purchases do you put on the card each month?') }}
            {{ Form::input('text','data[12][answer][purchase]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Are you using this card for cash flow?') }}
            <label class="radio-custom-label">  {{ Form::radio('data[12][answer][cash-flow]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">   {{ Form::radio('data[12][answer][cash-flow]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the credit limit?') }}
            {{ Form::input('text','data[12][answer][credit-limit]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'In whose name is this card?') }}
            {{ Form::select('data[12][answer][name-on-card]', ['SELECT','my name', 'spouse/partner','joint']) }}
        </div>
    </div>
</div>