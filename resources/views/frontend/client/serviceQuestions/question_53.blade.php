<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Living Trust</h2>
        <p>The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[53]','Living Trust') }}

        <div class="col-sm-6 inner-left">
            {{ Form::label('label', 'Do you have a living trust?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[53][trust]', 'yes',(!empty($answer) && array_key_exists('trust',$answer)) ? (($answer['trust']=="yes")  ? true : false):false,['class'=>'living-trust' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[53][trust]', 'no',(!empty($answer) && array_key_exists('trust',$answer)) ? (($answer['trust']=="no")  ? true : false):false,['class'=>'living-trust' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-6 inner-left number-of-trustees" style="display: <?php
        if (!empty($answer) && array_key_exists('trust', $answer) && ($answer['trust']
            == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>;">


            <!-- single and co trustee radios starts -->
            <div class="OptionSelection paddingLeft0 col-xs-6 col-sm-6 <?php
            if (!empty($answer) && array_key_exists('living_trust', $answer) && $answer['living_trust']
                == 'singletrustee') {
                echo "married-selected";
            }
            ?>" for="single-trustee">
                <label for="single-trustee" class="label">Single <br>Trustee</label>
                {{ Form::radio('answer[53][living_trust]', 'singletrustee',(!empty($answer) && array_key_exists('living_trust',$answer)) ? (($answer['living_trust']=="singletrustee")  ? true : false) :false,['class'=>'select-trustee',  'id' => 'single-trustee', ' required' => true]) }}
            </div>

            <div class="OptionSelection paddingLeft0 col-xs-6 col-sm-6 <?php
            if (!empty($answer) && array_key_exists('living_trust', $answer) && $answer['living_trust']
                == 'cotrustee') {
                echo "married-selected";
            }
            ?>" for="co-trustee">
                <label for="co-trustee" class="label cotrustee">Co-Trustee</label>
                {{ Form::radio('answer[53][living_trust]', 'cotrustee',(!empty($answer) && array_key_exists('living_trust',$answer)) ? (($answer['living_trust']=="cotrustee")  ? true : false) :false,['class'=>'select-trustee',  'id' => 'co-trustee', ' required'=>true]) }}
            </div>

            <!-- single and co trustee radios finish -->


        </div> 
        <div class="col-sm-6 inner-left type-of-trustee" style="display: <?php
            if (!empty($answer) && array_key_exists('trust', $answer) && ($answer['trust']
                == "yes")) {
                echo 'block';
            } else {
                echo 'none';
            }
            ?>;">

            {{ Form::label('label', 'Is there are corporate trustee now or as successor?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[53][trustee type]', 'yes',(!empty($answer) && array_key_exists('trustee type',$answer)) ? (($answer['trustee type']=="yes")  ? true : false):false,['class'=>'trustee-type' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[53][trustee type]', 'no',(!empty($answer) && array_key_exists('trustee type',$answer)) ? (($answer['trustee type']=="no")  ? true : false):false,['class'=>'trustee-type' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>

        </div>
    </div>
</div>