<div class="col-sm-12 ">
   
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about you</h6>
        <h2>Let's get the rest of your personal information.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[219]','Let\s get the rest of your personal information') }}

        <div class="col-sm-6 padding-left0">
            <div class="inner-left marital-status">
                {{ Form::label('label', 'Marital status') }}
                <select readonly style="position: absolute;" class="status valid select-marital" name="answer[219][marital_status]" required="" aria-invalid="false">
                    <option selected="" disabled="" value="">Select</option>
                    <option value="3" <?php if ($defaultData['taxFillingStatus'] == 1) echo "selected"; ?>>Single</option>
                <option value="1" <?php if (($defaultData['taxFillingStatus'] == 2)||($defaultData['taxFillingStatus'] == 3)||($defaultData['taxFillingStatus'] == 4)||($defaultData['taxFillingStatus'] == 5)||($defaultData['taxFillingStatus'] == 6)) echo "selected"; ?>>Married</option>
                </select>
                <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="top:45px! important; right:25px! important;"></i>
            </div>
        </div>
        
        <div class="col-sm-6 compre-partner-name" style="clear:left; display:<?php
        if (($defaultData['taxFillingStatus'] == 2)|| ($defaultData['taxFillingStatus'] == 3)||($defaultData['taxFillingStatus'] == 4)||($defaultData['taxFillingStatus'] == 5)||($defaultData['taxFillingStatus'] == 6)) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>" >
            <div class="inner-left ">
                <label for="label"> Spouse/partner's first name </label>
                {{ Form::input('text','answer[219][spouse_name]',$spouse_name,['data-validation'=> '' , 'class'=>'spouse_name custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'onKeyup'=>'myFunction()','id'=>'spousefirstname','readonly']) }}
            </div>
        </div>


    </div>
</div>
