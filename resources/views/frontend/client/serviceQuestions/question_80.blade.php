@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',22) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6 style="margin-bottom: 0;">LIABILITIES</h6>
                                    </div>
                                    <h2>Auto Loans</h2>
                                    <p>(Do not include lease information.)</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[80]','Do you have any vehicle loans?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any vehicle loans?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[80][CPM-vl-vechicle-loan]', 'yes',(!empty($answer) && array_key_exists('CPM-vl-vechicle-loan',$answer)) ? (($answer['CPM-vl-vechicle-loan']=="yes")  ? true : false):false,['class'=>'CPMVLVechicleLoan table-confirmation' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[80][CPM-vl-vechicle-loan]', 'no',(!empty($answer) && array_key_exists('CPM-vl-vechicle-loan',$answer)) ? (($answer['CPM-vl-vechicle-loan']=="no")  ? true : false):false,['class'=>'CPMVLVechicleLoan table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left CPMVLDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('CPM-vl-vechicle-loan', $answer) && ($answer['CPM-vl-vechicle-loan'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <label> List all auto loans.</label>

                                                        <table id='CPMVLInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Vehicle</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["CPMvehicle_loan_records"])) {
                                                                    $childs = json_decode($answer["CPMvehicle_loan_records"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->CPMVLVehicle}}</td>
                                                                                <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                    <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="CPMAddVLBtn add-dependent" data-toggle="modal" data-target="#CPMVLModal">Add loan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="CPMvehicleLoanDetails" class="hidden" name="answer[80][CPMvehicle_loan_records]">{{(!empty($answer["CPMvehicle_loan_records"]))? $answer["CPMvehicle_loan_records"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="CPMVLModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD VEHICLE</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->
                                                        <!-- first step starts --> 
                                                        <div class=" setup-content" id="step-1">
                                                            <div class=" section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For which vehicle?') }}
                                                                    {{ Form::input('text','CPM-vl-vehicle',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMVLVehicle', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'make/model or nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Origination date of the loan?') }}
                                                                    {{ Form::input('text','CPM-vl-loan-originate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker CPMVLOriginateLoan', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Term of loan (in months)?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','CPM-vl-loan-term',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMVLTermLoan ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'  ']) }}
                                                                    </div> 
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','CPM-vl-interest-rate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMVLInterestRate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the monthly payment?') }}
                                                                    <div class="input-group">
                                                                       <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPM-vl-monthly-payment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMVLMonthlyPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the remaining balance?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPM-vl-remaining-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMVLRemainingBalance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button id="CPMAddVLBtnnew" class='CPMAddVLBtn finishBtn pull-right' type="button">
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- second step finsih -->
                                                <!-- steps form finsih -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var CPMVLDetails = [];
        if ($('#CPMvehicleLoanDetails').val() != '') {
            CPMVLDetails = JSON.parse($('#CPMvehicleLoanDetails').val());
        }
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        $('#CPMVLModal').modal('hide');
                        CPMVLDetails[$(this).attr('data-index')] = {
                            CPMVLVehicle: $('#CPMVLModal .CPMVLVehicle').val(),
                            CPMVLOriginateLoan: $('#CPMVLModal .CPMVLOriginateLoan').val(),
                            CPMVLTermLoan: $('#CPMVLModal .CPMVLTermLoan').val(),
                            CPMVLInterestRate: $('#CPMVLModal .CPMVLInterestRate').val(),
                            CPMVLMonthlyPayment: $('#CPMVLModal .CPMVLMonthlyPayment').val(),
                            CPMVLRemainingBalance: $('#CPMVLModal .CPMVLRemainingBalance').val()
                        };
                        $('#CPMVLInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#CPMVLModal .CPMVLVehicle').val() + '</td>  <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#CPMVLInfo tbody').append('<tr data-index=' + CPMVLDetails.length + '><td>' + $('#CPMVLModal .CPMVLVehicle').val() + '</td>   <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        CPMVLDetails.push({
                            CPMVLVehicle: $('#CPMVLModal .CPMVLVehicle').val(),
                            CPMVLOriginateLoan: $('#CPMVLModal .CPMVLOriginateLoan').val(),
                            CPMVLTermLoan: $('#CPMVLModal .CPMVLTermLoan').val(),
                            CPMVLInterestRate: $('#CPMVLModal .CPMVLInterestRate').val(),
                            CPMVLMonthlyPayment: $('#CPMVLModal .CPMVLMonthlyPayment').val(),
                            CPMVLRemainingBalance: $('#CPMVLModal .CPMVLRemainingBalance').val()
                        });
                        $('#CPMVLModal').modal('hide');
                    }
                    $('#CPMvehicleLoanDetails').html(JSON.stringify(CPMVLDetails));
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        //        $('input[type=radio]').prop('checked', false);

        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('change', '.CPMVLVechicleLoan', function () {
            if ($(this).val() === 'yes') {
                $('.CPMVLDetailsTable').show();
            } else {
                $('.CPMVLDetailsTable').hide();
                CPMVLDetails = [];
                $('#CPMvehicleLoanDetails').html(JSON.stringify(CPMVLDetails));
            }
        });


        $(document).on('click', '#CPMVLInfo .fa-pencil', function () {
            $('#CPMVLModal').modal();
            $('#CPMVLModal #step-1').show();
            $('#CPMVLModal #step-2, #CPMVLModal #step-3').css('display', 'none');
            $('#CPMVLModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            CPMVLDetailsList = CPMVLDetails[index];
            $('#CPMVLModal .CPMVLVehicle').val(CPMVLDetailsList.CPMVLVehicle);
            $('#CPMVLModal .CPMVLOriginateLoan').val(CPMVLDetailsList.CPMVLOriginateLoan);
            $('#CPMVLModal .CPMVLTermLoan').val(CPMVLDetailsList.CPMVLTermLoan);
            $('#CPMVLModal .CPMVLInterestRate').val(CPMVLDetailsList.CPMVLInterestRate);
            $('#CPMVLModal .CPMVLMonthlyPayment').val(CPMVLDetailsList.CPMVLMonthlyPayment);
            $('#CPMVLModal .CPMVLRemainingBalance').val(CPMVLDetailsList.CPMVLRemainingBalance);
            $('#CPMAddVLBtnnew').addClass('edit-form');
            $('#CPMAddVLBtnnew').attr('data-index', index);
        });

        $(document).on('click', '.CPMAddVLBtn', function () {
            $('#CPMAddVLBtnnew').removeClass('edit-form');
            $('#CPMVLModal input[type="text"], input[type="number"]').val('');
            $('#CPMVLModal select').val('').trigger('change');
            $('#CPMVLModal .error-alert').remove();
            $('#CPMVLModal #step-1').show();
            $('#CPMVLModal #step-2').hide();
            $('#CPMVLModal').find('.error-alert').remove();
        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);


            $('.swal-button--danger').click(function () {

                $(this).closest('tr').remove();
                CPMVLDetails.splice(index_id, 1);
                $("#CPMVLInfo tbody").empty();
                if (CPMVLDetails.length != 0) {
                    var tr = '';
                    $.each(CPMVLDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.CPMVLVehicle + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#CPMVLInfo tbody").html(tr);
                }
                $('#CPMvehicleLoanDetails').html(JSON.stringify(CPMVLDetails));
                return false;
            });
        });



    });

</script>
@stop