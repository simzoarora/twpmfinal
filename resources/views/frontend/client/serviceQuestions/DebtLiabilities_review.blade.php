@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3> 
                    <section class="sections">
                        <div class="col-sm-12 ">
                            <div class="col-md-4 col-xs-11 section-left">
                                <h6>Liabilities</h6>
                                <h2>Let's talk about your liabilities</h2>
                                <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply <a href="#" class="contact-modal-show"> contact us</a> </p>
                            </div>
                            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>Mortgage</h4>
                                    </div> 
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,34,199])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(34, $subTopicInfo)) {
                                                echo $subTopicInfo[34];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>Home Equity Line of credit</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,35,200])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(35, $subTopicInfo)) {
                                                echo $subTopicInfo[35];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>vehicle loans</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,36,201])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(36, $subTopicInfo)) {
                                                echo $subTopicInfo[36];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>Credit Cards</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,37,204])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(37, $subTopicInfo)) {
                                                echo $subTopicInfo[37];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>Student Loans </h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,38,202])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(38, $subTopicInfo)) {
                                                echo $subTopicInfo[38];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8"> 
                                        <h4>Other debts (non-business)</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a  href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,802,39,203])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(39, $subTopicInfo)) {
                                                echo $subTopicInfo[39];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="custon-continue-button">
                                    <a  href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Continue</a>
                                </div>
                                <div class="custon-return">
                                    <a class="" href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                                </div>
                            </div>

                    </section>
                    <!--{{ Form::close() }}--> 
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
    $(document).ready(function () {
        $("select").selectBoxIt();
    });
</script>
<script>
    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";
</script>
@stop