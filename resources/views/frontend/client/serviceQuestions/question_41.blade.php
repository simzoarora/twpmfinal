@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',203) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Tell us about all your real estate assets.</h2>
                                    <p>This helps us understand your complete financial picture, so we can provide the best guidance.
                                        if you don't have any real estate holding ,you can enter 0 or just leave blank.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[41]','Tell us about all your real estate assets.') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Your home value') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon ">$</span> 
                                            {{ Form::input('number','answer[41][home_value]',(!empty($answer) && array_key_exists('home_value',$answer)) ? $answer['home_value']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 stock-width ', 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Total of mortgages') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon ">$</span> 
                                            {{ Form::input('number','answer[41][total_of_mortgages]',(!empty($answer) && array_key_exists('total_of_mortgages',$answer)) ? $answer['total_of_mortgages']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 stock-width', 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'If you own rental properties, enter the net values of properties(value less debt)') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon ">$</span> 
                                            {{ Form::input('number','answer[41][net_values_of_properties]',(!empty($answer) && array_key_exists('net_values_of_properties',$answer)) ? $answer['net_values_of_properties']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 stock-width', 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'if you own rental properties, what is your annual net rental income') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon ">$</span> 
                                            {{ Form::input('number','answer[41][annual_net_rental_income]',(!empty($answer) && array_key_exists('annual_net_rental_income',$answer)) ? $answer['annual_net_rental_income'] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 stock-width', 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    <!--<a id="returnLater" class="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });

        addRemoveHref();
    });
</script>
@stop