@extends('frontend.layouts.client')
@section('title')
@stop
@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',803) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6> Investment statements</h6>
                                    <h2>Upload your recent financial statements.</h2>
                                    <p style="margin-bottom:0;">Please gather and upload all of your most recent financial statements. This may include : </p>
                                    <ul style="font-size: 13px; padding-left: 25px;">
                                        <li>Investment Accounts</li>
                                        <li>IRA Accounts   </li>
                                        <li>401(k), SEP, SRA Accounts, etc. </li>
                                        <li>529 Accounts</li>
                                        <li>Deferred Compensation Accounts</li>
                                        <li>Annuities</li>
                                        <li>Employee Stock Options </li>
                                        <li> Any Other Investment statements</li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    <div class='add-file'>
                                        <table style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <td class="col-xs-4 col-sm-4 paddingLeft0">Name</td>
                                                    <td class="col-xs-5 col-sm-4 paddingLeft0">Document Type</td>
                                                    <td  class="col-xs-3 col-sm-4 paddingLeft0" style="text-align:right; padding-right: 23px;">Actions</td>
                                                </tr>
                                            </thead>
                                            <tbody class="add-input">
                                                <?php
                                                if (!empty($answer) && array_key_exists('files', $answer)) {
                                                    foreach ($answer["files"] as $key => $file) {
                                                        ?>
                                                        <tr  class="fileUploadRow appendrow col-xs-12 paddingLeft0" style="border-bottom:1px solid #bbb3b3"> 
                                                            <td class="col-xs-4 col-sm-4 paddingLeft0"> 
                                                                <input name="answer[205][files][{{$key}}][file_name]" class="save" readonly="readonly" type="text" value='<?php if (array_key_exists('file_name', $file)) echo $file["file_name"]; ?>'> 
                                                                <input name="answer[205][files][{{$key}}][file_path]" type="hidden" value='<?php if (array_key_exists('file_path', $file)) echo $file["file_path"]; ?>'>
                                                            </td>  
                                                            <td class="col-xs-5 col-sm-4 paddingLeft0"> 
                                                                <input type="text" class="document-selected" value="<?php if (array_key_exists('file_type', $file)) echo $file["file_type"]; ?>">
                                                                <i class="fa fa-angle-down" aria-hidden="true" style="display: none; position: absolute; top: 5px; left: 132px;  z-index: 11; font-size: 16px !important; text-transform: capitalize;">
                                                                </i> 
                                                                <select style="position: absolute;" style="border:0; background-color: #fff;" class="hide status document-type valid" name="answer[205][files][{{$key}}][file_type]" required="" aria-invalid="false"> 
                                                                    <option selected=="selected"  disabled="" value="">select</option> 

                                                                    <option value="401(k)" <?php if (array_key_exists('file_type', $file) && $file["file_type"] == "401(k)") echo "selected"; ?> >401(k)</option>
                                                                    <option value="SEP" <?php if (array_key_exists('file_type', $file) && $file["file_type"] == "SEP") echo "selected"; ?>>SEP</option>
                                                                    <option value="SRA Account" <?php if (array_key_exists('file_type', $file) && $file["file_type"] == "SRA Account") echo "selected"; ?>>SRA Account</option>
                                                                </select>
                                                            </td>
                                                            <td class="col-xs-3 col-sm-4">
                                                                <span class="pull-right"> 
                                                                    <i style="display:none;" class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i> 
                                                                    <i title="Edit" class="fa fa-pencil editUploadRow" aria-hidden="true"></i> 
                                                                    <i  class="fa fa-trash deleteUploadRow" aria-hidden="true"></i> 
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                } else {
                                                    ?>
                                                <input data-attr="0"  name="answer[205][file_name]" readonly="readonly" type="hidden" value=''>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <label class="radio-custom-label"> 
                                        {{ Form::input('hidden','questionName[205]','Upload your executive compensation documents.') }}
                                        <!--<span class="radio-icon"></span>-->
                                    </label>
                                    <div class="col-sm-12 save-file-section paddingLeft0" style="margin-top:0">
                                        <div class="col-sm-6 add-file-btn mt-20">
                                            <label for="files" class="btn btn-primary open_button" style="background:none;">Add File</label>
                                            <label for="files" class="btn btn-primary uploaderBtn no-action" style="background:none; display: none">Add File</label>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> 
@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script> 

<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $('.fileNameSaver').hide();

    var executorList = [];
    executorList[$(this).attr('data-index')] = {
        fileNameSaver: $('.file-uploader .fileNameSaver').val()
    };

    var trcount = 0;
    
    $(".open_button").on('click', function () {
       
        $.FileDialog({multiple: false})
                .on('files.bs.filedialog', function (ev) {
                    var nooftr = $('.fileUploadRow').length,
                            files = ev.files,
                            formData = new FormData(),
                            i = 0;
                    files.forEach(function (f) {
                        $('.add-file table tbody').append('<tr data-attr= " ' + nooftr + ' " class="fileUploadRow appendrow col-xs-12 paddingLeft0" style="border-bottom:0"><td  class="col-xs-4 col-sm-4 paddingLeft0 pull-left">  \n\
        <input name="answer[205][files][' + nooftr + '][file_name]" readonly="readonly" type="text" class="save" value=' + $('.file-uploader .fileNameSaver').val() + '><input name="answer[205][files][' + nooftr + '][file_path]" type="hidden" class="save"></td>  \n\
        <td class="col-xs-5 col-sm-4 paddingLeft0 pull-left"><input type="text" class="document-selected hide" value=""> <i class="fa fa-angle-down selectAngle" aria-hidden="true" style="position: absolute; top: 15px; left: 132px;"></i> \n\
         <select style="position: absolute;" style="border:0;" class="status document-type select-value valid" name="answer[205][files][' + nooftr + '][file_type]" required="true" aria-invalid="false"> <option selected value="0">select</option> <option value="401(k)">401(k)</option> <option value="SEP">SEP</option> <option value="SRA Account">SRA Account</option> </select></td>\n\
        <td class="col-xs-3 col-sm-4 pull-left"><span class="pull-right"> <i class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i>    <i title="Edit" class="fa fa-pencil editUploadRow" style="display:none;" aria-hidden="true"></i>  \n\
        <i title="Delete" class="fa fa-trash deleteUploadRow" style="display:none;" aria-hidden="true"></i> </span></td></tr>');
            $('.fileSave').closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','none');
                        formData.append("file", f, f.name);
                        trcount++;
                        i++;
                    });
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) { 
                            $('input[name="answer[205][files][' + nooftr + '][file_path]"]').val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }
                    });
                })
                .on('cancel.bs.filedialog', function (ev) {
                });
    });
    // ----------------- File uploader js finish
    $(document).on('click', '.deleteUploadRow', function () {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('tr').remove();

                        swal("Your file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });

    });
    // for id increment finsih

    $(document).on('click', '.bfd-ok', function () {
        $('.file-length-error').remove();
        $(this).closest('.appendrow').find('td:last-child .fa-trash').hide();
        $(this).closest('.appendrow').find('td:last-child .fileSave').show();
        $('.file-uploader .fileNameSaver').val(executorList.fileNameSaver);
        
        $('.fileUploadRow').addClass('fileNotSaved');
        if ($('.bfd-files').find('.bfd-info').length > 0) {
            $('.uploaderBtn').show();
            $('.open_button').hide();
        } else {
            $('.uploaderBtn').hide();
            $('.open_button').show();
        }

        $(".fileNameSaver").validate({
            ignore: ".ignore, :hidden",
            errorPlacement: function errorPlacement(error, element) {
            }
        });
       
    });
    // for id increment finsih

    $(document).each(function () {
        $(this).on('click', '.fileSave', function () {
            var selectdropdown = $(this).closest('tr').find('.select-value');
            if ($(selectdropdown).val() === '0') {
                $(this).closest('.fileUploadRow ').find('.select-value').parent().append('<div class="error error-alert">Please select value</div>');
                $(this).closest('.fileUploadRow td:nth-child(3n)').find('.editUploadRow, .deleteUploadRow').hide();
                $(this).closest('.fileUploadRow td:nth-child(3n)').find('.fileSave').show();
                return false;
            } else {
                $(this).closest('.fileUploadRow').find('.error-alert').remove();
                $(this).hide();
                $(this).siblings('.editUploadRow, .deleteUploadRow').show().css('opacity', '.7');
                $('.uploaderBtn').hide();
                $('.open_button').show();
                $(this).closest('tr').find(".document-selected, input[type=text], select").removeClass('hide edit').attr('readonly', 'readonly').addClass('save');
                $(this).closest('tr').find(".document-type").addClass('hide');
                $(this).closest('tr').find('.fa-angle-down').hide();
                $(this).closest('.fileUploadRow').css('border-bottom', '1px solid #bbb3b3');
                $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','auto');
            }
        });
    });

    $(document).on('click', '.editUploadRow', function () {
        $(this).closest('tr').find(".document-selected ").addClass('hide');
        $(this).closest('tr').find(".document-type").removeClass('hide');
        $(this).closest('tr').find('.fa-trash, .editUploadRow').hide();
        $(this).closest('tr').find('.fileSave').show();
        $(this).closest('tr').find("input[type=text], select").addClass('edit').removeAttr("readonly").removeClass('save');
        $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','none');
        $(this).closest('tr').find('.fa-angle-down').show();
    });

    // file uploader js starts 
    $(document).on('click', '.open_button', function () {
        $('.bfd-dropfield-inner').attr('id', 'flUpload');
        $('.bfd-files').append("<span class='inner-left' style='position:relative;'><span class='col-xs-12 paddingLeft0 paddingRight0'> <input class='form-control fileNameSaver hide' id='newName' required='required' placeholder='Enter File Name'></span><label id='lblSize' class='file-size' ></label></span>");
        if ($('.fileNameSaver').val() == "") {
            $('.bfd-ok').prop("disabled", "true");
        }
        $('.file-size').text('');
    });
    $('.editUploadRow, .deleteUploadRow').show();
    $('.fileSave').hide();


    // =----------- get file size while uploading file
    $(document).on('change', 'input[type=file]', function () {
        
        if ($('.bfd-files').find('.bfd-info').length == 1) {
            $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
            $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
// $('.bfd-ok').prop("disabled", "true");
        }

        $('.fileNameSaver').removeClass('hide');
        var iSize = ($('input[type=file]')[0].files[0].size / 1024);
        if (iSize / 1024 > 1)
        {
            if (((iSize / 1024) / 1024) > 1)
            {
                iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
                $("#lblSize").html(iSize + "Gb");
            } else
            {
                iSize = (Math.round((iSize / 1024) * 100) / 100)
                $("#lblSize").html(iSize + "Mb");
            }
        } else
        {
            iSize = (Math.round(iSize * 100) / 100)
            $("#lblSize").html(iSize + "kb");
        }



    });
    // ------------ get file size while uploading file finish 
});

$(document).on('keyup change', '.bfd-files input', function () {
    $(this).closest('.inner-left').find('.error-alert').remove();
    $('.bfd-ok').removeAttr('disabled');

    var inputs = $(".save");

    for (var i = 0; i < inputs.length; i++) {   

        if ($(this).val() && $('#newName').val() == $(inputs[i]).val()) {
            $('.bfd-ok').prop("disabled", "true");
            $(this).closest('.inner-left').append('<label class="error error-alert">This name already exists.</label>');
            return;


        } else if (!$(this).val()) {
            $(this).closest('.inner-left').find('.error-alert').remove();
            $(this).closest('.inner-left').append('<label class="error error-alert">This field is required.</label>');
            $('.bfd-ok').prop("disabled", "true");
        }
    }

});
$(document).on('keyup change', '.edit', function () {
    $(this).closest('td').find('.parent-error-alert').remove();
    var inputs = $(".save");
    for (var i = 0; i < inputs.length; i++) {
        if ($(this).val() == $(inputs[i]).val()) {
            $(this).addClass('col-sm-5');
            $(this).closest('td').removeClass('col-sm-4').addClass('col-sm-5');
            $(this).closest('td').append('<div class="col-sm-7 parent-error-alert" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">name already exist</label></div>');
            $(this).closest('tr').find('#saveFile').css('pointer-events', 'none');
            $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'none');
            return;
        } else if (!$(this).val()) {
            $(this).closest('td').append('<div class="col-sm-7" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">name already exist</label></div> ');
        } else {
            $(this).closest('tr').find('#saveFile').css('pointer-events', 'all');
            $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'all');
        }
    }

});

$(document).on('change', '.document-type', function () {
    var selectedVal = $(this).closest('.fileUploadRow ').find('.document-type  option:selected ');
    $(this).closest('.fileUploadRow ').find(".document-selected").val($(selectedVal).text());
    if ($('.document-type:not(.hide) option:selected').val() == 0)
    {
        $('.document-type:not(.hide)').parent().append('<div class="error error-alert">Please select value</div>');
    } else {
        $('div.error-alert').remove();
    }
});

</script>
<style type="text/css">
    .file-size{float: left;  width: 100%; text-align: right; color: #ABB1C9; font-size: 12px; margin: 0; font-weight: normal}
    .modal-header{border-radius: 5px 5px 0 0;}
    @media (max-width: 768px){
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow{
            display: inherit;
        }
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow td .error-alert {
            position: absolute;
            top: 29px;
            left: 0;
            padding: 4px 10px !important;
            width: 150px !important;
        }
        .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{
            background: #fff;
            margin-right: 8px;
            padding-left: 8px;
            left: auto !important;
            right: 0;
            top: 13px !important;
        }
    }

    .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{ top: 13px !important;}
    label.error-alert{
        border-radius: 0;
        width:100%;
        position: absolute;
        bottom: -22px;
        left: 0;
        font-size: 13px;
        padding: 7px;
        font-weight: 400;
        padding-left: 15px;
        background-color: #f2dede;
        border-color: #ebccd1;
        color: #a94442;}
</style>
<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script> 
@stop
