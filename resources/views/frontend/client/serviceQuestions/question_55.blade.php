<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Bypass Trust</h2>
        <p>The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[55]','Bypass Trust') }}
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Does your estate plan include provision for a by-pass trust?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[55][provision of by-pass trust]', 'yes',(!empty($answer) && array_key_exists('provision of by-pass trust',$answer)) ? (($answer['provision of by-pass trust']=="yes")  ? true : false):false,['class'=>'by-pass-trust' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[55][provision of by-pass trust]', 'no',(!empty($answer) && array_key_exists('provision of by-pass trust',$answer)) ? (($answer['provision of by-pass trust']=="no")  ? true : false):false,['class'=>'by-pass-trust' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>