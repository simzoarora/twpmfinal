@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
    .add-mortgage {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .mortgageInfo{
        position: relative;
    }
</style>
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">

                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0;">Assets</p>
                                    <h2>Mortgages on non-rental property</h2>
                                    <p>Please answer the following questions, so we can get the most accurate picture of your finances. If you have questions, feel free to <a href="#">contact us</a>.</p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','data[78][questionName]','Mortgages on non-rental property') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have any mortgages on non-rental property?</label>
                                        <label class="radio-custom-label">
                                            <input class="mortgage-on-non-rental" required="required" name="data[78][answer][mortgages on non-rental property]" type="radio" value="yes" aria-required="true">Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="mortgage-on-non-rental" required="required" name="data[78][answer][mortgages on non-rental property]" type="radio" value="no" aria-required="true">No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-6 inner-left retirement-table "  style="display:none;">
                                        {{ Form::label('label', 'List all non-rental mortgages.') }}
                                        <table id='mortgageInfo'>
                                            <thead>
                                                <tr>
                                                    <td>Property address</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-mortgage" data-toggle="modal" data-target="#mortgageModal">Add mortgage</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

<!---------Mortgage modal--------->
<div id="mortgageModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;  border: 0;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: 0;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD MORTGAGE</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[78][questionName]','ADD a retirement account') }}
                        <div class="col-sm-12  validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','For which property?') }}
                                {{ Form::input('text','data[78][answer][For which property]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control street-address', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What was the original mortgage amount?') }}
                                {{ Form::input('text','data[78][answer][original amount]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','When did the mortgage begin?') }}
                                {{ Form::input('text','data[78][answer][mortgage begin date]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-datetimepicker', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What is the current mortgage balance?') }}
                                {{ Form::input('text','data[78][answer][mortgage balance]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='mortgageForm'  class="modal-button"type="button" style="border-radius: 3px;">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------Mortgage modal end--------->

@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

    $(document).ready(function () {
        $("select").selectBoxIt();
        var mortgageList = [];
        $('#mortgageForm').on('click', function () {
            $('#mortgageModal .error-alert').remove();
            if ($('#mortgageModal .street-address').val()) {

                if ($(this).hasClass('edit-form')) {
                    mortgageList[$(this).attr('data-index')] = {
                        address: $('#mortgageModal .street-address').val()
                    }
                    $('#mortgageInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#mortgageModal .street-address').val() + '</td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> </td> <td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                } else {
                    $('#mortgageInfo tbody').append('<tr data-index=' + mortgageList.length + '><td>' + $('#mortgageModal .street-address').val() + '</td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> </td> <td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                    mortgageList.push({
                        address: $('#mortgageModal .street-address').val()
                    });
                }
                $('#mortgageModal').modal('hide');
                $('#mortgageModal .street-address').val('');
            } else {

                $('#mortgageModal input[type="text"], #mortgageModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });
        $('#mortgageModal input, #mortgageModal select').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val()) {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $('#mortgageInfo').on('click', '.fa-pencil', function () {
            $('#mortgageModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = mortgageList[index];
            $('#mortgageModal .street-address').val(insuranceDetails.address);
            $('#mortgageForm').addClass('edit-form').attr('data-index', index);
            $('#mortgageModal .error-alert').remove();
        });
        $(document).on('click', '.add-mortgage', function () {
            $('#mortgageForm').removeClass('edit-form');
            $('#mortgageModal .error-alert').remove();
            $('#mortgageModal input').val('');
        });
        $('#mortgageInfo').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();

        });

        $(document).on('click', '.mortgage-on-non-rental', function () {
            if ($(this).val() == 'yes') {
                $('.retirement-table').show();
            } else {
                $('.retirement-table').hide();
            }
        });
    });

</script>
@stop