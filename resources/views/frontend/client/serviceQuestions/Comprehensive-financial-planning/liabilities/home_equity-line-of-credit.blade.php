@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
    .add-heloc {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .helocInfo{
        position: relative;
    }
</style>
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">

                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0;">Assets</p>
                                    <h2>Equity Lines of Credit(HELOC)</h2>
                                    <p>Text area for explanation of HELOC</p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','data[79][questionName]','Mortgages on non-rental property') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have any home equity lines of credit?</label>
                                        <label class="radio-custom-label">
                                            <input class="line-of-credit" required="required" name="data[79][answer][home equity lines of credit]" type="radio" value="yes" aria-required="true">Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="line-of-credit" required="required" name="data[79][answer][home equity lines of credit]" type="radio" value="no" aria-required="true">No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-6 inner-left heloc-table "  style="display:none;">
                                        {{ Form::label('label', 'List all home equity lines of credit.') }}
                                        <table id='helocInfo'>
                                            <thead>
                                                <tr>
                                                    <td>Property address</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-heloc" data-toggle="modal" data-target="#helocModal">Add mortgage</button>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop

<!---------heloc modal--------->
<div id="helocModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;  border: 0;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: 0;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD HELOC</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[79][questionName]','ADD a retirement account') }}
                        <div class="col-sm-12  validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','For which property?') }}
                                {{ Form::input('text','data[79][answer][For which property]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control heloc-street-address', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What was the original mortgage amount?') }}
                                {{ Form::input('text','data[79][answer][original amount]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control original-amount', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','When did the mortgage begin?') }}
                                {{ Form::input('text','data[79][answer][mortgage begin date]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-begin-date heloc-datetimepicker', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What is the current mortgage balance?') }}
                                {{ Form::input('text','data[79][answer][mortgage balance]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-balance', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='helocForm'  class="modal-button next-modal "type="button" style="border-radius: 3px;"  data-toggle="modal" data-target="#nextModal">
                                    next
                                </button>
                            </div>
                            <!--                            <div class="col-sm-12 inner-left">
                                                            <button   class="modal-button next-modal "type="button" style="border-radius: 3px;" data-toggle="modal" data-target="#nextModal" >
                                                              next
                                                            </button>
                                                        </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------heloc modal end--------->
<!---------heloc modal--------->
<div id="nextModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;  border: 0;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: 0;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD HELOC</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[79][questionName]','ADD a retirement account') }}
                        <div class="col-sm-12  validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What is the interest rate?') }}
                                {{ Form::input('text','data[79][answer][interest rate]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control heloc-interest-rate', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Upon what is the interest rate based?') }}
                                <select style="position: absolute;" class="interest-based" name="data[79][answer][state]"  required><option selected disabled value="">SELECT</option><option value="1">10-Year</option><option value="2">Treasury</option><option value="3">PRIME</option><option value="4">LIBOR</option><option value="5">Unsure</option><option value="6">Other</option></select>
                            </div>

                            <div class="col-sm-12 inner-left interest-rate-description" style="display:none;">
                                {{ Form::label('label','Other') }}
                                {{ Form::input('text','data[79][answer][other description]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'please describe', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','When did the loan originate') }}
                                {{ Form::input('text','data[79][answer][When did the loan originate]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control heloc-datetimepicker', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','What is the (in years)?') }}
                                {{ Form::input('text','data[79][answer][term of loan]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control term-of-loan', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='closeModal'  class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------heloc modal end--------->

@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

    $(document).ready(function () {
        $("select").selectBoxIt();
        var helocList = [];
        $('#helocForm').on('click', function () {
            $('#helocModal .error-alert').remove();
            if ($('#helocModal .heloc-street-address').val()) {

                if ($(this).hasClass('edit-form')) {
                    helocList[$(this).attr('data-index')] = {
                        address: $('#helocModal .heloc-street-address').val(),
                        amount: $('#helocModal .original-amount').val(),
                        mortgageBegin: $('#helocModal .mortgage-begin-date').val(),
                        mortgageBalance: $('#helocModal .mortgage-balance').val()
                    }
                    $('#helocInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#helocModal .heloc-street-address').val() + '</td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> </td> <td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                } else {
                    $('#helocInfo tbody').append('<tr data-index=' + helocList.length + '><td>' + $('#helocModal .heloc-street-address').val() + '</td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> </td> <td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                    helocList.push({
                        address: $('#helocModal .heloc-street-address').val(),
                        amount: $('#helocModal .original-amount').val(),
                        mortgageBegin: $('#helocModal .mortgage-begin-date').val(),
                        mortgageBalance: $('#helocModal .mortgage-balance').val()
                    });
                }
                $('#helocModal').modal('hide');
                $('#helocModal .heloc-street-address').val('');
            } else {

                $('#helocModal input[type="text"], #helocModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });
        $('#helocModal input, #helocModal select').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val()) {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $('#helocInfo').on('click', '.fa-pencil', function () {
            $('#helocModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            helocDetails = helocList[index];
            $('#helocModal .heloc-street-address').val(helocDetails.address);
            $('#helocModal .original-amount').val(helocDetails.amount);
            $('#helocModal .mortgage-begin-date').val(helocDetails.mortgageBegin);
            $('#helocModal .mortgage-balance').val(helocDetails.mortgageBalance);
            $('#helocForm').addClass('edit-form').attr('data-index', index);
            $('#helocModal .error-alert').remove();
        });
        $(document).on('click', '.add-heloc', function () {
            $('#helocForm').removeClass('edit-form');
            $('#helocModal .error-alert').remove();
            $('#helocModal input').val('');
            $('#nextModal input').val('');
            $('#nextModal select').val('').trigger('change');
        });
        $('#helocInfo').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();

        });
    });


    $(document).on('click', '.next-modal', function () {
        $('#helocModal').modal('hide');

    });
    $(document).on('click', '#closeModal', function () {
        $('#nextModal').modal('hide');

    });
    $(document).on('change', '.interest-based', function () {
        if ($('.interest-based').val() == 6) {
            $('.interest-rate-description').show();
        } else {
            $('.interest-rate-description').hide();
            $('.interest-rate-description input').val('');
        }

    });

    $(document).on('click', '.line-of-credit', function () {
        if ($(this).val() == 'yes') {
            $('.heloc-table').show();
        } else {
            $('.heloc-table').hide();
        }
    });

</script>
@stop