@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',504) }}

                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Auto Loans</h2>
                                    <p>(Do not include lease information.)</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[8]','Do you have any vehicle loans?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any vehicle loans?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[8][semi-annual-vl-vechicle-loan]', 'yes',(!empty($answer) && array_key_exists('semi-annual-vl-vechicle-loan',$answer)) ? (($answer['semi-annual-vl-vechicle-loan']=="yes")  ? true : false):false,['class'=>'DebtReductionlVLVechicleLoan table-confirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[8][semi-annual-vl-vechicle-loan]', 'no',(!empty($answer) && array_key_exists('semi-annual-vl-vechicle-loan',$answer)) ? (($answer['semi-annual-vl-vechicle-loan']=="no")  ? true : false):false,['class'=>'DebtReductionlVLVechicleLoan table-confirmation', 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left DebtReductionlVLDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('semi-annual-vl-vechicle-loan', $answer) && ($answer['semi-annual-vl-vechicle-loan'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h6 style="
                                                            font-size: 15px;
                                                            "> List all auto loans.</h6>

                                                        <table id='DebtReductionlVLInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Vehicle</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["vehicle_loan_records"])) {
                                                                    $childs = json_decode($answer["vehicle_loan_records"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->DebtReductionlVLVehicle}}</td>
                                                                                <td class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="semiAnnualAddVLBtn add-dependent" data-toggle="modal" data-target="#DebtReductionlVLModal">Add Loan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="vehicleLoanDetails" class="hidden" name="answer[8][vehicle_loan_records]">{{(!empty($answer["vehicle_loan_records"]))? $answer["vehicle_loan_records"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="DebtReductionlVLModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD VEHICLE</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->
                                                        <form role="form">
                                                            <!-- first step starts -->
                                                            <div class=" setup-content" id="step-1">
                                                                <div class="section-right">
                                                                    <!-- first step content starts here -->
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'For which vehicle?') }}
                                                                        {{ Form::input('text','semi-annual-vl-vehicle',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DebtReductionlVLVehicle', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'make/model or nickname']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'Origination date of the loan?') }}
                                                                        {{ Form::input('text','semi-annual-vl-loan-originate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker DebtReductionlVLOriginateLoan', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'Term of loan (in months)?') }}
                                                                        {{ Form::input('number','semi-annual-vl-loan-term',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DebtReductionlVLTermLoan', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0 ]) }}
                                                                    </div>


                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the interest rate?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','semi-annual-vl-interest-rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DebtReductionlVLInterestRate', 'data-rule-regex'=>"false", 'required'=>true,'min'=>0 ]) }}
                                                                            <span class="input-group-addon" id="basic-addon1">%</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button class="nextBtn" type="button" >next
                                                                            <i class="fa fa-arrow-right"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- first step finsih -->

                                                            <!-- second step starts -->
                                                            <div class=" setup-content" id="step-2" style="display: none">
                                                                <div class="section-right">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the monthly payment?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                                                            {{ Form::input('number','semi-annual-vl-monthly-payment',null,['data-validation'=> '' , 'class'=>' borderLeft0 custom-validation form-control DebtReductionlVLMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','min'=>0]) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the remaining balance?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                                                            {{ Form::input('number','semi-annual-vl-remaining-balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DebtReductionlVLRemainingBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','min'=>0]) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <!--id='semiAnnualAddVLBtn'-->  
                                                                        <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                        <button id="semiAnnualAddVLBtnnew" class='semiAnnualAddVLBtn finishBtn pull-right' type="button">finish<i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- second step finsih -->
                                                            <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <!--<a class="returnLater"  href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var DebtReductionlVLDetails = [];
        if ($('#vehicleLoanDetails').val() != '') {
            DebtReductionlVLDetails = JSON.parse($('#vehicleLoanDetails').val());
        }
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        $('#DebtReductionlVLModal').modal('hide');
                        DebtReductionlVLDetails[$(this).attr('data-index')] = {
//                            DebtReductionlVLVechicleLoan: $('.DebtReductionlVLVechicleLoan:checked').val(),
                            DebtReductionlVLVehicle: $('#DebtReductionlVLModal .DebtReductionlVLVehicle').val(),
                            DebtReductionlVLOriginateLoan: $('#DebtReductionlVLModal .DebtReductionlVLOriginateLoan').val(),
                            DebtReductionlVLTermLoan: $('#DebtReductionlVLModal .DebtReductionlVLTermLoan').val(),
                            DebtReductionlVLInterestRate: $('#DebtReductionlVLModal .DebtReductionlVLInterestRate').val(),
                            DebtReductionlVLMonthlyPayment: $('#DebtReductionlVLModal .DebtReductionlVLMonthlyPayment').val(),
                            DebtReductionlVLRemainingBalance: $('#DebtReductionlVLModal .DebtReductionlVLRemainingBalance').val()
                        };
                        $('#DebtReductionlVLInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#DebtReductionlVLModal .DebtReductionlVLVehicle').val() + '</td>  <td  class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#DebtReductionlVLInfo tbody').append('<tr data-index=' + DebtReductionlVLDetails.length + '><td>' + $('#DebtReductionlVLModal .DebtReductionlVLVehicle').val() + '</td>   <td  class="right-align"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        DebtReductionlVLDetails.push({
                            DebtReductionlVLVechicleLoan: $('.DebtReductionlVLVechicleLoan:checked').val(),
                            DebtReductionlVLVehicle: $('#DebtReductionlVLModal .DebtReductionlVLVehicle').val(),
                            DebtReductionlVLOriginateLoan: $('#DebtReductionlVLModal .DebtReductionlVLOriginateLoan').val(),
                            DebtReductionlVLTermLoan: $('#DebtReductionlVLModal .DebtReductionlVLTermLoan').val(),
                            DebtReductionlVLInterestRate: $('#DebtReductionlVLModal .DebtReductionlVLInterestRate').val(),
                            DebtReductionlVLMonthlyPayment: $('#DebtReductionlVLModal .DebtReductionlVLMonthlyPayment').val(),
                            DebtReductionlVLRemainingBalance: $('#DebtReductionlVLModal .DebtReductionlVLRemainingBalance').val()
                        });
                        $('#DebtReductionlVLModal').modal('hide');
                    }
                    $('#vehicleLoanDetails').html(JSON.stringify(DebtReductionlVLDetails));
                }
            }
            return false;
        });

        $("select").selectBoxIt();
//        $('input[type=radio]').prop('checked', false);

        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });


        $(document).on('change', '.DebtReductionlVLVechicleLoan', function () {
            if ($(this).val() === 'yes') {
                $('.DebtReductionlVLDetailsTable').show();
            } else {
                $('.DebtReductionlVLDetailsTable').hide();
                DebtReductionlVLDetails = [];
                $('#vehicleLoanDetails').html(JSON.stringify(DebtReductionlVLDetails));
                $('.DebtReductionlVLDetailsTable tbody tr').remove();
            }
        });

        $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.semiAnnualexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('click', '.semiAnnualAddVLBtn', function () {
            $('#DebtReductionlVLModal #step-1').show();
            $('#DebtReductionlVLModal #step-2').hide();
            $('#DebtReductionlVLModal').find('.error-alert').remove();
            if ($('#DebtReductionlVLModal .DebtReductionlVLVehicle').val() && $('#DebtReductionlVLModal .DebtReductionlVLTermLoan').val()) {
                $('#DebtReductionlVLModal .DebtReductionlVLVehicle, #DebtReductionlVLModal .DebtReductionlVLTermLoan').val('');
                $('#DebtReductionlVLModal .DebtReductionlVLVechicleLoan').prop('checked', false);
//                $('#vehicleLoanDetails').html(JSON.stringify(DebtReductionlVLDetails));
            }
        });

        $(document).on('click', '#DebtReductionlVLInfo .fa-pencil', function () {
            $('#DebtReductionlVLModal').modal();
            $('#DebtReductionlVLModal #step-1').show();
            $('#DebtReductionlVLModal #step-2, #DebtReductionlVLModal #step-3').css('display', 'none');
            $('#DebtReductionlVLModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            DebtReductionlVLDetailsList = DebtReductionlVLDetails[index];
            $('.DebtReductionlVLVechicleLoan[value=' + DebtReductionlVLDetailsList.DebtReductionlVLVechicleLoan + ']').prop('checked', true);
            $('#DebtReductionlVLModal .DebtReductionlVLVehicle').val(DebtReductionlVLDetailsList.DebtReductionlVLVehicle);
            $('#DebtReductionlVLModal .DebtReductionlVLOriginateLoan').val(DebtReductionlVLDetailsList.DebtReductionlVLOriginateLoan);
            $('#DebtReductionlVLModal .DebtReductionlVLTermLoan').val(DebtReductionlVLDetailsList.DebtReductionlVLTermLoan);
            $('#DebtReductionlVLModal .DebtReductionlVLInterestRate').val(DebtReductionlVLDetailsList.DebtReductionlVLInterestRate);
            $('#DebtReductionlVLModal .DebtReductionlVLMonthlyPayment').val(DebtReductionlVLDetailsList.DebtReductionlVLMonthlyPayment);
            $('#DebtReductionlVLModal .DebtReductionlVLRemainingBalance').val(DebtReductionlVLDetailsList.DebtReductionlVLRemainingBalance);
            $('#semiAnnualAddVLBtnnew').addClass('edit-form');
            $('#semiAnnualAddVLBtnnew').attr('data-index', index);
        });

        $(document).on('click', '.semiAnnualAddVLBtn', function () {
            $('#semiAnnualAddVLBtnnew').removeClass('edit-form');
            $('#DebtReductionlVLModal input[type="text"], input[type="number"]').val('');
            $('#DebtReductionlVLModal select').val('').trigger('change');
            $('#DebtReductionlVLModal .error-alert').remove();
            $('#DebtReductionlVLModal #step-1').show();
            $('#DebtReductionlVLModal #step-2').hide();
        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                DebtReductionlVLDetails.splice(index_id, 1);
                $("#DebtReductionlVLInfo tbody").empty();
                if (DebtReductionlVLDetails.length != 0) {
                    var tr = '';
                    $.each(DebtReductionlVLDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.DebtReductionlVLVehicle + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#DebtReductionlVLInfo tbody").html(tr);
                }


                $('#vehicleLoanDetails').html(JSON.stringify(DebtReductionlVLDetails));
                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });

        $(document).on('click', 'semiAnnualAddVLBtn', function () { // <-- changes
            $('#DebtReductionlVLModal').find('.error-alert').remove();
        });
    });

</script>
@stop
