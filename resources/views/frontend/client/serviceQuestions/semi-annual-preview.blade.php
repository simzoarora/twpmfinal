<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>Main Menu</h6>
        <h2>Select a section to work on</h2> 
        <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply <a href="#"> contact us</a> </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        <div class="categories">
            <div class="headings col-sm-8"> 
                <h4>Confirm Income/Tax Profile</h4>
            </div> 
            <div class="status col-sm-4">
                <a  href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,801,23])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(801, $topicsInfo)) {
                        echo $topicsInfo[801];
                    }
                    ?></a> 
            </div>
        </div>

        <div class="categories">
            <div class="headings col-sm-8"> 
                <h4>Debts/Liabilities</h4>
            </div>
            <div class="status col-sm-4">
                <a topic-id='802' href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(802, $topicsInfo)) {
                        echo $topicsInfo[802];
                    }
                    ?></a>
            </div>
        </div>

        <div class="categories">
            <div class="headings col-sm-8"> 
                <h4>Investment statements</h4>
            </div>
            <div class="status col-sm-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,803,205])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(803, $topicsInfo)) {
                        echo $topicsInfo[803];
                    }
                    ?></a>
            </div>
        </div>
        <div class="custon-continue-button">
                    <a  href="<?php
            if ($topicsInfo['serviceCompleted']) {
                echo route('frontend.client.selectedService', [config('constant.subdomain'), $currentService->id]);
            } else {
                echo'javascript:;';
            }
            ?>" class="<?php echo (!$topicsInfo['serviceCompleted']) ? 'toastrForService' : '' ?>">Continue</a>
        </div>
        <div class="custon-return">
            <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>
        </div>
    </div>
</div>

