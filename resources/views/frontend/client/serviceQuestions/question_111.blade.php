<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">INVESTMENT POLICIES</p>
        <h2>Investment Policy Statements</h2>
        <p>Descriptive/explanatory statement.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have an Investment Policy Statement(IPS)?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[111][answer][Investment Policy Statement]', 'yes',false,[ 'class' => 'radio-value investmentPolicyChecked']) }} Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[111][answer][Investment Policy Statement]', 'no',false,[ 'class' => 'radio-value investmentPolicyChecked']) }} No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-12 inner-left" id="uploadInvestmentFile" style="display: none;">
            <label for="label">Upload your Investment Policy Statement.</label>
            <div>
                <label for="investment-policy-statement" class="btn upload-statment upload-account-statment">Upload File</label>
                <input id="investment-policy-statement" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.investmentPolicyChecked').on('change', function () {
            if ($(this).val() == 'yes') {
                $('#uploadInvestmentFile').show();
            } else {
                $('#uploadInvestmentFile').hide();
            }
        });
    });
</script>