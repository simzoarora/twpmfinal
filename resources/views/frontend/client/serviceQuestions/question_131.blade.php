@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',13) }}
                    {{ Form::input('hidden','subTopicId',1) }}
                    {{ Form::input('hidden','redirectPageName','spouse-employment-income-retirement-preview') }}

                    <h3></h3>                
                    <fieldset>      
                        <section class="sections">


                            <div class="col-sm-12 "> 
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{$spouse_name}} INCOME & RETIREMENT PLANS</h6>
                                    <h2>Employment & Income</h2>
                                    <p>We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[131]','Employment & Income') }}

                                    <div class="col-sm-6 inner-left partner-age" >
                                        {{ Form::label('label', 'Job title or description') }}
                                        {{ Form::input('text','answer[131][job_title]',(!empty($answer) && array_key_exists('job_title',$answer)) ? $answer['job_title'] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control job-title', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                    </div>
                                    <div class="col-sm-6 inner-left partner-age" >
                                        {{ Form::label('label', 'Annual income, including bonus') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon">$</span>
                                            {{ Form::input('number','answer[131][employ_annual_income]',(!empty($answer) && array_key_exists('employ_annual_income',$answer)) ? $answer['employ_annual_income'] : null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0  custom-validation form-control job-title', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section> 
                    </fieldset>  
                    {{ Form::close() }}
                </div> 
            </div> 
        </div> 
    </div> 
</div> 
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,13,'spouse-employment-income-retirement-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
    });
</script>

@stop



