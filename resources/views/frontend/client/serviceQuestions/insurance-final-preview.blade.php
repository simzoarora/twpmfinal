@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    {{ Form::input('hidden','serviceId',$currentService->id) }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                     <h6>{{session::get('loggedInUserName')}} INSURANCE</h6>
                                    <h2>Let's talk about insurance.</h2>
                                    <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply <a href="#" class="contact-modal-show">contact us</a>.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>life insurance</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,6,61])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(6, $subTopicInfo)) {
                                                    echo $subTopicInfo[6];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>disability insurance </h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,7,64])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(7, $subTopicInfo)) {
                                                    echo $subTopicInfo[7];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>health insurance </h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,8,68])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(8, $subTopicInfo)) {
                                                    echo $subTopicInfo[8];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>long-term insurance </h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,9,70])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(9, $subTopicInfo)) {
                                                    echo $subTopicInfo[9];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>auto insurance</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,10,69])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(10, $subTopicInfo)) {
                                                    echo $subTopicInfo[10];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>other insurance </h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,6,11,71])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(11, $subTopicInfo)) {
                                                    echo $subTopicInfo[11];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="custon-continue-button">
                                        <a  href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Continue</a>
                                    </div>
                                     <div class="custon-return">
                                        <a href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset> 
                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>

@stop 