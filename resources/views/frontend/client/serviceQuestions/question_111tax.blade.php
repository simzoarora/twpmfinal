<div class="col-sm-12">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> TAX CERTIFICATION </p>
        <h4> Tax Classification</h4>
        <p>Descriptive/explanatory statements </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        <div class="col-sm-12">
            <h5 class="right-head"> What is the tax classification? </h5>
            <i class="i-head">Please select one</i>
        </div>

        <div class="acc-categories">
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name tax_type">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Individual/Sole Proprietor</p>

                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>C-Corp</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>S-Corp</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--row2-->
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name tax_type ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Partnership</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>LLC C-Corp</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>LLC S-Corp</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--row3-->
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name tax_type ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>LLC Partnership</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Trust/Estate</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name tax_type" id="other_tax">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Other</p>
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12 inner-left other-box" style="display: none;">
            {{ Form::label('label', 'Other') }}
            {{ Form::input('text','data[1111][answer][other][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'describe']) }}
        </div>
    </div>
</div>



