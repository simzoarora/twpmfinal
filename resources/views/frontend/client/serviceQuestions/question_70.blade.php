<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    .add-retirement-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .retirementInfo{
        position: relative;
    }
    .filename span{font-size: 14px !important}
</style>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',6) }}
                    {{ Form::input('hidden','subTopicId',9) }}
                    {{ Form::input('hidden','redirectPageName','insurance-final-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}}</h6>
                                    <h2>Long-term care insurance</h2>
                                    <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <p class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[70]','Long-term care insurance.') }}

                                    <div class="col-sm-6 inner-left ">

                                        <label for="label">Do you have Long-Term Care Insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="long-term-care" required="required" name="answer[70][long_term_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('long_term_insurance', $answer) && ($answer['long_term_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="long-term-care" required="required" name="answer[70][long_term_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('long_term_insurance', $answer) && ($answer['long_term_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}}</h6>
                                    <h2>Long-term care insurance</h2>
                                    <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <p class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[701]','Long-term care insurance.') }}

                                    <div class="col-sm-6 inner-left number-of-trustees medicare-section table-width" >
                                        <p style="width: 100%; font-weight: 400;">Check all that apply:</p>

                                        <div class="trustee termLife">
                                            <a class="long-term-insurance-parts homeCareModal <?php
                                            if (!empty($answer) && array_key_exists('home_care', $answer) && ($answer["home_care"] == "true")) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-toggle="modal" data-target="#homeCareModal">
                                                <div class="medicare-options long-term-insurance">
                                                    {{ Form::input('hidden','answer[701][home_care]',(!empty($answer) && array_key_exists('home_care',$answer)) ? $answer['home_care']:null,['id'=>'homeCareData']) }}
                                                    <p>In-home <br> care only</p>
                                                </div>
                                            </a>
                                            <a class=" long-term-insurance-parts nursingHomeCareModal custom-margin <?php
                                            if (!empty($answer) && array_key_exists('nursing_home', $answer) && ($answer['nursing_home'] == "true")) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-toggle="modal" data-target="#nursingHomeCareModal">
                                                <div class="medicare-options long-term-insurance">
                                                    {{ Form::input('hidden','answer[701][nursing_home]',(!empty($answer) && array_key_exists('nursing_home',$answer)) ? $answer['nursing_home']:null,['id'=>'nursingHomeCareData']) }}
                                                    <p>Nursing <br>home care</br></p>
                                                </div>
                                            </a>
                                            <a class="long-term-insurance-parts otherLongTermCareModal <?php
                                            if (!empty($answer) && array_key_exists('long_term_care', $answer) && ($answer['long_term_care'] == "true")) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-toggle="modal" data-target="#otherLongTermCareModal">
                                                <div class="medicare-options long-term-insurance">
                                                    {{ Form::input('hidden','answer[701][long_term_care]',(!empty($answer) && array_key_exists('long_term_care',$answer)) ? $answer['long_term_care']:null,['id'=>'otherLongTermCareData']) }}
                                                    <p class="custom-height" style="padding-top: 12px;">Other</p>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                   
                                </div>
                            </div>


                            <div class="sections">

                                <div id="homeCareModal" class="modal fade" role="dialog" >
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">IN-HOME CARE POLICY</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class=" section-right">
                                                            {{ Form::input('hidden','questionName[701]','TERM LIFE INSURANCE POLICY') }}
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Name of the issuing company?</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="company name" name="answer[701][issuing_company]" type="text" aria-required="true" value="<?php
                                                                    if (!empty($answer) && array_key_exists('issuing_company', $answer)) {
                                                                        echo $answer['issuing_company'];
                                                                    }
                                                                    ?>">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Policy start date</label>
                                                                    <input data-validation="" class="custom-validation form-control homeCareDatetimepicker" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="answer[701][policy_start_date]" type="text" aria-required="true" value="<?php
                                                                    if (!empty($answer) && array_key_exists('policy_start_date', $answer)) {
                                                                        echo $answer['policy_start_date'];
                                                                    }
                                                                    ?>">
                                                                </div>
                                                                <div class="col-sm-12 inner-left premium_box">
                                                                    <label for="label">Premium amount</label>
                                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                                    <input class="premium_input" type="number" required="required" name="answer[701][per_premium_amount]" style="z-index: 1 !important;" value="<?php
                                                                    if (!empty($answer) && array_key_exists('per_premium_amount', $answer)) {
                                                                        echo $answer['per_premium_amount'];
                                                                    }
                                                                    ?>">
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <p style="position: absolute; font-size: 12px; top: 38px; left: 25%; z-index: 1 !important; font-weight: 400;"><i style="z-index: 1">per</i></p>
                                                                    <select style="position: absolute;" id="custom-select" class="" required="required" name="answer[701][premium_amount]"  aria-required="true"><option disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount', $answer) && ($answer['premium_amount'] == 1))
                                                                            echo "selected";
                                                                        ?>>month</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount', $answer) && ($answer['premium_amount'] == 2))
                                                                            echo "selected";
                                                                        ?>>quarter</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount', $answer) && ($answer['premium_amount'] == 3))
                                                                                    echo "selected";
                                                                                ?>>semi-annual</option><option value="4" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount', $answer) && ($answer['premium_amount'] == 4))
                                                                                    echo "selected";
                                                                                ?>>annual</option></select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Benefit Period</label>
                                                                    <div class="primary-option benefit-option">
                                                                        <ul>
                                                                            <p class="benefit-select primary-select"><i>check box if lifetime</i></p>
                                                                            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">

                                                                                    <input type="checkbox" class="benefit-check" name="answer[701][benefit-check-home]" <?php
                                                                                    if (!empty($answer) && array_key_exists('benefit-check-home', $answer)) {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?> />

                                                                                    <span class="checkbox-icon"></span>
                                                                                    <span>
                                                                                    </span>
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="number of years " name="answer[701][benefit_period]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('benefit_period', $answer)) {
                                                                        echo $answer['benefit_period'];
                                                                    }
                                                                    ?>" type="number" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Daily benefit amount</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-dollar"></i>
                                                                        </span>
                                                                        <input data-validation="" class="borderLeft0 custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][daily_benefit_amount]" value="<?php
                                                                        if (!empty($answer) && array_key_exists('daily_benefit_amount', $answer)) {
                                                                            echo $answer['daily_benefit_amount'];
                                                                        }
                                                                        ?>" type="number" aria-required="true">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Elimination period (in days)</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][elimination_period]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('elimination_period', $answer)) {
                                                                        echo $answer['elimination_period'];
                                                                    }
                                                                    ?>" type="number" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Inflation option</label>
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <select style="position: absolute;"  class="" required="" name="answer[701][inflation_option]"  aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option', $answer) && ($answer['inflation_option'] == 1))
                                                                            echo "selected";
                                                                        ?>>none</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option', $answer) && ($answer['inflation_option'] == 2))
                                                                            echo "selected";
                                                                        ?>>simple</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('inflation_option', $answer) && ($answer['inflation_option'] == 3))
                                                                                    echo "selected";
                                                                                ?>>compounded</option></select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Upload copy of your policy (highly recommended if available) </label>


                                                                    <!-- uploader starts -->
                                                                    <div class="col-sm-12 paddingLeft0 inner-left paddingRight0 home-care-info-statement js">
                                                                        <input type="file" name="home_care_info_statement" id="file-29" class="inputfile inputfile-2" onclick="this.value=null;" />
                                                                        <!-- add this attribute for the multiple file upload data-multiple-caption="{count} files selected" multiple-->
                                                                        <label for="file-29" class=" upload-statment upload-statement-click" style="font-size:14px; color: #0991e1; width: auto !important; font-weight: normal;  border: 1px solid #0991e1;">
                                                                            <span style="font-size: 0"></span>Upload file
                                                                        </label>

                                                                        <label  class="btn upload-statment-noclick hide" style="color: #0991e1; box-shadow: 0 0 0 ; font-size:14px;  border: 1px solid #0991e1; width: auto !important; cursor: not-allowed;" >
                                                                            Upload  file
                                                                        </label>

                                                                        <div class="upload-last-year-info <?php
                                                                        if (!empty($answer) && array_key_exists('homeCareFileName', $answer) && $answer['homeCareFileName'] !="") {
                                                                            echo "";
                                                                        } else {
                                                                            echo "hide";
                                                                        }
                                                                        ?>">
                                                                            <div class="col-sm-12 row inner-left">
                                                                                <table>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <td style="font-size: 13px;">Documents</td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (!empty($answer) && array_key_exists('homeCareFileName', $answer) && $answer['homeCareFileName'] !="") {
                                                                                            ?>
                                                                                            <tr style="border-bottom:0">
                                                                                                <td><span class="filename"><?php
                                                                                                        if (!empty($answer) && array_key_exists('homeCareFileName', $answer))
                                                                                                            echo $answer['homeCareFileName'];
                                                                                                        ?></span><input type="hidden" name="answer[701][homeCareFilePath]" class="home-care-file-path" value="<?php
                                                                                                        if (!empty($answer) && array_key_exists('homeCareFilePath', $answer))
                                                                                                            echo $answer['homeCareFilePath'];
                                                                                                        ?>"><input type="hidden" name="answer[701][homeCareFileName]" class="homeCareFileName" value="<?php
                                                                                                                      if (!empty($answer) && array_key_exists('homeCareFileName', $answer))
                                                                                                                          echo $answer['homeCareFileName'];
                                                                                                                      ?>"> </td>
                                                                                                <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- uplaoder finish -->

                                                                </div>
                                                                <div class="col-sm-12 inner-left text-center">
                                                                    <button class="modal-button  finishBtn" type="button" style="float:none; display: inline-block; border-radius: 3px; background: #2179EE; margin-left: 0; color: #fff;">
                                                                        Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="nursingHomeCareModal" class="modal fade" role="dialog" >
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">NURSING HOME CARE POLICY</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class=" section-right ">
                                                            {{ Form::input('hidden','questionName[701]','NURSING HOME CARE POLICY  ') }}
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Name of the issuing company?</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="company name" name="answer[701][issuing_company_1]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('issuing_company_1', $answer)) {
                                                                        echo $answer['issuing_company_1'];
                                                                    }
                                                                    ?>" type="text" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Policy start date</label>
                                                                    <input data-validation="" class="custom-validation form-control homeCareDatetimepicker" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="answer[701][policy_start_date_1]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('policy_start_date_1', $answer)) {
                                                                        echo $answer['policy_start_date_1'];
                                                                    }
                                                                    ?>" type="text" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left premium_box">
                                                                    <label for="label">Premium amount</label>
                                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                                    <input class="premium_input" type="number" name="answer[701][per_premium_amount_1]" required="required" style="z-index: 1 !important" value="<?php
                                                                    if (!empty($answer) && array_key_exists('per_premium_amount_1', $answer)) {
                                                                        echo $answer['per_premium_amount_1'];
                                                                    }
                                                                    ?>">
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <p style="position: absolute; font-size: 12px; top: 38px; left: 25%; z-index: 1 !important; font-weight: 400;"><i>per</i></p>
                                                                    <select style="position: absolute;" id="custom-select" class=""  required="required" name="answer[701][premium_amount_1]" style="z-index: 1 !important;" aria-required="true"><option disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount_1', $answer) && ($answer['premium_amount_1'] == 1))
                                                                            echo "selected";
                                                                        ?>>month</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount_1', $answer) && ($answer['premium_amount_1'] == 2))
                                                                            echo "selected";
                                                                        ?>>quarter</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount_1', $answer) && ($answer['premium_amount_1'] == 3))
                                                                                    echo "selected";
                                                                                ?>>semi-annual</option><option value="4" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount_1', $answer) && ($answer['premium_amount_1'] == 4))
                                                                                    echo "selected";
                                                                                ?>>annual</option></select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Benefit Period</label>

                                                                    <div class="primary-option benefit-option">
                                                                        <ul>
                                                                            <p class="benefit-select primary-select"><i>check box if lifetime</i></p>
                                                                            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">

                                                                                    <input type="checkbox" class="benefit-check" name="answer[701][benefit-check-nursing]" <?php
                                                                                    if (!empty($answer) && array_key_exists('benefit-check-nursing', $answer)) {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?> />

                                                                                    <span class="checkbox-icon"></span>
                                                                                    <span>
                                                                                    </span>
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="number of years " name="answer[701][benefit_period_1]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('benefit_period_1', $answer)) {
                                                                        echo $answer['benefit_period_1'];
                                                                    }
                                                                    ?>" type="number" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Daily benefit amount</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-dollar"></i>
                                                                        </span>
                                                                        <input data-validation="" class="borderLeft0 service-input-groupcustom-validation borderLeft0 form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][daily_benefit_amount_1]" value="<?php
                                                                        if (!empty($answer) && array_key_exists('daily_benefit_amount_1', $answer)) {
                                                                            echo $answer['daily_benefit_amount_1'];
                                                                        }
                                                                        ?>" type="number" aria-required="true">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Elimination period (in days)</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][elimination_period_1]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('elimination_period_1', $answer)) {
                                                                        echo $answer['elimination_period_1'];
                                                                    }
                                                                    ?>" type="number" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Inflation option</label>
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <select style="position: absolute;"  class="" required="" name="answer[701][inflation_option_1]"  aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option_1', $answer) && ($answer['inflation_option_1'] == 1))
                                                                            echo "selected";
                                                                        ?>>none</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option_1', $answer) && ($answer['inflation_option_1'] == 2))
                                                                            echo "selected";
                                                                        ?>>simple</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('inflation_option_1', $answer) && ($answer['inflation_option_1'] == 3))
                                                                                    echo "selected";
                                                                                ?>>compounded</option></select>
                                                                </div>
                                                                <div class="col-sm-6 inner-left ">

                                                                    <label for="label">Is there a home-care option?</label>
                                                                    <label class="radio-custom-label">
                                                                        <input class="home-care-option" required="required" name="answer[701][home_care_option_1]" type="radio" value="yes" <?php
                                                                        if (!empty($answer) && array_key_exists('home_care_option_1', $answer) && ($answer['home_care_option_1'] == "yes")) {
                                                                            echo "checked";
                                                                        }
                                                                        ?> aria-required="true">Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">
                                                                        <input class="home-care-option" required="required" name="answer[701][home_care_option_1]" type="radio" value="no" aria-required="true" <?php
                                                                        if (!empty($answer) && array_key_exists('home_care_option_1', $answer) && ($answer['home_care_option_1'] == "no")) {
                                                                            echo "checked";
                                                                        }
                                                                        ?>>No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12 inner-left facility-care-percent" style="display: <?php
                                                                if (!empty($answer) && array_key_exists('home_care_option_1', $answer) && ($answer['home_care_option_1'] == "yes")) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                ?>">
                                                                    <label for="label">What is the benefit % of facility care?</label>

                                                                    <div class="input-group service-input-group">

                                                                        <input data-validation="" class="borderRight0 custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][benefit_facility_care_1]" value="<?php
                                                                        if (!empty($answer) && array_key_exists('benefit_facility_care_1', $answer)) {
                                                                            echo $answer['benefit_facility_care_1'];
                                                                        }
                                                                        ?>" type="number" aria-required="true">
                                                                        <span class="input-group-addon">
                                                                            %
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Upload copy of your policy (if available highly recommended) </label>
                                                                    <!-- uploader starts -->

                                                                    <div class="col-sm-12 paddingLeft0 inner-left paddingRight0 nursing-care-info-statement js">
                                                                        <input type="file" name="nursing_care_info_statement" id="file-28" class="inputfile inputfile-2" required onclick="this.value=null;" />
                                                                        <label for="file-28" class=" upload-statment upload-statement-click" style="font-size:14px; color: #0991e1; width: auto !important; font-weight: normal;  border: 1px solid #0991e1;">
                                                                            <span style="font-size: 0"></span>Upload file
                                                                        </label>

                                                                        <label  class="btn upload-statment-noclick hide" style="color: #0991e1; box-shadow: 0 0 0 ; font-size:14px;  border: 1px solid #0991e1; width: auto !important; cursor: not-allowed;" >
                                                                            Upload  file
                                                                        </label>

                                                                        <div class="nursing-care-year-info <?php
                                                                        if (!empty($answer) && array_key_exists('nursing_file_name', $answer)) {
                                                                            echo "";
                                                                        } else {
                                                                            echo "hide";
                                                                        }
                                                                        ?>">
                                                                            <div class="col-sm-12 row inner-left">
                                                                                <table>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <td style="font-size: 13px;">Documents</td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (!empty($answer) && array_key_exists('nursing_file_name', $answer)) {
                                                                                            ?>
                                                                                            <tr style="border-bottom:0">
                                                                                                <td><span class="filename"><?php
                                                                                                        if (!empty($answer) && array_key_exists('nursing_file_name', $answer))
                                                                                                            echo $answer['nursing_file_name'];
                                                                                                        ?></span><input type="hidden" name="answer[701][nursing_file_path]" class="nursingFilePath" value="<?php
                                                                                                        if (!empty($answer) && array_key_exists('nursing_file_path', $answer))
                                                                                                            echo $answer['nursing_file_path'];
                                                                                                        ?>"><input type="hidden" name="answer[701][nursing_file_name]" class="nursingFileName" value="<?php
                                                                                                                      if (!empty($answer) && array_key_exists('nursing_file_name', $answer))
                                                                                                                          echo $answer['nursing_file_name'];
                                                                                                                      ?>"> </td>
                                                                                                <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left text-center">
                                                                    <button class="modal-button finishBtn" type="button" style="float:none; display: inline-block; border-radius: 3px; background: #2179EE; margin-left: 0; color: #fff;">
                                                                        Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="otherLongTermCareModal" class="modal fade" role="dialog" >
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">OTHER LONG TERM CARE POLICY</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class="section-right">
                                                            {{ Form::input('hidden','questionName[701]','OTHER LONG TERM CARE POLICY') }}
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Name of the issuing company?</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="company name" name="answer[701][issuing_company_2]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('issuing_company_2', $answer)) {
                                                                        echo $answer['issuing_company_2'];
                                                                    }
                                                                    ?>" type="text" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Describe type of policy</label>
                                                                    <textarea data-validation="" class="custom-validation form-control policy-description" rows="4" data-rule-regex="false" required="" placeholder="description" name="answer[701][type_of_policy_2]" type="text" aria-required="true"><?php
                                                                        if (!empty($answer) && array_key_exists('type_of_policy_2', $answer)) {
                                                                            echo $answer['type_of_policy_2'];
                                                                        }
                                                                        ?></textarea>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Policy start date</label>
                                                                    <input data-validation="" class="custom-validation form-control homeCareDatetimepicker" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="answer[701][policy_start_date_2]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('policy_start_date_2', $answer)) {
                                                                        echo $answer['policy_start_date_2'];
                                                                    }
                                                                    ?>" type="text" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left premium_box">
                                                                    <label for="label">Premium amount</label>
                                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                                    <input class="premium_input" type="number" name="answer[701][per_premium_amount_2]" style="z-index:1" required="required" value="<?php
                                                                    if (!empty($answer) && array_key_exists('per_premium_amount_2', $answer)) {
                                                                        echo $answer['per_premium_amount_2'];
                                                                    }
                                                                    ?>">
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <p style="position: absolute; font-size: 12px; top: 38px; left: 25%; z-index: 1 !important; font-weight: 400;">per</p>
                                                                    <select style="position: absolute;" id="custom-select" class="" required="" name="answer[701][premium_amount_2]" required="required" style="z-index:1 !important" aria-required="true"><option disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount_2', $answer) && ($answer['premium_amount_2'] == 1))
                                                                            echo "selected";
                                                                        ?>>month</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('premium_amount_2', $answer) && ($answer['premium_amount_2'] == 2))
                                                                            echo "selected";
                                                                        ?>>quarter</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount_2', $answer) && ($answer['premium_amount_2'] == 3))
                                                                                    echo "selected";
                                                                                ?>>semi-annual</option><option value="4" <?php
                                                                                if (!empty($answer) && array_key_exists('premium_amount_2', $answer) && ($answer['premium_amount_2'] == 4))
                                                                                    echo "selected";
                                                                                ?>>annual</option></select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Benefit Period</label>
                                                                    <div class="primary-option benefit-option">
                                                                        <ul>
                                                                            <p class="benefit-select primary-select"><i>check box if lifetime</i></p>
                                                                            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">

                                                                                    <input type="checkbox" class="benefit-check" name="answer[701][benefit-check-other]" <?php
                                                                                    if (!empty($answer) && array_key_exists('benefit-check-other', $answer)) {
                                                                                        echo "checked";
                                                                                    }
                                                                                    ?> />

                                                                                    <span class="checkbox-icon"></span>
                                                                                    <span>
                                                                                    </span>
                                                                                </label>
                                                                            </li>
                                                                        </ul>
                                                                    </div>

                                                                    <input style="text-align: left;" data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="number of years " name="answer[701][benefit_period_2]" value="<?php
                                                                    if (!empty($answer) && array_key_exists('benefit_period_2', $answer)) {
                                                                        echo $answer['benefit_period_2'];
                                                                    }
                                                                    ?>" type="number" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Daily benefit amount</label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-dollar"></i>
                                                                        </span>
                                                                        <input data-validation="" class="borderLeft0 custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][daily_benefit_amount_2]" value="<?php
                                                                        if (!empty($answer) && array_key_exists('daily_benefit_amount_2', $answer)) {
                                                                            echo $answer['daily_benefit_amount_2'];
                                                                        }
                                                                        ?>" type="number" aria-required="true">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Elimination period (in days)</label>
                                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][elimination_period_2]" type="number" value="<?php
                                                                    if (!empty($answer) && array_key_exists('elimination_period_2', $answer)) {
                                                                        echo $answer['elimination_period_2'];
                                                                    }
                                                                    ?>" aria-required="true">
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Inflation option</label>
                                                                    <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; top: 41px; right: 10%; z-index: 1; font-size: 16px;"></i>
                                                                    <select style="position: absolute;"  class="" required="" name="answer[701][inflation_option_2]"  aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option_2', $answer) && ($answer['inflation_option_2'] == 1))
                                                                            echo "selected";
                                                                        ?>>none</option>
                                                                        <option value="2" <?php
                                                                        if (!empty($answer) && array_key_exists('inflation_option_2', $answer) && ($answer['inflation_option_2'] == 2))
                                                                            echo "selected";
                                                                        ?>>simple</option><option value="3" <?php
                                                                                if (!empty($answer) && array_key_exists('inflation_option_2', $answer) && ($answer['inflation_option_2'] == 3))
                                                                                    echo "selected";
                                                                                ?>>compounded</option></select>
                                                                </div>
                                                                <div class="col-sm-6 inner-left ">

                                                                    <label for="label">Is there a home-care option?</label>
                                                                    <label class="radio-custom-label">
                                                                        <input class="other-home-care-option" required="required" name="answer[701][home_care_option_2]" type="radio" value="yes" aria-required="true" <?php
                                                                        if (!empty($answer) && array_key_exists('home_care_option_2', $answer) && ($answer['home_care_option_2'] == "yes")) {
                                                                            echo "checked";
                                                                        }
                                                                        ?>>Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">
                                                                        <input class="other-home-care-option" required="required" name="answer[701][home_care_option_2]" type="radio" value="no" aria-required="true" <?php
                                                                        if (!empty($answer) && array_key_exists('home_care_option_2', $answer) && ($answer['home_care_option_2'] == "no")) {
                                                                            echo "checked";
                                                                        }
                                                                        ?>>No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-sm-12 inner-left other-facility-care-percent" style="display: <?php
                                                                if (!empty($answer) && array_key_exists('home_care_option_2', $answer) && ($answer['home_care_option_2'] == "yes")) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                ?>">
                                                                    <label for="label">What is the benefit % of facility care?</label>
                                                                    <div class="input-group">
                                                                        <i class="fa fa-percentselectArrow" aria-hidden="true" ></i>
                                                                        <input data-validation="" class="borderRight0 custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="answer[701][benefit_facility_care_2]" value="<?php
                                                                        if (!empty($answer) && array_key_exists('benefit_facility_care_2', $answer)) {
                                                                            echo $answer['benefit_facility_care_2'];
                                                                        }
                                                                        ?>" type="number" aria-required="true">
                                                                        <span class="input-group-addon">
                                                                            %
                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label for="label">Upload copy of your policy (if available highly recommended) </label>

                                                                    <!-- uploader starts -->

                                                                    <div class="col-sm-12 paddingLeft0 inner-left paddingRight0 otherlong-care-info-statement js">
                                                                        <input type="file" name="other_long_care_info_statement" id="file-21" class="inputfile inputfile-2" required onclick="this.value=null;" />
                                                                        <label for="file-21" class=" upload-statment upload-statement-click" style="font-size:14px; color: #0991e1; width: auto !important; font-weight: normal;  border: 1px solid #0991e1;">
                                                                            <span style="font-size: 0"></span>Upload file
                                                                        </label>

                                                                        <label  class="btn upload-statment-noclick hide" style="color: #0991e1; box-shadow: 0 0 0 ; font-size:14px;  border: 1px solid #0991e1; width: auto !important; cursor: not-allowed;" >
                                                                            Upload  file
                                                                        </label>

                                                                        <div class="other-long-term-care-info <?php
                                                                        if (!empty($answer) && array_key_exists('other_file_name', $answer)) {
                                                                            echo "";
                                                                        } else {
                                                                            echo "hide";
                                                                        }
                                                                        ?>">
                                                                            <div class="col-sm-12 row inner-left">
                                                                                <table>
                                                                                    <thead>
                                                                                        <tr>
                                                                                            <td style="font-size: 13px;">Documents</td>
                                                                                            <td></td>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                        <?php
                                                                                        if (!empty($answer) && array_key_exists('other_file_name', $answer)) {
                                                                                            ?>
                                                                                            <tr style="border-bottom:0">
                                                                                                <td><span class="filename"><?php
                                                                                                        if (!empty($answer) && array_key_exists('other_file_name', $answer))
                                                                                                            echo $answer['other_file_name'];
                                                                                                        ?></span><input type="hidden" name="answer[701][other_file_path]" class="otherFilePath" value="<?php
                                                                                                        if (!empty($answer) && array_key_exists('other_file_path', $answer))
                                                                                                            echo $answer['other_file_path'];
                                                                                                        ?>"><input type="hidden" name="answer[701][other_file_name]" class="otherFileName" value="<?php
                                                                                                                      if (!empty($answer) && array_key_exists('other_file_name', $answer))
                                                                                                                          echo $answer['other_file_name'];
                                                                                                                      ?>"> </td>
                                                                                                <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>
                                                                                        <?php } ?>
                                                                                    </tbody>
                                                                                </table>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- uplaoder finish -->

                                                                </div>

                                                                <div class="col-sm-12 inner-left text-center">
                                                                    <button class="modal-button finishBtn" type="button" style="float:none; display: inline-block; border-radius: 3px; background: #2179EE; margin-left: 0; color: #fff;">
                                                                        Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <textarea class="hidden" value="" id="filesdata" name="answer[701][educationalList]"> {{(!empty($answer) && array_key_exists('educationalList',$answer))? $answer["educationalList"]:null}} </textarea>
                        </section>
                    </fieldset>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,6,'insurance-final-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $('.homeCareDatetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $("select").selectBoxIt();
        var educationalList = [];
        var filesComprehensive = [];
        $('#homeCareModal .finishBtn').on('click', function () {

            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                $('#homeCareModal').modal('hide');
                $('#homeCareModal').find('.error-alert').remove();
                $('.homeCareModal').addClass('selected-option');
                $('#homeCareData').val('true');
                
                

                    filesComprehensive.push(
                            {
                                "key": "homeCareFile", "file_name": $('.upload-last-year-info .homeCareFileName').val()
                                , "file_path": $(".upload-last-year-info .home-care-file-path").val()
                            }
                            
                    );
            educationalList.push({
                filesComprehensive : filesComprehensive
            });
                $('#filesdata').html(JSON.stringify(educationalList));
            }
            
            return false;
                
            
        });

        $('#nursingHomeCareModal .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                $('#nursingHomeCareModal').modal('hide');
                $('.nursingHomeCareModal').addClass('selected-option');
                $('#nursingHomeCareData').val('true');
                

                    filesComprehensive.push(
                            {
                                "key": "nursingFile", "file_name": $('.nursing-care-year-info .nursingFileName').val()
                                , "file_path":  $(".nursing-care-year-info .nursingFilePath").val()
                            }
                            
                    );
               educationalList.push({
                filesComprehensive : filesComprehensive
            });
                 $('#filesdata').html(JSON.stringify(educationalList));
            }
            return false;
        });

        $('#otherLongTermCareModal .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                $('#otherLongTermCareModal').modal('hide');
                $('.otherLongTermCareModal').addClass('selected-option');
                $('#otherLongTermCareData').val('true');

                    filesComprehensive.push(
                            {
                                "key": "otherFile", "file_name": $('.otherFilePath').val()
                                , "file_path":  $(".other-long-term-care-info .otherFilePath").val()
                            }
                            
                    );
             educationalList.push({
                filesComprehensive : filesComprehensive
            });
             $('#filesdata').html(JSON.stringify(educationalList));
            }
            return false;
        });

        $(document).on('change', '.home-care-option', function () {
            if ($(this).val() === 'yes') {

                $('.facility-care-percent').show();
            } else {
                $('.facility-care-percent').hide();
                $('.facility-care-percent input').val('');
            }
        });

        $(document).on('change', '.other-home-care-option', function () {
            if ($(this).val() === 'yes') {
                $('#otherLongTermCareModal').find('.error-alert').remove();
                $('.other-facility-care-percent').show();
            } else {
                $('.other-facility-care-percent').hide();
                $('.other-facility-care-percent input').val('');
            }
        });

        if ($('.long-term-care:checked').val() == 'no') {
            $('#homeCareModal').find("input").val('');
            $('#nursingHomeCareModal').find("input").val('');
            $('#otherLongTermCareModal').find("input").val('');
            $('#nursingHomeCareModal').find("select").val('').trigger('change');
            $('#otherLongTermCareModal').find("select").val('').trigger('change');
            $('#otherLongTermCareModal').find("textarea").text('');
            $('#homeCareModal').find("select").val('').trigger('change');
            $('#homeCareData').val('');
            $('#otherLongTermCareData').val('');
            $('#nursingHomeCareData').val('');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
        }

        $(document).on('change', '.long-term-care', function () {
            if ($(this).val() == 'no') {
                $('#homeCareModal').find("input").val('');
                $('#nursingHomeCareModal').find("input").val('');
                $('#otherLongTermCareModal').find("input").val('');
                $('#nursingHomeCareModal').find("select").val('').trigger('change');
                $('#otherLongTermCareModal').find("select").val('').trigger('change');
                $('#otherLongTermCareModal').find("textarea").text('');
                $('#homeCareModal').find("select").val('').trigger('change');
                $('#homeCareData').val('');
                $('#otherLongTermCareData').val('');
                $('#nursingHomeCareData').val('');
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });
        $(document).on('click', '.hide-modal', function () {
            $('.modal').modal('hide');
            $('.' + $(this).closest('.modal').attr('id')).addClass('selected-option');
            $('.' + $(this).closest('.modal').attr('id')).find('input[type="hidden"]').val('true');
        });


        $(document).each(function () {
            $(document).on('change', '.home-care-info-statement input[type=file]', function () {
                if ($('.home-care-info-statement .upload-statment span').text().length > 0) {
                    $('.home-care-info-statement .upload-last-year-info').removeClass('hide');
                    $('.home-care-info-statement .upload-last-year-info table tbody').html('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="answer[701][homeCareFilePath]" class="home-care-file-path"><input type="hidden" name="answer[701][homeCareFileName]" class="homeCareFileName"> </td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>');
                    $(".upload-last-year-info .filename").text($('.home-care-info-statement .upload-statment span').text());
                    $(".home-care-info-statement .upload-statement-click").addClass('hide');
                    $(".home-care-info-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var UploadLastYearStatementInfo = new FormData();
                    $('.homeCareFileName').val($('.home-care-info-statement .upload-statment span').text());
                    UploadLastYearStatementInfo.append('file', $('[name=home_care_info_statement]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: UploadLastYearStatementInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".upload-last-year-info .home-care-file-path").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });

        $('.home-care-info-statement table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $('.upload-last-year-info ').addClass('hide');
            $(".home-care-info-statement .upload-statement-click").removeClass('hide');
            $(".home-care-info-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.home-care-info-statement .upload-statment span').text('');
        });


        $(document).each(function () {
            $(document).on('change', '.otherlong-care-info-statement input[type=file]', function () {
                if ($('.otherlong-care-info-statement .upload-statment span').text().length > 0) {
                    $('.otherlong-care-info-statement .other-long-term-care-info').removeClass('hide');
                    $('.otherlong-care-info-statement .other-long-term-care-info table tbody').append('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="answer[701][other_file_path]" class="otherFilePath"><input type="hidden" name="answer[701][other_file_name]" class="otherFileName"> </td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>');
                    $(".other-long-term-care-info .filename").text($('.otherlong-care-info-statement .upload-statment span').text());
                    $(".otherlong-care-info-statement .upload-statement-click").addClass('hide');
                    $(".otherlong-care-info-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var otherLongTermCareInfo = new FormData();
                    $('.otherFileName').val($('.otherlong-care-info-statement .upload-statment span').text());
                    otherLongTermCareInfo.append('file', $('[name=other_long_care_info_statement]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: otherLongTermCareInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".other-long-term-care-info .otherFilePath").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });

        $('.otherlong-care-info-statement table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $('.other-long-term-care-info').addClass('hide');
            $(".otherlong-care-info-statement .upload-statement-click").removeClass('hide');
            $(".otherlong-care-info-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.otherlong-care-info-statement .upload-statment span').text('');
        });


        $(document).each(function () {
            $(document).on('change', '.nursing-care-info-statement input[type=file]', function () {
                if ($('.nursing-care-info-statement .upload-statment span').text().length > 0) {
                    $('.nursing-care-info-statement .nursing-care-year-info').removeClass('hide');
                    $('.nursing-care-info-statement .nursing-care-year-info table tbody').append('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="answer[701][nursing_file_path]" class="nursingFilePath"> <input type="hidden" name="answer[701][nursing_file_name]" class="nursingFileName"></td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash  cursor-pointer"></td></tr>');
                    $(".nursing-care-year-info .filename").text($('.nursing-care-info-statement .upload-statment span').text());
                    $(".nursing-care-info-statement .upload-statement-click").addClass('hide');
                    $(".nursing-care-info-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var nursingCareInfo = new FormData();
                    $('.nursingFileName').val($('.nursing-care-info-statement .upload-statment span').text());
                    nursingCareInfo.append('file', $('[name=nursing_care_info_statement]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: nursingCareInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".nursing-care-year-info .nursingFilePath").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });

        $('.nursing-care-info-statement table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $('.nursing-care-year-info').addClass('hide');
            $(".nursing-care-info-statement .upload-statement-click").removeClass('hide');
            $(".nursing-care-info-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.nursing-care-info-statement .upload-statment span').text('');
        });



        'use strict';
        ;
        (function (document, window, index)
        {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener('focus', function () {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('has-focus');
                });
            });
        }(document, window, 0));

    });

    $(document).on('click', '.homeCareModal', function () {
        $('#homeCareModal').find('.error-alert').remove();
    });

    $(document).on('click', '.nursingHomeCareModal', function () {
        $('#nursingHomeCareModal').find('.error-alert').remove();
    });

    $(document).on('click', '.otherLongTermCareModal', function () {
        $('#otherLongTermCareModal').find('.error-alert').remove();
    });




</script>
<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script>
@stop

