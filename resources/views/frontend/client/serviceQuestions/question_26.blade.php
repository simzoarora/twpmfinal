 
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p style="margin-bottom: 0" >INCOME & TAX BRACKET</p>
        <h2>Let's make sure we have the right income information</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="#" class="contact-modal-show">contact us.</a></p>
    </div>

    <?php $array = config('constant.tax_filing_status'); ?>
    <div class="col-sm-6 col-sm-offset-1 section-right taxBracket">
        {{ Form::input('hidden','questionName[26]','Let\'s make sure we have the right income information.') }}
        <div class="col-sm-8 inner-left ">
            {{ Form::label('label', 'Household gross income',['title'=>'Total income from all sources']) }}
             <div class="input-group service-input-group">
            <span class="input-group-addon">$</span>
            {{ Form::input('number','answer[26][household_gross_income]',isset($defaultData)? $defaultData['housesholdIncome']:'',['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control','readonly'=>true, 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$', 'style'=>'background:transparent;']) }}
             </div>
             </div>
        <div class="col-sm-8 inner-left pos-rel">
            {{ Form::label('label', 'Your gross income') }} 
            <div class="input-group service-input-group">
            <span class="input-group-addon">$</span>
            {{ Form::input('number','answer[26][estimated_taxable_income]',(!empty($answer) && array_key_exists('estimated_taxable_income',$answer)) ? $answer['estimated_taxable_income'] :null,['data-validation'=> '' , 'class'=>' comprehensive-width borderLeft0 custom-validation form-control taxable-income', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
            </div>
       <div class="lds-dual-ring" style="display: none;"></div>
       
        </div>

        

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Tax filling status') }}
            <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 102px; z-index: 1; top: 42px; font-size: 16px;"></i>
            <select style="position: absolute;" readonly="true" class="tax_filling_status" name="answer[26][tax_filling_status]" required><option selected disabled value="">SELECT</option> 
                <option value="1" <?php if ($defaultData['taxFillingStatus'] == 1) echo "selected"; ?>>Single</option>
                <option value="2" <?php if ($defaultData['taxFillingStatus'] == 2) echo "selected"; ?>>Head of household</option>
                <option value="3" <?php if ($defaultData['taxFillingStatus'] == 3) echo "selected"; ?>>Qualifying Widow(er)</option>
                <option value="4" <?php if ($defaultData['taxFillingStatus'] == 4) echo "selected"; ?>>Married filling jointly</option>
                <option value="5" <?php if ($defaultData['taxFillingStatus'] == 5) echo "selected"; ?>>Married Filing Separately</option> 
                <option value="5" <?php if ($defaultData['taxFillingStatus'] == 6) echo "selected"; ?>>Married Filing Separately (living apart)</option></select>
            <!--{{ Form::select('answer[26][tax_filling_status]',$array,isset($defaultData)? $defaultData['taxFillingStatus']:'',['disabled'=>'disabled'])}}-->
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Tax bracket',[' title'=>"Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income"]) }}
            <div class="service-input-group coverage-input-group">
                {{ Form::input('text','answer[26][tax_bracket]',(!empty($answer) && array_key_exists('tax_bracket',$answer)) ? $answer['tax_bracket'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control tax-current-value', 'readonly'=>"readonly", 'data-rule-regex'=>"false" ,  'style'=>'background:transparent;text-align: right;' ]) }}
            </div> 
        </div> 
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Total value of other non-retirement investment account') }}
            <div class="input-group service-input-group">
            <span class="input-group-addon">$</span>
            {{ Form::input('number','answer[26][non_retirement_investment_account]',(!empty($answer) && array_key_exists('non_retirement_investment_account',$answer)) ? $answer['non_retirement_investment_account'] :null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
            </div>
            </div>
    </div>
</div>
