@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',1) }}
                    {{ Form::input('hidden','topicId',14) }}
                    {{ Form::input('hidden','subTopicId',7) }}
                    {{ Form::input('hidden','redirectPageName','spouse-insurance-final-preview') }}

                    <?php
                    if (!empty($answer) && !empty($answer["copy"])) {
                        foreach ($answer["copy"] as $key => $data) {
                            ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">

                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                        </div>

                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}

                                            <div class="col-sm-6 inner-left ">

                                                {{ Form::label('label', 'Does your employer offer group disability insurance?',['style'=>'width:100;']) }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[156][copy]['.$key.'][group_disability_insurance_'.$key.']', 'yes',(array_key_exists('group_disability_insurance_'.$key,$data) && $data['group_disability_insurance_'.$key] == "yes") ? true : false,['class'=>'group-disability-insurance' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[156][copy]['.$key.'][group_disability_insurance_'.$key.']', 'no',($data['group_disability_insurance_'.$key] == "no") ? true : false,['class'=>'group-disability-insurance' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                            
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'What is the tax status of premiums? (ask HR if unsure, as this is vitally important)') }}
                                                <i class="fa fa-angle-down serviceSelectArrow" aria-hidden="true" style="top:87px! important;"></i>
                                                <select style="position: absolute;" required="" class="" name="answer[156][copy][<?php echo $key; ?>][tax_status_premium_<?php echo $key; ?>]" style="display: none;" >
                                                    <option selected disabled value="">SELECT</option>
                                                    <option value="1" <?php
                                                    if (array_key_exists('tax_status_premium_' . $key, $data) && ($data['tax_status_premium_' . $key] == 1))
                                                        echo "selected";
                                                    ?>>before tax</option>
                                                    <option value="2" <?php
                                                    if (array_key_exists('tax_status_premium_' . $key, $data) && ($data['tax_status_premium_' . $key] == 2))
                                                        echo "selected";
                                                    ?>>after tax</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'Annual premium') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon">
                                                        $
                                                    </span>
                                                    {{ Form::input('number','answer[156][copy]['.$key.'][annual_premium_'.$key.']',(array_key_exists('annual_premium_'.$key,$data)) ? $data['annual_premium_'.$key] : null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>'required' , 'placeholder'=>'']) }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'Benefit amount (Please provide both dollar amount and % of salary)') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon">$
                                                    </span> 
                                                    {{ Form::input('number','answer[156][copy]['.$key.'][benefit_amount_'.$key.']',(array_key_exists('benefit_amount_'.$key,$data)) ? $data['benefit_amount_'.$key] : null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required', 'placeholder'=>'']) }}
                                                </div>
                                            </div>
                                            <div class="col-sm-6 inner-left ">
                                                <div class="input-group service-input-group benefit-input-group">
                                                    {{ Form::input('number','answer[156][copy]['.$key.'][benefit_amount_percentage_'.$key.']',(array_key_exists('benefit_amount_percentage_'.$key,$data)) ? $data['benefit_amount_percentage_'.$key] : null,['data-validation'=> '' , 'class'=>'borderRight0 comprehensive-width custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required' , 'placeholder'=>'']) }}
                                                    <span class="input-group-addon percent-input-group-addon">%
                                                    </span> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                            
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'What is the maximum amount the employer\'s plan may provide?(both dollar amount and % of salary)') }}
                                                <div class="input-group service-input-group">
                                                    <span class="input-group-addon">
                                                        $
                                                    </span>
                                                    {{ Form::input('number','answer[156][copy]['.$key.'][maximum_amount_employer_plan_provide_'.$key.']',(array_key_exists('maximum_amount_employer_plan_provide_'.$key,$data)) ? $data['maximum_amount_employer_plan_provide_'.$key] : null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required' , 'placeholder'=>'']) }}
                                                </div>
                                            </div>

                                            <div class="col-sm-6 inner-left ">
                                                <div class="input-group service-input-group benefit-input-group">
                                                    {{ Form::input('number','answer[156][copy]['.$key.'][maximum_amount_employer_plan_provide_percentage_'.$key.']',(array_key_exists('maximum_amount_employer_plan_provide_percentage_'.$key,$data)) ? $data['maximum_amount_employer_plan_provide_percentage_'.$key] : null,['data-validation'=> '' , 'class'=>'borderRight0 comprehensive-width custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required' , 'placeholder'=>'']) }}

                                                    <span class="input-group-addon percent-input-group-addon">
                                                        %
                                                    </span> 
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                           <div class="col-sm-6 inner-left number-of-trustees">
                                                {{ Form::label('label', 'What type of disability insurance is it?') }}
                                                <div class="trustee termLife"  >
                                                    {{ Form::input('hidden','answer[156][copy]['.$key.'][trustee_option_'.$key.']',array_key_exists('trustee_option_'.$key,$data) ? $data['trustee_option_'.$key]:null, ['class'=>'disability-insurance','required'=>'required']) }}
                                                    <a class="options disability-insurance-term type-disability <?php
                                                    if (array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == '1')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-value="1">
                                                        <div class="select-trustee" >
                                                            <p>Short Term</p>
                                                        </div>
                                                    </a>
                                                    <a class="options disability-insurance-term type-disability <?php
                                                    if (array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == '2')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-value="2">
                                                        <div class="select-trustee" >
                                                            <p>Long Term</p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}

                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Elimination Period (How long until it starts paying after the onset of disability)</label>
                                                <div class="input-group service-input-group">
                                                    <input style="margin-right: 0px;border-right:0px;" data-validation="" class="comprehensive-width-addon bordeRight0 custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[156][copy][<?php echo $key; ?>][elimination_period_<?php echo $key; ?>]" type="text" aria-required="true" style="width:210px! important;" value="<?php
                                                    if (array_key_exists('elimination_period_' . $key, $data)) {
                                                        echo $data["elimination_period_" . $key];
                                                    }
                                                    ?>">
                                                    <span class="input-group-addon">
                                                        days
                                                    </span> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'Payout period (how long will the benefit will be paid?)') }}
                                                <i class="fa fa-angle-down serviceSelectArrow" aria-hidden="true"></i>
                                                <select style="position: absolute;" required="" class="payout-period" name="answer[156][copy][<?php echo $key; ?>][payout_period_long_benefit_<?php echo $key; ?>]" style="display: none;" >
                                                    <option selected disabled value="">SELECT</option>
                                                    <option value="1" <?php
                                                    if (array_key_exists('payout_period_long_benefit_' . $key, $data) && ($data['payout_period_long_benefit_' . $key] == 1))
                                                        echo "selected";
                                                    ?>>total # of months</option>
                                                    <option value="2" <?php
                                                    if (array_key_exists('payout_period_long_benefit_' . $key, $data) && ($data['payout_period_long_benefit_' . $key] == 2))
                                                        echo "selected";
                                                    ?>>until certain age</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 inner-left  right-side number-of-months" style="display: <?php
                                            if (array_key_exists('payout_period_long_benefit_' . $key, $data) && ($data['payout_period_long_benefit_' . $key] == 1 )) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>;">
                                                {{ Form::input('number','answer[156][copy]['.$key.'][group_disability_months_'.$key.']',array_key_exists('group_disability_months_'.$key,$data) ? $data['group_disability_months_'.$key] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required', 'placeholder'=>'Total # of months']) }}
                                            </div>
                                            <div class="col-sm-6 inner-left  right-side age" style="display: <?php
                                            if (array_key_exists('payout_period_long_benefit_' . $key, $data) && ($data['payout_period_long_benefit_' . $key] == 2 )) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>;">
                                                {{ Form::input('number','answer[156][copy]['.$key.'][group_disability_age_'.$key.']',(array_key_exists('group_disability_age_'.$key,$data)) ? $data['group_disability_age_'.$key] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required' , 'placeholder'=>'Age']) }}
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Other group disability insurance.</h2>
                                           
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Other group disability insurance') }}
                                            <div class="col-sm-6 inner-left ">

                                                <label style="width:100%;" for="label">Does your employer offer another group disability insurance? (For example, an employer might sponsor a 1 and also a 2 policy)</label>
                                                <label class="radio-custom-label">
                                                    <input class="other-group-disability-insurance" required="required" name="answer[156][copy][<?php echo $key; ?>][other_group_disability_insurance_<?php echo $key; ?>]" type="radio" value="yes" aria-required="true" <?php
                                                    if (array_key_exists('other_group_disability_insurance_' . $key, $data) && ($data['other_group_disability_insurance_' . $key] == "yes")) {
                                                        echo "checked";
                                                    }
                                                    ?>>Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    <input class="other-group-disability-insurance" required="required" name="answer[156][copy][<?php echo $key; ?>][other_group_disability_insurance_<?php echo $key; ?>]" type="radio" value="no" aria-required="true" <?php
                                                    if (array_key_exists('other_group_disability_insurance_' . $key, $data) && ($data['other_group_disability_insurance_' . $key] == "no")) {
                                                        echo "checked";
                                                    }
                                                    ?>>No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <?php
                        }
                    }
                    ?>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                    <h2>Personal disability insurance.</h2>
                                   
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[157]','Group disability insurance.') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have personally owned disability insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="personal-disability-insurance" required="required" name="answer[157][personal_disability_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('personal_disability_insurance', $answer) && ($answer['personal_disability_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="personal-disability-insurance" required="required" name="answer[157][personal_disability_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('personal_disability_insurance', $answer) && ($answer['personal_disability_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <?php
                    if (!empty($answer) && !empty($answer["copypersonal"])) {
                        foreach ($answer["copypersonal"] as $key1 => $data1) {
                            ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Personal disability insurance.</h2>
                                            <?php
                                            if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                ?>
                                                <p  class="question-description">
                                                    We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class=" hide-personal-insurance-info " >
                                                <div class="col-sm-6 inner-left ">
                                                    <label for="label">Annual premium</label>
                                                    <div class="input-group service-input-group">
                                                        <span class="input-group-addon">
                                                            $
                                                        </span> 
                                                        <input data-validation="" class="full-width comprehensive-width borderLeft0 custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[156][copypersonal][<?php echo $key1; ?>][annual_premium_<?php echo $key1; ?>]" type="number" aria-required="true" value="<?php
                                                        if (!empty($answer) && array_key_exists('annual_premium_' . $key1, $data1)) {
                                                            echo $data1["annual_premium_" . $key1];
                                                        }
                                                        ?>">
                                                    </div>
                                                </div>
                                                <div class="col-sm-6 inner-left ">
                                                    <label for="label">Benefit amount </label>
                                                    <div class="input-group service-input-group">
                                                        <span class="input-group-addon">
                                                            $
                                                        </span> 
                                                        <input data-validation="" class="full-width comprehensive-width borderLeft0 custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[156][copypersonal][<?php echo $key1; ?>][benefit_amt_<?php echo $key1; ?>]" type="number" aria-required="true" value="<?php
                                                        if (!empty($answer) && array_key_exists('benefit_amt_' . $key1, $data1)) {
                                                            echo $data1["benefit_amt_" . $key1];
                                                        }
                                                        ?>">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Personal disability insurance.</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left number-of-trustees">
                                                <label for="label">What type of disability insurance is it?</label>
                                                <div class="trustee termLife">
                                                    {{ Form::input('hidden','answer[156][copypersonal]['.$key1.'][trustee_option_'.$key1.']',(!empty($answer) && array_key_exists('trustee_option_'.$key1,$data1) ? $data1['trustee_option_'.$key1]:null), ['class'=>'disability_type1','required'=>'required']) }}
                                                    <a class="options disability-insurance-term type-disability1 <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key1, $data1) && ($data1['trustee_option_' . $key1] == '1')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-value="1">
                                                        <div class="select-trustee">
                                                            <p>Short Term</p>
                                                        </div>
                                                    </a>
                                                    <a class="options disability-insurance-term type-disability1 <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key1, $data1) && ($data1['trustee_option_' . $key1] == '2')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-value="2">
                                                        <div class="select-trustee">
                                                            <p>Long Term</p>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Personal disability insurance.</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}

                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Elimination Period (How long until it starts paying after the onset of disability)</label>
                                                <div class="service-input-group input-group">
                                                    <input data-validation="" class="comprehensive-width-addon borderRight0 custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="answer[156][copypersonal][<?php echo $key1; ?>][elimination_period_onset_disability_<?php echo $key1; ?>]" type="number" aria-required="true" value="<?php
                                                    if (!empty($answer) && array_key_exists('elimination_period_onset_disability_' . $key1, $data1)) {
                                                        echo $data1["elimination_period_onset_disability_" . $key1];
                                                    }
                                                    ?>">
                                                    <span class="input-group-addon">
                                                        days
                                                    </span> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Personal disability insurance.</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}
                                            <div class="col-sm-6 inner-left ">
                                                {{ Form::label('label', 'Payout period (how long will the benefit will be paid?') }}
                                                <i class="fa fa-angle-down serviceSelectArrow" aria-hidden="true"></i>
                                                <select style="position: absolute;" required="" class="personal-payout-period" name="answer[156][copypersonal][<?php echo $key1; ?>][payout_period_long_benefit_<?php echo $key1; ?>]" style="display: none;" >
                                                    <option selected disabled value="">SELECT</option>
                                                    <option value="1" <?php
                                                    if (array_key_exists('payout_period_long_benefit_' . $key1, $data1) && ($data1['payout_period_long_benefit_' . $key1] == 1))
                                                        echo "selected";
                                                    ?>>total # of months</option>
                                                    <option value="2" <?php
                                                    if (array_key_exists('payout_period_long_benefit_' . $key1, $data1) && ($data1['payout_period_long_benefit_' . $key1] == 2))
                                                        echo "selected";
                                                    ?>>until certain age</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 inner-left  right-side number-of-months" style="display: <?php
                                            if (array_key_exists('payout_period_long_benefit_' . $key1, $data1) && ($data1['payout_period_long_benefit_' . $key1] == 1)) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>;">
                                                {{ Form::input('number','answer[156][copypersonal]['.$key1.'][group_disability_months_'.$key1.']',array_key_exists('group_disability_months_'.$key1,$data1) ? $data1['group_disability_months_'.$key1] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required' , 'placeholder'=>'Total # of months']) }}
                                            </div>
                                            <div class="col-sm-6 inner-left  right-side age" style="display: <?php
                                            if (array_key_exists('payout_period_long_benefit_' . $key1, $data1) && ($data1['payout_period_long_benefit_' . $key1] == 2)) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>;">
                                                {{ Form::input('number','answer[156][copypersonal]['.$key1.'][group_disability_age_'.$key1.']',(array_key_exists('group_disability_age_'.$key1, $data1)) ? $data1['group_disability_age_'.$key1] : null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false",'required'=>'required', 'placeholder'=>'Age']) }}
                                            </div>
                                        </div>
                                    </div>

                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                                            <h2>Additional personal disability insurance policies.</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                            {{ Form::input('hidden','questionName[156]','Group disability insurance.') }}

                                            <div class="col-sm-6 inner-left ">

                                                <label for="label">Do you have any other personally-owned disability insurance?</label>
                                                <label class="radio-custom-label">
                                                    <input class="other-personal-disability-insurance" required="required" name="answer[156][copypersonal][<?php echo $key1; ?>][other_personal_owned_disability_insurance_<?php echo $key1; ?>]" type="radio" value="yes" aria-required="true" <?php
                                                    if (!empty($answer) && array_key_exists('other_personal_owned_disability_insurance_' . $key1, $data1) && ($data1['other_personal_owned_disability_insurance_' . $key1] == "yes")) {
                                                        echo "checked";
                                                    }
                                                    ?>>Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    <input class="other-personal-disability-insurance" required="required" name="answer[156][copypersonal][<?php echo $key1; ?>][other_personal_owned_disability_insurance_<?php echo $key1; ?>]" type="radio" value="no" aria-required="true" <?php
                                                    if (!empty($answer) && array_key_exists('other_personal_owned_disability_insurance_' . $key1, $data1) && ($data1['other_personal_owned_disability_insurance_' . $key1] == "no")) {
                                                        echo "checked";
                                                    }
                                                    ?>>No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <?php
                        }
                    }
                    ?>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div> 
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script id="groupDisabilityStep1" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
                <?php
                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                    ?>
                    <p  class="question-description">
                        We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    </p>
                <?php } ?>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <input name="questionName[156]" type="hidden" value="Group disability insurance.">

                <div class="col-sm-6 inner-left ">

                    <label  style="width:100%;" for="label">Does your employer offer group disability insurance?</label>
                    <label class="radio-custom-label">
                        <input class="group-disability-insurance" required="required" name="answer[156][copy][<%= copynum %>][group_disability_insurance_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="group-disability-insurance" required="required" name="answer[156][copy][<%= copynum %>][group_disability_insurance_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep2" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left">
                    <label for="label">What is the tax status of premiums? (ask HR if unsure, as this is vitally important)</label>
                    <i class="fa fa-angle-down serviceSelectArrow" style="top:87px !important; " aria-hidden="true"></i>
                    <select style="position: absolute;" required="required" class="" name="answer[156][copy][<%= copynum %>][tax_status_premium_<%= copynum %>]" style="display: none;">
                        <option selected="" disabled="" value="">SELECT</option>
                        <option value="1" >before tax</option>
                        <option value="2">after tax</option>
                    </select>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep3" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Annual premium</label>
                    <div class="input-group service-input-group">
                        <span class="input-group-addon">
                            $
                        </span> 
                        <input required="required" data-validation="" class="comprehensive-width borderLeft0 custom-validation form-control" data-rule-regex="false" placeholder="" name="answer[156][copy][<%= copynum %>][annual_premium_<%= copynum %>] " type="number">
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep4" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Benefit amount (Please provide both dollar amount and % of salary)</label>



                    <div class="input-group service-input-group">
                        <span class="input-group-addon">
                            $
                        </span> 
                        <input  required="required" data-validation="" class="comprehensive-width borderLeft0 custom-validation form-control" data-rule-regex="false" placeholder="" name="answer[156][copy][<%= copynum %>][benefit_amount_<%= copynum %>]" type="number">
                    </div>
                </div>
                <div class="col-sm-6 inner-left ">


                    <div class="input-group service-input-group benefit-input-group">
                        <input required="required" data-validation="" class=" comprehensive-width borderRight0 custom-validation form-control" data-rule-regex="false" placeholder="" name="answer[156][copy][<%= copynum %>][benefit_amount_percentage_<%= copynum %>]" type="number">
                        <span class="input-group-addon percent-input-group-addon">
                            %
                        </span> 
                    </div>


                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep5" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">What is the maximum amount the employer's plan may provide?(both dollar amount and % of salary)</label>
                    <div class="input-group service-input-group">
                        <span class="input-group-addon">
                            $
                        </span> 
                        <input required="required" data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control" data-rule-regex="false" placeholder="" name="answer[156][copy][<%= copynum %>][maximum_amount_employer_plan_provide_<%= copynum %>]" type="number">
                    </div>
                </div>

                <div class="col-sm-6 inner-left ">
                    <div class=" input-group service-input-group benefit-input-group">
                        <input required="required" data-validation="" class="comprehensive-width borderRight0 custom-validation form-control" data-rule-regex="false" placeholder=""  name="answer[156][copy][<%= copynum %>][maximum_amount_employer_plan_provide_percentage_<%= copynum %>]" type="number">
                        <span class="input-group-addon percent-input-group-addon">
                            %
                        </span> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep6" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left number-of-trustees">
                    <label for="label">What type of disability insurance is it?</label>

                    <!--                    <div class="trustee termLife">
                        <a class="options disability-insurance-term type-disability" data-value="1">
                            <div class="select-trustee" >
                                <p>Short Term</p>
                            </div>
                        </a>
                        <a class="options disability-insurance-term type-disability" data-value="2">
                            <div class="select-trustee">
                                <p>Long Term</p>
                            </div>
                        </a>
                        <input class="disability-insurance" name="answer[64][copy][<%= copynum %>][trustee_option_<%= copynum %>]" type="hidden">
                                        </div>-->
                    <div class="trustee termLife disability-insurance"> 

                        <input id="toggle[<%= copynum %>]" data-value="1" class="term-input" type="radio" name="answer[156][copy][<%= copynum %>][trustee_option_<%= copynum %>]" value="1" data-value="1" required="">
                        <label for="toggle[<%= copynum %>]" class="term-label" style="margin-right:11px! important; padding: 22px 4px! important; width: 110px !important;">Short Term</label>

                        <input id="toggle[<%= copynum +5 %>]" data-value="2" class="term-input" type="radio" name="answer[156][copy][<%= copynum %>][trustee_option_<%= copynum %>]" value="2" data-value="2" required="">
                        <label for="toggle[<%= copynum +5 %>]" class="term-label" style="margin-right:11px! important; padding: 22px 4px! important; width: 110px !important;" >Long Term </label>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep7" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Elimination Period (How long until it starts paying after the onset of disability)</label>
                    <div class="input-group service-input-group">
                        <input style="margin-right: 0px;" data-validation="" class="comprehensive-width-addon borderRight0 custom-validation form-control" data-rule-regex="false" required="" name="answer[156][copy][<%= copynum %>][elimination_period_<%= copynum %>]" type="number" aria-required="true">
                        <span class="input-group-addon">
                            days
                        </span> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep8" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Group disability insurance.</h2>
            </div>

            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Payout period (how long will the benefit will be paid?</label> 
                    <i class="fa fa-angle-down serviceSelectArrow" aria-hidden="true" ></i>
                    <select style="position: absolute;" required="" class="payout-period" name="answer[156][copy][<%= copynum %>][payout_period_long_benefit_<%= copynum %>]" style="display: none;"><option selected="" disabled="" value="">SELECT</option><option value="1">total # of months</option><option value="2">until certain age</option></select>
                </div>
                <div class="col-sm-6 inner-left  right-side number-of-months" style="display: none;">
                    <input required="required" data-validation="" class="custom-validation form-control" data-rule-regex="false" placeholder="Total # of months" name="answer[156][copy][<%= copynum %>][group_disability_months_<%= copynum %>]" type="number">
                </div>
                <div class="col-sm-6 inner-left  right-side age" style="display: none;">
                    <input required="required"  data-validation="" class="custom-validation form-control" data-rule-regex="false" placeholder="Age" name="answer[156][copy][<%= copynum %>][group_disability_age_<%= copynum %>]" type="number">
                </div>
            </div>
        </div>
    </section>
</script>
<script id="groupDisabilityStep9" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Other group disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">

                    <label  style="width:100%;" for="label">Does your employer offer another group disability insurance? (For example, an employer might sponsor a 1 and also a 2 policy)</label>
                    <label class="radio-custom-label">
                        <input class="other-group-disability-insurance" required="required" name="answer[156][copy][<%= copynum %>][other_group_disability_insurance_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="other-group-disability-insurance" required="required" name="answer[156][copy][<%= copynum %>][other_group_disability_insurance_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                    <input type="hidden" class="hiddenvalue" val="" />
                </div>
            </div>
        </div>
    </section>
</script>
<!--Personal Disability Script-->

<script id="personalDisabilityStep1" type="text/html">


    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Personal disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class=" hide-personal-insurance-info " >
                    <div class="col-sm-6 inner-left ">
                        <label for="label">Annual premium</label>
                        <div class="input-group service-input-group">
                            <span class="input-group-addon">$</span> 
                            <input data-validation="" class="full-width borderLeft0 comprehensive-width custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[156][copypersonal][<%= copynum %>][annual_premium_<%= copynum %>]" type="number" aria-required="true" >
                        </div>
                    </div>
                    <div class="col-sm-6 inner-left ">
                        <label for="label">Benefit amount </label>
                        <div class="input-group service-input-group">
                            <span class="input-group-addon">$</span> 
                            <input data-validation="" class="full-width borderLeft0 comprehensive-width  custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[156][copypersonal][<%= copynum %>][benefit_amt_<%= copynum %>]" type="number" aria-required="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


</script>
<script id="personalDisabilityStep2" type="text/html">

    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Personal disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left number-of-trustees">
                    <label for="label">What type of disability insurance is it?</label>
                    <div class="trustee termLife">
                        <input type="hidden" name="answer[156][copypersonal][<%= copynum %>][trustee_option_<%= copynum %>]" class="disability_type1" />
                        <a class="options disability-insurance-term type-disability1" data-value="1">
                            <div class="select-trustee">
                                <p>Short Term</p>
                            </div>
                        </a>
                        <a class="options disability-insurance-term type-disability1" data-value="2">
                            <div class="select-trustee">
                                <p>Long Term</p>
                            </div>
                        </a>
                    </div>
<!--                    <div class="trustee termLife"> 

                        <input class="term-input options disability-insurance-term type-disability1" id="toggle1" type="checkbox" name="answer[64][copypersonal][<%= copynum %>][trustee_option_<%= copynum %>]" class="disability_type1" data-value="1"  required="">
                        <label for="toggle1" class="term-label">Short Term </label>
                        
                         <input class="term-input options disability-insurance-term type-disability1" id="toggle2" type="checkbox" name="answer[64][copypersonal][<%= copynum %>][trustee_option_<%= copynum %>]" class="disability_type1" data-value="2"  required="">
                        <label for="toggle1" class="term-label">Long Term </label>
                    </div>-->
                </div>
            </div>
        </div>
        </div>
    </section>
</script>
<script id="personalDisabilityStep3" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Personal disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Elimination Period (How long until it starts paying after the onset of disability)</label>
                    <div class="service-input-group input-group">
                        <input data-validation="" class="comprehensive-width-addon borderRight0 custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="answer[156][copypersonal][<%= copynum %>][elimination_period_onset_disability_<%= copynum %>]" type="number" aria-required="true" >
                        <span class="input-group-addon">
                            days
                        </span> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="personalDisabilityStep4" type="text/html">


    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInSpouseName')}} DISABILITY INSURANCE</h6> 
                <h2>Personal disability insurance.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">
                    {{ Form::label('label', 'Payout period (how long will the benefit will be paid?') }}
                    <i class="fa fa-angle-down serviceSelectArrow" aria-hidden="true"></i>
                    <select style="position: absolute;" required="required"  class="personal-payout-period" name="answer[156][copypersonal][<%= copynum %>][payout_period_long_benefit_<%= copynum %>]" style="display: none;" >
                        <option selected disabled value="">SELECT</option>
                        <option value="1">total # of months</option>
                        <option value="2">until certain age</option>
                    </select>
                </div>
                <div class="col-sm-6 inner-left  right-side number-of-months" style="display:none;">
                    <input data-validation="" class=" custom-validation form-control" data-rule-regex="false" required="" placeholder="Total # of months" name="answer[156][copypersonal][<%= copynum %>][group_disability_months_<%= copynum %>]" type="number" aria-required="true" >
                </div>
                <div class="col-sm-6 inner-left  right-side age" style="display: none;">
                    <input data-validation="" class=" custom-validation form-control" data-rule-regex="false" required="" placeholder="Age" name="answer[156][copypersonal][<%= copynum %>][group_disability_age_<%= copynum %>]" type="number" aria-required="true" >
                </div>
            </div>
        </div>

    </section>


</script>
<script id="personalDisabilityStep5" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h2>Additional personal disability insurance policies.</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                <div class="col-sm-6 inner-left ">

                    <label for="label">Do you have any other personally-owned disability insurance?</label>
                    <label class="radio-custom-label">
                        <input class="other-personal-disability-insurance" required="required" name="answer[156][copypersonal][<%= copynum %>][other_personal_owned_disability_insurance_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="other-personal-disability-insurance" required="required" name="answer[156][copypersonal][<%= copynum %>][other_personal_owned_disability_insurance_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </section>
</script>


<script>

     var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,14,'spouse-insurance-final-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var tempArray = <?php echo json_encode($answer); ?>;
        if (!($.isEmptyObject(tempArray.copy))) {
            var copynumber = tempArray.copy.length - 1;
        } else {
            var copynumber = 0;
            appendData(copynumber);
        }



        if (!($.isEmptyObject(tempArray.copypersonal))) {
            var copynumber = tempArray.copypersonal.length - 1;
        } else {
            var copynumber = 0;
            appendDataPersonal(copynumber);
        }



        function appendData(copynumber)
        {
            var cind = 0;
            var insurance1 = $('#groupDisabilityStep1').html();
            var insurance2 = $('#groupDisabilityStep2').html();
            var insurance3 = $('#groupDisabilityStep3').html();
            var insurance4 = $('#groupDisabilityStep4').html();
            var insurance5 = $('#groupDisabilityStep5').html();
            var insurance6 = $('#groupDisabilityStep6').html();
            var insurance7 = $('#groupDisabilityStep7').html();
            var insurance8 = $('#groupDisabilityStep8').html();
            var insurance9 = $('#groupDisabilityStep9').html();
            html1 = _.template(insurance1);
            html2 = _.template(insurance2);
            html3 = _.template(insurance3);
            html4 = _.template(insurance4);
            html5 = _.template(insurance5);
            html6 = _.template(insurance6);
            html7 = _.template(insurance7);
            html8 = _.template(insurance8);
            html9 = _.template(insurance9);
            $("form").steps("insert", cind, {
                content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 1, {
                content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 2, {
                content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 3, {
                content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 4, {
                content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 5, {
                content: html6({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 6, {
                content: html7({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 7, {
                content: html8({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("insert", cind + 8, {
                content: html9({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("select").selectBoxIt();
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("form").steps('previous');
            $("select").selectBoxIt();
        }
        function appendDataPersonal(copynumber)
        {
            var plan1 = $('#personalDisabilityStep1').html();
            var plan2 = $('#personalDisabilityStep2').html();
            var plan3 = $('#personalDisabilityStep3').html();
            var plan4 = $('#personalDisabilityStep4').html();
            var plan5 = $('#personalDisabilityStep5').html();
            html1 = _.template(plan1);
            html2 = _.template(plan2);
            html3 = _.template(plan3);
            html4 = _.template(plan4);
            html5 = _.template(plan5);
            $("form").steps("add", {
                content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("add", {
                content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("add", {
                content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("add", {
                content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
            $("form").steps("add", {
                content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
            });
        }
        function removeSteps() {
            var copyind = form.steps('getCurrentIndex');
            var totalSteps = $('#services-question-form fieldset').length;
            if (totalSteps > copyind) {
                var len = totalSteps - copyind;
                for (var i = 1; i <= len; i++) {
                    var index = copyind + 1;
                    $("form").steps("remove", index);
                }
            }
        }

        $("select").selectBoxIt();
        if ($('.personal-payout-period').val() == '1') {
            $('.personal-number-of-months, .personal-number-of-months input').show();
            $('.personal-age, .personal-age input').hide();
        } else {
            $('.personal-age, .personal-age input').show();
            $('.personal-number-of-months, .personal-number-of-months input').hide();
        }


        $(document).on('click', '.type-disability', function () {
            $(this).addClass('selected').siblings().removeClass('selected');
            $(this).closest('.termLife').find('.disability-insurance').val($(this).attr('data-value'));
        });

        $(document).on('click', '.type-disability1', function () {
            $(this).addClass('selected').siblings().removeClass('selected');
            $(this).parent().find('.disability_type1').val($(this).attr('data-value'));
        });
        $(document).on('change', '.payout-period', function () {
            $("select").selectBoxIt();
            if ($(this).val() == '1') {
                $('.number-of-months, .number-of-months input').show();
                $('.age, .age input').hide();
            } else {
                $('.age, .age input').show();
                $('.number-of-months, .number-of-months input').hide();
            }
        });
        $(document).on('change', '.personal-payout-period', function () {
            if ($(this).val() == '1') {
                $('.number-of-months, .number-of-months input').show();
                $('.age, .age input').hide();
            } else {
                $('.age, .age input').show();
                $('.number-of-months, .number-of-months input').hide();
            }
        });
        $(document).on('change', '.other-group-disability-insurance', function () {
            var $this = $(this);
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').on('click', function (event) {
                if ($this.parent().find('.other-group-disability-insurance:checked').val() == 'yes') {
                    copynumber++;
                    var cind = form.steps('getCurrentIndex');
                    var insurance1 = $('#groupDisabilityStep1').html();
                    var insurance2 = $('#groupDisabilityStep2').html();
                    var insurance3 = $('#groupDisabilityStep3').html();
                    var insurance4 = $('#groupDisabilityStep4').html();
                    var insurance5 = $('#groupDisabilityStep5').html();
                    var insurance6 = $('#groupDisabilityStep6').html();
                    var insurance7 = $('#groupDisabilityStep7').html();
                    var insurance8 = $('#groupDisabilityStep8').html();
                    var insurance9 = $('#groupDisabilityStep9').html();
                    html1 = _.template(insurance1);
                    html2 = _.template(insurance2);
                    html3 = _.template(insurance3);
                    html4 = _.template(insurance4);
                    html5 = _.template(insurance5);
                    html6 = _.template(insurance6);
                    html7 = _.template(insurance7);
                    html8 = _.template(insurance8);
                    html9 = _.template(insurance9);
                    $("form").steps("insert", cind, {
                        content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 1, {
                        content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 2, {
                        content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 3, {
                        content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 4, {
                        content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 5, {
                        content: html6({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 6, {
                        content: html7({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 7, {
                        content: html8({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps("insert", cind + 8, {
                        content: html9({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
                    });
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $("form").steps('previous');
                    $(this).off(event);
                } else {
                    $("form").steps('previous');
                    var copyind = form.steps('getCurrentIndex');
                    var totalSteps = $("fieldset").length;
                    var size = copyind + 7;
                    if (totalSteps > size) {
                        var len = totalSteps - size;
                        for (var i = 1; i <= len; i++) {
                            var index = copyind + 1;
                            $("form").steps("remove", index);
                        }
                        var sub = len / 9;
                        copynumber -= sub;
                        $("form").steps('next');
                    } else {
                        $("form").steps('next');
                    }
                    $(this).off(event);
                }
            });
        });

//        Personal yes-no code
        $(document).on('change', '.personal-disability-insurance', function () {
            var $this = $(this);
            if ($this.val() == 'yes') {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                removeSteps();
            }
            $('a[href="#finish"]').on('click', function () {
                if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.personal-disability-insurance:checked').val() == 'yes') {
                    $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                    $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                    appendDataPersonal(copynumber);
                    copynumber++;
                    $(form).steps('next');
                } else {
                    $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                }
            });
        });
//        Personal other code
        $(document).on('change', '.other-personal-disability-insurance', function () {
            if ($(this).val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                removeSteps();
            }
            $('a[href="#finish"]').on('click', function () {
                if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-personal-disability-insurance:checked').val() == 'yes') {
                    $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                    $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                    copynumber++;
                    appendDataPersonal(copynumber);
                    $(form).steps('next');
                } else {
                    $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                }
            });
        });
        $('a[href="#next"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('.current').find('.personal-disability-insurance:checked').val() == 'no') {
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                removeSteps();
            } else {

            }

            if ($(this).parent().parent().parent().parent().children('.content').find('.current').find('.personal-disability-insurance:checked').val() == 'yes') {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }

        });
    });
</script>
@stop