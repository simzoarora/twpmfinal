<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Personal disability insurance.</h2>
        <p style="display:none;" class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','data[67][questionName]','Group disability insurance.') }}

        <div class="col-sm-6 inner-left ">

            <label for="label">Do you have any other personally owned disability insurance?</label>
            <label class="radio-custom-label">
                <input class="other-personal-disability-insurance" required="required" name="data[67][answer][other personal disability insurance]" type="radio" value="yes" aria-required="true">Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                <input class="other-personal-disability-insurance" required="required" name="data[67][answer][other personal disability insurance]" type="radio" value="no" aria-required="true">No 
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class=" hide-other-personal-insurance-info " style="display: none;">

            <div class="col-sm-6 inner-left ">
                <label for="label">Annual premium</label>
                <i class="fa fa-dollar" aria-hidden="true"></i>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[67][answer][Annual premium]" type="number" aria-required="true">
            </div>
            <div class="col-sm-6 inner-left ">
                <label for="label">Benefit amount </label>
                <i class="fa fa-dollar " aria-hidden="true"></i>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[67][answer][Annual premium]" type="number" aria-required="true">
            </div>
            <div class="col-sm-6 inner-left number-of-trustees">
                <label for="label">What type of disability insurance is it?</label>

                <div class="trustee termLife">
                    <a class="options disability-insurance-term ">
                        <div class="select-trustee">
                            <p>Short Term</p>    
                        </div>
                    </a>
                    <a class="options disability-insurance-term ">
                        <div class="select-trustee">
                            <p>Long Term</p>    
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 inner-left ">
                <label for="label">Elimination Period(How long until it starts paying after the onset of disability)</label>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="data[67][answer][Elimination Period]" type="number" aria-required="true">
            </div>
            <div class="alignment">
                <div class="col-sm-6 inner-left left-side ">

                    <label for="label">Payout period(how long the benefit will be paid)</label>
                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 12%; z-index: 1; top: 61px; font-size: 16px;"></i>
              <select style="position: absolute;" class="payout-period valid" name="data[67][answer][Citezenship status]" style="display: none;" required="" aria-required="true" aria-invalid="false"><option selected="" disabled="" value="">SELECT</option><option value="1">total # of months</option><option value="2">until certain age</option></select>
                </div>
                <div class="col-sm-6 inner-left  right-side number-of-months" style="display: none;">
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Total # of months" style="position:absolute; top:  46px;" name="data[67][answer][months]" type="text" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left  right-side age" style="display: none;">
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Age" style="position:absolute; top:  46px;" name="data[67][answer][age]" type="text" aria-required="true">
                </div>
            </div>
        </div>
    </div>
</div>