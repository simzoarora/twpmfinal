@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
@include('frontend.includes.contact')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',201) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12">
                                <div class="col-md-4 col-xs-11 section-left">

                                    <h2>Tell us about you and your family</h2>
                                    <p>Learning more about your family and dependents can help paint a more comprehensive financial picture.
                                        If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[43]','Tell us about you and your family') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Marital status') }}
                                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 27%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                        <!--{{ Form::select('data[43][answer][Marital status]', ['SELECT','Single','Married','Domestic Partner'],null,['class'=>'status'])}}-->
                                        <select style="position: absolute;" class="status valid select-marital" name="answer[43][marital_status]" required="" aria-invalid="false" style="display: none; position: absolute;"><option selected="" disabled="" value="">SELECT</option>
                                            <option selected="" disabled="" value="">Select</option>
                                            <option value="3" <?php if ($defaultData['taxFillingStatus'] == 1) echo "selected"; ?>>Single</option>
                                            <option value="2" <?php if ($defaultData['taxFillingStatus'] == 2) echo "selected"; ?>>Domestic Partner</option>
                                            <option value="1" <?php if (($defaultData['taxFillingStatus'] == 4)|| ($defaultData['taxFillingStatus'] == 3)|| ($defaultData['taxFillingStatus'] == 5)|| ($defaultData['taxFillingStatus'] == 6)) echo "selected"; ?>>Married</option>
                                        </select>
                                    </div> 
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Your age') }}
                                        <?php $dob = $defaultData['userData']['dob']; $diff = (date('Y') - date('Y', strtotime($dob))); ?> 
                                        {{ Form::input('number','answer[43][your_age]',$diff ,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','style'=>'pointer-events:none;']) }}
                                    </div>
                                    <div class="col-sm-8 inner-left partner-age" style="display: <?php
                                    if (array_key_exists('taxFillingStatus', $defaultData) && ($defaultData['taxFillingStatus'] == 4)) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        <label for="label">{{$spouse_name}}'s age </label>
                                        {{ Form::input('number','answer[43][spouse_partner_age]',(!empty($spouse_age)) ? $spouse_age  :'',['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0, 'max'=>150]) }}
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have children?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[43][have_children]', 'yes',(!empty($answer) && array_key_exists('have_children',$answer)) ? (($answer['have_children']=="yes")  ? true : false) :false ,['class'=>'children-confirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[43][have_children]', 'no',(!empty($answer) && array_key_exists('have_children',$answer)) ? (($answer['have_children']=="no")  ? true : false) :false ,['class'=>'children-confirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left children-details-table" style="display: <?php
                                    if (!empty($answer) && array_key_exists('have_children', $answer) && ($answer['have_children'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12  ">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <div class="row">
                                                        <div class="add-child">

                                                            <table id='student-info' class="find-table-length">
                                                                <thead>
                                                                    <tr>
                                                                        <td>Dependent</td>
                                                                        <td>Age</td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (!empty($answer["childs"])) {
                                                                        $childs = json_decode($answer["childs"]);
                                                                        if (!empty($childs)) {
                                                                            foreach ($childs as $key => $child) {
                                                                                ?>
                                                                                <tr class="children-info" data-index="{{$key}}">
                                                                                    <td class="child-name">{{$child->name}}</td>
                                                                                    <td class="child-age">{{$child->age}}</td>
                                                                                    <td style="text-align:right;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                                    <td class="child-need hidden">{{$child->needs}}</td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                            <div class="col-md-4 ">
                                                                <div class="row">
                                                                    <button type='button' class="add-dependent" data-toggle="modal" data-target="#childrenModal">Add child</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <textarea id="hiddenTextArea" class="hidden" name="answer[43][childs]">{{(!empty($answer["childs"]))? $answer["childs"]:null}}</textarea>

                            <div class="sections">
                                <div id="childrenModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        Modal content
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD CHILD/DEPENDENT</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class=" section-right">
                                                            {{ Form::input('hidden','questionName[43]','ADD CHILD/DEPENDENT') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Child/Dependent\'s name/nickname') }}
                                                                {{ Form::input('text','child_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control children-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'name']) }}
                                                            </div>


                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Child/Dependent\'s age') }}
                                                                {{ Form::input('number','child_age',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control children-age', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age','style'=>'text-align:left;']) }}
                                                            </div>



                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'WIll you be planning for thus child\'s educational expenses?') }}
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('child_educational_expenses', 'yes',false,['class'=>'educational-needs','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('child_educational_expenses', 'no',false,['class'=>'educational-needs','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                <button id='childrenForm' class="finishBtn" type="button" style="border-radius: 3px;
                                                                        color: #fff;
                                                                        font-family: Lato;
                                                                        font-size: 18px;
                                                                        line-height: 24px;
                                                                        text-align: center;
                                                                        width: 150px;
                                                                        padding: 12px 35px;
                                                                        background-color: #2179EE;
                                                                        text-decoration: none;
                                                                        border: none;
                                                                        margin-left: 62px;
                                                                        margin-bottom: 40px;">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->
                </div>
            </div>
            {{ Form::close() }}

        </div> 
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {
    $('.select-marital').prop('disabled', true);
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();

//    if ($('.status').val() === '1') {
//        $('.partner-age, .partner-age input').hide().val('');
//    } else {
//        $('.partner-age, .partner-age input').show();
//    }
    $("select").selectBoxIt();
    $('select.status').data('selectBox-selectBoxIt');


//    $(document).on('change', '.status', function () {
//
//        if ($(this).val() === '1') {
//            $('.partner-age, .partner-age input').hide().val('');
//        } else {
//            $('.partner-age, .partner-age input').show();
//        }
//    });

    var childrenList = [];
    if ($('#hiddenTextArea').val() != '') {
        childrenList = JSON.parse($('#hiddenTextArea').val());
    }

    $(document).on('change', '.children-confirmation', function () {

        if ($(this).val() == 'yes') {
            $('.children-details-table').show();
        } else {
            $('.children-details-table').hide();
            childrenList = [];
            $('#hiddenTextArea').html(JSON.stringify(childrenList));
        }
    });

    $('#childrenForm').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#childrenModal .children-name').val() && $('#childrenModal .children-age').val() && $('#childrenModal input[type="radio"]').is(':checked')) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        childrenList[$(this).attr('data-index')] = {
                            name: $('#childrenModal .children-name').val(),
                            age: $('#childrenModal .children-age').val(),
                            needs: $('#childrenModal .educational-needs:checked').val()
                        }
                        $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td class="child-name">' + $('#childrenModal .children-name').val() + '</td> <td class="child-age">' + $('#childrenModal .children-age').val() + '</td> <td style="text-align:right;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td><td class="child-need hidden">' + $('#childrenModal .educational-needs:checked').val() + '</td>');
                    } else {
                        $('#student-info tbody').append('<tr class="children-info" data-index=' + childrenList.length + '><td class="child-name">' + $('#childrenModal .children-name').val() + '</td> <td class="child-age">' + $('#childrenModal .children-age').val() + '</td> <td style="text-align:right;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td><td class="child-need hidden">' + $('#childrenModal .educational-needs:checked').val() + '</td></tr>');
                        childrenList.push({
                            name: $('#childrenModal .children-name').val(),
                            age: $('#childrenModal .children-age').val(),
                            needs: $('#childrenModal .educational-needs:checked').val()
                        });
                    }
                }
                $('#childrenModal').modal('hide');
                $('#hiddenTextArea').html(JSON.stringify(childrenList));
            }
        }
        return false;
    });

    $(document).on('click', '#student-info .fa-pencil', function () {
        $('#childrenModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        studentDetails = childrenList[index];
        $('#childrenModal .children-name').val(studentDetails.name);
        $('#childrenModal .children-age').val(studentDetails.age);
        $('#childrenModal .educational-needs[value=' + studentDetails.needs + ']').prop('checked', true);
        $('#childrenForm').addClass('edit-form');
        $('#childrenForm').attr('data-index', index);
    });

    $(document).on('click', '.add-dependent', function () {
        $('#childrenForm').removeClass('edit-form');
        $('#childrenModal input[type="text"], #childrenModal input[type="number"]').val('');
        $('#childrenModal input[type="radio"]').prop('checked', false);
    });

    $(document).on('click', '.fa-trash', function () { // <-- changes

        var index_id = $(this).closest('tr').attr('data-index');

        deleteRow(index_id);
        $('.swal-button--danger').click(function () {
            childrenList.splice(index_id, 1);

            $("#student-info tbody").empty();
            if (childrenList.length != 0) {
                var tr = '';
                $.each(childrenList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.name + '</td><td>' + value.age + '</td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#student-info tbody").html(tr);
            }

            $('#hiddenTextArea').html(JSON.stringify(childrenList));
            return false;
        });
    });

});

</script>
@stop