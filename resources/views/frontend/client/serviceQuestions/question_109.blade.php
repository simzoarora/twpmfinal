<form method="post">
    <div class="col-sm-12">
        <div class="col-md-4 col-xs-11 section-left">
            <p class="section-name"> ACCOUNT REGISTRATION </p>
            <h4> Name of Estate and Executors.</h4>
            <p>Enter the estate information as well as information of all executors. </p>
        </div>
        <div class="col-sm-6 col-sm-offset-1 section-right">
            {{ Form::input('hidden','data[109][questionName]','Name of Estate and Executors') }}

            <div class="col-sm-8 inner-left">
                {{ Form::label('label', 'What is the name of the estate?') }}
                {{ Form::input('text','data[109][answer][estatename]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control estate_name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'estate name ']) }}
            </div>

            <div class="col-sm-12 inner-left children-details-table" >
                {{ Form::label('label', 'List all executors.') }}
                <div class="col-sm-12  ">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="row">
                                <div class="add-child" style="padding-top:0px;">
                                    <table id='estate-info'>
                                        <thead>
                                            <tr> 
                                                <td>Executor name</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="children-info">
                                            </tr> 
                                        </tbody>
                                    </table> 
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <button type='button' class="add-dependent" data-toggle="modal" data-target="#executorModal">Add executor</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!----------- estate modal starts ------------>
    <div id="executorModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD EXECUTOR</h4>
                </div>
                <!-- steps modal body starts -->
                <div class="modal-body" style='border:none;'>

                    <!-- first section starts -->
                        <div class="row"> 
                    <div class=" setup-content" id="step-1">
                            <div class="section-right">
                                {{ Form::input('hidden','data[109][questionName]','ADD ACCOUNT OWNER') }}

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Full name') }}
                                    {{ Form::input('text','IM-Owner-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorFullName', 'data-rule-regex'=>"false", 'required'=>true,  'placeholder'=>'full name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Percent ownership (max 100%)') }}
                                    
                                    {{ Form::input('number','owner-percent',null,['data-validation'=>'' , 'class'=>'custom-validation form-control IMExecutorPercent', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'%']) }}
                                </div>


                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label','Date of Birth') }}
                                    {{ Form::input('text','owner-dob',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMExecutorDOB', 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'US Citizenship') }}
                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                    <!--{{ Form::select('data[109][answer][citizenship]', ['SELECT','US Citizen', 'US Resident Alien', 'Non-resident Alien'],null,['class'=>'citizenship']) }}-->
                                    <select style="position: absolute;" class="status valid IMExecutorCitizen" name="citizenship" aria-invalid="false" required="true"  style="display: none;">
                                        <option selected disabled="" value="0">SELECT</option>
                                        <option value="1">US Citizen</option>
                                        <option value="2">US Resident Alien</option>
                                        <option value="3">Non-resident Alien</option>
                                    </select>
                                </div>

                                <div class="col-sm-12 inner-left account-trustee-upload IMExecutorUploadForm" style="display: none;">
                                    {{ Form::label('label', 'Upload Form W-8') }}

                                    <div>
                                        <label for="investment-account-owner" class="btn IMExecutorUploadStatement IMExecutorUploadAccountStatement">Upload statement</label>
                                        <input id="investment-account-owner" class="account-trustee-upload IMExecutorUploadFormBox" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
                                    </div>
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20"> 
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>
                                </div> 

                            </div>
                        </div>
                    </div>

                    <!-- first step finish -->

                    <!-- second step starts -->
                    <div class=" setup-content" id="step-2"  style="display: none;">
                        <div class="row"> 
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[109][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Marital Status') }}
                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-one', 'Married', ['class' => 'label', 'for' => 'option-one'])}}
                                        {{ Form::radio('selector', 'married',null,['class'=>'IMExecutorMaritalStatus',  'id' => 'option-one', ' required'=>'required']) }}
                                    </div>

                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-two', 'Single', ['class' => 'label', 'for' => 'option-two'])}}
                                        {{ Form::radio('selector', 'single',null,['class'=>'IMExecutorMaritalStatus',  'id' => 'option-two', ' required'=>'required']) }}

                                    </div>
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'SSN or Tax ID') }}
                                    {{ Form::input('text','ssn-tax-id',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorSsnTaxId', 'data-rule-regex'=>"false",  'required'=>true,  'placeholder'=>'enter SSN or Tax ID']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Email Address') }}
                                    {{ Form::input('email','im-owner-email',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorEmailId', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'email@email.com']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Preferred phone contact number') }}
                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'mobile',null,['class'=>'IMExecutorPreferedPhoneContact',  'id' => 'mobile', ' required'=>'required']) }}
                                        {{Form::label('mobile', 'Mobile', ['class' => 'label', 'for' => 'mobile'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'home',null,['class'=>'IMExecutorPreferedPhoneContact',  'id' => 'home', ' required'=>'required']) }}
                                        {{Form::label('home', 'Home', ['class' => 'label', 'for' => 'home'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-4 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'work',null,['class'=>'IMExecutorPreferedPhoneContact',  'id' => 'work', ' required'=>'required']) }}
                                        {{Form::label('work', 'Work', ['class' => 'label', 'for' => 'work'])}}
                                    </div>

                                </div>
                                <div class="col-sm-9 inner-left"> 
                                    {{ Form::label('label', 'Phone number') }}
                                    {{ Form::input('number','im-phone-number',null,['data-validation'=> '' , 'required'=>true, 'class'=>'custom-validation form-control IMExecutorPhoneNumber', 'data-rule-regex'=>"false", 'placeholder'=>'XXX-XXX-XXXX']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- second step finsih -->
                    <!-- third step starts -->
                    <div class=" setup-content" id="step-3"  style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">

                                <!-- tabs pannel starts -->
                                <div class="mortgage-tabed mortgageSecondStep">
                                    <h2>Please provide one of the following forms of identification:</h2> 
                                    <ul class="mortgage-nav">
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#stateIssueLicense">State issued Driver's License</a></li>
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#passport">Passport</a></li>
                                    </ul>

                                    <div class="tab-content row">
                                        <!-- State issued Driver's License content starts --> 
                                        <div id="stateIssueLicense" class="tab-pane fade active in">
                                            <div class="col-sm-12 inner-left" >
                                                {{ Form::label('label', 'Driver license number') }}
                                                {{ Form::input('text','IMExecutor-license',null,['data-validation'=> '',  'class'=>'custom-validation form-control IMExecutorLicense', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'License number']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','IMOwnder-licence-expire-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMExecutorLicenseExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'State of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMExecutorLicenceIssuingState" name="states" required ="required" aria-invalid="false">
                                                    <option selected="" disabled="" value="">STATE</option>
                                                    <option value="1">a</option>
                                                    <option value="2">b</option>
                                                    <option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- State issued Driver's License content finish -->
                                        <!-- Passport tab content starts -->
                                        <div id="passport" class="tab-pane fade">
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Passport ID number') }}
                                                {{ Form::input('text','im-owner-passport',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorPassportNumber', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'Passport ID number']) }}
                                            </div>
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','passport-expiry-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMExecutorPassportExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'Country of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMExecutorPassportIssuanceCountry" name="passport-issue-country" aria-invalid="false" required="required"><option selected="" disabled="" value="">COUNTRY</option><option value="1">a</option><option value="2">b</option><option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- Passport tab content finish -->   
                                    </div>
                                    <!-- tab content finish -->
                                </div> 
                                <!-- tabs panel finish -->
                                {{ Form::input('hidden','data[109][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-9 mt-20">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- third step finish -->
                    <!-- fourth step starts -->
                    <div class=" setup-content" id="step-4" style="display: none">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[109][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', "Please provide this account owner's mailing address") }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 1') }}
                                    {{ Form::input('text','IM-owner-address-1',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorAddress1', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 2') }}
                                    {{ Form::input('text','IM-owner-address2',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorAddress2', 'required' => true,  'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'City') }}
                                    {{ Form::input('text','IM-ownerCity',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorCity', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'State') }}
                                    {{ Form::input('text','IM-owner-state',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorState', 'required' => true, 'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Zipcode') }}
                                    {{ Form::input('text','IM-owner-zipcode',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorZipCode', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-9 mt-20 paddingRight0">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fourth steps finish -->
                    <!-- fifth step starts -->
                    <div class=" setup-content" id="step-5" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[109][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Name of employer(if retired, enter most recent employer). ') }}
                                    {{ Form::input('text','IMowner-employer-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorEmployerName', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'employer name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Occupation') }}
                                    {{ Form::input('text','IM-owner-employer-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMExecutorEmployerOccupation', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'occupation']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button id='employerForm' class="finishBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Finish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fifth step finish -->
                </div>
                <!-- steps modal body finish -->

            </div>
        </div>
    </div>
</form>
<!----------- estate modal finish ------------>
@section('after-scripts')
<!--<script src="{{ asset('js/dashboard.js') }}"></script>-->
<script src="{{ asset('js/investment-management.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        
        $("select").selectBoxIt();

        $(document).on('change', '.status', function () {
            if ($(this).val() === '3') {
                $('.uploadform').show();
            } else {
                $('.uploadform').hide();
            }
            $("select").selectBoxIt();
        });

        // -------------------- vishal js starts 
        var executorList = [];
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#executorModal').modal('hide');
                        executorList[$(this).attr('data-index')] = {
                            IMExecutorFullName: $('#executorModal .IMExecutorFullName').val(),
                            IMExecutorPercent: $('#executorModal .IMExecutorPercent').val(),
                            IMExecutorDOB: $('#executorModal .IMExecutorDOB').val(),
                            IMExecutorCitizen: $('#executorModal .IMExecutorCitizen').val(),
                            upload: $('#executorModal .IMExecutorUploadFormBox').val(),
                            IMExecutorMaritalStatus: $('#executorModal .IMExecutorMaritalStatus:checked').val(),
                            IMExecutorSsnTaxId: $('#executorModal .IMExecutorSsnTaxId').val(),
                            IMExecutorEmailId: $('#executorModal .IMExecutorEmailId').val(),
                            IMExecutorPreferedPhoneContact: $('#executorModal .IMExecutorPreferedPhoneContact:checked').val(),
                            IMExecutorPhoneNumber: $('#executorModal .IMExecutorPhoneNumber').val(),
                            IMExecutorLicense: $('#executorModal .IMExecutorLicense').val(),
                            IMExecutorLicenseExpireDate: $('#executorModal .IMExecutorLicenseExpireDate').val(),
                            IMExecutorLicenceIssuingState: $('executorModal .IMExecutorLicenceIssuingState').val(),
                            IMExecutorPassportNumber: $('#executorModal .IMExecutorPassportNumber').val(),
                            IMExecutorPassportExpireDate: $('#executorModal .IMExecutorPassportExpireDate').data('date'),
                            IMExecutorPassportIssuanceCountry: $('#executorModal .IMExecutorPassportIssuanceCountry').val(),
                            IMExecutorAddress1: $('#executorModal .IMExecutorAddress1').val(),
                            IMExecutorAddress2: $('#executorModal .IMExecutorAddress1').val(),
                            IMExecutorCity: $('#executorModal .IMExecutorCity').val(),
                            IMExecutorState: $('#executorModal .IMExecutorState').val(),
                            IMExecutorZipCode: $('#executorModal .IMExecutorZipCode').val(),
                            IMExecutorEmployerName: $('#executorModal .IMExecutorEmployerName').val(),
                            IMExecutorEmployerOccupation: $('#executorModal .IMExecutorEmployerOccupation').val()
                        };
                        $('#estate-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#executorModal .IMExecutorFullName').val() + '</td>  <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                    } else {
                        $('#estate-info tbody').append('<tr data-index=' + executorList.length + '><td>' + $('#executorModal .IMExecutorFullName').val() + '</td>  <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                        executorList.push({
                            IMExecutorFullName: $('#executorModal .IMExecutorFullName').val(),
                            IMExecutorPercent: $('#executorModal .IMExecutorPercent').val(),
                            IMExecutorDOB: $('#executorModal .IMExecutorDOB').val(),
                            IMExecutorCitizen: $('#executorModal .IMExecutorCitizen').val(),
                            IMExecutorUploadDoc: $('#executorModal .IMExecutorUploadDoc').val(),
                            IMExecutorMaritalStatus: $('#executorModal .IMExecutorMaritalStatus:checked').val(),
                            IMExecutorSsnTaxId: $('#executorModal .IMExecutorSsnTaxId').val(),
                            IMExecutorEmailId: $('#executorModal .IMExecutorEmailId').val(),
                            IMExecutorPreferedPhoneContact: $('#executorModal .IMExecutorPreferedPhoneContact:checked').val(),
                            IMExecutorPhoneNumber: $('#executorModal .IMExecutorPhoneNumber').val(),
                            IMExecutorLicense: $('#executorModal .IMExecutorLicense').val(),
                            IMExecutorLicenseExpireDate: $('#executorModal .IMExecutorLicenseExpireDate').val(),
                            IMExecutorLicenceIssuingState: $('executorModal .IMExecutorLicenceIssuingState').val(),
                            IMExecutorPassportNumber: $('#executorModal .IMExecutorPassportNumber').val(),
                            IMExecutorPassportExpireDate: $('#executorModal .IMExecutorPassportExpireDate').data('date'),
                            IMExecutorPassportIssuanceCountry: $('#executorModal .IMExecutorPassportIssuanceCountry').val(),
                            IMExecutorAddress1: $('#executorModal .IMExecutorAddress1').val(),
                            IMExecutorAddress2: $('#executorModal .IMExecutorAddress1').val(),
                            IMExecutorCity: $('#executorModal .IMExecutorCity').val(),
                            IMExecutorState: $('#executorModal .IMExecutorState').val(),
                            IMExecutorZipCode: $('#executorModal .IMExecutorZipCode').val(),
                            IMExecutorEmployerName: $('#executorModal .IMExecutorEmployerName').val(),
                            IMExecutorEmployerOccupation: $('#executorModal .IMExecutorEmployerOccupation').val()
                        });
                        $('#executorModal').modal('hide');
                    }
                }

                $(document).on('click', '#estate-info .fa-pencil', function () {
                    $('#executorModal').modal();
                    $('#executorModal #step-1').show();
                    $('#executorModal #step-2, #executorModal #step-3, #executorModal #step-4, #executorModal #step-5').css('display', 'none');
                    $('#executorModal .inner-left input, #executorModal .inner-left select').parent().find('label.error').remove();
                    var index = $(this).closest('tr').attr('data-index');
                    executorListDetails = executorList[index];
                    loanTermSteps = executorListDetails[index];
                    $('#executorModal .IMExecutorFullName').val(executorListDetails.IMExecutorFullName);
                    $('#executorModal .IMExecutorPercent').val(executorListDetails.IMExecutorPercent);
                    $('#executorModal .IMExecutorDOB').val(executorListDetails.IMExecutorDOB);
                    $('#executorModal .IMExecutorCitizen').val(executorListDetails.IMExecutorCitizen).trigger('change');
                    $('#executorModal .IMExecutorUploadDoc').val(executorListDetails.IMExecutorUploadDoc);
                    $('#SemiAnnualCCModal .IMExecutorMaritalStatus[value=' + executorListDetails.IMExecutorMaritalStatus + ']').prop('checked', true);
                    $('#executorModal .IMExecutorSsnTaxId').val(executorListDetails.IMExecutorSsnTaxId);
                    $('#executorModal .IMExecutorEmailId').val(executorListDetails.IMExecutorEmailId);
                    $('#SemiAnnualCCModal .IMExecutorPreferedPhoneContact[value=' + executorListDetails.IMExecutorPreferedPhoneContact + ']').prop('checked', true);
                    $('#executorModal .IMExecutorPhoneNumber').val(executorListDetails.IMExecutorPhoneNumber);
                    $('#executorModal .IMExecutorLicense').val(executorListDetails.IMExecutorLicense);
                    $('#executorModal .IMExecutorLicenseExpireDate').val(executorListDetails.IMExecutorLicenseExpireDate);
                    $('#executorModal .IMExecutorLicenceIssuingState').val(executorListDetails.IMExecutorLicenceIssuingState);
                    $('#executorModal .IMExecutorPassportNumber').val(executorListDetails.IMExecutorPassportNumber);
                    $('#executorModal .IMExecutorPassportExpireDate').val(executorListDetails.IMExecutorPassportExpireDate);
                    $('#executorModal .IMExecutorPassportIssuanceCountry').val(executorListDetails.IMExecutorPassportIssuanceCountry);
                    $('#executorModal .IMExecutorAddress1').val(executorListDetails.IMExecutorAddress1);
                    $('#executorModal .IMExecutorAddress2').val(executorListDetails.IMExecutorAddress2);
                    $('#executorModal .IMExecutorCity').val(executorListDetails.IMExecutorCity);
                    $('#executorModal .IMExecutorState').val(executorListDetails.IMExecutorState);
                    $('#executorModal .IMExecutorZipCode').val(executorListDetails.IMExecutorZipCode);
                    $('#executorModal .IMExecutorEmployerName').val(executorListDetails.IMExecutorEmployerName);
                    $('#executorModal .IMExecutorEmployerOccupation').val(executorListDetails.IMExecutorEmployerOccupation);
                    $('#executorModal .mortgageSecondStep ul.mortgage-nav li.active a').text(executorListDetails.secondMortgageTab);
                    $('#employerForm').addClass('edit-form');
                    $('#employerForm').attr('data-index', index);
                    //   for third step fixed 15yr 30 yr and others
                    if (loanTermSteps) {
                        $('#executorModal .loan-li a:textEquals("' + loanTermSteps.loanTerm + '")').closest('.loan-li').addClass('active').siblings().removeClass('active');
                    } else {
                        $('#executorModal .loan-li.active').removeClass('active');
                    }
                });
            }
            return false;
        });

        $(document).on('click', '.add-dependent', function () {
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            $('#executorModal input[type=text], #executorModal input[type=number], #executorModal input[type=email]').val('');
            $('#employerForm').removeClass('edit-form');
            $('#executorModal input[type="radio"]').prop('checked', false);
            $('#executorModal .IMExecutorCitizen, #executorModal .IMExecutorLicenceIssuingState, #executorModal .IMExecutorPassportIssuanceCountry').val('').trigger('change');
            $('.OptionSelection').removeClass('married-selected');
            $('.mortgageSecondStep .mortgage-nav .secondloan-li').removeClass('active');
            $('#stateIssueLicense, #passport').removeClass('active in');
            $('#executorModal input[type="radio"]').prop('checked', false);
            $('.OptionSelection label').removeClass('married-selected');
            //        OptionSelection
        });

        $(document).on('change', '.IMExecutorMaritalStatus', function () {
            if ($(this).val() === 'married') {
                $('label[for=option-one]').addClass('married-selected');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }
        });

        if ($('.IMExecutorMaritalStatus:checked').val() === 'married') {
            $('label[for=option-one]').addClass('married-selected');
        } else {
            $('label[for=option-one]').removeClass('married-selected');
        }

        $(document).on('change', '.IMExecutorMaritalStatus', function () {
            if ($(this).val() === 'single') {
                $('label[for=option-two]').addClass('married-selected');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }
        });

        if ($('.IMExecutorMaritalStatus:checked').val() === 'single') {
            $('label[for=option-two]').addClass('married-selected');
        } else {
            $('label[for=option-two]').removeClass('married-selected');
        }

        $(document).on('change', '.IMExecutorPreferedPhoneContact', function () {
            if ($(this).val() === 'mobile') {
                $('label[for=mobile]').addClass('married-selected');
            } else {
                $('label[for=mobile]').removeClass('married-selected');
            }
        });

        if ($('.IMExecutorPreferedPhoneContact:checked').val() === 'mobile') {
            $('label[for=mobile]').addClass('married-selected');
        } else {
            $('label[for=mobile]').removeClass('married-selected');
        }

        $(document).on('change', '.IMExecutorPreferedPhoneContact', function () {
            if ($(this).val() === 'home') {
                $('label[for=home]').addClass('married-selected');
            } else {
                $('label[for=home]').removeClass('married-selected');
            }
        });

        if ($('.IMExecutorPreferedPhoneContact:checked').val() === 'home') {
            $('label[for=home]').addClass('married-selected');
        } else {
            $('label[for=home]').removeClass('married-selected');
        }

        $(document).on('change', '.IMExecutorPreferedPhoneContact', function () {
            if ($(this).val() === 'work') {
                $('label[for=work]').addClass('married-selected');
            } else {

                $('label[for=work]').removeClass('married-selected');
            }
        });

        if ($('.IMExecutorPreferedPhoneContact:checked').val() === 'work') {
            $('label[for=work]').addClass('married-selected');
        } else {
            $('label[for=work]').removeClass('married-selected');
        }

        $(document).on('change', '.status', function () {
            if ($(this).val() === '3') {
                $('.IMExecutorUploadForm').show();
            } else {
                $('.IMExecutorUploadForm').hide();
            }
            $("select").selectBoxIt();
        });

        //            delete row
        $('#estate-info').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
        });

        //--------------------- vishal js finsih 
    });
</script>

<style type="text/css">
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li a {float: left; color: #2179ee; margin-right: 12px; border: solid 2px #2179ee; border-radius: 4px; margin-right: 8px; text-align: center; min-height: 52px; padding: 11px 5px; width: calc(100% - 10px);
        font-size: 13px; font-weight: normal; line-height: normal;}
    .mortgage-nav{margin-bottom: 0} 
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li:last-child a {line-height: 32px;} 
</style>
@endsection