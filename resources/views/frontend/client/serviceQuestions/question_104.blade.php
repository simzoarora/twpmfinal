<div class="col-sm-12">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> ACCOUNT REGISTRATION </p>
        <h4> Select your account type. </h4>
        <p> Descriptive/explanatory statement </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        <div class="col-sm-12">
            <h5 class="right-head"> How would you categorize this account? </h5>
            <i class="i-head">Please select one</i>
        </div>

        <div class="acc-categories">
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Individual</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Joint</p>
                            <i>weights of Ownership</i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Joint</p>
                            <i>tenants by Enviesadgry</i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Community Property</p>
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
            <!--row2-->
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Tenants in Commom</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Partnership</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Proprietorship</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Corporation</p>
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
            <!--row3-->
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>LLC</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Unincorporated Association</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>UTMA/ UGMA</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Qualified Plan</p>
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
            <!--row4-->
            <div class="col-sm-12">
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Trust</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Estate</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Guardianship</p>
                            <i></i>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 acc-name ">
                    <div class="nested-div">
                        <div class="position-absolute">
                            <p>Other</p>
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

    </div>

</div>