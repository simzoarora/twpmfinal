<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>ADD CHILD/DEPENDENT</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
        {{ Form::input('hidden','data[29][questionName]','ADD CHILD/DEPENDENT') }}
        <div class="col-md-4 ">
            {{ Form::label('label', 'Child name/nickname') }}
            {{ Form::input('text','data[29][answer][child name][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'name']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('label', 'Child/Dependent\'s age') }}
            {{ Form::input('text','data[29][answer][child age][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age']) }}
        </div>
        <div class="col-md-4">
            {{ Form::label('label', 'Does this child/dependent have special needs?') }}
            {{ Form::radio('data[29][answer][child/dependent have special needs][0]', 'yes') }}Yes
            {{ Form::radio('data[29][answer][child/dependent have special needs][0]', 'no') }}No
        </div>
        {{ Form::submit('Save') }}
    </div>
</div>