<div class="col-sm-12 ">
    <div class="col-xs-12 col-sm-12  col-md-4 section-left">
        <h2>Preparing for your financial planning meeting.</h2>
        <p>Now that we have your personal information, Let's take a deeper look into your finances. Having this information helps us providing you with the best guidance and support possible.If you need help, simply <a href="#" class="contact-modal-show"> contact us</a> </p>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-1  section-right section-size">
        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8">  
                <h4>personal information</h4> 
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,1,47])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(1, $topicsInfo)) {
                        echo $topicsInfo[1];
                    }
                    ?></a>
            </div>
        </div>


        <!--        <div class="categories">
                    <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                        <h4>income tax information</h4>
                    </div>
                    <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                        <a>Revisit</a>
                    </div>
                </div>-->


        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>family information</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,3,49])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(3, $topicsInfo)) {
                        echo $topicsInfo[3];
                    }
                    ?></a>
            </div>
        </div>

        <?php // if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>charities</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,4,50])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(4, $topicsInfo)) {
                        echo $topicsInfo[4];
                    }
                    ?></a>
            </div> 
        </div>
        <?php // } ?>


        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>employement, income, retirement plans</h4>
                <p style="padding-bottom: 5px;"> {{session::get('loggedInUserName')}}</p>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p class="marital-section"> <?php echo $spouse_name; ?> </p>
                <?php } ?>
            </div>
            <div class="status status col-xs-4 col-sm-4 col-md-3 col-lg-3 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 dual-buttons"> 
                <span style="width:100%;margin-bottom: 17px;margin-top: 23px;display:inline-block;padding-bottom:9px;">
                    <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,5,'employment-income-retirement-preview'])}}"><?php
                        if (!empty($topicsInfo) && array_key_exists(5, $topicsInfo)) {
                            echo $topicsInfo[5];
                        }
                        ?></a>
                </span>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <span class="marital-section">
                        <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,13,'spouse-employment-income-retirement-preview'])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(13, $topicsInfo)) {
                                echo $topicsInfo[13];
                            }
                            ?></a>
                    </span>
                <?php } ?>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>insurance</h4> 
                <p style="padding-bottom: 5px;">{{session::get('loggedInUserName')}}</p>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p class="marital-section"><?php echo $spouse_name; ?> </p>
                <?php } ?>
            </div>
            <div class="status status col-xs-4 col-sm-4 col-md-3 col-lg-3 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 dual-buttons">
                <span style="margin-bottom: 17px ; float: right; margin-top: 23px;display:inline-block; padding-bottom: 9px; width: 100%; float: right; padding-bottom:9px; clear:both">
                    <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,6,'insurance-final-preview'])}}"><?php
                        if (!empty($topicsInfo) && array_key_exists(6, $topicsInfo)) {
                            echo $topicsInfo[6];
                        }
                        ?></a>
                </span>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <span style="clear:both; width: 100%; float: right;">
                        <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,14,'spouse-insurance-final-preview'])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(14, $topicsInfo)) {
                                echo $topicsInfo[14];
                            }
                            ?></a>
                    </span>
                <?php } ?>
            </div>
        </div>

        <div class="categories">                     
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>estate planning</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,7,52])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(7, $topicsInfo)) {
                        echo $topicsInfo[7];
                    }
                    ?></a>
            </div>
        </div>
        <div class="categories">    
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>assets</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(8, $topicsInfo)) {
                        echo $topicsInfo[8];
                    }
                    ?></a>
            </div>
        </div>

        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8">  
                <h4>liabilities</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(9, $topicsInfo)) {
                        echo $topicsInfo[9];
                    }
                    ?></a>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>Investment Experience</h4>
                <p style="padding-bottom: 5px;"> {{session::get('loggedInUserName')}}</p>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p class="marital-section"> <?php echo $spouse_name; ?> </p>
                <?php } ?>
            </div>
            <div class="status status col-xs-4 col-sm-4 col-md-3 col-lg-3 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1 dual-buttons">
                <span style="width:100%; margin-bottom: 17px;margin-top: 23px;display:inline-block;padding-bottom:9px;">
                    <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,10,'investment-experience-preview'])}}"><?php
                        if (!empty($topicsInfo) && array_key_exists(10, $topicsInfo)) {
                            echo $topicsInfo[10];
                        }
                        ?></a>
                </span>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <span>
                        <a href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,15,'spouse-investment-experience-preview'])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(15, $topicsInfo)) {
                                echo $topicsInfo[15];
                            }
                            ?></a>
                    </span>
                <?php } ?>
            </div>
        </div>

        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>income tax</h4>   
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,11,84])}}"><?php
                    if (!empty($topicsInfo) && array_key_exists(11, $topicsInfo)) {
                        echo $topicsInfo[11];
                    }
                    ?></a>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-xs-8 col-sm-8 col-md-8 col-lg-8"> 
                <h4>living expenses & savings</h4>
            </div>
            <div class="status col-xs-4 col-sm-4 col-md-4 col-lg-4">
                <a href="{{ route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,12,85]) }}"><?php
                    if (!empty($topicsInfo) && array_key_exists(12, $topicsInfo)) {
                        echo $topicsInfo[12];
                    }
                    ?></a>
            </div>
        </div>
        <div class="custon-continue-button">
            <?php // dd($topicsInfo); ?>
            <a  href="<?php
            if ($topicsInfo['serviceCompleted']) {
                echo route('frontend.client.selectedService', [config('constant.subdomain'), $currentService->id]);
            } else {
                echo'javascript:;';
            }
            ?>" class="<?php echo (!$topicsInfo['serviceCompleted']) ? 'toastrForService' : '' ?>">Continue</a>
        </div>
        <div class="custon-return">
            <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>
        </div>
    </div>
</div>

@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>

<script>

<?php if (session::get('tax_filing_status') == config('constant.tax_filing_status_inverse.single')) { ?>
    $('.marital-section').hide();
<?php } else { ?>
    $('.marital-section').show();
<?php } ?>
</script>

<style type="text/css">
    @media (max-width: 500px){
        .wrapper .sections .section-right .categories .status a{float:right; padding:4px 10px 5px 9px;}
    }

    @media (max-width: 900px){
        .wrapper .sections .section-right{width: 98% !important; max-width: 100%;}
    }
    .categories p, h4{float: left; width: 100%;}
</style>

@stop