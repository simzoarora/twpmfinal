@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',506) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio"> 
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Student Loan</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[14]','Do you, or your children or grandchildren have any student loans?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you, or your children or grandchildren have any student loans?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[14][DRSLModalConfirmation]', 'yes',(!empty($answer) && array_key_exists('DRSLModalConfirmation',$answer)) ? (($answer['DRSLModalConfirmation']=="yes")  ? true : false):false,['class'=>'DRSLModalConfirmation table-confirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[14][DRSLModalConfirmation]', 'no',(!empty($answer) && array_key_exists('DRSLModalConfirmation',$answer)) ? (($answer['DRSLModalConfirmation']=="no")  ? true : false):false,['class'=>'DRSLModalConfirmation table-confirmation', 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left DRSLModalDetailTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('DRSLModalConfirmation', $answer) && ($answer['DRSLModalConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h5> List all student loans relevant to your finances.</h5>

                                                        <table id='semiAnnuaSLInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Loan</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["studentLoanRecords"])) {
                                                                    $loans = json_decode($answer["studentLoanRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$data->DRSLModalLoanName}}</td>
                                                                                <td class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="DRSLBtn add-dependent" data-toggle="modal" data-target="#DRSLModal">Add Loan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="studentLoanDetails" class="hidden" name="answer[14][studentLoanRecords]">{{(!empty($answer["studentLoanRecords"]))? $answer["studentLoanRecords"]:null}}</textarea>
                                    <!-- info table finsih -->

                                </div>




                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="DRSLModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD STUDENT LOAN</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">

                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this loan a name?') }}
                                                                    {{ Form::input('text','HELOC Property Name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalLoanName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nick Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid DRSLModalNameLoaner" id="DRSLStudentLoanOthers"  name="semiAnnual loan name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>

                                                                        if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {

                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                                            <option value="2">{{$spouse_name}}</option>
                                                                        <?php } ?>
                                                                        <option value="3">Child</option>
                                                                        <option value="4">Grand Child</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left semiAnnual-otherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','DRSLModalLoanOtherName',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalLoanOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>    

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For whose education is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid DRSLEducationLoanOthers" id="DRSLEducationLoanOthers" name="education_loan" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="2">Same as above</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left semiAnnual-educationotherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','education',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalEducationOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn" type="button" >next<i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->


                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a Federal Loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-federal-loan', 'yes',false,['class'=>'DRSLModalFederaLLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-federal-loan', 'no',false,['class'=>'DRSLModalFederaLLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a private lender loan, i.e. a bank?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-private-lender-loan', 'yes',false,['class'=>'DRSLPrivateLenderConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-private-lender-loan', 'no',false,['class'=>'DRSLPrivateLenderConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'From whom do you receive the statement?') }}
                                                                    {{ Form::input('text','DRSL-receive_statement',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLReceiveStatement', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Navient, Bank, Salie Mae, etc.']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this consolidation loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-consolidation-loan', 'yes',null,['class'=>'DRSLConsolidationLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('DRSL-consolidation-loan', 'no',null,['class'=>'DRSLConsolidationLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left semiAnnual-consolidation-Datetimepicker" style="display: none;">
                                                                    {{ Form::label('label', 'When was it consolidated?') }}
                                                                    {{ Form::input('text','DRSL-consolidation-date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker DRSLConsolidationDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button class='DRSLBtn nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- second step finsih -->

                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan?') }}
                                                                    {{ Form::input('number','semiAnnualStudent',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalStudentLoanTerm', 'data-rule-regex'=>"false", 'required'=>true,'min'=>0 ]) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','semiAnnualStudentLoanInterestRate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DRSLModalInterestRate', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label>
                                                                        What is the minimum monthly payment? <i>Enter 0 if in deferment?</i>
                                                                    </label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semiAnnualStudentMinimumMonthlyPayment',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRSLModalMinimumMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button class='DRSLBtn nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- third step finish --> 
                                                        <!-- fourth step starts -->
                                                        <div class=" setup-content" id="step-4" style="display: none">
                                                            <div class=" section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is the borrower on an income-based repayment program?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'yes',null,['class'=>'DRSLModalincomeBasedRepayment','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'no',null,['class'=>'DRSLModalincomeBasedRepayment','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the occupation of the borrower?') }}
                                                                    {{ Form::input('text','borrower-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalBorrowerOccupation', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">  
                                                                    {{ Form::label('label', 'In what city and state does the borrower work?') }}
                                                                    {{ Form::input('text','borrower-work',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRSLModalBorrowerWork', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button id="DRSLBtnnew" class='DRSLBtn finishBtn pull-right' type="button">finish<i class="fa fa-arrow-right"></i></button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- fourth step finish --> 
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    var DRSLModalDetails = [];
    $('.nextBtn, .finishBtn').on('click', function () {

        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('.finishBtn').hasClass('edit-form')) {
                    $('#DRSLModal').modal('hide');
                    DRSLModalDetails[$(this).attr('data-index')] = {
                        DRSLModalLoanName: $('#DRSLModal .DRSLModalLoanName').val(),
                        DRSLModalNameLoaner: $('#DRSLModal .DRSLModalNameLoaner').val(),
                        DRSLModalLoanOtherName: $('#DRSLModal .DRSLModalLoanOtherName').val(),
                        DRSLEducationLoanOthers: $('#DRSLModal .DRSLEducationLoanOthers option:selected').val(),
                        DRSLModalEducationOtherName: $('#DRSLModal .DRSLModalEducationOtherName ').val(),

                        DRSLModalFederaLLoanConfirmation: $('#DRSLModal .DRSLModalFederaLLoanConfirmation:checked').val(),
                        DRSLPrivateLenderConfirmation: $('#DRSLModal .DRSLPrivateLenderConfirmation:checked').val(),
                        DRSLReceiveStatement: $('#DRSLModal .DRSLReceiveStatement').val(),
                        DRSLConsolidationLoanConfirmation: $('#DRSLModal .DRSLConsolidationLoanConfirmation:checked').val(),
                        DRSLConsolidationDate: $('#DRSLModal .DRSLConsolidationDate').val(),

                        DRSLModalStudentLoanTerm: $('#DRSLModal .DRSLModalStudentLoanTerm').val(),
                        DRSLModalInterestRate: $('#DRSLModal .DRSLModalInterestRate').val(),
                        DRSLModalMinimumMonthlyPayment: $('#DRSLModal .DRSLModalMinimumMonthlyPayment').val(),

                        DRSLModalincomeBasedRepayment: $('#DRSLModal .DRSLModalincomeBasedRepayment:checked').val(),
                        DRSLModalBorrowerOccupation: $('#DRSLModal .DRSLModalBorrowerOccupation').val(),
                        DRSLModalBorrowerWork: $('#DRSLModal .DRSLModalBorrowerWork').val()
                    }
                    $('#semiAnnuaSLInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#DRSLModal .DRSLModalLoanName').val() + '</td>  <td class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                } else {
                    $('#semiAnnuaSLInfo tbody').append('<tr data-index=' + DRSLModalDetails.length + '><td>' + $('#DRSLModal .DRSLModalLoanName').val() + '</td>   <td class="right-align"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    DRSLModalDetails.push({
                        DRSLModalLoanName: $('#DRSLModal .DRSLModalLoanName').val(),
                        DRSLModalNameLoaner: $('#DRSLModal .DRSLModalNameLoaner').val(),
                        DRSLModalLoanOtherName: $('#DRSLModal .DRSLModalLoanOtherName').val(),
                        DRSLEducationLoanOthers: $('#DRSLModal .DRSLEducationLoanOthers option:selected').val(),
                        DRSLModalEducationOtherName: $('#DRSLModal .DRSLModalEducationOtherName ').val(),

                        DRSLModalFederaLLoanConfirmation: $('#DRSLModal .DRSLModalFederaLLoanConfirmation:checked').val(),
                        DRSLPrivateLenderConfirmation: $('#DRSLModal .DRSLPrivateLenderConfirmation:checked').val(),
                        DRSLReceiveStatement: $('#DRSLModal .DRSLReceiveStatement').val(),
                        DRSLConsolidationLoanConfirmation: $('#DRSLModal .DRSLConsolidationLoanConfirmation:checked').val(),
                        DRSLConsolidationDate: $('#DRSLModal .DRSLConsolidationDate').val(),

                        DRSLModalStudentLoanTerm: $('#DRSLModal .DRSLModalStudentLoanTerm').val(),
                        DRSLModalInterestRate: $('#DRSLModal .DRSLModalInterestRate').val(),
                        DRSLModalMinimumMonthlyPayment: $('#DRSLModal .DRSLModalMinimumMonthlyPayment').val(),

                        DRSLModalincomeBasedRepayment: $('#DRSLModal .DRSLModalincomeBasedRepayment:checked').val(),
                        DRSLModalBorrowerOccupation: $('#DRSLModal .DRSLModalBorrowerOccupation').val(),
                        DRSLModalBorrowerWork: $('#DRSLModal .DRSLModalBorrowerWork').val()
                    });
                    $('#DRSLModal').modal('hide');
                }
                $('#studentLoanDetails').html(JSON.stringify(DRSLModalDetails));
            }
        }
        return false;
    });


    $(document).on('click', '#semiAnnuaSLInfo .fa-pencil', function () {
        $('#DRSLModal').modal();
        $('#DRSLModal #step-1').show();
        $('#DRSLModal #step-2, #DRSLModal #step-3, #DRSLModal #step-4').hide();
        $('#DRSLModal .modal-content .inner-left').find('label.error').remove();
        var index = $(this).closest('tr').attr('data-index');
        DRSLModalListDetails = DRSLModalDetails[index];
        $('#DRSLModal .DRSLModalLoanName').val(DRSLModalListDetails.DRSLModalLoanName);
        $('#DRSLModal .DRSLModalNameLoaner').val(DRSLModalListDetails.DRSLModalNameLoaner).trigger('change');
        $('#DRSLModal .DRSLModalLoanOtherName').val(DRSLModalListDetails.DRSLModalLoanOtherName);
        $('#DRSLModal .DRSLEducationLoanOthers').val(DRSLModalListDetails.DRSLEducationLoanOthers).trigger('change');
        $('#DRSLModal .DRSLModalEducationOtherName').val(DRSLModalListDetails.DRSLModalEducationOtherName);

        $('#DRSLModal .DRSLModalFederaLLoanConfirmation[value=' + DRSLModalListDetails.DRSLModalFederaLLoanConfirmation + ']').prop('checked', true);
        $('#DRSLModal .DRSLPrivateLenderConfirmation[value=' + DRSLModalListDetails.DRSLPrivateLenderConfirmation + ']').prop('checked', true);
        $('#DRSLModal .DRSLConsolidationLoanConfirmation[value=' + DRSLModalListDetails.DRSLConsolidationLoanConfirmation + ']').prop('checked', true);
        $('#DRSLModal .DRSLReceiveStatement').val(DRSLModalListDetails.DRSLReceiveStatement);
        $('#DRSLModal .DRSLConsolidationDate').val(DRSLModalListDetails.DRSLConsolidationDate);

        $('#DRSLModal .DRSLModalStudentLoanTerm').val(DRSLModalListDetails.DRSLModalStudentLoanTerm);
        $('#DRSLModal .DRSLModalInterestRate').val(DRSLModalListDetails.DRSLModalInterestRate);
        $('#DRSLModal .DRSLModalMinimumMonthlyPayment').val(DRSLModalListDetails.DRSLModalMinimumMonthlyPayment);

        $('#DRSLModal .DRSLModalincomeBasedRepayment[value=' + DRSLModalListDetails.DRSLModalincomeBasedRepayment + ']').prop('checked', true);
        $('#DRSLModal .DRSLModalBorrowerOccupation').val(DRSLModalListDetails.DRSLModalBorrowerOccupation);
        $('#DRSLModal .DRSLModalBorrowerWork').val(DRSLModalListDetails.DRSLModalBorrowerWork);

        $('#DRSLBtnnew').addClass('edit-form');
        $('#DRSLBtnnew').attr('data-index', index);

        if ($('.DRSLConsolidationLoanConfirmation:checked').val() === 'yes') {
            $('.semiAnnual-consolidation-Datetimepicker').show();
        } else {
            $('.semiAnnual-consolidation-Datetimepicker').hide();
        }

    });
    $(document).on('click', '.DRSLBtn', function () {
        $('#DRSLModal .modal-content .inner-left').find('label.error').remove();
        $('#DRSLModal #step-1').show();
        $('#DRSLModal #step-2, #DRSLModal #step-3, #DRSLModal #step-4').hide();
        $('#DRSLModal input[type="text"], input[type="number"]').val('');
        $('#DRSLModal input[type="radio"]').prop('checked', false);
        $('#DRSLModal select').val('').trigger('change');
        $('#DRSLBtnnew').removeClass('edit-form');
        $('.semiAnnual-consolidation-Datetimepicker').hide();

    });


    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });


    if ($('#studentLoanDetails').val() != '') {
        DRSLModalDetails = JSON.parse($('#studentLoanDetails').val());
    }

    $(document).on('change', '.DRSLModalConfirmation', function () {
        if ($(this).val() === 'yes') {
            $('.DRSLModalDetailTable').show();
        } else {
            $('.DRSLModalDetailTable').hide();
            DRSLModalDetails = [];
            $('#studentLoanDetails').html(JSON.stringify(DRSLModalDetails));
        }
    });

    $("#DRSLStudentLoanOthers").on('change', function () {
        if ($(this).val() === '5' || $(this).val() === '3' || $(this).val() === '4') {
            $('.semiAnnual-otherName').show();
        } else {
            $('.semiAnnual-otherName').hide();
        }
    });

    $("#DRSLEducationLoanOthers").on('change', function () {
        if ($(this).val() === '5') {
            $('.semiAnnual-educationotherName').show();
        } else {
            $('.semiAnnual-educationotherName').hide();
        }
    });

    $(document).on('change', '.DRSLConsolidationLoanConfirmation', function () {
        if ($(this).val() == 'yes') {
            $('.semiAnnual-consolidation-Datetimepicker').show();
        } else {
            $('.semiAnnual-consolidation-Datetimepicker').hide();
        }
    });

    $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
        if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
            $('.CashBackYesOption ').show();
        } else {
            $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
        }
    });

    $(document).on('change', '.semiAnnualexchange-point-cash', function () {
        if ($(this).val() == 'yes') {
            $('.exchangeforCasgYesOption ').show();
        } else {
            $('.exchangeforCasgYesOption').hide();
        }
    });


    $(document).on('click', '.fa-trash', function () { // <-- changes
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);
        $('.swal-button--danger').click(function () {

            DRSLModalDetails.splice(index_id, 1);
            $("#semiAnnuaSLInfo tbody").empty();
            if (DRSLModalDetails.length != 0) {
                var tr = '';
                $.each(DRSLModalDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.DRSLModalLoanName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#semiAnnuaSLInfo tbody").html(tr);
            }

            $('#studentLoanDetails').html(JSON.stringify(DRSLModalDetails));
            return false;
        });
    });


    $("#helocothers").on('change', function () {
        if ($(this).val() === '4') {
            $('.DRSLInterestRateOthers').show();
        } else {
            $('.DRSLInterestRateOthers').hide();
        }
    });
    $(document).on('click', 'DRSLBtn', function () { // <-- changes
        $('#DRSLModal').find('.error-alert').remove();
    });
});

</script>
@stop
