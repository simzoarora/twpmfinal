@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<style>
    .selected-option div { background-color: #2179ee; border: 1px solid #1098cc; color: #fff; }
</style>
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',10) }}
                    {{ Form::input('hidden','subTopicId',28) }}
                    {{ Form::input('hidden','redirectPageName','investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Options on stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[98]','Options on stocks') }}

                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you invest in options on stocks?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[98][invest_in_options_stocks]', 'yes',(!empty($answer)&& array_key_exists('invest_in_options_stocks',$answer)) ?(($answer['invest_in_options_stocks']=="yes")  ? true : false):false,['class'=>'options-stocks','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[98][invest_in_options_stocks]', 'no',(!empty($answer)&& array_key_exists('invest_in_options_stocks',$answer)) ?(($answer['invest_in_options_stocks']=="no")  ? true : false):false,['class'=>'options-stocks','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Options on stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[981]','Options on stocks') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','About how many trades do you make per year (or your advisor makes for you)?') }}
                                        {{ Form::input('number','answer[981][trades_per_year]',(!empty($answer) && array_key_exists('trades_per_year',$answer)) ? $answer['trades_per_year']:null,['data-validation'=> '' , 'style' => 'padding-right:35px !important;', 'class'=>'custom-validation form-control number-roller trades-per-year', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','About what is the average dollar amount on trades?') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[981][average_amount]',(!empty($answer) && array_key_exists('average_amount',$answer)) ? $answer['average_amount']:null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control average-amount', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Average number of contracts in a single trade?') }}
                                        {{ Form::input('number','answer[981][contracts]',(!empty($answer) && array_key_exists('contracts',$answer)) ? $answer['contracts']:null,['data-validation'=> '' , 'style' => 'padding-right:35px !important;', 'class'=>'custom-validation form-control contracts number-roller', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Options on stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[982]','Individual bonds') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','How many years have you been investing in options?') }}
                                        {{ Form::input('number','answer[982][investing_since]',(!empty($answer) && array_key_exists('investing_since',$answer)) ? $answer['investing_since']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control investing-since number-roller', 'data-rule-regex' =>"false", 'style' => 'padding-right:35px !important', 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','In what year did you begin investing in individual stocks?') }}
                                        {{ Form::input('number','answer[982][begin_investing]',(!empty($answer) && array_key_exists('begin_investing',$answer)) ? $answer['begin_investing']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control begin-investing', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','What kind of options trading do you do? (check all that apply) ') }}
                                        <div class="trading-types">
                                            <a class="option-trading <?php
                                            if (!empty($answer) && array_key_exists('cover_writing', $answer) && ($answer['cover_writing'] == 'cover')) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="cover">
                                                <div class="trading-options">
                                                    {{ Form::input('checkbox','answer[982][aaa]',(!empty($answer) && array_key_exists('cover_writing',$answer)) ? $answer['cover_writing']:null, ['id'=>'trading_option_cover'  , 'style'=>'display:none']) }}
                                                    <p style="padding-top: 14px;">Covered <br> writing</p>
                                                </div>
                                            </a>
                                            <a class="option-trading <?php
                                            if (!empty($answer) && array_key_exists('uncover_writing', $answer) && ($answer['uncover_writing'] == 'uncover')) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="uncover">
                                                <div class="trading-options">
                                                    {{ Form::input('checkbox','answer[982][uncover_writing]',(!empty($answer) && array_key_exists('uncover_writing',$answer)) ? $answer['uncover_writing']:null, ['id'=>'trading_option_uncover' , 'style'=>'display:none']) }}
                                                    <p style="padding-top: 14px;">Uncovered<br> writing</p>
                                                </div> 
                                            </a>
                                            <a class="option-trading <?php
                                            if (!empty($answer) && array_key_exists('trading_purchase', $answer) && ($answer['trading_purchase'] == 'purchase')) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="purchase">
                                                <div class="trading-options">
                                                    {{ Form::input('checkbox','answer[982][trading_purchase]',(!empty($answer) && array_key_exists('trading_purchase',$answer)) ? $answer['trading_purchase']:null, ['id'=>'trading_option_purchase'  , 'style'=>'display:none']) }}
                                                    <p style="padding-top: 21px;">Purchases</p>
                                                </div>
                                            </a >
                                            <a style="font-size: 12px;" class="option-trading <?php
                                            if (!empty($answer) && array_key_exists('spread_complex', $answer) && ($answer['spread_complex'] == 'strategy')) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="strategy">
                                                <div class="trading-options">
                                                    {{ Form::input('checkbox','answer[982][spread_complex]',(!empty($answer) && array_key_exists('spread_complex',$answer)) ? $answer['spread_complex']:null, ['id'=>'trading_option_strategy'  , 'style'=>'display:none']) }}
                                                    <p style="font-size: 12px; padding-top:3px;">Spreads,Straddles <br>and Complex <br> Strategies</p>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }} 
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
   var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,10,'investment-experience-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

//        $('.trading-options').on('click', function () {
//            $(this).toggleClass('selected-type');
//        });

        if ($('.options-stocks:checked').val() == 'no') {
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }

        $(document).on('change', '.options-stocks', function () {
            if ($(this).val() == 'no') {
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });

        $(document).on('click', '.option-trading', function () {
            $('#trading_option_' + $(this).attr('data-value')).val($(this).attr('data-value'));
            if ($(this).hasClass('selected-option')) {
                $('#trading_option_' + $(this).attr('data-value')).removeAttr('value');
            }
            $(this).toggleClass('selected-option');
        });

        $(".number-roller").spinner();

        $('.begin-investing').datetimepicker({
            format: 'YYYY'
        });

    });


</script>
@stop
