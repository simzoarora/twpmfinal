@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <div class="col-sm-12 recommended-service-div liabilities">
                        <!--<form id="services-question-form" class="col-sm-11 col-xs-11" action="">-->
                        <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                        {{ Form::input('hidden','serviceId',$currentService->id) }}-->

                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-12 section-left">
                                    <h6>Liabilities</h6>
                                    <h2>Let's talk about your liabilities</h2>
                                    <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply <a href="#" class="contact-modal-show"> contact us.</a> </p>
                                             <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">As user completes each section, direct them back to this main Liabilities screen, so they can select which section to work on next.</p>-->
                                             <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">User cannot submit info and proceed to payment/sc heduling until they have completed all sections.</p>-->
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 col-xs-12 section-right section-size">

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>Credit Score</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,501,2])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(501, $topicsInfo)) {
                                                    echo $topicsInfo[501];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>mortgage</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,502,3])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(502, $topicsInfo)) {
                                                    echo $topicsInfo[502];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>home equity line of credit</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,503,7])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(503, $topicsInfo)) {
                                                    echo $topicsInfo[503];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>vehicle loans</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,504,8])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(504, $topicsInfo)) {
                                                    echo $topicsInfo[504];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>credit cards</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,505,10])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(505, $topicsInfo)) {
                                                    echo $topicsInfo[505];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>student loans</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,506,14])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(506, $topicsInfo)) {
                                                    echo $topicsInfo[506];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>other debts (non-business)</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,507,17])}}"><?php
                                                if (!empty($topicsInfo) && array_key_exists(507, $topicsInfo)) {
                                                    echo $topicsInfo[507];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="custon-continue-button">
                                        <a href="<?php
                                        if ($topicsInfo['serviceCompleted']) {
                                            echo route('frontend.client.selectedService', [config('constant.subdomain'), $currentService->id]);
                                        } else {
                                            echo'javascript:;';
                                        }
                                        ?>" class="<?php echo (!$topicsInfo['serviceCompleted']) ? 'toastrForService' : '' ?>">Continue</a>
                                    </div>
                                    <div class="custon-return">
                                        <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!--</form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>

@stop

