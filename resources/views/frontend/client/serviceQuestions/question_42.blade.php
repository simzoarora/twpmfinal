@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
@include('frontend.includes.contact')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',202) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Tell us about you your income & investments.</h2>
                                    <p>This helps us understand your complete financial picture, so we can provide the best guidance.
                                        if you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                </div>
                                <?php $array = config('constant.tax_filing_status'); ?>
                                <div class="col-sm-6 col-sm-offset-1 section-right taxBracket">
                                    {{ Form::input('hidden','questionName[42]','Tell us about you your income & investments.') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Household gross income',['title'=>'Total income from all sources']) }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[42][household_gross_income]',isset($defaultData)? $defaultData['housesholdIncome']:'',['data-validation'=> '' , 'class'=>'custom-validation form-control taxable-income comprehensive-width borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$', 'style'=>'background: transparent' , 'readonly'=>'readonly']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Tax filling status') }}
                                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 27%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                        {{ Form::select('answer[42][tax_filling_status]', $array,isset($defaultData)? $defaultData['taxFillingStatus']:'', ['readonly'=>true, 'class'=>'tax_filling_status'])}} 
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Tax bracket',[' title'=>"Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income"]) }}
                                        {{ Form::input('text','answer[42][estimated_taxable_income]',(!empty($answer) && array_key_exists('estimated_taxable_income',$answer))? $answer['estimated_taxable_income']:'',['data-validation'=> '' , 'class'=>'custom-validation form-control tax-current-value', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'readonly'=>'readonly' , 'style'=>'background: transparent']) }}
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        <label>Total <b>non retirement</b> investment assets</label>
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[42][non_retirement_investment_assets]',(!empty($answer) && array_key_exists('non_retirement_investment_assets',$answer)) ? $answer['non_retirement_investment_assets'] :'',['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        <label>Total <b>retirement</b> accounts investment assets</label>
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[42][retirement]',(!empty($answer) && array_key_exists('retirement',$answer))? $answer['retirement']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control comprehensive-width borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->


                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div> 
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {
    $('.tax_filling_status').prop('disabled', true);

    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });

    addRemoveHref();


    $("select").selectBoxIt();

//      Fill tax bracket value according to income.        
    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";
    $(document).on('blur', '.taxable-income', function (event) {

        getTaxBracketValue()


    });
    getTaxBracketValue();

    function getTaxBracketValue() {

        var taxFilling = $('.taxBracket').find('input[name="answer[42][household_gross_income]"]').val();

        $.ajax({
            type: 'GET',
            url: ajaxUrl,
            data: {
                taxableIncome: taxFilling
            },
            success: function (resp) {
                $('.tax-current-value').val(resp);
            },
            error: function (error) {
//               twpmApp.ajaxInputError(error, $("#example-advanced-form"));
            }
        });
    }

});
</script>
@stop