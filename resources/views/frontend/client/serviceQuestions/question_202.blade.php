@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',802) }}
                    {{ Form::input('hidden','subTopicId',38) }}
                    {{ Form::input('hidden','redirectPageName','DebtLiabilities_review') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Student Loan</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[202]','Do you, or your children or grandchildren have any student loans?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you, or your children or grandchildren have any student loans?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[202][semiAnnualSLConfirmation]', 'yes',(!empty($answer) && array_key_exists('semiAnnualSLConfirmation',$answer)) ? (($answer['semiAnnualSLConfirmation']=="yes")  ? true : false):false,['class'=>'table-confirmation semiAnnualSLConfirmation' , 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[202][semiAnnualSLConfirmation]', 'no',(!empty($answer) && array_key_exists('semiAnnualSLConfirmation',$answer)) ? (($answer['semiAnnualSLConfirmation']=="no")  ? true : false):false,['class'=>'table-confirmation semiAnnualSLConfirmation' , 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left semiAnnualSLDetailTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('semiAnnualSLConfirmation', $answer) && ($answer['semiAnnualSLConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h5> List all student loans relevant to your finances.</h5>

                                                        <table id='semiAnnuaSLInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Loan</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["studentLoanRecords"])) {
                                                                    $loans = json_decode($answer["studentLoanRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$data->semiAnnualSLLoanName}}</td>
                                                                                <td></td> <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <button type='button' class="SemiAnnualSLBtn add-dependent" data-toggle="modal" data-target="#SemiAnnualSLModal">Add Loan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="studentLoanDetails" class="hidden" name="answer[202][studentLoanRecords]">{{(!empty($answer["studentLoanRecords"]))? $answer["studentLoanRecords"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>

                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="SemiAnnualSLModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD STUDENT LOAN</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">

                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this loan a name?') }}
                                                                    {{ Form::input('text','HELOC Property Name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualSLLoanName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nick Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid semiAnnualSLNameLoaner" id="SemiAnnualStudentLoanOthers"  name="semiAnnual loan name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <option value="2">{{$spouse_name}}</option>
                                                                        <option value="3">Child</option>
                                                                        <option value="4">Grand Child</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left semiAnnual-otherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','semiAnnualSLLoanOtherName',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualSLLoanOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>    

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For whose education is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid SemiAnnualEducationLoanOthersOptions" id="SemiAnnualEducationLoanOthers" name="education_loan" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="1">Same as above</option>
                                                                        <option value="2">Other</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left semiAnnual-educationotherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','education',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnual-educationotherName1', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn"  type="button" >next <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->


                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a Federal Loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-federal-loan', 'yes',false,['class'=>'semiAnnualSLFederaLLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-federal-loan', 'no',false,['class'=>'semiAnnualSLFederaLLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a private lender loan, i.e. a bank?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-private-lender-loan', 'yes',false,['class'=>'SemiAnnualSLPrivateLenderConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-private-lender-loan', 'no',false,['class'=>'SemiAnnualSLPrivateLenderConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'From whom do you receive the statement?') }}
                                                                    {{ Form::input('text','semi_annual_receive_statement',null,['data-validation'=> '' , 'class'=>'custom-validation form-control SemiAnnualSLReceiveStatement', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Navient, Bank, Salie Mae, etc.']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this consolidation loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('semi-annual-consolidation-loan', 'yes',null,['class'=>'SemiAnnualSLConsolidationLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('semi-annual-consolidation-loan', 'no',null,['class'=>'SemiAnnualSLConsolidationLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left semiAnnual-consolidation-Datetimepicker" style="display: none;">
                                                                    {{ Form::label('label', 'When was it consolidated?') }}
                                                                    {{ Form::input('text','semi-annual-consolidation-date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker SemiAnnualSLConsolidationDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i> back </button>
                                                                    <button class='SemiAnnualSLBtn nextBtn pull-right' type="button"> next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- second step finsih -->

                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan?') }}
                                                                    {{ Form::input('number','semiAnnualStudent',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualSLStudentLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','semiAnnualStudentLoanInterestRate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control semiAnnualSLInterestRate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label> What is the minimum monthly payment? <em>Enter 0 if in deferment? </em></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semiAnnualStudentMinimumMonthlyPayment',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualSLMinimumMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button class='SemiAnnualSLBtn nextBtn pull-right' type="button">next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- third step finish --> 
                                                        <!-- fourth step starts -->
                                                        <div class=" setup-content" id="step-4" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is the borrower on an income-based repayment program?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'yes',null,['class'=>'semiAnnualSLincomeBasedRepayment','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'no',null,['class'=>'semiAnnualSLincomeBasedRepayment','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the occupation of the borrower?') }}
                                                                    {{ Form::input('text','borrower-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualSLBorrowerOccupation', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In what city and state does the borrower work?') }}
                                                                    {{ Form::input('text','borrower-work',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualSLBorrowerWork', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button id="SemiAnnualSLBtnnew" class='SemiAnnualSLBtn finishBtn pull-right' type="button">  finish <i class="fa fa-arrow-right"></i> </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- fourth step finish --> 
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}">Save and return later</a>-->

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review\n\
'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {

            addRemoveHref();
        });
        addRemoveHref();
        $('.nextBtn, .finishBtn').on('click', function () {

            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#SemiAnnualSLModal .semiAnnualSLLoanName').val()) {
                        if ($('.finishBtn').hasClass('edit-form')) {
                            semiAnnualSLDetails[$(this).attr('data-index')] = {
                                semiAnnualSLLoanName: $('#SemiAnnualSLModal .semiAnnualSLLoanName').val(),
                                semiAnnualSLNameLoaner: $('#SemiAnnualSLModal .semiAnnualSLNameLoaner').val(),
                                semiAnnualSLLoanOtherName: $('#SemiAnnualSLModal .semiAnnualSLLoanOtherName').val(),
                                SemiAnnualEducationLoanOthersOptions: $('#SemiAnnualSLModal .SemiAnnualEducationLoanOthersOptions').val(),
                                semiAnnualEducationotherName: $('#SemiAnnualSLModal .semiAnnual-educationotherName1').val(),

                                semiAnnualSLFederaLLoanConfirmation: $('#SemiAnnualSLModal .semiAnnualSLFederaLLoanConfirmation:checked').val(),
                                SemiAnnualSLPrivateLenderConfirmation: $('#SemiAnnualSLModal .SemiAnnualSLPrivateLenderConfirmation:checked').val(),
                                SemiAnnualSLReceiveStatement: $('#SemiAnnualSLModal .SemiAnnualSLReceiveStatement').val(),
                                SemiAnnualSLConsolidationLoanConfirmation: $('#SemiAnnualSLModal .SemiAnnualSLConsolidationLoanConfirmation:checked').val(),
                                SemiAnnualSLConsolidationDate: $('#SemiAnnualSLModal .SemiAnnualSLConsolidationDate').val(),

                                semiAnnualSLStudentLoanTerm: $('#SemiAnnualSLModal .semiAnnualSLStudentLoanTerm').val(),
                                semiAnnualSLInterestRate: $('#SemiAnnualSLModal .semiAnnualSLInterestRate').val(),
                                semiAnnualSLMinimumMonthlyPayment: $('#SemiAnnualSLModal .semiAnnualSLMinimumMonthlyPayment').val(),

                                semiAnnualSLincomeBasedRepayment: $('#SemiAnnualSLModal .semiAnnualSLincomeBasedRepayment:checked').val(),
                                semiAnnualSLBorrowerOccupation: $('#SemiAnnualSLModal .semiAnnualSLBorrowerOccupation').val(),
                                semiAnnualSLBorrowerWork: $('#SemiAnnualSLModal .semiAnnualSLBorrowerWork').val()
                            }
                            $('#semiAnnuaSLInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#SemiAnnualSLModal .semiAnnualSLLoanName').val() + '</td>  <td></td> <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                        } else {
                            $('#semiAnnuaSLInfo tbody').append('<tr class="children-info" data-index=' + semiAnnualSLDetails.length + '><td>' + $('#SemiAnnualSLModal .semiAnnualSLLoanName').val() + '</td>   <td> </td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                            semiAnnualSLDetails.push({
                                semiAnnualSLLoanName: $('#SemiAnnualSLModal .semiAnnualSLLoanName').val(),
                                semiAnnualSLNameLoaner: $('#SemiAnnualSLModal .semiAnnualSLNameLoaner').val(),
                                semiAnnualSLLoanOtherName: $('#SemiAnnualSLModal .semiAnnualSLLoanOtherName').val(),
                                SemiAnnualEducationLoanOthersOptions: $('#SemiAnnualSLModal .SemiAnnualEducationLoanOthersOptions').val(),
                                semiAnnualEducationotherName: $('#SemiAnnualSLModal .semiAnnual-educationotherName1').val(),

                                semiAnnualSLFederaLLoanConfirmation: $('#SemiAnnualSLModal .semiAnnualSLFederaLLoanConfirmation:checked').val(),
                                SemiAnnualSLPrivateLenderConfirmation: $('#SemiAnnualSLModal .SemiAnnualSLPrivateLenderConfirmation:checked').val(),
                                SemiAnnualSLReceiveStatement: $('#SemiAnnualSLModal .SemiAnnualSLReceiveStatement').val(),
                                SemiAnnualSLConsolidationLoanConfirmation: $('#SemiAnnualSLModal .SemiAnnualSLConsolidationLoanConfirmation:checked').val(),
                                SemiAnnualSLConsolidationDate: $('#SemiAnnualSLModal .SemiAnnualSLConsolidationDate').val(),

                                semiAnnualSLStudentLoanTerm: $('#SemiAnnualSLModal .semiAnnualSLStudentLoanTerm').val(),
                                semiAnnualSLInterestRate: $('#SemiAnnualSLModal .semiAnnualSLInterestRate').val(),
                                semiAnnualSLMinimumMonthlyPayment: $('#SemiAnnualSLModal .semiAnnualSLMinimumMonthlyPayment').val(),

                                semiAnnualSLincomeBasedRepayment: $('#SemiAnnualSLModal .semiAnnualSLincomeBasedRepayment:checked').val(),
                                semiAnnualSLBorrowerOccupation: $('#SemiAnnualSLModal .semiAnnualSLBorrowerOccupation').val(),
                                semiAnnualSLBorrowerWork: $('#SemiAnnualSLModal .semiAnnualSLBorrowerWork').val()
                            });
                        }
                        $('#SemiAnnualSLModal').modal('hide');
                        $('#SemiAnnualSLModal .semiAnnualSLLoanName, #SemiAnnualSLModal .semiAnnualHELOCCreditAmount').val('');
                        $('#SemiAnnualSLModal .semiAnnualSLConfirmation').prop('checked', false);
                        $('#studentLoanDetails').html(JSON.stringify(semiAnnualSLDetails));
                    }
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        var semiAnnualSLDetails = [];
        if ($('#studentLoanDetails').val() != '') {
            semiAnnualSLDetails = JSON.parse($('#studentLoanDetails').val());
        }

        $(document).on('change', '.semiAnnualSLConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.semiAnnualSLDetailTable').show();
            } else {
                $('.semiAnnualSLDetailTable').hide();
                semiAnnualSLDetails = [];
                $('#studentLoanDetails').html(JSON.stringify(semiAnnualSLDetails));
                $('.semiAnnualSLDetailTable tbody tr').remove();
            }
        });

        if ($('#SemiAnnualStudentLoanOthers').val() === '5' || $(this).val() === '4' || $(this).val() === '3') {
            $('.semiAnnual-otherName').show();
        } else {
            $('.semiAnnual-otherName').hide();
        }


        $("#SemiAnnualStudentLoanOthers").on('change', function () {
            if ($(this).val() === '5' || $(this).val() === '4' || $(this).val() === '3') {
                $('.semiAnnual-otherName').show();
            } else {
                $('.semiAnnual-otherName').hide();
            }
        });


        if ($('#SemiAnnualEducationLoanOthers').val() === '2') {
            $('.semiAnnual-educationotherName').show();
        } else {
            $('.semiAnnual-educationotherName').hide();
        }

        $("#SemiAnnualEducationLoanOthers").on('change', function () {
            if ($(this).val() === '2') {
                $('.semiAnnual-educationotherName').show();
            } else {
                $('.semiAnnual-educationotherName').hide();
            }
        });

        $(document).on('change', '.SemiAnnualSLConsolidationLoanConfirmation', function () {
            if ($(this).val() == 'yes') {
                $('.semiAnnual-consolidation-Datetimepicker').show();
//                $('.SemiAnnualSLConsolidationDate').val('');
            } else {
                $('.semiAnnual-consolidation-Datetimepicker').hide();
            }
        });

        $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.semiAnnualexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('click', '#semiAnnuaSLInfo .fa-pencil', function () {
            $('#SemiAnnualSLModal').modal();
            $('#SemiAnnualSLModal #step-1').show();
            $('#SemiAnnualSLModal #step-2, #SemiAnnualSLModal #step-3, #SemiAnnualSLModal #step-4').css('display', 'none');
            $('#SemiAnnualSLModal .inner-left input, #SemiAnnualSLModal .inner-left select').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            semiAnnualSLListDetails = semiAnnualSLDetails[index];
            $('#SemiAnnualSLModal .semiAnnualSLLoanName').val(semiAnnualSLListDetails.semiAnnualSLLoanName);
            $('#SemiAnnualSLModal .semiAnnualSLNameLoaner').val(semiAnnualSLListDetails.semiAnnualSLNameLoaner).trigger('change');
            $('#SemiAnnualSLModal .semiAnnualSLLoanOtherName').val(semiAnnualSLListDetails.semiAnnualSLLoanOtherName);
            $('#SemiAnnualSLModal .SemiAnnualEducationLoanOthersOptions').val(semiAnnualSLListDetails.SemiAnnualEducationLoanOthersOptions).trigger('change');
            $('#SemiAnnualSLModal .semiAnnual-educationotherName1').val(semiAnnualSLListDetails.semiAnnualEducationotherName);

            $('#SemiAnnualSLModal .semiAnnualSLFederaLLoanConfirmation[value=' + semiAnnualSLListDetails.semiAnnualSLFederaLLoanConfirmation + ']').prop('checked', true);
            $('#SemiAnnualSLModal .SemiAnnualSLPrivateLenderConfirmation[value=' + semiAnnualSLListDetails.SemiAnnualSLPrivateLenderConfirmation + ']').prop('checked', true);
            $('#SemiAnnualSLModal .SemiAnnualSLConsolidationLoanConfirmation[value=' + semiAnnualSLListDetails.SemiAnnualSLConsolidationLoanConfirmation + ']').prop('checked', true);
            $('#SemiAnnualSLModal .SemiAnnualSLReceiveStatement').val(semiAnnualSLListDetails.SemiAnnualSLReceiveStatement);
            $('#SemiAnnualSLModal .SemiAnnualSLConsolidationDate').val(semiAnnualSLListDetails.SemiAnnualSLConsolidationDate);

            $('#SemiAnnualSLModal .semiAnnualSLStudentLoanTerm').val(semiAnnualSLListDetails.semiAnnualSLStudentLoanTerm);
            $('#SemiAnnualSLModal .semiAnnualSLInterestRate').val(semiAnnualSLListDetails.semiAnnualSLInterestRate);
            $('#SemiAnnualSLModal .semiAnnualSLMinimumMonthlyPayment').val(semiAnnualSLListDetails.semiAnnualSLMinimumMonthlyPayment);

            $('#SemiAnnualSLModal .semiAnnualSLincomeBasedRepayment[value=' + semiAnnualSLListDetails.semiAnnualSLincomeBasedRepayment + ']').prop('checked', true);
            $('#SemiAnnualSLModal .semiAnnualSLBorrowerOccupation').val(semiAnnualSLListDetails.semiAnnualSLBorrowerOccupation);
            $('#SemiAnnualSLModal .semiAnnualSLBorrowerWork').val(semiAnnualSLListDetails.semiAnnualSLBorrowerWork);

            $('#SemiAnnualSLBtnnew').addClass('edit-form');
            $('#SemiAnnualSLBtnnew').attr('data-index', index);

            if ($('.SemiAnnualSLConsolidationLoanConfirmation:checked').val() == 'yes') {
                $('.semiAnnual-consolidation-Datetimepicker').show();
//                $('.SemiAnnualSLConsolidationDate').val('');
            } else {
                $('.semiAnnual-consolidation-Datetimepicker').hide();
            }

        });

        $(document).on('click', '.SemiAnnualSLBtn', function () {
            $('#SemiAnnualSLBtnnew').removeClass('edit-form');
            $('#SemiAnnualSLModal input[type="text"], input[type="number"]').val('');
            $('#SemiAnnualSLModal input[type="radio"]').prop('checked', false);
            $('#SemiAnnualSLModal .semiAnnualSLNameLoaner, #SemiAnnualSLModal .SemiAnnualEducationLoanOthersOptions').val('').trigger('change');
            $('#SemiAnnualSLModal').find('.error-alert').remove();
            $('#SemiAnnualSLModal #step-1').show();
            $('#SemiAnnualSLModal #step-2, #SemiAnnualSLModal .semiAnnual-consolidation-Datetimepicker, #SemiAnnualSLModal #step-3, #SemiAnnualSLModal #step-4').hide();
        });

        $(document).on('click', '#semiAnnuaSLInfo .fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                semiAnnualSLDetails.splice(index_id, 1);

                $("#semiAnnuaSLInfo tbody").empty();
                if (semiAnnualSLDetails.length != 0) {
                    var tr = '';
                    $.each(semiAnnualSLDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.semiAnnualSLLoanName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#semiAnnuaSLInfo tbody").html(tr);
                }

                $('#studentLoanDetails').html(JSON.stringify(semiAnnualSLDetails));
                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });

    });

</script>
@stop
