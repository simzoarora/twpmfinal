<style>
    table {
        width:100%;
    } 
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    /*    table tbody td .fa-pencil {
            position: absolute;
            right: 17px;
            top: 20px;
        }*/
    .add-retirement-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .retirementInfo{ 
        position: relative;
    }
    .filename span{font-size: 14px !important}
</style> 
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',12) }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Monthly expenses</h2>
                                </div> 
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[85]','monthly expenses') }}
                                    <div class="col-sm-6 inner-left ">
                                        {{ Form::label('label', 'Approximately, how much to do spend each month on an average?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>      
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width " data-rule-regex="false" required="" placeholder="" name="answer[85][how_much_you_spend_each_month_average]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('how_much_you_spend_each_month_average', $answer)) {
                                                echo $answer["how_much_you_spend_each_month_average"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Budget</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[851]','budget') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have a budget?</label>
                                        <label class="radio-custom-label">
                                            <input class="budget" required="required" name="answer[851][have_a_budget]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('have_a_budget', $answer) && ($answer['have_a_budget'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="budget" required="required" name="answer[851][have_a_budget]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('have_a_budget', $answer) && ($answer['have_a_budget'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-12 inner-left budget-statement" style="display:<?php
                                    if (!empty($answer) && array_key_exists('have_a_budget', $answer) && ($answer['have_a_budget'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">


                                        <div class="col-sm-12 paddingLeft0 inner-left paddingRight0 budget-statement-list js">
                                            <label>Click below to upload your budget  (<i>optional</i> ).</label>
                                            <input type="file" name="budget_statement_info" id="file-21" class="inputfile inputfile-2" data-multiple-caption="{count} files selected" onclick="this.value=null;" multiple />
                                            <label for="file-21" class="upload-statment upload-statement-click <?php
                                            if (!empty($answer) && array_key_exists('upload_name', $answer) && $answer['upload_name'] != "") {
                                                echo "hide";
                                            }
                                            ?>" style="width: auto !important; font-size:13px; padding: 3px 20px; text-align:center; border-radius: 4px;  border:  1px solid #0991e1; width: 45% !important;">
                                                <span style="font-size: 0"></span>Upload
                                            </label>

                                            <label  class="upload-statment-noclick <?php
                                            if (!empty($answer) && array_key_exists('upload_name', $answer) && $answer['upload_name'] != "") {
                                                echo "";
                                            } else {
                                                echo "hide";
                                            }
                                            ?>" style="width: auto !important; font-size:13px; padding: 3px 20px; text-align:center; border-radius: 4px;  border:  1px solid #0991e1;  color:#0991e1; width: 45% !important; cursor: not-allowed;" >
                                                Upload
                                            </label>

                                            <div class="budget-information <?php
                                            if (!empty($answer) && array_key_exists('upload_name', $answer) && $answer['upload_name'] != "") {
                                                echo "";
                                            } else {
                                                echo "hide";
                                            }
                                            ?>">
                                                <div class="col-sm-12 row inner-left">
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td style="font-size: 13px;">Documents</td>
                                                                <td></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr style="border-bottom:0">
                                                                <td><span class="filename"><?php
                                                                        if (!empty($answer) && array_key_exists('upload_path', $answer))
                                                                            echo $answer['upload_name'];
                                                                        ?></span><input type="hidden" name="answer[851][upload_name]" class="uploadFileName" value="<?php
                                                                        if (!empty($answer) && array_key_exists('upload_name', $answer))
                                                                            echo $answer['upload_name'];
                                                                        ?>"><input type="hidden" name="answer[851][upload_path]" <?php
                                                                                      if (!empty($answer) && array_key_exists('upload_path', $answer))
                                                                                          echo $answer['upload_path'];
                                                                                      ?> class="uploadFilePath"></td></td>
                                                                <td style="text-align:right;"><i title="Delete" class="fa fa-trash cursor-pointer"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Annual savings goal</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[852]','budget') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have a target annual savings goal for taxable accounts?</label>
                                        <label class="radio-custom-label">
                                            <input class="annual-saving-target" required="required" name="answer[852][target_annual_savings_goal_taxable_accounts]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('target_annual_savings_goal_taxable_accounts', $answer) && ($answer['target_annual_savings_goal_taxable_accounts'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="annual-saving-target" required="required" name="answer[852][target_annual_savings_goal_taxable_accounts]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('target_annual_savings_goal_taxable_accounts', $answer) && ($answer['target_annual_savings_goal_taxable_accounts'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-6 inner-left annual-target" style="display:<?php
                                    if (!empty($answer) && array_key_exists('target_annual_savings_goal_taxable_accounts', $answer) && ($answer['target_annual_savings_goal_taxable_accounts'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'What is your annual target?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>   
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width " data-rule-regex="false" required="" placeholder="" name="answer[852][your_annual_target]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('your_annual_target', $answer)) {
                                                echo $answer["your_annual_target"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Deductible IRA's</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[853]','Deductible IRA\'s') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you contribute to a deductible IRA each year?</label>
                                        <label class="radio-custom-label">
                                            <input class="ira-contribution" required="required" name="answer[853][contribute_to_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribute_to_deductible_IRA_each_year', $answer) && ($answer['contribute_to_deductible_IRA_each_year'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="ira-contribution" required="required" name="answer[853][contribute_to_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribute_to_deductible_IRA_each_year', $answer) && ($answer['contribute_to_deductible_IRA_each_year'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left annual-contribution" style="display: <?php
                                    if (!empty($answer) && array_key_exists('contribute_to_deductible_IRA_each_year', $answer) && ($answer['contribute_to_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do you contribute each year?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>    
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[853][contribute_each_year]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('contribute_each_year', $answer)) {
                                                echo $answer["contribute_each_year"];
                                            }
                                            ?>">
                                        </div>
                                    </div>


                                    <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <div class="col-sm-6 inner-left ">
                                            <label for="label">Does your spouse/partner contribute to a deductible IRA each year?</label>
                                            <label class="radio-custom-label">
                                                <input class="spouse-ira-contribution" required="required" name="answer[853][spouse_partner_contribute_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_deductible_IRA_each_year'] == "yes")) {
                                                    echo "checked";
                                                }
                                                ?>>Yes
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">
                                                <input class="spouse-ira-contribution" required="required" name="answer[853][spouse_partner_contribute_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_deductible_IRA_each_year'] == "no")) {
                                                    echo "checked";
                                                }
                                                ?>>No
                                                <span class="radio-icon"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <div class="col-sm-6 inner-left spouse-annual-contribution" style="display: <?php
                                    if (!empty($answer) && array_key_exists('spouse_partner_contribute_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do they contribute?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>    
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[853][spouse_contribute_each_year]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('spouse_contribute_each_year', $answer)) {
                                                echo $answer["spouse_contribute_each_year"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Non-deductible IRA's</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[854]','Non-deductible IRA\'s') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you contribute to a non-deductible IRA each year?</label>
                                        <label class="radio-custom-label">
                                            <input class="non-deductible-ira-contribution" required="required" name="answer[854][non_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('non_deductible_IRA_each_year', $answer) && ($answer['non_deductible_IRA_each_year'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="non-deductible-ira-contribution" required="required" name="answer[854][non_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('non_deductible_IRA_each_year', $answer) && ($answer['non_deductible_IRA_each_year'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left non-deductible-annual-contribution" style="display: <?php
                                    if (!empty($answer) && array_key_exists('non_deductible_IRA_each_year', $answer) && ($answer['non_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do you contribute each year?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>    
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[854][contribute_to_a_non_deductible_IRA]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('contribute_to_a_non_deductible_IRA', $answer)) {
                                                echo $answer["contribute_to_a_non_deductible_IRA"];
                                            }
                                            ?>">
                                        </div>
                                    </div>

                                    <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <div class="col-sm-6 inner-left ">
                                            <label for="label">Does your spouse/partner contribute to a non-deductible IRA each year?</label>
                                            <label class="radio-custom-label">
                                                <input class="non-deductible-spouse-ira-contribution" required="required" name="answer[854][spouse_partner_contribute_to_a_non_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_to_a_non_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_to_a_non_deductible_IRA_each_year'] == "yes")) {
                                                    echo "checked";
                                                }
                                                ?>>Yes
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">
                                                <input class="non-deductible-spouse-ira-contribution" required="required" name="answer[854][spouse_partner_contribute_to_a_non_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_to_a_non_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_to_a_non_deductible_IRA_each_year'] == "no")) {
                                                    echo "checked";
                                                }
                                                ?>>No
                                                <span class="radio-icon"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <div class="col-sm-6 inner-left non-deductible-spouse-annual-contribution" style="display: <?php
                                    if (!empty($answer) && array_key_exists('spouse_partner_contribute_to_a_non_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_to_a_non_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do they contribute?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>    
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[854][spouse_contribute_to_a_non_deductible_IRA]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('spouse_contribute_to_a_non_deductible_IRA', $answer)) {
                                                echo $answer["spouse_contribute_to_a_non_deductible_IRA"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIVING EXPENSES & SAVINGS</h6>
                                    <h2>Roth IRA's</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[855]','Roth IRA\'s') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you contribute to a Roth IRA each year?</label>
                                        <label class="radio-custom-label">
                                            <input class="roth-ira-contribution" required="required" name="answer[855][contribute_to_roth_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribute_to_roth_deductible_IRA_each_year', $answer) && ($answer['contribute_to_roth_deductible_IRA_each_year'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="roth-ira-contribution" required="required" name="answer[855][contribute_to_roth_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribute_to_roth_deductible_IRA_each_year', $answer) && ($answer['contribute_to_roth_deductible_IRA_each_year'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left roth-annual-contribution" style="display: <?php
                                    if (!empty($answer) && array_key_exists('contribute_to_roth_deductible_IRA_each_year', $answer) && ($answer['contribute_to_roth_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do you contribute each year?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>  
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[855][contribute_roth_deductible_IRA_each_year_amount]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('contribute_roth_deductible_IRA_each_year_amount', $answer)) {
                                                echo $answer["contribute_roth_deductible_IRA_each_year_amount"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                    <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <div class="col-sm-6 inner-left ">
                                            <label for="label">Does your spouse/partner contribute to a Roth IRA each year?</label>
                                            <label class="radio-custom-label">
                                                <input class="roth-spouse-ira-contribution" required="required" name="answer[855][spouse_partner_contribute_roth_deductible_IRA_each_year]" type="radio" value="yes" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_roth_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_roth_deductible_IRA_each_year'] == "yes")) {
                                                    echo "checked";
                                                }
                                                ?>>Yes
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">
                                                <input class="roth-spouse-ira-contribution" required="required" name="answer[855][spouse_partner_contribute_roth_deductible_IRA_each_year]" type="radio" value="no" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_partner_contribute_roth_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_roth_deductible_IRA_each_year'] == "no")) {
                                                    echo "checked";
                                                }
                                                ?>>No
                                                <span class="radio-icon"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    <div class="col-sm-6 inner-left roth-spouse-annual-contribution" style="display:<?php
                                    if (!empty($answer) && array_key_exists('spouse_partner_contribute_roth_deductible_IRA_each_year', $answer) && ($answer['spouse_partner_contribute_roth_deductible_IRA_each_year'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'How much do they contribute?') }}
                                        <div class="input-group service-input-group ">
                                            <span class="input-group-addon">$</span>  
                                            <input data-validation="" class="custom-validation form-control borderLeft0 comprehensive-width" data-rule-regex="false" required="" placeholder="" name="answer[855][spouse_contribute_to_roth_deductible_IRA]" type="number" aria-required="true" value="<?php
                                                   if (!empty($answer) && array_key_exists('spouse_contribute_to_roth_deductible_IRA', $answer)) {
                                                       echo $answer["spouse_contribute_to_roth_deductible_IRA"];
                                                   }
                                                   ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id,])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        $(document).each(function () {
            $(document).on('change', '.budget-statement-list input[type=file]', function () {
                if ($('.budget-statement-list .upload-statment span').text().length > 0) {
                    $('.budget-statement-list .budget-information').removeClass('hide');
                    $('.budget-statement-list .budget-information table tbody').html('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="answer[851][upload_name]" class="uploadFileName"><input type="hidden" name="answer[851][upload_path]" class="uploadFilePath"></td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash cursor-pointer"></td></tr>');
                    $(".budget-information .filename").text($('.budget-statement-list .upload-statment span').text());
                    $(".budget-statement-list .upload-statement-click").addClass('hide');
                    $(".budget-statement-list .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var uploadStatementInfo = new FormData();
                    $('.uploadFileName').val($('.budget-statement-list .upload-statment span').text());
                    uploadStatementInfo.append('file', $('[name=budget_statement_info]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: uploadStatementInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".budget-information .uploadFilePath").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });
        $('.budget-statement-list table').on('click', '.fa-trash', function () {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $(this).closest('tr').remove();
                            $(".budget-information").addClass('hide');
                            $(".budget-statement-list .upload-statement-click").removeClass('hide');
                            $(".budget-statement-list .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
                            $('.budget-statement-list .upload-statment span').text('');

                            swal("Your file has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
        });

        'use strict';
        ;
        (function (document, window, index)
        {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener('focus', function () {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('has-focus');
                });
            });
        }(document, window, 0));



        $("select").selectBoxIt();
        $('.budget').on('change', function () {
            if ($(this).val() == 'yes') {

                $('.budget-statement').show();
                $('.budget-statement .budget-statement-list .upload-statement-click').removeClass('hide');
                $('.budget-statement .budget-statement-list .upload-statment-noclick').addClass('hide');
            } else {
                $('.budget-statement').hide();
                $('.budget-statement-list table tbody').find('tr').remove();

            }
        });
        $('.annual-saving-target').on('change', function () {
            if ($(this).val()) {
                $('.annual-target, .annual-target input').show();
            } else {
                $('.annual-target, .annual-target input').hide().val('');
            }
        });

        //------deductible IRA-------//
        $('.ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.annual-contribution, .annual-contribution input').show();
            } else {
                $('.annual-contribution, .annual-contribution input').hide().val('');
            }
        });
        $('.spouse-ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.spouse-annual-contribution, .spouse-annual-contribution input').show();
            } else {
                $('.spouse-annual-contribution, .spouse-annual-contribution input').hide().val('');
            }
        });

        //-------Non-deductible IRA-------//
        $('.non-deductible-ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.non-deductible-annual-contribution, .non-deductible-annual-contribution input').show();
            } else {
                $('.non-deductible-annual-contribution, .non-deductible-annual-contribution input').hide().val('');
            }
        });
        $('.non-deductible-spouse-ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.non-deductible-spouse-annual-contribution, .non-deductible-spouse-annual-contribution input').show();
            } else {
                $('.non-deductible-spouse-annual-contribution, .non-deductible-spouse-annual-contribution input').hide().val('');
            }
        });

        //-------Roth IRA-------//
        $('.roth-ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.roth-annual-contribution, .roth-annual-contribution input').show();
            } else {
                $('.roth-annual-contribution, .roth-annual-contribution input').hide();
            }
        });
        $('.roth-spouse-ira-contribution').on('change', function () {
            if ($(this).val()) {
                $('.roth-spouse-annual-contribution, .roth-spouse-annual-contribution input').show();
            } else {
                $('.roth-spouse-annual-contribution, .roth-spouse-annual-contribution input').hide();
            }
        });
    });
</script>
@stop

<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script> 