@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">

                    <!--{{ Form::open(['method' => 'get','url' => route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id]), 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3>
                    <section class="sections">
                        <div class="col-sm-12 ">
                            <div class="col-md-4 col-xs-11 section-left">
                                <h2>Let's talk about your Liabilities.</h2>
                                <p>Now that we have your personal information, Let's take a deeper look into your finances. Having this information helps us providing you with the best guidance and support possible.If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'> contact us</a> </p>

                            </div>
                            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>credit score</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,19,77])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(19, $subTopicInfo)) {
                                                echo $subTopicInfo[19];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>mortgage</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,20,78])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(20, $subTopicInfo)) {
                                                echo $subTopicInfo[20];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>home equity line of credit</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,21,79])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(21, $subTopicInfo)) {
                                                echo $subTopicInfo[21];
                                            }
                                            ?></a>
                                    </div>
                                </div>

                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>vehicle loans</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,22,80])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(22, $subTopicInfo)) {
                                                echo $subTopicInfo[22];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>credit cards</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,23,81])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(23, $subTopicInfo)) {
                                                echo $subTopicInfo[23];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>student loans</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,24,82])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(24, $subTopicInfo)) {
                                                echo $subTopicInfo[24];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="categories">
                                    <div class="headings col-sm-8">
                                        <h4>other debts (non-business)</h4>
                                    </div>
                                    <div class="status col-sm-4">
                                        <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,9,25,83])}}"><?php
                                            if (!empty($subTopicInfo) && array_key_exists(25, $subTopicInfo)) {
                                                echo $subTopicInfo[25];
                                            }
                                            ?></a>
                                    </div>
                                </div>
                                <div class="custon-continue-button">
                                    <a  href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Continue</a>
                                </div>
                                <div class="custon-return">
                                    <a href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--{{ Form::close() }}--> 
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
@stop