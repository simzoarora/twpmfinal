<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about <?php echo $spouse_name ;?></h6>
        <h2>Let's get more information about <?php echo $spouse_name ;?>.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[223]','Let\s get the rest of your personal information') }}

        <div style="clear:left;" class="col-sm-12">
            <label for="label">Does <?php echo $spouse_name ;?> have the same address? </label>
            <div class="inner-left" style="width:100% !important;">
                <label class="radio-custom-label">
                    <input class="spouse-address-confirmation " required="required" name="answer[223][spouse_same_address]" type="radio" value="yes" aria-required="true" <?php
                    if (!empty($answer) && array_key_exists('spouse_same_address', $answer) && ($answer['spouse_same_address'] == "yes")) {
                        echo "checked";
                    }
                    ?>>Yes
                    <span class="radio-icon"></span>
                </label>

                <label class="radio-custom-label">
                    <input class="spouse-address-confirmation " required="required" name="answer[223][spouse_same_address]" type="radio" value="no" aria-required="true" <?php
                    if (!empty($answer) && array_key_exists('spouse_same_address', $answer) && ($answer['spouse_same_address'] == "no")) {
                        echo "checked";
                    }
                    ?>>No
                    <span class="radio-icon"></span>
                </label>
            </div>
        </div>
        <!-- form show -->
        <div class="addres-section <?php
        if (!empty($answer) && array_key_exists('spouse_same_address', $answer) && ($answer['spouse_same_address'] == "no")) {
            echo 'show';
        } else {
            echo 'hide';
        }
        ?>">
            <div class="col-sm-6 left-side">
                <div class="inner-left ">
                    {{ Form::label('label', 'Address line 1') }}
                    {{ Form::input('text','answer[223][spouse_address_line_1]', (!empty($answer) && array_key_exists('spouse_address_line_1', $answer)) ? $answer['spouse_address_line_1']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'' ]) }}
                </div>
            </div>

            <div class="col-sm-8">
                <div class="col-sm-9 paddingLeft0 paddingRight0">
                    <div class="inner-left ">
                        {{ Form::label('label', 'Address line 2') }}
                        {{ Form::input('text','answer[223][spouse_address_line_2]', (!empty($answer) && array_key_exists('spouse_address_line_2', $answer)) ? $answer['spouse_address_line_2']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                    </div>
                </div>


            </div>

            <div class="col-sm-8">
                <div class="col-sm-9 paddingLeft0 paddingRight0 ">
                    <div class="inner-left ">
                        {{ Form::label('label', 'City') }}
                        {{ Form::input('text','answer[223][spouse_city]', (!empty($answer) && array_key_exists('spouse_city', $answer)) ? $answer['spouse_city']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                    </div>
                </div>
                <div class="col-sm-3 paddingLeft0 paddingRight0 select-spouse_state">
                    <div class="inner-left ">
                        {{ Form::label('label', 'State') }}
                        <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="top:45px! important; right: 5px! important;"></i>
                        <select style="position: absolute;" class="status valid" name="answer[223][spouse_state]" required="" aria-invalid="false" style="width:100% !important; max-width:100% !important;">
                            <option selected="" disabled="" value="">STATE</option>
                            @foreach ($states as $key=>$state)
                            <option value="{{$key}}" <?php
                            if (array_key_exists('spouse_state', $answer) && ($answer['spouse_state'] == $key ))
                                echo "selected";
                            ?> >{{$state}}</option>
                            @endforeach
                            
                            
                        </select>
                      <!--  {{ Form::select('answer[223][spouse_id]',$states,(!empty($answer) && array_key_exists('spouse_id', $answer)) ? $answer['spouse_id']:null,['class'=> 'form-control selectboxit']) }} -->

                    </div>
                </div>
            </div>

            <div class="col-sm-6" style="clear: left;">
                <div class="inner-left ">
                    {{ Form::label('label', 'Zip code') }}
                    {{ Form::input('text','answer[223][spouse_zipcode]', (!empty($answer) && array_key_exists('spouse_zipcode', $answer)) ? $answer['spouse_zipcode']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                </div>
            </div>
        </div>
        <!-- form hideen -->
    </div> 
</div>
