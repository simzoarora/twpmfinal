<div class="col-sm-12 ">   
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Let's get more information about {spouse/partner name}.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply contact us.</p>
    </div>
            
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','data[47][questionName]','Let\s get the rest of your personal information') }}
        <div class="alignment">
            <div class="col-sm-6 inner-left left-side ">

                {{ Form::label('label', 'First name') }}
               <!--<i class="fa fa-dollar" aria-hidden="true"></i>-->
                {{ Form::input('text','data[48][answer][first name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
            </div>
            <div class="col-sm-6 inner-left  right-side ">

                {{ Form::label('label', 'MI') }}
               <!--<i class="fa fa-dollar" aria-hidden="true"></i>-->
                {{ Form::input('text','data[48][answer][MI]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'' , 'style'=>'width:40%;']) }}
            </div>
        </div>
        <div class="alignment">
            <div class="col-sm-6 inner-left left-side ">

                {{ Form::label('label', 'Last name') }}
                {{ Form::input('text','data[48][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
            </div>
            <div class="col-sm-6 inner-left  right-side ">

                {{ Form::label('label', 'suffix') }}
                {{ Form::input('text','data[48][answer][suffix]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'' , 'style'=>'width:40%;']) }}
            </div>
        </div>

        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Date of birth') }}
            {{ Form::input('number','data[48][answer][dob]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>

        <div class="col-sm-6 inner-left">
            {{ Form::label('label', 'Gender') }}
            <label class="radio-custom-label" >
                {{ Form::radio('data[48][answer][gender]', 'female',false,['class'=>'gender' ,'required'=>'required']) }}Female
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('data[48][answer][gender]', 'male',false,['class'=>'gender' ,'required'=>'required']) }}Male 
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Last four digits of your Social Security Number') }}
            {{ Form::input('number','data[48][answer][dob]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'XXXX']) }}
        </div>
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Citizenship status') }}
            <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 15%; z-index: 1; top: 42px; font-size: 16px;"></i>
            <select style="position: absolute;" class="" name="data[48][answer][Citizenship status]" style="display: none;" required><option selected disabled value="">SELECT</option><option value="1">U.S. Citizen/National</option><option value="2">non-U.S. Citezen</option></select>
        </div>

        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Address Line 1') }}
            {{ Form::input('text','data[48][answer][address-1]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Address Line 2') }}
            {{ Form::input('text','data[48][answer][address-2]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>

        <div class="alignment">
            <div class="col-sm-6 inner-left left-side ">

                {{ Form::label('label', 'City') }}
                {{ Form::input('text','data[48][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
            </div>
            <div class="col-sm-6 inner-left  right-side ">

                {{ Form::label('label', 'State') }}
                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 15%; z-index: 1; top: 42px; font-size: 16px;"></i>
                <select style="position: absolute;" style='width:40%;' class="" name="data[48][answer][state]" style="display: none;" required><option selected disabled value="">SELECT</option><option value="1">single</option><option value="2">married</option><option value="3">domestic partner</option></select>
            </div>
        </div> 
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'ZIP') }}
            {{ Form::input('text','data[48][answer][zip code]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', '(name)\'s Phone number') }}
            {{ Form::input('text','data[48][answer][address-2]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', '(name)\'s email address') }}
            {{ Form::input('text','data[48][answer][address-2]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>

    </div>
</div>