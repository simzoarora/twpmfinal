<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> ACCOUNT SUMMARY </p>
        <h2>Financial Management Summary.</h2>
        <p>You can review and/or edit any of the sections before submission. You are satisfied with everything,you can submit for review.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">

        <div class="categories">
            <div class="headings col-sm-8">
                <h4>SELECT ACCOUNT TYPE</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>
        <div class="categories"> 
            <div class="headings col-sm-8">
                <h4>ACCOUNT OWNER INFORMATION</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>

        <div class="categories">
            <div class="headings col-sm-8">
                <h4>COMBINED INCOME/NET WORTH</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>

        <div class="categories">
            <div class="headings col-sm-8">
                <h4>TAX EXEMPTIONS</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-sm-8">
                <h4>INVESTMENT POLICY STATEMENT</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-sm-8">
                <h4>DEPOSITS</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>
        <div class="categories">
            <div class="headings col-sm-8">
                <h4>OTHER ACCOUNTS</h4>
            </div>
            <div class="status col-sm-4">
                <a>Revisit</a>
            </div>
        </div>
    </div>
</div>
