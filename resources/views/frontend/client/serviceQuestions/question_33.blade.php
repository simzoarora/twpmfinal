@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',303) }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIABILITIES</h6>
                                    <h2>Tell us about your liabilities/debts.</h2>
                                    <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="#" class="contact-modal-show">contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[33]','Tell us about your liabilities/debts') }}
                                    <div class="col-sm-8 inner-left total-mortgage" style="display: <?php
                                    if (!empty($answer) && array_key_exists('own', $answer) && ($answer['own'] == "own")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'Estimated total mortgages.') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">$</span>
                                            {{ Form::input('number','answer[33][estimated_total_mortgages]',(!empty($answer) && array_key_exists('estimated_total_mortgages',$answer)) ? $answer['estimated_total_mortgages'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left total-other-debts">
                                        {{ Form::label('label', 'Estimated total other debts(this may include credit cards,auto loans,student loans,private loans, etc.)') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">$</span>
                                            {{ Form::input('number','answer[33][estimated_total_other_debts]',(!empty($answer) && array_key_exists('estimated_total_other_debts',$answer)) ? $answer['estimated_total_other_debts'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/life-insurance.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

    });
</script>
@stop