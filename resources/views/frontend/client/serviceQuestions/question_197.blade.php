@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',15) }}
                    {{ Form::input('hidden','subTopicId',32) }}
                    {{ Form::input('hidden','redirectPageName','spouse-investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Margin</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[197]','Margin') }}
                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you, or have you, used margin for trading?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[197][margin]', 'yes',(!empty($answer)&& array_key_exists('margin',$answer)) ?(($answer['margin']=="yes")  ? true : false):false,['class'=>'margin','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[197][margin]', 'no',(!empty($answer)&& array_key_exists('margin',$answer)) ?(($answer['margin']=="no")  ? true : false):false,['class'=>'margin','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left margin-input" style="display:<?php
                                         if (!empty($answer) && array_key_exists('margin', $answer) && ($answer['margin'] == "yes")) {
                                             echo 'block';
                                         } else {
                                             echo 'none';
                                         }
                                         ?>">
                                        {{ Form::label('label','When did you begin using margin?') }}
                                        {{ Form::input('number','answer[197][using_margin]',(!empty($answer) && array_key_exists('using_margin',$answer)) ? $answer['using_margin']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control using-margin', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,15,'spouse-investment-experience-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        if ($('.margin:checked').val() == 'yes') {
            $('.margin-input').show();
            $('.margin-input input').show();
        } else {
            $('.margin-input').hide();
            $('.margin-input input').hide().val('');
        }
        $('.using-margin').datetimepicker({
            format: 'YYYY'
        });
        $(document).on('click', '.margin', function () {
            if ($(this).val() == 'yes') {
                $('.margin-input').show();
                $('.margin-input input').show();
            } else {
                $('.margin-input').hide();
                $('.margin-input input').hide().val('');
            }
        });
    });
</script>
@stop
