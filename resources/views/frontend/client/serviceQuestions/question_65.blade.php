<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Group disability insurance.</h2>
        <p style="display:none;" class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','data[65][questionName]','Group disability insurance.') }}

        <div class="col-sm-6 inner-left ">

            <label for="label">Does your employer offer another group disability insurance? (For example, an employer might sponsor a short-term and also a long-term policy)</label>
            <label class="radio-custom-label">
                <input class="other-group-disability-insurance" required="required" name="data[65][answer][group disability insurance]" type="radio" value="yes" aria-required="true">Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                <input class="other-group-disability-insurance" required="required" name="data[65][answer][group disability insurance]" type="radio" value="no" aria-required="true">No 
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class=" hide-other-disability-info " style="display: none;">

            <div class="col-sm-6 inner-left ">
                <label for="label">What is the tax status of premiums? (ask HR if unsure, as this is vitally important)</label>
                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 12%; z-index: 1; top: 61px; font-size: 16px;"></i>
                <select style="position: absolute;" class="" name="data[65][answer][Citezenship status]" style="display: none;" required="" aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1">before tax</option><option value="2">after tax</option></select>
            </div>
            <div class="col-sm-6 inner-left ">
                <label for="label">Annual premium</label>
                <i class="fa fa-dollar" aria-hidden="true"></i>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[65][answer][Annual premium]" type="number" aria-required="true">
            </div>
            <div class="col-sm-6 inner-left ">
                <label for="label">Benefit amount (Please provide both dollar amount and % of salary)</label>
                <i class="fa fa-dollar fa-dollar-bottom" aria-hidden="true"></i>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[65][answer][Annual premium]" type="number" aria-required="true">
                <!--<i class="fa fa-percent" aria-hidden="true"></i>-->
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" style="margin-top:15px;" name="data[65][answer][Annual premium]" type="number" aria-required="true">
            </div>
            <div class="col-sm-6 inner-left number-of-trustees">
                <label for="label">What type of disability insurance is it?</label>

                <div class="trustee termLife">
                    <a class="options disability-insurance-term ">
                        <div class="select-trustee">
                            <p>Short Term</p>    
                        </div>
                    </a>
                    <a class="options disability-insurance-term ">
                        <div class="select-trustee">
                            <p>Long Term</p>    
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-sm-6 inner-left ">
                <label for="label">Elimination Period(How long until it starts paying after the onset of disability)</label>
                <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="data[65][answer][Elimination Period]" type="number" aria-required="true">
            </div>
            <div class="alignment">
                <div class="col-sm-6 inner-left left-side ">

                    <label for="label">Payout period(how long the benefit will be paid)</label>
                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 12%; z-index: 1; top: 61px; font-size: 16px;"></i>
              <select style="position: absolute;" class="payout-period valid" name="data[65][answer][Citezenship status]" style="display: none;" required="" aria-required="true" aria-invalid="false"><option selected="" disabled="" value="">SELECT</option><option value="1">total # of months</option><option value="2">until certain age</option></select>
                </div>
                <div class="col-sm-6 inner-left  right-side number-of-months" style="display: none;">
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Total # of months" style="position:absolute; top:  46px;" name="data[65][answer][months]" type="text" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left  right-side age" style="">
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Age" style="position:absolute; top:  46px;" name="data[65][answer][age]" type="text" aria-required="true">
                </div>
            </div>
        </div>
    </div>
</div>