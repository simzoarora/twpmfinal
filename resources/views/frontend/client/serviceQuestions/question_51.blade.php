<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Tell us about you and your family</h2>
        <p>Learning more about your family and dependents can help paint a more comprehensive financial picture.
            If you need help, simply <a href="">Contact Us</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','data[51][questionName]','Tell us about you and your family') }}
        <div class="col-sm-8 inner-left partner-age" >
            {{ Form::label('label', 'Job title or description') }}
            {{ Form::input('number','data[51][answer][job title]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control job-title', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
        <div class="col-sm-8 inner-left partner-age" >
            {{ Form::label('label', 'Annual income, including bonus') }}
            {{ Form::input('number','data[51][answer][job title]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control job-title', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
        <div class="col-sm-12 inner-left children-details-table"  >
            <div class="col-sm-12 ">
                <div class="row">
                    <div class="add-child">
                        {{ Form::label('label', 'Annual Non-employment reccuring income from non-retirement accounts') }}

                        <table id='income-info'>
                            <thead>
                                <tr> 
                                    <td>Income type</td>
                                    <td>Total amount</td>
                                    <td>Notes</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="children-info">
                                </tr> 
                            </tbody>
                        </table> 
                        <div class="col-md-4 ">
                            <div class="row">
                                <button type='button' class="add-income add-dependent" data-toggle="modal" data-target="#incomeModal">Add income</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="sections">
    <div id="incomeModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD INCOME</h4>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 section-right">
                            {{ Form::input('hidden','data[51][questionName]','ADD INCOME') }}


                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Type of income') }}
                                <select style="position: absolute;" class="income-type" name="data[51][answer][Marital status]" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Interest</option><option value="2">Dividends</option><option value="3">Royalties</option><option value="4">Net business income</option><option value="5">Retirement account withdrawals</option><option value="6">Other</option></select>
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::input('text','data[51][answer][description]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control description', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'description']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Total amount') }}
                                {{ Form::input('text','data[51][answer][total amount]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control total-amount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Additional notes') }}
                                {{ Form::input('text','data[51][answer][additional notes]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control additional-notes', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='incomeForm' type="button" style="border-radius: 3px;
                                        color: #fff;
                                        font-family: Lato;
                                        font-size: 18px;
                                        line-height: 24px;
                                        text-align: center;
                                        width: 150px;
                                        padding: 12px 35px;
                                        background-color: #2179EE;
                                        text-decoration: none;
                                        border: none;
                                        margin-left: 62px;
                                        margin-bottom: 40px;">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


</script>
    
