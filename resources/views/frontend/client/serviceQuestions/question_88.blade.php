
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',5) }}
                    {{ Form::input('hidden','subTopicId',3) }}
                    {{ Form::input('hidden','redirectPageName','employment-income-retirement-preview') }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                    <h2>Defined contribution retirement plan</h2>
                                    <p>You may know this as a 401(k), 403(b), etc.
                                        <?php
                                        if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                            ?>
                                            We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[88]','Defined contribution retirement plan') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Does your employer offer a defined contribution retirement plan?</label>
                                        <label class="radio-custom-label">
                                            <input class="defined-contribution-retirement" required="required" name="answer[88][contribution_retirement_plan]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribution_retirement_plan', $answer) && ($answer['contribution_retirement_plan'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="defined-contribution-retirement" required="required" name="answer[88][contribution_retirement_plan]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('contribution_retirement_plan', $answer) && ($answer['contribution_retirement_plan'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <?php
                    if (!empty($answer) && !empty($answer["copy"])) {
                        foreach ($answer["copy"] as $key => $data) {
                            ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2>Defined contribution retirement plan</h2>
                                            <p>
                                                You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <input name="questionName[88]" type="hidden" value="Defined contribution retirement plan">
                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Type of retirement plan</label>
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 48px; z-index:0; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" style="position: absolute;"  class="income-type valid" name="answer[88][copy][<?php echo $key; ?>][type_of_retirement_plan_<?php echo $key; ?>]" required="" aria-invalid="false" aria-required="true" style="display: none;"><option selected="" disabled="" value="">SELECT</option>
                                                    <option value="1"  <?php
                                                    if (array_key_exists('type_of_retirement_plan_' . $key, $data) && ($data['type_of_retirement_plan_' . $key] == 1))
                                                        echo "selected";
                                                    ?>>401(k)</option><option value="2"   <?php
                                                            if (array_key_exists('type_of_retirement_plan_' . $key, $data) && ($data['type_of_retirement_plan_' . $key] == 2))
                                                                echo "selected";
                                                            ?>>403(b)</option><option value="3"   <?php
                                                            if (array_key_exists('type_of_retirement_plan_' . $key, $data) && ($data['type_of_retirement_plan_' . $key] == 3))
                                                                echo "selected";
                                                            ?>>457</option><option value="4"   <?php
                                                            if (array_key_exists('type_of_retirement_plan_' . $key, $data) && ($data['type_of_retirement_plan_' . $key] == 4))
                                                                echo "selected";
                                                            ?>>TSP</option><option value="other"   <?php
                                                            if (array_key_exists('type_of_retirement_plan_' . $key, $data) && ($data['type_of_retirement_plan_' . $key] == "other"))
                                                                echo "selected";
                                                            ?>>Other</option></select>
                                            </div>

                                            <div class="col-sm-12 inner-left other-type" style="display:<?php
                                            if (array_key_exists('type_of_retirement_plan_'.$key, $data) && ($data['type_of_retirement_plan_'.$key] == "other")) {
                                                echo 'block'; 
                                            } else {
                                                echo 'none';
                                            }
                                            ?>;">
                                                <label for="label">Type of retirement plan</label>
                                                <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="type of plan" name="answer[88][copy][<?php echo $key; ?>][type_of_retirement_plan_description_<?php echo $key; ?>]" type="text" aria-required="true" value="<?php
                                                if (array_key_exists('type_of_retirement_plan_description_' . $key, $data)) {
                                                    echo $data["type_of_retirement_plan_description_" . $key];
                                                }
                                                ?>">
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2>Defined contribution retirement plan</h2>
                                            <p> You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-12 inner-left">

                                                <label for="label">Minimum annual amount (as % of income) that your employer contributes whether or not you contribute:</label>
                                                <div class="input-group service-input-group" style="margin-bottom: 0 !important;">
                                                    <input data-validation="" class="full-width comprehensive-width borderRight0 custom-validation form-control " type="number" data-rule-regex="false" required="required" style="padding-right:  0!important; width:100% !important;" placeholder="" name="answer[88][copy][<?php echo $key; ?>][minimum_annual_amount_your_employer_contribute_<?php echo $key; ?>]" type="number" aria-required="true" value="<?php
                                                    if (array_key_exists('minimum_annual_amount_your_employer_contribute_' . $key, $data)) {
                                                        echo $data["minimum_annual_amount_your_employer_contribute_" . $key];
                                                    }
                                                    ?>">
                                                    <span class="input-group-addon percent-input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2> Defined contribution retirement plan</h2>
                                            <p> You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left ">
                                                <label for="label">Is there a match offered</label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][match_offered_'.$key.']', 'yes',(array_key_exists('match_offered_'.$key,$data) && $data['match_offered_'.$key] == "yes") ? true : false,['class'=>'match-offered' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][match_offered_'.$key.']', 'no',(array_key_exists('match_offered_'.$key,$data) && $data['match_offered_'.$key] == "no") ? true : false,['class'=>'match-offered' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                            <div class="col-sm-12  first-match" style="display:<?php
                                            if (array_key_exists("match_offered_". $key, $data) && ($data["match_offered_". $key] == "yes")) {
                                                echo 'block';
                                            } else {
                                                echo "none";
                                            }
                                            ?>">
                                                <label for="label">First match amount</label>
                                                <div class="first-input col-sm-6 paddingLeft0">
                                                    <div class="col-sm-8 paddingLeft0 paddingRight0 small-error">
                                                        <div class="inner-left" style="margin-top: 0 !important; width:100% !important">
                                                            <i style="position: absolute; top: 14px; left: 73%;"> % </i>
                                                            <input type="number" required style="width:100%; padding-right: 26px !important;" name="answer[88][copy][<?php echo $key; ?>][first_match_amount_upto_<?php echo $key; ?>]" value="<?php
                                                            if (array_key_exists('first_match_amount_upto_'. $key, $data)) {
                                                                echo $data["first_match_amount_upto_". $key];
                                                            }
                                                            ?>"> 
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 paddingRight0 paddingLeft0 text-right" style="margin-top: 13px;">up to</div>
                                                </div>
                                                <div class="second-input col-sm-6 paddingLeft0" style="width:25%;">
                                                    <div class="col-sm-8 paddingLeft0 paddingRight0 small-error">
                                                        <div class="inner-left" style="margin-top: 0 !important; width:100% !important"><i style="position: absolute; top: 14px; left: 73%;"> % </i>
                                                            <input style="width:100% !Important; padding-right: 26px !important;" type="number" required name="answer[88][copy][<?php echo $key; ?>][first_match_amount_of_<?php echo $key; ?>]" value="<?php
                                                            if (array_key_exists('first_match_amount_of_' . $key, $data)) {
                                                                echo $data["first_match_amount_of_" . $key];
                                                            }
                                                            ?>">

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 paddingRight0 ">of compensation.</div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12  first-match" style="display:<?php
                                            if (array_key_exists("match_offered_". $key, $data) && ($data["match_offered_". $key] == "yes")) {
                                                echo 'block';
                                            } else {
                                                echo "none";
                                            }
                                            ?>">
                                                <label for="label">Second match amount</label>
                                                <div class="first-input col-sm-6 paddingLeft0">
                                                    <div class="col-sm-8 paddingLeft0 paddingRight0 small-error">
                                                        <div class="inner-left" style="margin-top: 0 !important; width:100% !important">
                                                            <i style="position: absolute; top: 14px; left: 73%;"> % </i>
                                                            <input type="number" style="width:100%; padding-right: 26px !important;" required name="answer[88][copy][<?php echo $key; ?>][second_match_amount_upto_<?php echo $key; ?>]" value="<?php
                                                            if (array_key_exists('second_match_amount_upto_' . $key, $data)) {
                                                                echo $data["second_match_amount_upto_". $key];
                                                            }
                                                            ?>"> 

                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4 paddingRight0 paddingLeft0 text-right" style="margin-top: 13px;">up to</div>
                                                </div>

                                                <div class="second-input col-sm-6 paddingLeft0" style="width:25%;">
                                                    <div class="col-sm-8 paddingLeft0 paddingRight0 small-error">
                                                        <div class="inner-left" style="margin-top: 0 !important; width:100% !important">
                                                            <i style="position: absolute; top: 14px; left: 73%;"> % </i>
                                                            <input type="number" style="width:100%; padding-right:26px !important" required name="answer[88][copy][<?php echo $key; ?>][second_match_amount_of_<?php echo $key; ?>]"  value="<?php
                                                            if (array_key_exists('second_match_amount_of_'. $key, $data)) {
                                                                echo $data["second_match_amount_of_". $key];
                                                            }
                                                            ?>"> </div>
                                                    </div>
                                                    <div class="col-sm-4 paddingRight0">of compensation.</div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2> Defined contribution retirement plan</h2>
                                            <p>You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left">
                                                <label for="label">What percentage do you contribute?</label>
                                                <div class="input-group service-input-group">
                                                    <input type="number" data-validation="" style="padding-right:0 !important;" class="full-width stock-width-addon borderRight0 custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[88][copy][<?php echo $key; ?>][percentage_you_contribute_<?php echo $key; ?>]" type="number" aria-required="true"  value="<?php
                                                    if (array_key_exists('percentage_you_contribute_' . $key, $data)) {
                                                        echo $data["percentage_you_contribute_" . $key];
                                                    }
                                                    ?>">
                                                    <span class="input-group-addon">%</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2> Defined contribution retirement plan</h2>
                                            <p> You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left table-width insurance-beneficiary-modal">

                                                <label for="label">Beneficiaries</label>
                                                <p style="width:100%;"><i>If a trust is named, click <a class="click-here-open1" data-toggle="modal" data-target="#clickHereModal1" data-count="{{$key}}">here</a></i></p>


                                                <table id="group-life-insurance-info" style="width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <td>First name</td>
                                                            <td>Last name</td>
                                                            <td>Date of birth</td>
                                                            <td>Percentage</td>
                                                            <td>Primary</td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">

                                                        <?php
                                                        if (array_key_exists('IBRecords_' . $key, $data)) {
                                                            $details = json_decode($data["IBRecords_" . $key]);
                                                            if (!empty($details)) {
                                                                foreach ($details as $key1 => $datas) {
                                                                    ?>
                                                                    <tr data-index='{{$key1}}'><td>{{$datas->InsuranceName}}</td>
                                                                        <td>{{$datas->lastName}}</td>
                                                                        <td>{{$datas->dob}}</td>
                                                                        <td>{{$datas->percentage}} %</td>
                                                                        <td>
                                                                            <?php if ($datas->insurancePrimary) { ?>
                                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                                            <?php } ?> </td>
                                                                        <td> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i>
                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                                <button type="button" class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal" data-copy="{{$key}}">Add beneficiary</button>
                                            </div>
                                            <div class="col-sm-6 inner-left insurance-beneficiary-modal table-width">
                                                <table id ='group-life-insurance-info1'>
                                                    <thead>
                                                        <tr>
                                                            <td style="width:35%;">Trust Name</td>
                                                            <td style="width:35%;">Trustee Name </td>
                                                            <td style="width:15%;"></td>
                                                            <td style="width:15%;"></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
                                                        <?php
                                                        if (array_key_exists('clickHereFormRecords_' . $key, $data)) {
                                                            $details1 = json_decode($data["clickHereFormRecords_" . $key]);
                                                            if (!empty($details1)) {
                                                                foreach ($details1 as $key11 => $datas) {
                                                                    ?>
                                                                    <tr data-index="{{$key11}}">
                                                                        <td style="width:25%;">{{$datas->nameOfTrust}}</td>
                                                                        <td style="width:25%;">{{$datas->trustee}}</td>
                                                                        <td align="right" style="width:25%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count="{{$key}}" aria-hidden="true"></i></td>
                                                                        <td style="width:25%;"><i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[88][copy][<?php echo $key; ?>][IBRecords_<?php echo $key; ?>]">{{(!empty($data["IBRecords_".$key]))? $data["IBRecords_".$key]:null}}</textarea>
                                            <textarea id="clickHereFormData1" class="hidden" name="answer[88][copy][<?php echo $key; ?>][clickHereFormRecords_<?php echo $key; ?>]">
                                                <?php
                                                if (array_key_exists('clickHereFormRecords_' . $key, $data)) {
                                                    echo $data["clickHereFormRecords_" . $key];
                                                }
                                                ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} RETIREMENT PLANS</h6>
                                            <h2>Defined contribution retirement plan</h2>
                                            <p> You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                            <div class="col-sm-6 inner-left contingent-data table-width">

                                                <label for="label">Are there contingent beneficiaries?</label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'yes',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "yes")? true:false,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'no',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "no")? true:false,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>

                                            </div>
                                            <div class="col-sm-6 inner-left contingent-beneficiaries-modal table-width"  display:<?php
                                            if (array_key_exists("contingent_beneficiaries_" . $key, $data) && ($data["contingent_beneficiaries_" . $key] == "yes")) {
                                                echo 'block';
                                            } else {
                                                echo "none";
                                            }
                                            ?>>

                                                <label for="label">Contingent Beneficiaries</label>

                                                <table id="contingent-beneficiary-info">
                                                    <thead>
                                                        <tr>
                                                            <td>First name</td>
                                                            <td>Last name</td>
                                                            <td>Date of birth</td>
                                                            <td>Percentage</td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
                                                        <?php
                                                        if (array_key_exists('CBRecords_' . $key, $data)) {
                                                            $details = json_decode($data["CBRecords_" . $key]);
                                                            if (!empty($details)) {
                                                                foreach ($details as $key1 => $datas) {
                                                                    ?>
                                                                    <tr data-index='{{$key1}}'><td>{{$datas->contingentInsuranceName}}</td>
                                                                        <td>{{$datas->contingentLastName}}</td>
                                                                        <td>{{$datas->contingentDateOfBirth}}</td>
                                                                        <td>{{$datas->contingentPercentage}} %</td>
                                                                        <td align="right"> <i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i>
                                                                        </td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <button type="button" class="contingent-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal" data-copy="{{$key}}">Add beneficiary</button>
                                                <textarea id="contingentBeneficiaryData" class="hidden" name="answer[88][copy][<?php echo $key; ?>][CBRecords_<?php echo $key; ?>]">{{(!empty($data["CBRecords_".$key]))? $data["CBRecords_".$key]:null}}</textarea>
                                            </div>

                                        </div>
                                    </div>
                                </section>

                            </fieldset>
                            <h3></h3>
                            <fieldset>

                                <section class="sections" >
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}}  RETIREMENT PLANS</h6>
                                            <h2>Defined contribution retirement plan</h2>
                                            <p> You may know this as a 401(k), 403(b), etc.
                                                <?php
                                                if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                    ?>
                                                    We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left ">

                                                <label for="label">Do you have any other defined contribution retirement plans?</label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][other_contribution_retirement_plans_'.$key.']', 'yes',(array_key_exists('other_contribution_retirement_plans_'.$key,$data) && $data['other_contribution_retirement_plans_'.$key] == "yes")? true:false,['class'=>'other-contribution-plans' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[88][copy]['.$key.'][other_contribution_retirement_plans_'.$key.']', 'no',(array_key_exists('other_contribution_retirement_plans_'.$key,$data) && $data['other_contribution_retirement_plans_'.$key] == "no")? true:false,['class'=>'other-contribution-plans' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <?php
                        }
                    }
                    ?>
                    <textarea id="clickHereFormData" class="hidden" name="answer[88][clickHereFormRecords]">{{(!empty($answer["clickHereFormRecords"]))? $answer["clickHereFormRecords"]:null}}</textarea>

                    <div id="modal-section" >
                        <div class="sections" >
                            <!-- BENEFICIARY MODAL-->
                            <div id="insuranceBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD BENEFICIARIES</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="setup-content">
                                                    <div class="section-right">

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','First Name') }}
                                                            {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Last Name') }}
                                                            {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Date of birth') }}
                                                            {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Relationship') }}
                                                            <i class="fa fa-angle-down selectArrow"></i>
                                                            <select style="position: absolute;" class="relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left defined-contribution-other" style="display: none;">
                                                            {{ Form::label('label','Other') }}
                                                            {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'describe', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <div class="primary-option">
                                                                <ul>
                                                                    <p class="primary-select" style="line-height: 20px; float: left; margin-right: 5px;">Primary ?</p>
                                                                    <li style="display:inline; float: left; width:30px"> <label class="checkbox-custom-label">
                                                                            <input type="checkbox" class="primary-selected">
                                                                            <span class="checkbox-icon"></span>
                                                                            <span>
                                                                            </span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Percentage (max 100%)') }}
                                                            <div class="input-group">
                                                                {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required', 'min'=>'0','max'=>'100','maxlength'=>'3']) }}
                                                                <span class="input-group-addon">%</span>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button data-number="" id='insuranceBeneficiariesForm' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- BENEFICIARY MODAL END-->
                            <!-- CLICK HERE MODAL-->

                            <div id="clickHereModal1" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD TRUST</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    <div class="col-sm-12 validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Name of trust') }}
                                                            {{ Form::input('text','name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Trustee') }}
                                                            {{ Form::input('text','trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Successor trustee') }}
                                                            {{ Form::input('text','successor_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control successor-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'successor trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Primary beneficiary') }}
                                                            {{ Form::input('text','primary_beneficiary',null,['data-validation'=> '' , 'class'=>'custom-validation form-control primary-beneficiary', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'primary beneficiary', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <a class='append-beneficiary-input1'>+Beneficiary</a>
                                                        </div>
                                                        <div class="col-sm-12 append-input1 inner-left">
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='clickHereForm1' type="button" style="border-radius: 3px;
                                                                    color: #fff;    
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;                                  
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none; 
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- CLICK HERE MODAL END-->

                            <!-------CONTINGENT BENEFICIARY MODAL-------->
                            <div id="contingentBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD CONTINGENT BENEFICIARIES</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="setup-content">
                                                    <div class="section-right">
                                                        <div class="validation-alert">

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','First Name') }}
                                                                {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control contingent-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Last Name') }}
                                                                {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Date of birth') }}
                                                                {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Relationship') }}
                                                                <i class="fa fa-angle-down selectArrow"></i>
                                                                <select style="position: absolute;" class="contingent-relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                            </div>
                                                            <div class="col-sm-12 inner-left contingent-other-relationship" style="display: none;">
                                                                {{ Form::label('label','Other') }}
                                                                {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Percentage (max 100%)') }}
                                                                <div class="input-group">
                                                                    {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3']) }}
                                                                    <span class="input-group-addon">%</span>
                                                                </div>
                                                            </div>


                                                            <div class="col-sm-12 inner-left">
                                                                <button data-number="" id='contingentBeneficiaryForm'  class="modal-button"type="button" style="border-radius: 3px; margin-top: 40px;">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-------CONTINGENT BENEFICIARY MODAL END-------->

@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script id="definedContributionstep1" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p> You may know this as a 401(k), 403(b), etc.
                    <?php
                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                        ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <input name="questionName[88]" type="hidden" value="Defined contribution retirement plan">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Type of retirement plan</label>
                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 48px; z-index: 1; top: 42px; font-size: 16px;"></i>
                    <select style="position: absolute;" class="income-type valid" name="answer[88][copy][<%= copynum %>][type_of_retirement_plan_<%= copynum %>]" required="" aria-invalid="false" aria-required="true" style="display: none;"><option selected="" disabled="" value="">SELECT</option><option value="1">401(k)</option><option value="2">403(b)</option><option value="3">457</option><option value="4">TSP</option><option value="5">Other</option></select>
                </div>

                <div class="col-sm-12 inner-left other-type" style="display:none;">
                    <label for="label">Type of retirement plan</label>
                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="type of plan" name="answer[88][copy][<%= copynum %>][type_of_retirement_plan_description_<%= copynum %>]" type="text" aria-required="true">
                </div>
            </div>
        </div>
    </section>
</script>


<script id="definedContributionstep2" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6> RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>

            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-12 inner-left">
                    <label for="label">Minimum annual amount (as % of income) that your employer contributes whether or not you contribute:</label>
                    <div class="input-group service-input-group" style="margin-bottom: 0 !important;">
                        <input data-validation="" required="required" type="number" class="full-width comprehensive-width borderRight0 custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[88][copy][<%= copynum %>][minimum_annual_amount_your_employer_contribute_<%= copynum %>]" type="text" aria-required="true">
                        <span class="input-group-addon percent-input-group-addon">%</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>


<script id="definedContributionstep3" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left ">
                    <label for="label">Is there a match offered</label>
                    <label class="radio-custom-label">
                        <input class="match-offered" required="required" name="answer[88][copy][<%= copynum %>][match_offered_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="match-offered" required="required" name="answer[88][copy][<%= copynum %>][match_offered_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-12  first-match" style="display:none">
                    <label for="label">First match amount</label>
                    <div class="first-input">
                        <input  placeholder="%" style="text-align:right; padding-right: 3px;" type="number" name="answer[88][copy][<%= copynum %>][first_match_amount_upto_<%= copynum %>]"> <p>up to</p>
                    </div>

                    <div class="second-input">
                        <input placeholder="%" style="text-align:right; padding-right: 3px;" type="number" name="answer[88][copy][<%= copynum %>][first_match_amount_of_<%= copynum %>]"> <p>of compensation.</p>
                    </div>
                </div>
                <div class="col-sm-12  first-match" style="display:none">
                    <label for="label">Second match amount</label>
                    <div class="first-input">
                        <input placeholder="%" style="text-align:right; padding-right: 3px;" type="number" name="answer[88][copy][<%= copynum %>][second_match_amount_upto_<%= copynum %>]"> <p>up to</p>
                    </div>
                    <div class="second-input">
                        <input placeholder="%" style="text-align:right; padding-right: 3px;" type="number" name="answer[88][copy][<%= copynum %>][second_match_amount_of_<%= copynum %>]"> <p>of compensation.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>

<script id="definedContributionstep4" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left">
                    <label for="label">What percentage do you contribute?</label>
                    <div class="service-input-group input-group">

                        <input data-validation="" type="number" class="custom-validation form-control comprehensive-width borderRight0" data-rule-regex="false" required="" placeholder="" name="answer[88][copy][<%= copynum %>][percentage_you_contribute_<%= copynum %>]" type="text" aria-required="true">
                        <span class="input-group-addon percent-input-group-addon"> % </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>

<script id="definedContributionstep5" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left table-width insurance-beneficiary-modal"> 

                    <label for="label">Beneficiaries</label>
                    <p style="width:100%;"><i>If a trust is named, click <a class="click-here-open1" data-toggle="modal" data-target="#clickHereModal1" data-count="<%= copynum %>">here</a></i></p>

                    <table id="group-life-insurance-info" style="width: 100%;">
                        <thead>
                            <tr>
                                <td>First name</td>
                                <td>Last name</td>
                                <td>Date of birth</td>
                                <td>Percentage</td>
                                <td>Primary</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type="button" class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal" data-copy="<%= copynum %>">Add beneficiary</button>
                </div>
                <div class="col-sm-6 inner-left insurance-beneficiary-modal table-width" >
                    <table id='group-life-insurance-info1'>
                        <thead>
                            <tr>
                                <td style="width:35%;">Trust name</td>
                                <td style="width:35%;">Trustee Name </td>
                                <td style="width:15%;"></td>
                                <td style="width:15%;"></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                </div>
                <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[88][copy][<%= copynum %>][IBRecords_<%= copynum %>]"></textarea>
                <textarea id="clickHereFormData1" class="hidden" name="answer[88][copy][<%= copynum %>][clickHereFormRecords_<%= copynum %>]"></textarea>
            </div>
        </div>
    </section>
</script>

<script id="definedContributionstep6" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <div class="col-sm-6 inner-left contingent-data table-width" >

                    <label for="label">Are there contingent beneficiaries?</label>
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries" required="required" name="answer[88][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries" required="required" name="answer[88][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>

                </div>
                <div class="col-sm-6 inner-left contingent-beneficiaries-modal table-width" style="display:none;">

                    <label for="label">Contingent Beneficiaries</label>

                    <table id="contingent-beneficiary-info">
                        <thead>
                            <tr>
                                <td>First name</td>
                                <td>Last name</td>
                                <td>Date of birth</td>
                                <td>Percentage</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type="button" class="contingent-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal" data-copy="<%= copynum %>">Add beneficiary</button>
                </div>
                <textarea id="contingentBeneficiaryData" class="hidden" name="answer[88][copy][<%= copynum %>][CBRecords_<%= copynum %>]"></textarea>

            </div>
        </div>
    </section>
</script>
<script id="definedContributionstep7" type="text/html">
    <section class="sections" >
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>RETIREMENT PLANS</h6>
                <h2>Defined contribution retirement plan</h2>
                <p>
                    You may know this as a 401(k), 403(b), etc.
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    <?php } ?>
                </p>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                <input name="questionName[887]" type="hidden" value="Defined contribution retirement plan">

                <div class="col-sm-6 inner-left ">

                    <label for="label">Do you have any other defined contribution retirement plans?</label>
                    <label class="radio-custom-label">
                        <input class="other-contribution-plans" required="required" name="answer[88][copy][<%= copynum %>][other_contribution_retirement_plans_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="other-contribution-plans" required="required" name="answer[88][copy][<%= copynum %>][other_contribution_retirement_plans_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>

                </div>
            </div>
        </div>
    </section>
</script>

<script id="clickHereBeneficiary1" type="text/html">
    <div class="primary-beneficiary-wrapper1">
        <label for="label" >Beneficiary <span class="beneficiary-count1">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" class="contingent-check" name="contingent1[<%= count%>]">
                        <span class="checkbox-icon"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[<%= count%>]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>

<script>
var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,5,'employment-income-retirement-preview'])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $(document).on('click', '#clickHereForm', function () {
        $('#clickHereModal').modal('hide');
    });
    if ($('.defined-contribution-retirement:checked').val() == 'yes') {
        $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    } else {
        $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
    }

    var tempArray = <?php echo json_encode($answer); ?>;
    if (!($.isEmptyObject(tempArray.copy))) {
        var copynumber = tempArray.copy.length - 1;
    } else {
        var copynumber = 0;
        appendSteps(copynumber);
    }
    $('a[href="#finish"]').on('click', function () {
        if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-contribution-plans:checked').val() == 'no' || $('a[href="#finish"]').text() == 'Continue') {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
    });


    $(document).on('change', '.match-offered:checked', function () {
        if ($(this).val() == 'yes') {
            $('.first-match').show();
        } else {
            $('.first-match').hide();
        }

    });


    $(document).on('change', '.other-contribution-plans', function () {
        $("select").selectBoxIt();
        if ($(this).val() == 'yes') {
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            ;
        } else {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            removeSteps();

        }
        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-contribution-plans:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                ;
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                ;
                copynumber++;
                appendSteps(copynumber);
                $(form).steps('next');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });
    });
    $(document).on('change', '.defined-contribution-retirement', function () {
        var $this = $(this);
        if ($this.val() == 'yes') {
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            ;
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            ;
        } else {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
//                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            removeSteps();
//                may be needed
//                $(document).on('click', 'a[href="#next"]', function ( ) {
//                    if ($('a[href="#next"]').text() == 'finish') {
//                        $('a[href="#next"]').attr('href', '#finish');
//                        $('#services-question-form input,  #services-question-form select').prop('required', false);
//                        removeSteps();
//                        $("form").steps("finish");
//                    }
//                });
        }
        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.defined-contribution-retirement:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                ;
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                ;
                appendSteps(copynumber);
                copynumber++;
                $(form).steps('next');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }

        });
    });



    $(document).on('change', '.income-type', function () {
        if ($(this).val() == 'other') {
            $('.other-type, .other-type input').show();
        } else {
            $('.other-type, .other-type input').hide().val('');
        }
    });

    function appendSteps(copynumber)
    {
        var plan1 = $('#definedContributionstep1').html();
        var plan2 = $('#definedContributionstep2').html();
        var plan3 = $('#definedContributionstep3').html();
        var plan4 = $('#definedContributionstep4').html();
        var plan5 = $('#definedContributionstep5').html();
        var plan6 = $('#definedContributionstep6').html();
        var plan7 = $('#definedContributionstep7').html();
        html1 = _.template(plan1);
        html2 = _.template(plan2);
        html3 = _.template(plan3);
        html4 = _.template(plan4);
        html5 = _.template(plan5);
        html6 = _.template(plan6);
        html7 = _.template(plan7);
        $("form").steps("add", {
            content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html6({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html7({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
    }
    function removeSteps() {
        var copyind = form.steps('getCurrentIndex');
        var totalSteps = $('#services-question-form fieldset').length;
        if (totalSteps > copyind) {
            var len = totalSteps - copyind;
            for (var i = 1; i <= len; i++) {
                var index = copyind + 1;
                $("form").steps("remove", index);
            }
        }
    }

    $(document).on('change', '.contingent-relationship', function () {
        if ($(this).val() == '3') {
            $('.contingent-other-relationship, .contingent-other-relationship input').show();
        } else {
            $('.contingent-other-relationship, .contingent-other-relationship input').hide().val('');
        }
    });

    $(document).on('change', '.contingent-beneficiaries', function () {
        if ($(this).val() == 'yes') {
            $(this).closest('.section-size').find('.contingent-beneficiaries-modal').show();
        } else {
            $(this).closest('.section-size').find('.contingent-beneficiaries-modal').hide();
            contingentList = [];
//                $('#contingentBeneficiaryData').html(JSON.stringify(contingentList));
        }

    });

    $(document).on('change', '.relationship', function () {
        if ($(this).val() == 3) {
            $('.defined-contribution-other , .defined-contribution-other input').show();
        } else {
            $('.defined-contribution-other , .defined-contribution-other input').hide().val('');
        }
    });

//        /------append fieldsets------ /


//    /------ append fieldsets end------/
    //    /---------BENEFICIARY FORM ---------/



// ---------------- new js starts --------------------
    var insuranceList = [];
    $(document).on('click', '#insuranceBeneficiariesForm', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                var copyVal = $(this).attr('data-number');
                var insuranceBeneficiaryDataVal = $.trim($('textarea[name="answer[88][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').val());
                if (insuranceBeneficiaryDataVal.length > 0) {
                    insuranceList = JSON.parse(insuranceBeneficiaryDataVal);
                }
                if ($('#insuranceBeneficiaryModal .insurance-first-name').val() && $('#insuranceBeneficiaryModal .last-name').val() && $('#insuranceBeneficiaryModal .percentage').val()) {
                    if ($(this).hasClass('edit-form')) {
                        insuranceList[$(this).attr('data-index')] = {
                            InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                            lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                            dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                            relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                            other: $('#insuranceBeneficiaryModal .insurance-other-relationship-description').val(),
                            insurancePrimary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                            percentage: $('#insuranceBeneficiaryModal .percentage').val()
                        }
                        if ($('.primary-selected').is(':checked')) {
                            $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .date-of-birth ').val() + '</td>   <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + ' %</td> <td> <i class="fa fa-check" aria-hidden="true"></i> </td> <td> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i> </td>');
                        } else {
                            $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .date-of-birth ').val() + '</td>   <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + ' %</td> <td> </td> <td><i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i> </td>');
                        }
                    } else {
                        if ($('.primary-selected').is(':checked')) {
                            $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td><td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .date-of-birth ').val() + '</td>  <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + ' %</td> <td> <i class="fa fa-check" aria-hidden="true"></i> </td> <td> <i title="Edit" class="fa fa-pencil group-insurance-edit " aria-hidden="true"></i> </td></tr>');
                        } else {
                            $('.insurance-beneficiary-modal #group-life-insurance-info tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td><td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .date-of-birth ').val() + '</td>  <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + ' %</td> <td></td> <td> <i title="Edit" class="fa fa-pencil group-insurance-edit" aria-hidden="true"></i> </td></tr>');
                        }
                        insuranceList.push({
                            InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                            lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                            dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                            relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                            other: $('#insuranceBeneficiaryModal .insurance-other-relationship-description').val(),
                            insurancePrimary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                            percentage: $('#insuranceBeneficiaryModal .percentage').val()
                        });
                    }
                    $('#insuranceBeneficiaryModal').modal('hide');
                    $('textarea[name="answer[88][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').html(JSON.stringify(insuranceList));
                }
            }
        }
        return false;
    });
// ---------------- new js finish --------------------

    $(document).on('click', '#group-life-insurance-info .group-insurance-edit', function () {
        $('#insuranceBeneficiaryModal').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#insuranceBeneficiariesForm").attr("data-number", copyArr);
        insuranceList = JSON.parse($('textarea[name="answer[88][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = insuranceList[index];
        $('#insuranceBeneficiaryModal .insurance-first-name').val(insuranceDetails.InsuranceName);
        $('#insuranceBeneficiaryModal .last-name').val(insuranceDetails.lastName);
        $('#insuranceBeneficiaryModal .date-of-birth').val(insuranceDetails.dob);
        $('#insuranceBeneficiaryModal .relationship').val(insuranceDetails.relationship).trigger('change');
        $('#insuranceBeneficiaryModal .insurance-other-relationship-description').val(insuranceDetails.other);
        $('#insuranceBeneficiaryModal .percentage').val(insuranceDetails.percentage);
        if (insuranceDetails.insurancePrimary) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', true);
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', false);
        }
        if ($(this).closest('tr').find('.fa-check').length || $('#group-life-insurance-info tbody[data-copy="' + copyArr + '"]').find('.fa-check').length == 0) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', false).parent().find('.checkbox-icon').css('background', '#fff');
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', true).parent().find('.checkbox-icon').css('background', '#ccc');
        }
        $('#insuranceBeneficiariesForm').addClass('edit-form').attr('data-index', index);
        $('#insuranceBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '.add-beneficiary', function () {
        var copyArr = $(this).attr('data-copy');
        $('#insuranceBeneficiariesForm').removeClass('edit-form');
        $('#insuranceBeneficiaryModal input[type="text"], #insuranceBeneficiaryModal input[type="number"]').val('');
        $('#insuranceBeneficiaryModal select').val('').trigger('change');
        $('#insuranceBeneficiaryModal .primary-selected:checked').prop('checked', false);
        $('#insuranceBeneficiaryModal .error-alert').remove();
        $("#insuranceBeneficiariesForm").attr("data-number", $(this).attr('data-copy'));
        if ($('#group-life-insurance-info tbody[data-copy="' + copyArr + '"]').find('.fa-check').length > 0) {
            $('.primary-selected').prop('disabled', true).parent().find('.checkbox-icon').css('background', '#ccc');
        } else {
            $('.primary-selected').prop('disabled', false).parent().find('.checkbox-icon').css('background', '#fff');
        }
    });
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });

//---------------- new js stars ---------------------

    $('#contingentBeneficiaryForm').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                var contingentList = [];
                var copyVal = $(this).attr('data-number');
                var contingentBeneficiaryDataVal = $('textarea[name="answer[88][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').val();
                if (contingentBeneficiaryDataVal.length > 0) {
                    contingentList = JSON.parse(contingentBeneficiaryDataVal);
                }
                if ($('#contingentBeneficiaryModal .contingent-first-name').val() && $('#contingentBeneficiaryModal .last-name').val() && $('#contingentBeneficiaryModal .percentage').val()) {
                    if ($(this).hasClass('edit-form')) {
                        contingentList[$(this).attr('data-index')] = {
                            contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                            contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                            contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                            contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                            otherInput: $('#contingentBeneficiaryModal .other-relationship-description').val(),
                            contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                        }
//                    $('#contingent-beneficiary-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td> <td>' + $('#contingentBeneficiaryModal .last-name').val() + '</td> <td>' + $('#contingentBeneficiaryModal .date-of-birth').val() + '</td>   <td>' + $('#contingentBeneficiaryModal .percentage ').val() + '</td> <td><i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i> </td>');
                        $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td> <td>' + $('#contingentBeneficiaryModal .last-name').val() + '</td> <td>' + $('#contingentBeneficiaryModal .date-of-birth').val() + '</td>   <td>' + $('#contingentBeneficiaryModal .percentage ').val() + ' %  </td> <td align="right"><i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i> </td>');
                    } else {

                        $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + contingentList.length + '><td>' + $('#contingentBeneficiaryModal .contingent-first-name').val() + ' </td><td>' + $('#contingentBeneficiaryModal .last-name').val() + '</td> <td>' + $('#contingentBeneficiaryModal .date-of-birth').val() + '</td> <td>' + $('#contingentBeneficiaryModal .percentage ').val() + ' % </td> <td align="right"> <i title="Edit" class="fa fa-pencil beneficiary-edit" aria-hidden="true"></i> </td></tr>');
                        contingentList.push({
                            contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                            contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                            contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                            contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                            otherInput: $('#contingentBeneficiaryModal .other-relationship-description').val(),
                            contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                        });

                    }
                    $('#contingentBeneficiaryModal').modal('hide');
                    $('textarea[name="answer[88][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').html(JSON.stringify(contingentList));
//                        $('#contingentBeneficiaryModal .contingent-first-name,#contingentBeneficiaryModal .last-name, #contingentBeneficiaryModal .percentage, #contingentBeneficiaryModal .date-of-birth').val('');
                }
            }
        }
        return false;
    });

//--------------- new js finish ----------------------


    $(document).on('click', '.beneficiary-edit', function () {
        $('#contingentBeneficiaryModal').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#contingentBeneficiaryForm").attr("data-number", copyArr);
        contingentList = JSON.parse($('textarea[name="answer[88][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        contingentDetails = contingentList[index];
        $('#contingentBeneficiaryModal .contingent-first-name').val(contingentDetails.contingentInsuranceName);
        $('#contingentBeneficiaryModal .last-name').val(contingentDetails.contingentLastName);
        $('#contingentBeneficiaryModal .date-of-birth').val(contingentDetails.contingentDateOfBirth);
        $('#contingentBeneficiaryModal .contingent-relationship').val(contingentDetails.contingentRelationship).trigger('change');
        $('#contingentBeneficiaryModal .contingent-other-relationship').val(contingentDetails.otherInput);
        $('#contingentBeneficiaryModal .percentage').val(contingentDetails.contingentPercentage);
        $('#contingentBeneficiaryForm').addClass('edit-form').attr('data-index', index);
        $('#contingentBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '.contingent-beneficiary', function () {
        $("#contingentBeneficiaryForm").attr("data-number", $(this).attr('data-copy'));
        $('#contingentBeneficiaryForm').removeClass('edit-form');
        $('#contingentBeneficiaryModal input[type="text"], #contingentBeneficiaryModal input[type="number"]').val('');
        $('#contingentBeneficiaryModal select').val('').trigger('change');
        $('#contingentBeneficiaryModal .error-alert').remove();
    });

    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    //            /---------DISABLE INPUT CHECKBOX---------/

    //<-----click here form list code----->
    //copy add trust
    $(document).on('click', '.append-beneficiary-input1', function () {
        var number1 = $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text();
        if (number1 == '') {
            number1 = 1;
        }
        var beneficiaryInput1 = $('#clickHereBeneficiary1').html();
        html = _.template(beneficiaryInput1);
        number1++;
        var countNumberr = {
            count: number1
        };
        $('.append-input1').append(html((countNumberr)));
        $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text(number1);
    });
    $(document).on('click', '.click-here-open1', function () {
        var count = $(this).attr('data-count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereForm1').removeClass('edit-form').removeAttr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(''),
                $('#clickHereModal1 .trustee').val(''),
                $('#clickHereModal1 .successor-trustee').val(''),
                $('#clickHereModal1 .primary-beneficiary').val('')

        $('#clickHereModal1 .error-alert').remove();

    });

    $(document).on('click', '#group-life-insurance-info1 .group-insurance-edit1', function () {

        var trustList1 = [];
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList1 = JSON.parse($.trim($('textarea[name="answer[88][copy][' + copyArr + '][clickHereFormRecords_' + copyArr + ']"]').val()));
        var index = $(this).closest('tr').attr('data-index');
        trustDetails1 = trustList1[index];

        var count = $(this).data('count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereModal1').modal();
        var index = $(this).closest('tr').attr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(trustDetails1.nameOfTrust),
                $('#clickHereModal1 .trustee').val(trustDetails1.trustee),
                $('#clickHereModal1 .successor-trustee').val(trustDetails1.sucessorTrustee),
                $('#clickHereModal1 .primary-beneficiary').val(trustDetails1.primaryBeneficiary)
        var html = '';
        var count = 1;
        for (var i = 0; i < trustDetails1.benficiary.length; i++) {
            count++;
            var checked = '';
            if (trustDetails1.contingent[i]) {
                checked = 'checked';
            }
            html += '<div class="primary-beneficiary-wrapper1"><label for="label" >Beneficiary <span class="beneficiary-count1">' + count + '</span></label><div class="primary-option"><ul>\n\
                     <p class="primary-select"><i>contingent</i></p><li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label"><input type="checkbox" class="contingent-check" name="contingent1[' + count + ']" ' + checked + '>\n\
<span class="checkbox-icon"></span><span></span> </label></li></ul></div><input data-validation="" value="' + trustDetails1.benficiary[i] + '" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[' + count + ']" type="text" aria-required="true" style="margin-top: 6px;"></div>';
        }
        $('.append-input1').append(html);
        $('#clickHereForm1').addClass('edit-form').attr('data-index', index);
        $('#clickHereModal1 .error-alert').remove();
    });



    $(document).on('click', '#clickHereForm1', function () {
        var count = $(this).attr('data-number');
        var extendedBeneficiary = $("[name^='extended_beneficiary1']")
                .map(function () {
                    return $(this).val();
                }).get();
        var contingentBeneficiary = $("[name^='contingent1']")
                .map(function () {
                    return $(this).prop('checked');
                }).get();
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {

            var trustListcopy = [];
            var trustDetails = $.trim($('textarea[name="answer[88][copy][' + count + '][clickHereFormRecords_' + count + ']"]').val());
            if (trustDetails.length > 0) {
                trustListcopy = JSON.parse(trustDetails);
            }



            $('#clickHereModal1 .error-alert').remove();
            if ($('#clickHereModal1 .name-of-trust').val()) {

                if ($(this).hasClass('edit-form')) {
                    trustListcopy[$(this).attr('data-index')] = {
                        nameOfTrust: $('#clickHereModal1 .name-of-trust').val(),
                        trustee: $('#clickHereModal1 .trustee').val(),
                        sucessorTrustee: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    }
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#clickHereModal1 .name-of-trust').val() + '</td> <td>' + $('#clickHereModal1 .trustee').val() + '</td> <td align="right" style="width:25%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count="' + count + '" aria-hidden="true"></i> </td><td style="width:25%;"><i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td>');

                } else {
                    trustListcopy.push({
                        nameOfTrust: $('#clickHereModal1 .name-of-trust').val(),
                        trustee: $('#clickHereModal1 .trustee').val(),
                        sucessorTrustee: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    });
                    var listlength = trustListcopy.length - 1;
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + ']').append('<tr data-index=' + listlength + '><td>' + $('#clickHereModal1 .name-of-trust').val() + '</td><td>' + $('#clickHereModal1 .trustee').val() + '</td><td align="right"  style="width:25%;"> <i title="Edit" class="fa fa-pencil group-insurance-edit1 " data-count="' + count + '" aria-hidden="true"></i></td><td style="width:25%;">  <i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td></tr>');

                }

            } else {
                $('#clickHereModal1 input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }

                });
            }
            $('textarea[name="answer[88][copy][' + count + '][clickHereFormRecords_' + count + ']"]').html(JSON.stringify(trustListcopy));
            $('#clickHereModal1').modal('hide');
        }
    });

    $(document).on('click', '.insurance-beneficiary-modal #group-life-insurance-info1 .group-insurance-trash1 ', function () {
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList = JSON.parse($('textarea[name="answer[88][copy][' + copyArr + '][clickHereFormRecords_' + copyArr + ']"]').val());
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            trustList.splice(index_id, 1);
            $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').empty();
            if (trustList.length != 0) {
                var tr = '';
                $.each(trustList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.nameOfTrust + '</td><td>' + value.trustee + '</td><td align="right" style="width:25%;"><i title="Edit" class="fa fa-pencil group-insurance-edit1" data-count=' + copyArr + ' aria-hidden="true"></i> </td><td style="width:25%;"> <i title="Delete" class="fa fa-trash group-insurance-trash1 " aria-hidden="true"></i> </td></tr>';
                });
                $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').html(tr);
            }


            $('textarea[name="answer[88][copy][' + copyArr + '][clickHereFormRecords_' + copyArr + ']"]').html(JSON.stringify(trustList));
            return false;
        });
    });
//end copy trust




    //    /--------APPEND BENIFICIARY INPUT END-------/
});


</script>
<style type="text/css">
    .contingent-beneficiary{
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
</style>
@stop 