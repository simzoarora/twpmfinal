<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Planning for educational expenses.</h2>
        <p>You indicated that you are planning for +
            s. Enter the beginning year of each child and to which school do they plan
            to attend,if known.If you need help, simply contact us.
        </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','data[38][questionName]','Planning for educational expenses') }}
        <div class="information-table">
            <table id='children-info' class='information-table'>
                <thead>
                    <tr>
                        <td>Student</td>
                        <td>Age</td>
                        <td>Beginning year</td>
                        <td>Level</td>
                        <td>School Name</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    </tr>
                </tbody>
            </table>
            <button type='button' class='add-dependent' data-toggle="modal" data-target="#myModal">Add Child</button>
        </div>
    </div>
</div>
<div class="sections">

</div>