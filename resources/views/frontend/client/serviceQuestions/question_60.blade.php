
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>Estate planning</h6>
          <h2>Pre- and Post-nuptial</h2>
        <p>It's never to early to consider your legacy. The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[60]','Estate planning') }}

        <div class="col-sm-6 inner-left ">

            {{ Form::label('label', 'Do you have a pre- or post-nuptial?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[60][prePostNuptial]', 'yes',(!empty($answer) && array_key_exists('prePostNuptial',$answer)) ? (($answer['prePostNuptial']=="yes")  ? true : false):false,['class'=>'nuptial' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[60][prePostNuptial]', 'no',(!empty($answer) && array_key_exists('prePostNuptial',$answer)) ? (($answer['prePostNuptial']=="no")  ? true : false):false,['class'=>'nuptial' ,'required'=>'required']) }}No
                <span class="radio-icon"></span>
            </label>

        </div>
    </div>
</div>