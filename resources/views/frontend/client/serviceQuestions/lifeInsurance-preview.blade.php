<div class="col-sm-12 recommended-service-div liabilities">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-12 section-left">
                <h6>LIABILITIES</h6>
                <h2>Let's talk about your liabilities.</h2>
                <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply  <a href="#" class="contact-modal-show"> contact us</a>. </p>
                 <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">As user completes each section, direct them back to this main Liabilities screen, so they can select which section to work on next.</p>-->
                 <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">User cannot submit info and proceed to payment/sc heduling until they have completed all sections.</p>-->
            </div>
            <div class="col-sm-6 col-sm-offset-1 col-xs-12 section-right section-size">

                <div class="categories">
                    <div class="headings col-sm-8">
                        <h4>INCOME/ MARITAL STATUS</h4>
                    </div>
                    <div class="status col-sm-4">
                        <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,301,45])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(301, $topicsInfo) ) {
                                                                                     echo $topicsInfo[301];

                            }
                            ?></a>
                    </div>
                </div>
                <div class="categories">
                    <div class="headings col-sm-8">
                        <h4>YOUR ASSETS</h4>
                    </div>
                    <div class="status col-sm-4">
                        <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,302,34])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(302, $topicsInfo) ) {
                                                                                        echo $topicsInfo[302];

                            }
                            ?></a>
                    </div>
                </div>

                <div class="categories">
                    <div class="headings col-sm-8">
                        <h4>YOUR LIABILITIES</h4>
                    </div>
                    <div class="status col-sm-4">
                        <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,303,33])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(303, $topicsInfo) ) {
                                                          echo $topicsInfo[303];

                            }
                            ?></a>
                    </div>
                </div>

                <div class="categories">
                    <div class="headings col-sm-8">
                        <h4>YOUR FAMILY</h4>
                    </div>
                    <div class="status col-sm-4">
                        <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,304,32])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(304, $topicsInfo)) {
                            echo $topicsInfo[304];
                            }
                            ?></a>
                    </div>
                </div>

                <div class="categories">
                    <div class="headings col-sm-8">
                        <h4>POLICY INFORMATION</h4>
                    </div>
                    <div class="status col-sm-4">
                        <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,305,28])}}"><?php
                            if (!empty($topicsInfo) && array_key_exists(305, $topicsInfo)) {
                               echo $topicsInfo[305];
                            }
                            ?></a>
                    </div>
                </div>
                <div class="custon-continue-button"> 
                    <a href="<?php
            if ($topicsInfo['serviceCompleted']) {
                echo route('frontend.client.selectedService', [config('constant.subdomain'), $currentService->id]);
            } else {
                echo'javascript:;';
            }
            ?>" class="<?php echo (!$topicsInfo['serviceCompleted']) ? 'toastrForService' : '' ?>">Continue</a>
                </div>
                <div class="custon-return">
                    <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                </div>
            </div>
        </div>
    </section>
</div>
