@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
<style>
    .selectboxit-text[data-val=""] {
        color: #b4b6b7! important;
        font-style: italic;
    }
    .selectboxit-container{
        margin-bottom: 0px! important;
    }
</style>
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}    
                    {{ Form::input('hidden','topicId',1) }}                    
                    <h3></h3>   
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>about you</h6>
                                    <h2>Let's get the rest of your personal information.</h2>
                                    <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
                                </div> 

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[47]','Let\s get the rest of your personal information') }}
                                    <div class="alignment about-you">
                                        <div class="col-sm-6 left-side">
                                            <div class="inner-left ">
                                                {{ Form::label('label', 'First name') }}
                                                {{ Form::input('text','answer[47][personal_first_name]',isset($defaultData)? $defaultData['firstName']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','readonly'=>true]) }}
                                            </div>
                                        </div>

                                        <div class="col-sm-2 small-error">
                                            <div class="inner-left" style="width:100% !important;">
                                                {{ Form::label('label', 'MI') }}
                                                {{ Form::input('text','answer[47][personal_mi]',isset($defaultData)? $defaultData['middleName']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false",  'readonly' => true, 'placeholder'=>'' , 'style'=>'width:100%;']) }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="alignment about-you">
                                        <div class="col-sm-6 left-side ">
                                            <div class="inner-left ">
                                                {{ Form::label('label', 'Last name') }}
                                                {{ Form::input('text','answer[47][personal_last_name]',isset($defaultData)? $defaultData['lastName']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','readonly'=>true]) }}
                                            </div>
                                        </div>

                                        <div class="col-sm-2 small-error">
                                            <div class="inner-left" style="width:100% !important;">
                                                {{ Form::label('label', 'Suffix') }}
                                                {{ Form::input('text','answer[47][personal_suffix]',(!empty($answer) && array_key_exists('personal_suffix', $answer)) ? $answer['personal_suffix']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'placeholder'=>'' , 'style'=>'width:100%;']) }}
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <!--dob-->
                            @include('frontend.client.serviceQuestions.question_222')
                        </section>
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <!--address-->
                            @include('frontend.client.serviceQuestions.question_218')
                        </section> 
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <!--marital status-->
                            @include('frontend.client.serviceQuestions.question_219')
                        </section> 
                    </fieldset>
                    <?php
                    if (array_key_exists('taxFillingStatus', $defaultData) && (($defaultData['taxFillingStatus'] == 4) || ($defaultData['taxFillingStatus'] == 2) || ($defaultData['taxFillingStatus'] == 3) || ($defaultData['taxFillingStatus'] == 5))) {
                        ?>
                        <h3></h3>
                        <fieldset>
                            <section class="sections">
                                <!--spouse section-->
                                @include('frontend.client.serviceQuestions.question_220')
                            </section>
                        </fieldset>
                    <?php } ?>
                    <?php
                    if (array_key_exists('taxFillingStatus', $defaultData) && (($defaultData['taxFillingStatus'] == 4) || ($defaultData['taxFillingStatus'] == 2) || ($defaultData['taxFillingStatus'] == 3) || ($defaultData['taxFillingStatus'] == 5))) {
                        ?>
                        <h3></h3>
                        <fieldset>
                            <section class="sections">
                                @include('frontend.client.serviceQuestions.question_221')
                            </section>
                        </fieldset>
                    <?php } ?>
                    <?php
                    if (array_key_exists('taxFillingStatus', $defaultData) && (($defaultData['taxFillingStatus'] == 4) || ($defaultData['taxFillingStatus'] == 2) || ($defaultData['taxFillingStatus'] == 3) || ($defaultData['taxFillingStatus'] == 5))) {
                        ?>
                        <h3></h3>
                        <fieldset>
                            <section class="sections">
                                @include('frontend.client.serviceQuestions.question_223')
                            </section>
                        </fieldset>
                    <?php } ?>
                    {{ Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),1])}}";

$(document).ready(function () {
    $('.select-marital').prop('disabled', true);
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $('.dobdatetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $(document).on('change', '.spouse-address-confirmation', function () {
        if ($(this).val() === 'no') {
            $('.addres-section').addClass('show').removeClass('hide');
        } else {
            $('.addres-section').addClass('hide').removeClass('show');
        }
    });
});
</script>

<!--question_219--> 

<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),1])}}";
    $(document).ready(function () {
//        $(document).on('change', '.select-marital', function () {
//            if ($(this).val() === '1' || $(this).val() === '2') {
//                $('.compre-partner-name').show();
//            } else {
//                $('.compre-partner-name').hide();
//            }
//        });



    });
    function myFunction() {
        var val = '';
        val = document.getElementById("spousefirstname").value;
        document.getElementById("firstnameprepopulate").innerHTML = document.getElementById("firstnameprepopulate").innerHTML + val;
        document.getElementById("firstnameprepopulate").value = val;
    }

</script>


<!--question_218--> 
<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),1])}}";
</script>


@stop
<style type="text/css">
    /*47*/
    .wrapper .sections .section-right .alignment.about-you .right-side{margin-top: 0;}
    /*47*/


</style>
<!--222-->
<style type="text/css">
    .wrapper .sections .section-right .alignment .left-side .inner-left{width:100% !important;}
    @media screen and (min-width: 768px) {
        .citizen .selectArrow{top: 43px; right:30px}
    }

    @media screen and (max-width: 767px) {
        .citizen .selectArrow{top: 44px; right:30px}
    }
</style>
<!--218-->
<style type="text/css">
    .wrapper .sections .section-right .alignment.about-you .right-side{margin-top: 0;}
    @media screen and (min-width: 768px) {
        .select-state .selectboxit-container span{max-width:77px !important; width: 77px !important; padding-right: 25px; margin-right: 0 !important}
        .select-state .selectArrow{top: 58px; right:25px}
    }

    @media screen and (max-width: 767px) {
        .select-state .selectboxit-container span{max-width:100% !important; width: 100% !important; padding-right: 25px;}
        .select-state .selectArrow{top: 44px; right:15px}
    }

    .wrapper .sections .section-right .inner-left label.error{width:100% !important;     max-width: 263px !important;}
    .wrapper .sections .section-right .inner-left, .wrapper .sections .section-right .alignment .inner-left{width:100% !important}
</style>
<!--219-->
<style type="text/css">
    .wrapper .sections .section-right .alignment.about-you .right-side{margin-top: 0;}
    wrapper .sections .section-right .inner-left label.error{width:100% !important;     max-width: 263px !important;}
    .select-state .selectboxit-container span{max-width:100% !important; width: 100%!important; padding-right: 25px;}
    .wrapper .sections .section-right .inner-left, .wrapper .sections .section-right .alignment .inner-left{width:100% !important}

    @media screen and (min-width: 768px) {
        .wrapper .sections .col-sm-12  .marital-status .selectArrow{top: 43px; right:30px}
    }

    @media screen and (max-width: 767px) {
        .wrapper .sections .section-right .inner-left .marital-status{background:red;}
        .wrapper .sections .section-right .inner-left .marital-status .fa.selectArrow{top: 44px; right:15px}
    }
</style>
<!--220-->
<style type="text/css">
    .wrapper .sections .section-right .alignment .left-side .inner-left{width:100% !important;}
</style>
<!--223-->
<style type="text/css">
    .wrapper .sections .section-right .alignment .left-side .inner-left{width:100% !important;}
    .selectboxit-container{width:inherit! important;}
    @media screen and (min-width: 768px) {
        .select-state .selectboxit-container span{max-width:77px !important; width: 77px !important; padding-right: 25px; margin-right: 0 !important}
        .select-state .selectArrow{top: 58px; right:25px}
    }

    @media screen and (max-width: 767px) {
        .select-state .selectboxit-container span{max-width:100% !important; width: 100% !important; padding-right: 25px;}
        .select-state .selectArrow{top: 44px; right:15px}
    }

</style>