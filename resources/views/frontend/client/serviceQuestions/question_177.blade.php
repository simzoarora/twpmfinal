@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',1) }}
                    {{ Form::input('hidden','topicId',14) }}
                    {{ Form::input('hidden','subTopicId',11) }}
                    {{ Form::input('hidden','redirectPageName','spouse-insurance-final-preview') }}

                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">


                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">{{session::get('loggedInSpouseName')}} OTHER INSURANCE POLICIES</h6>
                                    <h2>Homeowner's Insurance</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[177]','Homeowner\'s Insurance') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have homeowner's or renter's insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[177][homeowner_renter_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('homeowner_renter_insurance', $answer) && ($answer['homeowner_renter_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[177][homeowner_renter_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('homeowner_renter_insurance', $answer) && ($answer['homeowner_renter_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>


                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">{{session::get('loggedInSpouseName')}} OTHER INSURANCE POLICIES</h6>
                                    <h2>Umbrella Policies</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[178]','Umbrella policies') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have an umbrella policy?</label>
                                        <label class="radio-custom-label">
                                            <input class="umbrella-policy" required="required" name="answer[178][umbrella_policy]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('umbrella_policy', $answer) && ($answer['umbrella_policy'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="umbrella-policy" required="required" name="answer[178][umbrella_policy]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('umbrella_policy', $answer) && ($answer['umbrella_policy'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="hide-umbrella-policy-section" style="display: <?php
                                    if (!empty($answer) && array_key_exists('umbrella_policy', $answer) && ($answer['umbrella_policy'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        <div class="col-sm-6 inner-left">
                                            <label for="label">What is the coverage amount?</label>
                                            <div class="input-group service-input-group coverage-input-group">
                                               <span class="input-group-addon">$</span> 
                                                <input data-validation="" class="borderLeft0 borderRight0 coverage-amt-width" data-rule-regex="false" required="" placeholder="" name="answer[178][coverage_amount]" type="number" aria-required="true" maxlength="2" style="padding-right:46px;" value="<?php
                                                if (!empty($answer) && array_key_exists('coverage_amount', $answer)) {
                                                    echo $answer["coverage_amount"];
                                                }
                                                ?>">
                                                <span class="input-group-addon">Million</span> 
                                            </div>
                                        </div>
                                        <?php
                                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                        ?>
                                        <div class="col-sm-6 inner-left spouse-umbrella-policy ">
                                            <label for="label">Is your spouse/partner covered by this umbrella policy?</label>
                                            <label class="radio-custom-label">
                                                <input class="" required="required" name="answer[178][spouse_covered_umbrella_policy]" type="radio" value="yes" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_covered_umbrella_policy', $answer) && ($answer['spouse_covered_umbrella_policy'] == "yes")) {
                                                    echo "checked";
                                                }
                                                ?>>Yes
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">
                                                <input class="" required="required" name="answer[178][spouse_covered_umbrella_policy]" type="radio" value="no" aria-required="true" <?php
                                                if (!empty($answer) && array_key_exists('spouse_covered_umbrella_policy', $answer) && ($answer['spouse_covered_umbrella_policy'] == "no")) {
                                                    echo "checked";
                                                }
                                                ?>>No
                                                <span class="radio-icon"></span>
                                            </label>
                                        </div>
                                    <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>

                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">{{session::get('loggedInSpouseName')}} OTHER INSURANCE POLICIES</h6>
                                    <h2>Hurricane policies</h2>
                                   
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[179]','Hurricane policies') }}
                                    <div class="col-sm-6 inner-left">
                                        <label for="label">Do you have Hurricane insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[179][hurricane_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('hurricane_insurance', $answer) && ($answer['hurricane_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[179][hurricane_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('hurricane_insurance', $answer) && ($answer['hurricane_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>

                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">{{session::get('loggedInSpouseName')}} OTHER INSURANCE POLICIES</h6>
                                    <h2>Flood Insurance</h2>
                                   
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[180]','Flood Insurance') }}
                                    <div class="col-sm-6 inner-left">
                                        <label for="label">Do you have flood insurance through FEMA?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[180][flood_insurance_through_fema]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('flood_insurance_through_fema', $answer) && ($answer['flood_insurance_through_fema'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[180][flood_insurance_through_fema]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('flood_insurance_through_fema', $answer) && ($answer['flood_insurance_through_fema'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left">
                                        <label for="label">Do you have excess flood insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="excess-flood-insurance" required="required" name="answer[180][excess_flood_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('excess_flood_insurance', $answer) && ($answer['excess_flood_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="excess-flood-insurance" required="required" name="answer[180][excess_flood_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('excess_flood_insurance', $answer) && ($answer['excess_flood_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="excess-insurance-section" style="display:  <?php
                                    if (!empty($answer) && array_key_exists('excess_flood_insurance', $answer) && ($answer['excess_flood_insurance'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        <div class="col-sm-6 inner-left">
                                            <label for="label">What is the coverage amount?</label>
                                         <div class="service-input-group input-group">
                                                <span class="input-group-addon"> $ </span>
                                            <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control" data-rule-regex="false" required="" placeholder=" " name="answer[180][what_coverage_amount]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('what_coverage_amount', $answer)) {
                                                echo $answer["what_coverage_amount"];
                                            }
                                            ?>">
                                         </div>
                                        </div>
                                        <div class="col-sm-6 inner-left">
                                            <label for="label">What is the deductible?</label>
                                           <div class="service-input-group input-group">
                                                <span class="input-group-addon"> $ </span>
                                            <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control" data-rule-regex="false" required="" placeholder=" " name="answer[180][what_deductible]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('what_deductible', $answer)) {
                                                echo $answer["what_deductible"];
                                            }
                                            ?>">
                                           </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>


                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">{{session::get('loggedInSpouseName')}} OTHER INSURANCE POLICIES</h6>
                                    <h2>Other Insurance </h2>
                                  
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[181]','Other Insurance') }}
                                    <div class="col-sm-6 inner-left">
                                        <label for="label">Are you covered by Errors and Omissions Insurance or Malpractice insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="malpractice-insurance" required="required" name="answer[181][error_omissions_malpractice_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('error_omissions_malpractice_insurance', $answer) && ($answer['error_omissions_malpractice_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="malpractice-insurance" required="required" name="answer[181][error_omissions_malpractice_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('error_omissions_malpractice_insurance', $answer) && ($answer['error_omissions_malpractice_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-6 inner-left selectable malpractice-insurance-section" style="display: <?php
                                    if (!empty($answer) && array_key_exists('error_omissions_malpractice_insurance', $answer) && ($answer['error_omissions_malpractice_insurance'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <p style="width: 100%; font-weight: 400;">Check all that apply:</p>
                                        <div class="trustee ">
                                            <a class=" disability-insurance-term insurance-type <?php
                                            if (!empty($answer) && array_key_exists('person_purchase', $answer) && ($answer["person_purchase"] == "purchased")) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="purchased">
                                                <div class="medicare-options purchased">
                                                    {{ Form::input('hidden','answer[181][person_purchase]',(!empty($answer) && array_key_exists('person_purchase',$answer)) ? $answer['person_purchase']:null, ['id'=>'disability_insurance_purchased', 'class'=>'disability_insurance']) }}
                                                    <p>Personally- <br> purchased</p>    
                                                </div>
                                            </a>
                                            <a class=" disability-insurance-term insurance-type <?php
                                            if (!empty($answer) && array_key_exists('employer_provide', $answer) && ($answer["employer_provide"] == "provided")) {
                                                echo 'selected-option';
                                            }
                                            ?>" data-value="provided">
                                                <div class="medicare-options purchased">
                                                    {{ Form::input('hidden','answer[181][employer_provide]',(!empty($answer) && array_key_exists('employer_provide',$answer)) ? $answer['employer_provide']:null, ['id'=>'disability_insurance_provided', 'class'=>'disability_insurance']) }}
                                                    <p>Employer- <br>provided</br></p>    
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
     var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,14,'spouse-insurance-final-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $(document).on('change', '.umbrella-policy', function () {
            if ($(this).val() == 'yes') {
                $('.hide-umbrella-policy-section').show();
            } else {
                $('.hide-umbrella-policy-section').hide();
                $('.hide-umbrella-policy-section input, .hide-umbrella-policy-section input[type="radio"]').val('').prop('checked', false);
            }

        });
        $(document).on('change', '.excess-flood-insurance', function () {
            if ($(this).val() == 'yes') {
                $('.excess-insurance-section').show();
            } else {
                $('.excess-insurance-section').hide();
                $('.excess-insurance-section input').val('');
            }

        });
        $(document).on('change', '.malpractice-insurance', function () {
            if ($(this).val() == 'yes') {
                $('.malpractice-insurance-section').show();
            } else {
                $('.malpractice-insurance-section').hide();
                $('.insurance-type').removeClass('selected-option');
            }

        });
        $(document).on('click', '.disability-insurance-term', function () {
            $('#disability_insurance_' + $(this).attr('data-value')).val($(this).attr('data-value'));
            if ($(this).hasClass('selected-option')) {
                $('#disability_insurance_' + $(this).attr('data-value')).removeAttr('value');
            }
            $(this).toggleClass('selected-option');
        });
    });

</script>
@stop 