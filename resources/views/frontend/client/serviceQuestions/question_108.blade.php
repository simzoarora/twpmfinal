
    <!-- top section finish -->
    <div class="col-sm-12">
        <div class="col-md-4 col-xs-11 section-left">
            <p class="section-name"> ACCOUNT REGISTRATION </p>
            <h4> Name of Trust and Trustees </h4>
            <p>Enter the trust information as well as information of all trustees. </p>
        </div>
        <div class="col-sm-6 col-sm-offset-1 section-right">
            {{ Form::input('hidden','data[108][questionName]','Name of Trust and Trustees') }}

            <div class="col-sm-8 inner-left">
                {{ Form::label('label', 'What is the name of the trust?') }}
                {{ Form::input('text','data[108][answer][trustname]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control trust_name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'trust name ']) }}
            </div>

            <div class="col-sm-8 inner-left">
                {{ Form::label('label', 'What is the date of inception?') }}
                <date-util format="MM/dd/yyyy"></date-util>
                <input data-validation="" class="custom-validation form-control datetimepicker trust_datetimepicker" data-rule-regex="false" required="" placeholder="MM/DD/YYYY"   name="data[108][answer][date]" type="text">
            </div>

            <div class="col-sm-12 inner-left uploadform">
                {{ Form::label('label', 'Upload your trust certificate or other trust documentation') }}
                <div>
                    <label for="investment-trustees" class="btn upload-statment upload-account-statment">Upload File</label>
                    <input id="investment-trustees" class="investment-trust-upload" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
                </div>
            </div>

            <div class="col-sm-12 inner-left children-details-table" >
                {{ Form::label('label', 'List all trustees.') }}
                <div class="col-sm-12  ">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="row">
                                <div class="add-child" style="padding-top:0px;">
                                    <table id='trust-info'>
                                        <thead>
                                            <tr> 
                                                <td>Trustee name</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="children-info">
                                            </tr> 
                                        </tbody>
                                    </table> 
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <button type='button' class="add-dependent" data-toggle="modal" data-target="#trustModal">Add trustee</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- top section finish --> 
    <form method="post" class="trustee-form">

    <!-- modal starts -->
    <div id="trustModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD TRUSTEE</h4>
                </div>
                <!-- steps modal body starts -->
                <div class="modal-body" style='border:none;'>

                    <!-- first section starts -->
                    <div class=" setup-content" id="step-1">
                        <div class="row"> 
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[108][questionName]','ADD ACCOUNT OWNER') }}

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Full name') }}
                                    {{ Form::input('text','IM-Owner-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeFullName', 'data-rule-regex'=>"false", 'required'=>true,  'placeholder'=>'full name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Percent ownership (max 100%)') }}
                                    {{ Form::input('number','owner-percent',null,['data-validation'=>'' , 'class'=>'custom-validation form-control IMTrusteePercent', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'%']) }}
                                </div>


                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label','Date of Birth') }}
                                    {{ Form::input('text','owner-dob',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMTrusteeDOB', 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'US Citizenship') }}
                                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 27%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                    <!--{{ Form::select('data[108][answer][citizenship]', ['SELECT','US Citizen', 'US Resident Alien', 'Non-resident Alien'],null,['class'=>'citizenship']) }}-->
                                    <select style="position: absolute;" class="status valid IMTrusteeCitizen" name="citizenship" aria-invalid="false" required="true"  style="display: none;">
                                        <option selected disabled="" value="0">SELECT</option>
                                        <option value="1">US Citizen</option>
                                        <option value="2">US Resident Alien</option>
                                        <option value="3">Non-resident Alien</option>
                                    </select>
                                </div>

                                <div class="col-sm-12 inner-left account-trustee-upload IMTrusteeUploadForm" style="display: none;">
                                    {{ Form::label('label', 'Upload Form W-8') }}

                                    <div>
                                        <label for="investment-account-owner" class="btn IMTrusteeUploadStatement IMTrusteeUploadAccountStatement">Upload statement</label>
                                        <input id="investment-account-owner" class="account-trustee-upload IMTrusteeUploadFormBox" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
                                    </div>
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20"> 
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>
                                </div> 

                            </div>
                        </div>
                    </div>

                    <!-- first step finish -->

                    <!-- second step starts -->
                    <div class=" setup-content" id="step-2"  style="display: none;">
                        <div class="row"> 
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[108][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Marital Status') }}
                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-one', 'Married', ['class' => 'label', 'for' => 'option-one'])}}
                                        {{ Form::radio('selector', 'married',null,['class'=>'IMTrusteeMaritalStatus',  'id' => 'option-one', ' required'=>'required']) }}
                                    </div>

                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-two', 'Single', ['class' => 'label', 'for' => 'option-two'])}}
                                        {{ Form::radio('selector', 'single',null,['class'=>'IMTrusteeMaritalStatus',  'id' => 'option-two', ' required'=>'required']) }}

                                    </div>
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'SSN or Tax ID') }}
                                    {{ Form::input('text','ssn-tax-id',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeSsnTaxId', 'data-rule-regex'=>"false",  'required'=>true,  'placeholder'=>'enter SSN or Tax ID']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Email Address') }}
                                    {{ Form::input('email','im-owner-email',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeEmailId', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'email@email.com']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Preferred phone contact number') }}
                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'mobile',null,['class'=>'IMTrusteePreferedPhoneContact',  'id' => 'mobile', ' required'=>'required']) }}
                                        {{Form::label('mobile', 'Mobile', ['class' => 'label', 'for' => 'mobile'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'home',null,['class'=>'IMTrusteePreferedPhoneContact',  'id' => 'home', ' required'=>'required']) }}
                                        {{Form::label('home', 'Home', ['class' => 'label', 'for' => 'home'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-4 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'work',null,['class'=>'IMTrusteePreferedPhoneContact',  'id' => 'work', ' required'=>'required']) }}
                                        {{Form::label('work', 'Work', ['class' => 'label', 'for' => 'work'])}}
                                    </div>

                                </div>
                                <div class="col-sm-9 inner-left"> 
                                    {{ Form::label('label', 'Phone number') }}
                                    {{ Form::input('number','im-phone-number',null,['data-validation'=> '' , 'required'=>true, 'class'=>'custom-validation form-control IMTrusteePhoneNumber', 'data-rule-regex'=>"false", 'placeholder'=>'XXX-XXX-XXXX']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- second step finsih -->
                    <!-- third step starts -->
                    <div class=" setup-content" id="step-3"  style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">

                                <!-- tabs pannel starts -->
                                <div class="mortgage-tabed mortgageSecondStep">
                                    <h2>Please provide one of the following forms of identification:</h2> 
                                    <ul class="mortgage-nav">
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#stateIssueLicense">State issued Driver's License</a></li>
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#passport">Passport</a></li>
                                    </ul>

                                    <div class="tab-content row">
                                        <!-- State issued Driver's License content starts --> 
                                        <div id="stateIssueLicense" class="tab-pane fade active in">
                                            <div class="col-sm-12 inner-left" >
                                                {{ Form::label('label', 'Driver license number') }}
                                                {{ Form::input('text','IMTrustee-license',null,['data-validation'=> '',  'class'=>'custom-validation form-control IMTrusteeLicense', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'License number']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','IMOwnder-licence-expire-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMTrusteeLicenseExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'State of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMTrusteeLicenceIssuingState" name="states" required ="required" aria-invalid="false">
                                                    <option selected="" disabled="" value="">STATE</option>
                                                    <option value="1">a</option>
                                                    <option value="2">b</option>
                                                    <option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- State issued Driver's License content finish -->
                                        <!-- Passport tab content starts -->
                                        <div id="passport" class="tab-pane fade">
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Passport ID number') }}
                                                {{ Form::input('text','im-owner-passport',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteePassportNumber', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'Passport ID number']) }}
                                            </div>
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','passport-expiry-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMTrusteePassportExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'Country of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMTrusteePassportIssuanceCountry" name="passport-issue-country" aria-invalid="false" required="required"><option selected="" disabled="" value="">COUNTRY</option><option value="1">a</option><option value="2">b</option><option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- Passport tab content finish -->   
                                    </div>
                                    <!-- tab content finish -->
                                </div> 
                                <!-- tabs panel finish -->
                                {{ Form::input('hidden','data[108][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-9 mt-20">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- third step finish -->
                    <!-- fourth step starts -->
                    <div class=" setup-content" id="step-4" style="display: none">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[108][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', "Please provide this account owner's mailing address") }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 1') }}
                                    {{ Form::input('text','IM-owner-address-1',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeAddress1', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 2') }}
                                    {{ Form::input('text','IM-owner-address2',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeAddress2', 'required' => true,  'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'City') }}
                                    {{ Form::input('text','IM-ownerCity',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeCity', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'State') }}
                                    {{ Form::input('text','IM-owner-state',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeState', 'required' => true, 'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Zipcode') }}
                                    {{ Form::input('text','IM-owner-zipcode',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeZipCode', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-9 mt-20 paddingRight0">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fourth steps finish -->
                    <!-- fifth step starts -->
                    <div class=" setup-content" id="step-5" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[108][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Name of employer(if retired, enter most recent employer). ') }}
                                    {{ Form::input('text','IMowner-employer-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeEmployerName', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'employer name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Occupation') }}
                                    {{ Form::input('text','IM-owner-employer-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMTrusteeEmployerOccupation', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'occupation']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button id='employerForm' class="finishBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Finish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fifth step finish -->
                </div>
                <!-- steps modal body finish -->

            </div>
        </div>
    </div>
    <!-- modal finish -->
 
@section('after-scripts')
<!--<script src="{{ asset('js/dashboard.js') }}"></script>-->
<script src="{{ asset('js/investment-management.js') }}"></script>
<script>

    $(document).ready(function () {
        $('.trust_datetimepicker, .license_expire, .passport_expire, .datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $("select").selectBoxIt();

        $(document).on('change', '.status', function () {
            if ($(this).val() === '3') {
                $('.uploadform').show();
            } else {
                $('.uploadform').hide();
            }
            $("select").selectBoxIt();
        });

// --------------------- page jquery starts 

        var TrusteeList = [];

        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this); 
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#trustModal').modal('hide');
                        TrusteeList[$(this).attr('data-index')] = {
                            IMTrusteeFullName: $('#trustModal .IMTrusteeFullName').val(),
                            IMTrusteePercent: $('#trustModal .IMTrusteePercent').val(),
                            IMTrusteeDOB: $('#trustModal .IMTrusteeDOB').val(),
                            IMTrusteeCitizen: $('#trustModal .IMTrusteeCitizen').val(),
                            upload: $('#trustModal .IMTrusteeUploadFormBox').val(),

                            IMTrusteeMaritalStatus: $('#trustModal .IMTrusteeMaritalStatus:checked').val(),
                            IMTrusteeSsnTaxId: $('#trustModal .IMTrusteeSsnTaxId').val(),
                            IMTrusteeEmailId: $('#trustModal .IMTrusteeEmailId').val(),
                            IMTrusteePreferedPhoneContact: $('#trustModal .IMTrusteePreferedPhoneContact:checked').val(),
                            IMTrusteePhoneNumber: $('#trustModal .IMTrusteePhoneNumber').val(),

                            IMTrusteeLicense: $('#trustModal .IMTrusteeLicense').val(),
                            IMTrusteeLicenseExpireDate: $('#trustModal .IMTrusteeLicenseExpireDate').val(),
                            IMTrusteeLicenceIssuingState: $('trustModal .IMTrusteeLicenceIssuingState').val(),
                            IMTrusteePassportNumber: $('#trustModal .IMTrusteePassportNumber').val(),
                            IMTrusteePassportExpireDate: $('#trustModal .IMTrusteePassportExpireDate').data('date'),
                            IMTrusteePassportIssuanceCountry: $('#trustModal .IMTrusteePassportIssuanceCountry').val(),

                            IMTrusteeAddress1: $('#trustModal .IMTrusteeAddress1').val(),
                            IMTrusteeAddress2: $('#trustModal .IMTrusteeAddress1').val(),
                            IMTrusteeCity: $('#trustModal .IMTrusteeCity').val(),
                            IMTrusteeState: $('#trustModal .IMTrusteeState').val(),
                            IMTrusteeZipCode: $('#trustModal .IMTrusteeZipCode').val(),

                            IMTrusteeEmployerName: $('#trustModal .IMTrusteeEmployerName').val(),
                            IMTrusteeEmployerOccupation: $('#trustModal .IMTrusteeEmployerOccupation').val()

                        };
                        $('#trust-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#trustModal .IMTrusteeFullName').val() + '</td>  <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                    } else {
                        $('#trust-info tbody').append('<tr data-index=' + TrusteeList.length + '><td>' + $('#trustModal .IMTrusteeFullName').val() + '</td>  <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                        TrusteeList.push({
                            IMTrusteeFullName: $('#trustModal .IMTrusteeFullName').val(),
                            IMTrusteePercent: $('#trustModal .IMTrusteePercent').val(),
                            IMTrusteeDOB: $('#trustModal .IMTrusteeDOB').val(),
                            IMTrusteeCitizen: $('#trustModal .IMTrusteeCitizen').val(),
                            IMTrusteeUploadDoc: $('#trustModal .IMTrusteeUploadDoc').val(),

                            IMTrusteeMaritalStatus: $('#trustModal .IMTrusteeMaritalStatus:checked').val(),
                            IMTrusteeSsnTaxId: $('#trustModal .IMTrusteeSsnTaxId').val(),
                            IMTrusteeEmailId: $('#trustModal .IMTrusteeEmailId').val(),
                            IMTrusteePreferedPhoneContact: $('#trustModal .IMTrusteePreferedPhoneContact:checked').val(),
                            IMTrusteePhoneNumber: $('#trustModal .IMTrusteePhoneNumber').val(),

                            IMTrusteeLicense: $('#trustModal .IMTrusteeLicense').val(),
                            IMTrusteeLicenseExpireDate: $('#trustModal .IMTrusteeLicenseExpireDate').val(),
                            IMTrusteeLicenceIssuingState: $('trustModal .IMTrusteeLicenceIssuingState').val(),
                            IMTrusteePassportNumber: $('#trustModal .IMTrusteePassportNumber').val(),
                            IMTrusteePassportExpireDate: $('#trustModal .IMTrusteePassportExpireDate').data('date'),
                            IMTrusteePassportIssuanceCountry: $('#trustModal .IMTrusteePassportIssuanceCountry').val(),

                            IMTrusteeAddress1: $('#trustModal .IMTrusteeAddress1').val(),
                            IMTrusteeAddress2: $('#trustModal .IMTrusteeAddress1').val(),
                            IMTrusteeCity: $('#trustModal .IMTrusteeCity').val(),
                            IMTrusteeState: $('#trustModal .IMTrusteeState').val(),
                            IMTrusteeZipCode: $('#trustModal .IMTrusteeZipCode').val(),

                            IMTrusteeEmployerName: $('#trustModal .IMTrusteeEmployerName').val(),
                            IMTrusteeEmployerOccupation: $('#trustModal .IMTrusteeEmployerOccupation').val()
                        });

                        $('#trustModal').modal('hide');
                    }
                }

                $(document).on('click', '#trust-info .fa-pencil', function () {
                    $('#trustModal').modal();
                    $('#trustModal #step-1').show();
                    $('#trustModal #step-2, #trustModal #step-3, #trustModal #step-4, #trustModal #step-5').css('display', 'none');
                    $('#trustModal .inner-left input, #trustModal .inner-left select').parent().find('label.error').remove();

                    var index = $(this).closest('tr').attr('data-index');
                    TrusteeListDetails = TrusteeList[index];
                    loanTermSteps = TrusteeListDetails[index];
                    $('#trustModal .IMTrusteeFullName').val(TrusteeListDetails.IMTrusteeFullName);
                    $('#trustModal .IMTrusteePercent').val(TrusteeListDetails.IMTrusteePercent);
                    $('#trustModal .IMTrusteeDOB').val(TrusteeListDetails.IMTrusteeDOB);
                    $('#trustModal .IMTrusteeCitizen').val(TrusteeListDetails.IMTrusteeCitizen).trigger('change');
                    $('#trustModal .IMTrusteeUploadDoc').val(TrusteeListDetails.IMTrusteeUploadDoc);

                    $('#SemiAnnualCCModal .IMTrusteeMaritalStatus[value=' + TrusteeListDetails.IMTrusteeMaritalStatus + ']').prop('checked', true);
                    $('#trustModal .IMTrusteeSsnTaxId').val(TrusteeListDetails.IMTrusteeSsnTaxId);
                    $('#trustModal .IMTrusteeEmailId').val(TrusteeListDetails.IMTrusteeEmailId);
                    $('#SemiAnnualCCModal .IMTrusteePreferedPhoneContact[value=' + TrusteeListDetails.IMTrusteePreferedPhoneContact + ']').prop('checked', true);
                    $('#trustModal .IMTrusteePhoneNumber').val(TrusteeListDetails.IMTrusteePhoneNumber);

                    $('#trustModal .IMTrusteeLicense').val(TrusteeListDetails.IMTrusteeLicense);
                    $('#trustModal .IMTrusteeLicenseExpireDate').val(TrusteeListDetails.IMTrusteeLicenseExpireDate);
                    $('#trustModal .IMTrusteeLicenceIssuingState').val(TrusteeListDetails.IMTrusteeLicenceIssuingState);
                    $('#trustModal .IMTrusteePassportNumber').val(TrusteeListDetails.IMTrusteePassportNumber);
                    $('#trustModal .IMTrusteePassportExpireDate').val(TrusteeListDetails.IMTrusteePassportExpireDate);
                    $('#trustModal .IMTrusteePassportIssuanceCountry').val(TrusteeListDetails.IMTrusteePassportIssuanceCountry);

                    $('#trustModal .IMTrusteeAddress1').val(TrusteeListDetails.IMTrusteeAddress1);
                    $('#trustModal .IMTrusteeAddress2').val(TrusteeListDetails.IMTrusteeAddress2);
                    $('#trustModal .IMTrusteeCity').val(TrusteeListDetails.IMTrusteeCity);
                    $('#trustModal .IMTrusteeState').val(TrusteeListDetails.IMTrusteeState);
                    $('#trustModal .IMTrusteeZipCode').val(TrusteeListDetails.IMTrusteeZipCode);

                    $('#trustModal .IMTrusteeEmployerName').val(TrusteeListDetails.IMTrusteeEmployerName);
                    $('#trustModal .IMTrusteeEmployerOccupation').val(TrusteeListDetails.IMTrusteeEmployerOccupation);
                    $('#trustModal .mortgageSecondStep ul.mortgage-nav li.active a').text(TrusteeListDetails.secondMortgageTab);
                    $('#employerForm').addClass('edit-form');
                    $('#employerForm').attr('data-index', index);

                    //   for third step fixed 15yr 30 yr and others
                    if (loanTermSteps) {
                        $('#trustModal .loan-li a:textEquals("' + loanTermSteps.loanTerm + '")').closest('.loan-li').addClass('active').siblings().removeClass('active');
                    } else {
                        $('#trustModal .loan-li.active').removeClass('active');
                    }

                });
            }
            return false;
        });

        $(document).on('click', '.add-dependent', function () {
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            $('#trustModal input[type=text], #trustModal input[type=number], #trustModal input[type=email]').val('');
            $('#employerForm').removeClass('edit-form');
            $('#trustModal input[type="radio"]').prop('checked', false);
            $('#trustModal .IMTrusteeCitizen, #trustModal .IMTrusteeLicenceIssuingState, #trustModal .IMTrusteePassportIssuanceCountry').val('').trigger('change');
            $('.OptionSelection').removeClass('married-selected');
            $('.mortgageSecondStep .mortgage-nav .secondloan-li').removeClass('active');
            $('#stateIssueLicense, #passport').removeClass('active in');
            $('#trustModal input[type="radio"]').prop('checked', false);
            $('.OptionSelection label').removeClass('married-selected');
//        OptionSelection

        });

        $(document).on('change', '.IMTrusteeMaritalStatus', function () {
            if ($(this).val() === 'married') {
                $('label[for=option-one]').addClass('married-selected');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }
        });
        
        if ($('.IMTrusteeMaritalStatus:checked').val() === 'married') {
            $('label[for=option-one]').addClass('married-selected');
        } else {
            $('label[for=option-one]').removeClass('married-selected');
        }

        $(document).on('change', '.IMTrusteeMaritalStatus', function () {
            if ($(this).val() === 'single') {
                $('label[for=option-two]').addClass('married-selected');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }
        });

        if ($('.IMTrusteeMaritalStatus:checked').val() === 'single') {
            $('label[for=option-two]').addClass('married-selected');
        } else {
            $('label[for=option-two]').removeClass('married-selected');
        }

        $(document).on('change', '.IMTrusteePreferedPhoneContact', function () {
            if ($(this).val() === 'mobile') {
                $('label[for=mobile]').addClass('married-selected');
            } else {
                $('label[for=mobile]').removeClass('married-selected');
            }
        });

        if ($('.IMTrusteePreferedPhoneContact:checked').val() === 'mobile') {
            $('label[for=mobile]').addClass('married-selected');
        } else {
            $('label[for=mobile]').removeClass('married-selected');
        }

        $(document).on('change', '.IMTrusteePreferedPhoneContact', function () {
            if ($(this).val() === 'home') {
                $('label[for=home]').addClass('married-selected');
            } else {
                $('label[for=home]').removeClass('married-selected');
            }
        });

        if ($('.IMTrusteePreferedPhoneContact:checked').val() === 'home') {
            $('label[for=home]').addClass('married-selected');
        } else {
            $('label[for=home]').removeClass('married-selected');
        }

        $(document).on('change', '.IMTrusteePreferedPhoneContact', function () {
            if ($(this).val() === 'work') {
                $('label[for=work]').addClass('married-selected');
            } else {

                $('label[for=work]').removeClass('married-selected');
            }
        });

        if ($('.IMTrusteePreferedPhoneContact:checked').val() === 'work') {
            $('label[for=work]').addClass('married-selected');
        } else {
            $('label[for=work]').removeClass('married-selected');
        }
        $(document).on('change', '.status', function () {
            if ($(this).val() === '3') {
                $('.IMTrusteeUploadForm').show();
            } else {
                $('.IMTrusteeUploadForm').hide();
            }
            $("select").selectBoxIt();
        });


        //            delete row
        $('#trust-info').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
        });

         

 

// --------------------- page jquery finish 
    });

</script>
<style type="text/css">
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li a {
        float: left;
        color: #2179ee;
        margin-right: 12px;
        border: solid 2px #2179ee;
        border-radius: 4px;
        margin-right: 8px;
        text-align: center;
        min-height: 52px;
        padding: 11px 5px;
        width: calc(100% - 10px);
        font-size: 13px;
        font-weight: normal;
        line-height: normal;
    }
    .mortgage-nav{margin-bottom: 0} 
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li:last-child a {line-height: 32px;} 
</style>
@endsection