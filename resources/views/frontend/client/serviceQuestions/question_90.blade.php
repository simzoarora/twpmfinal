
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa{ 
        /*float: right;*/
        margin-left: 10px;
        font-size: 13px;
    } 
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }  
    .nonRetirementInfo{
        position: relative;
    }  
    .nextBtn, .finishBtn{
        margin-top: 20%
    }
    .wrapper .sections .section-right .selectboxit-options.selectboxit-list {
        max-height: 160px !important;
    }
    .wrapper .sections .modal-content .section-right .inner-left input[type=number].CPMNonRentalExpectedSale{
        text-align: left;
    }
    .wrapper .sections .modal-content .section-right .inner-left input[type=number].CPMNonRentalExpectedSale:focus{
        text-align: right;
    }

</style>
@extends('frontend.layouts.client') 

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',13) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Real Estate (non-rental)</h2>
                                    <p>Use this section to enter information on all residential properties that are <em> not </em> being used as rental or income properties. If you have questions, feel free to <a>contact us</a>.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[90]','Real Estate (non-rental)') }}
                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Do you own a house?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[90][own_house]', 'yes',(!empty($answer) && array_key_exists('own_house',$answer)) ? (($answer['own_house']=="yes")  ? true : false):false,['class'=>'own-house table-confirmation' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[90][own_house]', 'no',(!empty($answer) && array_key_exists('own_house',$answer)) ? (($answer['own_house']=="no")  ? true : false):false,['class'=>'own-house table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>
                                    </div> 
                                    <div class="col-sm-6 inner-left non-rental-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('own_house', $answer) && ($answer['own_house'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">

                                        {{ Form::label('label', 'List all non-rental houses.') }}

                                        <table id='nonRetirementInfo'>  
                                            <thead>                       
                                                <tr>     
                                                    <td style="font-size: 13px;">Property name</td>   
                                                    <td></td>   
                                                    <td></td>  
                                                </tr>
                                            </thead>   
                                            <tbody>

                                                <?php
                                                if (!empty($answer) && array_key_exists('own_house_record', $answer)) {
                                                    $account = json_decode($answer["own_house_record"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->CPMNonRentalHouseName}}</td> 
                                                                <td></i></td>
                                                                <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>

                                            </tbody>   
                                        </table>
                                        <button type='button' class="add-saving-account" data-toggle="modal" data-target="#nonRentalPropertyModal">Add property</button>
                                    </div>
                                </div>
                            </div>
                            <textarea id="ownhouse" class="hidden" name="answer[90][own_house_record]">{{(!empty($answer) && array_key_exists('own_house_record',$answer))? $answer["own_house_record"]:null}}</textarea>


                            <div class="sections">
                                <div id="nonRentalPropertyModal" class="modal fade add-student-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD NON-RENTAL PROPERTY</h4>
                                            </div>
                                            <div class="modal-body" style='border:none;'>
                                                <!-- first section starts -->
                                                <div class="row"> 
                                                    <div class=" setup-content" id="step-1">
                                                        <div class="validation-alert">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give the house a name') }}
                                                                    {{ Form::input('text','house_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalHouseName', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Address') }}
                                                                    {{ Form::input('text','address',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalAddress', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','City') }}
                                                                    {{ Form::input('text','city',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalCity', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'city', 'required'=>'required']) }}
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-6 inner-left" style="padding:0; width: 47%; float:left;">
                                                                        {{ Form::label('label','State') }}
                                                                        {{ Form::input('text','state',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalState', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'state', 'required'=>'required','style'=>'width:100%;']) }}
                                                                    </div>
                                                                    <div class="col-sm-6 inner-left" style="padding:0; width: 47%; float:right;">
                                                                        {{ Form::label('label','Zip code') }}
                                                                        {{ Form::input('number','zip_code',null,['data-validation'=> '' , 'style' => 'text-align:left', 'class'=>'custom-validation form-control CPMNonRentalZipCode', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'00000', 'required'=>'required','min'=>0]) }}
                                                                    </div> 
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="nextBtn pull-right" type="button" style="width: auto"> next <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- first section finish -->

                                                    <!-- second step starts -->
                                                    <div class=" setup-content" id="step-2" style="display: none;">
                                                        <div class=" validation-alert">
                                                            <div class="section-right" style="padding-left:0px;">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Estimated value of the house?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','Estimated_value_of_the_house',null,['data-validation'=> '' , 'class'=>'borderLeft0 ustom-validation form-control CPMNonRentalEstimatedValue', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this your primary residence?') }}
                                                                    <label class="radio-custom-label">    
                                                                        {{ Form::radio('Is_this_your_primary_residence', 'yes',false,['class'=>'CPMNonRentalPrimaryResidence','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('Is_this_your_primary_residence', 'no',false,['class'=>'CPMNonRentalPrimaryResidence','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','When did you purchase this house?') }}
                                                                    {{ Form::input('text','When_did_you_purchase_this_house',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control CPMNonRentalPurchasingDate', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Purchase price') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','Purchase_price',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control CPMNonRentalPurchasePrice', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="backBtn pull-left" type="button" style="width:auto"> <i class="fa fa-arrow-left"></i> back</button>
                                                                        <button  class="nextBtn pull-right" type="button" style="width:auto"> next <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- second step finish -->

                                                    <!-- third step starts -->
                                                    <div class=" setup-content" id="step-3" style="display: none;">
                                                        <div class="validation-alert">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    {{ Form::label('label','In whose name is the deed of this house?') }}
                                                                    <select style="position: absolute;"   class="CPMNonRentalDeed" id="CPMNonRentalDeeds" name="deed-first-holder" style="display: none;" required>
                                                                        <option selected disabled value="">SELECT</option>
                                                                        <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="2"><?php echo $spouse_name; ?> </option><?php } ?>
                                                                        <option value="3">other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CPMNonRentalDeedHolder" style="display: none"> 
                                                                    {{ Form::label('label','Deed-holder&#39;s name') }}
                                                                    {{ Form::input('text','Deed-holder-name-first',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalDeedHolderFirst', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'Name', 'required'=>'required' ]) }}
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="backBtn pull-left" type="button" style="width:auto"> <i class="fa fa-arrow-left"></i> back</button>
                                                                        <button  class="nextBtn pull-right" type="button" style="width:auto"> next <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- third step finish -->

                                                    <!-- fourth step starts -->
                                                    <div class=" setup-content" id="step-4" style="display: none;">
                                                        <div class="section-right">
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Do you intend to sell this property at some point?') }}
                                                                    <label class="radio-custom-label">    
                                                                        {{ Form::radio('CPMNonRentalIntendToSell', 'yes',false,['class'=>'CPMNonRentalIntendToSell','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('CPMNonRentalIntendToSell', 'no',false,['class'=>'CPMNonRentalIntendToSell','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>
                                                                <div class="non-rental-hide-section CPMExpectedDateToSell" style="display: <?php
                                                                if (!empty($answer) && array_key_exists('CPMNonRentalIntendToSell', $answer) && ($answer['CPMNonRentalIntendToSell'] == "yes")) {
                                                                    echo 'block';
                                                                } else {
                                                                    echo 'none';
                                                                }
                                                                ?>;">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label','When do you expect to sell this house?') }}
                                                                        {{ Form::input('number','CPMNonRentalExpectedSale',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalExpectedSale', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required' , 'maxlength'=>'4']) }}
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12 inner-left CPMExpectedDateToSell" style="display:none;">
                                                                    {{ Form::label('label','How do you intend to use this proceeds?') }}
                                                                    {{ Form::input('text','CPMNon-rental-proceeds',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRentalProceeds', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'explain', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button class="backBtn pull-left"> <i class="fa fa-arrow-left"></i> back</button>
                                                                        <button id="CPMNonRentalBtnnew" class="nextBtn modal-button pull-right finishBtn" type="button" style="width:auto;">finish <i class="fa fa-arrow-right"></i> </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- fourth step finish -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal finsih -->



@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>


<script>
var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).ready(function () {


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();

    var CPMNonRentalDetails = [];

    if ($('#ownhouse').val() != '') {
        CPMNonRentalDetails = JSON.parse($('#ownhouse').val());
    }

    $('.nextBtn, .finishBtn').on('click', function () {

        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('.finishBtn').hasClass('edit-form')) {
                    $('#nonRentalPropertyModal').modal('hide');
                    CPMNonRentalDetails[$(this).attr('data-index')] = {
                        CPMNonRentalHouseName: $('#nonRentalPropertyModal .CPMNonRentalHouseName').val(),
                        CPMNonRentalAddress: $('#nonRentalPropertyModal .CPMNonRentalAddress').val(),
                        CPMNonRentalCity: $('#nonRentalPropertyModal .CPMNonRentalCity').val(),
                        CPMNonRentalState: $('#nonRentalPropertyModal .CPMNonRentalState').val(),
                        CPMNonRentalZipCode: $('#nonRentalPropertyModal .CPMNonRentalZipCode').val(),

                        CPMNonRentalEstimatedValue: $('#nonRentalPropertyModal .CPMNonRentalEstimatedValue').val(),
                        CPMNonRentalPrimaryResidence: $('#nonRentalPropertyModal .CPMNonRentalPrimaryResidence:checked').val(),
                        CPMNonRentalPurchasingDate: $('#nonRentalPropertyModal .CPMNonRentalPurchasingDate').val(),
                        CPMNonRentalPurchasePrice: $('#nonRentalPropertyModal .CPMNonRentalPurchasePrice').val(),

                        CPMNonRentalDeed: $('#nonRentalPropertyModal .CPMNonRentalDeed').val(),
                        CPMNonRentalDeedHolderFirst: $('#nonRentalPropertyModal .CPMNonRentalDeedHolderFirst').val(),
                        CPMNonRentalDeedNamer: $('#nonRentalPropertyModal .CPMNonRentalDeedNamer').val(),

                        CPMNonRentalIntendToSell: $('#nonRentalPropertyModal .CPMNonRentalIntendToSell:checked').val(),
                        CPMNonRentalExpectedSale: $('#nonRentalPropertyModal .CPMNonRentalExpectedSale').val(),
                        CPMNonRentalProceeds: $('#nonRentalPropertyModal .CPMNonRentalProceeds').val()
                    };
                    $('#nonRetirementInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#nonRentalPropertyModal .CPMNonRentalHouseName').val() + '</td>  <td></td> <td  align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                } else {
                    $('#nonRetirementInfo tbody').append('<tr data-index=' + CPMNonRentalDetails.length + '><td>' + $('#nonRentalPropertyModal .CPMNonRentalHouseName').val() + '</td>   <td> </td><td  align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    CPMNonRentalDetails.push({
                        CPMNonRentalHouseName: $('#nonRentalPropertyModal .CPMNonRentalHouseName').val(),
                        CPMNonRentalAddress: $('#nonRentalPropertyModal .CPMNonRentalAddress').val(),
                        CPMNonRentalCity: $('#nonRentalPropertyModal .CPMNonRentalCity').val(),
                        CPMNonRentalState: $('#nonRentalPropertyModal .CPMNonRentalState').val(),
                        CPMNonRentalZipCode: $('#nonRentalPropertyModal .CPMNonRentalZipCode').val(),

                        CPMNonRentalEstimatedValue: $('#nonRentalPropertyModal .CPMNonRentalEstimatedValue').val(),
                        CPMNonRentalPrimaryResidence: $('#nonRentalPropertyModal .CPMNonRentalPrimaryResidence:checked').val(),
                        CPMNonRentalPurchasingDate: $('#nonRentalPropertyModal .CPMNonRentalPurchasingDate').val(),
                        CPMNonRentalPurchasePrice: $('#nonRentalPropertyModal .CPMNonRentalPurchasePrice').val(),

                        CPMNonRentalDeed: $('#nonRentalPropertyModal .CPMNonRentalDeed').val(),
                        CPMNonRentalDeedHolderFirst: $('#nonRentalPropertyModal .CPMNonRentalDeedHolderFirst').val(),
                        CPMNonRentalDeedNamer: $('#nonRentalPropertyModal .CPMNonRentalDeedNamer').val(),

                        CPMNonRentalIntendToSell: $('#nonRentalPropertyModal .CPMNonRentalIntendToSell:checked').val(),
                        CPMNonRentalExpectedSale: $('#nonRentalPropertyModal .CPMNonRentalExpectedSale').val(),
                        CPMNonRentalProceeds: $('#nonRentalPropertyModal .CPMNonRentalProceeds').val()
                    });
                    $('#nonRentalPropertyModal').modal('hide');
                }
                $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
            }
        }
        return false;
    });

    $(document).on('click', '#nonRetirementInfo .fa-pencil', function () {
        $('#nonRentalPropertyModal').modal();
        $('#nonRentalPropertyModal #step-1').show();
        $('#nonRentalPropertyModal #step-2, #nonRentalPropertyModal #step-3, #nonRentalPropertyModal #step-4').css('display', 'none');
        $('#nonRentalPropertyModal .inner-left input, #nonRentalPropertyModal .inner-left select').parent().find('label.error').remove();
        var index = $(this).closest('tr').attr('data-index');
        CPMNonRentalListDetails = CPMNonRentalDetails[index];
        $('#nonRentalPropertyModal .CPMNonRentalHouseName').val(CPMNonRentalListDetails.CPMNonRentalHouseName);
        $('#nonRentalPropertyModal .CPMNonRentalAddress').val(CPMNonRentalListDetails.CPMNonRentalAddress);
        $('#nonRentalPropertyModal .CPMNonRentalCity').val(CPMNonRentalListDetails.CPMNonRentalCity);
        $('#nonRentalPropertyModal .CPMNonRentalState').val(CPMNonRentalListDetails.CPMNonRentalState);
        $('#nonRentalPropertyModal .CPMNonRentalZipCode').val(CPMNonRentalListDetails.CPMNonRentalZipCode);

        $('#nonRentalPropertyModal .CPMNonRentalEstimatedValue').val(CPMNonRentalListDetails.CPMNonRentalEstimatedValue);
        $('#nonRentalPropertyModal .CPMNonRentalPrimaryResidence[value=' + CPMNonRentalListDetails.CPMNonRentalPrimaryResidence + ']').prop('checked', true);
        $('#nonRentalPropertyModal .CPMNonRentalPurchasingDate').val(CPMNonRentalListDetails.CPMNonRentalPurchasingDate);
        $('#nonRentalPropertyModal .CPMNonRentalPurchasePrice').val(CPMNonRentalListDetails.CPMNonRentalPurchasePrice);

        $('#nonRentalPropertyModal .CPMNonRentalDeed').val(CPMNonRentalListDetails.CPMNonRentalDeed).trigger('change');
        $('#nonRentalPropertyModal .CPMNonRentalDeedHolderFirst').val(CPMNonRentalListDetails.CPMNonRentalDeedHolderFirst);
        $('#nonRentalPropertyModal .CPMNonRentalDeedNamer').val(CPMNonRentalListDetails.CPMNonRentalDeedNamer).trigger('change');

        $('#nonRentalPropertyModal .CPMNonRentalIntendToSell[value=' + CPMNonRentalListDetails.CPMNonRentalIntendToSell + ']').prop('checked', true);
        $('#nonRentalPropertyModal .CPMNonRentalExpectedSale').val(CPMNonRentalListDetails.CPMNonRentalExpectedSale);
        $('#nonRentalPropertyModal .CPMNonRentalProceeds').val(CPMNonRentalListDetails.CPMNonRentalProceeds);

        $('#CPMNonRentalBtnnew').addClass('edit-form');
        $('#CPMNonRentalBtnnew').attr('data-index', index);

        if ($('.CPMNonRentalIntendToSell:checked').val() == 'yes') {
            $('.CPMExpectedDateToSell').show();
        } else {
            $('.CPMExpectedDateToSell').hide();
        }
    });

    $("select").selectBoxIt();

    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    $('.CPMNonRentalExpectedSale').datetimepicker({
        format: 'YYYY'
    });

    $(document).on('change', '.own-house', function () {
        if ($(this).val() == 'yes') {
            $('.non-rental-table').show();
        } else {
            $('.non-rental-table').hide();
            CPMNonRentalDetails = [];
            $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
            $('.non-rental-table tbody tr').remove();
        }
    });

    $(document).on('click', '.fa-trash', function () {
        $(this).closest('tr').remove();
        CPMNonRentalDetails.splice($(this).closest('tr').attr('data-index'), 1);

        $("#nonRetirementInfo tbody").empty();
        if (CPMNonRentalDetails.length != 0) {
            var tr = '';
            $.each(CPMNonRentalDetails, function (key, value) {
                tr += '<tr data-index="' + key + '"><td>' + value.CPMNonRentalHouseName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
            });
            $("#nonRetirementInfo tbody").html(tr);
        }

        $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
        return false;
    });

    $(document).on('click', '.add-saving-account', function () { // <-- changes
        $('#step-1').show();
        $('#step-2, #step-2, #step-3, #step-4').hide();
        $('#nonRentalPropertyModal input[type=text], #nonRentalPropertyModal input[type=number]').val('');
        $('#CPMNonRentalBtnnew').removeClass('edit-form');
        $('#nonRentalPropertyModal input[type="radio"]').prop('checked', false);
        $('#nonRentalPropertyModal .CPMNonRentalDeed, #nonRentalPropertyModal .CPMNonRentalDeedNamer').val('').trigger('change');
        $('#nonRentalPropertyModal .CPMNonRentalDeedBox, #nonRentalPropertyModal .CPMNonRentalDeedNamerBox, #nonRentalPropertyModal .CPMExpectedDateToSell').css('display', 'none');
    });

    $("#nonRentalPropertyModal .CPMNonRentalDeed").on('change', function () {
        if ($(this).val() === '3') {
            $('#nonRentalPropertyModal .CPMNonRentalDeedHolder').css('display', 'block');
        } else {
            $('#nonRentalPropertyModal .CPMNonRentalDeedHolder').css('display', 'none');
            $('#nonRentalPropertyModal .CPMNonRentalDeedHolderFirst').val('');
        }
    });

    $("#nonRentalPropertyModal .CPMNonRentalDeedNamer").on('change', function () {
        if ($(this).val() === '8') {
            $('#nonRentalPropertyModal .CPMNonRentalDeedNamerBox').css('display', 'block');
        } else {
            $('#nonRentalPropertyModal .CPMNonRentalDeedNamerBox').css('display', 'none');
        }

        if ($(this).val() === '4') {
            $('#nonRentalPropertyModal .CPMNonSingleName').css('display', 'block');
        } else {
            $('#nonRentalPropertyModal .CPMNonSingleName').css('display', 'none');
            $('#nonRentalPropertyModal .CPMNonSingleName').val('');
        }
    });

    if ($('#nonRentalPropertyModal .CPMNonRentalDeedNamer').val() === '4') {
        $('#nonRentalPropertyModal .CPMNonSingleName').css('display', 'block');
    } else {
        $('#nonRentalPropertyModal .CPMNonSingleName').css('display', 'none');
        $('#nonRentalPropertyModal .CPMNonSingleName').val('');
    }

    if ($('#nonRentalPropertyModal .CPMNonRentalDeed').val() === '3') {
        $('#nonRentalPropertyModal .CPMNonRentalDeedHolder').css('display', 'block');
    } else {
        $('#nonRentalPropertyModal .CPMNonRentalDeedHolder').css('display', 'none');
    }

    if ($('#nonRentalPropertyModal .CPMNonRentalDeedNamer').val() === '8') {
        $('#nonRentalPropertyModal .CPMNonRentalDeedNamerBox').css('display', 'block');
    } else {
        $('.CPMNonRentalDeedNamerBox').css('display', 'none');
    }

    $(document).on('change', '.CPMNonRentalIntendToSell', function () {
        if ($(this).val() == 'yes') {
            $('.CPMExpectedDateToSell').show();
        } else {
            $('.CPMExpectedDateToSell').hide();
            $('.CPMNonRentalExpectedSale, .CPMNonRentalProceeds').val('');

        }
    });
});

</script> 
@stop 