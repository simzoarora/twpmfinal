@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',20) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}

                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIABILITIES</h6>      
                                    <h2>Mortgages on non-rental property</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[78]','Mortgages on non-rental property') }}
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label', 'Do you have any mortgages on non-rental property?') }}
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[78][CPMMortgageConfirmation]', 'yes',(!empty($answer) && array_key_exists('CPMMortgageConfirmation',$answer)) ? (($answer['CPMMortgageConfirmation']=="yes")  ? true : false) :false,['class'=>'cpmMortgageConfirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label> 
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[78][CPMMortgageConfirmation]', 'no',(!empty($answer) && array_key_exists('CPMMortgageConfirmation',$answer)) ? (($answer['CPMMortgageConfirmation']=="no")  ? true : false) :false,['class'=>'cpmMortgageConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left cpmDetails-table" style="display:  <?php
                                    if (!empty($answer) && array_key_exists('CPMMortgageConfirmation', $answer) && ($answer['CPMMortgageConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12  ">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <div class="row">
                                                        <div class="add-child add-property">

                                                            <table id='cpmproperty-info' class="find-table-length">
                                                                <thead>
                                                                    <tr> 
                                                                        <td colspan="2">List of all non-rental mortgages.</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Complete property Address</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (!empty($answer["CPMNonRentalMortgagesRecords"])) {
                                                                        $mortgage = json_decode($answer["CPMNonRentalMortgagesRecords"]);
                                                                        if (!empty($mortgage)) {
                                                                            foreach ($mortgage as $key => $data) {
                                                                                ?>
                                                                                <tr data-index="{{$key}}">
                                                                                    <td>{{$data->propertyName}}</td>
                                                                                    <td valign="top" style="text-align:right;">
                                                                                        <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                        <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table> 
                                                            <div class="col-md-6 ">
                                                                <div class="row">
                                                                    <button type='button' class="add-dependent CPM-add-mortgage" data-toggle="modal" data-target="#cpmPropertyModal">Add mortgage</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <textarea id="cpmnonRentalMortgagesData" class="hidden" name="answer[78][CPMNonRentalMortgagesRecords]">{{(!empty($answer["CPMNonRentalMortgagesRecords"]))? $answer["CPMNonRentalMortgagesRecords"]:null}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sections">
                                <div id="cpmPropertyModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD RENTAL PROPERTY</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <!-- first step starts -->
                                                    <div class=" setup-content" id="step-1">
                                                        <div class="section-right">
                                                            {{ Form::input('hidden','questionName[78]','ADD MORTGAGE') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'For which Property?') }}
                                                                {{ Form::input('text','property_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-property-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Street Address']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What was the original mortgage amount?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        $  
                                                                    </span>
                                                                    {{ Form::input('number','original_mortgage_amount',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-mortgage-original-amount borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'When did the mortgage begin?') }}
                                                                {{ Form::input('text','motrgage_begin_dat',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker cpm-annual-mortgage-begin-datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What is current mortgage balance?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        $
                                                                    </span>
                                                                    {{ Form::input('number','mortgage_balance', null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-mortgage-balance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 ">
                                                                <a class="nextBtn pull-right cursor-pointer" type="button" >next
                                                                    <i class="fa fa-arrow-right"></i>
                                                                </a>
                                                            </div>
                                                        </div>



                                                    </div>
                                                    <!-- first step finsih -->
                                                    <!-- second step starts -->
                                                    <div class="setup-content" id="step-2" style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="mortgage-tabed mortgageSecondStep inner-left">
                                                                <label>What type of mortgage is it?</label> 
                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0"> 
                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-one', 'Fixed', ['class' => 'label', 'for' => 'option-one'])}}
                                                                        {{ Form::radio('selector', 'fixed',null,['class'=>'cpm-annual-type-mortgage',  'id' => 'option-one', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-two', 'Variable', ['class' => 'label', 'for' => 'option-two'])}}
                                                                        {{ Form::radio('selector', 'variable',null,['class'=>'cpm-annual-type-mortgage',  'id' => 'option-two', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        <label class="label" for="option-three" style="line-height: normal; padding: 14px 5px;">Interest <br> - Only </label>
                                                                        <!--{{Form::label('option-three', 'Interest <br> - Only  ', ['class' => 'label', 'for' => 'option-three'])}}-->
                                                                        {{ Form::radio('selector', 'interestonly',null,['class'=>'cpm-annual-type-mortgage',  'id' => 'option-three', ' required'=>'required']) }}

                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-four', 'Other', ['class' => 'label', 'for' => 'option-four'])}}
                                                                        {{ Form::radio('selector', 'other',null,['class'=>'cpm-annual-type-mortgage',  'id' => 'option-four', ' required'=>'required']) }}
                                                                    </div>   
                                                                </div>

                                                                <div class="tab-content row">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is Interest Rate?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','mortgage interest_rate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-interest-rate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <span class="input-group-addon">%</span>
                                                                        </div> 
                                                                    </div> 
                                                                    <!-- fixed tab content starts --> 
                                                                    <div id="fixed" class="tab-pane fade">

                                                                    </div>
                                                                    <!-- fixed tab content finish -->
                                                                    <!-- variable tab content starts -->
                                                                    <div id="variable" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'In how many years after origination do the adjustments begin?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','adjustments_begin',null,['data-validation'=> '' , 'class'=>'custom-validation form-control  cpm-annual-adjustments-begin borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">years</span>
                                                                            </div> 
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'How often does it adjust?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','adjust_often',null,['data-validation'=> '' , 'class'=>'custom-validation form-control   cpm-annual-adjust-oftennumber borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon" style="position: relative;padding: 0 10px;">
                                                                                    <p style="position: absolute;margin: 0;padding: 0;top: 0;z-index: 100;width: 100%;left: 0;" class=" text-center"><i class="fa fa-angle-up"></i></p>
                                                                                    <input type="text" name="adjust_often" class="select-val  cpm-annual-adjust-often" readonly="" value="Years" style="text-transform: capitalize;outline: none;width: 50px!important;border: 0;text-align: center;float: left;padding: 0!important;height: auto;font-size: 14px;margin: 0;line-height: initial;margin: 0;">
                                                                                    <p style="margin:0;padding: 0;position: absolute;left: 0;bottom: 0;width: 100%;" class="col-xs-12 col-sm-12 text-center"><i class="fa fa-angle-down"></i></p>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Interest rate cap (leave blank if there is no rate cap).') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','interest_rate_cap',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-interest-rate-cap borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">%</span>
                                                                            </div> 
                                                                        </div>  
                                                                    </div>
                                                                    <!-- variable tab content finish --> 
                                                                    <!-- interest tab content starts --> 
                                                                    <div id="interestOnly" class="tab-pane fade">

                                                                        <div class="col-sm-12 inner-left">
                                                                            <i class="fa fa-percentage" aria-hidden="true"></i>
                                                                            {{ Form::label('label', 'For what number of years is the loan interest-only?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_years_interest',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-loan-years-interest borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">years</span>
                                                                            </div> 
                                                                        </div> 

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Is the interest rate fixed during the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'yes',false,['class'=>'cpm-annual-fixed-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'no',false,['class'=>'cpm-annual-fixed-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Does the interest rate float after the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'yes',false,['class'=>'cpm-annual-float-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'no',false,['class'=>'cpm-annual-float-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>


                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                            <select style="position: absolute;" class="status valid cpm-annual-mortgage-interest-dropdown" id="mortgageothersdropdown"  name="interest_rate_based" required="" aria-invalid="false" style="display: none;">
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">10-year Treasury</option>
                                                                                <option value="2">Prime</option>
                                                                                <option value="3">Libor</option>
                                                                                <option value="4">Unsure</option>
                                                                                <option value="5">Other</option>
                                                                            </select>
                                                                        </div> 

                                                                        <div class="col-sm-12 inner-left mortgageothers" style="display: none">
                                                                            {{ Form::label('label', 'Other') }}
                                                                            <!--                                                                            <div class="input-group">
                                                                                                                                                            <span class="input-group-addon">
                                                                                                                                                                <i class="fa fa-dollar"></i>    
                                                                                                                                                            </span>-->
                                                                            {{ Form::input('text','mortgage_other]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-mortgage-other ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <!--</div>-->
                                                                        </div>
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label','Does this loan negatively amortize?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                            <select style="position: absolute;" class="status valid cpm-annual-negatively-amortize" name="negatively_amortize" required="" aria-invalid="false" >
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">Yes</option>
                                                                                <option value="2">No</option>
                                                                                <option value="3">I don't know</option></select>
                                                                        </div>
                                                                    </div>


                                                                    <div id="others" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter type of Mortgage?') }}
                                                                            {{ Form::textarea('mortgage_type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-mortgage-type','rows'=>'3','cols'=>'4', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'describe']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>



                                                                <div class="col-sm-12">
                                                                    <!--id='childrenForm'-->  
                                                                    <button  class='backBtn pull-left cursor-pointer' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right cursor-pointer' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                    <!-- second step finsih -->
                                                    <!-- third step starts -->
                                                    <div class="setup-content" id="step-3"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts --> 
                                                            <div class="mortgage-tabed loan-term-step inner-left">
                                                                <label>What is loan term?</label> 

                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0"> 
                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-five', '15yr', ['class' => 'label', 'for' => 'option-five'])}}
                                                                        {{ Form::radio('yearselection', 'fifteenyr',null,['class'=>'cpm-annual-fixed-year-selection',  'id' => 'option-five', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-six', '30yr', ['class' => 'label', 'for' => 'option-six'])}}
                                                                        {{ Form::radio('yearselection', 'thirtyyear',null,['class'=>'cpm-annual-fixed-year-selection',  'id' => 'option-six', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-seven', 'Other', ['class' => 'label', 'for' => 'option-seven'])}}
                                                                        {{ Form::radio('yearselection', 'otheryear',null,['class'=>'cpm-annual-fixed-year-selection',  'id' => 'option-seven', ' required'=>'required']) }}
                                                                    </div>
                                                                </div>



                                                                <div class="tab-content row">
                                                                    <!-- fiftenyr tab content starts --> 
                                                                    <div id="fiftenyr" class="tab-pane fade in"></div>
                                                                    <!-- fiftenyr tab content finish -->
                                                                    <!-- thirtyyr tab content starts -->
                                                                    <div id="thirtyyr" class="tab-pane fade"></div>
                                                                    <!-- thirtyyr tab content finish --> 

                                                                    <div id="loantermothers" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter loan term') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_term',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-enter-loan-term borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">years</span>
                                                                            </div> 
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12">
                                                                        <button  class='backBtn pull-left cursor-pointer' type="button">
                                                                            <i class="fa fa-arrow-left"></i>
                                                                            back
                                                                        </button>
                                                                        <button  class='nextBtn pull-right cursor-pointer' type="button">
                                                                            next
                                                                            <i class="fa fa-arrow-right"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <!-- tabbed pannel finsih -->
                                                        </div>
                                                    </div>
                                                    <!-- third step finsih -->
                                                    <!-- fourth step starts -->
                                                    <div class="setup-content" id="step-4"  style="display: none">
                                                        <div class="section-right">
                                                            <div class="col-sm-12 inner-left priv-insurace">
                                                                {{ Form::label('label', 'Do you pay Private Mortgage Insurance (PMI) on this loan?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'yes',false,['class'=>'cpm-annual-private-insurance','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'no',false,['class'=>'cpm-annual-private-insurance','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-pmi" style="display:none;">
                                                                {{ Form::label('label', 'Is PMI paid as a seperate itemized payment on your mortgage statement?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'yes',false,['class'=>'cpm-annual-mortgage-pmi-confirmation','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'no',false,['class'=>'cpm-annual-mortgage-pmi-confirmation','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-paid-monthly"  style="display:none;">
                                                                {{ Form::label('label', 'What amount is paid monthly?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-dollar"></i>    
                                                                    </span>
                                                                    {{ Form::input('number','mortgage_paid_monthly',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpm-annual-mortgage-paid-monthly-box borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div> 

                                                            <div class="col-sm-12">
                                                                <button  class='backBtn pull-left cursor-pointer' type="button">
                                                                    <i class="fa fa-arrow-left"></i>
                                                                    back
                                                                </button>
                                                                <button  class='nextBtn pull-right cursor-pointer' type="button">
                                                                    next
                                                                    <i class="fa fa-arrow-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fourth step finish -->
                                                    <!-- fifth step starts -->
                                                    <div class="setup-content mortgage-last-step" id="step-5"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="latest-statement"> 

                                                                <div class="col-sm-12 inner-left">
                                                                    <label>Please enter the following amounts from your latest statement?</label> 
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Principal', [ 'class' => 'single-line'] ) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input('number','prinicpal_amount',null,['data-validation'=> '' , 'class'=>'qty1 inlineinput custom-validation form-control cpm-annual-mortgage-prinicpal-amount', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v0', 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Interest' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input('number','loan_interest',null,['data-validation'=> '' , 'class'=>'qty1 cpm-annual-mortgage-loan-interest inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v1', 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Property Tax' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input('number','property_tax',null,['data-validation'=> '' , 'class'=>'qty1 cpm-annual-mortgage-property-tax inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true ,  'id' => 'v2','placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Private mortgage Insurance',[ 'class' => 'double-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input('number','private_motor_insurance',null,['data-validation'=> '' , 'class'=>'qty1 cpm-annual-mortgage-private-motor-insurance inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v3', 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Other (CDD fees, etc.)',[ 'class' => 'double-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input('number','others_cdd_fees',null,['data-validation'=> '' , 'class'=>'qty1 cpm-annual-mortgage-others-cdd-fees inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v4', 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        <h4  class="single-line">Monthly total</h4>
                                                                    </div> 
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <i class="fa fa-dollar bottom" aria-hidden="true" style="top:17px"></i>
                                                                        {{ Form::input(' number','monthly_total',null,['data-validation'=> '' ,'readonly' => 'readonly',  'class'=>'cpm-annual-mortgage-monthly-total inlineinput custom-validation form-control', 'style' => 'border:0; border-top:solid 2px #000; text-align:right', 'data-rule-cpm-annual-mortgage-monthly-total inlineinput custom-validation form-controlregex'=>"false", 'readonly'=>true , 'id' => 'result', 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'If property taxes, are not paid in the mortgage, what are the annual property taxes?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-dollar"></i>    
                                                                        </span>
                                                                        {{ Form::input('number','tax_not_paidl',null,['data-validation'=> '', 'class'=>'cpm-annual-mortgage-tax-not-paid inlineinput custom-validation form-control borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <!--id='childrenForm'-->  
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button id="cpm-finish-mortgage-New" class='finishCPMMortgage finishBtn pull-right' type="button" >
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fifth step finsih -->  
                                                    <!-- Form ends here -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{asset('js/comprehensive-planning.js')}}"></script>
<script>
var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";</script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        $('.fa-angle-up').on('click', function () {
            $('.select-val').val('Months');
        });
        $('.fa-angle-down').on('click', function () {
            $('.select-val').val('Years');
        });

        var childrenList = [];
        $(document).on('change', '.cpmMortgageConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.cpmDetails-table').show();
            } else {
                $('.cpmDetails-table').hide();
                childrenList = [];
                $('#cpmnonRentalMortgagesData').html(JSON.stringify(childrenList));
                $('.cpmDetails-table tbody tr').remove();
            }
        });
        if ($('#cpmnonRentalMortgagesData').val() != '') {
            childrenList = JSON.parse($('#cpmnonRentalMortgagesData').val());
        }

        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        childrenList[$(this).attr('data-index')] = {
                            cpmAnnualMortgagefirstCheck: $('.cpmMortgageConfirmation:checked').val(),
                            propertyName: $('#cpmPropertyModal .cpm-annual-property-name').val(),
                            cpmAnnualMortgageOriginalAmount: $('#cpmPropertyModal .cpm-annual-mortgage-original-amount').val(),
                            cpmAnnualMortgageBeginDatetimepicker: $('#cpmPropertyModal .cpm-annual-mortgage-begin-datetimepicker').val(),
                            cpmAnnualMortgageBalance: $('#cpmPropertyModal .cpm-annual-mortgage-balance').val(),

                            cpmAnnualMortgageInterestRate: $('#cpmPropertyModal .mortgage-interest-rate').val(),
                            cpmAnnualMortgageAdjustmentsBegin: $('#cpmPropertyModal .cpm-annual-adjustments-begin').val(),
                            cpmAnnualMortgageAdjustOftenNumber: $('#cpmPropertyModal .cpm-annual-adjust-oftennumber').val(),
                            cpmAnnualMortgageAdjustOften: $('#cpmPropertyModal .cpm-annual-adjust-often').val(),
                            cpmAnnualConcatenate: $('#cpmPropertyModal .cpm-annual-adjust-oftennumber').val() + '' + $('#cpmPropertyModal .cpm-annual-adjust-often').val(),
                            cpmAnnualMortgageInterestRateCap: $('#cpmPropertyModal .cpm-annual-interest-rate-cap').val(),
                            cpmAnnualMortgageLoanYearsInterest: $('#cpmPropertyModal .cpm-annual-loan-years-interest').val(),
                            cpmAnnualMortgageFixedInterestConfirmation: $('#cpmPropertyModal .cpm-annual-fixed-interest-confirmation:checked').val(),
                            cpmAnnualMortgageFloatInterestConfirmation: $('#cpmPropertyModal .cpm-annual-float-interest-confirmation:checked').val(),
                            cpmAnnualMortgageInterestDropdown: $('#cpmPropertyModal .cpm-annual-mortgage-interest-dropdown').val(),
                            cpmAnnualMortgageOther: $('#cpmPropertyModal .cpm-annual-mortgage-other').val(),
                            cpmAnnualMortgageNegativelyAmortize: $('#cpmPropertyModal .cpm-annual-negatively-amortize').val(),
                            cpmAnnualMortgageType: $('#cpmPropertyModal .cpm-annual-mortgage-type').val(),

                            cpmAnnualMortgageFixedYearSelection: $('#cpmPropertyModal .cpm-annual-fixed-year-selection:checked').val(),
                            cpmAnnualMortgageTypeMortgage: $('#cpmPropertyModal .cpm-annual-type-mortgage:checked').val(),

                            cpmAnnualMortgageEnterLoanTerm: $('#cpmPropertyModal .cpm-annual-enter-loan-term').val(),

                            cpmAnnualMortgagePrivateInsurance: $('#cpmPropertyModal .cpm-annual-private-insurance:checked').val(),
                            cpmAnnualMortgagePmiConfirmation: $('#cpmPropertyModal .cpm-annual-mortgage-pmi-confirmation:checked').val(),
                            cpmAnnualMortgagePaidMonthly: $('#cpmPropertyModal .cpm-annual-mortgage-paid-monthly-box').val(),

                            cpmAnnualMortgagePrinicpalAmount: $('#cpmPropertyModal .cpm-annual-mortgage-prinicpal-amount').val(),
                            cpmAnnualMortgageMonthlyTotal: $('#cpmPropertyModal .cpm-annual-mortgage-monthly-total').val(),
                            cpmAnnualMortgageLoanInterest: $('#cpmPropertyModal .cpm-annual-mortgage-loan-interest').val(),
                            cpmAnnualMortgagePropertyTax: $('#cpmPropertyModal .cpm-annual-mortgage-property-tax').val(),
                            cpmAnnualMortgagePrivateMotorInsurance: $('#cpmPropertyModal .cpm-annual-mortgage-private-motor-insurance').val(),
                            cpmAnnualMortgageOthersCddFees: $('#cpmPropertyModal .cpm-annual-mortgage-others-cdd-fees').val(),
                            cpmAnnualMortgageTaxNotPaid: $('#cpmPropertyModal .cpm-annual-mortgage-tax-not-paid').val()
                        };
                        $('#cpmproperty-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#cpmPropertyModal .cpm-annual-property-name').val() + '</td>  <td valign="top" style="text-align:right;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#cpmproperty-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#cpmPropertyModal .cpm-annual-property-name').val() + '</td>  <td valign="top" style="text-align:right;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        childrenList.push({
                            cpmAnnualMortgagefirstCheck: $('.cpmMortgageConfirmation:checked').val(),
                            propertyName: $('#cpmPropertyModal .cpm-annual-property-name').val(),
                            cpmAnnualMortgageOriginalAmount: $('#cpmPropertyModal .cpm-annual-mortgage-original-amount').val(),
                            cpmAnnualMortgageBeginDatetimepicker: $('#cpmPropertyModal .cpm-annual-mortgage-begin-datetimepicker').val(),
                            cpmAnnualMortgageBalance: $('#cpmPropertyModal .cpm-annual-mortgage-balance').val(),
                            cpmAnnualMortgageInterestRate: $('#cpmPropertyModal .mortgage-interest-rate').val(),
                            cpmAnnualMortgageAdjustmentsBegin: $('#cpmPropertyModal .cpm-annual-adjustments-begin').val(),
                            cpmAnnualMortgageAdjustOftenNumber: $('#cpmPropertyModal .cpm-annual-adjust-oftennumber').val(),
                            cpmAnnualMortgageAdjustOften: $('#cpmPropertyModal .cpm-annual-adjust-often').val(),
                            cpmAnnualConcatenate: $('#cpmPropertyModal .cpm-annual-adjust-oftennumber').val() + '' + $('#cpmPropertyModal .cpm-annual-adjust-often').val(),
                            cpmAnnualMortgageInterestRateCap: $('#cpmPropertyModal .cpm-annual-interest-rate-cap').val(),
                            cpmAnnualMortgageLoanYearsInterest: $('#cpmPropertyModal .cpm-annual-loan-years-interest').val(),
                            cpmAnnualMortgageFixedInterestConfirmation: $('#cpmPropertyModal .cpm-annual-fixed-interest-confirmation:checked').val(),
                            cpmAnnualMortgageFloatInterestConfirmation: $('#cpmPropertyModal .cpm-annual-float-interest-confirmation:checked').val(),
                            cpmAnnualMortgageInterestDropdown: $('#cpmPropertyModal .cpm-annual-mortgage-interest-dropdown').val(),
                            cpmAnnualMortgageOther: $('#cpmPropertyModal .cpm-annual-mortgage-other').val(),
                            cpmAnnualMortgageNegativelyAmortize: $('#cpmPropertyModal .cpm-annual-negatively-amortize').val(),
                            cpmAnnualMortgageType: $('#cpmPropertyModal .cpm-annual-mortgage-type').val(),

//                            secondStepNestedTabs: $('#cpmPropertyModal .mortgageSecondStep ul.mortgage-nav li.active a').text(),
//                            loanTerm: $('#cpmPropertyModal .loan-term-step ul.mortgage-nav li.active a').text(),

                            cpmAnnualMortgageFixedYearSelection: $('#cpmPropertyModal .cpm-annual-fixed-year-selection:checked').val(),
                            cpmAnnualMortgageTypeMortgage: $('#cpmPropertyModal .cpm-annual-type-mortgage:checked').val(),
                            cpmAnnualMortgageEnterLoanTerm: $('#cpmPropertyModal .cpm-annual-enter-loan-term').val(),
                            cpmAnnualMortgagePrivateInsurance: $('#cpmPropertyModal .cpm-annual-private-insurance:checked').val(),
                            cpmAnnualMortgagePmiConfirmation: $('#cpmPropertyModal .cpm-annual-mortgage-pmi-confirmation:checked').val(),
                            cpmAnnualMortgagePaidMonthly: $('#cpmPropertyModal .cpm-annual-mortgage-paid-monthly-box').val(),
                            cpmAnnualMortgagePrinicpalAmount: $('#cpmPropertyModal .cpm-annual-mortgage-prinicpal-amount').val(),
                            cpmAnnualMortgageLoanInterest: $('#cpmPropertyModal .cpm-annual-mortgage-loan-interest').val(),
                            cpmAnnualMortgagePropertyTax: $('#cpmPropertyModal .cpm-annual-mortgage-property-tax').val(),
                            cpmAnnualMortgagePrivateMotorInsurance: $('#cpmPropertyModal .cpm-annual-mortgage-private-motor-insurance').val(),
                            cpmAnnualMortgageOthersCddFees: $('#cpmPropertyModal .cpm-annual-mortgage-others-cdd-fees').val(),
                            cpmAnnualMortgageTaxNotPaid: $('#cpmPropertyModal .cpm-annual-mortgage-tax-not-paid').val(),
                            cpmAnnualMortgageMonthlyTotal: $('#cpmPropertyModal .cpm-annual-mortgage-monthly-total').val()
                        });
                    }
                    $('#cpmnonRentalMortgagesData').html(JSON.stringify(childrenList));
                    $('#cpmPropertyModal').modal('hide');
                }

            }
            return false;
        });
        $(document).on('click', '#cpmproperty-info .fa-pencil', function () {
            $('#cpmPropertyModal').modal();
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            var index = $(this).closest('tr').attr('data-index');
            propertyDetails = childrenList[index];
            $('cpmMortgageConfirmation:checked').val(propertyDetails.cpmAnnualMortgagefirstCheck);
            $('#cpmPropertyModal .cpm-annual-property-name').val(propertyDetails.propertyName);
            $('#cpmPropertyModal .cpm-annual-mortgage-original-amount').val(propertyDetails.cpmAnnualMortgageOriginalAmount);
            $('#cpmPropertyModal .cpm-annual-mortgage-begin-datetimepicker').val(propertyDetails.cpmAnnualMortgageBeginDatetimepicker);
            $('#cpmPropertyModal .cpm-annual-mortgage-balance').val(propertyDetails.cpmAnnualMortgageBalance);
            $('#cpmPropertyModal .mortgage-interest-rate').val(propertyDetails.cpmAnnualMortgageInterestRate);
            $('#cpmPropertyModal .cpm-annual-adjustments-begin').val(propertyDetails.cpmAnnualMortgageAdjustmentsBegin);
            $('#cpmPropertyModal .cpm-annual-adjust-oftennumber').val(propertyDetails.cpmAnnualMortgageAdjustOftenNumber);
            $('#cpmPropertyModal .cpm-annual-adjust-often').val(propertyDetails.cpmAnnualMortgageAdjustOften);
            $('#cpmPropertyModal .cpm-annual-interest-rate-cap').val(propertyDetails.cpmAnnualMortgageInterestRateCap);
            $('#cpmPropertyModal .cpm-annual-loan-years-interest').val(propertyDetails.cpmAnnualMortgageLoanYearsInterest);

            $('#cpmPropertyModal .cpm-annual-fixed-interest-confirmation[value=' + propertyDetails.cpmAnnualMortgageFixedInterestConfirmation + ']').prop('checked', true);
            $('#cpmPropertyModal .cpm-annual-float-interest-confirmation[value=' + propertyDetails.cpmAnnualMortgageFloatInterestConfirmation + ']').prop('checked', true);
            $('#cpmPropertyModal .cpm-annual-mortgage-interest-dropdown').val(propertyDetails.cpmAnnualMortgageInterestDropdown).trigger('change');
            ;
            $('#cpmPropertyModal .cpm-annual-mortgage-other').val(propertyDetails.cpmAnnualMortgageOther);
            $('#cpmPropertyModal .cpm-annual-negatively-amortize').val(propertyDetails.cpmAnnualMortgageNegativelyAmortize).trigger('change');
            ;
            $('#cpmPropertyModal .cpm-annual-mortgage-type').val(propertyDetails.cpmAnnualMortgageType);

//            $('#cpmPropertyModal .mortgageSecondStep ul.mortgage-nav li.active a').text(propertyDetails.secondMortgageTab);
//            $('#cpmPropertyModal .loan-term-step ul.mortgage-nav li.active a').text(propertyDetails.loanTermSteps);

            $('#cpmPropertyModal .cpm-annual-fixed-year-selection[value=' + propertyDetails.cpmAnnualMortgageFixedYearSelection + ']').prop('checked', true);
            $('#cpmPropertyModal .cpm-annual-type-mortgage[value=' + propertyDetails.cpmAnnualMortgageTypeMortgage + ']').prop('checked', true);

            $('#cpmPropertyModal .cpm-annual-enter-loan-term').val(propertyDetails.cpmAnnualMortgageEnterLoanTerm);
            $('#cpmPropertyModal .cpm-annual-private-insurance[value=' + propertyDetails.cpmAnnualMortgagePrivateInsurance + ']').prop('checked', true);
            $('#cpmPropertyModal .cpm-annual-mortgage-pmi-confirmation[value=' + propertyDetails.cpmAnnualMortgagePmiConfirmation + ']').prop('checked', true);

            $('#cpmPropertyModal .cpm-annual-mortgage-paid-monthly-box').val(propertyDetails.cpmAnnualMortgagePaidMonthly);
            $('#cpmPropertyModal .cpm-annual-mortgage-prinicpal-amount').val(propertyDetails.cpmAnnualMortgagePrinicpalAmount);
            $('#cpmPropertyModal .cpm-annual-mortgage-loan-interest').val(propertyDetails.cpmAnnualMortgageLoanInterest);
            $('#cpmPropertyModal .cpm-annual-mortgage-property-tax').val(propertyDetails.cpmAnnualMortgagePropertyTax);
            $('#cpmPropertyModal .cpm-annual-mortgage-private-motor-insurance').val(propertyDetails.cpmAnnualMortgagePrivateMotorInsurance);
            $('#cpmPropertyModal .cpm-annual-mortgage-others-cdd-fees').val(propertyDetails.cpmAnnualMortgageOthersCddFees);
            $('#cpmPropertyModal .cpm-annual-mortgage-tax-not-paid').val(propertyDetails.cpmAnnualMortgageTaxNotPaid);
            $('#cpmPropertyModal .cpm-annual-mortgage-monthly-total').val(propertyDetails.cpmAnnualMortgageMonthlyTotal);
            $('#cpm-finish-mortgage-New').addClass('edit-form');
            $('#cpm-finish-mortgage-New').attr('data-index', index);
            // for second step fixed variable interst-only and others

            if ($('.cpm-annual-private-insurance:checked').val() === 'yes') {
                $('#cpmPropertyModal .error-alert').remove();
                $('.mortgage-pmi').show();
            } else {
                $('.mortgage-pmi').hide();
                $('.cpm-annual-mortgage-pmi-confirmation').prop('checked', false);
            }

            if ($('.cpm-annual-mortgage-pmi-confirmation:checked').val() === 'yes') {
                $('#cpmPropertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }

            if ($('.cpm-annual-type-mortgage:checked').val() === 'fixed') {
                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }

            if ($('.cpm-annual-type-mortgage:checked').val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }

            if ($('.cpm-annual-type-mortgage:checked').val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }

            if ($('.cpm-annual-type-mortgage:checked').val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }

            //  this is for the second step tab panne finsh



            //   this is for the third step tab panne


            if ($('.cpm-annual-fixed-year-selection:checked').val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }

            if ($('.cpm-annual-fixed-year-selection:checked').val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#loantermothers, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }

            if ($('.cpm-annual-fixed-year-selection:checked').val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
            // this is the third  step finsih

        });


//edit finish
        $(document).on('click', '.CPM-add-mortgage', function () {
            $('#step-1').show();
            $('#cpmPropertyModal .error-alert').remove();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            $('#cpm-finish-mortgage-New').removeClass('edit-form');
            $('#cpmPropertyModal  .cpm-annual-mortgage-monthly-total, #cpmPropertyModal input[type="text"], #cpmPropertyModal input[type="number"]').val('');
            $('#cpmPropertyModal input[type="radio"]').prop('checked', false);
//            $('#cpmPropertyModal  .select-val').val('Years');
            $('.mortgage-tabed .OptionSelection label').removeClass('married-selected');
            $('.secondloan-li, .loan-li').removeClass('active');
            $('#fixed, #interestOnly, #variable, #others, #loantermothers').removeClass('active in');
            $('#cpmPropertyModal .mortgage-paid-monthly, #cpmPropertyModal .mortgage-pmi').hide();
            $('#cpmPropertyModal .mortgageSecondStep .mortgage-nav .secondloan-li:first-child ').removeClass('active');
            $('#fixed').removeClass('active in');
            $('#cpmPropertyModal .cpm-annual-mortgage-interest-dropdown, #cpmPropertyModal .cpm-annual-negatively-amortize').val('').trigger('change');

        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {

                childrenList.splice(index_id, 1);
                $("#cpmproperty-info tbody").empty();
                if (childrenList.length != 0) {
                    var tr = '';
                    $.each(childrenList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.cpm-annual-property-name + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#cpmproperty-info tbody").html(tr);
                }
                $('#cpmnonRentalMortgagesData').html(JSON.stringify(childrenList));
                return false;
            });
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        // on click show listing table


        $("#mortgageothersdropdown").on('change', function () {
            if ($(this).val() === '5') {
                $('.mortgageothers').show();
            } else {
                $('.mortgageothers').hide();
            }
        });


        $(document).on('change', '.cpm-annual-private-insurance', function () {
            if ($(this).val() === 'yes') {
                $('#cpmPropertyModal .error-alert').remove();
                $('.mortgage-pmi').show();

            } else {
                $('.cpm-annual-mortgage-pmi-confirmation').prop('checked', false);
                $('.mortgage-pmi').hide();
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });


        $(document).on('change', '.cpm-annual-mortgage-pmi-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('#cpmPropertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });
        // calculate total
        $(document).on("change", ".qty1", function () {
            var sum = 0;
            $(".qty1").each(function () {
                sum += +$(this).val();
            });
            $(".cpm-annual-mortgage-monthly-total").val(sum);
        });

        //   this is for the second step tab panne

        $(document).on('change', '.cpm-annual-type-mortgage', function () {
            if ($(this).val() === 'fixed') {

                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cpm-annual-type-mortgage', function () {
            if ($(this).val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cpm-annual-type-mortgage', function () {
            if ($(this).val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cpm-annual-type-mortgage', function () {
            if ($(this).val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }
        });


        //   this is for the third step tab panne


        $(document).on('change', '.cpm-annual-fixed-year-selection', function () {
            if ($(this).val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cpm-annual-fixed-year-selection', function () {
            if ($(this).val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#fiftenyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cpm-annual-fixed-year-selection', function () {
            if ($(this).val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
        });

    });</script>

@stop
