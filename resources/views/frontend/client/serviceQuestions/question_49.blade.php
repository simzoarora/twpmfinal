@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<style>
    table {
        width:100%;
    }
    table tbody tr {
        color: #818181;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid #B6B6B6;
        position: relative;
    }
    table thead th {
        border-bottom: 1px solid #B6B6B6;
        font-weight: 400;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 34px;
        top: 20px;
        font-size: 16px;

    }
    table tbody td .fa-trash {
        position: absolute;
        right: 3px;
        top: 20px;
        font-size: 16px;
    }
    .add-family-member {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 13px;
        margin-top: 30px;
        color: #1d99d4;
        padding: 5px;
    }
    .student-info{
        position: relative;
    }

    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid #B6B6B6;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid #B6B6B6;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 34px;
        top: 20px;
        font-size: 16px;
    }
    table tbody td .fa-trash {
        position: absolute;
        right: 3px;
        top: 20px;
        font-size: 16px;
    }
    .add-charity {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    #charity-info {
        position: relative;
    }
    .wrapper .sections .section-right .selectboxit-list {
        max-height: 223px !important;
        min-width: 100% !important;
        max-width: 100% !important;
    }
</style>
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">

            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">  
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',3) }}
                    <!-- beneficary feild set starts -->
                    <h3></h3>   
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Do you have other family to be considered in the plan?</h2>
                                    <p>Please use this section to add any other family members that should be considered in this plan. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>. </p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[49]','Do you have other family to be considered in the plan. ') }}

                                    <div class="col-sm-6 inner-left ">
                                        {{ Form::label('label', 'List your additional family members.') }}
                                        <table id='student-info' class="find-table-length custom-validation-table">
                                            <thead>
                                                <tr>
                                                    <th>First name</th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer["otherFamilyPlanRecords"])) {
                                                    $family = json_decode($answer["otherFamilyPlanRecords"]);
                                                    if (!empty($family)) {
                                                        foreach ($family as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->name}}</td>
                                                                <td></td>
                                                                <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-family-member" data-toggle="modal" data-target="#beneficiaryModal" style="font-size:12px; border-radius:3px">Add family member</button>
                                    </div>
                                    <textarea id="otherFamilyPlanData" class="hidden" name="answer[49][otherFamilyPlanRecords]">{{(!empty($answer["otherFamilyPlanRecords"]))? $answer["otherFamilyPlanRecords"]:null}}</textarea>
                                </div>
                            </div>
                        </section>  
                    </fieldset>   

                    <!-- modal benificiary starts --> 
                    <div id="beneficiaryModal" class="modal fade sections" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">ADD FAMILY/BENEFICIARIES</h4>
                                </div>
                                <div class="modal-body" >
                                    <div class="row">
                                        <!-- Steps starts here --> 
                                        <div class=" setup-content" id="step-3" >
                                            <div class="section-right">
                                                <div class=" validation-alert">

                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','First name') }}
                                                        {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Last name') }}
                                                        {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Date of birth') }}
                                                        {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label','Relationship') }}
                                                        <i class="fa fa-angle-down selectArrow" ></i>
                                                        <select style="position: absolute;" class="relation" required="" name="relationship" style="display: none;"><option selected disabled value="">Select</option><option value="1">Daughter</option><option value="2">Son</option><option value="3">Grandchild</option><option value="4">Great grandchild</option><option value="5">other</option></select>
                                                    </div>

                                                    <div class="col-sm-12 inner-left parent" style='display: none;'>
                                                        {{ Form::label('label','Parent') }}
                                                        {{ Form::input('text','parent',null,['data-validation'=> '' , 'class'=>'custom-validation form-control parent-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'parent', 'required'=>'required']) }}
                                                    </div>
                                                    <div class="col-sm-12 inner-left">
                                                        <button id='beneficiariesForm' class="finishBtn" type="button" style="border-radius: 3px;
                                                                color: #fff;
                                                                font-family: Lato;
                                                                font-size: 18px;
                                                                line-height: 24px;
                                                                text-align: center;
                                                                width: 150px;
                                                                padding: 12px 35px;
                                                                background-color: #2179EE;
                                                                text-decoration: none;
                                                                border: none;
                                                                float: none;
                                                                margin-bottom: 40px;">
                                                            Save
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                        <!-- modal form finsih -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <!-- modal beneficialry finsih -->
                    <!-- beneficary fieldset finsih -->
                    <?php // if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <!-- charity feildset starts -->
                    <!--                    <h3></h3>   -->
                    <!--                    <fieldset>
                                            <section class="sections">
                                                <div    class="col-sm-12 " >
                                                    <div class="col-md-4 col-xs-11 section-left">
                                                        <h2>Charities you wish to be included in the plan?</h2>
                                                        <p>Please use this section to add any other family members that should be considered in this plan. If you need help, simply <a href="#">contact us.</a></p>
                                                    </div>
                                                    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                                        {{ Form::input('hidden','questionName[491]','Charities you wish to be included in the plan') }}
                                                        <div class="col-sm-6 inner-left ">
                                                            {{ Form::label('label', 'List charities you wish to be included.') }}
                                                            <table id='charity-info' class="new find-table-length">
                                                                <thead>                  
                                                                    <tr>   
                                                                        <td>Charity name</td>
                                                                        <td></td>  
                                                                        <td> </td>    
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                    <?php
                    if (!empty($answer["additionalFamilyMemberRecords"])) {
                        $otherFamily = json_decode($answer["additionalFamilyMemberRecords"]);
                        if (!empty($otherFamily)) {
                            foreach ($otherFamily as $key => $info) {
                                ?>
                                                                                            <tr data-index="{{$key}}">
                                                                                                <td>{{$info->charity}}</td>
                                                                                                <td></td>
                                                                                                <td align="right">  <i title="Edit" class="fa fa-pencil charity-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                                                            </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                                                                </tbody>
                                                            </table> 
                                                            <div class="col-md-4 ">
                                                                <div class="row">
                                                                    <button type='button' class="add-charity" data-toggle="modal" data-target="#charityModal" style="border-radius:3px">Add charity</button>
                                                                </div>
                                                            </div>
                                                            <textarea id="additionalFamilyMemberData" class="hidden" name="answer[491][additionalFamilyMemberRecords]">{{(!empty($answer["additionalFamilyMemberRecords"]))? $answer["additionalFamilyMemberRecords"]:null}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                    
                                                 charity modal starts 
                                                <div id="charityModal" class="modal fade sections" role="dialog">
                                                    <div class="modal-dialog">
                    
                                                        <div class="modal-content">
                                                            <div class="modal-header" style= "background-color: #2179EE; color:#fff; border: none;">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">ADD CHARITY</h4>
                                                            </div>
                                                            <div class="modal-body" style="overflow: hidden;">
                                                                <div class="row">
                                                                    <div class="setup-content">
                                                                        <div class="section-right">
                                                                            <div class="col-sm-12 inner-left">
                                                                                {{ Form::label('label', 'Charity name') }}
                                                                                {{ Form::input('text','charity_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control charity-name', 'data-rule-regex'=>"false", 'required'=>true, 'placeholder'=>'first name']) }}
                                                                            </div>
                                                                            <div class="col-sm-12 inner-left"> 
                                                                                {{ Form::label('label', 'Address') }}
                                                                                {{ Form::input('text','address',null,['data-validation'=> '' , 'class'=>'custom-validation form-control address', 'data-rule-regex'=>"false" , 'required'=>true,  'placeholder'=>'address']) }}
                                                                            </div>
                                                                            <div class="alignment">
                                                                                <div class="col-sm-6 inner-left left-side ">
                    
                                                                                    {{ Form::label('label', 'City') }}
                                                                                    {{ Form::input('text','city',null,['data-validation'=> '' , 'class'=>'custom-validation form-control city', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'style'=> 'width: 100%;']) }}
                                                                                </div>
                                                                                <div class="col-sm-2 state-select small-error ">
                                                                                    <div class="inner-left" style="width:100% !important;">    
                                                                                        {{ Form::label('label', 'State') }}
                                                                                        {{ Form::input('text','State',null,['data-validation'=> '' , 'class'=>'custom-validation form-control state', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'style'=> 'width: 100%;']) }}
                    
                                                                                    </div>
                                                                                </div>
                                                                            </div> 
                                                                            <div class="col-sm-12 inner-left">
                                                                                {{ Form::label('label','Tax status') }}
                                                                                <i class="fa fa-angle-down selectArrow" ></i>
                                                                                <select style="position: absolute;" class="tax-status" required="" name="tax_status" style="display: none;"><option selected disabled value="">Select</option><option value="1">Contributions are tax deductible</option><option value="2">Contributions are not tax deductible</option></select>
                                                                            </div>
                    <?php
                    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                        ?>
                                                                                    <div class="col-sm-12 inner-left partner-charity">
                                                                                        {{ Form::label('label', 'Whose charity') }}
                                                                                        <i class="fa fa-angle-down selectArrow"></i>
                                                                                        <select style="position: absolute;" style='width:40%;' class="whose-charity" name="answer[491][whoseCharity]" style="display: none;" required>
                                                                                            <option selected disabled value="">Select</option>
                                                                                            <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                                            <option value="2">Spouse first name</option>
                                                                                            <option value="3">both</option></select>
                                                                                    </div>
                    <?php } ?>
                                                                            <div class="col-sm-12 inner-left text-center">
                                                                                <button id='charityform' type="button" class="finishBtn" style="border-radius: 3px;
                                                                                        color: #fff;
                                                                                        font-family: Lato;
                                                                                        font-size: 18px;
                                                                                        line-height: 24px;
                                                                                        text-align: center;
                                                                                        width: 150px;
                                                                                        padding: 12px 35px;
                                                                                        background-color: #2179EE;
                                                                                        text-decoration: none;
                                                                                        border: none;
                                                                                        float: none;
                                                                                        margin-left: 0;
                                                                                        margin-top: 40px;
                                                                                        margin-bottom: 40px;">
                                                                                    Save
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 charity modal starts 
                                            </section>
                                        </fieldset> -->
                    <!-- charity finish --> 
                    <?php // } ?>
                    {{form::close()}}
                </div>
            </div>
        </div>

    </div>
</div>

@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    var familyList = [];
    if ($('#otherFamilyPlanData').val() != '') {
        familyList = JSON.parse($('#otherFamilyPlanData').val());
    }
    // ---------- beneficary modal value appending js starts -----------
    $('#beneficiaryModal .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $('.recommended-service-div form');
        if (form.valid() === true) {
            if ($('#beneficiaryModal .finishBtn').hasClass('edit-form')) {
                familyList[$(this).attr('data-index')] = {
                    name: $('#beneficiaryModal .first-name').val(),
                    latsNameInput: $('#beneficiaryModal .last-name-input').val(),
                    dob: $('#beneficiaryModal .date-of-birth').val(),
                    relation: $('#beneficiaryModal .relation').val(),
                    parent: $('#beneficiaryModal .parent-input').val()
                }
                $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#beneficiaryModal .first-name').val() + '</td> <td> </td><td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
            } else {
                $('#student-info tbody').append('<tr data-index=' + familyList.length + '><td>' + $('#beneficiaryModal .first-name').val() + '</td><td> </td> <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                familyList.push({
                    name: $('#beneficiaryModal .first-name').val(),
                    latsNameInput: $('#beneficiaryModal .last-name-input').val(),
                    dob: $('#beneficiaryModal .date-of-birth').val(),
                    relation: $('#beneficiaryModal .relation').val(),
                    parent: $('#beneficiaryModal .parent-input').val()
                });
            }
            $('#otherFamilyPlanData').html(JSON.stringify(familyList));
            $('#beneficiaryModal').modal('hide');
            $('#beneficiaryModal select').val('').trigger('change');
        }
        return false;
    });


    $('#student-info').on('click', '.fa-pencil', function () {
        $('#beneficiaryModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        studentDetails = familyList[index];
        $('#beneficiaryModal .first-name').val(studentDetails.name);
        $('#beneficiaryModal .last-name-input').val(studentDetails.latsNameInput);
        $('#beneficiaryModal .date-of-birth').val(studentDetails.dob);
        $('#beneficiaryModal .relation').val(studentDetails.relation).trigger('change');
        $('#beneficiaryModal .parent-input').val(studentDetails.parent);
        $('#beneficiariesForm').addClass('edit-form').attr('data-index', index);
        $('#beneficiaryModal .error-alert').remove();
    });

    // ---------- beneficary modal value appending js finish -----------


    $('.add-family-member').on('click', function () {
        $('#beneficiariesForm').removeClass('edit-form');
        $('#beneficiaryModal input[type="text"], input[type="number"]').val('');
        $('#beneficiaryModal .parent').hide();
    });

    $('#student-info').on('click', '.fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index')
        deleteRow(index_id);
        $('.swal-button--danger').click(function () {
            familyList.splice(index_id, 1);
            $("#student-info tbody").empty();
            if (familyList.length != 0) {
                var tr = '';
                $.each(familyList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.name + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#student-info tbody").html(tr);
            }

            $('#otherFamilyPlanData').html(JSON.stringify(familyList));
            return false;
        });
    });


    $(document).on('change', '.relation', function () {
        if ($(this).val() == '1' || $(this).val() == '2') {
            $('.parent ,.parent-input').hide().val('');
        } else {
            $('.parent, .parent-input').show();
        }

    });

// ------------------ charity list js starts -----------------------------

    var charityList = [];
    if ($('#additionalFamilyMemberData').val() != '') {
        charityList = JSON.parse($('#additionalFamilyMemberData').val());
    }

    $('#charityModal .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $('.recommended-service-div form');
        if (form.valid() === true) {
            if ($('#charityModal .finishBtn').hasClass('edit-form')) {
                charityList[$(this).attr('data-index')] = {
                    charity: $('#charityModal .charity-name').val(),
                    address: $('#charityModal .address').val(),
                    city: $('#charityModal .city').val(),
                    state: $('#charityModal .state').val(),
                    whoseName: $('#charityModal .whose-charity').val(),
                    tax: $('#charityModal .tax-status').val(),
                }
                $('#charity-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#charityModal .charity-name').val() + '</td><td> </td><td> <i title="Edit" class="fa fa-pencil charity-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>');
            } else {
                $('#charity-info tbody').append('<tr data-index=' + charityList.length + '><td class="dependent-name">' + $('#charityModal .charity-name').val() + '</td>  <td> </td><td> <i title="Edit" class="fa fa-pencil charity-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                charityList.push({
                    charity: $('#charityModal .charity-name').val(),
                    address: $('#charityModal .address').val(),
                    city: $('#charityModal .city').val(),
                    state: $('#charityModal .state').val(),
                    whoseName: $('#charityModal .whose-charity').val(),
                    tax: $('#charityModal .tax-status').val()
                });
            }
            $('#additionalFamilyMemberData').html(JSON.stringify(charityList));
            $('#charityModal').modal('hide');
            $('#beneficiaryModal select').val('').trigger('change');
        }
        return false;
    });

    // ------------------- charity list js finish --------------------------


    $(document).on('click', '#charity-info .charity-pencil', function () {
        $('#charityModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        charityDetails = charityList[index];
        $('#charityModal .charity-name').val(charityDetails.charity);
        $('#charityModal .address').val(charityDetails.address);
        $('#charityModal .city').val(charityDetails.city);
        $('#charityModal .state').val(charityDetails.state);
        $('#charityModal .whose-charity').val(charityDetails.whoseName).trigger('change');
        $('#charityModal .tax-status').val(charityDetails.tax).trigger('change');
        $('#charityform').addClass('edit-form');
        $('#charityform').attr('data-index', index);
    });
    $(document).on('click', '.add-charity', function () {
        $('#charityform').removeClass('edit-form');
        $('#charityModal input').val('');
        $('#charityModal .tax-status, #charityModal select').val('').trigger('change');
        $('#charityModal .error-alert').remove();
    });
    $('#charity-info').on('click', '.fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index')
        deleteRow(index_id);
        $('.swal-button--danger').click(function () {
            charityList.splice(index_id, 1);
            $("#charity-info tbody").empty();
            if (charityList.length != 0) {
                var tr = '';
                $.each(charityList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.charity + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#charity-info tbody").html(tr);
            }
            $('#additionalFamilyMemberData').html(JSON.stringify(charityList));
            return false;
        });
    });
});
</script>
@stop 
