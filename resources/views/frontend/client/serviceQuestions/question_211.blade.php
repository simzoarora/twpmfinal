@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',702) }}
                    {{ Form::input('hidden','subTopicId',37) }}
                    {{ Form::input('hidden','redirectPageName','annual-portfolio-debt-liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Credit Cards</h2>
                                    <p>Do not include cards for which you payoff the full amount each month</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[211]','Do you have any credit cards on which you carry a balance?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any credit cards on which you carry a balance?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[211][semi-annual-cc-confirmation]', 'yes',(!empty($answer) && array_key_exists('semi-annual-cc-confirmation',$answer)) ? (($answer['semi-annual-cc-confirmation']=="yes")  ? true : false):false,['class'=>'semiAnnualCCConfirmation table-confirmation' , 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[211][semi-annual-cc-confirmation]', 'no',(!empty($answer) && array_key_exists('semi-annual-cc-confirmation',$answer)) ? (($answer['semi-annual-cc-confirmation']=="no")  ? true : false):false,['class'=>'semiAnnualCCConfirmation table-confirmation' , 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnualCCDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('semi-annual-cc-confirmation', $answer) && ($answer['semi-annual-cc-confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="add-child">
                                                <h5> List all credit cards on which you carry a balance.</h5>

                                                <table id='semiAnnualCCInfo' class="find-table-length">
                                                    <thead>
                                                        <tr> 
                                                            <td>Credit card</td> 
                                                            <td> </td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        if (!empty($answer["credit_card_recods"])) {
                                                            $childs = json_decode($answer["credit_card_recods"]);
                                                            if (!empty($childs)) {
                                                                foreach ($childs as $key => $child) {
                                                                    ?>
                                                                    <tr class="children-info" data-index="{{$key}}">
                                                                        <td class="">{{$child->semiAnnualCCCardName}}</td>
                                                                        <td></td> <td align="right"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table> 
                                                <div class="col-md-4 ">
                                                    <div class="row">
                                                        <button type='button' class="semiAnnualAddCCBtn add-dependent" data-toggle="modal" data-target="#semiAnnualCCModal">Add Card</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="semiAnnualCredit-CardDetails" class="hidden" name="answer[211][credit_card_recods]">{{(!empty($answer["credit_card_recods"]))? $answer["credit_card_recods"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="semiAnnualCCModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD CREDIT CARD</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->
                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this card name') }}
                                                                    {{ Form::input('text','semi-annual-cc-card-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCCardName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Which bank issue this card?') }}
                                                                    {{ Form::input('text','semi-annual-cc-bank-issuer',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCBankIssuer', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Bank Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What type of card is it?') }}
                                                                    {{ Form::input('text','semi-annual-cc-card-type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCCardType', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Visa, Discover, Nordstorm']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label> Brand? <em>(may be store, airline, etc.) </em> </label>
                                                                    <!--{{ Form::label('label', "{!!Brand? <em>(may be store, airline, etc.) </em>!!}") }}-->
                                                                    {{ Form::input('text','semi-annual-cc-card-brand',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCBrand', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'United Airlines, etc.']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn" type="button" >next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a rewards points or cash back card?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-cash-back-card', 'yes',false,['class'=>'semiAnnualCCCashbackCard','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-cash-back-card', 'no',false,['class'=>'semiAnnualCCCashbackCard','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashbackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'What is the point balance?') }}
                                                                    {{ Form::input('number','semi-annual-cc-point-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCPointBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashbackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'Can points be exchanged for cash?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-points-exchanged', 'yes',false,['class'=>'semiAnnualCCPointsExchanged','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-points-exchanged', 'no',false,['class'=>'semiAnnualCCPointsExchanged','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left exchangeforCasgYesOption" style="display: none">
                                                                    <label> What is the rate at which points may be exchanged for cash <em>(Example: 1 to 0.01 would equal one cent per point) </em>?</label>
                                                                    {{ Form::input('number','semi-annual-cc-exchangefor-cash',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualCCExchangeforCash', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','semi-annual-cc-interest-rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control semiAnnualCCInterestRate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance on card?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-current-balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualCCCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i>  back</button>
                                                                    <button class="nextBtn" type="button">next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <!-- second step finsih -->
                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-monthly-payment',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualCCMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-pay-per-month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualCCPayPerMonth', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Approximately how much in a new purchases do you put on the card each month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-purchase-per-month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualCCPurchasePerMonth', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Are you using this card for cash flow?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-card-cash-flow', 'yes',false,['class'=>'semiAnnualCCCardCashFlow','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-card-cash-flow', 'no',false,['class'=>'semiAnnualCCCardCashFlow','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the credit limit?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-credit-limit',null,['data-validation'=> '' , 'class'=>'custom-validation borderLeft0 form-control semiAnnualCCCreditLimit', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this card?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="status valid semiAnnualCCNameThisCard" id="helocothers" name="semi-annual-cc-name-this-card" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <option value ='2'>{{$spouse_name}}</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <!--id='semiAnnualAddCCBtn'-->  
                                                                    <button class="backBtn" type="button" ><i class="fa fa-arrow-left" ></i>  back</button>
                                                                    <button id="semiAnnualAddCCBtnnew" class='semiAnnualAddCCBtn finishBtn pull-right' type="button">
                                                                        finish <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- third step finsih -->
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,702,'annual-portfolio-debt-liabilities-preview'])}}">Save and return later</a>-->
                    {{ Form::close() }} 
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
{{ Html::script(elixir('js/annual-portfolio-review.js')) }}
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,702,'annual-portfolio-debt-liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        var SemiAnnualCCDetailsList = [];
        if ($('#semiAnnualCredit-CardDetails').val() != '') {
            SemiAnnualCCDetailsList = JSON.parse($('#semiAnnualCredit-CardDetails').val());
        }

        $(".backBtn").click(function () {
            if ($(".modal-body .setup-content:visible").prev().length != 0) {
                $(".modal-body .setup-content:visible").prev().show().next().hide();
            } else {
                $(".modal-body .setup-content:visible").hide();
                $(".modal-body .setup-content:visible:last").show();
            }
            return false;
        });




        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#semiAnnualCCModal .semiAnnualCCCardName').val() && $('#semiAnnualCCModal .semiAnnualCCCardType').val()) {

                        if ($('.finishBtn').hasClass('edit-form')) {
                            SemiAnnualCCDetailsList[$(this).attr('data-index')] = {
//                                semiAnnualCCConfirmation: $('.semiAnnualCCConfirmation:checked').val(),

                                semiAnnualCCCardName: $('#semiAnnualCCModal .semiAnnualCCCardName').val(),
                                semiAnnualCCBankIssuer: $('#semiAnnualCCModal .semiAnnualCCBankIssuer').val(),
                                semiAnnualCCCardType: $('#semiAnnualCCModal .semiAnnualCCCardType').val(),
                                semiAnnualCCBrand: $('#semiAnnualCCModal .semiAnnualCCBrand').val(),

                                semiAnnualCCCashbackCard: $('.semiAnnualCCCashbackCard:checked').val(),
                                semiAnnualCCPointBalance: $('#semiAnnualCCModal .semiAnnualCCPointBalance').val(),
                                semiAnnualCCPointsExchanged: $('.semiAnnualCCPointsExchanged:checked').val(),
                                semiAnnualCCExchangeforCash: $('#semiAnnualCCModal .semiAnnualCCExchangeforCash').val(),
                                semiAnnualCCInterestRate: $('#semiAnnualCCModal .semiAnnualCCInterestRate').val(),
                                semiAnnualCCCurrentBalance: $('#semiAnnualCCModal .semiAnnualCCCurrentBalance').val(),

                                semiAnnualCCMonthlyPayment: $('#semiAnnualCCModal .semiAnnualCCMonthlyPayment').val(),
                                semiAnnualCCPayPerMonth: $('#semiAnnualCCModal .semiAnnualCCPayPerMonth').val(),
                                semiAnnualCCPurchasePerMonth: $('#semiAnnualCCModal .semiAnnualCCPurchasePerMonth').val(),
                                semiAnnualCCCardCashFlow: $('.semiAnnualCCCardCashFlow:checked').val(),
                                semiAnnualCCCreditLimit: $('#semiAnnualCCModal .semiAnnualCCCreditLimit').val(),
                                semiAnnualCCNameThisCard: $('#semiAnnualCCModal .semiAnnualCCNameThisCard').val(),
                            };
                            $('#semiAnnualCCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#semiAnnualCCModal .semiAnnualCCCardName').val() + '</td>  <td></td> <td align="right"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>');

                        } else {
                            $('#semiAnnualCCInfo tbody').append('<tr class="children-info" data-index=' + SemiAnnualCCDetailsList.length + '><td>' + $('#semiAnnualCCModal .semiAnnualCCCardName').val() + '</td>   <td> </td><td  align="right"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td></tr>');
                            SemiAnnualCCDetailsList.push({
//                                semiAnnualCCConfirmation: $('.semiAnnualCCConfirmation:checked').val(),

                                semiAnnualCCCardName: $('#semiAnnualCCModal .semiAnnualCCCardName').val(),
                                semiAnnualCCBankIssuer: $('#semiAnnualCCModal .semiAnnualCCBankIssuer').val(),
                                semiAnnualCCCardType: $('#semiAnnualCCModal .semiAnnualCCCardType').val(),
                                semiAnnualCCBrand: $('#semiAnnualCCModal .semiAnnualCCBrand').val(),

                                semiAnnualCCCashbackCard: $('.semiAnnualCCCashbackCard:checked').val(),
                                semiAnnualCCPointBalance: $('#semiAnnualCCModal .semiAnnualCCPointBalance').val(),
                                semiAnnualCCPointsExchanged: $('.semiAnnualCCPointsExchanged:checked').val(),
                                semiAnnualCCExchangeforCash: $('#semiAnnualCCModal .semiAnnualCCExchangeforCash').val(),
                                semiAnnualCCInterestRate: $('#semiAnnualCCModal .semiAnnualCCInterestRate').val(),
                                semiAnnualCCCurrentBalance: $('#semiAnnualCCModal .semiAnnualCCCurrentBalance').val(),

                                semiAnnualCCMonthlyPayment: $('#semiAnnualCCModal .semiAnnualCCMonthlyPayment').val(),
                                semiAnnualCCPayPerMonth: $('#semiAnnualCCModal .semiAnnualCCPayPerMonth').val(),
                                semiAnnualCCPurchasePerMonth: $('#semiAnnualCCModal .semiAnnualCCPurchasePerMonth').val(),
                                semiAnnualCCCardCashFlow: $('.semiAnnualCCCardCashFlow:checked').val(),
                                semiAnnualCCCreditLimit: $('#semiAnnualCCModal .semiAnnualCCCreditLimit').val(),
                                semiAnnualCCNameThisCard: $('#semiAnnualCCModal .semiAnnualCCNameThisCard').val(),
                            });
                        }
                        $('#semiAnnualCCModal').modal('hide');
                        $('#semiAnnualCCModal .semiAnnualCCCardName, #semiAnnualCCModal .semiAnnualCCCardType').val('');
                        $('#semiAnnualCCModal .semiAnnualCCConfirmation').prop('checked', false);
                        $('#semiAnnualCredit-CardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));
                    }

                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('change', '.semiAnnualCCConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnualCCDetailsTable').show();
            } else {
                $('.SemiAnnualCCDetailsTable').hide();
                SemiAnnualCCDetailsList = [];
                $('#semiAnnualCredit-CardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));
                $('.SemiAnnualCCDetailsTable tbody tr').remove();
            }
        });

        $(document).on('change', '.semiAnnualCCCashbackCard', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashbackYesOption ').show();
            } else {
                $('.CashbackYesOption, .exchangeforCasgYesOption').hide();
                $('.exchangeforCasgYesOption input ').val('');
                $('.exchangeforCasgYesOption input[type="radio"]').prop('checked', false);
            }
        });

        $(document).on('change', '.semiAnnualCCPointsExchanged', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('click', '#semiAnnualCCInfo .fa-pencil', function () {
            $('#semiAnnualCCModal').modal();
            $('#semiAnnualCCModal #step-1').show();
            $('#semiAnnualCCModal #step-2, #semiAnnualCCModal #step-3').css('display', 'none');
            $('#semiAnnualCCModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualCCDetails = SemiAnnualCCDetailsList[index];
//            $('.semiAnnualCCConfirmation[value=' + SemiAnnualCCDetails.semiAnnualCCConfirmation + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualCCCardName').val(SemiAnnualCCDetails.semiAnnualCCCardName);
            $('#semiAnnualCCModal .semiAnnualCCBankIssuer').val(SemiAnnualCCDetails.semiAnnualCCBankIssuer);
            $('#semiAnnualCCModal .semiAnnualCCCardType').val(SemiAnnualCCDetails.semiAnnualCCCardType);
            $('#semiAnnualCCModal .semiAnnualCCBrand').val(SemiAnnualCCDetails.semiAnnualCCBrand);

            $('#semiAnnualCCModal .semiAnnualCCCashbackCard[value=' + SemiAnnualCCDetails.semiAnnualCCCashbackCard + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualCCPointBalance').val(SemiAnnualCCDetails.semiAnnualCCPointBalance);
            $('#semiAnnualCCModal .semiAnnualCCPointsExchanged[value=' + SemiAnnualCCDetails.semiAnnualCCPointsExchanged + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualCCExchangeforCash').val(SemiAnnualCCDetails.semiAnnualCCExchangeforCash);
            $('#semiAnnualCCModal .semiAnnualCCInterestRate').val(SemiAnnualCCDetails.semiAnnualCCInterestRate);
            $('#semiAnnualCCModal .semiAnnualCCCurrentBalance').val(SemiAnnualCCDetails.semiAnnualCCCurrentBalance);

            $('#semiAnnualCCModal .semiAnnualCCMonthlyPayment').val(SemiAnnualCCDetails.semiAnnualCCMonthlyPayment);
            $('#semiAnnualCCModal .semiAnnualCCPayPerMonth').val(SemiAnnualCCDetails.semiAnnualCCPayPerMonth);
            $('#semiAnnualCCModal .semiAnnualCCPurchasePerMonth').val(SemiAnnualCCDetails.semiAnnualCCPurchasePerMonth);
            $('#semiAnnualCCModal .semiAnnualCCCardCashFlow[value=' + SemiAnnualCCDetails.semiAnnualCCCardCashFlow + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualCCCreditLimit').val(SemiAnnualCCDetails.semiAnnualCCCreditLimit);
            $('#semiAnnualCCModal .semiAnnualCCNameThisCard').val(SemiAnnualCCDetails.semiAnnualCCNameThisCard).trigger('change');


            $('#semiAnnualAddCCBtnnew').addClass('edit-form');
            $('#semiAnnualAddCCBtnnew').attr('data-index', index);

            if ($('.semiAnnualCCPointsExchanged:checked').val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
            if ($('.semiAnnualCCCashbackCard:checked').val() == 'yes') {
                $('.CashbackYesOption ').show();
            } else {
                $('.CashbackYesOption, .exchangeforCasgYesOption').hide();
                $('.exchangeforCasgYesOption input ').val('');
                $('.exchangeforCasgYesOption input[type="radio"]').prop('checked', false);
            }
        });

        $(document).on('click', '.semiAnnualAddCCBtn', function () {
            $('#semiAnnualAddCCBtnnew').removeClass('edit-form');
            $('#semiAnnualCCModal input[type="text"], #semiAnnualCCModal input[type="number"]').val('');
            $('#semiAnnualCCModal select').val('').trigger('change');
            $('#semiAnnualCCModal .error-alert').remove();
            $('#semiAnnualCCModal #step-1').show();
            $('#semiAnnualCCModal #step-2, #semiAnnualCCModal #step-3, .CashbackYesOption, .exchangeforCasgYesOption').hide();
        });

        $(document).on('click', '#semiAnnualCCInfo .fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);
            $('.swal-button--danger').click(function () {


                SemiAnnualCCDetailsList.splice(index_id, 1);
                $("#semiAnnualCCInfo tbody").empty();
                if (SemiAnnualCCDetailsList.length != 0) {
                    var tr = '';
                    $.each(SemiAnnualCCDetailsList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.semiAnnualCCCardName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
                    });
                    $("#semiAnnualCCInfo tbody").html(tr);
                }


                $('#semiAnnualCredit-CardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));
                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });

        $(document).on('click', '.semiAnnualAddCCBtn', function () { // <-- changes
            $('#semiAnnualCCModal').find('.error-alert').remove();
            $('#semiAnnualCCModal input[type="radio"]').prop('checked', false);

        });
    });

</script>
@stop