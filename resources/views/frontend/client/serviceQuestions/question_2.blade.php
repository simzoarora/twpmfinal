@extends('frontend.layouts.client')
@section('title')
@stop
@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',501) }}    
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>LIABILITIES</h6> 
                                    <h2>What is your Credit Score ?</h2>
                                    <p>Explanation of why this is needed.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[2]','What is your Credit Score ?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Your Credit Score') }}
                                              <!--<i class="fa fa-dollar" aria-hidden="true" style="position:absolute; left: 18px; z-index: 1; top: 41px; font-size: 16px; color: #bfb9b9;"></i>-->
                                        {{ Form::input('number','answer[2][yourcreditscore]',(!empty($answer) && array_key_exists('yourcreditscore',$answer))? $answer['yourcreditscore'] : null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'XXX', 'min'=>0]) }}
                                    </div>
                                    <?php
                                    
                                    if ($defaultData['taxFillingStatus'] != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <div class="col-sm-8 inner-left">
                                            {{ Form::label('label', 'Your spouse\'s /partner\'s credit score') }}
                                            <div class="input-group service-input-group">
                                                {{ Form::input('number','answer[2][spousecreditscore]',(!empty($answer) && array_key_exists('spousecreditscore',$answer))? $answer['spousecreditscore'] : null,['data-validation'=>'' , 'class'=>' custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'XXX','min'=>0]) }}
                                            </div>
                                        </div>
                                    <?php } ?> 
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater"  href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
    });
</script>
@stop