@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content') 
<style>
    table {
        width:100%;
    }
    table tbody td { 
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    } 
    table thead td {  
        border-bottom: 1px solid grey;
    }
    table tbody td .fa {
        font-size: 13px;
        margin:0  3px;
        display: inline; 
    }
    .add-charity {
        background: transparent;
        border: 1px solid #048cdc;
        color: #018aff;
        margin-top: 24px;
        border-radius: 3px;
        padding: 2px 10px 2px 10px;
        width: 115px;
    }
    #charity-info {
        position: relative;
    }
</style>
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',4) }}
                    <h3></h3>   
                    <fieldset>
                        <section class="sections" style="margin-top:1px;">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Charities you wish to be included in the plan?</h2>
                                    <p>Please use this section to add any other family members that should be considered in this plan. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>

                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[50]','Charities you wish to be included in the plan') }}
                                    <div class="col-sm-6 inner-left ">
                                        {{ Form::label('label', 'List charities you wish to be included.') }}
                                        <table id='charity-info'>
                                            <thead>
                                                <tr> 
                                                    <td>Charity name</td>
                                                    <td></td>
                                                    <td> </td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer["charityRecords"])) {
                                                    $charity = json_decode($answer["charityRecords"]);
                                                    if (!empty($charity)) {
                                                        foreach ($charity as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td class="dependent-name"> {{$data->charityName}}</td>

                                                                <td align="right" style="width:80px;"><i title="Edit" class="fa fa-pencil charity-pencil" title="Edit" aria-hidden="true"></i>
                                                                    <i title="Delete" class="fa fa-trash"  aria-hidden="true" title="Delete"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table> 
                                        <div class="col-md-4 ">
                                            <div class="row">
                                                <button type='button' class="add-charity" data-toggle="modal" data-target="#charityModal">Add charity</button>
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <textarea id="charityData" class="hidden" name="answer[50][charityRecords]">{{(!empty($answer["charityRecords"]))? $answer["charityRecords"]:null}}</textarea>


                            <div class="sections">
                                <div id="charityModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD CHARITY</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class=" section-right">
                                                            {{ Form::input('hidden','questionName[50]','ADD CHARITY') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Charity name') }}
                                                                {{ Form::input('text','answer[charity name][50]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control charity-name', 'data-rule-regex'=>"false", 'required'=>true, 'placeholder'=>'first name']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Address') }}
                                                                {{ Form::input('text','answer[address][50]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control address', 'data-rule-regex'=>"false" , 'required'=>true, 'placeholder'=>'address']) }}
                                                            </div>
                                                            <div class="alignment">
                                                                <div class="col-sm-6 inner-left left-side ">

                                                                    {{ Form::label('label', 'City') }}
                                                                    {{ Form::input('text','answer[city][50]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control city', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'city', 'style'=> 'width: 100%;']) }}
                                                                </div>
                                                                <div class="col-sm-6 state-select  small-error">
                                                                    <div class=" inner-left">
                                                                        {{ Form::label('label', 'State') }}
                                                                        {{ Form::input('text','answer[state][50]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control state', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'ST', 'style'=> 'width: 100%;']) }}
                                                                    </div>
                                                                </div>
                                                            </div>   
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Tax status') }}
                                                                <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                <select style="position: absolute;" class="tax-status" required name="answer[relationship][50]"><option selected disabled value="">Select</option><option value="1">Contributions are tax deductible</option><option value="2">Contributions are not tax deductible</option></select>
                                                            </div>
                                                            <?php
                                                            if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                                ?>
                                                                <div class="col-sm-12 inner-left partner-charity">
                                                                    {{ Form::label('label', 'Whose charity') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <!--{{ Form::input('text','answer[whose charity]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control whose-charity', 'data-rule-regex'=>"false", 'placeholder'=>'name']) }}-->
                                                                    <select style="position: absolute;" style='width:40%;' required class="charity-select" name="answer[charityoptions][50]" style="display: none;"  >
                                                                        <option selected disabled value="">Select</option><option value="1">{{Session::get('loggedInUserName')}}</option><option value="2"><?php echo $spouse_name; ?></option><option value="3">both</option></select>

                                                                </div>
                                                            <?php } ?>
                                                            <div class="col-sm-12 inner-left">
                                                                <button id='charityform' type="button" style="border-radius: 3px;
                                                                        color: #fff;
                                                                        font-family: Lato;
                                                                        margin-top: 40px;
                                                                        font-size: 18px;
                                                                        line-height: 24px;
                                                                        text-align: center;
                                                                        width: 150px;  
                                                                        padding: 12px 35px;
                                                                        background-color: #2179EE;
                                                                        text-decoration: none;
                                                                        border: none;
                                                                        margin-left: 62px;
                                                                        margin-bottom: 40px;">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                            </div>

                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div> 
            </div>
        </div>
    </div>
</div>


@endsection
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
//-------charity modal---------//

var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).ready(function () {

    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();


    $("select").selectBoxIt();
    var charityList = [];
    if ($('#charityData').val() != '') {
        charityList = JSON.parse($('#charityData').val());
    }

    // ----------- new js for modal validation ------------------------

    $('#charityform').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#charityModal .charity-name').val() && $('#charityModal .address').val() && $('#charityModal .tax-status').val()) {
                    if ($(this).hasClass('edit-form')) {
                        charityList[$(this).attr('data-index')] = {
                            charityName: $('#charityModal .charity-name').val(),
                            address: $('#charityModal .address').val(),
                            city: $('#charityModal .city').val(),
                            state: $('#charityModal .state').val(),
                            taxStatus: $('#charityModal .tax-status').val(),
                            charitySelect: $('#charityModal .charity-select').val(),
                        }
                        $('#charity-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td style="width:100%! important;">' + $('#charityModal .charity-name').val() + '</td> <td align="right" style="width:70px;"> <i title="Edit" class="fa fa-pencil charity-pencil" title="Edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" title="Delete" aria-hidden="true"></i></td>');
                    } else {
                        $('#charity-info tbody').append('<tr data-index=' + charityList.length + '><td class="dependent-name">' + $('#charityModal .charity-name').val() + '</td>  <td align="right" style="width:70px;"> <i title="Edit" class="fa fa-pencil charity-pencil" title="Edit" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" title="Delete" aria-hidden="true"></i></td></tr>');
                        charityList.push({
                            charityName: $('#charityModal .charity-name').val(),
                            address: $('#charityModal .address').val(),
                            city: $('#charityModal .city').val(),
                            state: $('#charityModal .state').val(),
                            taxStatus: $('#charityModal .tax-status').val(),
                            charitySelect: $('#charityModal .charity-select').val(),
                        });
                    }
                    $('#charityModal').modal('hide');
                    $('#charityData').html(JSON.stringify(charityList));
                }
            }
        }
        return false;
    });

    $('a[href="#finish"]').on('click', function () {
        if ($('#charity-info').find('tr').length >= '0') {
        } else {

        }

    });
    // ----------- new js for modal validation finsih -----------------


    $(document).on('click', '#charity-info .charity-pencil', function () {
        $('#charityModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        var studentDetails = charityList[index];
        $('#charityModal .charity-name').val(studentDetails.charityName);
        $('#charityModal .address').val(studentDetails.address);
        $('#charityModal .city').val(studentDetails.city);
        $('#charityModal .state').val(studentDetails.state);
        $('#charityModal .tax-status').val(studentDetails.taxStatus).trigger('change');
        $('#charityModal .charity-select').val(studentDetails.charitySelect).trigger('change');
        ;
        $('#charityform').addClass('edit-form');
        $('#charityform').attr('data-index', index);
    });
    $(document).on('click', '.add-charity', function () {
        $('#charityform').removeClass('edit-form');
        $('#charityModal input').val('');
        $('#charityModal select').val('').trigger('change');
        $('#charityModal .error-alert').remove();
    });
    $('#charity-info').on('click', '.fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index')
        deleteRow(index_id);


        $('.swal-button--danger').click(function () {

            $(this).closest('tr').remove();
            charityList.splice(index_id, 1);

            $("#charity-info tbody").empty();
            if (charityList.length != 0) {
                var tr = '';
                $.each(charityList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.charityName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#charity-info tbody").html(tr);
            }


            $('#charityData').html(JSON.stringify(charityList));
        });
    });
});
</script>
@stop
