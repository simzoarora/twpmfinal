<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">OTHER ACCOUNTS</p>
        <h2>Investment Accounts not being transferred.</h2>
        <p>Descriptive/explanatory statement.</p> 
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have other investment accounts you will NOT be transferring?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[114][answer][Investment Accounts]', 'yes',false,[ 'class' => 'radio-value investmentAccountChecked']) }} Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[114][answer][Investment Accounts]', 'no',false,[ 'class' => 'radio-value investmentAccountChecked']) }} No
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-12 inner-left children-details-table uploadInvestmentAccounts" style="display: none;" >
            <label for="label">Upload your most recent statements</label>
            <div class="col-sm-12">   
                <div class="row">
                    <div class="col-sm-12 ">
                        <div class="row">
                            <div class="add-child">
                                <table id='other-account-info'>
                                    <thead>
                                        <tr>
                                            <td>Account</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="children-info">
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="col-md-4 ">
                                    <div class="row">
                                        <button type='button' class="add-dependent" data-toggle="modal" data-target="#OtherAccountModal">Add statement</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="OtherAccountModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        Modal content
        <div class="modal-content">
            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD OTHER ACCOUNT STATEMENT</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right">
                        <div id="otherAccountsForm">
                            {{ Form::input('hidden','data[114][questionName]','ADD OTHER ACCOUNT STATEMENT') }}
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Give this account a name') }}
                                {{ Form::input('text','data[114][answer][nickname][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control nickname', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'nickname']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Upload your most recent statements') }}
                                <div>
                                    <label for="other-investment-account" class="btn upload-statment upload-account-statment">Upload statement</label>
                                    <input id="other-investment-account" class="other-account-upload" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
                                </div>
                            </div>
                            <div class="col-sm-12 inner-left">
                                <button id='otherAccountsFormSubmit' type="button" style="border-radius: 3px;
                                        color: #fff;
                                        font-family: Lato;
                                        font-size: 18px;
                                        line-height: 24px;
                                        text-align: center;
                                        width: 150px;
                                        padding: 12px 35px;
                                        background-color: #2179EE;
                                        text-decoration: none;
                                        border: none;
                                        margin-left: 62px;
                                        margin-bottom: 40px;">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(document).on('change', '.investmentAccountChecked', function () {
            if ($(this).val() == 'yes') {
                $('.uploadInvestmentAccounts').show();
            } else {
                $('.uploadInvestmentAccounts').hide();
            }
        });

        var childrenList = [];
        $(document).on('click', '#otherAccountsFormSubmit', function () {
            $('#OtherAccountModal .error-alert').remove();
            if ($('#OtherAccountModal .nickname').val() && ($('#OtherAccountModal .other-account-upload').val() || !$('#OtherAccountModal .other-account-upload').prop('required'))) {

                if ($('#otherAccountsForm').hasClass('edit-form')) {
                    childrenList[$('#otherAccountsForm').attr('data-index')] = {
                        name: $('#OtherAccountModal .nickname').val(),
                        upload: $('#OtherAccountModal .other-account-upload').val()
                    }
                    $('#other-account-info tbody tr[data-index=' + $('#otherAccountsForm').attr('data-index') + ']').html('<td>' + $('#OtherAccountModal .nickname').val() + '</td> <td></td> <td><i title="Edit" class="fa fa-pencil edit-other-account-form" aria-hidden="true"></i></td> <td><i class="fa fa-archive delete-other-account-form" aria-hidden="true" style="margin-top: 5px;"></i></td>');
                } else {

                    $('#other-account-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#OtherAccountModal .nickname').val() + '</td> <td></td> <td> <i title="Edit" class="fa fa-pencil edit-other-account-form" aria-hidden="true"></i></td> <td> <i class="fa fa-archive delete-other-account-form" aria-hidden="true" style="margin-top: 5px;"></i></td></tr>');
                    childrenList.push({
                        name: $('#OtherAccountModal .nickname').val(),
                        upload: $('#OtherAccountModal .other-account-upload').val()
                    });
                }
                $('#OtherAccountModal .nickname,#OtherAccountModal .other-account-upload').val('');
                $('#OtherAccountModal').modal('hide');
            } else {
                $('#OtherAccountModal input').each(function (i, elem) {
                    if (!$(elem).val() && $(elem).prop('required')) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });

        $('#OtherAccountModal input').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val() || $(this).val() == '0') {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        }); 

        $(document).on('click', '#other-account-info .edit-other-account-form', function () {
            $('#OtherAccountModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            studentDetails = childrenList[index];
            $('#OtherAccountModal .nickname').val(studentDetails.name);
            if(studentDetails.upload){
                $('#OtherAccountModal .other-account-upload').prop('required',false);
            }

            $('#otherAccountsForm').addClass('edit-form');
            $('#otherAccountsForm').attr('data-index', index);

        });

        $(document).on('click', '#other-account-info .delete-other-account-form', function () {
            $(this).closest('tr').remove();
        });

        $(document).on('click', '.add-dependent', function () {
            $('#otherAccountsForm').removeClass('edit-form');
        });
    });

</script>
