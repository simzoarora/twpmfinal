@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop 
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',7) }}

                    <h3></h3>                 
                    <fieldset>  
                        <section class="sections"> 
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>estate planning</h6>
                                    <h2>Wills</h2>
                                    <p>The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[52]','Wills') }}
                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you have a will?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[52][will]', 'yes',(!empty($answer) && array_key_exists('will',$answer)) ? (($answer['will']=="yes")  ? true : false):false,['class'=>'will-confirmation' ,'required'=>'required'])}}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[52][will]', 'no',(!empty($answer) && array_key_exists('will',$answer)) ? (($answer['will']=="no")  ? true : false):false,['class'=>'will-confirmation' ,'required'=>'required']) }}No 
                                            <span class="radio-icon"></span>
                                        </label>

                                    </div>
                                    <div class="col-sm-6 inner-left execution-details" style="display: <?php
                                    if (!empty($answer) && array_key_exists('will', $answer) && ($answer['will'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'When was it executed?') }}
                                        {{ Form::input('text','answer[52][When was it executed]',(!empty($answer) && array_key_exists('When was it executed',$answer)) ? $answer['When was it executed'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control execution-date datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'mm/dd/yyyy']) }}
                                    </div>
                                    <div class="col-sm-6 inner-left execution-details" style="display:<?php
                                    if (!empty($answer) && array_key_exists('will', $answer) && ($answer['will'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'When was it last reviewed?') }}
                                        {{ Form::input('text','answer[52][When was it last reviewed]',(!empty($answer) && array_key_exists('When was it last reviewed',$answer)) ? $answer['When was it last reviewed'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control review-date datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'mm/dd/yyyy']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_53')
                        </section>
                    </fieldset> 
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_54')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                   
                            @include('frontend.client.serviceQuestions.question_55')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_56')
                        </section>
                    </fieldset> 

                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        <h3></h3>                
                        <fieldset>  
                            <section class="sections">                  
                                @include('frontend.client.serviceQuestions.question_216')
                            </section>
                        </fieldset> 
                    <?php } ?>

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                   
                            @include('frontend.client.serviceQuestions.question_57')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                 
                            @include('frontend.client.serviceQuestions.question_58')
                        </section>
                    </fieldset> 

                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        <h3></h3>                
                        <fieldset>  
                            <section class="sections">
                                @include('frontend.client.serviceQuestions.question_60')
                            </section>
                        </fieldset> 
                    <?php } ?>

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_59')
                        </section>
                    </fieldset> 


                    {{ Form::close() }}
                </div>
            </div>
        </div> 
    </div>
</div>
<!-- @include('frontend.client.serviceQuestions.question_60')-->
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).on('keypress', 'input[type=text]', function (e) {
    if (e.which === 32 && !this.value.length) {
        e.preventDefault();
    }
    var inputValue = event.charCode;
    if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
        event.preventDefault();
    }
});
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });

//        if ($('.will-confirmation:checked').val() == 'yes') {
//            $('.execution-details, .execution-details input').show();
//        } else {
//            $('.execution-details, .execution-details input').hide().val('');
//        }
//
//        if ($('.living-trust:checked').val() == 'yes') {
//            $('.number-of-trustees, .type-of-trustee').show();
//        } else {
//            $('.number-of-trustees, .type-of-trustee').hide();
//            $('.trustee-type:checked').prop('checked', false);
//            $('.options').removeClass('selected');
//        }
//
//        if ($('.life-insurance-trust:checked').val() == 'yes') {
//            $('.current-insurance, .current-insurance input, .name-of-trust, .name-of-trust input').show();
//        } else {
//            $('.current-insurance, .name-of-trust, .name-of-trust input').hide().val('');
//            $('.current-insurance input').prop('checked', false);
//        }
//
//        if ($('.spouse-life-insurance:checked').val() == 'yes') {
//            $('.spouse-current-insurance ,.spouse-current-insurance input, .spouse-name-of-trust, .spouse-name-of-trust input').show();
//        } else {
//            $('.spouse-current-life-insurance').hide();
//            $('.spouse-current-insurance, .spouse-name-of-trust,.spouse-name-of-trust input').hide().val('');
//            $('.spouse-current-life-insurance').prop('checked', false);
//        }
//
//        if ($('.power-of-attorney:checked').val() == 'yes') {
//            $('.durable-power-of-attorney, .durable-power-of-attorney select').show();
//        } else {
//            $('.durable-power-of-attorney, .durable-power-of-attorney select, .attorney-their-name, .attorney-their-name input').hide();
//            $('.business-type').val('').trigger('change');
//        } 

    if ($('.durable-power-of-attorney select').val() == '2') {
        $('.attorney-their-name').show();
    } else {
        $('.attorney-their-name').hide().val('');
    }
    if ($('.spouse-durable-power-of-attorney select').val() == '2') {
        $('.spouse-attorney-their-name').show();
    } else {
        $('.spouse-attorney-their-name').hide().val('');
    }

    $(document).on('change', '.will-confirmation', function () {
        if ($(this).val() == 'yes') {
            $('.execution-details, .execution-details input').show();
        } else {
            $('.execution-details, .execution-details input').hide().val('');
        }
    });

    $(document).on('change', '.living-trust', function () {
        if ($(this).val() == 'yes') {
            $('.number-of-trustees, .type-of-trustee').show();
        } else {
            $('.number-of-trustees, .type-of-trustee').hide();
            $('.trustee-type:checked').prop('checked', false);
            $('.options').removeClass('selected');
        }
    });


//RADIO//
    $(document).on('change', '.power-of-attorney', function () {
        $("select").selectBoxIt();
        if ($(this).val() == 'yes') {
            $('.durable-power-of-attorney, .durable-power-of-attorney select').show();
        } else {
            $('.durable-power-of-attorney, .durable-power-of-attorney select, .attorney-their-name, .attorney-their-name input').hide();
            $('.business-type').val('').trigger('change');
        }
    });


    $(document).on('change', '.durable-power-of-attorney select', function () {
        if ($(this).val() == '2') {
            $('.attorney-their-name, .attorney-their-name input').show();
        } else {
            $('.attorney-their-name, .attorney-their-name input').hide().val('');
        }
    });
    $(document).on('change', '.spouse-durable-power-of-attorney select', function () {
        if ($(this).val() == '2') {
            $('.spouse-attorney-their-name, .spouse-attorney-their-name input').show();
        } else {
            $('.spouse-attorney-their-name, .spouse-attorney-their-name input').hide().val('');
        }
    });

    $(document).on('click', '.options', function () {
        $(this).addClass('selected').siblings().removeClass('selected');
        $('.single-trustee-co-trustee').val($(this).attr('data-value'));
    });

    $(document).on('change', '.life-insurance-trust', function () {
        if ($(this).val() == 'yes') {
            $('.current-insurance, .current-insurance input, .name-of-trust, .name-of-trust input').show();
        } else {
            $('.current-insurance, .name-of-trust, .name-of-trust input').hide().val('');
            $('.current-insurance input').prop('checked', false);
        }
    });

    $(document).on('change', '.spouse-life-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.spouse-current-insurance ,.spouse-current-insurance input, .spouse-name-of-trust, .spouse-name-of-trust input').show();
        } else {
            $('.spouse-current-insurance, .spouse-name-of-trust,.spouse-name-of-trust input').hide().val('');
            $('.spouse-current-life-insurance').prop('checked', false);
        }
    });


    $(document).on('change', '.select-trustee', function () {
        if ($(this).val() === 'cotrustee') {
            $('div[for=co-trustee]').addClass('married-selected');
        } else {
            $('div[for=co-trustee]').removeClass('married-selected');
        }
    });

    $(document).on('change', '.select-trustee', function () {
        if ($(this).val() === 'singletrustee') {
            $('div[for=single-trustee]').addClass('married-selected');
        } else {
            $('div[for=single-trustee]').removeClass('married-selected');
        }
    });

    if ($('.select-trustee:checked').val() === 'cotrustee') {
        $('div[for=co-trustee]').addClass('married-selected');
    } else {
        $('div[for=co-trustee]').removeClass('married-selected');
    }


    if ($('.select-trustee:checked').val() === 'singletrustee') {
        $('div[for=single-trustee]').addClass('married-selected');
    } else {
        $('div[for=single-trustee]').removeClass('married-selected');
    }




});

</script>
<style type="text/css">
    .wrapper .sections .section-right .inner-left .OptionSelection label{height: auto; line-height: normal; padding: 20px 10px;}
    .wrapper .sections .section-right .inner-left .OptionSelection label.cotrustee{padding: 28px 10px;}
    .OptionSelection.married-selected label{background-color: #2079ee; color: #fff;}
</style>

@stop 