
<style>
    .add-income {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }

</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',13) }}
                    {{ Form::input('hidden','subTopicId',2) }}
                    {{ Form::input('hidden','redirectPageName','spouse-employment-income-retirement-preview') }}

                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{$spouse_name}} EMPLOYMENT & INCOME</h6>
                                    <h2>Additional Income</h2>
                                    <p>We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    {{ Form::input('hidden','questionName[132]','Additional Income') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Does your employer offer a defined contribution retirement plan?</label>
                                        <label class="radio-custom-label">
                                            <input class="retirement-plan-contribution" required="required" name="answer[132][employer_defined_retirement]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('employer_defined_retirement',
                                                    $answer) && ($answer['employer_defined_retirement']
                                                == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="retirement-plan-contribution" required="required" name="answer[132][employer_defined_retirement]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('employer_defined_retirement',
                                                    $answer) && ($answer['employer_defined_retirement']
                                                == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left non-retirement-account-table" style="display:<?php
                                    if (!empty($answer) && array_key_exists('employer_defined_retirement',
                                            $answer) && ($answer['employer_defined_retirement']
                                        == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="row">
                                                <div class="add-child">
                                                    {{ Form::label('label', 'Annual Non-employment reccuring income from non-retirement accounts') }}

                                                    <table id='income-info'>
                                                        <thead>
                                                            <tr> 
                                                                <td style="font-size:14px;">Income type</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody style="font-size:14px;">
                                                            <?php
                                                            if (!empty($answer) && array_key_exists('nonEmployementAccountRecords',
                                                                    $answer)) {
                                                                $employmentData = json_decode($answer["nonEmployementAccountRecords"]);
                                                                if (!empty($employmentData)) {
                                                                    foreach ($employmentData as $key => $data) {
                                                                        ?>
                                                                        <tr class="children-info" data-index="{{$key}}">
                                                                            <?php
                                                                            if ($data->type
                                                                                == 1) {
                                                                                ?><td>Interest</td><?php
                                                                            } elseif ($data->type
                                                                                == 2) {
                                                                                ?><td>Dividends</td><?php
                                                                            } elseif ($data->type
                                                                                == 3) {
                                                                                ?><td>Royalities</td><?php
                                                                            } elseif ($data->type
                                                                                == 4) {
                                                                                ?><td>Net business income</td><?php
                                                                            } elseif ($data->type
                                                                                == 5) {
                                                                                ?><td>Retirement account withdrawals</td><?php
                                                                            } elseif ($data->type
                                                                                == 6) {
                                                                                ?><td>Other</td><?php
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?>
                                                                            <td></td>
                                                                            <td  style="text-align:right; width:80px;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table> 
                                                    <div class="col-md-4 ">
                                                        <div class="row">
                                                            <button type='button' class="add-income " data-toggle="modal" data-target="#incomeModal" style="border-radius:3px; padding: 2px 13px;">Add income</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="nonEmployementAccountData" class="hidden" name="answer[132][nonEmployementAccountRecords]">{{(!empty($answer["nonEmployementAccountRecords"]))? $answer["nonEmployementAccountRecords"]:null}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="sections">
                                <div id="incomeModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD INCOME</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class="section-right">
                                                            {{ Form::input('hidden','questionName[132]','ADD INCOME') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Type of income') }}
                                                                <i class="fa fa-angle-down selectArrow"></i>
                                                                <select style="position: absolute;" class="income-type" name="marital_status" required="required" aria-invalid="false" >
                                                                    <option selected="" disabled="" value="">SELECT</option>
                                                                    <option value="1">Interest</option>
                                                                    <option value="2">Dividends</option>
                                                                    <option value="3">Royalties</option>
                                                                    <option value="4">Net business income</option>
                                                                    <option value="5">Retirement account withdrawals</option>
                                                                    <option value="6">Other</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 inner-left discription-input" style="display:none;">
                                                                {{ Form::input('text','description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control income-type-description', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'description']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Total amount') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                    {{ Form::input('number','total_amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control total-amount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Additional notes/description') }}
                                                                {{ Form::input('text','additional_notes',null,['data-validation'=> '' , 'class'=>'custom-validation form-control additional-notes', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left text-center">
                                                                <button id='incomeForm' type="button" style="border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center;  width: 150px;
                                                                        padding: 12px 35px;
                                                                        background-color: #2179EE;
                                                                        text-decoration: none;
                                                                        margin-top: 40px;
                                                                        border: none; float: none;
                                                                        margin-bottom: 40px;">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>  
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    //----------INCOME MODAL------------//
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,13,'spouse-employment-income-retirement-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        var incomeList = [];
        if ($('#nonEmployementAccountData').val() != '') {
            incomeList = JSON.parse($('#nonEmployementAccountData').val());
        }


        // new js starts  -----------------
        $('#incomeForm').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#incomeModal .income-type').val() && $('#incomeModal .total-amount').val() && $('#incomeModal .additional-notes').val()) {
                        if ($(this).hasClass('edit-form')) {
                            incomeList[$(this).attr('data-index')] = {
                                type: $('#incomeModal .income-type').val(),
                                description: $('#incomeModal .income-type-description').val(),
                                total: $('#incomeModal .total-amount').val(),
                                notes: $('#incomeModal .additional-notes').val()
                            }
                            $('#income-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#incomeModal .income-type option:selected').text() + '</td>   <td></td> <td style="text-align:right; width:80px;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>');
                        } else {
                            $('#income-info tbody').append('<tr data-index=' + incomeList.length + '><td>' + $('#incomeModal .income-type option:selected').text() + '</td>  <td> </td> <td  style="text-align:right; width:80px;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                            incomeList.push({
                                type: $('#incomeModal .income-type').val(),
                                description: $('#incomeModal .income-type-description').val(),
                                total: $('#incomeModal .total-amount').val(),
                                notes: $('#incomeModal .additional-notes').val()
                            });
                        }
                        $('#incomeModal').modal('hide');
                        $('#incomeModal .additional-notes,#incomeModal .description,#incomeModal .total-amount').val('');
                        $('#incomeModal .income-type').val('').trigger('change');
                        $('#nonEmployementAccountData').html(JSON.stringify(incomeList));
                    }
                }
            }
            return false;
        });

        // new js finish  -----------------


        $(document).on('click', '#income-info .fa-pencil', function () {
            $('#incomeModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            studentDetails = incomeList[index];

            $('#incomeModal .income-type').val(studentDetails.type).trigger('change');
            $('#incomeModal .income-type-description').val(studentDetails.description);
            $('#incomeModal .total-amount').val(studentDetails.total);
            $('#incomeModal .additional-notes').val(studentDetails.notes);
            $('#incomeForm').addClass('edit-form');
            $('#incomeForm').attr('data-index', index);
        });

        $(document).on('click', '.add-income', function () {
            $('#incomeForm').removeClass('edit-form');
            $('#incomeModal .additional-notes,#incomeModal .description,#incomeModal .total-amount').val('');
        });

        $('#income-info').on('click', '.fa-trash', function () {
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                incomeList.splice(index_id, 1);

                $("#income-info tbody").empty();

                if (incomeList.length != 0) {
                    var tr = '';
                    $.each(incomeList, function (key, value) {
                        tr += '<tr data-index="' + key + '">';
                        if (value.type == 1) {
                            tr += '<td>Interest</td>';
                        } else if (value.type == 2) {
                            tr += '<td>Dividends</td>';
                        } else if (value.type == 3) {
                            tr += '<td>Royalities</td>';
                        } else if (value.type == 4) {
                            tr += '<td>Net business income</td>';
                        } else if (value.type == 5) {
                            tr += '<td>Retirement account withdrawals</td>';
                        } else if (value.type == 6) {
                            tr += '<td>Other</td>';
                        } else {
                            tr += '<td></td>';
                        }
                        tr += '<td></td><td align="right" style="width:80px;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#income-info tbody").html(tr);
                }

                $('#nonEmployementAccountData').html(JSON.stringify(incomeList));
            });
            return false;
        });

        $(document).on('change', '.retirement-plan-contribution', function () {
            if ($(this).val() == 'yes') {
                $('.non-retirement-account-table').show();
            } else {
                $('.non-retirement-account-table').hide();
                incomeList = [];
                $('#nonEmployementAccountData').html(JSON.stringify(incomeList));
            }
        });
        $(document).on('change', '.income-type', function () {
            if ($(this).val() == '6') {
                $('.discription-input, .discription-input input').show();
            } else {
                $('.discription-input ,.discription-input input').hide().val('');
            }
        });

    });
</script>
@stop

