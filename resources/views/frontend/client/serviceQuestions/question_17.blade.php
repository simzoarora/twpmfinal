@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',507) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Other non-business related debts.</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[17]','Do you have any other non-business related debts?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any other non-business related debts?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[17][DRODConfirmation]', 'yes',(!empty($answer) && array_key_exists('DRODConfirmation',$answer)) ? (($answer['DRODConfirmation']=="yes")  ? true : false):false,['class'=>'DRODConfirmation table-confirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[17][DRODConfirmation]', 'no',(!empty($answer) && array_key_exists('DRODConfirmation',$answer)) ? (($answer['DRODConfirmation']=="no")  ? true : false):false,['class'=>'DRODConfirmation table-confirmation', 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left DRODDetailTable" style="display:<?php
                                    if (!empty($answer) && array_key_exists('DRODConfirmation', $answer) && ($answer['DRODConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child" >
                                                        <h6  style="
                                                             font-size: 15px;
                                                             "> List other debts.</h6>

                                                        <table id='DRODCCInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Item</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["nonBusinessRelatedDebtsRecords"])) {
                                                                    $loans = json_decode($answer["nonBusinessRelatedDebtsRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="SemiAnnualCardInfo" data-index="{{$key}}">
                                                                                <td class="">{{$data->DRODDebtNickname}}</td>
                                                                                <td class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="DRODBtn add-dependent" style="cursor:pointer; background: transparent; border: 1px solid #048cdc; color: #018aff; margin-top: 24px; border-radius: 3px;" data-toggle="modal" data-target="#DRODModal">Add debt</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="nonBusinessRelatedDebtsDetails" class="hidden" name="answer[17][nonBusinessRelatedDebtsRecords]">{{(!empty($answer["nonBusinessRelatedDebtsRecords"]))? $answer["nonBusinessRelatedDebtsRecords"]:null}}</textarea>
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="DRODModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD NON-BUSINESS DEBT</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">

                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this debt a nickname?') }}
                                                                    {{ Form::input('text','DROD-debt-nickname',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODDebtNickname', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this debt?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid DRODOnDebtName"  id="DRODothers" name="DROD-on-debt-name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                                            <option value="2">{{$spouse_name}}</option>
                                                                        <?php } ?>
                                                                        <option value="3">Joint</option>
                                                                        <option value="4">Other</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left DROdOtherNames" style="display: none;">
                                                                    {{ Form::label('label', 'Their Name?') }}
                                                                    {{ Form::input('text','DROD-on-debt-other-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODOnDebtOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Name']) }}
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For what were the proceeds of this loan used?') }}
                                                                    {{ Form::input('text','DRODLoanUsedProceed',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODLoanUsedProceed', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn"  type="button" >next <i class="fa fa-arrow-right" style="    font-size: 15px;    font-weight: normal;    margin-left: 4px;"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->
                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Who is the lender?') }}
                                                                    {{ Form::input('text','lender',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODLender', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'When was this loan originated?') }}
                                                                    {{ Form::input('text','loan_originated',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker SemiAnnualODConsolidationLoanDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan in months?') }}
                                                                    {{ Form::input('number','term_of_loan',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the rate of interest?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','rate_of_interest',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DRODInterestRate', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next<i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- second step finish -->

                                                        <!-- third step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What was the original loan amount?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','original_loan_amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRODOriginalLoanAmount', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','current_balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRODCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payemnt?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','monthly_payemnt',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRODMinMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','pay_per_month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRODPayPerMonth', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next<i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tihrd step finsih -->

                                                        <!-- fourth step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a revolving loan?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'yes',false,['class'=>'DRODRevolvingLoan','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'no',false,['class'=>'DRODRevolvingLoan','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is there collateral pledged against this loan(i.e. house, car, etc.)?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'yes',false,['class'=>'DRODCollateralPledge','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'no',false,['class'=>'DRODCollateralPledge','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div> 



                                                                <div class="col-sm-12 inner-left collateral-block" display="<?php
                                                                        if (!empty($answer) && array_key_exists('pledged_against_loan', $answer) && ($answer['pledged_against_loan'] == "yes")) {
                                                                            echo 'block';
                                                                        } else {
                                                                            echo 'none';
                                                                        }
                                                                        ?>;">
                                                                    {{ Form::label('label', 'Describe the collateral?') }}
                                                                    {{ Form::textarea('collateral_description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRODCollateralDescribe', 'rows' => '4' , 'cols' => '30', 'data-rule-regex'=>"false", 'required'=>true]) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button  id="DRODBtnnew" class='finishBtn DRODCCBT pull-right' type="button">
                                                                        finish <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- fourth step finsih -->


                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    var SemiAnnualHELOCDetails = [];
    if ($('#nonBusinessRelatedDebtsDetails').val() != '') {
        SemiAnnualHELOCDetails = JSON.parse($('#nonBusinessRelatedDebtsDetails').val());
    }
    $('.nextBtn, .finishBtn').on('click', function () {

        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {

                if ($('.finishBtn').hasClass('edit-form')) {
                    $('#DRODModal').modal('hide');
                    SemiAnnualHELOCDetails[$(this).attr('data-index')] = {
                        DRODDebtNickname: $('#DRODModal .DRODDebtNickname').val(),
                        DRODOnDebtName: $('#DRODModal .DRODOnDebtName').val(),
                        DRODOnDebtOtherName: $('#DRODModal .DRODOnDebtOtherName').val(),
                        DRODLoanUsedProceed: $('#DRODModal .DRODLoanUsedProceed').val(),

                        DRODLender: $('#DRODModal .DRODLender').val(),
                        SemiAnnualODConsolidationLoanDate: $('#DRODModal .SemiAnnualODConsolidationLoanDate').val(),
                        DRODLoanTerm: $('#DRODModal .DRODLoanTerm').val(),
                        DRODInterestRate: $('#DRODModal .DRODInterestRate').val(),

                        DRODOriginalLoanAmount: $('#DRODModal .DRODOriginalLoanAmount').val(),
                        DRODCurrentBalance: $('#DRODModal .DRODCurrentBalance').val(),
                        DRODMinMonthlyPayment: $('#DRODModal .DRODMinMonthlyPayment').val(),
                        DRODPayPerMonth: $('#DRODModal .DRODPayPerMonth').val(),
                        DRODCollateralPledge: $('#DRODModal .DRODCollateralPledge:checked').val(),
                        DRODRevolvingLoan: $('#DRODModal .DRODRevolvingLoan:checked').val(),
                        DRODCollateralDescribe: $('#DRODModal .DRODCollateralDescribe').val()
                    };
                    $('#DRODCCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#DRODModal .DRODDebtNickname').val() + '</td>  <td class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');

                } else {
                    $('#DRODCCInfo tbody').append('<tr data-index=' + SemiAnnualHELOCDetails.length + '><td>' + $('#DRODModal .DRODDebtNickname').val() + '</td>   <td class="right-align"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    SemiAnnualHELOCDetails.push({
                        DRODDebtNickname: $('#DRODModal .DRODDebtNickname').val(),
                        DRODOnDebtName: $('#DRODModal .DRODOnDebtName').val(),
                        DRODOnDebtOtherName: $('#DRODModal .DRODOnDebtOtherName').val(),
                        DRODLoanUsedProceed: $('#DRODModal .DRODLoanUsedProceed').val(),
                        DRODLender: $('#DRODModal .DRODLender').val(),
                        SemiAnnualODConsolidationLoanDate: $('#DRODModal .SemiAnnualODConsolidationLoanDate').val(),
                        DRODLoanTerm: $('#DRODModal .DRODLoanTerm').val(),
                        DRODInterestRate: $('#DRODModal .DRODInterestRate').val(),
                        DRODOriginalLoanAmount: $('#DRODModal .DRODOriginalLoanAmount').val(),
                        DRODCurrentBalance: $('#DRODModal .DRODCurrentBalance').val(),
                        DRODMinMonthlyPayment: $('#DRODModal .DRODMinMonthlyPayment').val(),
                        DRODPayPerMonth: $('#DRODModal .DRODPayPerMonth').val(),
                        DRODCollateralPledge: $('#DRODModal .DRODCollateralPledge:checked').val(),
                        DRODRevolvingLoan: $('#DRODModal .DRODRevolvingLoan:checked').val(),
                        DRODCollateralDescribe: $('#DRODModal .DRODCollateralDescribe').val()
                    });

                    $('.DRODBtn').trigger('click');
                    $('#DRODModal').modal('hide');
                }
                $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
            }
        }
        return false;
    });

    $(document).on('click', '#DRODCCInfo .fa-pencil', function () {
        $('#DRODModal').modal();
        $('#DRODModal #step-1').show();
        $('#DRODModal #step-2, #DRODModal #step-3').css('display', 'none');
        $('#DRODModal .inner-left input').parent().find('label.error').remove();
        var index = $(this).closest('tr').attr('data-index');
        SemiAnnualCCDetails = SemiAnnualHELOCDetails[index];
        $('#DRODModal .DRODDebtNickname').val(SemiAnnualCCDetails.DRODDebtNickname);
        $('#DRODModal .DRODOnDebtName').val(SemiAnnualCCDetails.DRODOnDebtName).trigger('change');
        $('#DRODModal .DRODOnDebtOtherName').val(SemiAnnualCCDetails.DRODOnDebtOtherName);
        $('#DRODModal .DRODLoanUsedProceed').val(SemiAnnualCCDetails.DRODLoanUsedProceed);

        $('#DRODModal .DRODLender').val(SemiAnnualCCDetails.DRODLender);
        $('#DRODModal .SemiAnnualODConsolidationLoanDate').val(SemiAnnualCCDetails.SemiAnnualODConsolidationLoanDate);
        $('#DRODModal .DRODLoanTerm').val(SemiAnnualCCDetails.DRODLoanTerm);
        $('#DRODModal .DRODInterestRate').val(SemiAnnualCCDetails.DRODInterestRate);

        $('#DRODModal .DRODOriginalLoanAmount').val(SemiAnnualCCDetails.DRODOriginalLoanAmount);
        $('#DRODModal .DRODCurrentBalance').val(SemiAnnualCCDetails.DRODCurrentBalance);
        $('#DRODModal .DRODMinMonthlyPayment').val(SemiAnnualCCDetails.DRODMinMonthlyPayment);
        $('#DRODModal .DRODPayPerMonth').val(SemiAnnualCCDetails.DRODPayPerMonth);

        $('#DRODModal .DRODCollateralPledge[value=' + SemiAnnualCCDetails.DRODCollateralPledge + ']').prop('checked', true);
        $('#DRODModal .DRODRevolvingLoan[value=' + SemiAnnualCCDetails.DRODRevolvingLoan + ']').prop('checked', true);
        $('#DRODModal .DRODCollateralDescribe').val(SemiAnnualCCDetails.DRODCollateralDescribe);


        $('#DRODBtnnew').addClass('edit-form');
        $('#DRODBtnnew').attr('data-index', index);

        if ($('.DRODCollateralPledge:checked').val() === 'yes') {
            $('.collateral-block').show();
        } else {
            $('.collateral-block').hide();
            $('.collateral-block').val('');
        }
    });



    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $(document).on('change', '.DRODConfirmation', function () {
        if ($(this).val() === 'yes') {
            $('.DRODDetailTable').show();
        } else {
            $('.DRODDetailTable').hide();
            SemiAnnualHELOCDetails = [];
            $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
        }
    });

    $(document).on('change', '.DRODcashback-confirmation', function () {
        if ($(this).val() === 'yes' || $(this).prop('checked', true).val() === 'yes') {
            $('.CashBackYesOption ').show();
        } else {
            $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
        }
    });

    $(document).on('change', '.DRODExchange-point-cash', function () {
        if ($(this).val() === 'yes') {
            $('.exchangeforCasgYesOption ').show();
        } else {
            $('.exchangeforCasgYesOption').hide();
        }
    });


    $(document).on('click', '.DRODBtn', function () {
        $('#DRODModal #step-1').show();
        $('#DRODModal #step-2,  #DRODModal #step-3 ').hide();
        $('#DRODModal').find('.error-alert').remove();
        if ($('#DRODModal .DRODDebtNickname').val() && $('#DRODModal .DRODDebtNickname').val()) {
        }
        $('#DRODModal input[type="text"], input[type="number"] ').val('');
        $('#DRODModal input[type="radio"]').prop('checked', false);
        $('#DRODModal select').val('').trigger('change');
        $('#DRODBtnnew').removeClass('edit-form');
    });

    $(document).on('click', '.fa-trash', function () { // <-- changes
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            SemiAnnualHELOCDetails.splice(index_id, 1);

            $("#DRODCCInfo tbody").empty();
            if (SemiAnnualHELOCDetails.length != 0) {
                var tr = '';
                $.each(SemiAnnualHELOCDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.DRODDebtNickname + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#DRODCCInfo tbody").html(tr);
            }


            $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
            return false;
        });
    });


    $("#DRODothers").on('change', function () {
        if ($(this).val() === '4') {
            $('.DROdOtherNames').show();
        } else {
            $('.DROdOtherNames').hide();
        }
    });
    $(".DRODCollateralPledge").on('change', function () {
        if ($(this).val() === 'yes') {
            $('.collateral-block').show();
        } else {
            $('.collateral-block').hide();
        }
    });
});
</script>
@stop