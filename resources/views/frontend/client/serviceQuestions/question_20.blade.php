<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Do you have any other non-business-related debts?</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[20][questionName]','Do you have any another student loan?') }}
        <div class="col-sm-8 inner-left">
            <label class="radio-custom-label"> {{ Form::radio('data[20][answer][Do you have any other non-business-related debts?]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">  {{ Form::radio('data[20][answer][Do you have any other non-business-related debts?]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>