<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p style="margin-bottom: 0">EMPLOYER</p>
        <h2>Tell us about you employer?</h2>
        <p>Please enter the necessary information about your employer.If you need help, simply <a class="contact-modal-show" href="#">contact us.</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','questionName[25]','Tell us about your employer?') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Name of the employer') }}
            {{ Form::input('text','answer[25][name_of_the_employer]', (!empty($answer) && array_key_exists('name_of_the_employer',$answer)) ? $answer['name_of_the_employer'] :null ,['data-validation'=> '' , 'class'=>'custom-validation form-control taxable-income', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Employer']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Company stock symbol') }}
            {{ Form::input('text','answer[25][company_stock_symbol]',(!empty($answer) && array_key_exists('company_stock_symbol',$answer)) ? $answer['company_stock_symbol'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control taxable-income', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'ABC']) }}
        </div>   
    </div>
</div>

