<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p style="margin-bottom: 0;">LIABILITIES</p>        
        <h2>What is your credit score?</h2>       
        <p>Explanation of why is this needed</p>                  
    </div>                        

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','service[1][topic][1][subTopic][1][question][77][questionName]','What is your credit score') }}      
        <div class="col-sm-6 inner-left " >
            {{ Form::label('label','Your credit score') }}
            {{ Form::input('text','service[1][topic][1][subTopic][1][question][77][answer][credit score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>
        <div class="col-sm-6 inner-left partner-credit-score" style="adisplay: none;" >
            {{ Form::label('label','Your spouse\'s/partner\'s credit score') }}
            {{ Form::input('text','service[1][topic][1][subTopic][1][question][77][answer][partner score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>                                                                   
    </div>                                                                                                                 

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','service11[1][topic][1][question][77][questionName]','What is your credit score') }}      
        <div class="col-sm-6 inner-left " >
            {{ Form::label('label','Your credit score') }}
            {{ Form::input('text','service11[1][topic][1][question][77][answer][credit score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>
        <div class="col-sm-6 inner-left partner-credit-score" style="adisplay: none;" >
            {{ Form::label('label','Your spouse\'s/partner\'s credit score') }}
            {{ Form::input('text','service11[1][topic][1][question][77][answer][partner score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>                                                                   
    </div>                                                                                                                 
    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','service22[1][question][77][questionName]','What is your credit score') }}      
        <div class="col-sm-6 inner-left " >
            {{ Form::label('label','Your credit score') }}
            {{ Form::input('text','service22[1][question][77][answer][credit score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>
        <div class="col-sm-6 inner-left partner-credit-score" style="adisplay: none;" >
            {{ Form::label('label','Your spouse\'s/partner\'s credit score') }}
            {{ Form::input('text','service22[1][question][77][answer][partner score]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required']) }}
        </div>                                                                   
    </div>                                                                                                                 
</div> 