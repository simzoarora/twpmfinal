<form method="post">
    <div class="col-sm-12">
        <div class="col-md-4 col-xs-11 section-left">
            <p class="section-name"> ACCOUNT OWNERSHIP </p>
            <h4> List account owners. </h4>
            <p>Use this section to enter information on all account owners. If you've selected an individual account, just enter your information. </p>
        </div>
        <div class="col-sm-6 col-sm-offset-1 section-right">
            {{ Form::input('hidden','data[107][questionName]','List account owners') }}

            <div class="col-sm-12 inner-left children-details-table" >
                {{ Form::label('label', 'List your additional family members.') }}
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-12 ">
                            <div class="row">
                                <div class="add-child" style="padding-top:0px;">
                                    <table id='owner-info'>
                                        <thead>
                                            <tr> 
                                                <td>Account Owner</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="children-info">
                                            </tr> 
                                        </tbody>
                                    </table> 
                                    <div class="col-md-6 ">
                                        <div class="row">
                                            <button type='button' class="add-dependent" data-toggle="modal" data-target="#ownerModal">Add account owner</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!---------OWNER MODAL 1 starts -------->
    <div id="ownerModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD ACCOUNT OWNER</h4>
                </div>
                <!-- steps modal body starts -->
                <div class="modal-body" style='border:none;'>

                    <!-- first section starts -->
                    <div class=" setup-content" id="step-1">
                        <div class="row"> 
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[107][questionName]','ADD ACCOUNT OWNER') }}

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Full name') }}
                                    {{ Form::input('text','IM-Owner-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerFullName', 'data-rule-regex'=>"false", 'required'=>true,  'placeholder'=>'full name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Percent ownership (max 100%)') }}
                                    {{ Form::input('number','owner-percent',null,['data-validation'=>'' , 'class'=>'custom-validation form-control IMOwnerPercent', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'%']) }}
                                </div>


                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label','Date of Birth') }}
                                    {{ Form::input('text','owner-dob',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMOwnerDOB', 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'US Citizenship') }}
                                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 27%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                    <!--{{ Form::select('data[107][answer][citizenship]', ['SELECT','US Citizen', 'US Resident Alien', 'Non-resident Alien'],null,['class'=>'citizenship']) }}-->
                                    <select style="position: absolute;" class="status valid IMOwnerCitizen" name="citizenship" aria-invalid="false" required="true"  style="display: none;">
                                        <option selected disabled="" value="0">SELECT</option>
                                        <option value="1">US Citizen</option>
                                        <option value="2">US Resident Alien</option>
                                        <option value="3">Non-resident Alien</option>
                                    </select>
                                </div>

                                <div class="col-sm-12 inner-left IMOwnerUploadForm" style="display: none;">
                                    {{ Form::label('label', 'Upload Form W-8') }}

                                    <div>
                                        <label for="investment-account-owner" class="btn IMOwnerUploadStatement IMOwnerUploadAccountStatement">Upload statement</label>
                                        <input id="investment-account-owner" class="account-owner-upload IMOwnerUploadFormBox" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
                                    </div>
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20"> 
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>
                                </div> 

                            </div>
                        </div>
                    </div>

                    <!-- first step finish -->

                    <!-- second step starts -->
                    <div class=" setup-content" id="step-2"  style="display: none;">
                        <div class="row"> 
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[107][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Marital Status') }}
                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-one', 'Married', ['class' => 'label', 'for' => 'option-one'])}}
                                        {{ Form::radio('selector', 'married',null,['class'=>'IMOwnerMaritalStatus',  'id' => 'option-one', ' required'=>'required']) }}
                                    </div>

                                    <div class="OptionSelection col-xs-5 paddingLeft0"> 
                                        {{Form::label('option-two', 'Single', ['class' => 'label', 'for' => 'option-two'])}}
                                        {{ Form::radio('selector', 'single',null,['class'=>'IMOwnerMaritalStatus',  'id' => 'option-two', ' required'=>'required']) }}

                                    </div>
                                </div>

                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'SSN or Tax ID') }}
                                    {{ Form::input('text','ssn-tax-id',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerSsnTaxId', 'data-rule-regex'=>"false",  'required'=>true,  'placeholder'=>'enter SSN or Tax ID']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Email Address') }}
                                    {{ Form::input('email','im-owner-email',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerEmailId', 'data-rule-regex'=>"false",  'required'=>true, 'placeholder'=>'email@email.com']) }}
                                </div>

                                <div class="col-sm-12 inner-left"> 
                                    {{ Form::label('label', 'Preferred phone contact number') }}
                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'mobile',null,['class'=>'IMOwnerPreferedPhoneContact',  'id' => 'mobile', ' required'=>'required']) }}
                                        {{Form::label('mobile', 'Mobile', ['class' => 'label', 'for' => 'mobile'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-3 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'home',null,['class'=>'IMOwnerPreferedPhoneContact',  'id' => 'home', ' required'=>'required']) }}
                                        {{Form::label('home', 'Home', ['class' => 'label', 'for' => 'home'])}}
                                    </div>

                                    <div class="OptionSelection col-xs-4 paddingLeft0"> 
                                        {{ Form::radio('prefered-contact', 'work',null,['class'=>'IMOwnerPreferedPhoneContact',  'id' => 'work', ' required'=>'required']) }}
                                        {{Form::label('work', 'Work', ['class' => 'label', 'for' => 'work'])}}
                                    </div>

                                </div>
                                <div class="col-sm-9 inner-left"> 
                                    {{ Form::label('label', 'Phone number') }}
                                    {{ Form::input('number','im-phone-number',null,['data-validation'=> '' , 'required'=>true, 'class'=>'custom-validation form-control IMOwnerPhoneNumber', 'data-rule-regex'=>"false", 'placeholder'=>'XXX-XXX-XXXX']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button  class="nextBtn modal-button pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                             padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;"> Next </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- second step finsih -->
                    <!-- third step starts -->
                    <div class=" setup-content" id="step-3"  style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">

                                <!-- tabs pannel starts -->
                                <div class="mortgage-tabed mortgageSecondStep">
                                    <h2>Please provide one of the following forms of identification:</h2> 
                                    <ul class="mortgage-nav">
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#stateIssueLicense">State issued Driver's License</a></li>
                                        <li class="secondloan-li" style="width:37%;"><a data-toggle="tab" class="a" href="#passport">Passport</a></li>
                                    </ul>

                                    <div class="tab-content row">
                                        <!-- State issued Driver's License content starts --> 
                                        <div id="stateIssueLicense" class="tab-pane fade active in">
                                            <div class="col-sm-12 inner-left" >
                                                {{ Form::label('label', 'Driver license number') }}
                                                {{ Form::input('text','IMOwner-license',null,['data-validation'=> '',  'class'=>'custom-validation form-control IMOwnerLicense', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'License number']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','IMOwnder-licence-expire-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMOwnerLicenseExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'State of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMOwnerLicenceIssuingState" name="states" required ="required" aria-invalid="false">
                                                    <option selected="" disabled="" value="">STATE</option>
                                                    <option value="1">a</option>
                                                    <option value="2">b</option>
                                                    <option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- State issued Driver's License content finish -->
                                        <!-- Passport tab content starts -->
                                        <div id="passport" class="tab-pane fade">
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Passport ID number') }}
                                                {{ Form::input('text','im-owner-passport',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerPassportNumber', 'required'=>true, 'data-rule-regex'=>"false", 'placeholder'=>'Passport ID number']) }}
                                            </div>
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label','Expiration date') }}
                                                {{ Form::input('text','passport-expiry-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control IMOwnerPassportExpireDate',  'required'=>true, 'data-rule-regex' =>"false", 'placeholder'=>'MM/DD/YYYY']) }}
                                            </div>

                                            <div class="col-sm-12 inner-left"> 
                                                {{ Form::label('label', 'Country of issuance ') }}
                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 34%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                <select style="position: absolute;" class="status valid IMOwnerPassportIssuanceCountry" name="passport-issue-country" aria-invalid="false" required="required"><option selected="" disabled="" value="">COUNTRY</option><option value="1">a</option><option value="2">b</option><option value="3">c</option></select>
                                            </div>
                                        </div>
                                        <!-- Passport tab content finish -->   
                                    </div>
                                    <!-- tab content finish -->
                                </div> 
                                <!-- tabs panel finish -->
                                {{ Form::input('hidden','data[107][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-9 mt-20">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- third step finish -->
                    <!-- fourth step starts -->
                    <div class=" setup-content" id="step-4" style="display: none">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[107][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', "Please provide this account owner's mailing address") }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 1') }}
                                    {{ Form::input('text','IM-owner-address-1',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerAddress1', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Address Line 2') }}
                                    {{ Form::input('text','IM-owner-address2',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerAddress2', 'required' => true,  'data-rule-regex'=>"false", 'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'City') }}
                                    {{ Form::input('text','IM-ownerCity',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerCity', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'State') }}
                                    {{ Form::input('text','IM-owner-state',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerState', 'required' => true, 'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Zipcode') }}
                                    {{ Form::input('text','IM-owner-zipcode',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerZipCode', 'required' => true,  'data-rule-regex'=>"false",'placeholder'=>'']) }}
                                </div>
                                <div class="col-sm-9 mt-20 paddingRight0">
                                    <button class="nextBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fourth steps finish -->
                    <!-- fifth step starts -->
                    <div class=" setup-content" id="step-5" style="display: none;">
                        <div class="row">
                            <div class="col-sm-6 col-sm-offset-3 section-right">
                                {{ Form::input('hidden','data[107][questionName]','ADD ACCOUNT OWNER') }}
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Name of employer(if retired, enter most recent employer). ') }}
                                    {{ Form::input('text','IMowner-employer-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerEmployerName', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'employer name']) }}
                                </div>
                                <div class="col-sm-12 inner-left">
                                    {{ Form::label('label', 'Occupation') }}
                                    {{ Form::input('text','IM-owner-employer-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control IMOwnerEmployerOccupation', 'required' => true, 'data-rule-regex'=>"false", 'placeholder'=>'occupation']) }}
                                </div>

                                <div class="col-sm-9 paddingRight0 mt-20">
                                    <button id='employerForm' class="finishBtn pull-right" type="button" style="border-radius: 3px; color: #fff;  font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                            padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 80px;">
                                        Finish
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- fifth step finish -->
                </div>
                <!-- steps modal body finish -->

            </div>
        </div>
    </div>
</form>
<!---------OWNER MODAL 1 finish -------->
@section('after-scripts')
<!--<script src="{{ asset('js/dashboard.js') }}"></script>-->
<script src="{{ asset('js/investment-management.js') }}"></script>


<script>
$(document).ready(function () {
    $("select").selectBoxIt();

    $('.datetimepicker, .IMOwnerLicenseExpireDate, .license_expire, .passport_expire').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    // ---------------------------------- vishal modal js starts 
    var ownerList = [];
 
    $('.nextBtn, .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('.finishBtn').hasClass('edit-form')) {
                       
                    $('#ownerModal').modal('hide');
                    ownerList[$(this).attr('data-index')] = {
                        IMOwnerFullName: $('#ownerModal .IMOwnerFullName').val(),
                        IMOwnerPercent: $('#ownerModal .IMOwnerPercent').val(),
                        IMOwnerDOB: $('#ownerModal .IMOwnerDOB').val(),
                        IMOwnerCitizen: $('#ownerModal .IMOwnerCitizen').val(),
                        upload: $('#ownerModal .account-owner-upload').val(),

                        IMOwnerMaritalStatus: $('#ownerModal .IMOwnerMaritalStatus:checked').val(),
                        IMOwnerSsnTaxId: $('#ownerModal .IMOwnerSsnTaxId').val(),
                        IMOwnerEmailId: $('#ownerModal .IMOwnerEmailId').val(),
                        IMOwnerPreferedPhoneContact: $('#ownerModal .IMOwnerPreferedPhoneContact:checked').val(),
                        IMOwnerPhoneNumber: $('#ownerModal .IMOwnerPhoneNumber').val(),

                        IMOwnerLicense: $('#ownerModal .IMOwnerLicense').val(),
                        IMOwnerLicenseExpireDate: $('#ownerModal .IMOwnerLicenseExpireDate').val(),
                        IMOwnerLicenceIssuingState: $('ownerModal .IMOwnerLicenceIssuingState').val(),
                        IMOwnerPassportNumber: $('#ownerModal .IMOwnerPassportNumber').val(),
                        IMOwnerPassportExpireDate: $('#ownerModal .IMOwnerPassportExpireDate').data('date'),
                        IMOwnerPassportIssuanceCountry: $('#ownerModal .IMOwnerPassportIssuanceCountry').val(),

                        IMOwnerAddress1: $('#ownerModal .IMOwnerAddress1').val(),
                        IMOwnerAddress2: $('#ownerModal .IMOwnerAddress1').val(),
                        IMOwnerCity: $('#ownerModal .IMOwnerCity').val(),
                        IMOwnerState: $('#ownerModal .IMOwnerState').val(),
                        IMOwnerZipCode: $('#ownerModal .IMOwnerZipCode').val(),

                        IMOwnerEmployerName: $('#ownerModal .IMOwnerEmployerName').val(),
                        IMOwnerEmployerOccupation: $('#ownerModal .IMOwnerEmployerOccupation').val()

                    };
                    $('#owner-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#ownerModal .IMOwnerFullName').val() + '</td>  <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                } else {
                    $('#owner-info tbody').append('<tr data-index=' + ownerList.length + '><td>' + $('#ownerModal .IMOwnerFullName').val() + '</td>  <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td><td> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td></tr>');
                    ownerList.push({
                        IMOwnerFullName: $('#ownerModal .IMOwnerFullName').val(),
                        IMOwnerPercent: $('#ownerModal .IMOwnerPercent').val(),
                        IMOwnerDOB: $('#ownerModal .IMOwnerDOB').val(),
                        IMOwnerCitizen: $('#ownerModal .IMOwnerCitizen').val(),
                        IMOwnerUploadDoc: $('#ownerModal .IMOwnerUploadDoc').val(),

                        IMOwnerMaritalStatus: $('#ownerModal .IMOwnerMaritalStatus:checked').val(),
                        IMOwnerSsnTaxId: $('#ownerModal .IMOwnerSsnTaxId').val(),
                        IMOwnerEmailId: $('#ownerModal .IMOwnerEmailId').val(),
                        IMOwnerPreferedPhoneContact: $('#ownerModal .IMOwnerPreferedPhoneContact:checked').val(),
                        IMOwnerPhoneNumber: $('#ownerModal .IMOwnerPhoneNumber').val(),

                        IMOwnerLicense: $('#ownerModal .IMOwnerLicense').val(),
                        IMOwnerLicenseExpireDate: $('#ownerModal .IMOwnerLicenseExpireDate').val(),
                        IMOwnerLicenceIssuingState: $('ownerModal .IMOwnerLicenceIssuingState').val(),
                        IMOwnerPassportNumber: $('#ownerModal .IMOwnerPassportNumber').val(),
                        IMOwnerPassportExpireDate: $('#ownerModal .IMOwnerPassportExpireDate').data('date'),
                        IMOwnerPassportIssuanceCountry: $('#ownerModal .IMOwnerPassportIssuanceCountry').val(),

                        IMOwnerAddress1: $('#ownerModal .IMOwnerAddress1').val(),
                        IMOwnerAddress2: $('#ownerModal .IMOwnerAddress1').val(),
                        IMOwnerCity: $('#ownerModal .IMOwnerCity').val(),
                        IMOwnerState: $('#ownerModal .IMOwnerState').val(),
                        IMOwnerZipCode: $('#ownerModal .IMOwnerZipCode').val(),

                        IMOwnerEmployerName: $('#ownerModal .IMOwnerEmployerName').val(),
                        IMOwnerEmployerOccupation: $('#ownerModal .IMOwnerEmployerOccupation').val()
                    });

                    $('#ownerModal').modal('hide');
                }
            }

            $(document).on('click', '#owner-info .fa-pencil', function () {
            alert('edit clicked');
                $('#ownerModal').modal();
                $('#ownerModal #step-1').show();
                $('#ownerModal #step-2, #ownerModal #step-3, #ownerModal #step-4, #ownerModal #step-5').css('display', 'none');
                $('#ownerModal .inner-left input, #ownerModal .inner-left select').parent().find('label.error').remove();

                var index = $(this).closest('tr').attr('data-index');
                loanTermSteps = ownerList[index];
                ownerListDetails = ownerList[index];
                $('#ownerModal .IMOwnerFullName').val(ownerListDetails.IMOwnerFullName);
                $('#ownerModal .IMOwnerPercent').val(ownerListDetails.IMOwnerPercent);
                $('#ownerModal .IMOwnerDOB').val(ownerListDetails.IMOwnerDOB);
                $('#ownerModal .IMOwnerCitizen').val(ownerListDetails.IMOwnerCitizen).trigger('change');
                $('#ownerModal .IMOwnerUploadDoc').val(ownerListDetails.IMOwnerUploadDoc);

                $('#SemiAnnualCCModal .IMOwnerMaritalStatus[value=' + ownerListDetails.IMOwnerMaritalStatus + ']').prop('checked', true);
                $('#ownerModal .IMOwnerSsnTaxId').val(ownerListDetails.IMOwnerSsnTaxId);
                $('#ownerModal .IMOwnerEmailId').val(ownerListDetails.IMOwnerEmailId);
                $('#SemiAnnualCCModal .IMOwnerPreferedPhoneContact[value=' + ownerListDetails.IMOwnerPreferedPhoneContact + ']').prop('checked', true);
                $('#ownerModal .IMOwnerPhoneNumber').val(ownerListDetails.IMOwnerPhoneNumber);

                $('#ownerModal .IMOwnerLicense').val(ownerListDetails.IMOwnerLicense);
                $('#ownerModal .IMOwnerLicenseExpireDate').val(ownerListDetails.IMOwnerLicenseExpireDate);
                $('#ownerModal .IMOwnerLicenceIssuingState').val(ownerListDetails.IMOwnerLicenceIssuingState);
                $('#ownerModal .IMOwnerPassportNumber').val(ownerListDetails.IMOwnerPassportNumber);
                $('#ownerModal .IMOwnerPassportExpireDate').val(ownerListDetails.IMOwnerPassportExpireDate);
                $('#ownerModal .IMOwnerPassportIssuanceCountry').val(ownerListDetails.IMOwnerPassportIssuanceCountry);

                $('#ownerModal .IMOwnerAddress1').val(ownerListDetails.IMOwnerAddress1);
                $('#ownerModal .IMOwnerAddress2').val(ownerListDetails.IMOwnerAddress2);
                $('#ownerModal .IMOwnerCity').val(ownerListDetails.IMOwnerCity);
                $('#ownerModal .IMOwnerState').val(ownerListDetails.IMOwnerState);
                $('#ownerModal .IMOwnerZipCode').val(ownerListDetails.IMOwnerZipCode);

                $('#ownerModal .IMOwnerEmployerName').val(ownerListDetails.IMOwnerEmployerName);
                $('#ownerModal .IMOwnerEmployerOccupation').val(ownerListDetails.IMOwnerEmployerOccupation);
                $('#ownerModal .mortgageSecondStep ul.mortgage-nav li.active a').text(ownerListDetails.secondMortgageTab);
                alert('reached edit form');
                $('#employerForm').addClass('edit-form');
                alert('class added');
                $('#employerForm').attr('data-index', index);

                //   for third step fixed 15yr 30 yr and others
                if (loanTermSteps) {
                    $('#ownerModal .loan-li a:textEquals("' + loanTermSteps.loanTerm + '")').closest('.loan-li').addClass('active').siblings().removeClass('active');
                } else {
                    $('#ownerModal .loan-li.active').removeClass('active');
                }

            });
        }
        return false;
    });

    $(document).on('click', '.add-dependent', function () {
        $('#step-1').show();
        $('#step-2, #step-3, #step-4, #step-5').hide();
        $('#ownerModal input[type=text], #ownerModal input[type=number], #ownerModal input[type=email]').val('');
        $('#employerForm').removeClass('edit-form');
        $('#ownerModal input[type="radio"]').prop('checked', false);
        $('#ownerModal .MOwnerCitizen, #ownerModal .IMOwnerLicenceIssuingState, #ownerModal .IMOwnerPassportIssuanceCountry').val('').trigger('change');
        $('.OptionSelection').removeClass('married-selected');
        $('.mortgageSecondStep .mortgage-nav .secondloan-li').removeClass('active');
        $('#stateIssueLicense').removeClass('active in');
        $('#ownerModal input[type="radio"]').prop('checked', false);
        $('.OptionSelection label').removeClass('married-selected');
//        OptionSelection

    });
    // ---------------------------------- vishal modal js finish 
    $(document).on('change', '.IMOwnerMaritalStatus', function () {
        if ($(this).val() === 'married') {
            $('label[for=option-one]').addClass('married-selected');
        } else {
            $('label[for=option-one]').removeClass('married-selected');
        }
    });
    if ($('.IMOwnerMaritalStatus:checked').val() === 'married') {
        $('label[for=option-one]').addClass('married-selected');
    } else {
        $('label[for=option-one]').removeClass('married-selected');
    }

    $(document).on('change', '.IMOwnerMaritalStatus', function () {
        if ($(this).val() === 'single') {
            $('label[for=option-two]').addClass('married-selected');
        } else {
            $('label[for=option-two]').removeClass('married-selected');
        }
    });

    if ($('.IMOwnerMaritalStatus:checked').val() === 'single') {
        $('label[for=option-two]').addClass('married-selected');
    } else {
        $('label[for=option-two]').removeClass('married-selected');
    }

    $(document).on('change', '.IMOwnerPreferedPhoneContact', function () {
        if ($(this).val() === 'mobile') {
            $('label[for=mobile]').addClass('married-selected');
        } else {
            $('label[for=mobile]').removeClass('married-selected');
        }
    });

    if ($('.IMOwnerPreferedPhoneContact:checked').val() === 'mobile') {
        $('label[for=mobile]').addClass('married-selected');
    } else {
        $('label[for=mobile]').removeClass('married-selected');
    }

    $(document).on('change', '.IMOwnerPreferedPhoneContact', function () {
        if ($(this).val() === 'home') {
            $('label[for=home]').addClass('married-selected');
        } else {

            $('label[for=home]').removeClass('married-selected');
        }
    });

    if ($('.IMOwnerPreferedPhoneContact:checked').val() === 'home') {
        $('label[for=home]').addClass('married-selected');
    } else {
        $('label[for=home]').removeClass('married-selected');
    }

    $(document).on('change', '.IMOwnerPreferedPhoneContact', function () {
        if ($(this).val() === 'work') {
            $('label[for=work]').addClass('married-selected');
        } else {

            $('label[for=work]').removeClass('married-selected');
        }
    });

    if ($('.IMOwnerPreferedPhoneContact:checked').val() === 'work') {
        $('label[for=work]').addClass('married-selected');
    } else {
        $('label[for=work]').removeClass('married-selected');
    }

  
    $(document).on('change', '.status', function () {
        if ($(this).val() === '3') {
            $('.IMOwnerUploadForm').show();
        } else {
            $('.IMOwnerUploadForm').hide();
        }
        $("select").selectBoxIt();
    });


    //            delete row
    $('#owner-info').on('click', '.fa-trash', function () {
        $(this).closest('tr').remove();
    });

    //        license/passport
    $(window).on('click', function () {
        if ($(".doc1").hasClass('docselected')) {
            $("#passport-box").hide();
            $("#license-box").show();
        } else {
            $("#passport-box").show();
            $("#license-box").hide();
        }
    });


    $(document).on('click', '#ownerForm', function () {
        if ($('#ownerModal .account-owner-upload').val()) {
            $('#license').hide();
        } else {
            $('#license').show();
        }
    });

});

</script>
<style type="text/css">
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li a {
        float: left;
        color: #2179ee;
        margin-right: 12px;
        border: solid 2px #2179ee;
        border-radius: 4px;
        margin-right: 8px;
        text-align: center;
        min-height: 52px;
        padding: 11px 5px;
        width: calc(100% - 10px);
        font-size: 13px;
        font-weight: normal;
        line-height: normal;
    }
    .mortgage-nav{margin-bottom: 0} 
    .wrapper .sections .section-right .mortgage-tabed .mortgage-nav li:last-child a {line-height: 32px;} 
</style>
@endsection
