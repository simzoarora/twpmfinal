@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',205) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-md-4 col-xs-11 section-left">
                                <h2>Okay, let's wrap this up.</h2>
                                <p>If there is anything else you would like to add, please feel free to do so
                                    here.Otherwise, you are all done -just click the Finish button!</p>
                            </div>
                            <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                {{ Form::input('hidden','questionName[39]','Okay, let\'s wrap this up') }}
                                <div class="col-sm-8 additional-notes inner-left textarea-inner-left ">
                                    {{ Form::label('label', 'Enter any additional notes or anything you want to add here.') }}
                                    <textarea required="true" data-validation="" class="custom-validation field form-control"  data-rule-regex="false" placeholder="" name="answer[39][additional_notes]" cols="30" rows="8">{{(!empty($answer) && array_key_exists('additional_notes',$answer)) ? $answer['additional_notes'] :''}}</textarea>
                                </div>

                            </div>
                        </section>
                    </fieldset>
                    <!--<a id="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });

    addRemoveHref();
});

</script>
@stop