<!--
<style>
    table, tbody, thead,    tr {
        width:100%;
    }
    table tbody td {
        padding: 17px 0;
        border-top: 1px solid grey;
        width: 33%;
        /*display: inline-block;*/
        position: relative;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
</style>-->

<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p>FAMILY</p>
        <h2>Tell us about you and your family</h2>
        <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="">Contact Us.</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="add-dependent">

            <table id='student-info'>
                <thead>
                    <tr>  
                        <td>Dependent</td>
                        <td>Age</td>
                        <td>Social Needs</td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
<!--                        <td>Sarah</td>
                        <td>12</td>
                        <td>
                            <i class="fa fa-check" aria-hidden="true"></i>
                        </td>
                        <td>
                            <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                        </td>-->
                    </tr> 
                </tbody>
            </table>
            <div class="col-md-4 ">
                <button type='button' class="add-dependent" data-toggle="modal" data-target="#myModal">Add dependent</button>
            </div>

        </div>
    </div>
</div>



<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        Modal content
        <div class="modal-content">
            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body" style="overflow: hidden;">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right">
                        {{ Form::input('hidden','data[29][questionName]','ADD CHILD/DEPENDENT') }}
                        <div class="col-sm-12 inner-left">
                            {{ Form::label('label', 'Child name/nickname') }}
                            {{ Form::input('text','data[29][answer][child name][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control student-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'name']) }}
                        </div>
                        <div class="col-sm-12 inner-left">
                            {{ Form::label('label', 'Child/Dependent\'s age') }}
                            {{ Form::input('text','data[29][answer][child age][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control student-age', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age']) }}
                        </div>
                        <div class="col-sm-12 inner-left">
                            {{ Form::label('label', 'Does this child/dependent have special needs?') }}
                            <label class="radio-custom-label"> 
                                {{ Form::radio('data[29][answer][child/dependent have special needs][0]', 'yes',false,['class'=>'special-needs']) }}Yes
                                <span class="radio-icon"></span>
                            </label>
                            <label class="radio-custom-label"> 
                                {{ Form::radio('data[29][answer][child/dependent have special needs][0]', 'no',false,['class'=>'special-needs']) }}No
                                <span class="radio-icon"></span>
                            </label>
                        </div>

                        <div class="col-sm-12 inner-left">
                            <button id='studentform' type="button" style="border-radius: 3px;
                                    color: #fff;
                                    font-family: Lato;
                                    font-size: 18px;
                                    line-height: 24px;
                                    text-align: center;
                                    width: 150px;
                                    padding: 12px 35px;
                                    background-color: #2179EE;
                                    text-decoration: none;
                                    border: none;
                                    margin-left: 62px;
                                    margin-bottom: 40px;">
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
