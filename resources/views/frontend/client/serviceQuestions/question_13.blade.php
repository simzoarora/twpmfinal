<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Do you have any another credit cards on which you carry a balance?</h2>
        <p>Do not include cards for which you pay   off the full amount each month.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[13][questionName]','Do you have any another credit cards on which you carry a balance?') }}
        <div class="col-sm-8 inner-left">
            <label class="radio-custom-label"> {{ Form::radio('data[13][answer][mortgage on non-rental property HELOC]', 'yes',false,['class'=>'another-credit-card']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[13][answer][mortgage on non-rental property HELOC]', 'no',false,['class'=>'another-credit-card']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>