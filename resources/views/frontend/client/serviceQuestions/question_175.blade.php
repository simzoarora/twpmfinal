@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')

<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',1) }}
                    {{ Form::input('hidden','topicId',14) }}
                    {{ Form::input('hidden','subTopicId',10) }}
                    {{ Form::input('hidden','redirectPageName','spouse-insurance-final-preview') }}

                    <h3></h3>                
                   <fieldset>     
                        <section class="sections">

                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInSpouseName')}} AUTO INSURANCE</h6>
                                    <h2>Auto Insurance</h2>
                                   
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[175]','Auto insurance.') }}

                                    <div class="col-sm-6 inner-left ">

                                        <label for="label">Do you have automobile insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="auto-insurance" required="required" name="answer[175][automobile_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('automobile_insurance', $answer) && ($answer['automobile_insurance'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="auto-insurance" required="required" name="answer[175][automobile_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('automobile_insurance', $answer) && ($answer['automobile_insurance'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left liability-coverage" style="display: <?php
                                    if (!empty($answer) && array_key_exists('automobile_insurance', $answer) && ($answer['automobile_insurance'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        <label for="label">What are your liability coverage limits?</label>
                                        <p style="width: 100%; font-weight: 400;"><i>Example:100/300/50</i></p>
                                        <div class="coverage-limits">
                                            <input type="number" class="coverage-input" name="answer[175][body_injury_person]" value="<?php
                                            if (!empty($answer) && array_key_exists('body_injury_person', $answer)) {
                                                echo $answer["body_injury_person"];
                                            }
                                            ?>">/
                                            <input type="number" class="coverage-input" name="answer[175][body_injury_accident]" value="<?php
                                            if (!empty($answer) && array_key_exists('body_injury_accident', $answer)) {
                                                echo $answer["body_injury_accident"];
                                            }
                                            ?>">/
                                            <input type="number" class="coverage-input" name="answer[175][property_damage]" value="<?php
                                            if (!empty($answer) && array_key_exists('property_damage', $answer)) {
                                                echo $answer["property_damage"];
                                            }
                                            ?>">
                                        </div>
                                        <div class="coverage-limits-title">
                                            <p>bodily injury <br> per person</br></p>
                                            <p>bodily injury <br> per accident</br></p>
                                            <p>property <br> damage</br></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 inner-left ">

                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>

                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInSpouseName')}} AUTO INSURANCE</h6>
                                    <h2>Auto Insurance</h2>
                                  
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[176]','Auto insurance.') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have comprehensive coverage?</label>
                                        <label class="radio-custom-label">
                                            <input class="comprehensive-coverage" required="required" name="answer[176][comprehensive_coverage]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('comprehensive_coverage', $answer) && ($answer['comprehensive_coverage'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="comprehensive-coverage" required="required" name="answer[176][comprehensive_coverage]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('comprehensive_coverage', $answer) && ($answer['comprehensive_coverage'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div> 
                                    <div class="col-sm-6 inner-left deductible" style="display: <?php
                                    if (!empty($answer) && array_key_exists('comprehensive_coverage', $answer) && ($answer['comprehensive_coverage'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        <label for="label">What is your deductible?</label>
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span> 

                                            <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[176][your_deductible]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('your_deductible', $answer)) {
                                                echo $answer["your_deductible"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 inner-left auto-insurance-status" style="display: <?php
                                    if (!empty($answer) && array_key_exists('comprehensive_coverage', $answer) && ($answer['comprehensive_coverage'] == "no")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">

                                        <label for="label">Is your spouse/partner also covered by this policy?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[176][spouse_covered_policy]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('spouse_covered_policy', $answer) && ($answer['spouse_covered_policy'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[176][spouse_covered_policy]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('spouse_covered_policy', $answer) && ($answer['spouse_covered_policy'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                </div>
                            </div>
                        </section> 
                    </fieldset>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,14,'spouse-insurance-final-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        $(document).on('change', '.auto-insurance', function () {
            if ($(this).val() == 'yes') {
                $('.liability-coverage').show();
                $('.liability-coverage .coverage-limits input').show();
            } else {
                $('.liability-coverage').hide();
                $('.liability-coverage .coverage-limits input').hide().val('');
            }
        });
        $(document).on('change', '.comprehensive-coverage', function () {
            if ($(this).val() == 'yes') {
                $('.deductible, .deductible input').show();
                $('.auto-insurance-status').hide();
                $('.auto-insurance-status input').prop('checked', false);
            } else {
                $('.deductible , .deductible input').hide().val('');
                $('.auto-insurance-status').show();
            }

            $(document).on('change', '.comprehensive-marital-status', function () {
                if ($(this).val() == '1') {
                    $('.auto-insurance-status').hide();
                    $('.spouse-umbrella-policy').hide();
                    $('.question-description').hide();
                    $('.partner-charity').hide();
                } else {
                    $('.auto-insurance-status').show();
                    $('.spouse-umbrella-policy').show();
                    $('.question-description').show();
                    $('.partner-charity').show();
                }

            });

        });

    });
</script>
@stop 

