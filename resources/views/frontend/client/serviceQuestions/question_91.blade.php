<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa{ 
        margin-left: 10px;
        font-size: 13px;
    } 
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }  
    .rawLandInfo{
        position: relative;
    }  
    .nextBtn, .finishBtn{
        margin-top: 20%
    }
    .wrapper .sections .section-right .selectboxit-options.selectboxit-list{
        max-height: 163px !important;
    }  
</style>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',14) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 "> 
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Real Estate (Raw land)</h2>
                                    <p>Use this section to list any without a house or permanent structure. If you have questions, feel free to  <a>contact us</a>.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[91]','Real Estate (Raw land)') }}
                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Do you own raw land?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[91][row_land_own_house]', 'yes',(!empty($answer) && array_key_exists('row_land_own_house',$answer)) ? (($answer['row_land_own_house']=="yes")  ? true : false):false,['class'=>'raw-land table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[91][row_land_own_house]', 'no',(!empty($answer) && array_key_exists('row_land_own_house',$answer)) ? (($answer['row_land_own_house']=="no")  ? true : false):false,['class'=>'raw-land  table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>
                                    </div> 
                                    <div class="col-sm-6 inner-left raw-land-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('row_land_own_house', $answer) && ($answer['row_land_own_house'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">

                                        {{ Form::label('label', 'List all raw land parcels') }}

                                        <table id='rawLandInfo'>  
                                            <thead>                       
                                                <tr>     
                                                    <td style="font-size: 13px;">Property name</td>   
                                                    <td></td>   
                                                    <td></td>  
                                                </tr>
                                            </thead>   
                                            <tbody>

                                                <?php
                                                if (!empty($answer) && array_key_exists('own_house_record', $answer)) {
                                                    $account = json_decode($answer["own_house_record"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->CPMNonRawLandHouseName}}</td> 
                                                                <td></td>
                                                                <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>



                                            </tbody>   
                                        </table>
                                        <button type='button' class="add-saving-account" data-toggle="modal" data-target="#CPMNonRawLand">Add property</button>
                                    </div>
                                </div> 
                            </div>
                            <textarea id="ownhouse" class="hidden" name="answer[91][own_house_record]">{{(!empty($answer) && array_key_exists('own_house_record',$answer))? $answer["own_house_record"]:null}}</textarea>


                            <div class="sections">
                                <div id="CPMNonRawLand" class="modal fade add-student-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD RAW LAND PARCEL</h4>
                                            </div>
                                            <div class="modal-body" style='border:none;'>
                                                <!-- first section starts -->
                                                <div class="row"> 
                                                    <div class=" setup-content" id="step-1">
                                                        <div class="validation-alert">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give the property a name') }}
                                                                    {{ Form::input('text','CPMNon-raw-land-house-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRawLandHouseName', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Address') }}
                                                                    {{ Form::input('text','CPMNon-raw-land-address',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRawLandAddress', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','When did you purchase this property?') }}
                                                                    {{ Form::input('text','CPMNon-raw-land-purchase-date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control CPMNonRawLandPurchaseDate', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="nextBtn pull-right" type="button" style="width:auto"> next <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- first section finish -->

                                                    <!-- second step starts -->
                                                    <div class=" setup-content" id="step-2" style="display: none;">
                                                        <div class="validation-alert">
                                                            <div class=" section-right" style="padding-left:0px;">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Approximate value of this property?') }}
                                                                     <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMNon-raw-landEstimate-value',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control CPMNonRawLandEstimateValue', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Purchase Price') }}
                                                                    <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMNon-raw-land-purchase-price',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control CPMNonRawLandPurchasePrice', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    {{ Form::label('label','In whose name is the deed to this property?') }}
                                                                    <select style="position: absolute;" style='width:40%;' class="CPMNonRawLandRentalDeed" id="CPMNonRawLandRentalDeeds" name="CPMNon-raw-land-rental-deeds" style="display: none;" required>
                                                                        <option selected disabled value="">SELECT</option>
                                                                        <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="2"><?php echo $spouse_name ;?></option><?php } ?>
                                                                        <option value="3">other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CPMNonRawLandRentalDeedHolder" style="display: none">
                                                                    {{ Form::label('label','Deed-holder&#39;s name') }}
                                                                    {{ Form::input('text','CPMNon-raw-land-rental-deed-holder',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRawLandRentalDeedHolderBox', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'Name', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="backBtn pull-left" type="button" style="width:auto"> <i class="fa fa-arrow-left"></i> back</button>
                                                                        <button  class="nextBtn pull-right" type="button" style="width:auto"> next <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- second step finish -->

                                                    <!-- third step starts -->
                                                    <div class=" setup-content" id="step-3" style="display: none;">
                                                        <div class="validation-alert">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','What are your intentions for this proceeds?') }}
                                                                    {{ Form::textarea('What are your intentions for this proceeds',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMNonRawLandLandProceeds', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'explain', 'required'=>'required', 'style' => 'margin-bottom:0']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class='inner-left'>
                                                                        <button  class="backBtn pull-left" type="button" style="width:auto"> <i class="fa fa-arrow-left"></i> back</button>
                                                                        <button   id='rawLandForm'  class="finishBtn pull-right" type="button" style="border-radius: 3px;"> finish  <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- third step finish -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
     var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var CPMNonRentalDetails = [];

        if ($('#ownhouse').val() != '') {
            CPMNonRentalDetails = JSON.parse($('#ownhouse').val());
        }

        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#CPMNonRawLand').modal('hide');
                        CPMNonRentalDetails[$(this).attr('data-index')] = {
                            CPMNonRawLandHouseName: $('#CPMNonRawLand .CPMNonRawLandHouseName').val(),
                            CPMNonRawLandAddress: $('#CPMNonRawLand .CPMNonRawLandAddress').val(),
                            CPMNonRawLandPurchaseDate: $('#CPMNonRawLand .CPMNonRawLandPurchaseDate').val(),

                            CPMNonRawLandEstimateValue: $('#CPMNonRawLand .CPMNonRawLandEstimateValue').val(),
                            CPMNonRawLandPurchasePrice: $('#CPMNonRawLand .CPMNonRawLandPurchasePrice').val(),
                            CPMNonRawLandRentalDeed: $('#CPMNonRawLand .CPMNonRawLandRentalDeed').val(),
                            CPMNonRawLandRentalDeedHolderBox: $('#CPMNonRawLand .CPMNonRawLandRentalDeedHolderBox').val(),

                            CPMNonRawLandLandProceeds: $('#CPMNonRawLand .CPMNonRawLandLandProceeds').val()
                        };
                        $('#rawLandInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#CPMNonRawLand .CPMNonRawLandHouseName').val() + '</td>  <td></td> <td  align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#rawLandInfo tbody').append('<tr data-index=' + CPMNonRentalDetails.length + '><td>' + $('#CPMNonRawLand .CPMNonRawLandHouseName').val() + '</td>   <td> </td><td  align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        CPMNonRentalDetails.push({
                            CPMNonRawLandHouseName: $('#CPMNonRawLand .CPMNonRawLandHouseName').val(),
                            CPMNonRawLandAddress: $('#CPMNonRawLand .CPMNonRawLandAddress').val(),
                            CPMNonRawLandPurchaseDate: $('#CPMNonRawLand .CPMNonRawLandPurchaseDate').val(),

                            CPMNonRawLandEstimateValue: $('#CPMNonRawLand .CPMNonRawLandEstimateValue').val(),
                            CPMNonRawLandPurchasePrice: $('#CPMNonRawLand .CPMNonRawLandPurchasePrice').val(),
                            CPMNonRawLandRentalDeed: $('#CPMNonRawLand .CPMNonRawLandRentalDeed').val(),
                            CPMNonRawLandRentalDeedHolderBox: $('#CPMNonRawLand .CPMNonRawLandRentalDeedHolderBox').val(),

                            CPMNonRawLandLandProceeds: $('#CPMNonRawLand .CPMNonRawLandLandProceeds').val()
                        });

                        $('#CPMNonRawLand').modal('hide');
                    }
                    $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
                }
            }
            return false;
        });

        $(document).on('click', '#rawLandInfo .fa-pencil', function () {
            $('#CPMNonRawLand').modal();
            $('#CPMNonRawLand #step-1').show();
            $('#CPMNonRawLand #step-2, #CPMNonRawLand #step-3').css('display', 'none');
            $('#CPMNonRawLand .inner-left input, #CPMNonRawLand .inner-left select').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            CPMNonRentalListDetails = CPMNonRentalDetails[index];
            $('#CPMNonRawLand .CPMNonRawLandHouseName').val(CPMNonRentalListDetails.CPMNonRawLandHouseName);
            $('#CPMNonRawLand .CPMNonRawLandAddress').val(CPMNonRentalListDetails.CPMNonRawLandAddress);
            $('#CPMNonRawLand .CPMNonRawLandPurchaseDate').val(CPMNonRentalListDetails.CPMNonRawLandPurchaseDate);

            $('#CPMNonRawLand .CPMNonRawLandEstimateValue').val(CPMNonRentalListDetails.CPMNonRawLandEstimateValue);
            $('#CPMNonRawLand .CPMNonRawLandPurchasePrice').val(CPMNonRentalListDetails.CPMNonRawLandPurchasePrice);
            $('#CPMNonRawLand .CPMNonRawLandRentalDeed').val(CPMNonRentalListDetails.CPMNonRawLandRentalDeed).trigger('change');
            $('#CPMNonRawLand .CPMNonRawLandRentalDeedHolderBox').val(CPMNonRentalListDetails.CPMNonRawLandRentalDeedHolderBox);

            $('#CPMNonRawLand .CPMNonRawLandLandProceeds').val(CPMNonRentalListDetails.CPMNonRawLandLandProceeds);

            if ($(".CPMNonRawLandRentalDeed").val() === '3') {
                $('.CPMNonRawLandRentalDeedHolder').css('display', 'block');
            } else {
                $('.CPMNonRawLandRentalDeedHolder').css('display', 'none');
            }
            $('#rawLandForm').addClass('edit-form');
            $('#rawLandForm').attr('data-index', index);
        });

        $(document).on('change', '.raw-land', function () {
            if ($(this).val() == 'yes') {
                $('.raw-land-table').show();
            } else {
                $('.raw-land-table').hide();
                CPMNonRentalDetails = [];
                $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
                $('.raw-land-table tbody tr').remove();
            }
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            CPMNonRentalDetails.splice($(this).closest('tr').attr('data-index'), 1);
            $("#rawLandInfo tbody").empty();
            if (CPMNonRentalDetails.length != 0) {
                var tr = '';
                $.each(CPMNonRentalDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.CPMNonRawLandHouseName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#rawLandInfo tbody").html(tr);
            }
            $('#ownhouse').html(JSON.stringify(CPMNonRentalDetails));
            return false;
        });


        $(".CPMNonRawLandRentalDeed").on('change', function () {
            if ($(this).val() === '3') {
                $('.CPMNonRawLandRentalDeedHolder').css('display', 'block');
            } else {
                $('.CPMNonRawLandRentalDeedHolder').css('display', 'none');
                $('.CPMNonRawLandRentalDeedHolderBox').val('');
            }
        });

        $(document).on('click', '.add-saving-account', function () { // <-- changes
            $('.CPMNonRawLandRentalDeed').val('').trigger('change');
            $('#step-1').show();
            $('#step-2, #step-3').hide();
            $('#CPMNonRawLand input[type=text], #CPMNonRawLand input[type=number], #CPMNonRawLand textarea').val('');
            $('#rawLandForm').removeClass('edit-form');
            $('#CPMNonRawLand input[type="radio"]').prop('checked', false);
            $('.CPMNonRawLandRentalDeedHolder').hide();
        });

    });

</script>
@stop 