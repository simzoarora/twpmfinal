
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa { 
        font-size: 13px; 
        margin-left: 10px;
    } 
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }  
    .rawLandInfo{
        position: relative;
    }  
    .future-asset-list {
        color: #1d99d4;
        font-size: 12px;
        cursor: pointer;
        margin-top: 10px;
    }
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',17) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Future Assets</h2>
                                    <p>Use this section to enter information about expected future assets. This could inheritences, trusts, settlements, etc.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[94]','Future Assets') }}
                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Are you expexcting any future assets?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[94][expexcting_any_future_assets]', 'yes',(!empty($answer) && array_key_exists('expexcting_any_future_assets',$answer)) ? (($answer['expexcting_any_future_assets']=="yes")  ? true : false):false,['class'=>'future-assets table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[94][expexcting_any_future_assets]', 'no',(!empty($answer) && array_key_exists('expexcting_any_future_assets',$answer)) ? (($answer['expexcting_any_future_assets']=="no")  ? true : false):false,['class'=>'future-assets table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>
                                    </div> 
                                    <div class="col-sm-6 inner-left raw-land-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('expexcting_any_future_assets', $answer) && ($answer['expexcting_any_future_assets'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'List anticipated future assets') }}
                                        <table id='rawLandInfo' class="find-table-length">  
                                            <thead>                       
                                                <tr>     
                                                    <td style="font-size: 13px;">Asset</td>   
                                                    <td></td>   
                                                    <td></td>  
                                                </tr>
                                            </thead>   
                                            <tbody>
                                                <?php
                                                if (!empty($answer) && array_key_exists('own_house_record', $answer)) {
                                                    $account = json_decode($answer["own_house_record"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->assetName}}</td>
                                                                <td></td>
                                                                <td align="right"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody> 
                                            </tbody>   
                                        </table>
                                        <div class="col-sm-4 paddingLeft0">
                                            <button type='button' class="add-saving-account"  style="width:100%; border-radius: 3px;" data-toggle="modal" data-target="#rawLandModal">Add item</button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <textarea id="ownhouse" class="hidden" name="answer[94][own_house_record]">{{(!empty($answer) && array_key_exists('own_house_record',$answer))? $answer["own_house_record"]:null}}</textarea>

                            <div class="sections">
                                <div id="rawLandModal" class="modal fade add-student-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD FUTURE ASSET</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class="section-right">
                                                            <div class="validation-alert"> 
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give this asset a name') }}
                                                                    {{ Form::input('text','Give_this_asset_a_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control asset-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                                                </div> 
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Who will receive this future asset?') }}
                                                                    <i class="fa fa-angle-down selectArrow"></i>
                                                                    <select style="position: absolute;" class="who-will-receive" name="Who_will_receive_this_future_asset" style="display: none;" required>
                                                                        <option selected disabled value="">SELECT</option>
                                                                        <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="2"><?php echo $spouse_name ;?></option><?php } ?>
                                                                        <option value="3">other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left future-asset-input">
                                                                    {{ Form::label('label','Other') }}
                                                                    {{ Form::input('text','Who_will_receive_this_future_asset_description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control new-test future-asset', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'other', 'required'=>'required']) }}
                                                                </div> 
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Source of this asset?') }}
                                                                    {{ Form::textarea('Breif_description_of_item',null,['data-validation'=> '' , 'rows'=>'5','class'=>'custom-validation form-control brief-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'brief description', 'required'=>'required', 'style'=>'border-radius:0;']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left"> 
                                                                    {{ Form::label('label', 'Will funds be received in lump sum?') }}
                                                                    <label class="radio-custom-label">    
                                                                        {{ Form::radio('Are_you_expexcting_any_future_assets', 'yes',false,['class'=>'lump-sum','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('Are_you_expexcting_any_future_assets', 'no',false,['class'=>'lump-sum','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span> 
                                                                    </label>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left lump-sum-questions" style="display:none;">
                                                                    {{ Form::label('label','In what year do you expect to receive this asset?') }}
                                                                    {{ Form::input('number','In_what_sense_do_you_expect_to_receive_this_asset',null,['data-validation'=> '' , 'class'=>'custom-validation form-control asset-receival', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                                                </div> 

                                                                <div class="col-sm-12 inner-left lump-sum-questions" style="display:none;">
                                                                    {{ Form::label('label','In today\'s dollars, what amount is expected or estimated?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','In_today\'s_dollars_what_amount_is_expected_or_estimated',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control another-asset-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-6 inner-left modal-raw-land-table" style="display:none;">

                                                                    {{ Form::label('label', 'Enter estimated installment schedule') }}

                                                                    <table id='rawLand-info'>  
                                                                        <thead>                       
                                                                            <tr style=" border-bottom: 1px solid grey;">      
                                                                                <td style="font-size: 13px;">Year</td>   
                                                                                <td style="font-size: 13px;">Amount</td>   
                                                                                <td></td>  
                                                                            </tr> 
                                                                        </thead>   
                                                                        <tbody class="assets-list">
                                                                            <?php
                                                                            if (!empty($answer) && array_key_exists('installment', $answer)) {
                                                                                foreach ($answer["installment"] as $key => $file) {
                                                                                    ?>
                                                                                    <tr>     
                                                                                        <td  style="padding:0px;">
                                                                                            <input type="number" required="true" value="{{$file["input-year"]}}" name="[{{$key}}][input-year]" class="asset-input-year" placeholder="years" style="font-size: 13px; text-align:left; color: #606060; padding-left: 0 !important; width:85px; margin: 13px 0 13px 10px; height: 29px; border: 0;">
                                                                                        </td>
                                                                                        <td  style="padding: 0px;">
                                                                                            <input type="number" required="true" value="{{$file["input-year"]}}" name="[{{$key}}][input-amount]" class="asset-input-amount" placeholder="amount" style="font-size: 13px; text-align:left; color: #606060; padding-left: 0 !important; width:85px; margin: 13px 0 13px 10px; height: 29px; border: 0;">
                                                                                        </td>
                                                                                        <td align="right" style="width:70px;">
                                                                                            <i title="Edit" class="fa fa-pencil cursor-pointer future-asset-pencil" aria-hidden="true"></i>
                                                                                            <i title="Delete" class="fa fa-trash future-asset-trash cursor-pointer" aria-hidden="true" ></i>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <?php
                                                                                }
                                                                            } else {
                                                                                
                                                                            }
                                                                            ?>
                                                                        </tbody>   
                                                                    </table>
                                                                    <p  class="future-asset-list" >+ installment</p>
                                                                </div> 
                                                                <div class="col-sm-6 inner-left append-input"></div>
                                                                <div class="col-sm-12 inner-left">
                                                                    <button id='rawLandForm'  class="modal-button finishBtn" type="button" style="border-radius: 3px; float:none; background-color: #2179EE; color:#fff;   ">
                                                                        Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

<script id="anticipatedFutureAssets" type="text/html">
    <tr>
        <td style="padding:0px;">
            <input required="true"  type="number" name="[input-year][copy]" class="asset-input-year" placeholder="years" style="font-size: 13px; color: #606060; padding-left: 0; width:85px; margin: 13px 0 13px 10px; height: 29px; border: 0;">
        </td>
        <td style="padding: 0px;">
            <input required="true" type="number" name="[input-amount][copy]"copynumber class="asset-input-amount" placeholder="amount" style="font-size: 13px; color: #606060; padding-left: 0; width:85px; margin: 13px 0 13px 10px; height: 29px; border: 0;">
        </td>
        <td> 
            <i title="Edit" class="fa fa-pencilcursor-pointer future-asset-pencil" aria-hidden="true" style="top: 19px; padding-right: 10px;"></i>
            <i title="Delete" class="fa fa-trash  cursor-pointer future-asset-trash" aria-hidden="true" style="position:absolute; top: 19px;"></i>
        </td>
    </tr>
</script>

@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
$(document).ready(function () {


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    $('.asset-receival, .asset-input-year').datetimepicker({
        format: 'YYYY'
    });
    var studentList = [];
    if ($('#ownhouse').val() != '') {
        studentList = JSON.parse($('#ownhouse').val());
    }

    // --------------------------- new js starts -----------------

    $(document).on('click', '#rawLandForm', function () {
        var inputyear = $("[name^='[input-year]']")
                .map(function () {
                    return $(this).val();
                }).get();
        var inputamount = $("[name^='[input-amount]']")
                .map(function () {
                    return $(this).val();
                }).get();
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#rawLandModal .asset-name').val()) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        studentList[$(this).attr('data-index')] = {
                            assetName: $('#rawLandModal .asset-name').val(),
                            whoWillReceive: $('#rawLandModal .who-will-receive').val(),
                            futureAsset: $('#rawLandModal .future-asset').val(),
                            briefDescription: $('#rawLandModal .brief-description').val(),
                            lumpSum: $('#rawLandModal .lump-sum:checked').val(),
                            futureAssest: $('#rawLandModal .future-assets:checked').val(),
                            assetReceival: $('#rawLandModal .asset-receival').val(),
                            anotherAssetName: $('#rawLandModal .another-asset-name').val(),
                            inputyear: inputyear,
                            inputamount: inputamount
                        }
                        $('#rawLandInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#rawLandModal .asset-name').val() + '</td> <td>  </td> <td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td>');
                    } else {
                        $('#rawLandInfo tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#rawLandModal .asset-name').val() + '</td> <td>  </td> <td align="right"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i>  <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>');
                        studentList.push({
                            assetName: $('#rawLandModal .asset-name').val(),
                            whoWillReceive: $('#rawLandModal .who-will-receive').val(),
                            briefDescription: $('#rawLandModal .brief-description').val(),
                            lumpSum: $('#rawLandModal .lump-sum:checked').val(),
                            futureAssets: $('#rawLandModal .future-assets:checked').val(),
                            futureAsset: $('#rawLandModal .future-asset').val(),
                            assetReceival: $('#rawLandModal .asset-receival').val(),
                            anotherAssetName: $('#rawLandModal .another-asset-name').val(),
                            inputyear: inputyear,
                            inputamount: inputamount
                        });
                    }
                    $('#rawLandModal').modal('hide');
                    $('#ownhouse').html(JSON.stringify(studentList));
                }
            }
        }
        return false;
    });


    // -------------------------- new js finish ------------------
// 
//        $('#rawLandModal input[type="text"],input[type="text"], #rawLandModal select').on('keyup change', function () {
//            $(this).parent().find('.error-alert').remove();
//            if (!$(this).val()) {
//                $(this).parent().append('<label class="error-alert">This field is required.</label>');
//            }
//        });
    $('#rawLandInfo').on('click', '.fa-pencil', function () {
        $('#rawLandModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = studentList[index];
        $('#rawLandModal .asset-name').val(insuranceDetails.assetName);
        $('#rawLandModal .future-asset').val(insuranceDetails.futureAsset);
        $('#rawLandModal .who-will-receive').val(insuranceDetails.whoWillReceive).trigger('change');
        $('#rawLandModal .brief-description').val(insuranceDetails.briefDescription);
        $('#rawLandModal .lump-sum[value=' + insuranceDetails.lumpSum + ']').prop('checked', true);
        $('#rawLandModal .future-assets[value=' + insuranceDetails.futureAssets + ']').prop('checked', true);
        $('#rawLandModal .asset-receival').val(insuranceDetails.assetReceival);
        $('#rawLandModal .another-asset-name').val(insuranceDetails.anotherAssetName);
        $('.assets-list').html('');
        var html = '';
        var count = 0;
        for (var i = 0; i < insuranceDetails.inputyear.length; i++) {
            count++;
            html += '<tr><td type="text" style="padding:0px;"><input required type="number" readonly name="[input-year][' + count + ']" value="' + insuranceDetails.inputyear[i] + '" class="asset-input-year" placeholder="years" style="font-size: 13px; color: #606060; text-align:left; width:85px; margin: 7px 0; padding-left:0!important; height: 29px; border: 0;"></td><td type="text" style="padding: 0px;"><input type="number" readonly name="[input-amount][' + count + ']" value="' + insuranceDetails.inputamount[i] + '" class="asset-input-amount" placeholder="amount" style="font-size: 13px; color: #606060; text-align:left; width:85px; margin: 7px 0; padding-left:0!important; height: 29px; border: 0;"></td><td align="right" style="width:70px"><i class="fa fa-pencil cursor-pointer future-asset-pencil" aria-hidden="true"></i><i class="fa fa-trash cursor-pointer future-asset-trash" aria-hidden="true"></i></td></tr>'
        }
        $('.assets-list').append(html);
        $('#rawLandForm').addClass('edit-form').attr('data-index', index);
        $('#rawLandModal .error-alert').remove();

        if ($('.lump-sum:checked').val() == 'yes') {
            $('.lump-sum-questions, .lump-sum-questions input').show();
            $('.modal-raw-land-table').hide();
        } else {
            $('.lump-sum-questions, .lump-sum-questions input').hide();
            $('.modal-raw-land-table').show();
        }
        $('.asset-receival, .asset-input-year').datetimepicker({
            format: 'YYYY'
        });
    });

    $('.add-saving-account').on('click', function () {
        $('#rawLandForm').removeClass('edit-form');
        $('.saving-account').removeClass('selected-option');
        $('#rawLandModal input[type="text"],#rawLandModal input[type="number"],#rawLandModal input[type="textarea"]').val('');
        $('#rawLandModal input[type="radio"]').prop('checked', false);
        $('#rawLandModal .lump-sum').prop('checked', false);
        $('#rawLandModal .brief-description').val('');
        $('#rawLandModal select').val('').trigger('change');
        $('#rawLandModal .error-alert').remove();
        $('.modal-raw-land-table #rawLandInfo .assets-list tr').remove();
        $('.modal-raw-land-table, .lump-sum-questions').hide();
        $('.modal-raw-land-table #rawLand-info tbody').find('tr').remove();
    });

    $('#rawLandInfo').on('click', '.fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            studentList.splice(index_id, 1);
            $("#rawLandInfo tbody").empty();
            if (studentList.length != 0) {
                var tr = '';
                $.each(studentList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.assetName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
                });
                $("#rawLandInfo tbody").html(tr);
            }


            $('#ownhouse').html(JSON.stringify(studentList));
            return false;
        });
    });

    $(document).on('change', '.who-will-receive', function () {
        if ($(this).val() == '3') {
            $('.future-asset-input, .future-asset-input input').show();
        } else {
            $('.future-asset-input,.future-asset-input input').hide().val('');
        }

    });

    $(document).on('change', '.lump-sum', function () {
        if ($(this).val() == 'yes') {
            $('.lump-sum-questions, .lump-sum-questions input').show();
            $('.modal-raw-land-table').hide();
        } else {
            $('.lump-sum-questions, .lump-sum-questions input').hide();
            $('.modal-raw-land-table').show();
        }

    });


    if ($('.lump-sum').val() == 'yes') {
        $('.lump-sum-questions, .lump-sum-questions input').show();
        $('.modal-raw-land-table').hide();
    } else {
        $('.lump-sum-questions, .lump-sum-questions input').hide();
        $('.modal-raw-land-table').show();
    }


    $(document).on('change', '.future-assets', function () {
        if ($(this).val() == 'yes') {
            $('.raw-land-table').show();
        } else {
            $('.raw-land-table').hide();
            studentList = [];
            $('#ownhouse').html(JSON.stringify(studentList));
            $('.raw-land-table tbody tr').remove();
        }
    });
    $(document).on('change', '.raw-land-deed', function () {
        if ($(this).val() == '3') {
            $('.raw-land-deed-holder, .raw-land-deed-holder input').show();
        } else {
            $('.raw-land-deed-holder, .raw-land-deed-holder input').hide().val('');
        }
    });
    $('.raw-land-purchasing-date').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $('.asset-receival, .asset-input-year').datetimepicker({
        format: 'YYYY'
    });

});
var copynumber = $('.assets-list tr').length;
$(document).on('click', '.future-asset-list', function () {
    var assetListAppend = $('#anticipatedFutureAssets').html();
    html = _.template(assetListAppend); 
//        $('.assets-list').append(html);
    $('#rawLand-info .assets-list').append('<tr><td type="text" style="padding:0px;"><input required="true" type="number" name="[input-year][' + copynumber + ']" class="asset-input-year" placeholder="years" style="font-size: 13px; color: #606060; text-align:left; width:85px; margin: 7px 0; padding-left:0!important; height: 29px; border: 0;"></td><td type="text" style="padding: 0px;"><input required="true" type="number" name="[input-amount][' + copynumber + ']" class="asset-input-amount" placeholder="amount" style="font-size: 13px; color: #606060; text-align:left; width:85px; margin: 7px 0; padding-left:0!important; height: 29px; border: 0;"></td><td align="right" style="width:70px"><i class="fa fa-pencil cursor-pointer future-asset-pencil" aria-hidden="true"></i><i class="fa fa-trash cursor-pointer future-asset-trash" aria-hidden="true"></i></td></tr>');
    copynumber++;
    $(".asset-input-year, .asset-input-amount").focusout(function () {
        $(this).attr('readonly', 'true');
    });
    $('.asset-receival, .asset-input-year').datetimepicker({
        format: 'YYYY'
    });

});
$(document).on('click', '.asset-input-year, .asset-input-amount', function () {
    $(this).focusout(function () {
        $(this).attr('readonly', 'true');
    });
});

$(document).on('click', '.future-asset-pencil', function () {
    $(this).closest('tr').find('input').removeAttr('readonly');
    $(this).closest('tr').find('.asset-input-year, .asset-input-amount').css('border', '1px solid lightgrey');

    $(this).removeClass('fa-pencil cursor-pointer future-asset-pencil').addClass('fa-check future-asset-check');
});
$(document).on('click', '.future-asset-check', function () {
    $(this).removeClass('fa-check future-asset-check').addClass('fa-pencil cursor-pointer future-asset-pencil');
    $(this).closest('tr').find('.asset-input-year, .asset-input-amount').css('border', '0');
    var amountval = $(this).closest('tr').find('.asset-input-amount').val();
    
    if (amountval === '') {
         $(this).closest('.modal-raw-land-table').find('.error-alert').remove();
        $(this).closest('.modal-raw-land-table').append('<label class="error-alert">This field is required.</label>');
    }
    else{
        $(this).closest('.modal-raw-land-table').find('.error-alert').remove();
    }

});

$(document).on('click', '.future-asset-trash', function () {
    $(this).closest('tr').remove();
    deleteRow(0);
});
$(document).on('keyup', '.asset-input-year, .asset-input-amount', function () {
    $(this).closest('.inner-left').find('.error-alert').remove();
    if (!$(this).val()) {
        $(this).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
    }
});

</script>
@stop
