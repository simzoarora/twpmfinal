@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',802) }}
                    {{ Form::input('hidden','subTopicId',39) }}
                    {{ Form::input('hidden','redirectPageName','DebtLiabilities_review') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Other non-business related debts.</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[203]','Do you have any other non-business related debts?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any other non-business related debts?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[203][semiAnnualODConfirmation]', 'yes',(!empty($answer) && array_key_exists('semiAnnualODConfirmation',$answer)) ? (($answer['semiAnnualODConfirmation']=="yes")  ? true : false):false,['class'=>'semiAnnualODConfirmation table-confirmation' , 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[203][semiAnnualODConfirmation]', 'no',(!empty($answer) && array_key_exists('semiAnnualODConfirmation',$answer)) ? (($answer['semiAnnualODConfirmation']=="no")  ? true : false):false,['class'=>'semiAnnualODConfirmation  table-confirmation' , 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnualODDetailTable" style="display:<?php
                                    if (!empty($answer) && array_key_exists('semiAnnualODConfirmation', $answer) && ($answer['semiAnnualODConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h5> List other debts.</h5>

                                                        <table id='semiAnnualCCInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Item</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["nonBusinessRelatedDebtsRecords"])) {
                                                                    $loans = json_decode($answer["nonBusinessRelatedDebtsRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="SemiAnnualCardInfo" data-index="{{$key}}">
                                                                                <td class="">{{$data->semiAnnualODDebtNickname}}</td>
                                                                                <td></td> 
                                                                                <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <button type='button' class="semiAnnualCCBTN add-dependent" style="cursor:pointer; background: transparent; border: 1px solid #048cdc; color: #018aff; margin-top: 24px; border-radius: 3px;" data-toggle="modal" data-target="#semiAnnualCCModal">Add debt</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="nonBusinessRelatedDebtsDetails" class="hidden" name="answer[203][nonBusinessRelatedDebtsRecords]">{{(!empty($answer["nonBusinessRelatedDebtsRecords"]))? $answer["nonBusinessRelatedDebtsRecords"]:null}}</textarea>
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="semiAnnualCCModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD NON-BUSINESS DEBT</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">

                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this debt a nickname?') }}
                                                                    {{ Form::input('text','semi-annual-od-debt-nickname',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualODDebtNickname', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nick Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this debt?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid semiAnnualODOnDebtName"  id="semiAnnualODothers" name="semi-annual-od-on-debt-name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="1">my name</option>
                                                                        <option value="2">{{$spouse_name}}</option>
                                                                        <option value="3">joint</option>
                                                                        <option value="4">other</option>
                                                                    </select>
                                                                </div> 


                                                                <div class="col-sm-12 inner-left semiAnnualOdOtherNames" style="display: none;">
                                                                    {{ Form::label('label', 'Their Name?') }}
                                                                    {{ Form::input('text','semi-annual-od-on-debt-other-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualODOnDebtOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For what were the proceeds of this loan used?') }}
                                                                    {{ Form::input('text','semiAnnualODLoanUsedProceed',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualODLoanUsedProceed', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn" type="button" >next <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finish -->
                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Who is the lender?') }}
                                                                    {{ Form::input('text','lender',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualODLender', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'When was this loan originated?') }}
                                                                    {{ Form::input('text','loan_originated',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker SemiAnnualODConsolidationLoanDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan in months?') }}
                                                                    {{ Form::input('number','term_of_loan',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualODLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','rate_of_interest',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control semiAnnualODInterestRate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <!--id='SemiAnnualAddCardBtn'-->  
                                                                    <button  class='backBtn' type="button"> <i class="fa fa-arrow-left"></i> back  </button>
                                                                    <button  class='nextBtn pull-right' type="button"> next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- second step finish -->

                                                        <!-- third step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What was the original loan amount?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','original_loan_amount]',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualODOriginalLoanAmount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','current_balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualODCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','monthly_payemnt',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualODMinMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','pay_per_month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualODPayPerMonth', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <!--id='SemiAnnualAddCardBtn'-->  
                                                                    <button class="backBtn" type="button"> <i class="fa fa-arrow-left"></i> back </button>
                                                                    <button  class='nextBtn pull-right' type="button"> next <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- tihrd step finish -->

                                                        <!-- fourth step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a revolving loan?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'yes',false,['class'=>'semiAnnualODRevolvingLoan','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'no',false,['class'=>'semiAnnualODRevolvingLoan','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is there collateral pledged against this loan(i.e. house, car, etc.)?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'yes',false,['class'=>'semiAnnualODCollateralPledge','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'no',false,['class'=>'semiAnnualODCollateralPledge','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left semiAnnualODCollateralDescribeBox" style="display: none">
                                                                    {{ Form::label('label', 'Describe the collateral?') }}
                                                                    {{ Form::textarea('collateral_description',null,['data-validation'=> '' , 'class'=>' custom-validation form-control semiAnnualODCollateralDescribe','rows' => '4' , 'cols' => '30', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <!--id='SemiAnnualAddCardBtn'-->  
                                                                    <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i> back </button>
                                                                    <button  id="semiAnnualCCBTNnew" class='finishBtn semiAnnualCCBT pull-right' type="button"> finish <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- fourth step finish -->


                                                        <!-- steps form finish -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}">Save and return later</a>-->

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finish -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        var SemiAnnualHELOCDetails = [];
        if ($('#nonBusinessRelatedDebtsDetails').val() != '') {
            SemiAnnualHELOCDetails = JSON.parse($('#nonBusinessRelatedDebtsDetails').val());
        }
        $('.nextBtn, .finishBtn').on('click', function () {

            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {

                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#semiAnnualCCModal').modal('hide');
                        SemiAnnualHELOCDetails[$(this).attr('data-index')] = {
                            semiAnnualODDebtNickname: $('#semiAnnualCCModal .semiAnnualODDebtNickname').val(),
                            semiAnnualODOnDebtName: $('#semiAnnualCCModal .semiAnnualODOnDebtName').val(),
                            semiAnnualODOnDebtOtherName: $('#semiAnnualCCModal .semiAnnualODOnDebtOtherName').val(),
                            semiAnnualODLoanUsedProceed: $('#semiAnnualCCModal .semiAnnualODLoanUsedProceed').val(),
                            semiAnnualODLender: $('#semiAnnualCCModal .semiAnnualODLender').val(),
                            SemiAnnualODConsolidationLoanDate: $('#semiAnnualCCModal .SemiAnnualODConsolidationLoanDate').val(),
                            semiAnnualODLoanTerm: $('#semiAnnualCCModal .semiAnnualODLoanTerm').val(),
                            semiAnnualODInterestRate: $('#semiAnnualCCModal .semiAnnualODInterestRate').val(),
                            semiAnnualODOriginalLoanAmount: $('#semiAnnualCCModal .semiAnnualODOriginalLoanAmount').val(),
                            semiAnnualODCurrentBalance: $('#semiAnnualCCModal .semiAnnualODCurrentBalance').val(),
                            semiAnnualODMinMonthlyPayment: $('#semiAnnualCCModal .semiAnnualODMinMonthlyPayment').val(),
                            semiAnnualODPayPerMonth: $('#semiAnnualCCModal .semiAnnualODPayPerMonth').val(),
                            semiAnnualODCollateralPledge: $('#semiAnnualCCModal .semiAnnualODCollateralPledge:checked').val(),
                            semiAnnualODRevolvingLoan: $('#semiAnnualCCModal .semiAnnualODRevolvingLoan:checked').val(),
                            semiAnnualODCollateralDescribe: $('#semiAnnualCCModal .semiAnnualODCollateralDescribe').val()
                        };
                        $('#semiAnnualCCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#semiAnnualCCModal .semiAnnualODDebtNickname').val() + '</td>  <td></td> <td  align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');

                    } else {
                        $('#semiAnnualCCInfo tbody').append('<tr class="SemiAnnualCardInfo"data-index=' + SemiAnnualHELOCDetails.length + '><td>' + $('#semiAnnualCCModal .semiAnnualODDebtNickname').val() + '</td>   <td> </td><td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        SemiAnnualHELOCDetails.push({
                            semiAnnualODDebtNickname: $('#semiAnnualCCModal .semiAnnualODDebtNickname').val(),
                            semiAnnualODOnDebtName: $('#semiAnnualCCModal .semiAnnualODOnDebtName').val(),
                            semiAnnualODOnDebtOtherName: $('#semiAnnualCCModal .semiAnnualODOnDebtOtherName').val(),
                            semiAnnualODLoanUsedProceed: $('#semiAnnualCCModal .semiAnnualODLoanUsedProceed').val(),
                            semiAnnualODLender: $('#semiAnnualCCModal .semiAnnualODLender').val(),
                            SemiAnnualODConsolidationLoanDate: $('#semiAnnualCCModal .SemiAnnualODConsolidationLoanDate').val(),
                            semiAnnualODLoanTerm: $('#semiAnnualCCModal .semiAnnualODLoanTerm').val(),
                            semiAnnualODInterestRate: $('#semiAnnualCCModal .semiAnnualODInterestRate').val(),
                            semiAnnualODOriginalLoanAmount: $('#semiAnnualCCModal .semiAnnualODOriginalLoanAmount').val(),
                            semiAnnualODCurrentBalance: $('#semiAnnualCCModal .semiAnnualODCurrentBalance').val(),
                            semiAnnualODMinMonthlyPayment: $('#semiAnnualCCModal .semiAnnualODMinMonthlyPayment').val(),
                            semiAnnualODPayPerMonth: $('#semiAnnualCCModal .semiAnnualODPayPerMonth').val(),
                            semiAnnualODCollateralPledge: $('#semiAnnualCCModal .semiAnnualODCollateralPledge:checked').val(),
                            semiAnnualODRevolvingLoan: $('#semiAnnualCCModal .semiAnnualODRevolvingLoan:checked').val(),
                            semiAnnualODCollateralDescribe: $('#semiAnnualCCModal .semiAnnualODCollateralDescribe').val()
                        });
                    }
                    $('#semiAnnualCCModal').modal('hide');
                    $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
                }
            }
            return false;
        });

        $(document).on('click', '#semiAnnualCCInfo .fa-pencil', function () {
            $('#semiAnnualCCModal').modal();
            $('#semiAnnualCCModal #step-1').show();
            $('#semiAnnualCCModal #step-2, #semiAnnualCCModal #step-3').css('display', 'none');
            $('#semiAnnualCCModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualCCDetails = SemiAnnualHELOCDetails[index];
            $('#semiAnnualCCModal .semiAnnualODDebtNickname').val(SemiAnnualCCDetails.semiAnnualODDebtNickname);
            $('#semiAnnualCCModal .semiAnnualODOnDebtName').val(SemiAnnualCCDetails.semiAnnualODOnDebtName).trigger('change');
            $('#semiAnnualCCModal .semiAnnualODOnDebtOtherName').val(SemiAnnualCCDetails.semiAnnualODOnDebtOtherName);
            $('#semiAnnualCCModal .semiAnnualODLoanUsedProceed').val(SemiAnnualCCDetails.semiAnnualODLoanUsedProceed);

            $('#semiAnnualCCModal .semiAnnualODLender').val(SemiAnnualCCDetails.semiAnnualODLender);
            $('#semiAnnualCCModal .SemiAnnualODConsolidationLoanDate').val(SemiAnnualCCDetails.SemiAnnualODConsolidationLoanDate);
            $('#semiAnnualCCModal .semiAnnualODLoanTerm').val(SemiAnnualCCDetails.semiAnnualODLoanTerm);
            $('#semiAnnualCCModal .semiAnnualODInterestRate').val(SemiAnnualCCDetails.semiAnnualODInterestRate);

            $('#semiAnnualCCModal .semiAnnualODOriginalLoanAmount').val(SemiAnnualCCDetails.semiAnnualODOriginalLoanAmount);
            $('#semiAnnualCCModal .semiAnnualODCurrentBalance').val(SemiAnnualCCDetails.semiAnnualODCurrentBalance);
            $('#semiAnnualCCModal .semiAnnualODMinMonthlyPayment').val(SemiAnnualCCDetails.semiAnnualODMinMonthlyPayment);
            $('#semiAnnualCCModal .semiAnnualODPayPerMonth').val(SemiAnnualCCDetails.semiAnnualODPayPerMonth);

            $('#semiAnnualCCModal .semiAnnualODCollateralPledge[value=' + SemiAnnualCCDetails.semiAnnualODCollateralPledge + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualODRevolvingLoan[value=' + SemiAnnualCCDetails.semiAnnualODRevolvingLoan + ']').prop('checked', true);
            $('#semiAnnualCCModal .semiAnnualODCollateralDescribe').val(SemiAnnualCCDetails.semiAnnualODCollateralDescribe);

            $('#semiAnnualCCBTNnew').addClass('edit-form');
            $('#semiAnnualCCBTNnew').attr('data-index', index);

            if ($('.semiAnnualODCollateralPledge:checked').val() === 'yes') {
                $('.semiAnnualODCollateralDescribeBox ').show();
            } else {
                $('.semiAnnualODCollateralDescribeBox').hide();
            }

        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('change', '.semiAnnualODConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnualODDetailTable').show();
            } else {
                $('.SemiAnnualODDetailTable').hide();
                SemiAnnualHELOCDetails = [];
                $('.find-table-length tbody tr').remove();
                $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
            }
        });

        $(document).on('change', '.semiAnnualODCollateralPledge', function () {
            if ($(this).val() === 'yes' || $(this).prop('checked', true).val() === 'yes') {
                $('.semiAnnualODCollateralDescribeBox ').show();
            } else {
                $('.semiAnnualODCollateralDescribeBox').hide();
            }
        });

        if ($('.semiAnnualODCollateralPledge:checked').val() === 'yes') {
            $('.semiAnnualODCollateralDescribeBox ').css('display', 'block');
        } else {
            $('.semiAnnualODCollateralDescribeBox').css('display', 'none');
        }



        $(document).on('click', '.semiAnnualCCBTN', function () {

            $('#semiAnnualCCModal #step-1').show();
            $('#semiAnnualCCModal #step-2, #semiAnnualCCModal #step-3, #semiAnnualCCModal #step-4, .semiAnnualODCollateralDescribeBox').hide();
            $('#semiAnnualCCModal').find('.error-alert').remove();

            $('#semiAnnualCCModal input[type="text"], input[type="number"]').val('');
            $('#semiAnnualCCModal input[type="radio"].semiAnnualODRevolvingLoan, #semiAnnualCCModal input[type="radio"].semiAnnualODCollateralPledge').prop('checked', false);
            $('#semiAnnualCCModal select').val('').trigger('change');
            $('#semiAnnualCCModal input[type="radio"]').prop('checked', false);

            $('#semiAnnualCCBTNnew').removeClass('edit-form');
        });
        $(document).on('click', '#semiAnnualCCInfo .fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                SemiAnnualHELOCDetails.splice(index_id, 1);

                $("#semiAnnualCCInfo tbody").empty();
                if (SemiAnnualHELOCDetails.length != 0) {
                    var tr = '';
                    $.each(SemiAnnualHELOCDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.semiAnnualODDebtNickname + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#semiAnnualCCInfo tbody").html(tr);
                }


                $('#nonBusinessRelatedDebtsDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
                return false;
            });
        });

        if ($('#semiAnnualODothers').val() === '4') {
            $('.semiAnnualOdOtherNames').show();
        } else {
            $('.semiAnnualOdOtherNames').hide();
        }

        $("#semiAnnualODothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.semiAnnualOdOtherNames').show();
            } else {
                $('.semiAnnualOdOtherNames').hide();
            }
        });
    });
</script>
@stop