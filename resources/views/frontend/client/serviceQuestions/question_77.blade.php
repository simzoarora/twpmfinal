@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')     
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',19) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">LIABILITIES</h6>        
                                    <h2>What is your credit score?</h2>        
                                    <p>Explanation of why is this needed</p>                  
                                </div>                        

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[77]','What is your credit score') }}      
                                    <div class="col-sm-6 inner-left " >
                                        {{ Form::label('label','Your credit score') }}
                                        {{ Form::input('number','answer[77][credit_score]',(!empty($answer) && array_key_exists('credit_score',$answer)) ? $answer['credit_score']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control user-credit-score', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required','style'=>'text-align:left;']) }}
                                    </div>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <div class="col-sm-6 inner-left partner-credit-score">
                                            {{ Form::label('label','Your spouse\'s/partner\'s credit score') }}
                                            {{ Form::input('number','answer[77][partner_score]',(!empty($answer) && array_key_exists('partner_score',$answer)) ? $answer['partner_score']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'XXX', 'required'=>'required','style'=>'text-align:left;']) }}
                                        </div>   
                                    <?php } ?>
                                </div>                                                                                                                 
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
http://client.twpmdemo.us/servicesQuestion/1/topic/9/preview/liabilities-preview
<script>
      var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $(document).on('focus', '.user-credit-score', function () {
            $(this).css("text-align", "left").prop('placeholder', '').attr('type', 'number');
        });
        $(document).on('keyup', '.user-credit-score', function () {
            if (!$(this).val()) { 
                $(this).css("text-align", "left").prop('placeholder', 'XXX');
            } else {
                $(this).css("text-align", "left").prop('placeholder', '');
            }

        });
    });
</script>

@stop