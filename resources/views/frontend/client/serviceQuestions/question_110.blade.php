<div class="col-sm-12">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> FINANCIAL INFORMATION </p>
        <h4> Combined Annual Income and Net Worth.</h4>
        <p>Descriptive/explanatory statements </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[110][questionName]','Combined Annual Income and Net Worth') }}

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the combined annual income?') }}
            {{ Form::input('hidden','data[110][answer][annualincome]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false"]) }}
            <div class="slidecontainer1">
                <input type="range" min="0" max="500000" value="50" class="slider annual_income" id="range_43">
            </div>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the combined net worth(excluding personal residences?') }}
            {{ Form::input('hidden','data[110][answer][networth]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false"]) }}
            <div class="slidecontainer2">
                <input type="range" min="0" max="2500000" value="50" class="slider net_worth" id="range_44">
            </div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function(){
        $("#range_43").ionRangeSlider({
        type: "single",
        min: 0,
        max: 1000000,
        from: 500000,
        prefix: "$",
        keyboard: true
    });
    
    $("#range_44").ionRangeSlider({
        type: "single",
        min: 0,
        max: 5000000,
        from: 2500000,
        prefix: "$",
        keyboard: true
    });
    
    $('.slidecontainer1 .irs-min').text('$0');
    $('.slidecontainer1 .irs-max').text('$1M+');
    $('.slidecontainer2 .irs-min').text('$0');
    $('.slidecontainer2 .irs-max').text('$5M+');
    });
    
</script>


