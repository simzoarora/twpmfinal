
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative; 
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    /*    table tbody td .fa{
            float: none;
        }*/
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .savingsAccountInfo{
        position: relative; 
    }
</style>
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>Assets</h6>
        <h2>Savings Accounts</h2>
        <p>Please answer the following questions, so we can get the most accurate picture of your finances. If you have questions, feel free to <a href="#">contact us</a>.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','questionName[73]','Savings accounts') }}
        <div class="col-sm-6 inner-left ">
            <label for="label">Do you have a savings account?</label>
            <label class="radio-custom-label">
                <input class="saving-account-button table-confirmation" required="required" name="answer[73][saving_account]" type="radio" value="yes" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('saving_account', $answer) && ($answer['saving_account'] == "yes")) {
                    echo "checked";
                }
                ?>>Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                <input class="saving-account-button table-confirmation" required="required" name="answer[73][saving_account]" type="radio" value="no" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('saving_account', $answer) && ($answer['saving_account'] == "no")) {
                    echo "checked";
                }
                ?>>No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-6 inner-left saving-account-table "  style="display:<?php
        if (!empty($answer) && array_key_exists('saving_account', $answer) && ($answer['saving_account'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>">

            {{ Form::label('label', 'List your savings accounts.') }}

            <table id='savingsAccountInfo' class="find-table-length">
                <thead>
                    <tr>
                        <td style="font-size: 13px;">Account name</td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($answer) && array_key_exists('allSavingData', $answer)) {
                        $account = json_decode($answer["allSavingData"]);
                        if (!empty($account)) {
                            foreach ($account as $key => $data) {
                                ?>
                                <tr data-index="{{$key}}">
                                    <td>{{$data->savingAccountFirstName}}</td>
                                    <td></td>
                                    <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
            <button type='button' class="add-saving-account" data-toggle="modal" data-target="#savingAccountModal">Add account</button>
        </div>
    </div>
    <textarea id="allSavingAccount" class="hidden" name="answer[73][allSavingData]">{{(!empty($answer) && array_key_exists('allSavingData',$answer))? $answer["allSavingData"]:null}}</textarea>
</div>

<!-- new saving account modal starts -->
<div class="sections">
    <!-- modal dialog starts -->
    <div id="savingAccountModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD SAVINGS ACCOUNT</h4>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <div class="row">
                        <!-- first step starts -->
                        <div class=" setup-content">
                            <div class="section-right">
                                <div class="validation-alert">
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Give the account a name') }}
                                        {{ Form::input('text','saving_account_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control saving-account-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Account type') }}
                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('individual-select', 'Individual', ['class' => 'label', 'for' => 'individual-select'])}}
                                            {{ Form::radio('saving_selector', 'individual',null,['class'=>'saving-checking-account',  'id' => 'individual-select', ' required'=>'required']) }}
                                        </div>

                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('joint-select', 'Joint', ['class' => 'label', 'for' => 'joint-select'])}}
                                            {{ Form::radio('saving_selector', 'joint',null,['class'=>'saving-checking-account',  'id' => 'joint-select', ' required'=>'required']) }}
                                        </div>

                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('trust-select', 'Trust  ', ['class' => 'label', 'for' => 'trust-select'])}}
                                            {{ Form::radio('saving_selector', 'trust',null,['class'=>'saving-checking-account',  'id' => 'trust-select', ' required'=>'required']) }}
                                        </div>
                                    </div>


                                    <div class="col-sm-12 saving-account-details" style="display:none;">

                                        <div class="col-sm-12 inner-left">
                                            <label><i>Name of trust</i></label>
                                            {{ Form::input('text','saving_name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control saving-name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                        </div>
                                        <div class="col-sm-12 inner-left">
                                            <label><i>Trustee </i></label>
                                            {{ Form::input('text','saving_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control saving-account-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee', 'required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','What is the average monthly balance?') }}
                                        <div class="input-group">
                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                            {{ Form::input('number','Average_monthly_balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control saving-account-monthly-balance', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        <button id='savingAccountForm'  class="modal-button savingFinishBtn" type="button" style="border-radius: 3px; margin-top: 40px;">
                                            Save
                                        </button>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- modal dialog finish -->
</div>

<!-- new saving account modal finsih -->


<style type="text/css">
    /*.modal-body .setup-content .OptionSelection{ width: calc(100% - 74%);}*/
    .modal-body .setup-content .OptionSelection:nth-child(4n){margin-right: 0}
    .modal-body .setup-content .OptionSelection input[type=radio]{margin: 0; padding: 1px 40px}
    .modal-body .setup-content .OptionSelection .label{height: 40px; line-height: 40px; border: solid 1px #c4c4c4; color:#9b9b9b; font-size: 12px; border-radius: 0}
    .modal-body .setup-content .OptionSelection .married-selected{border:solid 1px #2079ee; color:#fff;}
</style>