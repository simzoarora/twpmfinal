<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about your mortgage on your non-rental property.</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[24][questionName]','Details about your mortgage on your non-rental property') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For Which Property') }}
            {{ Form::input('number','data[24][answer][property]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Address']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the total amount of the line of credit?') }}
            {{ Form::input('number','data[24][answer][amount of the line of credit]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the current balance?') }}
            {{ Form::input('number','data[24][answer][current balance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the interest rate?') }}
            {{ Form::input('number','data[24][answer][interest rate]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Upon what is the interst rate based?') }}
            {{ Form::select('data[24][answer][interest-rate-based]', ['SELECT','10-Year treasury','PRIME','LIBOR','Unsure','Other'],null,['class'=>'rate-based']) }}
        </div>
        <div class="col-sm-8 inner-left rate-based-other" style="display: none;">
            {{ Form::label('label', 'Other') }}
            {{ Form::input('text','data[24][answer][interst rate other]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is your current monthly payment?') }}
            {{ Form::input('number','data[24][answer][current monthly payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
         <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'When did it originate?') }}
            {{  Form::date('data[24][answer][originate]', null, ['class'  => 'form-control'], 'd/m/Y')}}
        </div>
         <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the term of the loan(in years)?') }}
            {{ Form::input('number','data[24][answer][term of the loan]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'years']) }}
        </div>
    </div>
</div>