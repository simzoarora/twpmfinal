@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',14) }}
                    {{ Form::input('hidden','subTopicId',8) }}
                    {{ Form::input('hidden','redirectPageName','spouse-insurance-final-preview') }}


                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} HEALTH INSURANCE</h6> 
                                    <h2>Health Insurance.</h2>
                                  
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[170]','Group disability insurance.') }}

                                    <div class="col-sm-6 inner-left ">

                                        <label for="label">Are you covered by an employer-sponsored health insurance plan?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[170][employer_sponsored_health_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('employer_sponsored_health_insurance',
                                                    $answer) && ($answer['employer_sponsored_health_insurance']
                                                == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[170][employer_sponsored_health_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('employer_sponsored_health_insurance',
                                                    $answer) && ($answer['employer_sponsored_health_insurance']
                                                == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>
                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} HEALTH INSURANCE</h6> 
                                    <h2>Health insurance.</h2>
                                   
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[171]','Group disability insurance.') }}

                                    <div class="col-sm-6 inner-left ">

                                        <label for="label">Do you have personally-owned health insurance?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[171][personally_owned_health_insurance]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('personally_owned_health_insurance',
                                                    $answer) && ($answer['personally_owned_health_insurance']
                                                == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[171][personally_owned_health_insurance]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('personally_owned_health_insurance',
                                                    $answer) && ($answer['personally_owned_health_insurance']
                                                == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>


                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} HEALTH INSURANCE</h6> 
                                    <h2>Health insurance.</h2>
                                   
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[172]','Group disability insurance.') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Are you covered by Medicare?</label>
                                        <label class="radio-custom-label">
                                            <input class="Medicare-cover" required="required" name="answer[172][medicare_cover]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('medicare_cover',
                                                    $answer) && ($answer['medicare_cover']
                                                == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="Medicare-cover" required="required" name="answer[172][medicare_cover]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('medicare_cover',
                                                    $answer) && ($answer['medicare_cover']
                                                == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-6 inner-left number-of-trustees medicare-section table-width" style="display: <?php
                                    if (!empty($answer) && array_key_exists('medicare_cover',
                                            $answer) && ($answer['medicare_cover']
                                        == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        <label for="label">What Medicare Parts do you have?</label>
                                        <p style="width: 100%; font-weight: 400;"><i>Check all that apply</i></p>
                                        <?php
                                        $values = [];
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life',
                                                $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                $values[$parts] = $parts;
                                            }
                                        }
                                        ?>
                                        <div class="trustee termLife"> 

                                            <input id="toggle1" class="term-input" type="checkbox" name="answer[172][medical_parts_term_life][]" value="a" required="" <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                if ($parts == 'a') {
                                                    echo 'checked';
                                                }
                                            }
                                        }
                                        ?>>
                                            <label for="toggle1" class="term-label">A</label>

                                            <input id="toggle2" class="term-input" type="checkbox" name="answer[172][medical_parts_term_life][]" value="b" required="" <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                if ($parts == 'b') {
                                                    echo 'checked';
                                                }
                                            }
                                        }
                                        ?>>
                                            <label for="toggle2" class="term-label" >B</label>

                                            <input id="toggle3" class="term-input" type="checkbox" name="answer[172][medical_parts_term_life][]" value="c" required="" <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                if ($parts == 'c') {
                                                    echo 'checked';
                                                }
                                            }
                                        }
                                        ?>>
                                            <label for="toggle3" class="term-label" style="margin-bottom: 0px;padding-bottom: 2px;line-height: 16px;">C 
                                                <span style="float: left;font-size: 8px; width: 100%;"> (advantage) </span></label>

                                            <input id="toggle4" class="term-input" type="checkbox" name="answer[172][medical_parts_term_life][]" value="d" required="" <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                if ($parts == 'd') {
                                                    echo 'checked';
                                                }
                                            }
                                        }
                                        ?>>
                                            <label for="toggle4" class="term-label">D</label>

                                            <input id="toggle5" class="term-input" type="checkbox" name="answer[172][medical_parts_term_life][]" value="other" required="" <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && !empty($answer['medical_parts_term_life'])) {
                                            foreach ($answer['medical_parts_term_life'] as $parts) {
                                                if ($parts == 'other') {
                                                    echo 'checked';
                                                }
                                            }
                                        }
                                        ?>>
                                            <label for="toggle5" class="term-label">Other</label>


                                        </div>
                                    </div>

                                    <div class="col-sm-6 inner-left explain-other" style="display: <?php
                                        if (!empty($answer) && array_key_exists('medical_parts_term_life', $answer) && ($answer['medical_parts_term_life']=="other")) {
                                                 echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                        ?>">
                                        <label for="label">Medicare type</label>
                                        <input data-validation="" id="otherDetails" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="answer[172][explain_other]" type="text" aria-required="true" value="<?php
                                               if (!empty($answer) && array_key_exists('explain_other',
                                                       $answer)) {
                                                   echo $answer['explain_other'];
                                               }
                                    ?>">
                                    </div>

                                </div>
                            </div>
                        </section> 
                    </fieldset>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,14,'spouse-insurance-final-preview'])}}";
   $(document).ready(function () {
$(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $(document).on('change', '.Medicare-cover', function () {
            if ($(this).val() == 'yes') {
                $('.medicare-section').show();
                $('.medicare-parts').removeClass('selected-option');
            } else {
                $('.medicare-section').hide();
                $('.medicare_inputs').removeAttr('value');
                $('#otherDetails').hide().val('');
            }

        });

        $(document).on('click', '.medicare-parts ', function () {
            $('#medicare_options_' + $(this).attr('data-value')).val($(this).attr('data-value'));
            if ($(this).hasClass('selected-option')) {
                $('#medicare_options_' + $(this).attr('data-value')).removeAttr('value');
            }
            $(this).toggleClass('selected-option');
        });

        var termLifeCheckbox = $("input[type='checkbox']#toggle5"),
                actions = $(".explain-other");
        termLifeCheckbox.change(function () {
            if ($(this).prop('checked')) {
                actions.css('display', 'block');
            } else {
                actions.css('display', 'none');
            }
        });



    });
</script>

@stop