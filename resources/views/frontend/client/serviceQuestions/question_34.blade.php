@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',302) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0;">ASSETS</p>
                                    <h2>Tell us more about your assets.</h2>
                                    <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="#" class="contact-modal-show">contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    <input name="questionName[34]" type="hidden" value="Tell us about your liabilities/debts">
                                    <div class="col-sm-8 inner-left">
                                        <label for="label">Do you own or rent your house?</label>.

                                        <label class="radio-custom-label">    
                                            <input class="own-house" required="required" name="answer[34][own_or_rent_your_house]" type="radio" value="own" aria-required="true"<?php
                                            if (!empty($answer) && array_key_exists('own_or_rent_your_house', $answer) && ($answer['own_or_rent_your_house'] == "own")) {
                                                echo "checked";
                                            }
                                            ?>>Own
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            <input class="own-house" required="required" name="answer[34][own_or_rent_your_house]" type="radio" value="rent" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('own_or_rent_your_house', $answer) && ($answer['own_or_rent_your_house'] == "rent")) {
                                                echo "checked";
                                            }
                                            ?>>Rent 
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-8 inner-left house-value" style="display: none;">
                                        {{ Form::label('label', 'Estimated house value.') }} 
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            {{ Form::input('number','answer[34][house_value]',(!empty($answer) && array_key_exists('house_value',$answer)) ? $answer['house_value'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left investment-value">
                                        {{ Form::label('label', 'Estimated total of cash and investment assets.') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            {{ Form::input('number','answer[34][total_of_cash_and_investment_assets]',(!empty($answer) && array_key_exists('total_of_cash_and_investment_assets',$answer)) ? $answer['total_of_cash_and_investment_assets'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/life-insurance.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();


        $(".actions").addClass('col-xs-offset-0 col-sm-offset-4 col-md-offset-4 col-lg-offset-4');
        $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');

        if ($('.own-house:checked').val() == 'own') {
            $('.total-mortgage').show();
            $('.house-value').show();
        } else {
            $('.total-mortgage').hide();
            $('.house-value').hide();
            $('.house-value input').val('');
        }
        $("select").selectBoxIt();

        $(document).on('change', '.own-house', function () {
            if ($(this).val() == 'own') {
                $('.total-mortgage').show();
                $('.house-value, .house-value input').show();
            } else {
                $('.total-mortgage').hide();
                $('.house-value,  .house-value input').hide();
                $('.house-value input').val('');
            }
        });
    });
</script>

@stop