<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">OTHER ACCOUNTS</p>
        <h2>Liablities Outstanding</h2>
        <p>Enter the estimated amount of liablities (debts) you have outstanding. Please provide the total amount, not the monthly payments.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Liablities Outstanding') }}
            <i class="fa fa-dollar" aria-hidden="true"></i>
            {{ Form::input('text','data[116][answer][Liablities Outstanding]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
    </div>
</div>