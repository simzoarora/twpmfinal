@include('frontend.includes.contact') 
<div class="col-sm-12 "> 
    <div class="col-md-4 col-xs-11 section-left ">
        <h2> Let's make sure we have the right income information</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="#" class="contact-modal-show"> contact us.</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[1]','Let\'s make sure we have the right income information.') }}
        <div class="col-sm-8 inner-left gross-income">
            {{ Form::label('label', 'Household gross income',['title'=>'Total income from all sources']) }} 
            <div class="input-group service-input-group">
                <span class="input-group-addon">$</span>
                {{ Form::input('number','answer[1][household_gross_income]',isset($defaultData)? $defaultData['housesholdIncome']:'',['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','readonly']) }}
            </div>
        </div>

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Household take home pay') }}
            <div class="input-group service-input-group">
                <span class="input-group-addon">$</span>  
                {{ Form::input('number','answer[1][take_home_pay]',(!empty($answer) && array_key_exists('take_home_pay',$answer)) ? $answer['take_home_pay']:null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','min'=>0]) }}
            </div>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Annual Income Tax Refund Average, if any') }}
            <div class="input-group service-input-group">
                <span class="input-group-addon">$</span>
                {{ Form::input('number','answer[1][income_tax_refund]',(!empty($answer) && array_key_exists('income_tax_refund',$answer)) ? $answer['income_tax_refund']:null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
            </div>
        </div>
        <div class="col-sm-8 inner-left custon-continue-button" style="text-align: right;padding-right: 30px;">
            <button type="submit">Continue</button>
        </div>
        <div class="col-sm-8 inner-left custon-return"  style="text-align: right;padding-right: 30px;">
            <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>
        </div>
    </div>
</div>