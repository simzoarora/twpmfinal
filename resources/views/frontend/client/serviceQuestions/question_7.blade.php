@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',503) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Equity Lines of Credit (HELOC)</h2>
                                    <p>Text Area for explanation of HELOC.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[7]','Do you have any home equity lines of credit?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any home equity lines of credit?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[7][heloc_confirmation]', 'yes',(!empty($answer) && array_key_exists('heloc_confirmation',$answer)) ? (($answer['heloc_confirmation']=="yes")  ? true : false):false,['class'=>'lDRConfirmation table-confirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[7][heloc_confirmation]', 'no',(!empty($answer) && array_key_exists('heloc_confirmation',$answer)) ? (($answer['heloc_confirmation']=="no")  ? true : false):false,['class'=>'lDRConfirmation table-confirmation', 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left DRDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('heloc_confirmation', $answer) && ($answer['heloc_confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h6 style="
                                                            font-size: 15px;
                                                            "> List all home equity lines of credit..</h6>

                                                        <table id='SemiAnnual-HELOCInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Property address</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["equity_line_of_credit_records"])) {
                                                                    $childs = json_decode($answer["equity_line_of_credit_records"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->DRPropertyName}}</td>
                                                                                <td  class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-5">
                                                            <div class="row">
                                                                <button type='button' class="SemiAnnualAddDRBtn add-dependent" data-toggle="modal" data-target="#SemiAnnualDRModal">Add HELOC</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="equityLineOfCreditDetails" class="hidden" name="answer[7][equity_line_of_credit_records]">{{(!empty($answer["equity_line_of_credit_records"]))? $answer["equity_line_of_credit_records"]:null}}</textarea>
                                    </div>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="SemiAnnualDRModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD HELOC</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->

                                                        <!-- steps form starts -->
                                                        <form role="form">
                                                            <!-- first step starts -->
                                                            <div class=" setup-content" id="step-1">
                                                                <!--<div class="row">-->
                                                                <div class="section-right">
                                                                    <!-- first step content starts here -->
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'For which property?') }}
                                                                        {{ Form::input('text','heloc-property-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRPropertyName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Street Address']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the total line of credit Amount?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                                                            {{ Form::input('number','heloc-credit-amount',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRCreditAmount borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the current balance?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                                                            {{ Form::input('number','heloc-current-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRCurrentBalance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is your current monthly payment?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" id="basic-addon1">$</span>
                                                                            {{ Form::input('number','heloc-current-month-payment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRCurrentMonthPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','min'=>0]) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button class="nextBtn" type="button" >next<i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                                <!--</div>-->
                                                            </div>
                                                            <!-- first step finsih -->

                                                            <!-- second step starts -->
                                                            <div class=" setup-content" id="step-2" style="display: none">
                                                                <!--<div class="row">-->
                                                                <div class="section-right">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the interest rate?') }} 
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','heloc-interest-rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DRInterestRate', 'data-rule-regex'=>"false", 'required'=>true]) }}
                                                                            <span class="input-group-addon" id="basic-addon1">%</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                        <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                        <select style="position: absolute;" class="DRInterestRateBased"  id="DROthers" name="heloc-interest-rate-based" required="" style="display: none;">
                                                                            <option selected="" disabled="" value="">SELECT</option>
                                                                            <option value="1">10-year Treasury</option>
                                                                            <option value="2">Prime</option>
                                                                            <option value="3">Libor</option>
                                                                            <option value="4">Unsure</option>
                                                                            <option value="5">Other</option>
                                                                        </select>
                                                                    </div>


                                                                    <div class="col-sm-12 inner-left DRInterestrateothers" style="display: none;">
                                                                        {{ Form::label('label', 'Other') }}
                                                                        {{ Form::input('text','heloc-other-option',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DROtherOption', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'please describe']) }}
                                                                    </div>


                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'When did the loan originate?') }}
                                                                        {{ Form::input('text','heloc-loan-originate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker DRLoanOriginate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the term of the loan (in years)?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','heloc-loan-term',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRLoanTerm borderRight0', 'data-rule-regex'=>"false", 'required'=>true ]) }}
                                                                            <span class="input-group-addon" id="basic-addon1">Years</span>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12">
                                                                        <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                        <button id="SemiAnnualAddDRBtnnew" class='SemiAnnualAddDRBtn finishBtn pull-right'type="button"> finish <i class="fa fa-arrow-right"></i></button>
                                                                    </div>

                                                                </div>
                                                                <!--</div>-->
                                                            </div>

                                                            <!-- second step finsih -->
                                                            <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <!--<a class="returnLater"  href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#SemiAnnualDRModal').modal('hide');
                        DRDetails[$(this).attr('data-index')] = {
//                            lDRConfirmation: $('#SemiAnnualCCModal .lDRConfirmation:checked').val(),
                            DRPropertyName: $('#SemiAnnualDRModal .DRPropertyName').val(),
                            DRCreditAmount: $('#SemiAnnualDRModal .DRCreditAmount').val(),
                            DRCurrentBalance: $('#SemiAnnualDRModal .DRCurrentBalance').val(),
                            DRCurrentMonthPayment: $('#SemiAnnualDRModal .DRCurrentMonthPayment').val(),
                            DRInterestRate: $('#SemiAnnualDRModal .DRInterestRate').val(),
                            DROtherOption: $('#SemiAnnualDRModal .DROtherOption').val(),
                            DRLoanOriginate: $('#SemiAnnualDRModal .DRLoanOriginate').val(),
                            DRLoanTerm: $('#SemiAnnualDRModal .DRLoanTerm').val(),
                            DRInterestRateBased: $('#SemiAnnualDRModal .DRInterestRateBased').val()
                        };
                        $('#SemiAnnual-HELOCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#SemiAnnualDRModal .DRPropertyName').val() + '</td>  <td  class="right-align"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#SemiAnnual-HELOCInfo tbody').append('<tr data-index=' + DRDetails.length + '><td>' + $('#SemiAnnualDRModal .DRPropertyName').val() + '</td>   <td  class="right-align"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        DRDetails.push({
                            lDRConfirmation: $('#SemiAnnualCCModal .lDRConfirmation:checked').val(),
                            DRPropertyName: $('#SemiAnnualDRModal .DRPropertyName').val(),
                            DRCreditAmount: $('#SemiAnnualDRModal .DRCreditAmount').val(),
                            DRCurrentBalance: $('#SemiAnnualDRModal .DRCurrentBalance').val(),
                            DRCurrentMonthPayment: $('#SemiAnnualDRModal .DRCurrentMonthPayment').val(),
                            DRInterestRate: $('#SemiAnnualDRModal .DRInterestRate').val(),
                            DROtherOption: $('#SemiAnnualDRModal .DROtherOption').val(),
                            DRLoanOriginate: $('#SemiAnnualDRModal .DRLoanOriginate').val(),
                            DRLoanTerm: $('#SemiAnnualDRModal .DRLoanTerm').val(),
                            DRInterestRateBased: $('#SemiAnnualDRModal .DRInterestRateBased').val()
                        });
                        $('.SemiAnnualAddDRBtn').trigger('click');
                        $('#SemiAnnualDRModal').modal('hide');
                    }
                    $('#SemiAnnualDRModal .DRPropertyName, #SemiAnnualDRModal .DRCreditAmount').val('');
                    $('#SemiAnnualDRModal .lDRConfirmation').prop('checked', false);
                    $('#equityLineOfCreditDetails').html(JSON.stringify(DRDetails));
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        var DRDetails = [];

        if ($('#equityLineOfCreditDetails').val() != '') {
            DRDetails = JSON.parse($('#equityLineOfCreditDetails').val());
        }
        $(document).on('change', '.lDRConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.DRDetailsTable').show();
            } else {
                $('.DRDetailsTable').hide();
                DRDetails = [];
                $('#equityLineOfCreditDetails').html(JSON.stringify(DRDetails));
                $('.DRDetailsTable tbody tr').remove();
            }
        });

        $(document).on('change', '.l-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.lexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('click', '.SemiAnnualAddDRBtn', function () {
            $('#SemiAnnualDRModal #step-1').show();
            $('#SemiAnnualDRModal #step-2').hide();
            $('#SemiAnnualDRModal').find('.error-alert').remove();
        });

        $(document).on('click', '#SemiAnnual-HELOCInfo .fa-pencil', function () {
            $('#SemiAnnualDRModal').find('label.error').remove();
            $('#SemiAnnualDRModal').modal();
            $('#SemiAnnualDRModal #step-1').show();
            $('#SemiAnnualDRModal #step-2, #SemiAnnualDRModal #step-3').css('display', 'none');
            var index = $(this).closest('tr').attr('data-index');
            DRDetailsList = DRDetails[index];
            $('#SemiAnnualDRModal .DRPropertyName').val(DRDetailsList.DRPropertyName);
            $('#SemiAnnualDRModal .DRCreditAmount').val(DRDetailsList.DRCreditAmount);
            $('#SemiAnnualDRModal .DRCurrentBalance').val(DRDetailsList.DRCurrentBalance);
            $('#SemiAnnualDRModal .DRCurrentMonthPayment').val(DRDetailsList.DRCurrentMonthPayment);
            $('#SemiAnnualDRModal .DRInterestRate').val(DRDetailsList.DRInterestRate);
            $('#SemiAnnualDRModal .DROtherOption').val(DRDetailsList.DROtherOption);
            $('#SemiAnnualDRModal .DRLoanOriginate').val(DRDetailsList.DRLoanOriginate);
            $('#SemiAnnualDRModal .DRLoanTerm').val(DRDetailsList.DRLoanTerm);
            $('#SemiAnnualDRModal .DRInterestRateBased').val(DRDetailsList.DRInterestRateBased).trigger('change');
            $('#SemiAnnualDRModal .DROtherOption').val(DRDetailsList.DROtherOption);
            $('#SemiAnnualAddDRBtnnew').addClass('edit-form');
            $('#SemiAnnualAddDRBtnnew').attr('data-index', index);
        });

        $(document).on('click', '.SemiAnnualAddDRBtn', function () {
            $('#SemiAnnualAddDRBtnnew').removeClass('edit-form');
            $('#SemiAnnualDRModal input[type="text"], input[type="number"]').val('');
            $('#SemiAnnualDRModal select').val('').trigger('change');
            $('#SemiAnnualDRModal .error-alert').remove();
            $('#SemiAnnualDRModal #step-1').show();
            $('#SemiAnnualDRModal #step-2').hide();
        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {

                DRDetails.splice(index_id, 1);
                $("#SemiAnnual-HELOCInfo tbody").empty();
                if (DRDetails.length != 0) {
                    var tr = '';
                    $.each(DRDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.DRPropertyName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#SemiAnnual-HELOCInfo tbody").html(tr);
                }
                $('#equityLineOfCreditDetails').html(JSON.stringify(DRDetails));
                return false;
            });
        });

        $("#DROthers").on('change', function () {
            if ($(this).val() === '5') {
                $('.DRInterestrateothers').show();
            } else {
                $('.DRInterestrateothers').hide();
            }
        });

        $(document).on('click', 'SemiAnnualAddDRBtn', function () { // <-- changes
            $('#SemiAnnualDRModal').find('.error-alert').remove();
        });





    });

</script>
@stop