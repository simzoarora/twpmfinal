<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> TAX CERTIFICATION </p>
        <h4> Exemptions-FACTA code.</h4>
        <p>Descriptive/explanatory statements </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have an exemption regarding from FACTA reporting code?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[1113][answer][reportcode]', 'yes',false,[ 'class' => 'radio-value reportcode']) }} Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[1113][answer][reportcode]', 'no',false,[ 'class' => 'radio-value exemptpayee']) }} No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-12 inner-left" id="report-box" style="display: none;">
            {{ Form::label('label', 'Exemption from FACTA reporting code') }}
            {{ Form::input('text','data[1112][answer][code][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'code']) }}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.reportcode').on('change', function () {
            if ($(this).val() === 'yes') {
                $('#report-box').show();
            } else {
                $('#report-box').hide();
            }
        });
    });
</script>
