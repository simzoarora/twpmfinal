<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Life Insurance Trust</h2>
        <p>The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[54]','Life Insurance Trust') }}

        <div class="col-sm-6 inner-left "> 
            {{ Form::label('label', 'Do you have a life insurance trust?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[54][life insurance trust]', 'yes',(!empty($answer) && array_key_exists('life insurance trust',$answer)) ? (($answer['life insurance trust']=="yes")  ? true : false):false,['class'=>'life-insurance-trust' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[54][life insurance trust]', 'no',(!empty($answer) && array_key_exists('life insurance trust',$answer)) ? (($answer['life insurance trust']=="no")  ? true : false):false,['class'=>'life-insurance-trust' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-6 inner-left current-insurance" style="display: <?php
        if (!empty($answer) && array_key_exists('life insurance trust', $answer) && ($answer['life insurance trust'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>;">
            {{ Form::label('label', 'Does it currently own life insurance?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[54][current life insurance]', 'yes',(!empty($answer) && array_key_exists('current life insurance',$answer)) ? (($answer['current life insurance']=="yes")  ? true : false):false,['class'=>'current-life-insurance' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[54][current life insurance]', 'no',(!empty($answer) && array_key_exists('current life insurance',$answer)) ? (($answer['current life insurance']=="no")  ? true : false):false,['class'=>'current-life-insurance' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-6 inner-left name-of-trust" style="display: <?php
        if (!empty($answer) && array_key_exists('life insurance trust', $answer) && ($answer['life insurance trust'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>; ">
            {{ Form::label('label', 'Name of the trust?') }}
            {{ Form::input('text','answer[54][Name of the trust]',(!empty($answer) && array_key_exists('Name of the trust',$answer)) ? $answer['Name of the trust'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'trust name']) }}
        </div>
        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
            <div class="spouse-section">
                <div class="col-sm-6 inner-left ">
                    {{ Form::label('label', 'Does your spouse/partner have a life insurance trust separate from yours?') }}
                    <label class="radio-custom-label">
                        {{ Form::radio('answer[54][spouse life insurance]', 'yes',(!empty($answer) && array_key_exists('spouse life insurance',$answer)) ? (($answer['spouse life insurance']=="yes")  ? true : false):false,['class'=>'spouse-life-insurance' ,'required'=>'required']) }}Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        {{ Form::radio('answer[54][spouse life insurance]', 'no',(!empty($answer) && array_key_exists('spouse life insurance',$answer)) ? (($answer['spouse life insurance']=="no")  ? true : false):false,['class'=>'spouse-life-insurance' ,'required'=>'required']) }}No 
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-6 inner-left spouse-current-insurance" style="display: <?php
                if (!empty($answer) && array_key_exists('spouse life insurance', $answer) && ($answer['spouse life insurance'] == "yes")) {
                    echo 'block';
                } else {
                    echo 'none';
                }
                ?>;">
                    {{ Form::label('label', 'Does it currently own life insurance?') }}
                    <label class="radio-custom-label">
                        {{ Form::radio('answer[54][spouse current life insurance]', 'yes',(!empty($answer) && array_key_exists('spouse current life insurance',$answer)) ? (($answer['spouse current life insurance']=="yes")  ? true : false):false,['class'=>'spouse-current-life-insurance' ,'required'=>'required']) }}Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        {{ Form::radio('answer[54][spouse current life insurance]', 'no',(!empty($answer) && array_key_exists('spouse current life insurance',$answer)) ? (($answer['spouse current life insurance']=="no")  ? true : false):false,['class'=>'spouse-current-life-insurance' ,'required'=>'required']) }}No 
                        <span class="radio-icon"></span>
                    </label>
                </div>       
                <div class="col-sm-6 inner-left spouse-name-of-trust" style="display:  <?php
                if (!empty($answer) && array_key_exists('spouse life insurance', $answer) && ($answer['spouse life insurance'] == "yes")) {
                    echo 'block';
                } else {
                    echo 'none';
                }
                ?>;">
                    {{ Form::label('label', 'Name of the trust?') }}
                    {{ Form::input('text','answer[54][spouse name of the trust]',(!empty($answer) && array_key_exists('spouse name of the trust',$answer)) ? $answer['spouse name of the trust'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control execution-date', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'trust name']) }}
                </div>
            </div>
        <?php } ?>
    </div>
</div> 