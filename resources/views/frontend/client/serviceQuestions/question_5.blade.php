<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about your mortgage on your non-rental property-continued..</h2>
        <p>Please list the total monthly payment amount for each item shown.use your most recent statement if possible.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right checkCalculations">
        {{ Form::input('hidden','data[5][questionName]','Details about your mortgage on your non-rental property-continued') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Principal') }}
            {{ Form::input('number','data[5][answer][principal]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control total-calc', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Principal']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Interest') }}
            {{ Form::input('number','data[5][answer][interest]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control total-calc', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Property tax') }}
            {{ Form::input('number','data[5][answer][property tax]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control total-calc', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Private mortgage insurance') }}
            {{ Form::input('number','data[5][answer][private mortgage insurance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control total-calc', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Other(CCD, fees, etc.)') }}
            {{ Form::input('number','data[5][answer][other]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control other-amount total-calc', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
        </div>
        <div class="col-sm-8 inner-left other-amount-description" style="display: none;">
            {{ Form::input('text','data[5][answer][other information]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Explain']) }}
        </div>
        <div class="col-sm-8 inner-left ">
            {{ Form::input('text','data[5][answer][total]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Total','id'=>'totalValue']) }}
        </div>
    </div>
</div>