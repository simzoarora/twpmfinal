<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Do you have any other vehicle loan?</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[11][questionName]','Do you have any other vehicle loan?') }}
        <div class="col-sm-8 inner-left">
            <label class="radio-custom-label"> {{ Form::radio('data[11][answer][mortgage on non-rental property HELOC ]', 'yes',false,['class'=>'other-vehicle-loan']) }}Yes
                <span class="radio-icon"></span>
            </label>

            <label class="radio-custom-label"> {{ Form::radio('data[11][answer][mortgage on non-rental property HELOC]', 'no',false,['class'=>'other-vehicle-loan']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>