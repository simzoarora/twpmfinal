
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        width: 100%;
        display: inline-block;  
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
    .add-student {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .student-info{
        position: relative;
    }
   .selectboxit-text[data-val=""] {
    color: #b4b6b7! important;
    font-style: italic;
}
</style>
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>GETTING STARTED</h6> 
        <h2>Tell us about the student(s) for whom you are planning.</h2>
        <p>We can best serve you when we know for who's educational expenses you are planning. If you need help, simply <a href="#" class="contact-modal-show" > contact us </a> </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[36]','Tell us about your liabilities/debts') }}
        <table id='student-info'>
            <thead>
                <tr>
                    <td>Student</td> 
                </tr>
            </thead>
            <tbody>
                <?php
                if (!empty($answer["liabilitiesRecords"])) {
                    $students = json_decode($answer["liabilitiesRecords"]);
                    if (!empty($students)) {
                        foreach ($students as $key => $data) {
                            ?>
                            <tr class="children-info" data-index="{{$key}}">
                                <td><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i>{{$data->name}}</td>
                            </tr>
                            <?php
                        }
                    }
                }
                ?>
            </tbody>
        </table>
        <button type='button' style="border-radius:3px;" class="add-student" data-toggle="modal" data-target="#myModal">Add Student</button>
        <textarea id="liabilitiesData" class="hidden" name="answer[36][liabilitiesRecords]">{{(!empty($answer["liabilitiesRecords"]))? $answer["liabilitiesRecords"]:null}}</textarea>
    </div>
</div>
<!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>-->

<!-- Modal -->
<div id="myModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content" style="float:left;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD STUDENT</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <div class="row">
                    <div class=" setup-content">
                        <div class="section-right">
                            <div class="col-sm-12 inner-left validation-alert">
                                {{ Form::label('label', 'Who is the student?') }} 
                                <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                <select placeholder="select" style="position: absolute;" class="who-is-student" required="" name="Who_is_the_student" style="display: none;"><option selected disabled value="">select</option><option value="0">me</option><option value="1">spouse</option><option value="2">child</option><option value="3">grandchild</option><option value="4">other</option></select>
                            </div>
                            <div class="col-sm-12 inner-left"> 
                                {{ Form::label('label', 'Student\'s first Name') }}
                                {{ Form::input('text','student_first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control student-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'first name', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Student\'s current age') }}
                                {{ Form::input('number','studnet_current_age',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control student-age', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age', 'required'=>'required', 'style'=>'text-align:left;','min'=>0, 'max'=>'100']) }}
                            </div>

                            <div class="col-sm-12 inner-left validation-alert">
                                {{ Form::label('label', 'For what level of education are you planning?') }}
                                <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="top: 65px;"></i>
                                {{ Form::select('level_of_education', [null=>'SELECT','primary', 'high school','associates/bachelor\'s','trade school','masters','JD/MD/PhD','other'] ,null,['class'=>'level-of-education', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                <button id='studentform' type="button" style="border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;
                                        padding: 12px 35px;
                                        background-color: #2179EE;
                                        text-decoration: none;
                                        border: none;
                                        margin-left: 62px;
                                        margin-bottom: 40px;">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>