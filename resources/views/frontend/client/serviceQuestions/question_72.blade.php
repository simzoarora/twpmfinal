
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }

    table tbody td .fa{
        /*float: right;*/
        margin-left: 10px;
        font-size: 13px;
    }
    .add-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .account-info{
        position: relative;
    }
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',12) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>Assets</h6>
                                    <h2>Checking Accounts</h2>
                                    <p>Please answer the following questions, so we can get the most accurate picture of your finances. If you have questions, feel free to <a href="#">contact us</a>.</p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[72]','Checking accounts') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have a checking account?</label>
                                        <label class="radio-custom-label">
                                            <input class="checking-account-button table-confirmation" required="required" name="answer[72][checking account]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('checking account', $answer) && ($answer['checking account'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="checking-account-button table-confirmation" required="required" name="answer[72][checking account]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('checking account', $answer) && ($answer['checking account'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left checking-account-table " style="display:<?php
                                    if (!empty($answer) && array_key_exists('checking account', $answer) && ($answer['checking account'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'List your checking accounts.') }}
                                        <table id='account-info'>
                                            <thead>
                                                <tr>
                                                    <td style="font-size: 13px;">Account name</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer) && array_key_exists('checkingAccountRecords', $answer)) {
                                                    $account = json_decode($answer["checkingAccountRecords"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->bankAccountFirstName}}</td>
                                                                <td></td>
                                                                <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-account" data-toggle="modal" data-target="#bankAccountModal">Add account</button>
                                    </div>
                                </div>
                            </div>
                            <textarea id="checkingAccountData" class="hidden" name="answer[72][checkingAccountRecords]">{{(!empty($answer) && array_key_exists('checkingAccountRecords',$answer))? $answer["checkingAccountRecords"]:null}}</textarea>
                            <div class="sections">
                                <!-- modal dialog starts -->
                                <div id="bankAccountModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD CHECKING ACCOUNT</h4>
                                            </div>
                                            <div class="modal-body add-checking-account" style="overflow: hidden;">
                                                <div class="row">
                                                    <!-- first step starts -->
                                                    <div class=" setup-content">
                                                        <div class="section-right">
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give the account a name') }}
                                                                    {{ Form::input('text','account_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control bank-account-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required','pattern'=>'^[a-zA-Z][\sa-zA-Z]*']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Account type') }}
                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('individual-select', 'Individual', ['class' => 'label', 'for' => 'individual-select'])}}
                                                                        {{ Form::radio('selector', 'individual',null,['class'=>'checking-account ',  'id' => 'individual-select', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('joint-select', 'Joint', ['class' => 'label', 'for' => 'joint-select'])}}
                                                                        {{ Form::radio('selector', 'joint',null,['class'=>'checking-account',  'id' => 'joint-select', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('trust-select', 'Trust  ', ['class' => 'label', 'for' => 'trust-select'])}}
                                                                        {{ Form::radio('selector', 'trust',null,['class'=>'checking-account ',  'id' => 'trust-select', ' required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 account-details" style="display:none;">

                                                                    <div class="col-sm-12 inner-left">
                                                                        <label><i>Name of trust</i></label>
                                                                        {{ Form::input('text','name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                                    </div>
                                                                    <div class="col-sm-12 inner-left">
                                                                        <label><i>Trustee</i></label>
                                                                        {{ Form::input('text','trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee name', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Average monthly balance, after expenses?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1">$</span>
                                                                        {{ Form::input('number','average_monthly_banlance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control average-monthly-balance', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <button id='bankAccountForm'  class="modal-button finishBtn" type="button" style="text-align:center; float:none; border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px; padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-left: 62px; margin-bottom: 40px;">
                                                                        Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <!-- modal dialog finish -->
                            </div>
                        </section>
                    </fieldset> 
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_73') 
                        </section>
                    </fieldset> 
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_74') 
                        </section>
                    </fieldset> 
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.question_75') 
                        </section>
                    </fieldset> 
                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">      
                            @include('frontend.client.serviceQuestions.question_76') 
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
{{ Html::script(asset('js/comprehensive-planning.js')) }}
<script>

    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
    $(document).on('keypress', 'input[type=text]', function (e) {
        if (e.which === 32 && !this.value.length) {
            e.preventDefault();
        }
        var inputValue = event.charCode;
        if (!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) {
            event.preventDefault();
        }
    });
//   for decimal
    $(document).on('keydown','input[type=number]',function (event) {


        if (event.shiftKey == true) {
            event.preventDefault();
        }

        if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190 || event.keyCode == 110) {
        } else {
            event.preventDefault();
        }

        if ($(this).val().indexOf('.') !== -1 && (event.keyCode == 190 || event.keyCode == 110))
            event.preventDefault();

    });
    $(document).ready(function () {

        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        var insuranceList = [];
        if ($('#checkingAccountData').val() != '') {
            insuranceList = JSON.parse($('#checkingAccountData').val());
        }
        $('#bankAccountModal').on('click', '.close, #bankAccountForm', function () {
            $('#bankAccountModal .checking-account').removeClass('selected-option');
        });

        // ------------------------------ form modal validation starts 
        $('.finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        insuranceList[$(this).attr('data-index')] = {
                            bankAccountFirstName: $('#bankAccountModal .bank-account-first-name').val(),
                            checkingAccount: $('#bankAccountModal .checking-account:checked').val(),
                            nameOfTrust: $('#bankAccountModal .name-of-trust').val(),
                            Trustee: $('#bankAccountModal .trustee').val(),
                            averageMonthlyBalance: $('#bankAccountModal .average-monthly-balance').val()
                        };
                        $('#account-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#bankAccountModal .bank-account-first-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>  <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i> </td>');
                    } else {
                        $('#account-info tbody').append('<tr data-index=' + insuranceList.length + '><td>' + $('#bankAccountModal .bank-account-first-name').val() + '</td>  <td valign="top"> </td><td valign="top" align="right">   <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i> </td></tr>');
                        insuranceList.push({
                            bankAccountFirstName: $('#bankAccountModal .bank-account-first-name').val(),
                            checkingAccount: $('#bankAccountModal .checking-account:checked').val(),
                            nameOfTrust: $('#bankAccountModal .name-of-trust').val(),
                            Trustee: $('#bankAccountModal .trustee').val(),
                            averageMonthlyBalance: $('#bankAccountModal .average-monthly-balance').val()
                        });
                    }
                    $('#checkingAccountData').html(JSON.stringify(insuranceList));
                    $('#bankAccountModal').modal('hide');
                }
            }
            return false;
        });

        // ------------------------------ form modal validation finish
        $('#account-info').on('click', '.fa-pencil', function () {
            $('#bankAccountModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = insuranceList[index];
            $('#bankAccountModal .bank-account-first-name').val(insuranceDetails.bankAccountFirstName);
            $('#bankAccountModal .checking-account[value=' + insuranceDetails.checkingAccount + ']').prop('checked', true);
            $('#bankAccountModal .name-of-trust').val(insuranceDetails.nameOfTrust);
            $('#bankAccountModal .trustee').val(insuranceDetails.Trustee);
            $('#bankAccountModal .average-monthly-balance').val(insuranceDetails.averageMonthlyBalance);
            $('#bankAccountForm').addClass('edit-form').attr('data-index', index);
            $('#bankAccountModal .error-alert').remove();

            if ($('.checking-account:checked').val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }

            if ($('.checking-account:checked').val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }

            if ($('.checking-account:checked').val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }
        });

        $('.add-account').on('click', function () {
            $('#bankAccountForm').removeClass('edit-form');
            $('.checking-account').removeClass('selected-option');
            $('#bankAccountModal .error-alert').remove();
            $('#bankAccountModal input[type=text], #bankAccountModal input[type=number]').val('');
            $('#bankAccountModal input[type="radio"]').prop('checked', false);
            $('#bankAccountModal .OptionSelection label').removeClass('married-selected');
            $('.account-details').hide();
        });

        $('#account-info').on('click', '.fa-trash', function () {



            var index_id = $(this).closest('tr').attr('data-index')
            deleteRow(index_id);
            $('.swal-button--danger').click(function () {
                $(this).closest('tr').remove();
                insuranceList.splice(index_id, 1);
                $("#account-info tbody").empty();
                if (insuranceList.length != 0) {
                    var tr = '';
                    $.each(insuranceList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.bankAccountFirstName + '</td><td></td><td align="right"> <i title="Edit" class="fa  fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#account-info tbody").html(tr);
                }
                // need to paste on all pages finsih --
                $('#checkingAccountData').html(JSON.stringify(insuranceList));
            });
        });

        $(document).on('change', '.checking-account-button', function () {
            if ($(this).val() === 'yes') {
                $('.checking-account-table').show();
            } else {
                $('.checking-account-table').hide();
                insuranceList = [];
                $('#checkingAccountData').html(JSON.stringify(insuranceList));
            }
        });

        $(document).on('change', '.checking-account', function () {
            if ($(this).val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.checking-account', function () {
            if ($(this).val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.checking-account', function () {
            if ($(this).val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }
        });


    });

</script>

<!--QUESTION 73--> 
<script>

    $(document).ready(function () {

        $("select").selectBoxIt();

        $(document).on('change', '.saving-account-button', function () {
            if ($(this).val() === 'yes') {
                $('.saving-account-table').show();
            } else {
                $('.saving-account-table').hide();
                savingList = [];
                $('#allSavingAccount').html(JSON.stringify(savingList));
            }
        });

        $('#savingAccountModal').on('click', '.close, #savingAccountForm', function () {
            $('.saving-account').removeClass('selected-option');
        });

        var savingList = [];
        var savingDetails = [];
        if ($('#allSavingAccount').val() !== '') {
            savingList = JSON.parse($('#allSavingAccount').val());
        }


        //// ------------------- form avalidation js starts 
        $('.savingFinishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        savingList[$(this).attr('data-index')] = {
                            savingAccountFirstName: $('#savingAccountModal .saving-account-first-name').val(),
                            savingcheckingAccount: $('#savingAccountModal .saving-checking-account:checked').val(),
                            savingNameOfTrust: $('#savingAccountModal .saving-name-of-trust').val(),
                            savingAccountTrustee: $('#savingAccountModal .saving-account-trustee').val(),
                            savingAccountMonthlyBalance: $('#savingAccountModal .saving-account-monthly-balance').val()
                        };
                        $('#savingsAccountInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#savingAccountModal .saving-account-first-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right">  <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#savingsAccountInfo tbody').append('<tr data-index=' + savingList.length + '><td>' + $('#savingAccountModal .saving-account-first-name').val() + '</td>  <td valign="top"> </td><td valign="top" align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>  </td></tr>');
                        savingList.push({
                            savingAccountFirstName: $('#savingAccountModal .saving-account-first-name').val(),
                            savingcheckingAccount: $('#savingAccountModal .saving-checking-account:checked').val(),
                            savingNameOfTrust: $('#savingAccountModal .saving-name-of-trust').val(),
                            savingAccountTrustee: $('#savingAccountModal .saving-account-trustee').val(),
                            savingAccountMonthlyBalance: $('#savingAccountModal .saving-account-monthly-balance').val()
                        });
                    }
                    $('#allSavingAccount').html(JSON.stringify(savingList));
                    $('#savingAccountModal').modal('hide');
                }
            }
            return false;
        });
        // -------------------- form validation js finish 

        $('#savingsAccountInfo').on('click', '.fa-pencil', function () {
            $('#savingAccountModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            savingDetails = savingList[index];
            $('#savingAccountModal .saving-account-first-name').val(savingDetails.savingAccountFirstName);
            $('#savingAccountModal .saving-checking-account[value=' + savingDetails.savingcheckingAccount + ']').prop('checked', true);
            $('#savingAccountModal .saving-name-of-trust').val(savingDetails.savingNameOfTrust);
            $('#savingAccountModal .saving-account-trustee').val(savingDetails.savingAccountTrustee);
            $('#savingAccountModal .saving-account-monthly-balance').val(savingDetails.savingAccountMonthlyBalance);
            $('#savingAccountForm').addClass('edit-form').attr('data-index', index);
            $('#savingAccountModal .error-alert').remove();

            if ($('.saving-checking-account:checked').val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.saving-account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }

            if ($('.saving-checking-account:checked').val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.saving-account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }

            if ($('.saving-checking-account:checked').val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.saving-account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }

        });

        $('.add-saving-account').on('click', function () {
            $('#savingAccountForm').removeClass('edit-form');
            $('.saving-account').removeClass('selected-option');
            $('#savingAccountModal .error-alert').remove();
            $('#savingAccountModal input[type=text], #savingAccountModal input[type=number]').val('');
            $('#savingAccountModal input[type="radio"]').prop('checked', false);
            $('#savingAccountModal .OptionSelection label').removeClass('married-selected');
            $('.saving-account-details').hide();
        });

        $('#savingsAccountInfo').on('click', '.fa-trash', function () {


            var index_id = $(this).closest('tr').attr('data-index')
            deleteRow(index_id);
            $('.swal-button--danger').click(function () {
                $(this).closest('tr').remove();
                savingList.splice(index_id, 1);
                $("#savingsAccountInfo tbody").empty();
                if (savingList.length != 0) {
                    var tr = '';
                    $.each(savingList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.savingAccountFirstName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#savingsAccountInfo tbody").html(tr);
                }
                $('#allSavingAccount').html(JSON.stringify(savingList));
            });
            return false;
        });

        $(document).on('change', '.saving-checking-account', function () {
            if ($(this).val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.saving-account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.saving-checking-account', function () {
            if ($(this).val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.saving-account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.saving-checking-account', function () {
            if ($(this).val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.saving-account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }
        });

    });
</script>

<!--QUESTION 74--> 
<script>

    $(document).ready(function () {
        $("select").selectBoxIt();

        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('click', '.retirement-account', function () {
            $(this).addClass('selected-option').siblings().removeClass('selected-option');
            $('.retirement-account').val($(this).attr('data-value'));
            if ($(this).val() == 'trust') {
                $('.retirement-account-details').show();
            } else {
                $('.retirement-account-details').hide();
            }
        });

        $('#nonRetirementModal').on('click', '.close, #nonRetirementForm', function () {
            $('.retirement-account').removeClass('selected-option');
        });

        var retirementList = [];
        if ($('#nonRetirementData').val() !== '') {
            retirementList = JSON.parse($('#nonRetirementData').val());
        }
        // ---------------------- form validation js starts 
        $('.non-retirement-finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        retirementList[$(this).attr('data-index')] = {
                            cdAccountFirstName: $('#nonRetirementModal .cd-account-first-name').val(),
                            cdCheckingAccount: $('#nonRetirementModal .cd-checking-account:checked').val(),
                            cdAccountValue: $('#nonRetirementModal .cd-account-value').val(),
                            cdTermDetail: $('#nonRetirementModal .cd-term-detail').val(),
                            maturitydatetimepicker: $('#nonRetirementModal .maturitydatetimepicker').val(),
                            cdInterestRate: $('#nonRetirementModal .cd-interest-rate').val(),
                            cdAccountTrustee: $('#nonRetirementModal .cd-account-trustee').val(),
                            cdNameofTrust: $('#nonRetirementModal .cd-name-of-trust').val()
                        };
                        $('#nonRetirementInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#nonRetirementModal .cd-account-first-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#nonRetirementInfo tbody').append('<tr data-index=' + retirementList.length + '><td>' + $('#nonRetirementModal .cd-account-first-name').val() + '</td>  <td valign="top"> </td><td valign="top" align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        retirementList.push({
                            cdAccountFirstName: $('#nonRetirementModal .cd-account-first-name').val(),
                            cdCheckingAccount: $('#nonRetirementModal .cd-checking-account:checked').val(),
                            cdAccountValue: $('#nonRetirementModal .cd-account-value').val(),
                            cdTermDetail: $('#nonRetirementModal .cd-term-detail').val(),
                            maturitydatetimepicker: $('#nonRetirementModal .maturitydatetimepicker').val(),
                            cdInterestRate: $('#nonRetirementModal .cd-interest-rate').val(),
                            cdAccountTrustee: $('#nonRetirementModal .cd-account-trustee').val(),
                            cdNameofTrust: $('#nonRetirementModal .cd-name-of-trust').val()
                        });
                    }
                    $('#nonRetirementData').html(JSON.stringify(retirementList));
                    $('#nonRetirementModal').modal('hide');
                }
            }
            return false;
        });

        $('#nonRetirementInfo').on('click', '.fa-pencil', function () {
            $('#nonRetirementModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            retirementDetails = retirementList[index];
            $('#nonRetirementModal .cd-account-first-name').val(retirementDetails.cdAccountFirstName);
            $('#nonRetirementModal .cd-checking-account[value=' + retirementDetails.cdCheckingAccount + ']').prop('checked', true);
            $('#nonRetirementModal .cd-account-value').val(retirementDetails.cdAccountValue);
            $('#nonRetirementModal .cd-term-detail').val(retirementDetails.cdTermDetail);
            $('#nonRetirementModal .maturitydatetimepicker').val(retirementDetails.maturitydatetimepicker);
            $('#nonRetirementModal .cd-interest-rate').val(retirementDetails.cdInterestRate);
            $('#nonRetirementModal .cd-account-trustee').val(retirementDetails.cdAccountTrustee);
            $('#nonRetirementModal .cd-name-of-trust').val(retirementDetails.cdNameofTrust);

            $('#nonRetirementForm').addClass('edit-form').attr('data-index', index);
            $('#nonRetirementModal .error-alert').remove();

            if ($('.cd-checking-account:checked').val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.retirement-account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }

            if ($('.cd-checking-account:checked').val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.retirement-account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }

            if ($('.cd-checking-account:checked').val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.retirement-account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }
        });
        // --------------------- form validation js finish 

        $('.add-retiremement-account').on('click', function () {
            $('#nonRetirementForm').removeClass('edit-form');
            $('.retirement-account').removeClass('selected-option');
            $('#nonRetirementModal .error-alert').remove();
            $('#nonRetirementModal input[type=text], #nonRetirementModal input[type=number]').val('');
            $('#nonRetirementModal input[type="radio"]').prop('checked', false);
            $('#nonRetirementModal .OptionSelection label').removeClass('married-selected');
            $('.retirement-account-details').hide();
        });

        $('#nonRetirementInfo').on('click', '.fa-trash', function () {
            var index_id = $(this).closest('tr').attr('data-index')

            deleteRow(index_id);

            $('.swal-button--danger').click(function () {

                $(this).closest('tr').remove();
                retirementList.splice(index_id, 1);

                $("#nonRetirementInfo tbody").empty();
                if (retirementList.length != 0) {
                    var tr = '';
                    $.each(retirementList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.cdAccountFirstName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#nonRetirementInfo tbody").html(tr);
                }


                $('#nonRetirementData').html(JSON.stringify(retirementList));
            });
            return false;
        });

        $(document).on('change', '.non-Retirement-button', function () {
            if ($(this).val() == 'yes') {
                $('.non-Retirement-table').show();
            } else {
                $('.non-Retirement-table').hide();
                retirementList = [];
                $('#nonRetirementData').html(JSON.stringify(retirementList));
            }
        });

        $(document).on('change', '.cd-checking-account', function () {
            if ($(this).val() === 'trust') {
                $('label[for=trust-select]').addClass('married-selected');
                $('.retirement-account-details').show();
            } else {
                $('label[for=trust-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cd-checking-account', function () {
            if ($(this).val() === 'joint') {
                $('label[for=joint-select]').addClass('married-selected');
                $('.retirement-account-details').hide();
            } else {
                $('label[for=joint-select]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.cd-checking-account', function () {
            if ($(this).val() === 'individual') {
                $('label[for=individual-select]').addClass('married-selected');
                $('.retirement-account-details').hide();
            } else {
                $('label[for=individual-select]').removeClass('married-selected');
            }
        });
    });

</script>

<!--QUESTION 75--> 
<script>

    $(document).ready(function () {

        $(document).each(function () {
            $(document).on('change', '.upload-statement input[type=file]', function () {
                if ($('.upload-statement .upload-statment span').text().length > 0) {
                    $('.upload-statement .statement-info').removeClass('hide');
                    $('.upload-statement .statement-info table tbody').append('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="file_path" class="filePath"></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                    $(".statement-info .filename").text($('.upload-statement .upload-statment span').text());
                    $(".upload-statement .upload-statement-click").addClass('hide');
                    $(".upload-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var uploadStatementFile = new FormData();
                    uploadStatementFile.append('file', $('[name=statement_file]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: uploadStatementFile,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".statement-info .filePath").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });

        $(document).each(function () {
            $(document).on('change', '.upload-list input[type=file]', function () {
                if ($('.upload-list .upload-statment span').text().length > 0) {
                    $('.upload-list .upload-info').removeClass('hide');
                    $('.upload-list .upload-info table tbody').append('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="upload_file_path" class="uploadFilePath"> </td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                    $(".upload-info .filename").text($('.upload-list .upload-statment span').text());
                    $(".upload-list .upload-statement-click").addClass('hide');
                    $(".upload-list .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var uploadStatementInfo = new FormData();
                    uploadStatementInfo.append('file', $('[name=upload_info]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: uploadStatementInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".upload-info .uploadFilePath").val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });


        'use strict';
        ;
        (function (document, window, index)
        {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener('focus', function () {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('has-focus');
                });
            });
        }(document, window, 0));


        $("select").selectBoxIt();
        var retirementAccountList = [];
        var retirementAccountDetails = [];
        if ($('#taxableAccountDetails').val() !== '') {
            retirementAccountList = JSON.parse($('#taxableAccountDetails').val());
        }

        $('.retirem-finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    var filesComprehensive = [];

                    filesComprehensive.push(
                            {
                                "key": "statementUpload", "file_name": $('.statement-info table tbody td .filename').text()
                                , "file_path": $('.statement-info table tbody td .filePath').val()
                            },
                            {
                                "key": "UploadList", "file_name": $('.upload-list table tbody td .filename').text()
                                , "file_path": $('.upload-list table tbody td .uploadFilePath').val()
                            }
                    );

                    if ($(this).hasClass('edit-form')) {
                        retirementAccountList[$(this).attr('data-index')] = {
                            retireAccountName: $('#retirementModal .retire-account-first-name').val(),
                            whoseAccount: $('#retirementModal .whose-retirement-account').val(),
                            accountHolder: $('#retirementModal .accountholder-name-input').val(),
                            sponsoredPlan: $('#retirementModal .employer-sponsored:checked').val(),
                            statementUploadText: $('.statement-info table tbody td .filename').text(),
                            statementUploadFilePath: $('.statement-info table tbody td .filePath').val(),
                            UploadListText: $('.upload-list table tbody td .filename').text(),
                            UploadListFilePath: $('.upload-list table tbody td .uploadFilePath').val(),
                            filesComprehensive: filesComprehensive
                        };
                        $('#retirementInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#retirementModal .retire-account-first-name').val() + '</td> <td> </td> <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>  <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                    } else {
                        $('#retirementInfo tbody').append('<tr data-index=' + retirementAccountList.length + '><td>' + $('#retirementModal .retire-account-first-name').val() + '</td> <td>  </td> <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                        retirementAccountList.push({
                            retireAccountName: $('#retirementModal .retire-account-first-name').val(),
                            whoseAccount: $('#retirementModal .whose-retirement-account').val(),
                            accountHolder: $('#retirementModal .accountholder-name-input').val(),
                            sponsoredPlan: $('#retirementModal .employer-sponsored:checked').val(),
                            statementUploadText: $('.statement-info table tbody td .filename').text(),
                            statementUploadFilePath: $('.statement-info table tbody td .filePath').val(),
                            UploadListText: $('.upload-list table tbody td .filename').text(),
                            UploadListFilePath: $('.upload-list table tbody td .uploadFilePath').val(),
                            filesComprehensive: filesComprehensive

                        });
                    }
                    $('#retirementModal').modal('hide');
                    $('#taxableAccountDetails').html(JSON.stringify(retirementAccountList));
                }
            }
            return false;
        });

        $(document).on('click', '#retirementInfo .fa-pencil', function () {
            $('#retirementModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            retirementAccountDetails = retirementAccountList[index];
            $('#retirementModal .retire-account-first-name').val(retirementAccountDetails.retireAccountName);
            $('#retirementModal .whose-retirement-account').val(retirementAccountDetails.whoseAccount).trigger('change');
            $('#retirementModal .accountholder-name-input').val(retirementAccountDetails.accountHolder);
            $('#retirementModal .employer-sponsored[value=' + retirementAccountDetails.sponsoredPlan + ']').prop('checked', true);
            $('#retirementModal .statement-info table tbody td .filename').text(retirementAccountDetails.statementUploadText);
            $('#retirementModal .upload-list table tbody td .filename').text(retirementAccountDetails.UploadListText);
            $('.upload-statement .upload-statment-noclick, .upload-list .upload-statment-noclick').removeClass('hide');
            $('.upload-statement .upload-statement-click, .upload-list .upload-statement-click').addClass('hide');
            if (retirementAccountDetails.statementUploadText != "") {
                $('.upload-statement .statement-info table tbody').html('<tr style="border-bottom:0"><td><span class="filename">' + retirementAccountDetails.statementUploadText + '</span><input type="hidden" name="file_path" class="filePath" value="' + retirementAccountDetails.statementUploadFilePath + '"></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                $('.upload-statement .statement-info').removeClass('hide');
                $('.upload-statement .upload-statment-noclick').css('cursor: not-allowed');
                $('.upload-statement .upload-statement-click').css('cursor: not-allowed');
            }
            if (retirementAccountDetails.sponsoredPlan == "yes" && retirementAccountDetails.UploadListText != "") {
                $('.upload-list .upload-info table tbody').html('<tr style="border-bottom:0"><td><span class="filename">' + retirementAccountDetails.UploadListText + '</span><input type="hidden" name="upload_file_path" value="' + retirementAccountDetails.UploadListFilePath + '" class="uploadFilePath"> </td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                $('.upload-list .upload-info').removeClass('hide');

            }
            $('#retirementForm').addClass('edit-form');
            $('#retirementForm').attr('data-index', index);
            $('#retirementModal .error-alert').remove();


            if ($('.employer-sponsored:checked').val() == 'yes') {
                $('.upload-list').show();
            } else {
                $('.upload-list').hide();
            }
            $(document).on('change', '.employer-sponsored', function () {
                if ($(this).val() == 'yes') {
                    $('.upload-list').show();
                    $('.upload-statement .upload-statment-noclick, .upload-list .upload-statment-noclick').addClass('hide');
                    $('.upload-statement .upload-statement-click, .upload-list .upload-statement-click').removeClass('hide');
                    $('.upload-statement .upload-statement-click').css('cursor: allowed');
                } else {
                    $('.upload-list').hide();
                }
            });
        });

        $('.add-retirement-account').on('click', function () {
            $('#retirementForm').removeClass('edit-form');
            $('#retirementModal input[type="text"],input[type="number"]').val('');
            $('#retirementModal select').val('').trigger('change');
            $('#retirementModal .employer-sponsored').prop('checked', false);
            //            $('#retirementModal .whose-retirement-account').prop('selectedIndex', 0);
            $('#retirementModal .error-alert').remove();
            $('.upload-statement .upload-statement-click, .upload-list .upload-statement-click').removeClass('hide');
            $('.upload-statement .upload-statment-noclick, .upload-list .upload-statment-noclick, .upload-info').addClass('hide');

            $('.upload-list table tbody, .statement-info table tbody').find('tr').remove();
            $('.upload-list').hide();

        });

        $('#retirementInfo').on('click', '.fa-trash', function () {

            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {

                $(this).closest('tr').remove();
                retirementAccountList.splice(index_id, 1);

                $("#retirementInfo tbody").empty();
                if (retirementAccountList.length != 0) {
                    var tr = '';
                    $.each(retirementAccountList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.retireAccountName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#retirementInfo tbody").html(tr);
                }
                $('#taxableAccountDetails').html(JSON.stringify(retirementAccountList));

            });
        });

        $('.statement-info table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $(".upload-statement .upload-statement-click").removeClass('hide');
            $(".upload-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.upload-statement .upload-statment span').text('');
        });

        $('.upload-list table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $(".upload-list .upload-statement-click").removeClass('hide');
            $(".upload-list .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.upload-list .upload-statment span').text('');
        });


        $(document).on('change', '.retirement-button', function () {
            if ($(this).val() == 'yes') {
                $('.retirement-table').show();
            } else {
                $('.retirement-table').hide();
            }
        });
        $(document).on('change', '.whose-retirement-account', function () {
            if ($(this).val() == '3') {
                $('.accountholder-name').show();
            } else {
                $('.accountholder-name').hide();
            }
        });
        $(document).on('change', '.employer-sponsored', function () {
            if ($(this).val() == 'yes') {
                $('.upload-list').show();
            } else {
                $('.upload-list').hide();
            }
        });
    });

</script>

<!--QUESTION 76-->
<script>

    $(document).ready(function () {

        $(document).each(function () {
            $(document).on('change', '.ret-upload-statement input[type=file]', function () {
                if ($('.ret-upload-statement .upload-statment span').text().length > 0) {
                    $('.ret-upload-statement .statement-info').removeClass('hide');
                    $('.ret-upload-statement .statement-info table tbody').append('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="upload_file_path" class="filePath"></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                    $(".statement-info .filename").text($('.ret-upload-statement .upload-statment span').text());
                    $(".ret-upload-statement .ret-upload-statement-click").addClass('hide');
                    $(".ret-upload-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    var formData = new FormData();
                    formData.append('file', $('[name=file]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $(".statement-info .filePath").val(response.message);
                        },

                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }

            });
        });

        'use strict';
        ;
        (function (document, window, index)
        {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener('focus', function () {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('has-focus');
                });
            });
        }(document, window, 0));


        $("select").selectBoxIt();
        var educationalList = [];

        if ($('#educationalAccountDetails').val() != '') {
            educationalList = JSON.parse($('#educationalAccountDetails').val());
        }
//         $('.datetimepicker').datetimepicker({
//            format: 'MM/DD/YYYY'
//        });

        $('.estimated-start-year').datetimepicker({
            format: 'YYYY'
        });
        // ------------------- js start for form validation
        $(document).on('click', '.eductaion-finishBtn', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    var filesComprehensive = [];

                    filesComprehensive.push(
                            {
                                "key": "retStatementUploadText", "file_name": $('.ret-upload-statement .statement-info table tbody td .filename').text()
                                , "file_path": $('.statement-info table tbody td .filePath').val()
                            }

                    );



                    if ($(this).hasClass('edit-form')) {
                        educationalList[$(this).attr('data-index')] = {
                            educationalAccountName: $('#eductaionAccountModal .educational-account-first-name').val(),
                            educationAccount: $('#eductaionAccountModal .whose-education-account').val(),
                            educationAccountholder: $('#eductaionAccountModal .education-accountholder-input').val(),
                            earmarked: $('#eductaionAccountModal .earmarked-to-pay:checked').val(),
                            whoseEducation: $('#eductaionAccountModal .whose-education').val(),
                            startYear: $('#eductaionAccountModal .estimated-start-year').val(),
                            funds: $('#eductaionAccountModal .funds').val(),
                            fundsPurpose: $('#eductaionAccountModal .purpose-of-funds').val(),
                            retStatementUploadText: $('.ret-upload-statement .statement-info .filename').text(),
                            retStatementUploadPath: $('.ret-upload-statement .statement-info .filePath').val(),
                            filesComprehensive: filesComprehensive
                        };
                        $('#educationAccountInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#eductaionAccountModal .educational-account-first-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#educationAccountInfo tbody').append('<tr data-index=' + educationalList.length + '><td>' + $('#eductaionAccountModal .educational-account-first-name').val() + '</td>  <td valign="top"></td><td valign="top"  align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        educationalList.push({
                            educationalAccountName: $('#eductaionAccountModal .educational-account-first-name').val(),
                            educationAccount: $('#eductaionAccountModal .whose-education-account').val(),
                            educationAccountholder: $('#eductaionAccountModal .education-accountholder-input').val(),
                            earmarked: $('#eductaionAccountModal .earmarked-to-pay:checked').val(),
                            whoseEducation: $('#eductaionAccountModal .whose-education').val(),
                            startYear: $('#eductaionAccountModal .estimated-start-year').val(),
                            funds: $('#eductaionAccountModal .funds').val(),
                            fundsPurpose: $('#eductaionAccountModal .purpose-of-funds').val(),
                            retStatementUploadText: $('.ret-upload-statement .statement-info .filename').text(),
                            retStatementUploadPath: $('.ret-upload-statement .statement-info .filePath').val(),
                            filesComprehensive: filesComprehensive
                        });
                    }
                    $('#eductaionAccountModal').modal('hide');
                    $('#eductaionAccountModal .educational-account-first-name').val('');
                    $('#eductaionAccountModal .select').val('').trigger('change');
                    $('#educationalAccountDetails').html(JSON.stringify(educationalList));
                }
            }
            return false;
        });
        //------------------- js validation for form validation

        $(document).on('click', '#educationAccountInfo .fa-pencil', function () {
            $('#eductaionAccountModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            educationalDetails = educationalList[index];
            $('#eductaionAccountModal .educational-account-first-name').val(educationalDetails.educationalAccountName);
            $('#eductaionAccountModal .whose-education-account').val(educationalDetails.educationAccount).trigger('change');
            $('#eductaionAccountModal .education-accountholder-input').val(educationalDetails.educationAccountholder);
            $('#eductaionAccountModal .earmarked-to-pay[value=' + educationalDetails.earmarked + ']').prop('checked', true);
            $('#eductaionAccountModal .whose-education').val(educationalDetails.whoseEducation);
            $('#eductaionAccountModal .estimated-start-year').val(educationalDetails.startYear);
            $('#eductaionAccountModal .funds').val(educationalDetails.funds);
            $('#eductaionAccountModal .purpose-of-funds').val(educationalDetails.fundsPurpose);

            $('#eductaionAccountModal .ret-upload-statement .statement-info table tbody td .filename').text(educationalDetails.retStatementUploadText);

            $('.ret-upload-statement .upload-statment-noclick').removeClass('hide');
            $('.ret-upload-statement .ret-upload-statement-click').addClass('hide');
            if (educationalDetails.retStatementUploadText != "") {
                $('.upload-statement .statement-info table tbody').html('<tr style="border-bottom:0"><td><span class="filename">' + educationalDetails.retStatementUploadText + '</span><input type="hidden" name="file_path" class="filePath" value="' + educationalDetails.retStatementUploadText + '"></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash"></td></tr>');
                $('.upload-statement .statement-info').removeClass('hide');
                $('.upload-statement .upload-statment-noclick').css('cursor: not-allowed');
                $('.upload-statement .upload-statement-click').css('cursor: not-allowed');
            }


            $('#eductaionAccountForm').addClass('edit-form').attr('data-index', index);
            $('#eductaionAccountModal .error-alert').remove();

            if ($('.earmarked-to-pay:checked').val() == 'yes') {
                $('.education-hide-section').show();
                $('.funds-detail-section').hide();
            } else {
                $('.education-hide-section').hide();
                $('.funds-detail-section').show();
            }
        });

        $(document).on('click', '.add-education-account', function () {
            $('#eductaionAccountForm').removeClass('edit-form');
            $('#eductaionAccountModal input[type="text"],input[type="number"]').val('');
            $('#eductaionAccountModal .whose-education-account').val('').trigger('change');
            $('#eductaionAccountModal input[type="radio"]').prop('checked', false);
            $('#eductaionAccountModal .earmarked-to-pay').prop('checked', false);
            $('#eductaionAccountModal .error-alert').remove();
            $('#eductaionAccountModal .education-hide-section, #eductaionAccountModal .education-accountholder-name, #eductaionAccountModal .funds-detail-section').hide();

            $('.ret-upload-statement .ret-upload-statement-click').removeClass('hide');
            $('.ret-upload-statement .statement-info, .ret-upload-statement .upload-statment-noclick').addClass('hide');

            $('.ret-upload-statement .statement-info table tbody').find('tr').remove();
        });

        $(document).on('click', '#educationAccountInfo .fa-trash', function () {
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);


            $('.swal-button--danger').click(function () {

                $(this).closest('tr').remove();
                educationalList.splice(index_id, 1);
                $("#educationAccountInfo tbody").empty();
                if (educationalList.length != 0) {
                    var tr = '';
                    $.each(educationalList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.educationalAccountName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#educationAccountInfo tbody").html(tr);
                }
                $('#educationalAccountDetails').html(JSON.stringify(educationalList));
            });
        });


        $('.ret-upload-statement .statement-info table').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            $(".ret-upload-statement .ret-upload-statement-click").removeClass('hide');
            $(".ret-upload-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
            $('.ret-upload-statement .upload-statment span').text('');
        });

        $(document).on('change', '.education-button', function () {
            if ($(this).val() == 'yes') {
                $('.education-table').show();
            } else {
                $('.education-table').hide();
                educationalList = [];
                $('#educationalAccountDetails').html(JSON.stringify(educationalList));
            }
        });
        $(document).on('change', '.whose-education-account', function () {
            if ($(this).val() == '3') {
                $('.education-accountholder-name').show();
            } else {
                $('.education-accountholder-name').hide();
            }
        });
        $(document).on('change', '.earmarked-to-pay', function () {
            if ($(this).val() == 'yes') {
                $('.education-hide-section').show();
                $('.funds-detail-section').hide();
            } else {
                $('.education-hide-section').hide();
                $('.funds-detail-section').show();
            }
        });

    });


</script>
<style type="text/css">
    .modal-body .setup-content .OptionSelection{ width: calc(100% - 68.8%);}
    .modal-body .setup-content .OptionSelection:nth-child(4n){margin-right: 0}
    .modal-body .setup-content .OptionSelection .label{height: 40px; line-height: 40px; border: solid 1px #c4c4c4; color:#9b9b9b; font-size: 12px; border-radius: 0}
    .modal-body .setup-content .OptionSelection .married-selected{border:solid 1px #2079ee; color:#fff;}
</style>
@stop 