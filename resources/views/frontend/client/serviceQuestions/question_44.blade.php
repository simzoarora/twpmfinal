<style>
    .section-right .inner-left .fa-angle-down {
        position:absolute; 
        right:16%; 
        z-index: 1; 
        top: 42px; 
        font-size: 16px;
    }
    @media (max-width:768px){
    .section-right .inner-left .fa-angle-down {
        position:absolute; 
        right:12%; 
        z-index: 1; 
        top: 42px; 
        font-size: 16px;
    }
}
</style>
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Tell us about your goal.</h2>
        <p>So we can help you make a plan, tell us about your goal. if you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[44]','Tell us about your goal.') }}
        <div class="col-sm-6 inner-left validation-alert">
            {{ Form::label('label', 'What is your goal?') }}
            
            
            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
            <select style="position: absolute;" class="client-goal" required="" name="answer[44][your_goal]" style="display: none;"><option value="" disabled="" selected="selected">SELECT</option>
                <option value="0" <?php
                if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 0)) {
                    echo "selected";
                }
                ?>>education</option>
                <option value="1" <?php
                if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 1)) {
                    echo "selected";
                }
                ?>>large purchase</option> 
               <option value="2" <?php
                if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 2)) {
                    echo "selected";
                }
                ?>>new home</option>
                <option value="3" <?php
                if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 3)) {
                    echo "selected";
                }
                ?>>retirement</option>
                <option value="4" <?php
                if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 4)) {
                    echo "selected";
                }
                ?>>other</option></select>
            <!--{{ Form::select('data[44][answer][your goal]', [null=>'SELECT','educational','large purchase', 'new home','retirement','other'],null,['class'=>'client-goal'])}}-->
        </div>
        <div class="col-sm-6 inner-left other-goal" style="display:<?php
        if (!empty($answer) && array_key_exists('your_goal', $answer) && ($answer['your_goal'] == 4)) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>">
            {{ Form::label('label', 'If "other", tell us what it is') }}
            {{ Form::input('text','answer[44][tell_us_what_it_is]',(!empty($answer) && array_key_exists('tell_us_what_it_is',$answer)) ? $answer['tell_us_what_it_is'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'description']) }}
        </div>
        <div class="col-sm-6 inner-left label-width">
            {{ Form::label('label', 'Any additional information ') }} <span>OPTIONAL</span>
            {{ Form::textarea('answer[44][additional_information]',(!empty($answer) && array_key_exists('additional_information',$answer)) ? $answer['additional_information'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'More details (optional)']) }}

        </div>
        <div class="col-sm-8 inner-left custon-continue-button" style="text-align: right;padding-right: 30px;">
            <button type="submit">Continue</button>
        </div>
        <div class="col-sm-8 inner-left custon-return"  style="text-align: right;padding-right: 30px;">
            <a class="" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>
        </div>
    </div>
</div>