
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa { 
        font-size: 13px; 
        margin-left: 10px;
    } 
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }  
    .rawLandInfo{
        position: relative;
    }  
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',16) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Vehicles</h2>
                                    <p>Use this section to enter information about the value of your vehicles. This would include Automobiles, Motorcycles, Boats, Personal Watercraft, RV's, ATV's, Aircraft, etc.</p>
                                    <p>Exclude any vehicles that are leased.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[93]','Vehicles') }}
                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Do you own any vehicles that are not leased?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[93][own_any_vehicles_not_leased]', 'yes',(!empty($answer) && array_key_exists('own_any_vehicles_not_leased',$answer)) ? (($answer['own_any_vehicles_not_leased']=="yes")  ? true : false):false,['class'=>'non-leased-vehicle table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[93][own_any_vehicles_not_leased]', 'no',(!empty($answer) && array_key_exists('own_any_vehicles_not_leased',$answer)) ? (($answer['own_any_vehicles_not_leased']=="no")  ? true : false):false,['class'=>'non-leased-vehicle table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>
                                    </div> 
                                    <div class="col-sm-6 inner-left vehlicle-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('own_any_vehicles_not_leased', $answer) && ($answer['own_any_vehicles_not_leased'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">

                                        {{ Form::label('label', 'List all vehicles') }}

                                        <table id='rawLandInfo'>  
                                            <thead>                       
                                                <tr>     
                                                    <td style="font-size: 13px;">Vehicle</td>   
                                                    <td></td>   
                                                    <td></td>  
                                                </tr>
                                            </thead>   
                                            <tbody>
                                                <?php
                                                if (!empty($answer["allVehicleRecords"])) {
                                                    $vehicle = json_decode($answer["allVehicleRecords"]);
                                                    if (!empty($vehicle)) {
                                                        foreach ($vehicle as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->cpmaVehicleName}}</td>
                                                                <td></td>
                                                                <td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>   
                                        </table>
                                        <div class="col-sm-4 paddingLeft0"><button type='button' class="add-saving-account" style="width:100%; border-radius: 3px;" data-toggle="modal" data-target="#rawLandModal">Add item</button></div>
                                        <textarea id="allVehicleData" class="hidden" name="answer[93][allVehicleRecords]">{{(!empty($answer["allVehicleRecords"]))? $answer["allVehicleRecords"]:null}}</textarea>
                                    </div>

                                </div> 
                            </div>

                            <div class="sections">
                                <!-- modal starts -->
                                <div id="rawLandModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD VEHICLES</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <!-- steps form starts -->
                                                    <form role="form">
                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this vehicle a name') }}
                                                                    {{ Form::input('text','cpma_vehicle_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-vehicle-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Type of vehicle') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="cpma-vehicle-type" name="cpma_vehicle_type" required="required" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="1">automobile</option>
                                                                        <option value="2">motorcycle</option>
                                                                        <option value="3">watercraft</option>
                                                                        <option value="4">aircraft</option>
                                                                        <option value="5">RV</option>
                                                                        <option value="6">ATV</option>
                                                                        <option value="7">Other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Brief description of vehicle') }}
                                                                    {{ Form::textarea('cpma_vehicle_description', null,['data-validation'=> '' ,'rows'=>'3','cols'=>'4', 'class'=>'custom-validation form-control cpma-vehicle-description', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'make, model, year, special notes']) }}
                                                                </div>


                                                                <div class="col-sm-12">
                                                                    <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'When was this vehicle acquired?') }}
                                                                    {{ Form::input('text','answer[cpma_vehicle_acquired_date]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker cpma-vehicle-acquired-date', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Estimated value of the vehicle?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','answer[cpma_vehicle_estimatedvalue]',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-vehicle-estimatedvalue', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'How long from the date of purchase do you plan to keep this vehicle?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','answer[cpma_vehicle_purchase_date]',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-vehicle-purchase-date', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">Years</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn' type="button">  <i class="fa fa-arrow-left"></i>  back </button>
                                                                    <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i> </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- second step finsih -->

                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this vehicle used for business purposes?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('cpma_vehicle_business_purpose', 'yes',false,['class'=>'cpma-vehicle-business-purpose','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('cpma_vehicle_business_purpose', 'no',false,['class'=>'cpma-vehicle-business-purpose','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Approximately how many miles are driven per year (if an automobile)?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','answer[cpma_miles_driven_year]',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-miles-driven-year', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">miles</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Who is the primary user of this vehicle?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="cpma-vehicle-primary-user" name="cpma_vehicle_primary_user" required="" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value ='2'>{{$spouse_name}}</option><?php } ?>
                                                                        <option value="3">other</option> 
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left cpma-vehicle-primary-user-others" style="display: none;">
                                                                    {{ Form::label('label', 'Primary user&#39;s name?') }}
                                                                    {{ Form::input('text','answer[cpma_vehicle_primary_user_other_option]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-vehicle-primary-user-other-option', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'full name']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button type="button" class="backBtn"> <i class="fa fa-arrow-left"></i> back </button>
                                                                    <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i> </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- third step finsih -->

                                                        <!-- fourth step starts -->
                                                        <div class=" setup-content" id="step-4" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Who hold the title of this vehicle?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="cpma-vehicle-title" name="cpma_vehicle_title" required="">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value ='2'>{{$spouse_name}}</option><?php } ?>
                                                                        <option value="3">other</option> 
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left titleHolder">
                                                                    {{ Form::label('label', 'Titleholder&#39;s name?') }}
                                                                    {{ Form::input('text','answer[cpma_vehicle_title_holder]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-vehicle-title-holder', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'full name']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button type="button" class="backBtn"> <i class="fa fa-arrow-left"></i> back </button>
                                                                    <button id="rawLandModalnew" class='finishBtn pull-right' type="button">  finish <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- fourth step finsih -->

                                                        <!-- steps form finsih -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- modal finish -->
                            </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
$(document).ready(function () {


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    var cpmaDetails = [];
    if ($('#allVehicleData').val() != '') {
        cpmaDetails = JSON.parse($('#allVehicleData').val());
    }

    //new js starts -------------------
    $('.nextBtn, .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#rawLandModal .cpma-vehicle-name').val() && $('#rawLandModal .cpma-vehicle-type').val()) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        cpmaDetails[$(this).attr('data-index')] = {
                            cpmaVehicleName: $('#rawLandModal .cpma-vehicle-name').val(),
                            cpmaVehicleType: $('#rawLandModal .cpma-vehicle-type').val(),
                            cpmaVehicleDescription: $('#rawLandModal .cpma-vehicle-description').val(),

                            cpmaVehicleAcquiredDate: $('#rawLandModal .cpma-vehicle-acquired-date').val(),
                            cpmaVehicleEstimatedValue: $('#rawLandModal .cpma-vehicle-estimatedvalue').val(),
                            cpmaVehiclePurchaseDate: $('#rawLandModal .cpma-vehicle-purchase-date').val(),

                            cpmaVehicleBusinessPurpose: $('#rawLandModal .cpma-vehicle-business-purpose:checked').val(),
                            cpmaMilesDrivenYear: $('#rawLandModal .cpma-miles-driven-year').val(),
                            cpmaVehiclePrimaryUser: $('#rawLandModal .cpma-vehicle-primary-user').val(),
                            cpmaVehiclePrimaryUserOtherOption: $('#rawLandModal .cpma-vehicle-primary-user-other-option').val(),

                            cpmaVehicleTitle: $('#rawLandModal .cpma-vehicle-title').val(),
                            cpmaVehicleTitleHolder: $('#rawLandModal .cpma-vehicle-title-holder').val()
                        };
                        $('#rawLandInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#rawLandModal .cpma-vehicle-name').val() + '</td>  <td></td> <td align="right"> <i class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>');
                    } else {
                        $('#rawLandInfo tbody').append('<tr data-index=' + cpmaDetails.length + '><td>' + $('#rawLandModal .cpma-vehicle-name').val() + '</td>   <td> </td><td align="right"> <i class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td></tr>');
                        cpmaDetails.push({
                            cpmaVehicleName: $('#rawLandModal .cpma-vehicle-name').val(),
                            cpmaVehicleType: $('#rawLandModal .cpma-vehicle-type').val(),
                            cpmaVehicleDescription: $('#rawLandModal .cpma-vehicle-description').val(),

                            cpmaVehicleAcquiredDate: $('#rawLandModal .cpma-vehicle-acquired-date').val(),
                            cpmaVehicleEstimatedValue: $('#rawLandModal .cpma-vehicle-estimatedvalue').val(),
                            cpmaVehiclePurchaseDate: $('#rawLandModal .cpma-vehicle-purchase-date').val(),

                            cpmaVehicleBusinessPurpose: $('#rawLandModal .cpma-vehicle-business-purpose:checked').val(),
                            cpmaMilesDrivenYear: $('#rawLandModal .cpma-miles-driven-year').val(),
                            cpmaVehiclePrimaryUser: $('#rawLandModal .cpma-vehicle-primary-user').val(),
                            cpmaVehiclePrimaryUserOtherOption: $('#rawLandModal .cpma-vehicle-primary-user-other-option').val(),

                            cpmaVehicleTitle: $('#rawLandModal .cpma-vehicle-title').val(),
                            cpmaVehicleTitleHolder: $('#rawLandModal .cpma-vehicle-title-holder').val()
                        });
                    }
                    $('#rawLandModal').modal('hide');
                    $('#allVehicleData').html(JSON.stringify(cpmaDetails));
                }
            }
        }
        return false;
    });
    $(document).on('click', '#rawLandInfo .fa-pencil', function () {
        $('#step-1').show();
        $('#step-2, #step-2, #step-3, #step-4 ').hide();
        $('#rawLandModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        var cpmaDetailsList = cpmaDetails[index];
        $('#rawLandModal .cpma-vehicle-name').val(cpmaDetailsList.cpmaVehicleName);
        $('#rawLandModal .cpma-vehicle-type').val(cpmaDetailsList.cpmaVehicleType).trigger('change');
        $('#rawLandModal .cpma-vehicle-description').val(cpmaDetailsList.cpmaVehicleDescription);
        $('#rawLandModal .cpma-vehicle-acquired-date').val(cpmaDetailsList.cpmaVehicleAcquiredDate);
        $('#rawLandModal .cpma-vehicle-estimatedvalue').val(cpmaDetailsList.cpmaVehicleEstimatedValue);
        $('#rawLandModal .cpma-vehicle-purchase-date').val(cpmaDetailsList.cpmaVehiclePurchaseDate);
        $('#rawLandModal .cpma-vehicle-business-purpose[value=' + cpmaDetailsList.cpmaVehicleBusinessPurpose + ']').prop('checked', true);

        $('#rawLandModal .cpma-miles-driven-year').val(cpmaDetailsList.cpmaMilesDrivenYear);
        $('#rawLandModal .cpma-vehicle-primary-user').val(cpmaDetailsList.cpmaVehiclePrimaryUser).trigger('change');
        $('#rawLandModal .cpma-vehicle-primary-user-other-option').val(cpmaDetailsList.cpmaVehiclePrimaryUserOtherOption);
        $('#rawLandModal .cpma-vehicle-title').val(cpmaDetailsList.cpmaVehicleTitle).trigger('change');
        $('#rawLandModal .cpma-vehicle-title-holder').val(cpmaDetailsList.cpmaVehicleTitleHolder);
        $('#rawLandModalnew').addClass('edit-form');
        $('#rawLandModalnew').attr('data-index', index);
        $('#rawLandModal .error-alert').remove();

        if ($('.cpma-vehicle-primary-user').val() == '3') {
            $('.cpma-vehicle-primary-user-others').show();
        } else {
            $('.cpma-vehicle-primary-user-others').hide();
        }

    });

    $('.add-saving-account').on('click', function () {
        $('#rawLandModalnew').removeClass('edit-form');
        $('.saving-account').removeClass('selected-option');
        $('#rawLandModal input[type="text"],#rawLandModal input[type="number"]').val('');
        $('#rawLandModal .cpma-vehicle-business-purpose').prop('checked', false);
        $('#rawLandModal select').val('').trigger('change');
        $('#rawLandModal .cpma-vehicle-description').val('');
        $('#rawLandModal .error-alert').remove();
        $('#step-1').show();
        $('#step-2, #step-2, #step-3, #step-4, .cpma-vehicle-primary-user-others ').hide();
    });

    $(document).on('click', '.fa-trash', function () {
        cpmaDetails.splice($(this).closest('tr').attr('data-index'), 1);
        $(this).closest('tr').remove();
        $("#rawLandInfo tbody").empty();
        if (cpmaDetails.length != 0) {
            var tr = '';
            $.each(cpmaDetails, function (key, value) {
                tr += '<tr data-index="' + key + '"><td>' + value.cpmaVehicleName + '</td><td></td> <td align="right"> <i class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td></tr>';
            });
            $("#rawLandInfo tbody").html(tr);
        }
        $('#allVehicleData').html(JSON.stringify(cpmaDetails));
    });

    $(document).on('change', '.non-leased-vehicle', function () {
        if ($(this).val() == 'yes') {
            $('.vehlicle-table').show();
        } else {
            $('.vehlicle-table').hide();
            cpmaDetails = [];
            $('#allVehicleData').html(JSON.stringify(cpmaDetails));
        }
    });

    $(document).on('change', '.cpma-vehicle-primary-user', function () {
        if ($(this).val() == '3') {
            $('.cpma-vehicle-primary-user-others').show();
        } else {
            $('.cpma-vehicle-primary-user-others').hide();
        }
    });
    $('.datetimepicker ').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $('.cpma-vehicle-purchase-date').datetimepicker({
        format: 'YYYY'
    });
});
$(document).on('change', '.cpma-vehicle-title', function () {
    if ($(this).val() == 3) {
        $('.titleHolder').show();
    } else {
        $('.titleHolder').hide();

    }
});

</script>
@stop 