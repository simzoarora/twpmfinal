@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">

                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom:0;"> Investment statements</p>
                                    <h2>Upload your recent financial statements.</h2>
                                    <p style="margin-bottom:0;">Please gather and upload all of your most recent financial statements. This may include : </p>
                                    <ul style="font-size: 13px; padding-left: 25px;">
                                        <li>Investment Accounts</li>
                                        <li>IRA Accounts   </li>
                                        <li>401(k), SEP, SRA Accounts, etc. </li>
                                        <li>529 Accounts</li>
                                        <li>Deferred Compensation Accounts</li>
                                        <li>Annuities</li>
                                        <li>Employee Stock Options </li>
                                        <li> Any Other Investment statements</li>
                                    </ul>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                    <div class='add-file'>
                                        <table style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <td class="col-sm-4 paddingLeft0">Name</td>
                                                    <td class="col-sm-4 paddingLeft0">Document Type</td>
                                                    <td  class="col-sm-4 paddingLeft0" style="text-align:right; padding-right: 23px;">Actions</td>
                                                </tr>
                                            </thead>
                                            <tbody class="add-input">
                                                <tr  style="border:0"> 
                                                    <td colspan="3" class="paddingLeft0 paddingRight0 new">
                                                        
                                                    </td>
                                                </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}


                                    <label class="radio-custom-label"> 
                                        {{ Form::input('hidden','data[28][questionName]','Upload your executive compensation documents.') }}
                                        <span class="radio-icon"></span>
                                    </label>
                                    <label class="radio-custom-label"> 
                                        {{ Form::input('hidden','data[28][questionName]','Upload your policy documents.') }}
                                        <span class="radio-icon"></span>
                                    </label>

                                    <div class="col-sm-12 save-file-section paddingLeft0" style="margin-top:0">

                                        <div class="col-sm-6 add-file-btn">
                                            <label for="files" class="btn btn-primary open_btn" style="background:none;">Add File</label>
                                            <label for="files" class="btn btn-primary uploaderBtn no-action" style="background:none; display: none">Add File</label>
                                            <!--                                        <div class="col-sm-6 " style="text-align:right;">
                                                                                        <i class="fa fa-check" aria-hidden="true" id="saveFile"></i>
                                                                                    </div>-->
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> 

@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>  
<script>
    $(document).ready(function () {
        $("select").selectBoxIt();

        $(".open_btn").on('click', function () {
            $.FileDialog({multiple: true}).on('files.bs.filedialog', function (ev) {
                var files = ev.files;
                var text = "";
                files.forEach(function (f) {
                    $('.add-file table tr td.new').append('<div class="fileUploadRow" style="border-bottom:0"><div class="col-sm-4 paddingLeft0"> <input readonly="readonly" disable=""  type="text" value=' + f.name + ' > </div>  <div class="col-sm-4 paddingLeft0"> <i class="fa fa-angle-down" aria-hidden="true" style="position: absolute; right: 41%; z-index: 11; top:4px; opacity:.5;  font-size: 16px !important; text-transform: capitalize;"></i> <select style="position: absolute;" style="border:0; opacity:.5" class="status valid" name="data[annual file uploader][answer][file ]" required="" aria-invalid="false"> <option selected="" disabled="" value="">select</option> <option value="1">401(k)</option> <option value="2">SEP</option> <option value="3">SRA Account</option> </select></div><div class="col-sm-4 "><span class="pull-right"> <i class="fa fa-check" aria-hidden="true" id="saveFile"></i> <i title="Edit" class="fa fa-pencil" style="display:none;" aria-hidden="true"></i> <i style="display:none;" class="fa fa-trash" aria-hidden="true"></i> </span></div></div>');
                });

            });
        });

        $(document).on('click', '.fa-trash', function () {
            $(this).closest('.fileUploadRow').remove();
            return false;
        });

        $(document).on('click', '.bfd-ok', function () {
            $('.fileUploadRow').addClass('fileNotSaved');
            if ($('.bfd-files').find('.bfd-info').length > 0) {
                $('.uploaderBtn').show();
                $('.open_btn').hide();
            } else {
                $('.uploaderBtn').hide();
                $('.open_btn').show();
            }
        });



        $(document).on('click', '.fa-check', function () {
            $(this).parent().children('.fa-pencil, .fa-trash').show();
            $(this).parent().children('.fa-check').hide();
            $(this).parent().parent().parent().find("input[type=text]").attr("readonly", "readonly");
            $(this).parent().parent().parent().find("input[type=text]").addClass("fileSaved");
            $('.uploaderBtn').hide();
            $('.open_btn').show();

            $(this).parent().parent().parent().find('select').prop('disabled', true);
            $(this).parent().parent().parent().find("select").addClass("fileSaved");
            $(this).parent().parent().parent().find("input[type=text],select").removeClass('edit');
            $(this).parent().parent().parent().find('.fa-angle-down').hide();
            $(this).parent().parent().parent('.fileUploadRow').css('border-bottom', '1px solid #bbb3b3');
        });

        $(document).on('click', '.fa-pencil', function () {
            $(this).parent().children('.fa-trash, .fa-pencil').hide();
            $(this).parent().children('.fa-check').show();
//            $(this).parent().parent().parent().find("input[type=text]").css("border", 'solid #a9a9a9 1px');
            $(this).parent().parent().parent().find("input[type=text], select").addClass('edit');
            $(this).parent().parent().parent().find("input[type=text]").removeAttr("readonly");
            $(this).parent().parent().parent().find('select').prop('disabled', false);
            $(this).parent().parent().parent().find('.fa-angle-down').show();

        });



    });
</script>


<script>

    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";

</script> 
@stop

