<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about this student loan.</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[15][questionName]','Details about this student loan') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'In whose name is this loan?') }}
            {{ Form::select('data[15][answer][name]', ['SELECT','my name', 'spouse/partner','child', 'grandchild'],null,['class'=>'loan-on-name']) }}
        </div>
        <div class="col-sm-8 inner-left their-name" style="display: none;">
            {{ Form::label('label', 'Their name') }}
            {{ Form::input('text','data[15][answer][other-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Give this loan a nickname') }}
            {{ Form::input('text','data[15][answer][nickname]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is this a Federal loan?') }}
            <label class="radio-custom-label">{{ Form::radio('data[15][answer][Is this a Federal loan]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">{{ Form::radio('data[15][answer][Is this a Federal loan]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is this a private lender loan, i.e. a bank?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[15][answer][Is this a private lender loan, i.e. a bank?]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">  {{ Form::radio('data[15][answer][Is this a private lender loan, i.e. a bank?]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'From whom do you receive this statement?') }}
            {{ Form::input('text','data[15][answer][loan-statement]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Navient,bank,Saille Mae,etc.']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For whose education is this loan?') }}
            {{ Form::select('data[15][answer][whose-education-loan]', ['SELECT','my education', 'spouse/partner name','name from above','child\'s name from above', 'grandchild\'s name from above'],null,['class'=>'loan-on-name']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the term of the loan?') }}
            {{ Form::input('text','data[15][answer][loan-term]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true ]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the interest rate?') }}
            {{ Form::input('text','data[15][answer][loan-interest]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the minimum monthly payment? Enter 0 if in deferment.') }}
            {{ Form::input('text','data[15][answer][monthly-payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is this a consolidation loan?') }}
            <label class="radio-custom-label">  
                {{ Form::radio('data[15][answer][Is this a consolidation loan?]', 'yes',false,['class'=>'consolidated-loan']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> 
                {{ Form::radio('data[15][answer][Is this a consolidation loan?]', 'no',false,['class'=>'consolidated-loan']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left consolidation-date" style="display: none;">
            {{ Form::label('label', 'When was it consolidated?') }}
            {{ Form::input('date','data[15][answer][monthly-payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is the borrower on an income-based repayment program?') }}
            <label class="radio-custom-label">  
                {{ Form::radio('data[15][answer][Is the borrower on an income-based repayment program?]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">  
                {{ Form::radio('data[15][answer][Is the borrower on an income-based repayment program?]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the occupation of the borrower?') }}
            {{ Form::input('text','data[15][answer][occupation]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'In what city and state does the borrower work?') }}
            {{ Form::input('text','data[15][answer][city/state]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>

    </div>
</div>