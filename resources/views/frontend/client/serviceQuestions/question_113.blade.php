<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">DEPOSITS</p>
        <h2>Account Transfer</h2>
        <p>Descriptive/explanatory statement.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Will you be transferring an account for management?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[113][answer][Transferring an Account]', 'yes',false,[ 'class' => 'radio-value depositAccountChecked']) }} Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[113][answer][Transferring an Account]', 'no',false,[ 'class' => 'radio-value depositAccountChecked']) }} No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-12 inner-left" id="uploadDepositFile" style="display: none;">
            <label for="label">Upload your most recent Statement.</label>
            <div>
                <label for="deposit-account-transfer" class="btn upload-statment upload-account-statment">Upload File</label>
                <input id="deposit-account-transfer" style="visibility:hidden; height: 0; padding: 0;" type="file" required="false" onclick="this.value=null;">
            </div> 
        </div> 
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.depositAccountChecked').on('change', function () {
            if ($(this).val() == 'yes') {
                $('#uploadDepositFile').show();
            } else {
                $('#uploadDepositFile').hide();
            }
        });
    });
</script>