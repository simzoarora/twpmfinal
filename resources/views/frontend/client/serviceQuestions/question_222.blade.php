<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about you</h6>
        <h2>Let's get the rest of your personal information.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[222]','Let\s get the rest of your personal information') }}
        <div class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', 'Date of birth.......!!') }} 
                {{ Form::input('text','answer[222][date_of_birth]',(!empty($defaultData) && array_key_exists('userData', $defaultData)) ? date('m/d/Y',strtotime($defaultData['userData']['dob'])):null,['data-validation'=> '' , 'class'=>'custom-validation form-control dobdatetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY','readonly']) }}
            </div> 
        </div>

        <div style="clear:left;" class="col-sm-6">
            <div class="inner-left" style="width:100% !important;">
                {{ Form::label('label', 'Gender') }}
                    <label class="radio-custom-label">
                        <input  class="user_gender table-confirmation" required="required" name="answer[222][user_gender]" type="radio" value="1" aria-required="true" <?php
                        if (!empty($defaultData) && array_key_exists('userData', $defaultData) && ($defaultData['userData']['sex'] == "1")) {
                            echo "checked";
                        }
                        ?>>Male
                        <span class="radio-icon"></span>
                    </label>
                   
                <label class="radio-custom-label">
                        <input  class="user_gender table-confirmation" required="required" name="answer[222][user_gender]" type="radio" value="2" aria-required="true" <?php
                        if (!empty($defaultData) && array_key_exists('userData', $defaultData) && ($defaultData['userData']['sex'] == "2")) {
                            echo "checked";
                        }
                        ?>>Female
                        <span class="radio-icon"></span>
                    </label>
            </div>
        </div>

        <div style="clear:left;" class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', 'Last four digits of your SSN') }} 
                {{ Form::input('number','answer[222][social_security_number]',(!empty($answer) && array_key_exists('social_security_number', $answer)) ? $answer['social_security_number']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'XXXX','maxlength'=>'4']) }}
            </div> 
        </div>

        <div style="clear:left;" class="col-sm-6 padding-left0 citizen">
            <div class="inner-left ">
                {{ Form::label('label', 'Citizenship status') }}
                <i class="fa fa-angle-down selectArrow" aria-hidden="true" style=""></i>
                <select style="position: absolute;" class="status valid" name="answer[222][citizen_status]" required="" aria-invalid="false">
                    <option selected="" disabled="" value="">Select</option>
                    <option value="1" <?php
                    if (array_key_exists('citizen_status', $answer) && ($answer['citizen_status'] == 1))
                    echo "selected";
                    ?>>U.S. Citizen/National</option>
                    <option value="2" <?php
                    if (array_key_exists('citizen_status', $answer) && ($answer['citizen_status'] == 2))
                    echo "selected";
                    ?>>non-U.S. Citizen</option>
                </select>
                
            </div>
        </div>


    </div> 
</div>

