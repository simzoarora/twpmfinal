@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',24) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6 style="margin-bottom: 0;">LIABILITIES</h6>
                                    </div>
                                    <h2>Student Loans</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[82]','Do you, or your children or grandchildren have any student loans?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you, or your children or grandchildren have any student loans?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[82][CPMSLConfirmation]', 'yes',(!empty($answer) && array_key_exists('CPMSLConfirmation',$answer)) ? (($answer['CPMSLConfirmation']=="yes")  ? true : false):false,['class'=>'CPMSLConfirmation table-confirmation' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[82][CPMSLConfirmation]', 'no',(!empty($answer) && array_key_exists('CPMSLConfirmation',$answer)) ? (($answer['CPMSLConfirmation']=="no")  ? true : false):false,['class'=>'CPMSLConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left CPMSLDetailTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('CPMSLConfirmation', $answer) && ($answer['CPMSLConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <label> List all student loans relevant to your finances.</label>

                                                        <table id="semiAnnuaSLInfo" class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Loan</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["studentLoanRecords"])) {
                                                                    $loans = json_decode($answer["studentLoanRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$data->CPMSLLoanName}}</td>
                                                                                <td style="text-align:right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="CPMSLBtn add-dependent" data-toggle="modal" data-target="#CPMSLModal">Add Loan</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="CPMSstudentLoanDetails" class="hidden" name="answer[82][studentLoanRecords]">{{(!empty($answer["studentLoanRecords"]))? $answer["studentLoanRecords"]:null}}</textarea>
                                    <!-- info table finsih -->

                                </div>

                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="CPMSLModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD STUDENT LOAN</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">

                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this loan a name') }}
                                                                    {{ Form::input('text','HELOC Property Name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLLoanName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="status valid CPMSLNameLoaner" id="CPMSstudentLoanOthers"  name="CPM loan name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <option value="2">{{$spouse_name}}</option>
                                                                        <option value="3">Child</option>
                                                                        <option value="4">Grandchild</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CPM-otherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','CPMSLLoanOtherName',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLLoanOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Enter Name']) }}
                                                                </div>    

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For whose education is this loan?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="status valid SemiAnnualEducationLoanOthersOptions" id="SemiAnnualEducationLoanOthers" name="education_loan" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="1">Same as above</option>
                                                                        <option value="2">Other</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left CPM-educationotherName" style="display: none">
                                                                    {{ Form::label('label', 'Their name?') }}
                                                                    {{ Form::input('text','education',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPM-educationotherName1', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Enter Name']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->


                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a Federal Loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-federal-loan', 'yes',false,['class'=>'CPMSLFederaLLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-federal-loan', 'no',false,['class'=>'CPMSLFederaLLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a private lender loan, i.e. a bank?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-private-lender-loan', 'yes',false,['class'=>'CPMSLPrivateLenderConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('SemiAnnual-private-lender-loan', 'no',false,['class'=>'CPMSLPrivateLenderConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'From whom do you receive the statement?') }}
                                                                    {{ Form::input('text','semi_annual_receive_statement',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLReceiveStatement', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Navient, Bank, Salie Mae, etc.']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this consolidation loan?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('CPM-consolidation-loan', 'yes',null,['class'=>'CPMSLConsolidationLoanConfirmation','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('CPM-consolidation-loan', 'no',null,['class'=>'CPMSLConsolidationLoanConfirmation','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left CPM-consolidation-Datetimepicker" style="display: none;">
                                                                    {{ Form::label('label', 'When was it consolidated?') }}
                                                                    {{ Form::input('text','CPM-consolidation-date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker CPMSLConsolidationDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- second step finsih -->

                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan?') }}
                                                                    {{ Form::input('number','CPMStudent',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLStudentLoanTerm ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','CPMStudentLoanInterestRate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLInterestRate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label>What is the minimum monthly payment? <i>Enter 0 if in deferment?</i></label>
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span> 
                                                                        {{ Form::input('number','CPMStudentMinimumMonthlyPayment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLMinimumMonthlyPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!-- third step finish --> 
                                                        <!-- fourth step starts -->
                                                        <div class=" setup-content" id="step-4" style="display: none">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is the borrower on an income-based repayment program?') }}
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'yes',null,['class'=>'CPMSLincomeBasedRepayment','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('income-based-repayment', 'no',null,['class'=>'CPMSLincomeBasedRepayment','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the occupation of the borrower?') }}
                                                                    {{ Form::input('text','borrower-occupation',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLBorrowerOccupation', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>   

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In what city and state does the borrower work?') }}
                                                                    {{ Form::input('text','borrower-work',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMSLBorrowerWork', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button id="CPMSLBtnnew" class='CPMSLBtn finishBtn pull-right' type="button">
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fourth step finish --> 
                                                    <!-- steps form finsih -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $('.nextBtn, .finishBtn').on('click', function () {

            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#CPMSLModal .CPMSLLoanName').val()) {
                        if ($('.finishBtn').hasClass('edit-form')) {
                            CPMSLDetails[$(this).attr('data-index')] = {
                                CPMSLLoanName: $('#CPMSLModal .CPMSLLoanName').val(),
                                CPMSLNameLoaner: $('#CPMSLModal .CPMSLNameLoaner').val(),
                                CPMSLLoanOtherName: $('#CPMSLModal .CPMSLLoanOtherName').val(),
                                SemiAnnualEducationLoanOthersOptions: $('#CPMSLModal .SemiAnnualEducationLoanOthersOptions').val(),
                                CPMEducationotherName: $('#CPMSLModal .CPM-educationotherName1').val(),

                                CPMSLFederaLLoanConfirmation: $('#CPMSLModal .CPMSLFederaLLoanConfirmation:checked').val(),
                                CPMSLPrivateLenderConfirmation: $('#CPMSLModal .CPMSLPrivateLenderConfirmation:checked').val(),
                                CPMSLReceiveStatement: $('#CPMSLModal .CPMSLReceiveStatement').val(),
                                CPMSLConsolidationLoanConfirmation: $('#CPMSLModal .CPMSLConsolidationLoanConfirmation:checked').val(),
                                CPMSLConsolidationDate: $('#CPMSLModal .CPMSLConsolidationDate').val(),

                                CPMSLStudentLoanTerm: $('#CPMSLModal .CPMSLStudentLoanTerm').val(),
                                CPMSLInterestRate: $('#CPMSLModal .CPMSLInterestRate').val(),
                                CPMSLMinimumMonthlyPayment: $('#CPMSLModal .CPMSLMinimumMonthlyPayment').val(),

                                CPMSLincomeBasedRepayment: $('#CPMSLModal .CPMSLincomeBasedRepayment:checked').val(),
                                CPMSLBorrowerOccupation: $('#CPMSLModal .CPMSLBorrowerOccupation').val(),
                                CPMSLBorrowerWork: $('#CPMSLModal .CPMSLBorrowerWork').val()
                            }
                            $('#semiAnnuaSLInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#CPMSLModal .CPMSLLoanName').val() + '</td>  <td style="text-align:right" ><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                        } else {
                            $('#semiAnnuaSLInfo tbody').append('<tr class="children-info" data-index=' + CPMSLDetails.length + '><td>' + $('#CPMSLModal .CPMSLLoanName').val() + '</td>   <td style="text-align:right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                            CPMSLDetails.push({
                                CPMSLLoanName: $('#CPMSLModal .CPMSLLoanName').val(),
                                CPMSLNameLoaner: $('#CPMSLModal .CPMSLNameLoaner').val(),
                                CPMSLLoanOtherName: $('#CPMSLModal .CPMSLLoanOtherName').val(),
                                SemiAnnualEducationLoanOthersOptions: $('#CPMSLModal .SemiAnnualEducationLoanOthersOptions').val(),
                                CPMEducationotherName: $('#CPMSLModal .CPM-educationotherName1').val(),

                                CPMSLFederaLLoanConfirmation: $('#CPMSLModal .CPMSLFederaLLoanConfirmation:checked').val(),
                                CPMSLPrivateLenderConfirmation: $('#CPMSLModal .CPMSLPrivateLenderConfirmation:checked').val(),
                                CPMSLReceiveStatement: $('#CPMSLModal .CPMSLReceiveStatement').val(),
                                CPMSLConsolidationLoanConfirmation: $('#CPMSLModal .CPMSLConsolidationLoanConfirmation:checked').val(),
                                CPMSLConsolidationDate: $('#CPMSLModal .CPMSLConsolidationDate').val(),

                                CPMSLStudentLoanTerm: $('#CPMSLModal .CPMSLStudentLoanTerm').val(),
                                CPMSLInterestRate: $('#CPMSLModal .CPMSLInterestRate').val(),
                                CPMSLMinimumMonthlyPayment: $('#CPMSLModal .CPMSLMinimumMonthlyPayment').val(),

                                CPMSLincomeBasedRepayment: $('#CPMSLModal .CPMSLincomeBasedRepayment:checked').val(),
                                CPMSLBorrowerOccupation: $('#CPMSLModal .CPMSLBorrowerOccupation').val(),
                                CPMSLBorrowerWork: $('#CPMSLModal .CPMSLBorrowerWork').val()
                            });
                        }
                        $('#CPMSLModal').modal('hide');
                        $('#CPMSLModal .CPMSLLoanName, #CPMSLModal .CPMHELOCCreditAmount').val('');
                        $('#CPMSLModal .CPMSLConfirmation').prop('checked', false);
                        $('#CPMSstudentLoanDetails').html(JSON.stringify(CPMSLDetails));
                    }
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        var CPMSLDetails = [];
        if ($('#CPMSstudentLoanDetails').val() != '') {
            CPMSLDetails = JSON.parse($('#CPMSstudentLoanDetails').val());
        }

        $(document).on('change', '.CPMSLConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.CPMSLDetailTable').show();
            } else {
                $('.CPMSLDetailTable').hide();
                CPMSLDetails = [];
                $('#CPMSstudentLoanDetails').html(JSON.stringify(CPMSLDetails));
                $('.CPMSLDetailTable tbody tr').remove();
            }
        });

        $("#CPMSstudentLoanOthers").on('change', function () {
            if ($(this).val() === '5' || $(this).val() === '4' || $(this).val() === '3') {
                $('.CPM-otherName').show();
            } else {
                $('.CPM-otherName').hide();
            }
        });

        $("#SemiAnnualEducationLoanOthers").on('change', function () {
            if ($(this).val() === '2') {
                $('.CPM-educationotherName').show();
            } else {
                $('.CPM-educationotherName').hide();
            }
        });

        $(document).on('change', '.CPMSLConsolidationLoanConfirmation', function () {
            if ($(this).val() == 'yes') {
                $('.CPM-consolidation-Datetimepicker').show();
//                $('.CPMSLConsolidationDate').val('');
            } else {
                $('.CPM-consolidation-Datetimepicker').hide();
            }
        });

        $(document).on('change', '.CPM-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.CPMexchange-point-cash', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        if ($('.CPMexchange-point-cash').val() == 'yes') {
            $('.exchangeforCasgYesOption ').show();
        } else {
            $('.exchangeforCasgYesOption').hide();
        }


        $(document).on('click', '#semiAnnuaSLInfo .fa-pencil', function () {
            $('#CPMSLModal').modal();
            $('#CPMSLModal #step-1').show();
            $('#CPMSLModal #step-2, #CPMSLModal #step-3, #CPMSLModal #step-4').hide();
            $('#CPMSLModal .inner-left input, #CPMSLModal .inner-left select').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            CPMSLListDetails = CPMSLDetails[index];
            $('#CPMSLModal .CPMSLLoanName').val(CPMSLListDetails.CPMSLLoanName);
            $('#CPMSLModal .CPMSLNameLoaner').val(CPMSLListDetails.CPMSLNameLoaner).trigger('change');
            $('#CPMSLModal .CPMSLLoanOtherName').val(CPMSLListDetails.CPMSLLoanOtherName);
            $('#CPMSLModal .SemiAnnualEducationLoanOthersOptions').val(CPMSLListDetails.SemiAnnualEducationLoanOthersOptions).trigger('change');
            $('#CPMSLModal .CPM-educationotherName1').val(CPMSLListDetails.CPMEducationotherName);

            $('#CPMSLModal .CPMSLFederaLLoanConfirmation[value=' + CPMSLListDetails.CPMSLFederaLLoanConfirmation + ']').prop('checked', true);
            $('#CPMSLModal .CPMSLPrivateLenderConfirmation[value=' + CPMSLListDetails.CPMSLPrivateLenderConfirmation + ']').prop('checked', true);
            $('#CPMSLModal .CPMSLConsolidationLoanConfirmation[value=' + CPMSLListDetails.CPMSLConsolidationLoanConfirmation + ']').prop('checked', true);
            $('#CPMSLModal .CPMSLReceiveStatement').val(CPMSLListDetails.CPMSLReceiveStatement);
            $('#CPMSLModal .CPMSLConsolidationDate').val(CPMSLListDetails.CPMSLConsolidationDate);

            $('#CPMSLModal .CPMSLStudentLoanTerm').val(CPMSLListDetails.CPMSLStudentLoanTerm);
            $('#CPMSLModal .CPMSLInterestRate').val(CPMSLListDetails.CPMSLInterestRate);
            $('#CPMSLModal .CPMSLMinimumMonthlyPayment').val(CPMSLListDetails.CPMSLMinimumMonthlyPayment);

            $('#CPMSLModal .CPMSLincomeBasedRepayment[value=' + CPMSLListDetails.CPMSLincomeBasedRepayment + ']').prop('checked', true);
            $('#CPMSLModal .CPMSLBorrowerOccupation').val(CPMSLListDetails.CPMSLBorrowerOccupation);
            $('#CPMSLModal .CPMSLBorrowerWork').val(CPMSLListDetails.CPMSLBorrowerWork);

            $('#CPMSLBtnnew').addClass('edit-form');
            $('#CPMSLBtnnew').attr('data-index', index);

            if ($('.CPMSLConsolidationLoanConfirmation:checked').val() == 'yes') {
                $('.CPM-consolidation-Datetimepicker').show();
//                $('.CPMSLConsolidationDate').val('');
            } else {
                $('.CPM-consolidation-Datetimepicker').hide();
            }


        });

        $(document).on('click', '.CPMSLBtn', function () {
            $('#CPMSLBtnnew').removeClass('edit-form');
            $('#CPMSLModal input[type="text"], input[type="number"]').val('');
            $('#CPMSLModal input[type="radio"]').prop('checked', false);
            $('#CPMSLModal .CPMSLNameLoaner, #CPMSLModal .SemiAnnualEducationLoanOthersOptions').val('').trigger('change');
            $('#CPMSLModal').find('.error-alert').remove();
            $('#CPMSLModal #step-1').show();
            $('#CPMSLModal #step-2, #CPMSLModal #step-3, #CPMSLModal #step-4').hide();
        });

        $(document).on('click', '#semiAnnuaSLInfo .fa-trash', function () {
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {

                CPMSLDetails.splice(index_id, 1);
                $("#semiAnnuaSLInfo tbody").empty();
                if (CPMSLDetails.length != 0) {
                    var tr = '';
                    $.each(CPMSLDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.CPMSLLoanName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#semiAnnuxaSLInfo tbody").html(tr);
                }
                $('#CPMSstudentLoanDetails').html(JSON.stringify(CPMSLDetails));
                return false;
            });
        });


    });

</script>
@stop