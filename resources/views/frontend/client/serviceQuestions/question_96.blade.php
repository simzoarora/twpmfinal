@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',10) }}
                    {{ Form::input('hidden','subTopicId',26) }}
                    {{ Form::input('hidden','redirectPageName','investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Let's talk about your investment experience.</h2>
                                    <p>
                                        We'll ask about your investment experience, then you can tell us more about {{$spouse_name}} later.
                                    </p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[96]','Let\'s talk about your investment experience') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','In what year did you begin investing?') }}
                                        {{ Form::input('number','answer[96][investment_begin]',(!empty($answer) && array_key_exists('investment_begin',$answer)) ? $answer['investment_begin']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control investment-date', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        <i class="fa fa-angle-down" style="position: absolute; top: 45px; z-index: 10; left: 250px;"></i>
                                        {{ Form::label('label','How do you usually pay for investing?') }}
                                        <select style="position: absolute;" style='width:40%;' class="payment-mode" name="answer[96][payment_mode]" style="display: none;" required>
                                            <option selected disabled value="">SELECT</option>
                                            <option value="1" <?php if (!empty($answer) && array_key_exists('payment_mode', $answer) && ($answer['payment_mode'] == 1)) echo "selected"; ?> >Fees</option>
                                            <option value="2" <?php if (!empty($answer) && array_key_exists('payment_mode', $answer) && ($answer['payment_mode'] == 2)) echo "selected"; ?> >Commissions</option>
                                            <option value="3" <?php if (!empty($answer) && array_key_exists('payment_mode', $answer) && ($answer['payment_mode'] == 3)) echo "selected"; ?> >Fees and Commissions</option>
                                            <option value="4" <?php if (!empty($answer) && array_key_exists('payment_mode', $answer) && ($answer['payment_mode'] == 4)) echo "selected"; ?> >Unsure</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[961]','Individual stocks') }}

                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you invest in individual bonds?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[961][individual_bonds]', 'yes',(!empty($answer)&& array_key_exists('individual_bonds',$answer)) ?(($answer['individual_bonds']=="yes")  ? true : false):false,['class'=>'individual-bonds','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[961][individual_bonds]', 'no',(!empty($answer)&& array_key_exists('individual_bonds',$answer)) ?(($answer['individual_bonds']=="no")  ? true : false):false,['class'=>'individual-bonds','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[962]','Individual stocks') }}

                                    <div class="col-sm-12 inner-left spinner">
                                        {{ Form::label('label','About how many trades do you make per year (or your advisor makes for you)?') }}
                                        {{ Form::input('number','answer[962][trades_per_year]',(!empty($answer) && array_key_exists('trades_per_year',$answer)) ? $answer['trades_per_year']:null,['data-validation'=> '' ,'id'=>'spin', 'style'=>'padding-right:35px !important', 'class'=>'custom-validation form-control trades-per-year', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'min'=> 0, 'max'=> 99999, 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','About what is the average dollar amount on trades?') }}
                                       <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                        {{ Form::input('number','answer[962][average_amount]',(!empty($answer) && array_key_exists('average_amount',$answer)) ? $answer['average_amount']:null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control average-amount', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual stocks</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[963]','Individual stocks') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','How many years have you been investing in stocks?') }}
                                        {{ Form::input('number','answer[963][investing_since]',(!empty($answer) && array_key_exists('investing_since',$answer)) ? $answer['investing_since']:null,['data-validation'=> '' , 'id'=>'spin_new', 'style'=>'padding-right:35px !important', 'class'=>'custom-validation form-control investing-since', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'min'=> '0', 'max'=> '99999',  'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','In what year did you begin investing?') }}
                                        {{ Form::input('text','answer[963][begin_investing]',(!empty($answer) && array_key_exists('begin_investing',$answer)) ? $answer['begin_investing']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control begin-investing', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required', 'placeholder'=>'YYYY',]) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,10,'investment-experience-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        $('.investment-date, .begin-investing').datetimepicker({
            format: 'YYYY'
        });

        $('.actions ul li a').click(function () {
            if ($('.individual-bonds:checked').val() == 'no') {
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
            }
        });

        $(document).on('change', '.individual-bonds', function () {
            if ($(this).val() == 'no') {
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });

        if ($('.individual-bonds:checked').val() == 'no') {
            ;
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }

        $("#spin, #spin_new").spinner();
    });
</script>
@stop