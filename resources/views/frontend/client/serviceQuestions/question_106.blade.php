<div class="col-sm-12">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> ACCOUNT REGISTRATION </p>
        <h4> Give this account a name. </h4>
        <p> Give this account a nickname to make it easier to reference in the future. You will enter legal title on the next screen. </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','data[106][questionName]','Give this account a name') }}

        <div class="col-sm-6 inner-left " >
            {{ Form::label('label','What should we call this account?') }}
            {{ Form::input('text','[question][106][answer][nickname]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'account nickname', 'required'=>'required']) }}
        </div>
      
    </div>
</div>