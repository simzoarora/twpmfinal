@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',10) }}
                    {{ Form::input('hidden','subTopicId',31) }}
                    {{ Form::input('hidden','redirectPageName','investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Annuities</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[101]','Annuities') }}

                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you invest using annuities') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[101][annuities]', 'yes',(!empty($answer)&& array_key_exists('annuities',$answer)) ?(($answer['annuities']=="yes")  ? true : false):false,['class'=>'annuities','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[101][annuities]', 'no',(!empty($answer)&& array_key_exists('annuities',$answer)) ?(($answer['annuities']=="no")  ? true : false):false,['class'=>'annuities','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left investing" style="display: <?php
                                         if (!empty($answer) && array_key_exists('annuities',$answer) &&  ($answer['annuities']
                                             == "yes")) {
                                             echo 'block';
                                         } else {
                                             echo 'none';
                                         }
                                         ?>">
                                        {{ Form::label('label','When did you begin investing in annuities?') }}
                                        {{ Form::input('number','answer[101][begin_investing]',(!empty($answer) && array_key_exists('begin_investing',$answer)) ? $answer['begin_investing']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control begin-investing', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,10,'investment-experience-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        if ($('.annuities:checked').val() == 'yes') {
                $('.investing').show();
                $('.investing input').show();
            } else {
                $('.investing').hide();
                $('.investing input').hide().val('');
            }
            
        $(document).on('change', '.annuities', function () {
            if ($(this).val() == 'yes') {
                $('.investing').show();
                $('.investing input').show();
            } else {
                $('.investing').hide();
                $('.investing input').hide().val('');
            }
        });
        
        $('.begin-investing').datetimepicker({
            format: 'YYYY'
        });
        
    });

</script>
@stop