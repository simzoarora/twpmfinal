<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Estate planning Wrapping up</h2>
        <p>It's never to early to consider your legacy. The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[59]','Estate planning Wrapping up') }}
        <div class="col-sm-6 inner-left execution-details1">
            {{ Form::label('label','When did you last review your estate planning documents?') }}
            {{ Form::input('text','answer[59][execution-date1]',(!empty($answer) && array_key_exists('execution-date1',$answer)) ? $answer['execution-date1'] :null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control estate-planning-review', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'mm/dd/yyyy']) }}
        </div>
        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
        <div class="spouse-section">
            <div class="col-sm-6 inner-left execution-details1" >
                {{ Form::label('label', 'When did your spouse/partner last review their estate planning documents?') }}
                {{ Form::input('text','answer[59][execution-details1]',(!empty($answer) && array_key_exists('execution-details1',$answer)) ? $answer['execution-details1'] :null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control spouse-estate-planning-review', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'mm/dd/yyyy']) }}
            </div>
        </div>
        <?php } ?>
    </div>
</div>

<!--!isset($answer['execution-date1']) ? '': $answer['execution-date1']-->