
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 14px;
        top: 14px;
    }
    .add-beneficiary, .other-add-beneficiary {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .group-life-insurance-info, .other-group-life-insurance-info{
        position: relative;
    }
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',6) }}
                    {{ Form::input('hidden','subTopicId',6) }}
                    {{ Form::input('hidden','redirectPageName','insurance-final-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[61]','Group Life Insurance') }}

                                    <div class="col-sm-6 inner-left ">

                                        {{ Form::label('label', 'Do you have group life insurance through your employer?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[61][life_insurance_employer]', 'yes',(!empty($answer) && array_key_exists('life_insurance_employer', $answer)) ? (($answer['life_insurance_employer']== "yes") ? true : false) : false,['class'=>'group-insurance' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[61][life_insurance_employer]', 'no',(!empty($answer) && array_key_exists('life_insurance_employer', $answer)) ? (($answer['life_insurance_employer']== "no") ? true : false) : false,['class'=>'group-insurance' ,'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>


                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    <div class="col-sm-6 inner-left" >
                                        {{ Form::label('label', 'Amount of insurance.') }}
                                        <div class="input-group service-input-group">

                                            <span class="input-group-addon">
                                                $
                                            </span> 
                                            {{ Form::input('number','answer[61][when_executed]',(!empty($answer) && array_key_exists('when_executed',$answer)) ? $answer['when_executed'] : null,['data-validation'=> '' , 'class'=>'borderLeft0 stock-width custom-validation form-control execution-date', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[611]','Group Life Insurance') }}
                                    <div class="col-sm-6 inner-left ">

                                        {{ Form::label('label', 'Is this the maximum coverage you can get?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[611][maximum_coverage]', 'yes',(!empty($answer) && array_key_exists('maximum_coverage', $answer)) ? (($answer['maximum_coverage']== "yes") ? true : false) : false,['class'=>'maximum-coverage' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[611][maximum_coverage]', 'no',(!empty($answer) && array_key_exists('maximum_coverage', $answer)) ? (($answer['maximum_coverage']== "no") ? true : false) : false,['class'=>'maximum-coverage' ,'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>

                                    </div>

                                    <div class="col-sm-6 inner-left maximum-available" style="display: <?php
                                    if (!empty($answer) && array_key_exists('maximum_coverage', $answer) && ($answer['maximum_coverage'] == 'no')) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        {{ Form::label('label', 'What is the maximum available to you?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span> 
                                            <input data-validation="" class="borderLeft0 stock-width custom-validation form-control" data-rule-regex="false" required="" placeholder=" " name="answer[61][maximum_available]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('maximum_available', $answer)) {
                                                echo $answer["maximum_available"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">

                                    <div class="col-sm-6 inner-left" >
                                        {{ Form::label('label', 'What is the annual premium?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span> 
                                            <input data-validation="" class="borderLeft0 stock-width custom-validation form-control" data-rule-regex="false" required="" placeholder=" " name="answer[61][annual_premium]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('annual_premium', $answer)) {
                                                echo $answer["annual_premium"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[612]','Group Life Insurance') }}
                                    <div class="col-sm-6 inner-left ">

                                        {{ Form::label('label', 'Is employer paid premium imputed as income? (if unsure ask HR)') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[612][paid_premium_imputed_income]', 'yes',(!empty($answer) && array_key_exists('paid_premium_imputed_income', $answer)) ? (($answer['paid_premium_imputed_income']== "yes") ? true : false) : false,['class'=>'' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[612][paid_premium_imputed_income]', 'no',(!empty($answer) && array_key_exists('paid_premium_imputed_income', $answer)) ? (($answer['paid_premium_imputed_income']== "no") ? true : false) : false,['class'=>'' ,'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>


                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Group Life Insurance</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right" style="width:440px! important;">
                                    {{ Form::input('hidden','questionName[613]','Group Life Insurance') }}

                                    <div class="col-sm-6 inner-left table-width">

                                        {{ Form::label('label', 'Beneficiaries') }}
                                        <p style="width:100%;"><i class="here-open">If a trust is named, click <a class="click-here-open" data-toggle='modal' data-target='#clickHereModal'data-count="0">here</a></i></p>

                                        <table id='group-life-insurance-info'> 
                                            <thead>
                                                <tr>
                                                    <td style="width:25%;">First name</td>
                                                    <td style="width:25%;">Last name</td>
                                                    <td style="width:25%;">Percentage</td>
                                                    <td style="width:25%;">Primary</td>
                                                    <td style="width:25%;"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer) && array_key_exists('beneficiaries', $answer)) {
                                                    $beneficiary = json_decode($answer["beneficiaries"]);
                                                    if (!empty($beneficiary)) {
                                                        foreach ($beneficiary as $key => $datas) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$datas->InsuranceName}}</td>
                                                                <td>{{$datas->lastName}}</td>
                                                                <td>{{$datas->percentage}} %</td>
                                                                <td>
                                                                    <?php if ($datas->primary == true) { ?>
                                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                                    <?php } ?>
                                                                </td>
                                                                <td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit " aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal">Add beneficiary</button>
                                        <!--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#insuranceBeneficiaryModal">Open Modal</button>-->
                                    </div>
                                    <div class="col-sm-6 inner-left table-width">
                                        <table id ='group-life-insurance-info0'>
                                            <thead>
                                                <tr>
                                                    <td style="width:25%;">Trust Name</td>
                                                    <td style="width:25%;">Trustee Name </td>
                                                    <td style="width:25%;"></td>
                                                    <td style="width:25%;"></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer) && array_key_exists('clickHereFormRecords', $answer)) {
                                                    $clickHere = json_decode($answer["clickHereFormRecords"]);
                                                    if (!empty($clickHere)) {
                                                        foreach ($clickHere as $key => $datasa) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$datasa->nameOfTrust}}</td>
                                                                <td>{{$datasa->trustee}}</td>
                                                                <td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit0 " aria-hidden="true"></i></td>
                                                                <td><i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash0 " aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <textarea id="benificiaryData" class="hidden" name="answer[613][beneficiaries]">{{(!empty($answer["beneficiaries"]))? $answer["beneficiaries"]:null}}</textarea>
                            <textarea id="clickHereFormData" class="hidden" name="answer[613][clickHereFormRecords]">{{(!empty($answer["clickHereFormRecords"]))? $answer["clickHereFormRecords"]:null}}</textarea>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                    <h2>Other Life Insurance Policies</h2>
                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                        <p  class="question-description">
                                            We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                        </p>
                                    <?php } ?>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[614]','Other Life Insurance Policies') }}

                                    <div class="col-sm-6 inner-left left-side">

                                        {{ Form::label('label', 'Do you have any personally owned life insurance policies?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[614][personally_owned_life_insurance]', 'yes',(!empty($answer) && array_key_exists('personally_owned_life_insurance', $answer)) ? (($answer['personally_owned_life_insurance']== "yes") ? true : false) : false,['class'=>'owned-life-insurance' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[614][personally_owned_life_insurance]', 'no',(!empty($answer) && array_key_exists('personally_owned_life_insurance', $answer)) ? (($answer['personally_owned_life_insurance']== "no") ? true : false) : false,['class'=>'owned-life-insurance' ,'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>

                                    </div>
                                    <div class="col-sm-6 inner-left medical-conditions right-side" style="display: <?php
                                    if (!empty($answer) && array_key_exists('personally_owned_life_insurance', $answer) && ($answer['personally_owned_life_insurance'] == 'no')) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'Do you have any medical conditions that could make it hard for you to get life insurance?') }}
                                        <label class="radio-custom-label" >
                                            {{ Form::radio('answer[614][medical_conditions]', 'yes',(!empty($answer) && array_key_exists('medical_conditions', $answer)) ? (($answer['medical_conditions']== "yes") ? true : false) : false,['class'=>'medical-conditions' ,'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[614][medical_conditions]', 'no',(!empty($answer) && array_key_exists('medical_conditions', $answer)) ? (($answer['medical_conditions']== "no") ? true : false) : false,['class'=>'medical-conditions' ,'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <?php
                    if (!empty($answer) && !empty($answer["copy"])) {
                        foreach ($answer["copy"] as $key => $data) {
                            ?>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                            <h2>Other Life Insurance Policies</h2>
                                            <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                <p  class="question-description">
                                                    We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right ">
                                            {{ Form::input('hidden','questionName[61]','Other Life Insurance Policies') }}

                                            <div class="col-sm-6 inner-left">

                                                {{ Form::label('label', 'Policy owner') }}
                                                <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="top:45px! important;right:100px! important;"></i>
                                                <select style="position: absolute;" class="owner-relationship" required="" name="answer[61][copy][<?php echo $key; ?>][relationship_<?php echo $key; ?>]" style="display: none;">
                                                    <option selected disabled value="">SELECT</option>
                                                    <option value="1"  <?php
                                                    if (array_key_exists('relationship_' . $key, $data) && ($data['relationship_' . $key] == 1))
                                                        echo "selected";
                                                    ?>>{{session::get('loggedInUserName')}}</option>
                                                    <option value="2"   <?php
                                                    if (array_key_exists('relationship_' . $key, $data) && ($data['relationship_' . $key] == 2))
                                                        echo "selected";
                                                    ?>><?php echo $spouse_name ;?></option>
                                                    <option value="3"   <?php
                                                    if (array_key_exists('relationship_' . $key, $data) && ($data['relationship_' . $key] == 3))
                                                        echo "selected";
                                                    ?>>Someone else</option>
                                                    <option value="4"   <?php
                                                    if (array_key_exists('relationship_' . $key, $data) && ($data['relationship_' . $key] == 4))
                                                        echo "selected";
                                                    ?>>Trust</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 inner-left">

                                                {{ Form::label('label', 'Owner\'s full name') }}
                                                <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="full name" name="answer[61][copy][<?php echo $key; ?>][Owners_fullname_<?php echo $key; ?>]" type="text" aria-required="true"  value="<?php
                                                if (array_key_exists('Owners_fullname_' . $key, $data)) {
                                                    echo $data["Owners_fullname_" . $key];
                                                }
                                                ?>">
                                            </div>
                                            <div class="col-sm-6 inner-left owner-relationship-box" style="display: <?php
                                            if (array_key_exists('relationship_' . $key, $data) && ($data['relationship_' . $key] == 4)) {
                                                echo "block";
                                            } else {
                                                echo "none";
                                            }
                                            ?>"> 
                                                <button type='button' class="add-beneficiary personal-insurance-beneficiary" data-count="{{$key}}" data-toggle="modal" data-target="#clickHereModal2">Trust Information</button>
                                                <textarea id="clickHereFormData2" class="hidden" name="answer[61][copy][<?php echo $key; ?>][clickHereFormRecordsOther]">{{(!empty($data["clickHereFormRecordsOther"]))? $data["clickHereFormRecordsOther"]:null}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>


                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                            <h2>Other Life Insurance Policies</h2>
                                            <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                <p  class="question-description">
                                                    We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left insurance-beneficiary-modal">

                                                {{ Form::label('label', 'Beneficiaries') }}
                                                <p style="width:100%;"><i class="here-open" >If a trust is named beneficiary, click <a class="click-here-open1" data-toggle='modal' data-target='#clickHereModal1' data-count="{{$key}}">here</a></i></p>

                                                <table id='group-life-insurance-info2' data-copy="{{$key}}">
                                                    <thead>
                                                        <tr>
                                                            <td>Beneficiary name</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
                                                        <?php
                                                        if (array_key_exists('IBRecords_' . $key, $data)) {
                                                            $details = json_decode($data["IBRecords_" . $key]);
                                                            if (!empty($details)) {
                                                                foreach ($details as $key1 => $datas) {
                                                                    ?>
                                                                    <tr data-index="{{$key1}}">
                                                                        <td>{{$datas->InsuranceNameC}}</td>
                                                                        <td align="right"><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i> </td>
                                                                        <td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                                <button type='button' class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal2" data-copy="<?php echo $key; ?>">Add beneficiary</button>
                                            </div>
                                            <div class="col-sm-6 inner-left insurance-beneficiary-modal">
                                                <table id ='group-life-insurance-info1'>
                                                    <thead>
                                                        <tr>
                                                            <td style="width:35%;">Trust Name</td>
                                                            <td style="width:35%;">Trustee Name </td>
                                                            <td style="width:15%;"></td>
                                                            <td style="width:15%;"></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">
                                                        <?php
                                                        if (array_key_exists('clickHereFormRecordsC_' . $key, $data)) {
                                                            $details1 = json_decode($data["clickHereFormRecordsC_" . $key]);
                                                            if (!empty($details1)) {
                                                                foreach ($details1 as $key11 => $datas) {
                                                                    ?>
                                                                    <tr data-index="{{$key11}}">
                                                                        <td style="width:25%;">{{$datas->nameOfTrustC}}</td>
                                                                        <td style="width:25%;">{{$datas->trusteeC}}</td>
                                                                        <td style="width:25%;"><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit1" data-count="{{$key}}" aria-hidden="true"></i></td>
                                                                        <td style="width:25%;"><i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash1 " aria-hidden="true"></i></td>
                                                                    </tr>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>

                                            <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[61][copy][<?php echo $key; ?>][IBRecords_<?php echo $key; ?>]"><?php
                                                if (array_key_exists('IBRecords_' . $key, $data)) {
                                                    echo $data["IBRecords_" . $key];
                                                }
                                                ?></textarea>
                                            <textarea id="clickHereFormData1" class="hidden" name="answer[61][copy][<?php echo $key; ?>][clickHereFormRecordsC_<?php echo $key; ?>]">
                                                <?php
                                                if (array_key_exists('clickHereFormRecordsC_' . $key, $data)) {
                                                    echo $data["clickHereFormRecordsC_" . $key];
                                                }
                                                ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>

                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{Session::get('loggedInUserName')}}LIFE INSURANCE</h6>
                                            <h2>Contingent Beneficiaries </h2>
                                            <?php
                                            if (Session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                                ?>
                                                <p>We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left ">

                                                {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[61][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'yes',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "yes") ? true : false,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[61][copy]['.$key.'][contingent_beneficiaries_'.$key.']', 'no',(array_key_exists('contingent_beneficiaries_'.$key,$data) && $data['contingent_beneficiaries_'.$key] == "no") ? true : false,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>

                                            </div>
                                            <div class="col-sm-6 inner-left contingent-beneficiaries-modal " style="display:<?php
                                            if (array_key_exists('contingent_beneficiaries_' . $key, $data) && ($data['contingent_beneficiaries_' . $key] == 'yes')) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>">
                                                {{ Form::label('label', 'Contingent Beneficiaries') }}
                                                <table id='contingent-beneficiary-info'>
                                                    <thead>
                                                        <tr>
                                                            <td>First name</td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody data-copy="{{$key}}">

                                                        <?php
                                                        if (array_key_exists('CBRecords_' . $key, $data)) {
                                                            $detailss = json_decode($data["CBRecords_" . $key]);
                                                            if (!empty($detailss)) {
                                                                foreach ($detailss as $key2 => $datass) {
                                                                    ?>
                                                                    <tr data-index='{{$key2}}'>
                                                                        <td>{{$datass->contingentInsuranceName}}</td>
                                                                        <td> <i title="Edit" class="fa fa-pencil cursor-pointer beneficiary-edit" aria-hidden="true"></i></td>
                                                                        <td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td>
                                                                    </tr>

                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                        ?>

                                                    </tbody>
                                                </table>
                                                <button type='button' class="contingent-beneficiary add-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal" data-copy="{{$key}}">Add beneficiary</button>
                                            </div>
                                            <textarea id="contingentBeneficiaryData" class="hidden" name="answer[61][copy][<?php echo $key; ?>][CBRecords_<?php echo $key; ?>]">
                                                <?php
                                                if (array_key_exists('CBRecords_' . $key, $data)) {
                                                    echo $data["CBRecords_" . $key];
                                                }
                                                ?>
                                            </textarea>
                                        </div>
                                    </div>
                                </section>

                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                            <h2>Other Life Insurance Policies</h2>
                                            <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                <p  class="question-description">
                                                    We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                </p>
                                            <?php } ?> 
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                            <div class="col-sm-6 inner-left number-of-trustees">
                                                {{ Form::label('label', 'What type of policy is it?') }}

                                                <div class="trustee termLife"  >
                                                    {{ Form::input('hidden','answer[61][copy]['.$key.'][trustee_option_'.$key.']',(!empty($answer) && array_key_exists('trustee_option_'.$key,$data) ? $data['trustee_option_'.$key]:null), ['id'=>'disability_type11','required'=>'required']) }}
                                                    <a class="options termLifeModal type-disability1 type-policy <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == 'termLifeModal')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-toggle="modal" data-target="#termLifeModal" data-value="termlife" data-copy="<?php echo $key; ?>" >
                                                        <div class="select-trustee" >
                                                            <p>Term Life</p>    
                                                        </div>
                                                    </a>
                                                    <a class="options wholeLifeModal type-disability1 type-policy <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == 'wholeLifeModal')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-toggle="modal" data-target="#wholeLifeModal" data-value="wholelife" data-copy="<?php echo $key; ?>">
                                                        <div class="select-trustee">
                                                            <p>Whole life</p>    
                                                        </div>
                                                    </a>
                                                    <a class="options universalLifeModal type-disability1 type-policy <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == 'universalLifeModal')) {
                                                        echo 'selected';
                                                    }
                                                    ?>" data-toggle="modal" data-target="#universalLifeModal" data-value="universal" data-copy="<?php echo $key; ?>">
                                                        <div class="select-trustee">
                                                            <p>Universal <br> (Flexible Premium)</p>    
                                                        </div>
                                                    </a>
                                                    <a class="options otherLifeModal type-disability1 type-policy <?php
                                                    if (!empty($answer) && array_key_exists('trustee_option_' . $key, $data) && ($data['trustee_option_' . $key] == 'otherLifeModal')) {
                                                        echo 'selected';
                                                    }
                                                    ?>"data-toggle="modal" data-target="#otherLifeModal" data-value="otherpolicy" data-copy="<?php echo $key; ?>">
                                                        <div class="select-trustee">
                                                            <p>Other</p>    
                                                        </div>
                                                    </a>
                                                    <textarea id="policyTypeData" class="hidden" name="answer[61][copy][<?php echo $key; ?>][policyTypeRecord_<?php echo $key; ?>]">
                                                        <?php
                                                        if (array_key_exists('policyTypeRecord_' . $key, $data)) {
                                                            echo $data["policyTypeRecord_" . $key];
                                                        }
                                                        ?>
                                                    </textarea>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12 ">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                                            <h2>Other Life Insurance Policies</h2>
                                            <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                <p  class="question-description">
                                                    We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right ">

                                            <div class="col-sm-6 inner-left left-side">

                                                {{ Form::label('label', 'Do you have any other personally-owned life insurance policies?') }}
                                                <label class="radio-custom-label">

                                                    <input class="other-owned-life-insurance" required="required" name="answer[61][copy][<?php echo $key; ?>][other_personally_owned_life_insurance_<?php echo $key; ?>]" type="radio" value="yes" aria-required="true" <?php
                                                    if (array_key_exists('other_personally_owned_life_insurance_' . $key, $data) && ($data['other_personally_owned_life_insurance_' . $key] == "yes")) {
                                                        echo "checked";
                                                    }
                                                    ?>>Yes


                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    <input class="other-owned-life-insurance" required="required" name="answer[61][copy][<?php echo $key; ?>][other_personally_owned_life_insurance_<?php echo $key; ?>]" type="radio" value="no" aria-required="true" <?php
                                                    if (array_key_exists('other_personally_owned_life_insurance_' . $key, $data) && ($data['other_personally_owned_life_insurance_' . $key] == "no")) {
                                                        echo "checked";
                                                    }
                                                    ?>>No
                                                    <span class="radio-icon"></span>
                                                </label>

                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <?php
                        }
                    }
                    ?>


                    <!--modal starts-->
                    <div id="modal-section" >
                        <div class="sections">
                            <!-- CLICK HERE MODAL-->
                            <div id="clickHereModal" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD TRUST</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    {{ Form::input('hidden','questionName[613]','add trust') }}
                                                    <div class="col-sm-12  validation-alert">

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Name of trust') }}
                                                            {{ Form::input('text','answer[613][name_of_trust]',(!empty($answer) && array_key_exists('name_of_trust',$answer)) ? $answer['name_of_trust'] :'',['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust  ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Trustee') }}
                                                            {{ Form::input('text','answer[613][trustee]',(!empty($answer) && array_key_exists('trustee',$answer)) ? $answer['trustee'] :'',['data-validation'=> '' , 'class'=>'custom-validation form-control trustee ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Successor trustee') }}
                                                            {{ Form::input('text','answer[613][successor_trustee]',(!empty($answer) && array_key_exists('successor_trustee',$answer)) ? $answer['successor_trustee'] :'',['data-validation'=> '' , 'class'=>'custom-validation form-control successor-trustee ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'successor trustee', 'required'=>'required']) }}
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Primary beneficiary') }}
                                                            {{ Form::input('text','answer[613][primary_beneficiary]',(!empty($answer) && array_key_exists('primary_beneficiary',$answer)) ? $answer['primary_beneficiary'] :'',['data-validation'=> '' , 'class'=>'custom-validation form-control primary-beneficiary', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'primary beneficiary', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <a class='append-beneficiary-input cursor-pointer'>+Beneficiary</a>
                                                        </div>
                                                        <div class="col-sm-12 append-input inner-left">
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='clickHereForm' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="clickHereModal1" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD TRUST</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    <div class="col-sm-12 validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Name of trust') }}
                                                            {{ Form::input('text','name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Trustee') }}
                                                            {{ Form::input('text','trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Successor trustee') }}
                                                            {{ Form::input('text','successor_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control successor-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'successor trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Primary beneficiary') }}
                                                            {{ Form::input('text','primary_beneficiary',null,['data-validation'=> '' , 'class'=>'custom-validation form-control primary-beneficiary', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'primary beneficiary', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <a class='append-beneficiary-input1 cursor-pointer'>+Beneficiary</a>
                                                        </div>
                                                        <div class="col-sm-12 append-input1 inner-left">
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='clickHereForm1' type="button" style="border-radius: 3px;
                                                                    color: #fff;    
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;                                  
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none; 
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--COPY TRUST INFORMATION MODAL START-->

                            <div id="clickHereModal2" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD POLICY OWNER: TRUST</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    <div class="col-sm-12 validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Name of trust') }}
                                                            {{ Form::input('text','name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Trustee') }}
                                                            {{ Form::input('text','trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Successor trustee') }}
                                                            {{ Form::input('text','successor_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control successor-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'successor trustee', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Primary beneficiary') }}
                                                            {{ Form::input('text','primary_beneficiary',null,['data-validation'=> '' , 'class'=>'custom-validation form-control primary-beneficiary', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'primary beneficiary', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <a class='append-beneficiary-input2 cursor-pointer'>+Beneficiary</a>
                                                        </div>
                                                        <div class="col-sm-12 append-input2 inner-left">
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='clickHereForm2' type="button" style="border-radius: 3px;
                                                                    color: #fff;    
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;                                  
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none; 
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--COPY TRUST INFORMATION MODAL END--> 

                            <!-- CLICK HERE MODAL END-->



                            <!-- GROUP LIFE Modal -->
                            <div id="insuranceBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD BENEFICIARY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    {{ Form::input('hidden','questionName[613]','ADD a Student') }}
                                                    <div class="col-sm-12  validation-alert">

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','First Name') }}
                                                            {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Last Name') }}
                                                            {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Date of birth') }}
                                                            {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Relationship') }}
                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                            <select style="position: absolute;" class="relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left other-relationship-input" style="display: none;" >
                                                            {{ Form::label('label','Other') }}
                                                            {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'describe', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <div class="primary-option">
                                                                <ul>
                                                                    <p class="primary-select">Primary ?</p>
                                                                    <li style="display:inline; float: left; width: 50%;">
                                                                        <label class="checkbox-custom-label">
                                                                            <input type="checkbox" class="primary-selected">
                                                                            <span class="checkbox-icon cursor-pointer"></span>
                                                                            <span>
                                                                            </span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Percentage (max 100%)') }}
                                                            <div class="input-group">
                                                                <div style="width:100%">
                                                                    {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0  custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3', 'style'=>'width:85%! important;, border-right:0px;']) }}
                                                                    <span class="input-group-addon" style="height: 51px; border-left: 0px;"> % </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='insuranceBeneficiariesForm' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--other beneficiries modal-->

                            <div id="insuranceBeneficiaryModal2" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD BENEFICIARY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                                    {{ Form::input('hidden','questionName[6]','ADD beneficiary') }}
                                                    <div class="col-sm-12  validation-alert">

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','First Name') }}
                                                            {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Last Name') }}
                                                            {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Date of birth') }}
                                                            {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Relationship') }}
                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                            <select style="position: absolute;" class="relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left other-relationship-input" style="display: none;" >
                                                            {{ Form::label('label','Other') }}
                                                            {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other-relationship-description', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'describe', 'required'=>'required']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <div class="primary-option">
                                                                <ul>
                                                                    <p class="primary-select">Primary ?</p>
                                                                    <li style="display:inline; float: left; width: 50%;">
                                                                        <label class="checkbox-custom-label">
                                                                            <input type="checkbox" class="primary-selected2">
                                                                            <span class="checkbox-icon cursor-pointer"></span>
                                                                            <span>
                                                                            </span>
                                                                        </label>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label','Percentage (max 100%)') }}
                                                            <div class="input-group">
                                                                <div style="width:100%">
                                                                    {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0  custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3', 'style'=>'width:85%! important;, border-right:0px;']) }}
                                                                    <span class="input-group-addon" style="height: 51px; border-left: 0px;"> % </span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='insuranceBeneficiariesForm2' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--contingent beneficiaries modal-->



                            <div id="contingentBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                                <div class="modal-dialog">

                                    <!-- Modal content-->
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD CONTINGENT BENEFICIARIES</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="setup-content">
                                                    <div class="section-right">
                                                        <div class="validation-alert">
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','First Name') }}
                                                                {{ Form::input('text','first_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control contingent-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Last Name') }}
                                                                {{ Form::input('text','last_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Date of birth') }}
                                                                {{ Form::input('text','dob',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Relationship') }}
                                                                <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                <select style="position: absolute;" class="contingent-relationship" name="relationship" required="" aria-invalid="false" ><option selected="" disabled="" value="">SELECT</option><option value="1">Spouse</option><option value="2">Child</option><option value="3">Other</option></select>
                                                            </div>
                                                            <div class="col-sm-12 inner-left contingent-other-relationship" style="display: none;">
                                                                {{ Form::label('label','Other') }}
                                                                {{ Form::input('text','other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other-relationship-description1', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label','Percentage (max 100%)') }}
                                                                <div class="input-group">
                                                                    {{ Form::input('number','percentage',null,['data-validation'=> '' , 'class'=>'borderRight0  custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>'0','max'=>'100','maxlength'=>'3', 'style'=>' border-right:0px;']) }}
                                                                    <span class="input-group-addon" style="height: 51px; border-left: 0px;"> % </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                <button id='contingentBeneficiaryForm' data-number=""  class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!--end contingent beneficiaries modal-->



                            <!--//---------TERM LIFE MODAL-----------//-->

                            <div id="termLifeModal" class="modal fade" role="dialog" >
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">TERM LIFE INSURANCE POLICY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                                    <div class="col-sm-12  validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Date purchased') }}
                                                            {{ Form::input('text','Date purchased',null,['data-validation'=> '' , 'class'=>'termlife1 custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Number of years in term') }}
                                                            {{ Form::input('number','Number of years',null,['data-validation'=> '' , 'class'=>'termlife2 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Annual premium') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Annual premium',null,['data-validation'=> '' , 'class'=>'borderLeft0  termlife3 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Amount of death benefit') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Amount of death benefit',null,['data-validation'=> '' , 'class'=>'borderLeft0  termlife4 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true  , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <button data-val="termLifeModal" class="policytypebutton modal-button" class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--{{ Form::close() }}-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//---------TERM LIFE MODAL END-----------//-->


                            <!--//---------WHOLE LIFE MODAL-----------//-->

                            <div id="wholeLifeModal" class="modal fade" role="dialog" >
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">WHOLE LIFE INSURANCE POLICY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                                    {{ Form::input('hidden','questionName[620]','WHOLE LIFE') }}
                                                    <div class="col-sm-12  validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Date purchased') }}
                                                            {{ Form::input('text','Date purchased',null,['data-validation'=> '' , 'class'=>'wholelife1 custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Annual premium') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Annual premium',null,['data-validation'=> '' , 'class'=>'borderLeft0  wholelife2 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Amount of death benefit') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Amount of death benefit',null,['data-validation'=> '' , 'class'=>'borderLeft0  wholelife3 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current cash value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current cash value',null,['data-validation'=> '' , 'class'=>'borderLeft0  wholelife4 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current loan value (if no loan, leave blank)') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current loan value',null,['data-validation'=> '' , 'class'=>'borderLeft0  wholelife5 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current surrender value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current surrender value',null,['data-validation'=> '' , 'class'=>'borderLeft0  wholelife6 borderLeft0 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            <button data-val="wholeLifeModal" class="policytypebutton modal-button" class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--{{ Form::close() }}-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//---------WHOLE LIFE MODAL END-----------//-->


                            <!--//---------UNIVERSAL MODAL-----------//-->

                            <div id="universalLifeModal" class="modal fade" role="dialog" >
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">UNIVERSAL LIFE INSURANCE POLICY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                                    {{ Form::input('hidden','questionName[621]','WHOLE LIFE') }}
                                                    <div class="col-sm-12  validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Date purchased') }}
                                                            {{ Form::input('text','Date purchased',null,['data-validation'=> '' , 'class'=>'universal1 custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Annual target premium') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Annual target premium',null,['data-validation'=> '' , 'class'=>'borderLeft0  universal2 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Amount of death benefit') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Amount of death benefit',null,['data-validation'=> '' , 'class'=>' borderLeft0  universal3 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current cash value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current cash value',null,['data-validation'=> '' , 'class'=>'borderLeft0  universal4 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current loan value(if no loan, leave blank)') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current loan value',null,['data-validation'=> '' , 'class'=>'borderLeft0  universal5 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current surrender value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current surrender value',null,['data-validation'=> '' , 'class'=>'borderLeft0  universal6 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Interest on cash value(if any)') }}
                                                            {{ Form::input('number','Current surrender value',null,['data-validation'=> '' , 'class'=>'universal7 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left ">
                                                            {{ Form::label('label', 'Are there investment choices offered?') }}
                                                            <label class="radio-custom-label">
                                                                {{ Form::radio('investment choices offered', 'yes',null,['class'=>'universal8 investment-choices' ,'required'=>'required']) }}Yes
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                            <label class="radio-custom-label">
                                                                {{ Form::radio('investment choices offered', 'no',null,['class'=>'universal8 investment-choices' ,'required'=>'required']) }}No 
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                        </div>

                                                        <div class="col-sm-12 inner-left upload-choice" style="display:none;">
                                                            {{ Form::label('label', 'Click below to upload current investment choices.') }}
                                                            {{ Form::input('file','Current surrender value',null,['data-validation'=> '' , 'class'=>'universal9 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                                                        </div>


                                                        <div class="col-sm-12 inner-left">
                                                            <button data-val="universalLifeModal" class="policytypebutton modal-button" class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--{{ Form::close() }}-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//---------UNIVERSAL MODAL END-----------//-->


                            <!--//---------OTHER MODAL-----------//-->

                            <div id="otherLifeModal" class="modal fade" role="dialog" >
                                <div class="modal-dialog">
                                    <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                        <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">OTHER LIFE INSURANCE POLICY</h4>
                                        </div>
                                        <div class="modal-body" style='border:none; float: left;'>
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                                    {{ Form::input('hidden','questionName[622]','OTHER LIFE ISURANCE POLICY') }}
                                                    <div class="col-sm-12  validation-alert">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'What is the name of insurance') }}
                                                            {{ Form::input('text','What is the name of insurance',null,['data-validation'=> '' , 'class'=>'other1 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Issued by what company?') }}
                                                            {{ Form::input('text','Issued by what company?',null,['data-validation'=> '' , 'class'=>'other2 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Annual premium or target premium') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Annual target premium',null,['data-validation'=> '' , 'class'=>'borderLeft0  other3 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Amount of death benefit') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Amount of death benefit',null,['data-validation'=> '' , 'class'=>'borderLeft0  other4 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current cash value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current cash value',null,['data-validation'=> '' , 'class'=>'borderLeft0  other5 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current loan value(if no loan, leave blank') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current loan value',null,['data-validation'=> '' , 'class'=>'borderLeft0  other6 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Current surrender value') }}
                                                            <div class="input-group">
                                                                <span class="input-group-addon"> $ </span>
                                                                {{ Form::input('number','Current surrender value',null,['data-validation'=> '' , 'class'=>'borderLeft0 other7 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Interest on cash value(if any)') }}
                                                            {{ Form::input('number','Current surrender value',null,['data-validation'=> '' , 'class'=>'other8 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left ">
                                                            {{ Form::label('label', 'Are there investment choices offered?') }}
                                                            <label class="radio-custom-label">
                                                                {{ Form::radio('investment choices offered', 'yes',false,['class'=>'other9 other-investment-choices' ,'required'=>'required']) }}Yes
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                            <label class="radio-custom-label">
                                                                {{ Form::radio('investment choices offered', 'no',false,['class'=>'other9 other-investment-choices' ,'required'=>'required']) }}No 
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                        </div>

                                                        <div class="col-sm-12 inner-left other-upload-choice" style="display:none;">
                                                            {{ Form::label('label', 'Click below to upload current investment choices.') }}
                                                            {{ Form::input('file','Current surrender value',null,['data-validation'=> '' , 'class'=>'other10 custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button data-val="otherLifeModal" class="modal-button policytypebutton" class="modal-button"type="button" style="border-radius: 3px;    ">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--{{ Form::close() }}-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--//---------UNIVERSAL MODAL END-----------//-->
                        </div>
                    </div>
                    {{form::close()}}
                </div>
            </div>
        </div>
    </div>
</div>

<script id="clickHereBeneficiary" type="text/html">
    <div class="primary-beneficiary-wrapper">
        <label for="label" >Beneficiary <span class="beneficiary-count">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" class="contingent-check" name="contingent[<%= count%>]">
                        <span class="checkbox-icon cursor-pointer"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control beneficiary-extended" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary[<%= count%>]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>
<script id="clickHereBeneficiary1" type="text/html">
    <div class="primary-beneficiary-wrapper1">
        <label for="label" >Beneficiary <span class="beneficiary-count1">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" class="contingent-check" name="contingent1[<%= count%>]">
                        <span class="checkbox-icon cursor-pointer"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[<%= count%>]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>
<script id="clickHereBeneficiary2" type="text/html">
    <div class="primary-beneficiary-wrapper2">
        <label for="label" >Beneficiary <span class="beneficiary-count2">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" class="contingent-check" name="contingent2[<%= count%>]">
                        <span class="checkbox-icon cursor-pointer"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control beneficiary-extended2" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary2[<%= count%>]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>

<!-- GROUP LIFE Modal END-->
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script id="step1" type="text/html"> 
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                <h2>Other Life Insurance Policies</h2>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p  class="question-description">
                        We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    </p>
                <?php } ?>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right ">
                {{ Form::input('hidden','questionName[61]','Other Life Insurance Policies') }}

                <div class="col-sm-6 inner-left">

                    {{ Form::label('label', 'Policy owner') }}
                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="top:45px! important;right:100px! important;"></i>
                    <select style="position: absolute;" class="owner-relationship" required="" name="answer[61][copy][<%= copynum %>][relationship_<%= copynum %>]" style="display: none;">
                        <option selected disabled value="">SELECT</option><option value="1">{{session::get('loggedInUserName')}}</option>
                        <option value="2"><?php echo $spouse_name ;?></option><option value="3">Someone else</option>
                        <option value="4">Trust</option></select>

                </div>

                <div class="col-sm-6 inner-left">

                    {{ Form::label('label', 'Owner\'s full name') }}
                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="full name" name="answer[61][copy][<%= copynum %>][Owners_fullname_<%= copynum %>]" type="text" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left owner-relationship-box" style="display:none;">
                    <button type='button' class="add-beneficiary personal-insurance-beneficiary" data-toggle="modal" data-target="#clickHereModal2" data-count="<%= copynum %>" >Trust Information</button>
                </div>
                <textarea id="clickHereFormData2" class="hidden" name="answer[61][copy][<%= copynum %>][clickHereFormRecordsOther]"></textarea>
            </div>
        </div>
    </section>



</script>



<script id="step2" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{Session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                <h2>Other Life Insurance Policies </h2>
                <?php if (Session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p>We'll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
                <?php } ?>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left insurance-beneficiary-modal">

                    {{ Form::label('label', 'Beneficiaries') }}
                    <p style="width:100%;"><i class="here-open" >If a trust is named beneficiary, click <a class="click-here-open1" data-toggle='modal' data-target='#clickHereModal1' data-count="<%= copynum %>">here.</a></i></p>

                    <table id='group-life-insurance-info2'>
                        <thead>
                            <tr>
                                <td>Beneficiary name</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type="button" class="add-beneficiary" data-toggle="modal" data-target="#insuranceBeneficiaryModal2" data-copy="<%= copynum %>" >Add beneficiary</button>
                </div>
                <div class="col-sm-6 inner-left insurance-beneficiary-modal">
                    <table id='group-life-insurance-info1'>
                        <thead>
                            <tr>
                                <td style="width:35%;">Trust name</td>
                                <td style="width:35%;">Trustee Name </td>
                                <td style="width:15%;"></td>
                                <td style="width:15%;"></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                </div>
                <textarea id="insuranceBeneficiaryData" class="hidden" name="answer[61][copy][<%= copynum %>][IBRecords_<%= copynum %>]"></textarea>
                <textarea id="clickHereFormData1" class="hidden" name="answer[61][copy][<%= copynum %>][clickHereFormRecordsC_<%= copynum %>]"></textarea>
            </div>
        </div>
    </section> 
</script>




<script id="step3" type="text/html">

    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                <h2>Other Life Insurance Policies</h2>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p  class="question-description">
                        We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    </p>
                <?php } ?> 
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left ">

                    {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries" required="required" name="answer[61][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="contingent-beneficiaries" required="required" name="answer[61][copy][<%= copynum %>][contingent_beneficiaries_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>

                </div>
                <div class="col-sm-6 inner-left contingent-beneficiaries-modal " style="display:none;">
                    {{ Form::label('label', 'Contingent Beneficiaries') }}
                    <table id='contingent-beneficiary-info'>
                        <thead>
                            <tr>
                                <td>First name</td>
                                <td></td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody data-copy="<%= copynum %>">
                        </tbody>
                    </table>
                    <button type='button' class="contingent-beneficiary add-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal"  data-copy="<%= copynum %>">Add beneficiary</button>
                </div>
                <textarea id="contingentBeneficiaryData" class="hidden" name="answer[61][copy][<%= copynum %>][CBRecords_<%= copynum %>]"></textarea>

            </div>
        </div>
    </section> 

</script>



<script id="step4" type="text/html">
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                <h2>Other Life Insurance Policies</h2>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p  class="question-description">
                        We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    </p>
                <?php } ?> 
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                <div class="col-sm-6 inner-left number-of-trustees">
                    {{ Form::label('label', 'What type of policy is it?') }}

                    <div class="trustee termLife"  >
                        <a class="options termLifeModal type-disability1 type-policy" data-toggle="modal" data-target="#termLifeModal" data-value="termlife" data-copy="<%= copynum %>" >
                            <div class="select-trustee" >
                                <p>Term Life</p>    
                            </div>
                        </a>
                        <a class="options wholeLifeModal type-disability1 type-policy" data-toggle="modal" data-target="#wholeLifeModal" data-value="wholelife" data-copy="<%= copynum %>">
                            <div class="select-trustee">
                                <p>Whole life</p>    
                            </div>
                        </a>
                        <a class="options universalLifeModal type-disability1 type-policy" data-toggle="modal" data-target="#universalLifeModal" data-value="universal" data-copy="<%= copynum %>">
                            <div class="select-trustee">
                                <p>Universal <br> (Flexible Premium)</p>    
                            </div>
                        </a>
                        <a class="options otherLifeModal type-disability1 type-policy"data-toggle="modal" data-target="#otherLifeModal" data-value="otherpolicy" data-copy="<%= copynum %>">
                            <div class="select-trustee">
                                <p>Other</p>    
                            </div>
                        </a>
                        <input type="hidden" name="answer[61][copy][<%= copynum %>][trustee_option_<%= copynum %>]" id="disability_type11" />
                        <textarea id="policyTypeData" class="hidden" name="answer[61][copy][<%= copynum %>][policyTypeRecord_<%= copynum %>]"></textarea>
                    </div>
                </div>


            </div>
        </div>
    </section>
</script>

<script id="step5" type="text/html"> 
    <section class="sections">
        <div class="col-sm-12 ">
            <div class="col-md-4 col-xs-11 section-left">
                <h6>{{session::get('loggedInUserName')}} LIFE INSURANCE</h6>
                <h2>Other Life Insurance Policies</h2>
                <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <p  class="question-description">
                        We'll  get all of your information first, then gather the same for <?php echo $spouse_name ;?>.
                    </p>
                <?php } ?>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right ">
                {{ Form::input('hidden','questionName[61]','Other Life Insurance Policies') }}

                <div class="col-sm-6 inner-left left-side">

                    {{ Form::label('label', 'Do you have any other personally-owned life insurance policies?') }}
                    <label class="radio-custom-label">
                        <input class="other-owned-life-insurance" required="required" name="answer[61][copy][<%= copynum %>][other_personally_owned_life_insurance_<%= copynum %>]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="other-owned-life-insurance" required="required" name="answer[61][copy][<%= copynum %>][other_personally_owned_life_insurance_<%= copynum %>]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>

                </div>
            </div>
        </div>
    </section>
</script>
<script>
   var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,6,'insurance-final-preview'])}}";

$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
    $("select").selectBoxIt();
    var tempArray = <?php echo json_encode($answer); ?>;
    if (!($.isEmptyObject(tempArray.copy))) {
        var copynumber = tempArray.copy.length - 1;
    } else {
        var copynumber = 0;
//            appendSteps(copynumber);
    }
    function appendSteps(copynumber)
    {
        var plan1 = $('#step1').html();
        var plan2 = $('#step2').html();
        var plan3 = $('#step3').html();
        var plan4 = $('#step4').html();
        var plan5 = $('#step5').html();
        html1 = _.template(plan1);
        html2 = _.template(plan2);
        html3 = _.template(plan3);
        html4 = _.template(plan4);
        html5 = _.template(plan5);
        $("form").steps("add", {
            content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
        $("form").steps("add", {
            content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber})
        });
    }
    function removeSteps() {
        var copyind = form.steps('getCurrentIndex');
        var totalSteps = $('#services-question-form fieldset').length;
        if (totalSteps > copyind) {
            var len = totalSteps - copyind;
            for (var i = 1; i <= len; i++) {
                var index = copyind + 1;
                $("form").steps("remove", index);
            }
        }
    }
    $(document).on('change', '.owned-life-insurance', function () {
        if ($('.owned-life-insurance:checked').val() == 'no') {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            removeSteps();
        } else {
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.owned-life-insurance:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                appendSteps(copynumber);
                $(form).steps('next');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }

        });
    });




    $(document).on('change', '.other-owned-life-insurance', function () {
        if ($('.other-owned-life-insurance:checked').val() == 'no') {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            removeSteps();
        } else {
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-owned-life-insurance:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                copynumber++;
                appendSteps(copynumber);
                $(form).steps('next');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }

        });
    });
    $('a[href="#finish"]').on('click', function () {
        if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-owned-life-insurance:checked').val() == 'no' || $('a[href="#finish"]').text() == 'next') {
            $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }
    });

    var studentList = [];
    if ($('#benificiaryData').val() != '') {
        studentList = JSON.parse($('#benificiaryData').val());
    }
    $('#insuranceBeneficiariesForm').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {

            $('#insuranceBeneficiaryModal .error-alert').remove();
            if ($('#insuranceBeneficiaryModal .insurance-first-name').val() && $('#insuranceBeneficiaryModal .last-name').val() && $('#insuranceBeneficiaryModal .percentage').val()) {

                if ($(this).hasClass('edit-form')) {
                    studentList[$(this).attr('data-index')] = {
                        InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                        lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                        dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                        relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                        primary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#insuranceBeneficiaryModal .percentage').val()
                    }
                    if ($('.primary-selected').is(':checked')) {

                        $('#group-life-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td>   <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + '% </td> <td> <i class="fa fa-check" aria-hidden="true"></i> </td> <td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit" aria-hidden="true"></i> </td>');
                    } else {
                        $('#group-life-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td>   <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + '% </td> <td> </td> <td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit" aria-hidden="true"></i> </td>');
                    }
                } else {
                    if ($('.primary-selected').is(':checked')) {

                        $('#group-life-insurance-info tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td><td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + '% </td> <td> <i class="fa fa-check" aria-hidden="true"></i> </td> <td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit " aria-hidden="true"></i> </td></tr>');
                    } else {
                        $('#group-life-insurance-info tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#insuranceBeneficiaryModal .insurance-first-name').val() + '</td><td>' + $('#insuranceBeneficiaryModal .last-name').val() + '</td> <td>' + $('#insuranceBeneficiaryModal .percentage ').val() + '% </td> <td></td> <td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit" aria-hidden="true"></i> </td></tr>');
                    }
                    studentList.push({
                        InsuranceName: $('#insuranceBeneficiaryModal .insurance-first-name').val(),
                        lastName: $('#insuranceBeneficiaryModal .last-name').val(),
                        dob: $('#insuranceBeneficiaryModal .date-of-birth').val(),
                        relationship: $('#insuranceBeneficiaryModal .relationship').val(),
                        primary: $('#insuranceBeneficiaryModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#insuranceBeneficiaryModal .percentage').val()
                    });
                }
                $('#insuranceBeneficiaryModal').modal('hide');
                $('#insuranceBeneficiaryModal .primary-selected:checked').prop('checked', false);
                $('#insuranceBeneficiaryModal .insurance-first-name,#insuranceBeneficiaryModal .last-name,#insuranceBeneficiaryModal .date-of-birth, #insuranceBeneficiaryModal .parent').val('');
            } else {

                //                alert('please fill values');
                $('#insuranceBeneficiaryModal input, #insuranceBeneficiaryModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }

                });
            }
            $('#benificiaryData').html(JSON.stringify(studentList));
        }
    });
//        $('#insuranceBeneficiaryModal input, #insuranceBeneficiaryModal select').on('keyup change', function () {
//            $(this).parent().find('.error-alert').remove();
//            if (!$(this).val()) {
//                $(this).parent().append('<label class="error-alert">This field is required.</label>');
//            }
//        });
    $(document).on('click', '#group-life-insurance-info .group-insurance-edit', function () {
        $('#insuranceBeneficiaryModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = studentList[index];
        $('#insuranceBeneficiaryModal .insurance-first-name').val(insuranceDetails.InsuranceName);
        $('#insuranceBeneficiaryModal .last-name').val(insuranceDetails.lastName);
        $('#insuranceBeneficiaryModal .date-of-birth').val(insuranceDetails.dob);
        $('#insuranceBeneficiaryModal .relationship').val(insuranceDetails.relationship).trigger('change');
        $('#insuranceBeneficiaryModal .percentage').val(insuranceDetails.percentage);
        if (insuranceDetails.primary) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', true);
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('checked', false);
        }
        if ($(this).closest('tr').find('.fa-check').length || $('#group-life-insurance-info .fa-check').length == 0) {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', false);
        } else {
            $('#insuranceBeneficiaryModal .primary-selected').prop('disabled', true);
        }
        $('#insuranceBeneficiariesForm').addClass('edit-form').attr('data-index', index);
        $('#insuranceBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '.add-beneficiary', function () {
        $('#insuranceBeneficiariesForm').removeClass('edit-form');
        $('#insuranceBeneficiaryModal input[type="text"]').val('');
        $('#insuranceBeneficiaryModal input[type="number"]').val('');
        $('#insuranceBeneficiaryModal select').val('').trigger('change');
        $('#insuranceBeneficiaryModal .primary-selected:checked').prop('checked', false);
        $('#insuranceBeneficiaryModal .error-alert').remove();
    });
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });

    $(document).on('click', '.add-beneficiary', function () {
        if ($('#group-life-insurance-info .fa-check').length > 0) {
            $('.primary-selected').prop('disabled', true);
        } else {
            $('.primary-selected').prop('disabled', false);
        }

    });

    $(document).on('change', '.owned-life-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.medical-conditions').hide();
            $('.medical-conditions input').prop('checked', false);
        } else {
            $('.medical-conditions, .medical-conditions input').show();
        }
    });

    $(document).on('change', '.maximum-coverage', function () {
        if ($(this).val() == 'no') {
            $('.maximum-available, .maximum-available input').show();
        } else {
            $('.maximum-available,.maximum-available input').hide().val('');
        }

    });
    $(document).on('change', '.relationship', function () {
        if ($(this).val() == '3') {
            $('.other-relationship-input, .other-relationship-input input').show();
        } else {
            $('.other-relationship-input, .other-relationship-input input').hide().val('');
        }
    });
    $(document).on('change', '.owner-relationship', function () {
        if ($(this).val() == '4') {
            $('.owner-relationship-box, .personal-insurance-beneficiary').show();
        } else {
            $('.owner-relationship-box, .personal-insurance-beneficiary').hide();
        }
    });

    $(document).on('change', '.contingent-beneficiaries', function () {
        if ($(this).val() == 'yes') {
            $('.contingent-beneficiaries-modal').show();
        } else {
            $('.contingent-beneficiaries-modal').hide();
        }

    });

    $(document).on('click', '#insuranceBeneficiariesForm2', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var insuranceList = [];
            $('#insuranceBeneficiaryModal2 .error-alert').remove();
            var copyVal = $(this).attr('data-number');
            var insuranceBeneficiaryDataVal = $('textarea[name="answer[61][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').val();
            if (insuranceBeneficiaryDataVal.length > 0) {
                insuranceList = JSON.parse(insuranceBeneficiaryDataVal);
            }
            if ($('#insuranceBeneficiaryModal2 .insurance-first-name').val() && $('#insuranceBeneficiaryModal2 .last-name').val() && $('#insuranceBeneficiaryModal2 .percentage').val()) {

                if ($(this).hasClass('edit-form')) {
                    insuranceList[$(this).attr('data-index')] = {
                        InsuranceNameC: $('#insuranceBeneficiaryModal2 .insurance-first-name').val(),
                        lastNameC: $('#insuranceBeneficiaryModal2 .last-name').val(),
                        dobC: $('#insuranceBeneficiaryModal2 .date-of-birth').val(),
                        relationshipC: $('#insuranceBeneficiaryModal2 .relationship').val(),
                        primaryC: $('#insuranceBeneficiaryModal2 .primary-selected2:checked').val(),
                        otherInputC: $('#insuranceBeneficiaryModal2 .other-relationship-description').val(),
                        percentageC: $('#insuranceBeneficiaryModal2 .percentage').val()
                    }
                    if ($('.primary-selected2').is(':checked')) {
                        $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal2 .insurance-first-name').val() + '</td><td> <i class="fa fa-check" aria-hidden="true"></i> </td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i></td><td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td>');
                        value = "1";
                    } else {
                        $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#insuranceBeneficiaryModal2 .insurance-first-name').val() + '</td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i></td><td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td>');
                        value = "0";
                    }



                } else {
                    if ($('.primary-selected2').is(':checked')) {
                        $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal2 .insurance-first-name').val() + '</td><td> <i class="fa fa-check" aria-hidden="true"></i> </td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i></td><td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td></tr>');
                        value = "1";
                    } else {
                        $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + insuranceList.length + '><td>' + $('#insuranceBeneficiaryModal2 .insurance-first-name').val() + '</td><td></td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i></td><td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td></tr>');
                        value = "0";
                    }
                    insuranceList.push({
                        InsuranceNameC: $('#insuranceBeneficiaryModal2 .insurance-first-name').val(),
                        lastNameC: $('#insuranceBeneficiaryModal2 .last-name').val(),
                        dobC: $('#insuranceBeneficiaryModal2 .date-of-birth').val(),
                        relationshipC: $('#insuranceBeneficiaryModal2 .relationship').val(),
                        primaryC: $('#insuranceBeneficiaryModal2 .primary-selected2:checked').val(),
                        otherInputC: $('#insuranceBeneficiaryModal2 .other-relationship-description').val(),
                        percentageC: $('#insuranceBeneficiaryModal2 .percentage').val()
                    });
                }
                $('.fa-check').hide();
                $('#insuranceBeneficiaryModal2').modal('hide');
                $('textarea[name="answer[61][copy][' + copyVal + '][IBRecords_' + copyVal + ']"]').html(JSON.stringify(insuranceList));
            } else {
                $('#insuranceBeneficiaryModal2 input, #insuranceBeneficiaryModal2 select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        }
        return false;

    });

    $('#insuranceBeneficiaryModal2 input, #insuranceBeneficiaryModal2 select').on('keyup change', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        if (!$(this).val()) {
            $(this).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
        }
    });

    $(document).on('click', '#group-life-insurance-info2 .group-insurance-edit2', function () {
        $('#insuranceBeneficiaryModal2').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#insuranceBeneficiariesForm2").attr("data-number", copyArr);
        insuranceList = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = insuranceList[index];
        $('#insuranceBeneficiaryModal2 .insurance-first-name').val(insuranceDetails.InsuranceNameC);
        $('#insuranceBeneficiaryModal2 .last-name').val(insuranceDetails.lastNameC);
        $('#insuranceBeneficiaryModal2 .date-of-birth').val(insuranceDetails.dobC);
        $('#insuranceBeneficiaryModal2 .relationship').val(insuranceDetails.relationshipC).trigger('change');
        $('#insuranceBeneficiaryModal2 .other-relationship-description').val(insuranceDetails.otherInputC);

        $('#insuranceBeneficiaryModal2 .percentage').val(insuranceDetails.percentageC);
        if (insuranceDetails.primaryC) {
            $('#insuranceBeneficiaryModal2 .primary-selected2').prop('checked', true);
        } else {
            $('#insuranceBeneficiaryModal2 .primary-selected2').prop('checked', false);
        }
        if ($(this).closest('tr').find('.fa-check').length || $('#group-life-insurance-info2 tbody[data-copy="' + copyArr + '"]').find('.fa-check').length == 0) {
            $('#insuranceBeneficiaryModal2 .primary-selected2').prop('disabled', false);
        } else {
            $('#insuranceBeneficiaryModal2 .primary-selected2').prop('disabled', true);
        }
        $('#insuranceBeneficiariesForm2').addClass('edit-form').attr('data-index', index);
        $('#insuranceBeneficiaryModal2 .error-alert').remove();
        $('.fa-check').hide();
    });

    $(document).on('click', '.add-beneficiary', function () {
        var copyArr = $(this).attr('data-copy');
        $('#insuranceBeneficiariesForm2').removeClass('edit-form');
        $('#insuranceBeneficiaryModal2 input[type="text"], #insuranceBeneficiaryModal2 input[type="number"]').val('');
        $('#insuranceBeneficiaryModal2 select').val('').trigger('change');
        $('#insuranceBeneficiaryModal2 .primary-selected2:checked').prop('checked', false);
        $('#insuranceBeneficiaryModal2 .error-alert').remove();
        $("#insuranceBeneficiariesForm2").attr("data-number", $(this).attr('data-copy'));
        if ($('#group-life-insurance-info2 tbody[data-copy="' + copyArr + '"]').find('.fa-check').length > 0) {
            $('.primary-selected2').prop('disabled', true);
        } else {
            $('.primary-selected2').prop('disabled', false);
        }
    });

    $(document).on('click', '#group-life-insurance-info2 .fa-trash', function () {
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        insuranceDetails = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').val());
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            insuranceDetails.splice(index_id, 1);
            $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyArr + ']').empty();
            if (insuranceDetails.length != 0) {
                var tr = '';
                $.each(insuranceDetails, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.InsuranceNameC + '</td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit2" aria-hidden="true"></i></td><td><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
                });
                $('.insurance-beneficiary-modal #group-life-insurance-info2 tbody[data-copy=' + copyArr + ']').html(tr);
            }

            $('textarea[name="answer[61][copy][' + copyArr + '][IBRecords_' + copyArr + ']"]').html(JSON.stringify(insuranceDetails));
            return false;
        });

    });



    //    /----CONTINGENT BENEFICIARY MODAL-----/

    $(document).on('click','#contingentBeneficiaryForm', function () {
        var contingentList = [];
        var copyVal = $(this).attr('data-number');
        var contingentBeneficiaryDataVal = $.trim($('textarea[name="answer[61][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').val());
        if (contingentBeneficiaryDataVal.length > 0) {
            contingentList = JSON.parse(contingentBeneficiaryDataVal);
        }
        $('#contingentBeneficiaryModal .error-alert').remove();
        if ($('#contingentBeneficiaryModal .contingent-first-name').val() && $('#contingentBeneficiaryModal .last-name').val() && $('#contingentBeneficiaryModal .percentage').val()) {

            if ($(this).hasClass('edit-form')) {
                contingentList[$(this).attr('data-index')] = {
                    contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                    contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                    contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                    contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                    otherInput: $('#contingentBeneficiaryModal .other-relationship-description1').val(),
                    contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                }
                $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td><td></td> <td style="text-align:right; width:80px;"> <i title="Edit" class="fa fa-pencil cursor-pointer beneficiary-edit" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td>');
            } else {
                $('.contingent-beneficiaries-modal table tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + contingentList.length + '><td>' + $('#contingentBeneficiaryModal .contingent-first-name').val() + '</td><td> </td> <td style="text-align:right; width:80px;"><i title="Edit" class="fa fa-pencil cursor-pointer beneficiary-edit" aria-hidden="true"></i></td><td>  <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td></tr>');
                contingentList.push({
                    contingentInsuranceName: $('#contingentBeneficiaryModal .contingent-first-name').val(),
                    contingentLastName: $('#contingentBeneficiaryModal .last-name').val(),
                    contingentDateOfBirth: $('#contingentBeneficiaryModal .date-of-birth').val(),
                    contingentRelationship: $('#contingentBeneficiaryModal .contingent-relationship').val(),
                    otherInput: $('#contingentBeneficiaryModal .other-relationship-description1').val(),
                    contingentPercentage: $('#contingentBeneficiaryModal .percentage').val()
                });
            }

            $('#contingentBeneficiaryModal').modal('hide');
            $('textarea[name="answer[61][copy][' + copyVal + '][CBRecords_' + copyVal + ']"]').html(JSON.stringify(contingentList));
//                $('#contingentBeneficiaryModal .contingent-first-name,#contingentBeneficiaryModal .last-name, #contingentBeneficiaryModal .percentage, #contingentBeneficiaryModal .date-of-birth').val('');
        } else {

            $('#contingentBeneficiaryModal input, #contingentBeneficiaryModal select').each(function (i, elem) {
                if (!$(elem).val()) {
                    $(elem).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
                }
            });
        }

    });
    $(document).on('change', '.contingent-relationship', function () {
        if ($(this).val() == '3') {
            $('.contingent-other-relationship, .contingent-other-relationship .other-relationship-description1').show();
        } else {
            $('.contingent-other-relationship, .contingent-other-relationship .other-relationship-description1').hide();
        }
    });

    $('#contingentBeneficiaryModal input, #contingentBeneficiaryModal select').on('keyup change', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        if (!$(this).val()) {
            $(this).closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
        }
    });

    $(document).on('click', '.beneficiary-edit', function () {
        $('#contingentBeneficiaryModal').modal();
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        $("#contingentBeneficiaryForm").attr("data-number", copyArr);
        contingentList = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        contingentDetails = contingentList[index];
        $('#contingentBeneficiaryModal .contingent-first-name').val(contingentDetails.contingentInsuranceName);
        $('#contingentBeneficiaryModal .last-name').val(contingentDetails.contingentLastName);
        $('#contingentBeneficiaryModal .date-of-birth').val(contingentDetails.contingentDateOfBirth);
        $('#contingentBeneficiaryModal .contingent-relationship').val(contingentDetails.contingentRelationship).trigger('change');
        $('#contingentBeneficiaryModal .other-relationship-description1').val(contingentDetails.otherInput);
        $('#contingentBeneficiaryModal .percentage').val(contingentDetails.contingentPercentage);
        $('#contingentBeneficiaryForm').addClass('edit-form').attr('data-index', index);
        $('#contingentBeneficiaryModal .error-alert').remove();
    });
    $(document).on('click', '.contingent-beneficiary', function () {
        $("#contingentBeneficiaryForm").attr("data-number", $(this).attr('data-copy'));
        $('#contingentBeneficiaryForm').removeClass('edit-form');
        $('#contingentBeneficiaryModal input[type="text"], #contingentBeneficiaryModal input[type="number"]').val('');
        $('#contingentBeneficiaryModal select').val('').trigger('change');
        $('#contingentBeneficiaryModal .error-alert').remove();
    });

    $(document).on('click', '#contingent-beneficiary-info .fa-trash', function () {


        var copyArr = $(this).closest('table tbody').attr('data-copy');
        contingentDetails = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
//            contingentDetails = contingentList[index];
        $(this).closest('tr').remove();
        contingentDetails.splice($(this).closest('tr').attr('data-index'), 1);

        $('#contingent-beneficiary-info tbody[data-copy=' + copyArr + ']').empty();
        if (contingentDetails.length != 0) {
            var tr = '';
            $.each(contingentDetails, function (key, value) {
                tr += '<tr data-index="' + key + '"><td>' + value.contingentInsuranceName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
            });
            $('#contingent-beneficiary-info tbody[data-copy=' + copyArr + ']').html(tr);
        }

        $('textarea[name="answer[61][copy][' + copyArr + '][CBRecords_' + copyArr + ']"]').html(JSON.stringify(contingentDetails));
        return false;
    });
    $('.date-of-birth').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    $('.termlife1, .wholelife1, .universal1, .other1, .other2').datetimepicker({
        format: 'MM/DD/YYYY'
    });



//        Add Trust (without copy )
    $(document).on('click', '.append-beneficiary-input', function () {
        var number = $(this).parent().next().find('.primary-beneficiary-wrapper:nth-last-child(1)').find('.beneficiary-count').text();
        if (number == '') {
            number = 1;
        }
        var beneficiaryInput = $('#clickHereBeneficiary').html();
        html = _.template(beneficiaryInput);
        number++;
        var countNumberr = {
            count: number
        };
        $('.append-input').append(html((countNumberr)));
        $(this).parent().next().find('.primary-beneficiary-wrapper:nth-last-child(1)').find('.beneficiary-count').text(number);
    });
    //<-----click here form list code----->
    $(document).on('click', '.click-here-open', function () {

        $('#clickHereForm').removeClass('edit-form');

        $(".primary-beneficiary-wrapper").remove();
        $('#clickHereModal .name-of-trust').val(''),
                $('#clickHereModal .trustee').val(''),
                $('#clickHereModal .successor-trustee').val(''),
                $('#clickHereModal .primary-beneficiary').val('')

        $('#clickHereModal .error-alert').remove();

    });
    var trustList = [];
    if ($('#clickHereFormData').val() != '') {
        trustList = JSON.parse($('#clickHereFormData').val());
    }

    $(document).on('click', '#group-life-insurance-info0 .group-insurance-edit0', function () {
        $('#clickHereModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        trustDetails = trustList[index];
        $(".primary-beneficiary-wrapper").remove();
        $('#clickHereModal .name-of-trust').val(trustDetails.nameOfTrust),
                $('#clickHereModal .trustee').val(trustDetails.trustee),
                $('#clickHereModal .successor-trustee').val(trustDetails.sucessorTrustee),
                $('#clickHereModal .primary-beneficiary').val(trustDetails.primaryBeneficiary)
        var html = '';
        var count = 1;
        for (var i = 0; i < trustDetails.benficiary.length; i++) {
            count++;
            var checked = '';
            if (trustDetails.contingent[i]) {
                checked = 'checked';
            }
            html += '<div class="primary-beneficiary-wrapper"><label for="label" >Beneficiary <span class="beneficiary-count">' + count + '</span></label><div class="primary-option"><ul>\n\
                     <p class="primary-select"><i>contingent</i></p><li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label"><input type="checkbox" class="contingent-check" name="contingent[' + count + ']" ' + checked + '>\n\
<span class="checkbox-icon cursor-pointer"></span><span></span> </label></li></ul></div><input data-validation="" value="' + trustDetails.benficiary[i] + '" class="custom-validation form-control beneficiary-extended" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary[' + count + ']" type="text" aria-required="true" style="margin-top: 6px;"></div>';
        }
        $('.append-input').append(html);

        $('#clickHereForm').addClass('edit-form').attr('data-index', index);
        $('#clickHereModal .error-alert').remove();

    });
    var trustList = [];
    if ($('#clickHereFormData').val() != '') {
        trustList = JSON.parse($('#clickHereFormData').val());
    }
    $(document).on('click', '#clickHereForm', function () {
        var extendedBeneficiary = $("[name^='extended_beneficiary']")
                .map(function () {
                    return $(this).val();
                }).get();
        var contingentBeneficiary = $("[name^='contingent']")
                .map(function () {
                    return $(this).prop('checked');
                }).get();


        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {

            $('#clickHereModal .error-alert').remove();
            if ($('#clickHereModal .name-of-trust').val()) {

                if ($(this).hasClass('edit-form')) {
                    trustList[$(this).attr('data-index')] = {
                        nameOfTrust: $('#clickHereModal .name-of-trust').val(),
                        trustee: $('#clickHereModal .trustee').val(),
                        sucessorTrustee: $('#clickHereModal .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    }
                    $('#group-life-insurance-info0 tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#clickHereModal .name-of-trust').val() + '</td> <td>' + $('#clickHereModal .trustee').val() + '</td> <td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit0" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash0 " aria-hidden="true"></i> </td>');

                } else {
                    $('#group-life-insurance-info0 tbody').append('<tr data-index=' + trustList.length + '><td>' + $('#clickHereModal .name-of-trust').val() + '</td><td>' + $('#clickHereModal .trustee').val() + '</td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit0 " aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash0 " aria-hidden="true"></i> </td></tr>');
                    trustList.push({
                        nameOfTrust: $('#clickHereModal .name-of-trust').val(),
                        trustee: $('#clickHereModal .trustee').val(),
                        sucessorTrustee: $('#clickHereModal .successor-trustee').val(),
                        primaryBeneficiary: $('#clickHereModal .primary-beneficiary').val(),
                        contingent: contingentBeneficiary,
                        benficiary: extendedBeneficiary,
                    });

                }

            } else {
                $('#clickHereModal input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }

                });
            }
            $('#clickHereFormData').html(JSON.stringify(trustList));
            $('#clickHereModal').modal('hide');
        }



    });

    $(document).on('click', '#group-life-insurance-info0 .group-insurance-trash0 ', function () {
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            trustList.splice(index_id, 1);
            $("#group-life-insurance-info0 tbody").empty();
            if (trustList.length != 0) {
                var tr = '';
                $.each(trustList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.nameOfTrust + '</td><td>' + value.trustee + '</td><td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit0" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash0 " aria-hidden="true"></i> </td></tr>';
                });
                $("#group-life-insurance-info0 tbody").html(tr);
            }


            $('#clickHereFormData').html(JSON.stringify(trustList));
            return false;
        });
    });
//        end add trust

//copy add trust
    $(document).on('click', '.append-beneficiary-input1', function () {
        var number1 = $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text();
        if (number1 == '') {
            number1 = 1;
        }
        var beneficiaryInput1 = $('#clickHereBeneficiary1').html();
        html = _.template(beneficiaryInput1);
        number1++;
        var countNumberr = {
            count: number1
        };
        $('.append-input1').append(html((countNumberr)));
        $(this).parent().next().find('.primary-beneficiary-wrapper1:nth-last-child(1)').find('.beneficiary-count1').text(number1);
    });
    $(document).on('click', '.click-here-open1', function () {
        var count = $(this).attr('data-count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereForm1').removeClass('edit-form').removeAttr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(''),
                $('#clickHereModal1 .trustee').val(''),
                $('#clickHereModal1 .successor-trustee').val(''),
                $('#clickHereModal1 .primary-beneficiary').val('')

        $('#clickHereModal1 .error-alert').remove();

    });

    $(document).on('click', '#group-life-insurance-info1 .group-insurance-edit1', function () {

        var trustList1 = [];
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList1 = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').val());
        var index = $(this).closest('tr').attr('data-index');
        trustDetails1 = trustList1[index];

        var count = $(this).data('count');
        $("#clickHereForm1").attr("data-number", count);

        $('#clickHereModal1').modal();
        var index = $(this).closest('tr').attr('data-index');

        $(".primary-beneficiary-wrapper1").remove();
        $('#clickHereModal1 .name-of-trust').val(trustDetails1.nameOfTrustC),
                $('#clickHereModal1 .trustee').val(trustDetails1.trusteeC),
                $('#clickHereModal1 .successor-trustee').val(trustDetails1.sucessorTrusteeC),
                $('#clickHereModal1 .primary-beneficiary').val(trustDetails1.primaryBeneficiaryC)
        var html = '';
        var count = 1;
        for (var i = 0; i < trustDetails1.benficiaryC.length; i++) {
            count++;
            var checked = '';
            if (trustDetails1.contingentC[i]) {
                checked = 'checked';
            }
            html += '<div class="primary-beneficiary-wrapper1"><label for="label" >Beneficiary <span class="beneficiary-count1">' + count + '</span></label><div class="primary-option"><ul>\n\
                     <p class="primary-select"><i>contingent</i></p><li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label"><input type="checkbox" class="contingent-check" name="contingent1[' + count + ']" ' + checked + '>\n\
<span class="checkbox-icon cursor-pointer"></span><span></span> </label></li></ul></div><input data-validation="" value="' + trustDetails1.benficiaryC[i] + '" class="custom-validation form-control beneficiary-extended1" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary1[' + count + ']" type="text" aria-required="true" style="margin-top: 6px;"></div>';
        }
        $('.append-input1').append(html);
        $('#clickHereForm1').addClass('edit-form').attr('data-index', index);
        $('#clickHereModal1 .error-alert').remove();
    });



    $(document).on('click', '#clickHereForm1', function () {
        var count = $(this).attr('data-number');
        var extendedBeneficiary = $("[name^='extended_beneficiary1']")
                .map(function () {
                    return $(this).val();
                }).get();
        var contingentBeneficiary = $("[name^='contingent1']")
                .map(function () {
                    return $(this).prop('checked');
                }).get();
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {

            var trustListcopy = [];
            var trustDetails = $.trim($('textarea[name="answer[61][copy][' + count + '][clickHereFormRecordsC_' + count + ']"]').val());
            if (trustDetails.length > 0) {
                trustListcopy = JSON.parse(trustDetails);
            }



            $('#clickHereModal1 .error-alert').remove();
            if ($('#clickHereModal1 .name-of-trust').val()) {

                if ($(this).hasClass('edit-form')) {
                    trustListcopy[$(this).attr('data-index')] = {
                        nameOfTrustC: $('#clickHereModal1 .name-of-trust').val(),
                        trusteeC: $('#clickHereModal1 .trustee').val(),
                        sucessorTrusteeC: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiaryC: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingentC: contingentBeneficiary,
                        benficiaryC: extendedBeneficiary,
                    }
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#clickHereModal1 .name-of-trust').val() + '</td> <td>' + $('#clickHereModal1 .trustee').val() + '</td> <td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit1" data-count="' + count + '" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash1 " aria-hidden="true"></i> </td>');

                } else {
                    trustListcopy.push({
                        nameOfTrustC: $('#clickHereModal1 .name-of-trust').val(),
                        trusteeC: $('#clickHereModal1 .trustee').val(),
                        sucessorTrusteeC: $('#clickHereModal1 .successor-trustee').val(),
                        primaryBeneficiaryC: $('#clickHereModal1 .primary-beneficiary').val(),
                        contingentC: contingentBeneficiary,
                        benficiaryC: extendedBeneficiary,
                    });
                    var listlength = trustListcopy.length - 1;
                    $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + count + ']').append('<tr data-index=' + listlength + '><td>' + $('#clickHereModal1 .name-of-trust').val() + '</td><td>' + $('#clickHereModal1 .trustee').val() + '</td><td> <i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit1 " data-count="' + count + '" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash1 " aria-hidden="true"></i> </td></tr>');

                }

            } else {
                $('#clickHereModal1 input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }

                });
            }
            $('textarea[name="answer[61][copy][' + count + '][clickHereFormRecordsC_' + count + ']"]').html(JSON.stringify(trustListcopy));
            $('#clickHereModal1').modal('hide');
        }
    });

    $(document).on('click', '.insurance-beneficiary-modal #group-life-insurance-info1 .group-insurance-trash1 ', function () {
        var copyArr = $(this).closest('table tbody').attr('data-copy');
        trustList = JSON.parse($('textarea[name="answer[61][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').val());
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);

        $('.swal-button--danger').click(function () {
            trustList.splice(index_id, 1);
            $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').empty();
            if (trustList.length != 0) {
                var tr = '';
                $.each(trustList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.nameOfTrustC + '</td><td>' + value.trustee + '</td><td><i title="Edit" class="fa fa-pencil cursor-pointer group-insurance-edit1" aria-hidden="true"></i> </td><td> <i title="Delete" class="fa fa-trash cursor-pointer group-insurance-trash1 " aria-hidden="true"></i> </td></tr>';
                });
                $('.insurance-beneficiary-modal #group-life-insurance-info1 tbody[data-copy=' + copyArr + ']').html(tr);
            }


            $('textarea[name="answer[61][copy][' + copyArr + '][clickHereFormRecordsC_' + copyArr + ']"]').html(JSON.stringify(trustList));
            return false;
        });
    });
//end copy trust
//trust information copy modal
    $(document).on('click', '.append-beneficiary-input2', function () {
        var number2 = $(this).parent().next().find('.primary-beneficiary-wrapper2:nth-last-child(1)').find('.beneficiary-count2').text();
        if (number2 == '') {
            number2 = 1;
        }
        var beneficiaryInput2 = $('#clickHereBeneficiary2').html();
        html = _.template(beneficiaryInput2);
        number2++;
        var countNumber2 = {
            count: number2
        };
        $('.append-input2').append(html((countNumber2)));
        $(this).parent().next().find('.primary-beneficiary-wrapper2:nth-last-child(1)').find('.beneficiary-count2').text(number2);
    });
    $(document).on('click', '.personal-insurance-beneficiary', function () {
        var trustList2 = [];
        var count = $(this).data('count');
        $("#clickHereForm2").attr("data-number", count);
        var trustData = $('textarea[name="answer[61][copy][' + count + '][clickHereFormRecordsOther]"]').val();
        if (trustData.length > 0) {
            trustList2 = JSON.parse(trustData);
        }
        if (trustList2.length > 0) {
            var trustDetails2 = trustList2[0];
            $(".primary-beneficiary-wrapper2").remove();
            $('#clickHereModal2 .name-of-trust').val(trustDetails2.nameOfTrustO),
                    $('#clickHereModal2 .trustee').val(trustDetails2.trusteeO),
                    $('#clickHereModal2 .successor-trustee').val(trustDetails2.sucessorTrusteeO),
                    $('#clickHereModal2 .primary-beneficiary').val(trustDetails2.primaryBeneficiaryO)
            var html = '';
            var count = 1;
            for (var i = 0; i < trustDetails2.benficiaryO.length; i++) {
                count++;
                var checked = '';
                if (trustDetails2.contingentO[i]) {
                    checked = 'checked';
                }
                html += '<div class="primary-beneficiary-wrapper2"><label for="label" >Beneficiary <span class="beneficiary-count2">' + count + '</span></label><div class="primary-option"><ul>\n\
                     <p class="primary-select"><i>contingent</i></p><li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label"><input type="checkbox" class="contingent-check" name="contingent2[' + count + ']" ' + checked + '>\n\
                     <span class="checkbox-icon cursor-pointer"></span><span></span> </label></li></ul></div><input data-validation="" value="' + trustDetails2.benficiaryO[i] + '" class="custom-validation form-control beneficiary-extended2" data-rule-regex="false" required="required" placeholder="full name" name="extended_beneficiary2[' + count + ']" type="text" aria-required="true" style="margin-top: 6px;"></div>';
            }
            $('.append-input2').append(html);
        } else {
            $(".primary-beneficiary-wrapper2").remove();
            $('#clickHereModal2 .name-of-trust').val(''),
                    $('#clickHereModal2 .trustee').val(''),
                    $('#clickHereModal2 .successor-trustee').val(''),
                    $('#clickHereModal2 .primary-beneficiary').val('')
        }

    });
    $(document).on('click', '#clickHereForm2', function () {
        $('#clickHereModal2  .error-alert').remove();
        var count = $(this).attr('data-number');
        var extendedBeneficiary = $("[name^='extended_beneficiary2']")
                .map(function () {
                    return $(this).val();
                }).get();
        var contingentBeneficiary = $("[name^='contingent2']")
                .map(function () {
                    return $(this).prop('checked');
                }).get();
                
       var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {        
                
        var trustList2 = [];
        var trustListData2 = $('textarea[name="answer[61][copy][' + count + '][clickHereFormRecordsOther]"]').val();
        if (trustListData2.length > 0) {            
            trustList2 = JSON.parse(trustListData2);
      }
                
        $('#clickHereModal2  .error-alert').remove();
        if (trustList2.length == 0 || !($.inArray(count, trustList2))) {
            trustList2.push({
                nameOfTrustO: $('#clickHereModal2 .name-of-trust').val(),
                trusteeO: $('#clickHereModal2 .trustee').val(),
                sucessorTrusteeO: $('#clickHereModal2 .successor-trustee').val(),
                primaryBeneficiaryO: $('#clickHereModal2 .primary-beneficiary').val(),
                contingentO: contingentBeneficiary,
                benficiaryO: extendedBeneficiary,
            });
        } else {
            trustList2[count] = {
                nameOfTrustO: $('#clickHereModal2 .name-of-trust').val(),
                trusteeO: $('#clickHereModal2 .trustee').val(),
                sucessorTrusteeO: $('#clickHereModal2 .successor-trustee').val(),
                primaryBeneficiaryO: $('#clickHereModal2 .primary-beneficiary').val(),
                contingentO: contingentBeneficiary,
                benficiaryO: extendedBeneficiary,
            }
        }

        $('textarea[name="answer[61][copy][' + count + '][clickHereFormRecordsOther]"]').html(JSON.stringify(trustList2));
        $('#clickHereModal2').modal('hide');
    }
    });
//end trust information copy modal

    $(document).on('click', '.type-disability1', function () {
        var copyArr = $(this).attr('data-copy');
        $(".policytypebutton").attr("data-number", copyArr);
    });

//policy type modal code
    $(document).on('click', '.policytypebutton', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var policyList = [];
            $('#termLifeModal .error-alert').remove();
            $('#wholeLifeModal .error-alert').remove();
            $('#universalLifeModal .error-alert').remove();
            $('#otherLifeModal .error-alert').remove();


            var copyVal = $(this).attr('data-number');
            $('input[name="answer[61][copy][' + copyVal + '][trustee_option_' + copyVal + ']"]').val($(this).attr('data-val'));
            $('.' + $(this).attr('data-val')).addClass('selected').siblings().removeClass('selected');

            if ($(this).attr('data-val') == "termLifeModal") {
                policyList.push({
                    termlife1: $('#termLifeModal .termlife1').val(),
                    termlife2: $('#termLifeModal .termlife2').val(),
                    termlife3: $('#termLifeModal .termlife3').val(),
                    termlife4: $('#termLifeModal .termlife4').val()
                });
                $('#wholeLifeModal').find("input").val('');
                $('#universalLifeModal').find("input").val('');
                $('#otherLifeModal').find("input").val('');
            } else if ($(this).attr('data-val') == "wholeLifeModal") {
                policyList.push({
                    wholelife1: $('#wholeLifeModal .wholelife1').val(),
                    wholelife2: $('#wholeLifeModal .wholelife2').val(),
                    wholelife3: $('#wholeLifeModal .wholelife3').val(),
                    wholelife4: $('#wholeLifeModal .wholelife4').val(),
                    wholelife5: $('#wholeLifeModal .wholelife5').val(),
                    wholelife6: $('#wholeLifeModal .wholelife6').val()
                });
                $('#termLifeModal').find("input").val('');
                $('#universalLifeModal').find("input").val('');
                $('#otherLifeModal').find("input").val('');
            } else if ($(this).attr('data-val') == "universalLifeModal") {
                policyList.push({
                    universallife1: $('#universalLifeModal .universal1').val(),
                    universallife2: $('#universalLifeModal .universal2').val(),
                    universallife3: $('#universalLifeModal .universal3').val(),
                    universallife4: $('#universalLifeModal .universal4').val(),
                    universallife5: $('#universalLifeModal .universal5').val(),
                    universallife6: $('#universalLifeModal .universal6').val(),
                    universallife7: $('#universalLifeModal .universal7').val(),
                    universallife8: $('#universalLifeModal .universal8:checked').val(),
                    universallife9: $('#universalLifeModal .universal9').val()
                });
                $('#termLifeModal').find("input").val('');
                $('#wholeLifeModal').find("input").val('');
                $('#otherLifeModal').find("input").val('');
            } else if ($(this).attr('data-val') == "otherLifeModal") {
                policyList.push({
                    otherlife1: $('#otherLifeModal .other1').val(),
                    otherlife2: $('#otherLifeModal .other2').val(),
                    otherlife3: $('#otherLifeModal .other3').val(),
                    otherlife4: $('#otherLifeModal .other4').val(),
                    otherlife5: $('#otherLifeModal .other5').val(),
                    otherlife6: $('#otherLifeModal .other6').val(),
                    otherlife7: $('#otherLifeModal .other7').val(),
                    otherlife8: $('#otherLifeModal .other8').val(),
                    otherlife9: $('#otherLifeModal .other9:checked').val(),
                    otherlife10: $('#otherLifeModal .other10').val()

                });
                $('#termLifeModal').find("input").val('');
                $('#wholeLifeModal').find("input").val('');
                $('#universalLifeModal').find("input").val('');
            }

            $('#termLifeModal, #wholeLifeModal, #universalLifeModal, #otherLifeModal ').modal('hide');
            $('textarea[name="answer[61][copy][' + copyVal + '][policyTypeRecord_' + copyVal + ']"]').html(JSON.stringify(policyList));

        }
        return false;

    });

    $(document).on('click', '.type-disability1', function () {
        var copyVal = $(this).attr('data-copy');
        var listDetails = $('textarea[name="answer[61][copy][' + copyVal + '][policyTypeRecord_' + copyVal + ']"]').val();
        if (listDetails.length > 0) {
            var listData = JSON.parse(listDetails);
            if ($(this).attr('data-target') == "#termLifeModal" && listData.length > 0) {
                $('#termLifeModal .termlife1').val(listData[0].termlife1);
                $('#termLifeModal .termlife2').val(listData[0].termlife2);
                $('#termLifeModal .termlife3').val(listData[0].termlife3);
                $('#termLifeModal .termlife4').val(listData[0].termlife4);

            } else if ($(this).attr('data-target') == "#wholeLifeModal" && listData.length > 0) {

                $('#wholeLifeModal .wholelife1').val(listData[0].wholelife1);
                $('#wholeLifeModal .wholelife2').val(listData[0].wholelife2);
                $('#wholeLifeModal .wholelife3').val(listData[0].wholelife3);
                $('#wholeLifeModal .wholelife4').val(listData[0].wholelife4);
                $('#wholeLifeModal .wholelife5').val(listData[0].wholelife5);
                $('#wholeLifeModal .wholelife6').val(listData[0].wholelife6);

            } else if ($(this).attr('data-target') == "#universalLifeModal" && listData.length > 0) {

                $('#universalLifeModal .universal1').val(listData[0].universallife1);
                $('#universalLifeModal .universal2').val(listData[0].universallife2);
                $('#universalLifeModal .universal3').val(listData[0].universallife3);
                $('#universalLifeModal .universal4').val(listData[0].universallife4);
                $('#universalLifeModal .universal5').val(listData[0].universallife5);
                $('#universalLifeModal .universal6').val(listData[0].universallife6);
                $('#universalLifeModal .universal7').val(listData[0].universallife7);
                $('#universalLifeModal .universal8[value=' + listData[0].universallife8 + ']').prop('checked', true);
                $('#universalLifeModal .universal9').text(listData[0].universallife9);
                if ($('.universal8:checked').val() === 'yes') {
                    $('.upload-choice, .upload-choice input').show();
                } else {
                    $('.upload-choice, .upload-choice input').hide();
                }


            } else if ($(this).attr('data-target') == "#otherLifeModal" && listData.length > 0) {
                $('#otherLifeModal .other1').val(listData[0].otherlife1);
                $('#otherLifeModal .other2').val(listData[0].otherlife2);
                $('#otherLifeModal .other3').val(listData[0].otherlife3);
                $('#otherLifeModal .other4').val(listData[0].otherlife4);
                $('#otherLifeModal .other5').val(listData[0].otherlife5);
                $('#otherLifeModal .other6').val(listData[0].otherlife6);
                $('#otherLifeModal .other7').val(listData[0].otherlife7);
                $('#otherLifeModal .other8').val(listData[0].otherlife8);
                $('#otherLifeModal .other9[value=' + listData[0].otherlife9 + ']').prop('checked', true);
                $('#otherLifeModal .other10').text(listData[0].otherlife10);
                if ($('.other9:checked').val() === 'yes') {
                    $('.other-upload-choice, .other-upload-choice input').show();
                } else {
                    $('.other-upload-choice, .other-upload-choice input').hide();
                }
            } 
        } else {
            $('#termLifeModal').find("input").val('');
            $('#wholeLifeModal').find("input").val('');
            $('#universalLifeModal').find("input").val('');
            $('#otherLifeModal').find("input").val('');
            $('#otherLifeModal').find("radio").prop('checked', false);
        }

    });

    if ($('.other9:checked').val() === 'yes') {
        $('.other-upload-choice, .other-upload-choice input').show();
    } else {
        $('.other-upload-choice, .other-upload-choice input').hide();
    }
     $(document).on('change', '.other9', function () {
            if ($(this).val() === 'yes') {
                $('.other-upload-choice, .other-upload-choice input').show();
    } else {
        $('.other-upload-choice, .other-upload-choice input').hide();
    }
    });
    $('a[href="#finish"]').on('click', function () {
        $('.name-of-trust').val(''),
                $(' .trustee').val(''),
                $(' .successor-trustee').val(''),
                $(' .primary-beneficiary').val('');
    });
});

</script>
@stop
