<div class="col-sm-12">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> ACCOUNT REGISTRATION </p>
        <h4> Select your account type. </h4>
        <p> Descriptive/explanatory statement </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','data[77][questionName]','Describe your account type') }}

        <div class="col-sm-6 inner-left " >
            {{ Form::label('label','Describe your account type') }}
            {{ Form::input('text','[question][77][answer][account type]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'account type', 'required'=>'required']) }}
        </div>
      
    </div>
</div>