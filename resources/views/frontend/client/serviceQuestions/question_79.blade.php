@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',21) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Equity Lines of Credit (HELOC)</h2>
                                    <p>Textarea for explanation of HELOC.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[79]','Do you have any home equity lines of credit?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any home equity lines of credit?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[79][CPM-heloc_confirmation]', 'yes',(!empty($answer) && array_key_exists('CPM-heloc_confirmation',$answer)) ? (($answer['CPM-heloc_confirmation']=="yes")  ? true : false):false,['class'=>'cpmHelocConfirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[79][CPM-heloc_confirmation]', 'no',(!empty($answer) && array_key_exists('CPM-heloc_confirmation',$answer)) ? (($answer['CPM-heloc_confirmation']=="no")  ? true : false):false,['class'=>'cpmHelocConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnuaHELOC-details-table" style="display: <?php
                                    if (!empty($answer) && array_key_exists('CPM-heloc_confirmation', $answer) && ($answer['CPM-heloc_confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child" >
                                                        <label> List all home equity lines of credit.</label>

                                                        <table id="cpmHelocInfo" class="find-table-length" >
                                                            <thead>
                                                                <tr> 
                                                                    <td>Property address</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["equity_line_of_credit_records"])) {
                                                                    $childs = json_decode($answer["equity_line_of_credit_records"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->semiAnnualHELOCPropertyName}}</td>
                                                                                <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                    <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="cpmHelocBtn add-dependent" data-toggle="modal" data-target="#cpmHelocModal">Add HELOC</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="equityLineOfCreditDetails" class="hidden" name="answer[79][equity_line_of_credit_records]">{{(!empty($answer["equity_line_of_credit_records"]))? $answer["equity_line_of_credit_records"]:null}}</textarea>
                                    </div>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="cpmHelocModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD HELOC</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <!-- Steps starts here -->

                                                    <!-- steps form starts -->
                                                    <!-- first step starts -->
                                                    <div class="row">
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For which property?') }}
                                                                    {{ Form::input('text','CPM-heloc-property-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCPropertyName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'street address']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the total line of credit amount?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPM-heloc-credit-amount',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCreditAmount borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPM-heloc-current-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCurrentBalance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is your current monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPM-heloc-current-month-payment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCurrentMonthPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 ">
                                                                    <button class="nextBtn pull-right">Next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','CPM-heloc-interest-rate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCInterestRate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="semiAnnualHELOCInterestRateBased"  id="helocothers" name="CPM-heloc--nterest-rate-based" required="" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value="1">10-year Treasury</option>
                                                                        <option value="2">Prime</option>
                                                                        <option value="3">Libor</option>
                                                                        <option value="4">Unsure</option>
                                                                        <option value="5">Other</option>
                                                                    </select>
                                                                </div>

                                                                <div class="col-sm-12 inner-left helocinterestrateothers" style="display: none;">
                                                                    {{ Form::label('label', 'Other') }}
                                                                    {{ Form::input('text','CPM-heloc-other-option',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCOtherOption', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'please describe']) }}
                                                                </div>


                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'When did the loan originate?') }}
                                                                    {{ Form::input('text','CPM-heloc-loan-originate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker semiAnnualHELOCLoanOriginate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of the loan (in years)?') }}
                                                                    {{ Form::input('number','CPM-heloc-loan-term',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>


                                                                <div class="col-sm-12 ">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button id="cpmHelocBtnnew" class='cpmHelocBtn finishBtn pull-right' type="button">
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!-- second step finsih -->
                                                <!-- steps form finsih -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var cpmHelocDetails = [];
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#cpmHelocModal .semiAnnualHELOCPropertyName').val() && $('#cpmHelocModal .semiAnnualHELOCCreditAmount').val()) {
                        if ($('.finishBtn').hasClass('edit-form')) {
                            cpmHelocDetails[$(this).attr('data-index')] = {
//                                firstCheck: $('.cpmHelocConfirmation:checked').val(),
//                                cpmHelocConfirmation: $('#SemiAnnualCCModal .cpmHelocConfirmation:checked').val(),
                                semiAnnualHELOCPropertyName: $('#cpmHelocModal .semiAnnualHELOCPropertyName').val(),
                                semiAnnualHELOCCreditAmount: $('#cpmHelocModal .semiAnnualHELOCCreditAmount').val(),
                                semiAnnualHELOCCurrentBalance: $('#cpmHelocModal .semiAnnualHELOCCurrentBalance').val(),
                                semiAnnualHELOCCurrentMonthPayment: $('#cpmHelocModal .semiAnnualHELOCCurrentMonthPayment').val(),
                                
                                semiAnnualHELOCInterestRate: $('#cpmHelocModal .semiAnnualHELOCInterestRate').val(),
                                semiAnnualHELOCOtherOption: $('#cpmHelocModal .semiAnnualHELOCOtherOption').val(),
                                semiAnnualHELOCLoanOriginate: $('#cpmHelocModal .semiAnnualHELOCLoanOriginate').val(),
                                semiAnnualHELOCLoanTerm: $('#cpmHelocModal .semiAnnualHELOCLoanTerm').val(),
                                semiAnnualHELOCInterestRateBased: $('#cpmHelocModal .semiAnnualHELOCInterestRateBased').val()
                            };
                            $('#cpmHelocInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#cpmHelocModal .semiAnnualHELOCPropertyName').val() + '</td>  <td><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                        } else {
                            $('#cpmHelocInfo tbody').append('<tr data-index=' + cpmHelocDetails.length + '><td>' + $('#cpmHelocModal .semiAnnualHELOCPropertyName').val() + '</td>   <td> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                            cpmHelocDetails.push({
//                                firstCheck: $('.cpmHelocConfirmation:checked').val(),
//                                cpmHelocConfirmation: $('#SemiAnnualCCModal .cpmHelocConfirmation:checked').val(),
                                semiAnnualHELOCPropertyName: $('#cpmHelocModal .semiAnnualHELOCPropertyName').val(),
                                semiAnnualHELOCCreditAmount: $('#cpmHelocModal .semiAnnualHELOCCreditAmount').val(),
                                semiAnnualHELOCCurrentBalance: $('#cpmHelocModal .semiAnnualHELOCCurrentBalance').val(),
                                semiAnnualHELOCCurrentMonthPayment: $('#cpmHelocModal .semiAnnualHELOCCurrentMonthPayment').val(),
                                semiAnnualHELOCInterestRate: $('#cpmHelocModal .semiAnnualHELOCInterestRate').val(),
                                semiAnnualHELOCOtherOption: $('#cpmHelocModal .semiAnnualHELOCOtherOption').val(),
                                semiAnnualHELOCLoanOriginate: $('#cpmHelocModal .semiAnnualHELOCLoanOriginate').val(),
                                semiAnnualHELOCLoanTerm: $('#cpmHelocModal .semiAnnualHELOCLoanTerm').val(),
                                semiAnnualHELOCInterestRateBased: $('#cpmHelocModal .semiAnnualHELOCInterestRateBased').val()
                            });
                        }
                        $('#cpmHelocModal').modal('hide');
                        $('#cpmHelocModal .semiAnnualHELOCPropertyName, #cpmHelocModal .semiAnnualHELOCCreditAmount').val('');
                        $('#cpmHelocModal .cpmHelocConfirmation').prop('checked', false);
                        $('#equityLineOfCreditDetails').html(JSON.stringify(cpmHelocDetails));
                    }
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        if ($('#equityLineOfCreditDetails').val() != '') {
            cpmHelocDetails = JSON.parse($('#equityLineOfCreditDetails').val());
        }
        $(document).on('change', '.cpmHelocConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnuaHELOC-details-table').show();
            } else {
                $('.SemiAnnuaHELOC-details-table').hide();
                cpmHelocDetails = [];
                $('#equityLineOfCreditDetails').html(JSON.stringify(cpmHelocDetails));
                $('.SemiAnnuaHELOC-details-table tbody tr').remove();
            }
        });

        $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.semiAnnualexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });



        $(document).on('click', '#cpmHelocInfo .fa-pencil', function () {
            $('#cpmHelocModal').modal();
            $('#cpmHelocModal #step-1').show();
            $('#cpmHelocModal #step-2, #cpmHelocModal #step-3').css('display', 'none');
            $('#cpmHelocModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualCCDetails = cpmHelocDetails[index];
            $('.cpmHelocConfirmation:checked').val(SemiAnnualCCDetails.cpmHelocConfirmation);
            $('#cpmHelocModal .semiAnnualHELOCPropertyName').val(SemiAnnualCCDetails.semiAnnualHELOCPropertyName);
            $('#cpmHelocModal .semiAnnualHELOCCreditAmount').val(SemiAnnualCCDetails.semiAnnualHELOCCreditAmount);
            $('#cpmHelocModal .semiAnnualHELOCCurrentBalance').val(SemiAnnualCCDetails.semiAnnualHELOCCurrentBalance);
            $('#cpmHelocModal .semiAnnualHELOCCurrentMonthPayment').val(SemiAnnualCCDetails.semiAnnualHELOCCurrentMonthPayment);
            $('#cpmHelocModal .semiAnnualHELOCInterestRate').val(SemiAnnualCCDetails.semiAnnualHELOCInterestRate);
            $('#cpmHelocModal .semiAnnualHELOCOtherOption').val(SemiAnnualCCDetails.semiAnnualHELOCOtherOption);
            $('#cpmHelocModal .semiAnnualHELOCLoanOriginate').val(SemiAnnualCCDetails.semiAnnualHELOCLoanOriginate);
            $('#cpmHelocModal .semiAnnualHELOCLoanTerm').val(SemiAnnualCCDetails.semiAnnualHELOCLoanTerm);
            $('#cpmHelocModal .semiAnnualHELOCInterestRateBased').val(SemiAnnualCCDetails.semiAnnualHELOCInterestRateBased).trigger('change');
            $('#cpmHelocModal .semiAnnualHELOCOtherOption').val(SemiAnnualCCDetails.semiAnnualHELOCOtherOption);
            $('#cpmHelocBtnnew').addClass('edit-form');
            $('#cpmHelocBtnnew').attr('data-index', index);
        });

        $(document).on('click', '.cpmHelocBtn', function () {
            $('#cpmHelocBtnnew').removeClass('edit-form');
            $('#cpmHelocModal input[type="text"], input[type="number"]').val('');
            $('#cpmHelocModal select').val('').trigger('change');
            $('#cpmHelocModal .error-alert').remove();
            $('#cpmHelocModal #step-1').show();
            $('#cpmHelocModal #step-2').hide();
        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);


            $('.swal-button--danger').click(function () {

                $(this).closest('tr').remove();

                cpmHelocDetails.splice(index_id, 1);
                $("#cpmHelocInfo tbody").empty();
                if (cpmHelocDetails.length != 0) {
                    var tr = '';
                    $.each(cpmHelocDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.semiAnnualHELOCPropertyName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#cpmHelocInfo tbody").html(tr);
                }
                $('#equityLineOfCreditDetails').html(JSON.stringify(cpmHelocDetails));

                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '5') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });
    });

</script>
@stop