<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about <?php echo $spouse_name ;?></h6>
        <h2>Let's get more information about <?php echo $spouse_name ;?>.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[224]','Let\s get the rest of your personal information') }}
        <div class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', '{Spouse / partner first name} Phone number') }}
                {{ Form::input('text','answer[224][spouse_phone]', null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'' ]) }}
            </div>
        </div>

        <div class="col-sm-6 left-side" style="clear:left;">
            <div class="inner-left ">
                {{ Form::label('label', '{Spouse / partner first name} email address') }}
                {{ Form::input('email','answer[224][spouse_email]', null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'style' => 'width:100%;', 'placeholder'=>'' ]) }}
            </div>
        </div>


    </div> 
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('change', '.spouse-address-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('.addres-section').removeClass('hide');
            } else {
                $('.addres-section').addClass('hide');
            }
        });
    });
</script>
<style type="text/css">
    .wrapper .sections .section-right .alignment .left-side .inner-left{width:100% !important;}
</style>