<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa { 
        font-size: 13px;
        margin-left: 10px;
    }  
    .add-saving-account {   
        background: transparent;   
        border: 1px solid #1d99d4;       
        font-size: 12px;
        margin-top: 30px;      
        color: #1d99d4;       
    }  
    .businessInfo{
        position: relative;
    }  
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',18) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Business Ownership</h2>
                                    <p>Use this section to enter information about any businesses you own or in which you have ownership interest or partnership.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[95]','Business Ownership') }}


                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Do you own a business or have an ownership interest in a business or partnership?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[95][items_of_particular_value_general_personal_property]', 'yes',(!empty($answer) && array_key_exists('items_of_particular_value_general_personal_property',$answer)) ? (($answer['items_of_particular_value_general_personal_property']=="yes")  ? true : false):false,['class'=>'other-items table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[95][items_of_particular_value_general_personal_property]', 'no',(!empty($answer) && array_key_exists('items_of_particular_value_general_personal_property',$answer)) ? (($answer['items_of_particular_value_general_personal_property']=="no")  ? true : false):false,['class'=>'other-items table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>  
                                    </div>  

                                    <div class="col-sm-6 inner-left personal-property-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('items_of_particular_value_general_personal_property', $answer) && ($answer['items_of_particular_value_general_personal_property'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">

                                        {{ Form::label('label', 'List business interests here.') }}

                                        <table id='businessInfo'>  
                                            <thead>                       
                                                <tr>       
                                                    <td style="font-size: 13px;">Business</td>    
                                                    <td></td>   
                                                    <td></td>    
                                                </tr>
                                            </thead>   
                                            <tbody>

                                                <?php
                                                if (!empty($answer) && array_key_exists('own_house_record', $answer)) {
                                                    $account = json_decode($answer["own_house_record"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->businessName}}</td> 
                                                                <td></td>
                                                                <td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>   
                                        </table>
                                        <button type='button' class="add-saving-account" data-toggle="modal" data-target="#businessModal">Add property</button>
                                    </div>
                                </div> 
                            </div>
                            <textarea id="ownhouse" class="hidden" name="answer[95][own_house_record]">{{(!empty($answer) && array_key_exists('own_house_record',$answer))? $answer["own_house_record"]:null}}</textarea>
                            <div class="sections">
                                <div id="businessModal" class="modal fade add-student-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD BUSINESS</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class="section-right">
                                                            <div class="validation-alert">        
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Name of the business') }}
                                                                    {{ Form::input('text','business_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control business-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'business name', 'required'=>'required']) }}
                                                                </div>    

                                                                <div class="col-sm-12 inner-left">   
                                                                    {{ Form::label('label','What type of business is this?') }}
                                                                    <i class="fa fa-angle-down selectArrow"></i>
                                                                    <select style="position: absolute;" class="business-type" name="business_type" style="display: none;" required><option selected disabled value="">SELECT</option><option value="1">C corp</option><option value="2">S corp</option><option value="3">LLC/LLP</option><option value="4">PA</option><option value="5">Sole proprietor</option><option value="6">Other</option></select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left other-business" style="display:none;">
                                                                    {{ Form::label('label','Type of business entity') }}
                                                                    {{ Form::input('text','business_type_description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control business-type-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'business-type', 'required'=>'required']) }}
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Who has the ownership stake?') }}
                                                                    <i class="fa fa-angle-down selectArrow"></i>
                                                                    <select style="position: absolute;"  class="ownership" name="ownership" style="display: none;" required>
                                                                        <option selected disabled value="">SELECT</option>
                                                                        <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="2"><?php echo $spouse_name; ?></option><?php } ?>
                                                                        <option value="3">other</option>
                                                                    </select>
                                                                </div>
                                                                <div class="col-sm-12 inner-left other-ownership" style="display:none;">
                                                                    {{ Form::label('label','Other') }}
                                                                    {{ Form::input('text','ownership_description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ownership-other-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'description', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','What is the percentage of ownership?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','percentage_of_ownership',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control ownership-percentage', 'data-rule-regex' =>"false", 'required'=>true , 'min'=>'0',  'max' => '100', 'placeholder'=>'', 'required'=>'required']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div> 
                                                                </div> 
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Estimated value of vehicle?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','estimated_value',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control estimated-value', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Additional notes') }}
                                                                    {{ Form::textarea('additional_notes',null,['data-validation'=> '' , 'rows'=>'5','class'=>'custom-validation form-control additional-notes', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'optional', 'required'=>'', 'style'=>'border-radius:0;']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <button id='businessForm'  class="modal-button finishBtn" type="button" style="border-radius: 3px; float:none; background-color: #2179EE; color:#fff;   ">
                                                                        Save
                                                                    </button> 
                                                                </div>
                                                            </div>   
                                                        </div>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>   
                            </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
$(document).ready(function () {


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    var studentList = [];
    if ($('#ownhouse').val() != '') {
        studentList = JSON.parse($('#ownhouse').val());
    }


// ----------------- new js starts ----------------
    $('.finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#businessModal .business-name').val()) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        studentList[$(this).attr('data-index')] = {
                            businessName: $('#businessModal .business-name').val(),
                            businessType: $('#businessModal .business-type').val(),
                            otherBusiness: $('#businessModal .business-type-input').val(),
                            ownership: $('#businessModal .ownership').val(),
                            otherOwnership: $('#businessModal .ownership-other-input').val(),
                            ownershipPercentage: $('#businessModal .ownership-percentage').val(),
                            value: $('#businessModal .estimated-value').val(),
                            notes: $('#businessModal .additional-notes').val()
                        }
                        $('#businessInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#businessModal .business-name').val() + '</td> <td>  </td> <td  align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i> </td>');
                    } else {
                        $('#businessInfo tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#businessModal .business-name').val() + '</td> <td> </td> <td  align="right">  <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>');
                        studentList.push({
                            businessName: $('#businessModal .business-name').val(),
                            businessType: $('#businessModal .business-type').val(),
                            otherBusiness: $('#businessModal .business-type-input').val(),
                            ownership: $('#businessModal .ownership').val(),
                            otherOwnership: $('#businessModal .ownership-other-input').val(),
                            ownershipPercentage: $('#businessModal .ownership-percentage').val(),
                            value: $('#businessModal .estimated-value').val(),
                            notes: $('#businessModal .additional-notes').val()
                        });
                    }
                    $('#businessModal').modal('hide');
                    $('#businessModal .property-name').val('');
                    $('#ownhouse').html(JSON.stringify(studentList));
                }
            }
        }
        return false;
    });
// ---------------- new jsa finish ---------------

    $('#businessInfo').on('click', '.fa-pencil', function () {
        $('#businessModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        insuranceDetails = studentList[index];

        $('#businessModal .business-name').val(insuranceDetails.businessName);
        $('#businessModal .business-type').val(insuranceDetails.businessType).trigger('change');
        $('#businessModal .business-type-input').val(insuranceDetails.otherBusiness);
        $('#businessModal .ownership').val(insuranceDetails.ownership).trigger('change');
        $('#businessModal .ownership-other-input').val(insuranceDetails.otherOwnership);
        $('#businessModal .ownership-percentage').val(insuranceDetails.ownershipPercentage);
        $('#businessModal .estimated-value').val(insuranceDetails.value);
        $('#businessModal .additional-notes').val(insuranceDetails.notes);
        $('#businessForm').addClass('edit-form').attr('data-index', index);
        $('#businessModal .error-alert').remove();
    });
    $('.add-saving-account').on('click', function () {
        $('#businessForm').removeClass('edit-form');
        $('#businessModal input[type="text"],#businessModal input[type="number"]').val('');
        $('#businessModal select').val('').trigger('change');
        $('#businessModal textarea').val('');
        $('#businessModal .error-alert').remove();
    });
    $('#businessInfo').on('click', '.fa-trash', function () { // changes 
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);
        $('.swal-button--danger').click(function () {
            studentList.splice(index_id, 1);
            $("#businessInfo tbody").empty();
            if (studentList.length != 0) {
                var tr = '';
                $.each(studentList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.businessName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
                });
                $("#businessInfo tbody").html(tr);
            }

            $('#ownhouse').html(JSON.stringify(studentList));
            return false;
        });
    });

    $(document).on('change', '.ownership', function () {
        if ($(this).val() == 3) {
            $('.other-ownership,.other-ownership input').show();
        } else {
            $('.other-ownership, .other-ownership input').hide().val('');
        }
    });
    $(document).on('change', '.business-type', function () {
        if ($(this).val() == 6) {
            $('.other-business, .other-business input').show();
        } else {
            $('.other-business, .other-business input').hide().val('');
        }
    });

    $(document).on('change', '.other-items', function () {
        if ($(this).val() == 'yes') {
            $('.personal-property-table').show();
        } else {
            $('.personal-property-table').hide();
            studentList = [];
            $('#ownhouse').html(JSON.stringify(studentList));
            $('.personal-property-table tbody tr').remove();
        }
    });
    $(document).on('change', '.owner', function () {
        if ($(this).val() == '4') {
            $('.owner-name, .owner-name input').show();
        } else {
            $('.owner-name, .owner-name input').hide().val('');
        }
    });
    $('.item-acquired').datetimepicker({
        format: 'MM/DD/YYYY'
    });
});

</script>

@stop