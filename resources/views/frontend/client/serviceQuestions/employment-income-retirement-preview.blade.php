
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open(['method' => 'get','url' => route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id]), 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    <!--{{ Form::input('hidden','serviceId',$currentService->id) }}-->

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INCOME & RETIREMENT PLANS</h6>
                                    <h2>Employment Income & Retirement Plans.</h2>
                                    <p>Having this information helps us provide with the best guidance and support possible. If you need help, simply <a  href="#" class="contact-modal-show">contact us</a>.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>employment income</h4> 
                                        </div>
                                        <div class="status col-sm-4 paddingRight0"> 
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,5,1,86])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(1, $subTopicInfo)) {
                                                    echo $subTopicInfo[1];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>additional income</h4>
                                        </div>
                                        <div class="status col-sm-4 paddingRight0">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,5,2,87])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(2, $subTopicInfo)) {
                                                    echo $subTopicInfo[2];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>defined contribution retirement plans</h4>
                                        </div>
                                        <div class="status col-sm-4 paddingRight0">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,5,3,88])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(3, $subTopicInfo)) {
                                                    echo $subTopicInfo[3];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>pension plans</h4>
                                        </div>
                                        <div class="status col-sm-4 paddingRight0">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,5,4,89])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(4, $subTopicInfo)) {
                                                    echo $subTopicInfo[4];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>executive compensation</h4>
                                        </div>
                                        <div class="status col-sm-4 paddingRight0">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,5,5,206])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(5, $subTopicInfo)) {
                                                    echo $subTopicInfo[5];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="custon-continue-button">
                                        <a href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Continue</a>
                                    </div>
                                    <div class="custon-return">
                                        <a href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset> 
                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div> 
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
@stop 