@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',10) }}
                    {{ Form::input('hidden','subTopicId',27) }}
                    {{ Form::input('hidden','redirectPageName','investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual bonds</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[97]','Individual bonds') }}

                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you invest in individual bonds') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[97][invest_in_individual_bonds]', 'yes',(!empty($answer)&& array_key_exists('invest_in_individual_bonds',$answer)) ?(($answer['invest_in_individual_bonds']=="yes")  ? true : false):false,['class'=>'individual-bonds','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[97][invest_in_individual_bonds]', 'no',(!empty($answer)&& array_key_exists('invest_in_individual_bonds',$answer)) ?(($answer['invest_in_individual_bonds']=="no")  ? true : false):false,['class'=>'individual-bonds','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual bonds</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[971]','Individual bonds') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','About how many trades do you make per year (or your advisor makes for you)?') }}
                                        {{ Form::input('number','answer[971][trades_per_year]',(!empty($answer) && array_key_exists('trades_per_year',$answer)) ? $answer['trades_per_year']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control number-roller trades-per-year', 'data-rule-regex' =>"false", 'required'=>true , 'style'=>'padding-right:35px !important;', 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','About what is the average dollar amount on trades?') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                        {{ Form::input('number','answer[971][average_amount]',(!empty($answer) && array_key_exists('average_amount',$answer)) ? $answer['average_amount']:null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control average-amount', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Individual bonds</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[972]','Individual bonds') }}

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','How many years have you been investing in stocks?') }}
                                        {{ Form::input('number','answer[972][investing_since]',(!empty($answer) && array_key_exists('investing_since',$answer)) ? $answer['investing_since']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control investing-since number-spinner number-roller', 'style' => 'padding-right:35px !important', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                    </div>
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','In what year did you begin investing?') }}
                                        {{ Form::input('number','answer[972][begin_investing]',(!empty($answer) && array_key_exists('begin_investing',$answer)) ? $answer['begin_investing']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control begin-investing', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
     var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,10,'investment-experience-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        
        $(".number-roller").spinner();

        $('.begin-investing').datetimepicker({
            format: 'YYYY'
        });

        if ($('.individual-bonds:checked').val() == 'no') {
            $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        } else {
            $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
        }

        $(document).on('change', '.individual-bonds', function () {
            if ($(this).val() == 'no') {
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });

    });
</script>
@stop
