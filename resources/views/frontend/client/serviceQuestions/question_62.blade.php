
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
    .contingent-beneficiary, .personal-insurance-beneficiary {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .personal-insurance-beneficiary{
        position: relative;
    }
    .contingent-beneficiary-info{
        position: relative;
    }
</style>

<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Other life Insurance policies.</h2>
        <p style="display:none;" class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','data[62][questionName]','Other life Insurance policies.') }}
        <div class="alignment">
            <div class="col-sm-6 inner-left left-side">

                {{ Form::label('label', 'Do you have any personally owned life insurance policies?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('data[62][answer][personally owned life insurance]', 'yes',null,['class'=>'owned-life-insurance' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[62][answer][personally owned life insurance]', 'no',null,['class'=>'owned-life-insurance' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
            <div class="col-sm-6 inner-left medical-conditions right-side" style="display: none;">
                {{ Form::label('label', 'Do you have any medical conditions that could make it hard for you to get life insurance?') }}
                <label class="radio-custom-label" >
                    {{ Form::radio('data[62][answer][medical conditions]', 'yes',null,['class'=>'' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[62][answer][medical conditions]', 'no',null,['class'=>'' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
        </div>

        <div class="hide-section" style="display:none;">
            <div class="alignment">
                <div class="col-sm-6 inner-left left-side ">

                    {{ Form::label('label', 'Policy owner') }}
                    <select style="position: absolute;" class="owner" required="" name="data[62][answer][relationship]" style="display: none;"><option selected disabled value="">SELECT</option><option value="1">user first name</option><option value="2"><?php echo $spouse_name ;?></option><option value="3">Someone else</option><option value="4">Trust</option></select>
                </div>

                <div class="col-sm-6 inner-left  right-side owner-full-name" style="display: none;">
                    {{ Form::label('label', 'Owner\'s full name') }}
                    {{ Form::input('text','data[62][answer][Owner\'s full name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'full name']) }}
                </div>
                <div class="col-sm-6 inner-left  right-side trust-information" style="display: none; margin-top: 39px;">
                    {{ Form::input('text','data[62][answer][trust information]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Trust information']) }}
                </div>
            </div>


            <div class="col-sm-6 inner-left table-width">

                {{ Form::label('label', 'Beneficiaries') }}
                <p style="width:100%;"><i>If a trust is named beneficiary, click <a data-toggle='modal' data-target='#clickHerePersonalModal'>here</a></i></p>

                <table id='personal-insurance-info'>
                    <thead>
                        <tr>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Percentage</td>
                            <td>Primary</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <button type='button' class="personal-insurance-beneficiary" data-toggle="modal" data-target="#personalInsuranceModal">Add beneficiary</button>
            </div>


            <div class="col-sm-6 inner-left ">

                {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('data[62][answer][contingent beneficiaries]', 'yes',null,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[62][answer][contingent beneficiaries]', 'no',null,['class'=>'contingent-beneficiaries' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
            <div class="col-sm-6 inner-left contingent-beneficiaries-modal table-width" style="display:none;">

                {{ Form::label('label', 'Beneficiaries') }}
                <p style="width:100%;"><i>If a trust is named beneficiary, click <a>here</a></i></p>

                <table id='contingent-beneficiary-info'>
                    <thead>
                        <tr>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Percentage</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <button type='button' class="contingent-beneficiary" data-toggle="modal" data-target="#contingentBeneficiaryModal">Add beneficiary</button>
            </div>

            <div class="col-sm-6 inner-left number-of-trustees table-width">
                {{ Form::label('label', 'What type of policy is it?') }}

                <div class="trustee termLife"  >
                    <a class="options" data-toggle="modal" data-target="#termLifeModal" >
                        <div class="select-trustee" >
                            <p>Term Life</p>    
                        </div>
                    </a>
                    <a class="options whole-life" data-toggle="modal" data-target="#wholeLifeModal">
                        <div class="select-trustee">
                            <p>Whole life</p>    
                        </div>
                    </a>
                    <a class="options universal" data-toggle="modal" data-target="#universalLifeModal">
                        <div class="select-trustee">
                            <p>Universal <br> (Flexible Premium)</p>    
                        </div>
                    </a>
                    <a class="options other-policy"data-toggle="modal" data-target="#otherLifeModal">
                        <div class="select-trustee">
                            <p>Other</p>    
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------INSURANCE BENEFICIARY MODAL-------->


<div id="personalInsuranceModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="overflow:hidden;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD BENEFICIARY</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','ADD a Student') }}
                        <div class="col-sm-12  validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','First Name') }}
                                {{ Form::input('text','data[62][answer][first name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Last Name') }}
                                {{ Form::input('text','data[62][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Date of birth') }}
                                {{ Form::input('text','data[62][answer][dob]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Percentage (max 100%)') }}
                                {{ Form::input('number','data[62][answer][Percentage]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required' , 'maxlength'=>3]) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <div class="primary-option">
                                    <ul> 
                                        <p class="primary-select">Primary ?</p>
                                        <li style="display:inline; float: left;"> <label class="checkbox-custom-label">
                                                <input type="checkbox" class="primary-selected">
                                                <span class="checkbox-icon"></span>
                                                <span>
                                                </span>
                                            </label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-sm-12 inner-left">
                                <button id='personalInsuranceForm'  class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div>
</div>



<!-------INSURANCE BENEFICIARY MODAL END-------->



<!-------CONTINGENT BENEFICIARY MODAL-------->
<div id="contingentBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="overflow:hidden;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD CONTINGENT BENEFICIARIES</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>

                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','ADD a Student') }}
                        <div class="col-sm-12 inner-left validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','First Name') }}
                                {{ Form::input('text','data[62][answer][first name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control contingent-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Last Name') }}
                                {{ Form::input('text','data[62][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Date of birth') }}
                                {{ Form::input('text','data[62][answer][dob]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Percentage (max 100%)') }}
                                {{ Form::input('text','data[62][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='contingentBeneficiaryForm'  class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-------CONTINGENT BENEFICIARY MODAL END-------->

<!--//---------TERM LIFE MODAL-----------//-->

<div id="termLifeModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">TERM LIFE INSURANCE POLICY</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','TERM LIFE INSURANCE POLICY') }}
                        <div class="col-sm-12  validation-alert">
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Date purchased') }}
                                {{ Form::input('text','data[62][answer][Date purchased]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Number of years in term') }}
                                {{ Form::input('number','data[62][answer][Number of years]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Annual premium') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Annual premium]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Amount of death benefit') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Amount of death benefit]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true  , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                <button   class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--{{ Form::close() }}-->
            </div>
        </div>
    </div>
</div>
<!--//---------TERM LIFE MODAL END-----------//-->


<!--//---------WHOLE LIFE MODAL-----------//-->

<div id="wholeLifeModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">WHOLE LIFE INSURANCE POLICY</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','WHOLE LIFE') }}
                        <div class="col-sm-12  validation-alert">
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Date purchased') }}
                                {{ Form::input('text','data[62][answer][Date purchased]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Annual premium') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Annual premium]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Amount of death benefit') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Amount of death benefit]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current cash value') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current cash value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current loan value(if no loan, leave blank') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current loan value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current surrender value') }}
                                {{ Form::input('number','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                <button class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--{{ Form::close() }}-->
            </div>
        </div>
    </div>
</div>
<!--//---------WHOLE LIFE MODAL END-----------//-->


<!--//---------UNIVERSAL MODAL-----------//-->

<div id="universalLifeModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">UNIVERSAL LIFE INSURANCE POLICY</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','WHOLE LIFE') }}
                        <div class="col-sm-12  validation-alert">
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Date purchased') }}
                                {{ Form::input('text','data[62][answer][Date purchased]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control termLifeInsurance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Annual target premium') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Annual target premium]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Amount of death benefit') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Amount of death benefit]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current cash value') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current cash value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current loan value(if no loan, leave blank') }}
                                <i class="fa fa-dollar " aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current loan value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current surrender value') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Interest on cash value(if any)') }}
                                {{ Form::input('number','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left ">
                                {{ Form::label('label', 'Are there investment choices offered?') }}
                                <label class="radio-custom-label">
                                    {{ Form::radio('data[62][answer][investment choices offered]', 'yes',null,['class'=>'investment-choices' ,'required'=>'required']) }}Yes
                                    <span class="radio-icon"></span>
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::radio('data[62][answer][investment choices offered]', 'no',null,['class'=>'investment-choices' ,'required'=>'required']) }}No 
                                    <span class="radio-icon"></span>
                                </label>
                            </div>

                            <div class="col-sm-12 inner-left upload-choice" style="display:none;">
                                {{ Form::label('label', 'Click below to upload current investment choices.') }}
                                {{ Form::input('file','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                            </div>


                            <div class="col-sm-12 inner-left">
                                <button class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--{{ Form::close() }}-->
            </div>
        </div>
    </div>
</div>
<!--//---------UNIVERSAL MODAL END-----------//-->


<!--//---------OTHER MODAL-----------//-->

<div id="otherLifeModal" class="modal fade" role="dialog" >
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">OTHER LIFE ISURANCE POLICY</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                        {{ Form::input('hidden','data[62][questionName]','OTHER LIFE ISURANCE POLICY') }}
                        <div class="col-sm-12  validation-alert">
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'What is the name of insurance') }}
                                {{ Form::input('text','data[62][answer][What is the name of insurance]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Issued by what company?') }}
                                {{ Form::input('text','data[62][answer][Issued by what company?]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Annual premium or target premium') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Annual target premium]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Amount of death benefit') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Amount of death benefit]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current cash value') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current cash value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current loan value(if no loan, leave blank') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current loan value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>' ']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Current surrender value') }}
                                <i class="fa fa-dollar" aria-hidden="true"></i>
                                {{ Form::input('number','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label', 'Interest on cash value(if any)') }}
                                {{ Form::input('number','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                            </div>
                            <div class="col-sm-12 inner-left ">
                                {{ Form::label('label', 'Are there investment choices offered?') }}
                                <label class="radio-custom-label">
                                    {{ Form::radio('data[62][answer][investment choices offered]', 'yes',null,['class'=>'other-investment-choices' ,'required'=>'required']) }}Yes
                                    <span class="radio-icon"></span>
                                </label>
                                <label class="radio-custom-label">
                                    {{ Form::radio('data[62][answer][investment choices offered]', 'no',null,['class'=>'other-investment-choices' ,'required'=>'required']) }}No 
                                    <span class="radio-icon"></span>
                                </label>
                            </div>

                            <div class="col-sm-12 inner-left other-upload-choice" style="display:none;">
                                {{ Form::label('label', 'Click below to upload current investment choices.') }}
                                {{ Form::input('file','data[62][answer][Current surrender value]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button class="modal-button"type="button" style="border-radius: 3px;    ">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--{{ Form::close() }}-->
            </div>
        </div>
    </div>
</div>
<!--//---------UNIVERSAL MODAL END-----------//-->

<!-- CLICK HERE MODAL-->




<div id="clickHerePersonalModal" class="modal fade add-student-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content" style="overflow:hidden;">
            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">ADD TRUST</h4>
            </div>
            <div class="modal-body" style='border:none; float: left;'>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                        {{ Form::input('hidden','data[61][questionName]','add trust') }}
                        <div class="col-sm-12  validation-alert">

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Name of trust') }}
                                {{ Form::input('text','data[61][answer][Name of trust]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Trustee') }}
                                {{ Form::input('text','data[61][answer][Trustee]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Successor trustee') }}
                                {{ Form::input('text','data[61][answer][Successor trustee]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                            </div>

                            <div class="col-sm-12 inner-left">
                                {{ Form::label('label','Primary beneficiary') }}
                                {{ Form::input('text','data[61][answer][Primary beneficiary]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                            </div>
                            <div class="col-sm-12 inner-left">
                                <a class='append-personal-insurance-input'>+Beneficiary</a>
                            </div>
                            <div class="col-sm-12 personal-insurance-append-input inner-left">
                            </div>

                            <div class="col-sm-12 inner-left">
                                <button id='clickHereForm' type="button" style="border-radius: 3px;
                                        color: #fff;    
                                        font-family: Lato;
                                        font-size: 18px;
                                        line-height: 24px;                                  
                                        text-align: center;
                                        width: 150px;
                                        padding: 12px 35px;
                                        background-color: #2179EE;
                                        text-decoration: none;
                                        border: none; 
                                        margin-left: 62px;
                                        margin-bottom: 40px;">
                                    Save
                                </button>
                            </div>
                        </div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>


<!-- CLICK HERE MODAL END-->
