<div class="col-sm-12 ">
    <div class="col-sm-6 col-sm-offset-5 section-right">
        {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
        {{ Form::input('hidden','data[35][questionName]','ADD a Student') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Who is the student?') }}
            {{ Form::select('data[35][answer][Who is the student]', ['SELECT','me', 'spouse','child','grandchild','other']) }}
       
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Student\'s First Name') }}
            {{ Form::input('text','data[35][answer][student first name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'First name']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Student\'s Current age)') }}
            {{ Form::input('text','data[35][answer][studnet current age]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For what level of education you are planning?') }}
            {{ Form::select('data[35][answer][level of education]', ['SELECT','primary', 'high school','associates/bachelor\'s','trade school','masters','JD/MD/PhD','other']) }}
        </div>
        <div class="col-sm-8 inner-left">
            <button type="submit" style="border-radius: 3px;
                    color: #fff;
                    font-family: Lato;
                    font-size: 18px;
                    line-height: 24px;
                    text-align: center;
                    width: 150px;
                    padding: 12px 35px;
                    background-color: #2179EE;
                    text-decoration: none;
                    border: none;
                    margin-left: 67px;
                    margin-bottom: 40px;">
                Save
            </button>
        </div>
    </div>
</div>