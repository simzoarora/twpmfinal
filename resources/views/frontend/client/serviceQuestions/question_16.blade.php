<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
<!--        <h2>Do you have any another student loan?</h2>-->
        <h2>Do you have another credit card on which you carry a balance?</h2>
        <p>(Do not include cards that you pay off each month.)</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[16][questionName]','Do you have another credit card on which you carry a balance?') }}
        <div class="col-sm-8 inner-left">
            <label class="radio-custom-label"> {{ Form::radio('data[16][answer][Do you have another credit card on which you carry a balance?]', 'yes',false,['class'=>'other-student-loan']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">   {{ Form::radio('data[16][answer][Do you have another credit card on which you carry a balance?]', 'no',false,['class'=>'other-student-loan']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>