
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa-pencil {
        position: absolute;
        right: 17px;
        top: 20px;
    }
    .other-contingent-beneficiary, .other-personal-insurance-beneficiary {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .other-personal-insurance-beneficiary{
        position: relative;
    }
    .other-other-contingent-beneficiary-info{
        position: relative;
    }
</style>

<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Other life Insurance policies.</h2>
        <p style="display:none;" class="question-description"> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','data[63][questionName]','Other life Insurance policies.') }}
        <div class="alignment">
            <div class="col-sm-6 inner-left left-side">

                {{ Form::label('label', 'Do you have any other personally owned life insurance policies?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('data[63][answer][personally owned life insurance]', 'yes',null,['class'=>'personal-life-insurance' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[63][answer][personally owned life insurance]', 'no',null,['class'=>'personal-life-insurance' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
            <div class="col-sm-6 inner-left personal-medical-conditions right-side" style="display: none;">
                {{ Form::label('label', 'Do you have any medical conditions that could make it hard for you to get life insurance?') }}
                <label class="radio-custom-label" >
                    {{ Form::radio('data[63][answer][medical conditions]', 'yes',null,['class'=>'' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[63][answer][medical conditions]', 'no',null,['class'=>'' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
        </div>

        <div class="personal-hide-section" style="display:none;">
            <div class="alignment">
                <div class="col-sm-6 inner-left left-side ">

                    {{ Form::label('label', 'Policy owner') }}
                    <select style="position: absolute;" class="owner" required="" name="data[63][answer][relationship]" style="display: none;"><option selected disabled value="">SELECT</option><option value="1">user first name</option><option value="2"><?php echo $spouse_name ;?></option><option value="3">Someone else</option><option value="4">Trust</option></select>
                </div>

                <div class="col-sm-6 inner-left  right-side owner-full-name" style="display: none;">
                    {{ Form::label('label', 'Owner\'s full name') }}
                    {{ Form::input('text','data[63][answer][Owner\'s full name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'full name']) }}
                </div>
                <div class="col-sm-6 inner-left  right-side trust-information" style="display: none; margin-top: 39px;">
                    {{ Form::input('text','data[63][answer][trust information]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Trust information']) }}
                </div>
            </div>


            <div class="col-sm-6 inner-left table-width">

                {{ Form::label('label', 'Beneficiaries') }}
                <p style="width:100%;"><i>If a trust is named beneficiary, click <a data-toggle='modal' data-target='#clickHereOtherInsurance'>here</a></i></p>

                <table id='other-personal-insurance-info'>
                    <thead>
                        <tr>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Percentage</td>
                            <td>Primary</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <button type='button' class="other-personal-insurance-beneficiary" data-toggle="modal" data-target="#otherPersonalInsuranceModal">Add beneficiary</button>
            </div>


            <div class="col-sm-6 inner-left ">

                {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('data[63][answer][contingent beneficiaries]', 'yes',null,['class'=>'other-contingent-beneficiaries' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('data[63][answer][contingent beneficiaries]', 'no',null,['class'=>'other-contingent-beneficiaries' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
            <div class="col-sm-6 inner-left other-contingent-beneficiaries-modal table-width" style="display:none;">

                {{ Form::label('label', 'Beneficiaries') }}
                <p style="width:100%;"><i>If a trust is named beneficiary, click <a>here</a></i></p>

                <table id='other-other-contingent-beneficiary-info'>
                    <thead>
                        <tr>
                            <td>First name</td>
                            <td>Last name</td>
                            <td>Percentage</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <button type='button' class="other-contingent-beneficiary" data-toggle="modal" data-target="#OtherContingentBeneficiaryModal">Add beneficiary</button>
            </div>

            <div class="col-sm-6 inner-left number-of-trustees table-width">
                {{ Form::label('label', 'What type of policy is it?') }}

                <div class="trustee termLife"  >
                    <a class="options" data-toggle="modal" data-target="#OtherTermLifeModal" >
                        <div class="select-trustee" >
                            <p>Term Life</p>    
                        </div>
                    </a>
                    <a class="options whole-life" data-toggle="modal" data-target="#OtherWholeLifeModal">
                        <div class="select-trustee">
                            <p>Whole life</p>    
                        </div>
                    </a>
                    <a class="options universal" data-toggle="modal" data-target="#OtherUniversalLifeModal">
                        <div class="select-trustee">
                            <p>Universal <br> (Flexible Premium)</p>    
                        </div>
                    </a>
                    <a class="options other-policy"data-toggle="modal" data-target="#otherInsuranceLifeModal">
                        <div class="select-trustee">
                            <p>Other</p>    
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-------INSURANCE BENEFICIARY MODAL-------->