
<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    table tbody td .fa { 
       margin-left: 10px;
       font-size: 13px;
    } 
    .add-saving-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }  
    .propertyInfo{
        position: relative;
    }  
    .wrapper .sections .section-right .inner-left.owner-dropdown .selectboxit-list{ max-height: 145px !important;}
</style>

@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',15) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>ASSETS</h6>
                                    <h2>Personal Property</h2>
                                    <p>Use this section to enter information about the value of your general personal property. This would include furniture, clothing, household items, dishes, plates, etc.</p>
                                    <p>Exclude anything of particular value, such as jewelry, artwork, tools, collection of substance, etc.</p>
                                    <p>HINT: One rule of thumb that may be used is to count all the finished rooms in your home(excluding bathrooms) and multiply by $10,000.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[92]','Personal Property') }}

                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'What is the value of your general personal property (furniture, clothes, etc.)?') }}
                                        <div class="input-group service-input-group">
                                        <span class="input-group-addon">$</span>
                                        {{ Form::input('number','answer[92][value_of_general_personal_property]',(!empty($answer) && array_key_exists('value_of_general_personal_property',$answer)) ? $answer['value_of_general_personal_property'] : null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control personal-property-value', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                        </div>
                                    </div> 

                                    <div class="col-sm-6 inner-left"> 
                                        {{ Form::label('label', 'Do you have any items of particular value in addition to general personal property?') }}
                                        <label class="radio-custom-label">    
                                            {{ Form::radio('answer[92][items_of_particular_value_addition_general_personal_property]', 'yes',(!empty($answer) && array_key_exists('items_of_particular_value_addition_general_personal_property', $answer)) ? (($answer['items_of_particular_value_addition_general_personal_property']== "yes") ? true : false) : false,['class'=>'other-items table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label"> 
                                            {{ Form::radio('answer[92][items_of_particular_value_addition_general_personal_property]', 'no',(!empty($answer) && array_key_exists('items_of_particular_value_addition_general_personal_property',$answer)) ? (($answer['items_of_particular_value_addition_general_personal_property']=="no")  ? true : false):false,['class'=>'other-items table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span> 
                                        </label>
                                    </div> 

                                    <div class="col-sm-6 inner-left personal-property-table "  style="display:<?php
                                    if (!empty($answer) && array_key_exists('items_of_particular_value_addition_general_personal_property', $answer) && ($answer['items_of_particular_value_addition_general_personal_property'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">

                                        {{ Form::label('label', 'List all items of particular value.') }}

                                        <table id='propertyInfo'>  
                                            <thead>                       
                                                <tr>     
                                                    <td style="font-size: 13px;">Item</td>   
                                                    <td></td>   
                                                    <td></td>  
                                                </tr>
                                            </thead>   
                                            <tbody>

                                                <?php
                                                if (!empty($answer) && array_key_exists('own_house_record', $answer)) {
                                                    $account = json_decode($answer["own_house_record"]);
                                                    if (!empty($account)) {
                                                        foreach ($account as $key => $data) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$data->accountName}}</td> 
                                                                <td></td>
                                                                <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>



                                            </tbody>   
                                        </table>
                                        <button type='button' class="add-saving-account" data-toggle="modal" data-target="#personalPropertyModal">Add Item</button>
                                    </div>
                                </div> 
                            </div>
                            <textarea id="ownhouse" class="hidden" name="answer[92][own_house_record]">{{(!empty($answer) && array_key_exists('own_house_record',$answer))? $answer["own_house_record"]:null}}</textarea>

                            <div class="sections">
                                <div id="personalPropertyModal" class="modal fade add-student-modal" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="overflow:hidden;">
                                            <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD PERSONAL PROPERTY</h4>
                                            </div>
                                            <div class="modal-body" style='border:none; float: left;'>
                                                <div class="row">
                                                    <div class="setup-content">
                                                        <div class="section-right">
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give this item a name') }}
                                                                    {{ Form::input('text','Give_this_item_a_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Brief description of item') }}
                                                                    {{ Form::textarea('answer[Breif description of item][92]',null,['data-validation'=> '' , 'rows'=>'5','class'=>'custom-validation form-control description-of-item', 'data-rule-regex' =>"false",  'placeholder'=>'description', 'required'=>'required', 'style'=>'border-radius:0;']) }}
                                                                </div>
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','When was this item acquired?') }}
                                                                    {{ Form::input('text','When_did_you_purchase_this_house',null,['data-validation'=> '' , 'class'=>'custom-validation form-control item-acquired', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Estimated value of the item?') }}
                                                                    <div class="input-group">
                                                                       <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','Estimated_value_of_the_item',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control value-of-item', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="col-sm-12 inner-left owner-dropdown">
                                                                {{ Form::label('label','Who is the owner?') }}
                                                                <i class="fa fa-angle-down selectArrow"></i>
                                                                <select style="position: absolute;" style='width:100%;' class="owner" name="Who_is_the_owner" style="display: none;" required>
                                                                    <option selected disabled value="">SELECT</option><option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?> <option value="2"><?php echo $spouse_name ;?></option><?php } ?>
                                                                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?> <option value="3">Joint</option><?php } ?>
                                                                    <option value="4">other</option></select>
                                                            </div>
                                                            <div class="col-sm-12 inner-left owner-name" style="display:none;">
                                                                {{ Form::label('label','Owner\'s name') }}
                                                                {{ Form::input('text','owner\'s_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control owner-name-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                            </div>
                                                            <div class="col-sm-12 inner-left text-center">
                                                                <button id='propertyForm'  class=" finishBtn" type="button" style="border-radius: 3px;
                                                                        color: #fff;
                                                                        float: none;
                                                                        border: 0;
                                                                        font-family: Lato;
                                                                        font-size: 18px;
                                                                        line-height: 24px;
                                                                        text-align: center;
                                                                        width: 150px;
                                                                        padding: 12px 35px;
                                                                        background-color: #2179EE;
                                                                        text-decoration: none;
                                                                        margin-left: 0;">
                                                                    Save
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </section>
                    </fieldset> 
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>

      var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        var studentList = [];
        if ($('#ownhouse').val() != '') {
            studentList = JSON.parse($('#ownhouse').val());
        }


        // ------------------ new js starts  ---------------

        $('.finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#personalPropertyModal .property-name').val()) {
                        if ($('.finishBtn').hasClass('edit-form')) {
                            studentList[$(this).attr('data-index')] = {
                                accountName: $('#personalPropertyModal .property-name').val(),
                                description: $('#personalPropertyModal .description-of-item').val(),
                                acquiredDate: $('#personalPropertyModal .item-acquired').val(),
                                value: $('#personalPropertyModal .value-of-item').val(),
                                ownerName: $('#personalPropertyModal .owner-name-input').val(),
                                owner: $('#personalPropertyModal .owner').val()
                            }
                            $('#propertyInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#personalPropertyModal .property-name').val() + '</td> <td>  </td> <td  align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i> </td>');
                        } else {
                            $('#propertyInfo tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#personalPropertyModal .property-name').val() + '</td> <td>  </td> <td  align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>');
                            studentList.push({
                                accountName: $('#personalPropertyModal .property-name').val(),
                                description: $('#personalPropertyModal .description-of-item').val(),
                                acquiredDate: $('#personalPropertyModal .item-acquired').val(),
                                value: $('#personalPropertyModal .value-of-item').val(),
                                ownerName: $('#personalPropertyModal .owner-name-input').val(),
                                owner: $('#personalPropertyModal .owner').val()
                            });
                        }
                        $('#personalPropertyModal').modal('hide');
                        $('#personalPropertyModal .property-name').val('');
                        $('#ownhouse').html(JSON.stringify(studentList));
                    }
                }
            }
            return false;
        });

        // -----------------  new js finsih -----------------

//        $('#personalPropertyModal input[type="text"],input[type="text"], #personalPropertyModal select').on('keyup change', function () {
//            $(this).parent().find('.error-alert').remove();
//            if (!$(this).val()) {
//                $(this).closest('.inner-left').append('<label class="error error-alert">This field is required.</label>');
//            }
//        });
        $('#propertyInfo').on('click', '.fa-pencil', function () {
            $('#personalPropertyModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = studentList[index];

            $('#personalPropertyModal .property-name').val(insuranceDetails.accountName);
            $('#personalPropertyModal .property-name').val(insuranceDetails.accountName);
            $('#personalPropertyModal .description-of-item').val(insuranceDetails.description);
            $('#personalPropertyModal .item-acquired').val(insuranceDetails.acquiredDate);
            $('#personalPropertyModal .value-of-item').val(insuranceDetails.value);
            $('#personalPropertyModal .owner').val(insuranceDetails.owner).trigger('change');
            $('#personalPropertyModal .owner-name-input').val(insuranceDetails.ownerName);
            $('#propertyForm').addClass('edit-form').attr('data-index', index);
            $('#personalPropertyModal .error-alert').remove();
        });
        $('.add-saving-account').on('click', function () {
            $('#propertyForm').removeClass('edit-form');
            $('#personalPropertyModal input[type="text"],#personalPropertyModal input[type="number"]').val('');
            $('#personalPropertyModal select').val('').trigger('change');
            $('#personalPropertyModal textarea').val('');
            $('#personalPropertyModal .error-alert').remove();
        });
        $('#propertyInfo').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            studentList.splice($(this).closest('tr').attr('data-index'), 1);
            
            $("#propertyInfo tbody").empty();
            if (studentList.length != 0) {
                var tr = '';
                $.each(studentList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.accountName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#propertyInfo tbody").html(tr);
            }
            
            $('#ownhouse').html(JSON.stringify(studentList));
            return false;
        });




        $(document).on('change', '.other-items', function () {
            if ($(this).val() == 'yes') {
                $('.personal-property-table').show();
            } else {
                $('.personal-property-table').hide();
                studentList = [];
                $('#ownhouse').html(JSON.stringify(studentList));
                $('.personal-property-table tbody tr').remove();
            }
        });
        $(document).on('change', '.owner', function () {
            if ($(this).val() == '4') {
                $('.owner-name, .owner-name input').show();
            } else {
                $('.owner-name, .owner-name input').hide().val('');
            }
        });
        $('.item-acquired').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });

</script>
@stop 