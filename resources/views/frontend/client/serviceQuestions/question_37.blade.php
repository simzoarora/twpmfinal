<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>ADD a Child/Dependent</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <!--{{Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
        {{ Form::input('hidden','data[37][questionName]','ADD a Child/Dependent') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Student\'s name/nickname') }}
            {{ Form::input('text','data[37][answer][student name/nickname]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'name']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Student\'s Current age)') }}
            {{ Form::input('text','data[37][answer][student current age]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Beginning Year') }}
            {{ Form::input('date','data[37][answer][beginning year]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'d/m/y']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Level') }}
            {{ Form::select('data[37][answer][level of education]', ['SELECT']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'School name(if known)') }}
            {{ Form::input('text','data[37][answer][school name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'school name']) }}
        </div>
        <!--{{ Form::submit('Save') }}-->
    </div>
</div>