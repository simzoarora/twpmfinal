@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',801) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>INCOME</h6>
                                    </div>
                                    <h2>Let's make sure we have the right income information</h2>
                                    <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="#" class="contact-modal-show"> contact us. </a> </p>
                                </div>
                                <?php $array = config('constant.tax_filing_status'); ?>
                                <div class="col-sm-6 col-sm-offset-1 section-right taxBracket">
                                    {{ Form::input('hidden','questionName[23]','Let\'s make sure we have the right income information.') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Household gross income',['title'=>'Total income from all sources']) }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">$</span>
                                            {{ Form::input('number','answer[23][household_gross_income]',isset($defaultData)? $defaultData['housesholdIncome']:null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0 custom-validation form-control', 'data-rule-regex'=>"false", 'readonly'=>true , 'placeholder'=>'' ,'style'=> 'background:transparent;', 'min'=>0]) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-8 inner-left pos-rel">
                                        {{ Form::label('label', 'Estimated taxable income',['title'=>'See line 43 of your latest tax return or ask your tax advisor']) }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">$</span>
                                            {{ Form::input('number','answer[23][estimated_taxable_income]',(!empty($answer) && array_key_exists('estimated_taxable_income',$answer)) ? $answer['estimated_taxable_income'] : null,['data-validation'=> '' , 'class'=>'comprehensive-width borderLeft0  custom-validation form-control taxable-income', 'data-rule-regex'=>"false", 'required'=>true ,  'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                        <div class="lds-dual-ring" style="display: none;"></div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Tax filling status') }}
                                        {{ Form::select('answer[23][tax_filling_status]', $array,isset($defaultData)? $defaultData['taxFillingStatus']:'',['readonly'=>true, 'class'=>'tax_filling_status'])}} 
                                    </div>

                                    <div class="col-sm-8">
                                        <div class=" inner-left">
                                            {{ Form::label('label', 'Tax bracket',[' title'=>"Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income"]) }}
                                            <div class="input-group service-input-group benefit-input-group">
                                                {{ Form::input('text','answer[23][tax_bracket]',(!empty($answer) && array_key_exists('tax_bracket',$answer)) ? $answer['tax_bracket'] : null,['data-validation'=> '' , 'class'=>'  custom-validation form-control tax-current-value', 'data-rule-regex'=>"false", 'readonly'=>true , 'placeholder'=>'' , 'style'=> 'background:transparent;']) }}

                                            </div>
                                        </div>
                                    </div>

                                </div> 
                            </div>

                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).ready(function () {
   
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $("select").selectBoxIt();
    $('.tax_filling_status').css('pointer-events','none');

    $(document).on('keypress', '.taxable-income', function () {

        $('.lds-dual-ring').show();

    });

    $("input[type=number]").on("keypress keyup blur", function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

});
</script>
<script>
    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";
    $(document).ready(function(){

        $('.taxable-income').keyup(function () {

    //        event.preventDefault();
            var taxFilling = $('.taxBracket').find('input[name="answer[23][estimated_taxable_income]"]').val();

            $.ajax({
                type: 'GET',
                url: ajaxUrl,
                data: {
                    taxableIncome: taxFilling
                },
                success: function (resp) {
                    $('.tax-current-value').val(resp);
                    $('.lds-dual-ring').hide();
                },
                error: function (error) {
    //               twpmApp.ajaxInputError(error, $("#example-advanced-form"));
                }
            });

        });
    });
</script>
<style>
    .content{float: left; width:100; margin-bottom:40px;}
</style>
@stop