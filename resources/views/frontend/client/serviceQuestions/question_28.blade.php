@extends('frontend.layouts.client')
@section('title')
@stop
@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',305) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-xs-12 col-sm-12 col-md-4 section-left">
                                    <h6> POLICY DOCUMENTS</h6>
                                    <h2>Upload your policy information.</h2>
                                    <p style="margin-bottom:0;">Please gather and upload All Policies Personally owned for either Life Insurance or Long-Term Care Insurance or quotes and sales material.</p>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-7 col-xs-offset-0 col-sm-offset-0  col-md-offset-1 section-right section-size">
                                    <div class='add-file'>
                                        <table style="width:100%;">
                                            <thead>
                                                <tr>
                                                    <td class="col-sm-5 col-xs-4">Name</td>
                                                    <td class="col-sm-5 col-xs-5 paddingLeft0">Policy Holder</td>
                                                    <td  class="col-sm-2 col-xs-3 paddingLeft0" style="text-align:right; padding-right: 23px;">Actions</td>
                                                </tr>
                                            </thead>
                                            <tbody class="add-input">


                                                <?php
                                                if (!empty($answer) && array_key_exists('files', $answer)) {
                                                    foreach ($answer["files"] as $key => $file) {
                                                        ?> <tr  style="border-bottom:rgb(187, 179, 179) solid 1px" class="fileUploadRow">
                                                            <td class="col-sm-5 col-xs-4 paddingLeft0"> 
                                                                <input data-attr="{{$key}}"  name="answer[28][files][{{$key}}][file_name]" class="hello" readonly="readonly" type="text" value='{{$file["file_name"]}}'>
                                                                <input name="answer[28][files][{{$key}}][file_path]" type="hidden" value='<?php if (array_key_exists('file_path', $file)) echo $file["file_path"]; ?>'>
                                                            </td>
                                                            <td class="col-sm-5 col-xs-5 paddingLeft0"> 
                                                                <input type="text" class="document-selected valid" value="{{@$file["fileholder_name"]}}" readonly="readonly" aria-invalid="false">
                                                                <i class="fa fa-angle-down" aria-hidden="true" style="display:none;position: absolute; right: 41%; z-index: 11; top:4px;  font-size: 16px !important; text-transform: capitalize;">
                                                                </i> 
                                                                <select style="position: absolute;" style="border:0; background-color: #fff;" class="hide document-type select-value status valid" name="answer[28][files][{{$key}}][fileholder_name]" required="" aria-invalid="false">
                                                                    <option selected=""  disabled="" value="">select</option> 
                                                                    <option value='{{ Session::get('loggedInUserName') }}' <?php if (isset($file["fileholder_name"]) && $file["fileholder_name"] == session::get('loggedInUserName')) echo "selected"; ?> >{{ Session::get('loggedInUserName') }}</option>
                                                                    <option value="{{$spouse_name}}" <?php if (isset($file["fileholder_name"]) && $file["fileholder_name"] == "$spouse_name") echo "selected"; ?> >{{$spouse_name}}</option>
                                                                </select>
                                                            </td>
                                                            <td class="col-sm-2 col-xs-3">
                                                                <span class="pull-right"> 
                                                                    <i class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i> 
                                                                    <i title="Edit" class="fa fa-pencil editUploadRow cursor-pointer" style="display:none;" aria-hidden="true"></i> 
                                                                    <i style="display:none;" class="fa fa-trash deleteUploadRow cursor-pointer" aria-hidden="true"></i> 
                                                                </span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }else {
                                                    ?>
                                                <input data-attr="0"  name="answer[28][file_name]" readonly="readonly" type="hidden" value=''>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <label class="radio-custom-label"> 
                                        {{ Form::input('hidden','questionName[28]','Upload your executive compensation documents.') }}
                                    </label>
                                    <div class="col-sm-12 save-file-section paddingLeft0" style="margin-top:0">
                                        <div class="col-sm-6 add-file-btn mt-20">
                                            <label for="files" class="btn btn-primary open_button" style="background:none;">Add File</label>
                                            <label for="files" class="btn btn-primary uploaderBtn no-action" style="background:none; display: none">Add File</label>
                                        </div>
                                    </div>                       
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>  
@stop @section('after-scripts')
<script src="{{ asset('js/life-insurance.js') }}"></script>  
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).ready(function () {



//    $('.hello').keyup(function() {
//    });

//        $(document).on('keyup', '.hello', function () {
//            
//            
//        }); 


    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $('.fileNameSaver').hide();
    var trcount = 0;
//        $(document).on('keyup', '#newName', function () {
//            var inputs = $(".hello");
//            for (var i = 0; i < inputs.length; i++) {
//                if ($('#newName').val() == $(inputs[i]).val()) {
//                    $('.bfd-ok').hide();
//                    $('#newName').closest('.inner-left').last().append('<label class="error error-alert">This name already exists</label>');
//                    break;
//                } else if ($('#newName').val() != $(inputs[i]).val()) {
//                    $('#newName').closest('.inner-left').find('.error-alert').remove();
//                    $('.bfd-ok').show();
//                }
//            }
//        });
    $(".open_button").on('click', function () {
        $.FileDialog({multiple: false})
                .on('files.bs.filedialog', function (ev) {
                    var nooftr = $('.fileUploadRow').length,
                            files = ev.files,
                            formData = new FormData(),
                            i = 0;
                    files.forEach(function (f) {

                        $('.add-file table tbody').append('<tr data-attr= " ' + nooftr + ' " class="fileUploadRow appendrow col-xs-12 paddingLeft0" style="border-bottom:0"><td  class="col-xs-4 col-sm-4 paddingLeft0 pull-left">  \n\
        <input name="answer[28][files][' + nooftr + '][file_name]" readonly="readonly" type="text" class="hello" value=' + $('.file-uploader .fileNameSaver').val() + ' ><input name="answer[28][files][' + nooftr + '][file_path]" readonly="readonly" type="hidden" class="hello" > </td>  \n\
        <td class="col-xs-5 col-sm-4 paddingLeft0 pull-left"><input type="text" class="document-selected hide" value=""> <i class="fa fa-angle-down selectAngle" aria-hidden="true" style="position: absolute; top: 14px !important; left: 132px;"></i> \n\
         <select style="position: absolute;" style="border:0;" class="status document-type select-value valid" name="answer[28][files][' + nooftr + '][fileholder_name]" required="true" aria-invalid="false"> <option selected value="0">select</option> <option value="{{ Session::get('loggedInUserName') }}">{{ Session::get('loggedInUserName') }}</option><?php    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="{{$spouse_name}}">{{$spouse_name}}</option><?php } ?></select></td>\n\
        <td class="col-xs-3 col-sm-4 pull-left"><span class="pull-right"> <i class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i>    <i title="Edit" class="fa fa-pencil editUploadRow cursor-pointer" style="display:none;" aria-hidden="true"></i>  \n\
        <i title="Delete" class="fa fa-trash deleteUploadRow cursor-pointer" style="display:none;" aria-hidden="true"></i> </span></td></tr>');
            $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','none');
                        formData.append("file", f, f.name);
                        trcount++;
                        i++;
                    });
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $('input[name="answer[28][files][' + nooftr + '][file_path]"]').val(response.message);
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }
//                            error: function (error) { 
//                                // handle error
//                            }
                    });
                })
                .on('cancel.bs.filedialog', function (ev) {
//                        alert('here');
                });
    });
    // ----------------- File uploader js finish
    $(document).on('click', '.deleteUploadRow', function () {

        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
                .then((willDelete) => {
                    if (willDelete) {
                        $(this).closest('tr').remove();

                        swal("Your file has been deleted!", {
                            icon: "success",
                        });
                    } else {
                        swal("Your file is safe!");
                    }
                });

    });
    var executorList = [];
    executorList[$(this).attr('data-index')] = {
        fileNameSaver: $('.file-uploader .fileNameSaver').val()
    };
    // for id increment finsih

    $(document).on('click', '.bfd-ok', function () {

        $(this).closest('.appendrow').find('td:last-child .fa-trash').hide();
        $(this).closest('.appendrow').find('td:last-child .fileSave').show();
        $('.file-uploader .fileNameSaver').val(executorList.fileNameSaver);
        $('.fileUploadRow').addClass('fileNotSaved');
        if ($('.bfd-files').find('.bfd-info').length > 0) {
            $('.uploaderBtn').show();
            $('.open_button').hide();
        } else {
            $('.uploaderBtn').hide();
            $('.open_button').show();
        }

        $(".fileNameSaver").validate({
            ignore: ".ignore, :hidden",
            errorPlacement: function errorPlacement(error, element) {
            }
        });
    });
    // for id increment finsih

    $(document).each(function () {
        $(this).on('click', '.fileSave', function () {
            var selectdropdown = $(this).closest('tr').find('.select-value');
            if ($(selectdropdown).val() === '0') {
                $(this).closest('.fileUploadRow ').find('.select-value').parent().append('<div class="error error-alert">Please select value</div>');
                $(this).closest('.fileUploadRow td:nth-child(3n)').find('.editUploadRow, .deleteUploadRow').hide();
                $(this).closest('.fileUploadRow td:nth-child(3n)').find('.fileSave').show();
                return false;
            } else {
                $(this).closest('.fileUploadRow').find('.error-alert').remove();
                $(this).hide();
                $(this).siblings('.editUploadRow, .deleteUploadRow').show().css('opacity', '.7');
                $('.uploaderBtn').hide();
                $('.open_button').show();
                $(this).closest('tr').find(".document-selected, input[type=text], select").removeClass('hide edit').attr('readonly', 'readonly').addClass('hello');
                $(this).closest('tr').find(".document-type").addClass('hide');
                $(this).closest('tr').find('.fa-angle-down').hide();
                $(this).closest('.fileUploadRow').css('border-bottom', '1px solid #bbb3b3');
                $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','auto');
            }
        });
    });
    $(document).on('click', '.editUploadRow', function () {
        $(this).closest('tr').find(".document-selected ").addClass('hide');
        $(this).closest('tr').find(".document-type").removeClass('hide');
        $(this).closest('tr').find(".document-type").removeClass('hide');
        $(this).closest('tr').find('.fa-trash, .editUploadRow').hide();
        $(this).closest('tr').find('.fileSave').show();
        $(this).closest('tr').find("input[type=text]").addClass('edit').removeAttr("readonly").removeClass('hello');
        $(this).closest('.content').next('.actions').find('a[href="#finish"]').css('pointer-events','none');
        $(this).closest('tr').find('.fa-angle-down').show();
    });
    // file uploader js starts 
    $(document).on('click', '.open_button', function () {
        $('.bfd-dropfield-inner').attr('id', 'flUpload');
        $('.bfd-files').append("<span class='inner-left' style='position:relative;'><span class='col-xs-12 paddingLeft0 paddingRight0'> <input class='form-control fileNameSaver hide' id='newName' required='required' placeholder='Enter File Name'></span><label id='lblSize' class='file-size'></label></span>");
        if ($('.fileNameSaver').val() == "") {
            $('.bfd-ok').prop("disabled", "true");
        }
        $('.file-size').text('');
    });
    $('.editUploadRow, .deleteUploadRow').show();
    $('.fileSave').hide();
    // =----------- get file size while uploading file
    $(document).on('change', 'input[type=file]', function () {
        if ($('.bfd-files').find('.bfd-info').length == 1) {
            $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
            $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
        }

        $('.fileNameSaver').removeClass('hide');
        var iSize = ($('input[type=file]')[0].files[0].size / 1024);
        if (iSize / 1024 > 1)
        {
            if (((iSize / 1024) / 1024) > 1)
            {
                iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
                $("#lblSize").html(iSize + "Gb");
            } else
            {
                iSize = (Math.round((iSize / 1024) * 100) / 100)
                $("#lblSize").html(iSize + "Mb");
            }
        } else
        {
            iSize = (Math.round(iSize * 100) / 100)
            $("#lblSize").html(iSize + "kb");
        }

    });
    $(document).on('keyup change', '.bfd-files input', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        $('.bfd-ok').removeAttr('disabled');

        var inputs = $(".hello");

        for (var i = 0; i < inputs.length; i++) {

            if ($(this).val() && $('#newName').val() == $(inputs[i]).val()) {
                $('.bfd-ok').prop("disabled", "true");
                $(this).closest('.inner-left').append('<label class="error error-alert">This name already exists.</label>');
                return;


            } else if (!$(this).val()) {
                $(this).closest('.inner-left').append('<label class="error error-alert">This field is required.</label>');
                $('.bfd-ok').prop("disabled", "true");
            }
        }

    });
    $(document).on('keyup change', '.edit', function () {
        $(this).closest('td').find('.parent-error-alert').remove();
        var inputs = $(".hello");
        for (var i = 0; i < inputs.length; i++) {
            if ($(this).val() == $(inputs[i]).val()) {
                $(this).addClass('col-sm-5');
                $(this).closest('td').append('<div class="col-sm-7 parent-error-alert" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">name already exist</label></div>');
                $(this).closest('tr').find('#saveFile').css('pointer-events', 'none');
                $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'none');
                return;
            } else if (!$(this).val()) {
                $(this).closest('td').append('<div class="col-sm-7" style="height:33px;"><label class="error-alert" style="left:0px! important; bottom:0px! important; top:0px! important;">name already exist</label></div> ');
            } else {
                $(this).closest('tr').find('#saveFile').css('pointer-events', 'all');
                $(this).closest('.content').siblings('.actions').find('a[href="#finish"]').css('pointer-events', 'all');
            }
        }

    });
    // ------------ get file size while uploading file finish
});
$(document).on('change', '.document-type', function () {
    var selectedVal = $(this).closest('.fileUploadRow ').find('.document-type  option:selected ');
    $(this).closest('.fileUploadRow ').find(".document-selected").val($(selectedVal).text());
    if ($('.document-type:not(.hide) option:selected').val() == 0)
    {
        $('.document-type:not(.hide)').parent().append('<div class="error error-alert">Please select value</div>');
    } else {
        $('div.error-alert').remove();
    }
});
</script>
<style type="text/css">
    .file-size{float: left;  width: 100%; text-align: right; color: #ABB1C9; font-size: 12px; margin: 0; font-weight: normal}
    .modal-header{border-radius: 5px 5px 0 0;}
    @media (max-width: 768px){
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow{
            display: inherit;
        }
        .wrapper .sections .section-right .add-file table tbody tr.fileUploadRow td .error-alert {
            position: absolute;
            top: 29px;
            left: 0;
            padding: 4px 10px !important;
            width: 150px !important;
        }
        .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{
            background: #fff;
            margin-right: 8px;
            padding-left: 8px;
            left: auto !important;
            right: 0;
            top: 13px !important;
        }
    }

    .wrapper .sections .section-right .add-file table tbody tr td i.fa-angle-down{ top: 13px !important;}
    label.error-alert{
        border-radius: 0;
        width:100%;
        position: absolute;
        bottom: -22px;
        left: 0;
        font-size: 13px;
        padding: 7px;
        font-weight: 400;
        padding-left: 15px;
        background-color: #f2dede;
        border-color: #ebccd1; 
        color: #a94442;}
</style>
<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script> 
@stop
