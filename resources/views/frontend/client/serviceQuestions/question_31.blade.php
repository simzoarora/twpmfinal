<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p style="margin-bottom: 0;">FAMILY</p>
        <h2>Tell us about you and your family</h2>
        <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="">Contact Us.</a></p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[31][questionName]','Do you have any children?') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have any children?') }}
            <label class="radio-custom-label"> 
                {{ Form::radio('data[31][answer][have any children]', 'yes',false,['class'=>'family-info' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> 
                {{ Form::radio('data[31][answer][have any children]', 'no',false,['class'=>'family-info' ,'required'=>'required']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>
