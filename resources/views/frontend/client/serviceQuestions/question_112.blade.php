<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">DEPOSITS</p>
        <h2>Initial Deposit/Transfer</h2>
        <p>Descriptive/explanatory statement.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the anticipated initial deposit and/or transferred assets to be managed in this account?') }}
            <i class="fa fa-dollar fa-dollar-bottom" aria-hidden="true"></i>
            {{ Form::input('text','data[112][answer][initial deposit]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
    </div>
</div>