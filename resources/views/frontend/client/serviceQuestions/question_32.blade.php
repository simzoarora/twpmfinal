@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',304) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6 style="margin-bottom: 0;">FAMILY</h6>
                                    <h2>About you, your health, and your coverage.</h2>
                                    <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="#" class="contact-modal-show" data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[32]','Tell us about you and your family') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Your age') }}
                                       <?php $dob = $defaultData['userData']['dob']; $diff = (date('Y') - date('Y', strtotime($dob))); ?> 
                                        <input type="number" name="answer[32][age]" min=0 max=100 class="custom-validation form-control" required="true" placeholder="" value="<?php echo $diff; ?>" style="pointer-events:none;" />
                                               
                                        <!--{{ Form::input('text','answer[32][age]',(!empty($defaultData['userData']) && array_key_exists('dob',$defaultData['userData'])) ? 0 : null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}-->

                                    </div>

                                    <div class="col-sm-8 inner-left">   
                                        {{ Form::label('label', 'Do you have serious health conditions?') }}
                                        <label class="radio-custom-label">     
                                            {{ Form::radio('answer[32][serious_health_conditions]', 'yes',(!empty($answer) && array_key_exists('serious_health_conditions',$answer)) ? (($answer['serious_health_conditions']=="yes")  ? true : false):false,['required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">     
                                            {{ Form::radio('answer[32][serious_health_conditions]', 'no',(!empty($answer) && array_key_exists('serious_health_conditions',$answer)) ? (($answer['serious_health_conditions']=="no")  ? true : false):false,['required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Amount of Group Life Insurance coverage insurance for you.') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            {{ Form::input('number','answer[32][amount_of_group_life_insurance]',(!empty($answer) && array_key_exists('amount_of_group_life_insurance',$answer)) ? $answer['amount_of_group_life_insurance']: null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                        <h3></h3> 
                        <fieldset>
                            <section class="sections">
                                <div class="col-sm-12 ">
                                    <div class="col-md-4 col-xs-11 section-left"> 
                                        <h6 style="margin-bottom: 0;">FAMILY</h6>
                                        <h2>About {{$spouse_name}}, their health, and their coverage.</h2> 
                                        <p>Simply follow the prompts and enter the requested information, so we can provide the best guidance.If you need help, you can always <a href="#" class="contact-modal-show" data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-1 section-right"> 
                                        {{ Form::input('hidden','questionName[322]','Tell us about you and your family') }}
                                        <div class="col-sm-8 inner-left spouse-age">
                                            <label for="label">{{$spouse_name}}'s age </label>
                                        <input type="number" name="answer[322][spouse_partner_age]" min=0 max=100 class="custom-validation form-control" required="true" placeholder="" value="<?php echo $spouse_age;  ?>" />
                                            <!--{{ Form::input('number','answer[322][spouse_partner_age]',(!empty($answer) && array_key_exists('spouse_partner_age',$answer)) ? $answer['spouse_partner_age']: null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0, 'max'=> 100]) }}-->
                                        </div>

                                        <div class="col-sm-8 inner-left spouse-health-condition">
                                            {{ Form::label('label', 'Do '.$spouse_name.' have any serious health conditions?') }}
                                            <label class="radio-custom-label">     
                                                {{ Form::radio('answer[322][spouse_have_serious_health_conditions]', 'yes',(!empty($answer) && array_key_exists('spouse_have_serious_health_conditions',$answer)) ? (($answer['spouse_have_serious_health_conditions']=="yes")  ? true : false):false,['required'=>'required']) }}Yes
                                                <span class="radio-icon"></span>
                                            </label>
                                            <label class="radio-custom-label">     
                                                {{ Form::radio('answer[322][spouse_have_serious_health_conditions]', 'no',(!empty($answer) && array_key_exists('spouse_have_serious_health_conditions',$answer)) ? (($answer['spouse_have_serious_health_conditions']=="no")  ? true : false):false,['required'=>'required']) }}No
                                                <span class="radio-icon"></span>
                                            </label>
                                        </div>

                                        <div class="col-sm-8 inner-left spouse-life-insurance">
                                            {{ Form::label('label', 'Amount of Group Life insurance for '.$spouse_name.'.') }}
                                            <div class="service-input-group input-group">
                                                <span class="input-group-addon"> $ </span>
                                                {{ Form::input('number','answer[322][amount_group_life_insurance_spouse]',(!empty($answer) && array_key_exists('amount_group_life_insurance_spouse',$answer)) ? $answer['amount_group_life_insurance_spouse']: null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </fieldset>
                    <?php } ?>
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0;">FAMILY</p>
                                    <h2>Tell us about you and your family</h2>
                                    <p>Learning more about your family and dependents can help paint a more comprehensive financial picture.
                                        If you need help, simply <a href="#" class="contact-modal-show">contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[321]','Tell us about you and your family') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have children?') }}
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[321][have_children]', 'yes',(!empty($answer) && array_key_exists('have_children',$answer)) ? (($answer['have_children']=="yes")  ? true : false) : false ,['class'=>'children-confirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label> 
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[321][have_children]', 'no',(!empty($answer) && array_key_exists('have_children',$answer)) ? (($answer['have_children']=="no")  ? true : false) : false,['class'=>'children-confirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="col-sm-12 inner-left children-details-table" style="display: <?php
                                    if (!empty($answer) && array_key_exists('have_children', $answer) && ($answer['have_children'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12  ">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <div class="row">
                                                        <div class="add-child">

                                                            <table id='student-info'>
                                                                <thead>
                                                                    <tr> 
                                                                        <td>Dependent</td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (!empty($answer["have_children_records"])) {
                                                                        $childs = json_decode($answer["have_children_records"]);
                                                                        if (!empty($childs)) {
                                                                            foreach ($childs as $key => $child) {
                                                                                ?>
                                                                                <tr class="children-info" data-index="{{$key}}">
                                                                                    <td class="child-name">{{$child->name}}</td>

                                                                                    <td></td> <td  align="right">  <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table> 
                                                            <div class="col-md-5 ">
                                                                <div class="row">
                                                                    <button type='button' class="add-dependent" data-toggle="modal" data-target="#childrenModal">Add dependent</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="haveChildrenDetails" class="hidden" name="answer[321][have_children_records]">{{(!empty($answer["have_children_records"]))? $answer["have_children_records"]:null}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div id="childrenModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">


                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD CHILD/DEPENDENT</h4>
                                        </div>
                                        <div class="modal-body" style="overflow: hidden;">
                                            <div class="row">
                                                <div class="setup-content">
                                                    <div class="section-right">
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Child/dependent\'s name/nickname') }}
                                                            {{ Form::input('text','childn_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control children-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'name']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Child/dependent\'s age') }}
                                                            {{ Form::input('number','child_age',null,['data-validation'=> '' , 'style' => 'text-align:left;', 'class'=>'custom-validation form-control children-age', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'age', 'min'=>0, 'max'=>100]) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Does this child/dependent have special needs?') }}
                                                            <label class="radio-custom-label"> 
                                                                {{ Form::radio('planning_for_this_child_educational_expenses', 'yes',false,['class'=>'educational-needs','required'=>'required']) }}Yes
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                            <label class="radio-custom-label"> 
                                                                {{ Form::radio('planning_for_this_child_educational_expenses', 'no',false,['class'=>'educational-needs','required'=>'required']) }}No
                                                                <span class="radio-icon"></span>
                                                            </label>
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='childrenForm' type="button" class="finishBtn" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/life-insurance.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();

    $("select").selectBoxIt();

    var childrenList = [];
    if ($('#haveChildrenDetails').val() != '') {
        childrenList = JSON.parse($('#haveChildrenDetails').val());
    }
    $('#childrenModal').on('click', '.close, #childrenForm', function () {
        $('#childrenModal .error-alert').remove();
    });

    $(document).on('change', '.children-confirmation', function () {
        if ($(this).val() === 'yes') {
            $('.children-details-table').show();
        } else {
            $('.children-details-table').hide();
            childrenList = [];
            $('#haveChildrenDetails').html(JSON.stringify(childrenList));
        }
    });



// ----------------------- new js starts -----------------------



    $('.finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#childrenModal .children-name').val() && $('#childrenModal .children-age').val() && $('#childrenModal input[type="radio"]').is(':checked')) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        childrenList[$(this).attr('data-index')] = {
                            name: $('#childrenModal .children-name').val(),
                            age: $('#childrenModal .children-age').val(),
                            needs: $('#childrenModal .educational-needs:checked').val()
                        };
                        $('#student-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#childrenModal .children-name').val() + '</td>    </td> <td> <td  align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" onclick="deleteRow(this)"></i> </td>');
                    } else {
                        $('#student-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#childrenModal .children-name').val() + '</td>   <td> </td> <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" onclick="deleteRow(this)"></i></td></tr>');
                        childrenList.push({
                            name: $('#childrenModal .children-name').val(),
                            age: $('#childrenModal .children-age').val(),
                            needs: $('#childrenModal .educational-needs:checked').val()
                        });


                    }
                    $('#childrenModal').modal('hide');
                    $('#childrenModal .children-name, #childrenModal .children-age').val('');
//                        $('#childrenModal .semiAnnuaHELOCConfirmation').prop('checked', false);
                    $('#haveChildrenDetails').html(JSON.stringify(childrenList));
                }
            }
        }
        return false;
    });
    // ------------------- new js finsih -----------------------------

    $(document).on('click', '#student-info .fa-pencil', function () {
        $('#childrenModal').modal();
        var index = $(this).closest('tr').attr('data-index');
        studentDetails = childrenList[index];
        $('#childrenModal .children-name').val(studentDetails.name);
        $('#childrenModal .children-age').val(studentDetails.age);
        $('#childrenModal .educational-needs[value=' + studentDetails.needs + ']').prop('checked', true);
        $('#childrenForm').addClass('edit-form');
        $('#childrenForm').attr('data-index', index);
    });
    $(document).on('click', '.add-dependent', function () {
        $('#childrenForm').removeClass('edit-form');
        $('#childrenModal .error-alert').remove();
//            $('#childrenModal input[type="text"], input[type="number"]').val('');
        $('#childrenModal .children-name').val('');
        $('#childrenModal .children-age').val('');
        $('#childrenModal input[type="radio"]').prop('checked', false);
    });

    $(document).on('click', '.fa-trash', function () {
        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);
        childrenList.splice($(this).closest('tr').attr('data-index'), 1);

//            $("#student-info tbody").empty();
//            if (childrenList.length != 0) {
//                var tr = '';
//                $.each(childrenList, function (key, value) {
//                    tr += '<tr data-index="' + key + '"><td>' + value.name + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
//                });
//                $("#student-info tbody").html(tr);
//            }

        $('#haveChildrenDetails').html(JSON.stringify(childrenList));
        return false;

    });
});

</script> 

@stop


