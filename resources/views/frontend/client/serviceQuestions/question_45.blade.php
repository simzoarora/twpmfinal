@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',301) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Let's make sure we have the right income information</h2>
                                    <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply<a href="#" class="contact-modal-show"> contact us.</a></p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[45]','Let\'s make sure we have the right income information.') }}
                                    <div class="col-sm-6 inner-left gross-income">
                                        {{ Form::label('label', 'Household gross income',['title'=>'Total income from all sources']) }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            {{ Form::input('number','answer[45][household_gross_income]',isset($defaultData)? $defaultData['housesholdIncome']:'',['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width','readonly'=>true, 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'style'=>'background:transparent;', 'min'=>0]) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Marital status') }}
                                        <i class="fa fa-angle-down " aria-hidden="true" style="position:absolute; right: 29%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                        <select readonly style="position: absolute;" class="life-insurance-marital-status error select-marital" required="required" name="answer[45][marital_status]" aria-required="true" aria-invalid="true" style="display: none;"><option value="" disabled="" selected="selected">SELECT</option>
                                            <option value="0" <?php if ($defaultData['taxFillingStatus'] == 1) echo "selected"; ?>>Single</option>
                                            <option value="1" <?php if (($defaultData['taxFillingStatus'] == 2) || ($defaultData['taxFillingStatus'] == 3) || ($defaultData['taxFillingStatus'] == 4) || ($defaultData['taxFillingStatus'] == 5) || ($defaultData['taxFillingStatus'] == 6)) echo "selected"; ?>>Married</option>
                                        </select>
                                    </div> 


                                    <div class="col-sm-8 inner-left your-gross-income">

                                        {{ Form::label('label', 'Your gross income') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[45][your_gross_income]',(!empty($answer) && array_key_exists('your_gross_income',$answer)) ? $answer['your_gross_income'] :null,['data-validation'=> null, 'class'=>'borderLeft0 comprehensive-width custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left spouse-income"  display:<?php
                                    if (($defaultData['taxFillingStatus'] == 2) || ($defaultData['taxFillingStatus'] == 3) || ($defaultData['taxFillingStatus'] == 4) || ($defaultData['taxFillingStatus'] == 5) || ($defaultData['taxFillingStatus'] == 6)) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >

                                         {{ Form::label('label', 'Your spouse/partner\'s gross income') }}
                                         <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[45][spouse_partner_gross_income]',(!empty($answer) && array_key_exists('spouse_partner_gross_income',$answer)) ? $answer['spouse_partner_gross_income'] :null,['data-validation'=> '' , 'class'=>'borderLeft0 comprehensive-width custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'min'=>0]) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <!--<a href="{{route(                                       'frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}" class="returnLater">Save and return later</a>-->
                    {{ Form::close() }} 
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/life-insurance.js') }}"></script>
<script>
var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();
    $(".actions").addClass('col-xs-offset-0 col-sm-offset-4 col-md-offset-4 col-lg-offset-4');
    $(".returnLater").addClass('col-xs-offset-0 col-sm-offset-1 col-md-offset-1 col-lg-offset-1');
    if ($('.life-insurance-marital-status').val() === '0') {
        $('.your-gross-income, .spouse-income').hide();
        $('.your-gross-income input, .spouse-income input').val('');
    } else {
        $('.your-gross-income, .spouse-income').show();
    }

    $(document).on('change', '.life-insurance-marital-status', function () {
        if ($(this).val() === '0') {
            $('.your-gross-income input, .spouse-income input').val('');
            $('.your-gross-income, .spouse-income').hide();
        } else {
            $('.your-gross-income, .spouse-income').show();
        }
    });
    $("select").selectBoxIt();
     $('.select-marital').css('pointer-events','none');
//        $(document).on('change', '.life-insurance-marital-status', function () {
//            if ($(this).val() === 0 || !$(this).val()) {
//                $('.spouse-income,.spouse-income input').hide().val('');
//                $('.spouse-age').hide();
//                $('.spouse-health-condition').hide();
//                $('.spouse-life-insurance').hide();
//                $('.your-gross-income, .your-gross-income input').hide().val('');
//            } else {
//                $('.spouse-income, .spouse-income input').show();
//                $('.spouse-age').show();
//                $('.spouse-health-condition').show();
//                $('.spouse-life-insurance').show();
//                $('.your-gross-income, .your-gross-income input').show();
//            }
//        });
});
</script>

@stop