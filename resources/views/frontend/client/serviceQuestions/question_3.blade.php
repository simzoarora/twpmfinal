 
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',502) }}

                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>LIABILITIES</h6>
                                    <h2>Mortgages on non-rental property</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[3]','Mortgages on non-rental property') }}
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label', 'Do you have any mortgages on non-rental property?') }}
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[3][DRconfirmation]', 'yes',(!empty($answer) && array_key_exists('DRconfirmation',$answer)) ? (($answer['DRconfirmation']=="yes")  ? true : false) :false,['class'=>'DRMortgageConfirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label> 
                                        <label class="radio-custom-label">  
                                            {{ Form::radio('answer[3][DRconfirmation]', 'no',(!empty($answer) && array_key_exists('DRconfirmation',$answer)) ? (($answer['DRconfirmation']=="no")  ? true : false) :false,['class'=>'DRMortgageConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left DRDetails-table" style="display:  <?php
                                    if (!empty($answer) && array_key_exists('DRconfirmation', $answer) && ($answer['DRconfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12  ">
                                            <div class="row">
                                                <div class="col-sm-12 ">
                                                    <div class="row">
                                                        <div class="add-child add-property">

                                                            <table id='property-info' class="find-table-length">
                                                                <thead>
                                                                    <tr> 
                                                                        <td colspan="2">
                                                                            <h5> 
                                                                                List of all non-rental mortgages.
                                                                            </h5> 
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Complete property address</td>
                                                                        <td></td>
                                                                        <td></td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    if (!empty($answer["nonRentalMortgagesRecords"])) {
                                                                        $mortgage = json_decode($answer["nonRentalMortgagesRecords"]);
                                                                        if (!empty($mortgage)) {
                                                                            foreach ($mortgage as $key => $data) {
                                                                                ?>
                                                                                <tr data-index="{{$key}}">
                                                                                    <td>{{$data->propertyName}}</td>
                                                                                    <td valign="top"> </td>
                                                                                    <td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                        <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                                </tr>
                                                                                <?php
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>
                                                                </tbody>
                                                            </table> 
                                                            <div class="col-md-5">
                                                                <div class="row">
                                                                    <button type='button' class="add-dependent add-mortgage" data-toggle="modal" data-target="#propertyModal">Add mortgage</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <textarea id="nonRentalMortgagesData" class="hidden" name="answer[3][nonRentalMortgagesRecords]">{{(!empty($answer["nonRentalMortgagesRecords"]))? $answer["nonRentalMortgagesRecords"]:null}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="sections">
                                <div id="propertyModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD MORTGAGE</h4>
                                            </div>
                                            <div class="modal-body" style="overflow: hidden;">
                                                <div class="row">
                                                    <!-- first step starts -->
                                                    <div class=" setup-content" id="step-1">
                                                        <div class="section-right ">
                                                            {{ Form::input('hidden','questionName[3]','ADD MORTGAGE') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'For which Property?') }}
                                                                {{ Form::input('text','property_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Street Address']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What was the original mortgage amount?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon" id="basic-addon1">$</span>
                                                                    {{ Form::input('number','original_mortgage_amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control mortgage-original-amount', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'When did the motrgage begin?') }}
                                                                {{ Form::input('text','motrgage_begin_dat',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker mortgage-begin-datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What is current mortgage balance?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon" id="basic-addon1">$</span>
                                                                    {{ Form::input('number','mortgage_balance', null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control mortgage-balance', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <button class="nextBtn" type="button" >next
                                                                    <i class="fa fa-arrow-right"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- first step finsih -->
                                                    <!-- second step starts -->
                                                    <div class="setup-content" id="step-2" style="display: none">
                                                        <div class="section-right ">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="mortgage-tabed mortgageSecondStep">
                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0"> 
                                                                    <label style="font-weight:normal;">What type of mortgage is it?</label>
                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-one', 'Fixed', ['class' => 'label', 'for' => 'option-one'])}}
                                                                        {{ Form::radio('selector', 'fixed',null,['class'=>'DBTypeMortgage',  'id' => 'option-one', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-two', 'Variable', ['class' => 'label', 'for' => 'option-two'])}}
                                                                        {{ Form::radio('selector', 'variable',null,['class'=>'DBTypeMortgage',  'id' => 'option-two', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        <label class="label" for="option-three" style="line-height: normal; padding: 14px 5px;">Interest <br> - only </label>
                                                                        <!--{{Form::label('option-three', 'Interest <br> - Only  ', ['class' => 'label', 'for' => 'option-three'])}}-->
                                                                        {{ Form::radio('selector', 'interestonly',null,['class'=>'DBTypeMortgage',  'id' => 'option-three', ' required'=>'required']) }}

                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-four', 'Other', ['class' => 'label', 'for' => 'option-four'])}}
                                                                        {{ Form::radio('selector', 'other',null,['class'=>'DBTypeMortgage',  'id' => 'option-four', ' required'=>'required']) }}
                                                                    </div>  
                                                                </div>

                                                                <div class="tab-content row">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is interest rate?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','mortgage_interest_rate',null,['data-validation'=> '' , 'class'=>'custom-validation borderRight0 form-control mortgage-interest-rate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <span class="input-group-addon"  >%</span>
                                                                        </div>
                                                                    </div>
                                                                    <!-- fixed tab content starts --> 
                                                                    <div id="fixed" class="tab-pane fade">

                                                                    </div>
                                                                    <!-- fixed tab content finish -->
                                                                    <!-- variable tab content starts -->
                                                                    <div id="variable" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'In how many years after origination do the adjustments begin?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','adjustments_begin',null,['data-validation'=> '' , 'class'=>'custom-validation borderRight0 form-control adjustments-begin', 'data-rule-regex'=>"false", 'required'=>true, 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon"  >Years</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'How often does it adjust?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','adjust_often_value',null,['data-validation'=> '' , 'class'=>'custom-validation borderRight0 form-control adjust-often-value', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon" style="position: relative;padding: 0 10px;">
                                                                                    <p style="position: absolute;margin: 0;padding: 0;top: 0;z-index: 100;width: 100%;left: 0;" class=" text-center"><i class="fa fa-angle-up cursor-pointer"></i></p>
                                                                                    <input type="text" name="often_adjust" class="select-val adjust-often-select-value" readonly="" value="Years" style="text-transform: capitalize;outline: none;width: 50px!important;border: 0;text-align: center;float: left;padding: 0!important;height: auto;font-size: 14px;margin: 0;line-height: initial;margin: 0;">
                                                                                    <p style="margin:0;padding: 0;position: absolute;left: 0;bottom: 0;width: 100%;" class="col-xs-12 col-sm-12 text-center"><i class="fa fa-angle-down cursor-pointer"></i></p>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Interest rate cap (leave blank if there is no rate cap).') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','interest_rate_cap',null,['data-validation'=> '' , 'class'=>'custom-validation borderRight0 form-control interest-rate-cap', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon"  >%</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- variable tab content finish --> 
                                                                    <!-- interest tab content starts --> 
                                                                    <div id="interestOnly" class="tab-pane fade">

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'For what number of years is the loan interest-only?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_years_interest',null,['data-validation'=> '' ,'class'=>'borderRight0 custom-validation form-control loan-years-interest', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon"  >Years</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Is the interest rate fixed during the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'yes',false,['class'=>'fixed-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'no',false,['class'=>'fixed-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Does the interest rate float after the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'yes',false,['class'=>'float-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'no',false,['class'=>'float-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>


                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                            <select style="position: absolute;" class="status valid mortgageinterestdropdown" id="mortgageothersdropdown"  name="interest_rate_based" required="" aria-invalid="false" style="display: none;">
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">10-year Treasury</option>
                                                                                <option value="2">Prime</option>
                                                                                <option value="3">Libor</option>
                                                                                <option value="4">Unsure</option>
                                                                                <option value="5">Other</option>
                                                                            </select>
                                                                        </div> 

                                                                        <div class="col-sm-12 inner-left mortgageothers" style="display: none">
                                                                            {{ Form::label('label', 'Other') }}
                                                                            {{ Form::input('text','mortgage_other',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-other', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>


                                                                        <div class="col-sm-12 inner-left negatively-amortize">
                                                                            {{ Form::label('label','Does this loan negatively amortize?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                            <select style="position: absolute;" class="status valid negativelyAmortize" name="negatively_amortize" required="" aria-invalid="false" >
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">Yes</option>
                                                                                <option value="2">No</option>
                                                                                <option value="3">I don't know</option></select>
                                                                        </div>
                                                                    </div>

                                                                    <div id="others" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter Type of Mortgage?') }}
                                                                            {{ Form::input('text','mortgage_type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control mortgage-type', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Describe']) }}
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-12">
                                                                        <!--id='childrenForm'--> 
                                                                        <button class="backBtn"  type="button" ><i class="fa fa-arrow-left" ></i> back</button>
                                                                        <button class="nextBtn" type="button" >next <i class="fa fa-arrow-right"></i></button>

                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <!-- second step finsih -->
                                                    <!-- third step starts -->
                                                    <div class="setup-content" id="step-3"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="mortgage-tabed loan-term-step">
                                                                <h2>What is loan term?</h2> 

                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0"> 
                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-five', '15 Yr', ['class' => 'label', 'for' => 'option-five'])}}
                                                                        {{ Form::radio('yearselection', 'fifteenyr',null,['class'=>'DRFixedYearSelection',  'id' => 'option-five', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-six', '30 Yr', ['class' => 'label', 'for' => 'option-six'])}}
                                                                        {{ Form::radio('yearselection', 'thirtyyear',null,['class'=>'DRFixedYearSelection',  'id' => 'option-six', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-seven', 'Other', ['class' => 'label', 'for' => 'option-seven'])}}
                                                                        {{ Form::radio('yearselection', 'otheryear',null,['class'=>'DRFixedYearSelection',  'id' => 'option-seven', ' required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="tab-content row">
                                                                    <!-- fiftenyr tab content starts --> 
                                                                    <div id="fiftenyr" class="tab-pane fade in"></div>
                                                                    <!-- fiftenyr tab content finish -->
                                                                    <!-- thirtyyr tab content starts -->
                                                                    <div id="thirtyyr" class="tab-pane fade"></div>
                                                                    <!-- thirtyyr tab content finish --> 

                                                                    <div id="loantermothers" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter loan term?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_term',null,['data-validation'=> '' ,  'class'=>'borderRight0 custom-validation form-control enter-loan-term', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon" id="basic-addon1">Years</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button class="backBtn"  type="button" ><i class="fa fa-arrow-left" ></i> back</button>
                                                                        <button class="nextBtn" type="button"> next
                                                                            <i class="fa fa-arrow-right"></i>
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <!-- tabbed pannel finsih -->
                                                        </div>
                                                    </div>
                                                    <!-- third step finsih -->
                                                    <!-- fourth step starts -->
                                                    <div class="setup-content" id="step-4"  style="display: none">
                                                        <div class="section-right">
                                                            <div class="col-sm-12 inner-left priv-insurace">
                                                                {{ Form::label('label', 'Do you pay Private Mortgage Insurance (PMI) on this loan?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'yes',false,['class'=>'private-insurance','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'no',false,['class'=>'private-insurance','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-pmi" style="display:none;">
                                                                {{ Form::label('label', 'Is PMI paid as a seperate itemized payment on your mortgage statement?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'yes',false,['class'=>'mortgage-pmi-confirmation','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'no',false,['class'=>'mortgage-pmi-confirmation','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-paid-monthly"  style="display:none;">
                                                                {{ Form::label('label', 'What amount is paid monthly?') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon"  >$</span>
                                                                    {{ Form::input('number','mortgage_paid_monthly',null,['data-validation'=> '' , 'class'=>'custom-validation borderLeft0 form-control mortgage-paid-monthly-box', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <!--id='childrenForm'-->  
                                                                <button class="backBtn"  type="button" ><i class="fa fa-arrow-left" style="font-size: 15px; font-weight: normal; margin-right: 4px;"></i> back</button>
                                                                <button class="nextBtn" type="button" >next
                                                                    <i class="fa fa-arrow-right" style="    font-size: 15px;    font-weight: normal;    margin-left: 4px;"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fourth step finish -->
                                                    <!-- fifth step starts -->
                                                    <div class="setup-content mortgage-last-step" id="step-5"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="latest-statement"> 

                                                                <div class="col-sm-12 inner-left">
                                                                    <h2>Please enter the following amounts from your latest statement</h2> 
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Principal', [ 'class' => 'single-line'] ) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','prinicpal_amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 qty1 inlineinput custom-validation form-control prinicpal-amount', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v0', 'placeholder'=>''
//                                                                                , 'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57'
                                                                            ]) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Interest' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','loan_interest',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 loan-interest inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v1', 'placeholder'=>'',
//                                                                            'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57'
                                                                ]) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Property Tax' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','property_tax',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 property-tax inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true ,  'id' => 'v2','placeholder'=>'', 
//                                                                        'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57'
                                                            ]) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Private mortgage Insurance', [ 'class' => 'double-line'] ) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','private_motor_insurance',null,['data-validation'=> '' , 'class'=>'borderLeft0 qty1 private-motor-insurance inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v3', 'required'=>true , 'placeholder'=>'',
//                                                                    'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57'
                                                        ]) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Other (CDD fees, etc.)' , [ 'class' => 'double-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','others_cdd_fees',null,['data-validation'=> '' , 'class'=>'borderLeft0 qty1 others-cdd-fees inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v4', 'required'=>true , 'placeholder'=>'', 
//                                                                'onkeypress'=>'return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57'
                                                    ]) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Monthly total' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" style="border:0; border-top:solid 2px #000;">$</span>
                                                                            {{ Form::input(' number','monthly_total',null,['data-validation'=> '' ,'readonly' => 'readonly',  'class'=>'borderLeft0 monthly-total inlineinput custom-validation form-control', 'style' => 'border:0; margin:0; border-top:solid 2px #000; text-align:right', 'data-rule-monthly-total inlineinput custom-validation form-controlregex'=>"false", 'required'=>true , 'id' => 'result', 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'If property taxes, are not paid in the mortgage, what are the annual property taxes?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','tax_not_paidl',null,['data-validation'=> '', 'class'=>'borderLeft0 tax-not-paid inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <!--id='childrenForm'-->  
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left" style="font-size: 15px; font-weight: normal; margin-right: 4px;"></i> back</button>
                                                                    <button id="finishDRMortgagenew" class='finishDRMortgage finishBtn pull-right' type="button" >
                                                                        finish<i class="fa fa-arrow-right" style="    font-size: 15px;    font-weight: normal;    margin-left: 4px;"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fifth step finsih -->  
                                                    <!-- Form ends here -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                        </section>
                    </fieldset>
                    <!--<a class="returnLater"  href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>

<script>
    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";</script>
<script>
    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        var childrenList = [];
        $(document).on('change', '.DRMortgageConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.DRDetails-table').show();
            } else {
                $('.DRDetails-table').hide();
                childrenList = [];
                $('#nonRentalMortgagesData').html(JSON.stringify(childrenList));
                $('.DRDetails-table tbody tr').remove();
            }
        });
        if ($('#nonRentalMortgagesData').val() != '') {
            childrenList = JSON.parse($('#nonRentalMortgagesData').val());
        }

        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        childrenList[$(this).attr('data-index')] = {
                            propertyName: $('#propertyModal .property-name').val(),
                            mortgageOriginalAmount: $('#propertyModal .mortgage-original-amount').val(),
                            mortgageBeginDatetimepicker: $('#propertyModal .mortgage-begin-datetimepicker').val(),
                            mortgageBalance: $('#propertyModal .mortgage-balance').val(),
                            mortgageInterestRate: $('#propertyModal .mortgage-balance').val(),
                            adjustmentsBegin: $('#propertyModal .adjustments-begin').val(),
                            adjustOften: $('#propertyModal .adjust-often').val(),
                            interestRateCap: $('#propertyModal .interest-rate-cap').val(),
                            loanYearsInterest: $('#propertyModal .loan-years-interest').val(),
                            fixedInterestConfirmation: $('#propertyModal .fixed-interest-confirmation:checked').val(),
                            floatInterestConfirmation: $('#propertyModal .float-interest-confirmation:checked').val(),
                            mortgageinterestdropdown: $('#propertyModal .mortgageinterestdropdown').val(),
                            mortgageOther: $('#propertyModal .mortgage-other').val(),
                            negativelyAmortize: $('#propertyModal .negativelyAmortize').val(),
                            mortgageType: $('#propertyModal .mortgage-type').val(),

                            AdjustOftenValue: $('#propertyModal .adjust-often-value').val(),
                            AdjustOftenSelectValue: $('#propertyModal .adjust-often-select-value').val(),
                            AdjustOftenValueConcatenate: $('#propertyModal .adjust-often-value').val() + ''+ $('#propertyModal .adjust-often-select-value').val() ,

                            DRFixedYearSelection: $('#propertyModal .DRFixedYearSelection:checked').val(),
                            DBTypeMortgage: $('#propertyModal .DBTypeMortgage:checked').val(),

                            enterLoanTerm: $('#propertyModal .enter-loan-term').val(),
                            privateInsurance: $('#propertyModal .private-insurance:checked').val(),
                            mortgagePmiConfirmation: $('#propertyModal .mortgage-pmi-confirmation:checked').val(),
                            mortgagePaidMonthly: $('#propertyModal .mortgage-paid-monthly-box').val(),
                            prinicpalAmount: $('#propertyModal .prinicpal-amount').val(),
                            monthlyTotal: $('#propertyModal .monthly-total').val(),
                            loanInterest: $('#propertyModal .loan-interest').val(),
                            propertyTax: $('#propertyModal .property-tax').val(),
                            privateMotorIinsurance: $('#propertyModal .private-motor-insurance').val(),
                            othersCddFees: $('#propertyModal .others-cdd-fees').val(),
                            taxNotPaid: $('#propertyModal .tax-not-paid').val()
                        };
                        $('#property-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#propertyModal .property-name').val() + '</td><td valign="top"> </td><td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#property-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#propertyModal .property-name').val() + '</td><td valign="top"> </td><td valign="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        childrenList.push({
                            propertyName: $('#propertyModal .property-name').val(),
                            mortgageOriginalAmount: $('#propertyModal .mortgage-original-amount').val(),
                            mortgageBeginDatetimepicker: $('#propertyModal .mortgage-begin-datetimepicker').val(),
                            mortgageBalance: $('#propertyModal .mortgage-balance').val(),
                            mortgageInterestRate: $('#propertyModal .mortgage-interest-rate').val(),
                            adjustmentsBegin: $('#propertyModal .adjustments-begin').val(),
                            adjustOften: $('#propertyModal .adjust-often').val(),
                            interestRateCap: $('#propertyModal .interest-rate-cap').val(),
                            loanYearsInterest: $('#propertyModal .loan-years-interest').val(),
                            fixedInterestConfirmation: $('#propertyModal .fixed-interest-confirmation:checked').val(),
                            floatInterestConfirmation: $('#propertyModal .float-interest-confirmation:checked').val(),
                            mortgageinterestdropdown: $('#propertyModal .mortgageinterestdropdown').val(),
                            mortgageOther: $('#propertyModal .mortgage-other').val(),
                            negativelyAmortize: $('#propertyModal .negativelyAmortize').val(),
                            mortgageType: $('#propertyModal .mortgage-type').val(),

                            AdjustOftenSelectValue: $('#propertyModal .adjust-often-select-value').val(),
                            AdjustOftenValue: $('#propertyModal .adjust-often-value').val(),
                            AdjustOftenValueConcatenate: $('#propertyModal .adjust-often-value').val() + '' + $('#propertyModal .adjust-often-select-value').val() ,

//                            secondStepNestedTabs: $('#propertyModal .mortgageSecondStep ul.mortgage-nav li.active a').text(),
//                            loanTerm: $('#propertyModal .loan-term-step ul.mortgage-nav li.active a').text(),

                            DRFixedYearSelection: $('#propertyModal .DRFixedYearSelection:checked').val(),
                            DBTypeMortgage: $('#propertyModal .DBTypeMortgage:checked').val(),
                            enterLoanTerm: $('#propertyModal .enter-loan-term').val(),
                            privateInsurance: $('#propertyModal .private-insurance:checked').val(),
                            mortgagePmiConfirmation: $('#propertyModal .mortgage-pmi-confirmation:checked').val(),
                            mortgagePaidMonthly: $('#propertyModal .mortgage-paid-monthly-box').val(),
                            prinicpalAmount: $('#propertyModal .prinicpal-amount').val(),
                            loanInterest: $('#propertyModal .loan-interest').val(),
                            propertyTax: $('#propertyModal .property-tax').val(),
                            privateMotorIinsurance: $('#propertyModal .private-motor-insurance').val(),
                            othersCddFees: $('#propertyModal .others-cdd-fees').val(),
                            taxNotPaid: $('#propertyModal .tax-not-paid').val(),
                            monthlyTotal: $('#propertyModal .monthly-total').val()
                        });
                    }
                    $('#nonRentalMortgagesData').html(JSON.stringify(childrenList));
                    $('#propertyModal').modal('hide');
                }

            }
            return false;
        });
        $(document).on('click', '#property-info .fa-pencil', function () {
            $('#propertyModal').modal();
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            var index = $(this).closest('tr').attr('data-index');
            propertyDetails = childrenList[index];
            secondMortgageTab = childrenList[index];
            loanTermSteps = childrenList[index];
            secondTabContainers = childrenList[index];
            $('#propertyModal .property-name').val(propertyDetails.propertyName);
            $('#propertyModal .mortgage-original-amount').val(propertyDetails.mortgageOriginalAmount);
            $('#propertyModal .mortgage-begin-datetimepicker').val(propertyDetails.mortgageBeginDatetimepicker);
            $('#propertyModal .mortgage-balance').val(propertyDetails.mortgageBalance);
            $('#propertyModal .mortgage-interest-rate').val(propertyDetails.mortgageInterestRate);
            $('#propertyModal .adjustments-begin').val(propertyDetails.adjustmentsBegin);
            $('#propertyModal .adjust-often').val(propertyDetails.adjustOften);
            $('#propertyModal .interest-rate-cap').val(propertyDetails.interestRateCap);
            $('#propertyModal .loan-years-interest').val(propertyDetails.loanYearsInterest);

            $('#propertyModal .fixed-interest-confirmation[value=' + propertyDetails.fixedInterestConfirmation + ']').prop('checked', true);
            $('#propertyModal .float-interest-confirmation[value=' + propertyDetails.floatInterestConfirmation + ']').prop('checked', true);
            $('#propertyModal .mortgageinterestdropdown').val(propertyDetails.mortgageinterestdropdown).trigger('change');
            $('#propertyModal .mortgage-other').val(propertyDetails.mortgageOther);
            $('#propertyModal .negativelyAmortize').val(propertyDetails.negativelyAmortize).trigger('change');
            $('#propertyModal .mortgage-type').val(propertyDetails.mortgageType);

            $('#propertyModal .adjust-often-select-value').val(propertyDetails.AdjustOftenSelectValue);
            $('#propertyModal .adjust-often-value').val(propertyDetails.AdjustOftenValue);

//            $('#propertyModal .mortgageSecondStep ul.mortgage-nav li.active a').text(propertyDetails.secondMortgageTab);
//            $('#propertyModal .loan-term-step ul.mortgage-nav li.active a').text(propertyDetails.loanTermSteps);

            $('#propertyModal .DRFixedYearSelection[value=' + propertyDetails.DRFixedYearSelection + ']').prop('checked', true);
            $('#propertyModal .DBTypeMortgage[value=' + propertyDetails.DBTypeMortgage + ']').prop('checked', true);

            $('#propertyModal .enter-loan-term').val(propertyDetails.enterLoanTerm);
            $('#propertyModal .private-insurance[value=' + propertyDetails.privateInsurance + ']').prop('checked', true);
            $('#propertyModal .mortgage-pmi-confirmation[value=' + propertyDetails.mortgagePmiConfirmation + ']').prop('checked', true);

            $('#propertyModal .mortgage-paid-monthly-box').val(propertyDetails.mortgagePaidMonthly);
            $('#propertyModal .prinicpal-amount').val(propertyDetails.prinicpalAmount);
            $('#propertyModal .loan-interest').val(propertyDetails.loanInterest);
            $('#propertyModal .property-tax').val(propertyDetails.propertyTax);
            $('#propertyModal .private-motor-insurance').val(propertyDetails.privateMotorIinsurance);
            $('#propertyModal .others-cdd-fees').val(propertyDetails.othersCddFees);
            $('#propertyModal .tax-not-paid').val(propertyDetails.taxNotPaid);
            $('#propertyModal .monthly-total').val(propertyDetails.monthlyTotal);
            $('#finishDRMortgagenew ').addClass('edit-form');
            $('#finishDRMortgagenew').attr('data-index', index);
            // for second step fixed variable interst-only and others

            if ($('.private-insurance:checked').val() === 'yes') {
                $('#propertyModal .error-alert').remove();
                $('.mortgage-pmi').show();
            } else {
                $('.mortgage-pmi').hide();
                $('.mortgage-pmi-confirmation').prop('checked', false);
            }

            if ($('.mortgage-pmi-confirmation:checked').val() === 'yes') {
                $('#propertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }

            if ($('.DBTypeMortgage:checked').val() === 'fixed') {
                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }

            if ($('.DBTypeMortgage:checked').val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }

            if ($('.DBTypeMortgage:checked').val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }

            if ($('.DBTypeMortgage:checked').val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }

            //  this is for the second step tab panne finsh



            //   this is for the third step tab panne


            if ($('.DRFixedYearSelection:checked').val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }

            if ($('.DRFixedYearSelection:checked').val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#loantermothers, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }

            if ($('.DRFixedYearSelection:checked').val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
            // this is the third  step finsih

        });


//edit finish
        $(document).on('click', '.finishDRMortgage', function () {
            $('#propertyModal .error-alert').remove();

        });

        $(document).on('click', '.add-mortgage', function () {
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5').hide();
            $('#finishDRMortgagenew').removeClass('edit-form');
            $('#propertyModal  .monthly-total, #propertyModal input[type="text"], #propertyModal input[type="number"]').val('');
            $('#propertyModal input[type="radio"]').prop('checked', false);
            $('.mortgage-tabed .OptionSelection label').removeClass('married-selected');
            $('.secondloan-li, .loan-li').removeClass('active');
            $('#fixed, #interestOnly, #variable, #others, #loantermothers').removeClass('active in');
            $('#propertyModal .mortgage-paid-monthly, #propertyModal .mortgage-pmi').hide();
            $('#propertyModal .mortgageSecondStep .mortgage-nav .secondloan-li:first-child ').removeClass('active');
            $('#propertyModal  .adjust-often-select-value').val('Years');
            $('#fixed').removeClass('active in');
            $('.mortgageinterestdropdown, .negativelyAmortize').val("").trigger('change');

        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');

            deleteRow(index_id);
            $('.swal-button--danger').click(function () {

                childrenList.splice(index_id, 1); 
                $("#property-info tbody").empty();
                if (childrenList.length != 0) {
                    var tr = '';
                    $.each(childrenList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.propertyName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#property-info tbody").html(tr);
                }

                $('#nonRentalMortgagesData').html(JSON.stringify(childrenList));
                return false;
            });
        });
 
        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        // on click show listing table


        $("#mortgageothersdropdown").on('change', function () {
            if ($(this).val() === '5') {
                $('.mortgageothers').show();
            } else {
                $('.mortgageothers').hide();
            }
        });


        $(document).on('change', '.private-insurance', function () {
            if ($(this).val() === 'yes') {
                $('#propertyModal .error-alert').remove();
                $('.mortgage-pmi').show();

            } else {
                $('.mortgage-pmi-confirmation').prop('checked', false);
                $('.mortgage-pmi').hide();
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });


        $(document).on('change', '.mortgage-pmi-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('#propertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });
        // calculate total
        $(document).on("change", ".qty1", function () {
            var sum = 0;
            $(".qty1").each(function () {
                sum += +$(this).val();
            });
            $(".monthly-total").val(sum);
        });

        //   this is for the second step tab panne

        $(document).on('change', '.DBTypeMortgage', function () {
            if ($(this).val() === 'fixed') {
                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.DBTypeMortgage', function () {
            if ($(this).val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.DBTypeMortgage', function () {
            if ($(this).val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.DBTypeMortgage', function () {
            if ($(this).val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }
        });


        //   this is for the third step tab panne


        $(document).on('change', '.DRFixedYearSelection', function () {
            if ($(this).val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.DRFixedYearSelection', function () {
            if ($(this).val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#fiftenyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }
        });

        $(document).on('change', '.DRFixedYearSelection', function () {
            if ($(this).val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
        });
        $('.fa-angle-up').on('click', function () {
            $('.select-val').val('Months');
        });
        $('.fa-angle-down').on('click', function () {
            $('.select-val').val('Years');
        });

    });

</script>
<style>
    .wrapper .sections .section-right .negatively-amortize .selectboxit-list{max-height: 111px !important}
</style>

@stop
