<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about you</h6>
        <h2>Let's get the rest of your personal information.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[220]','Let\s get the rest of your personal information') }}
        <div class="alignment about-you">
            <div class="col-sm-6 left-side">
                <div class="inner-left ">
                    {{ Form::label('label', 'First name') }}
                    {{ Form::input('text','answer[220][spouse_personal_name]', $spouse_name,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>false , 'placeholder'=>'','id'=>'firstnameprepopulate','readonly']) }}
                </div>
            </div>

            <div class="col-sm-2 small-error">
                <div class="inner-left" style="width:100% !important;">
                    {{ Form::label('label', 'MI') }}
                    {{ Form::input('text','answer[220][spouse_personal_mi]', (!empty($answer) && array_key_exists('spouse_personal_mi', $answer)) ? $answer['spouse_personal_mi']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'placeholder'=>'' , 'style'=>'width:100%;']) }}
                </div>
            </div>
        </div>

        <div class="alignment about-you">
            <div class="col-sm-6 left-side ">
                <div class="inner-left ">
                    {{ Form::label('label', 'Last name') }} 
                    {{ Form::input('text','answer[220][spouse_personal_last_name]',(!empty($answer) && array_key_exists('spouse_personal_last_name', $answer)) ? $answer['spouse_personal_last_name']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                </div>
            </div>

            <div class="col-sm-2 small-error">
                <div class="inner-left" style="width:100% !important;">
                    {{ Form::label('label', 'Suffix') }}
                    {{ Form::input('text','answer[220][spouse_personal_suffix]',(!empty($answer) && array_key_exists('spouse_personal_suffix', $answer)) ? $answer['spouse_personal_suffix']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'placeholder'=>'' , 'style'=>'width:100%;']) }}
                </div>
            </div>
        </div> 
    </div>
</div>