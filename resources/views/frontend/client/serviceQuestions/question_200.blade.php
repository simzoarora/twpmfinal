@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',802) }}
                    {{ Form::input('hidden','subTopicId',35) }}
                    {{ Form::input('hidden','redirectPageName','DebtLiabilities_review') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <h6>LIABILITIES</h6>
                                    </div>
                                    <h2>Equity Lines of Credit (HELOC)</h2>
                                    <p>Textarea for explanation of HELOC</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[200]','Do you have any home equity lines of credit?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any home equity lines of credit?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[200][semi_annual_heloc_confirmation]', 'yes',(!empty($answer) && array_key_exists('semi_annual_heloc_confirmation',$answer)) ? (($answer['semi_annual_heloc_confirmation']=="yes")  ? true : false):false,['class'=>'table-confirmation semiAnnuaHELOCConfirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[200][semi_annual_heloc_confirmation]', 'no',(!empty($answer) && array_key_exists('semi_annual_heloc_confirmation',$answer)) ? (($answer['semi_annual_heloc_confirmation']=="no")  ? true : false):false,['class'=>'table-confirmation semiAnnuaHELOCConfirmation' , 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnuaHELOC-details-table" style="display: <?php
                                    if (!empty($answer) && array_key_exists('semi_annual_heloc_confirmation', $answer) && ($answer['semi_annual_heloc_confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h5> List all home equity lines of credit.</h5>

                                                        <table id='SemiAnnual-HELOCInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Property address</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["equity_line_of_credit_records"])) {
                                                                    $childs = json_decode($answer["equity_line_of_credit_records"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->semiAnnualHELOCPropertyName}}</td>
                                                                                <td></td> <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-5 ">
                                                            <div class="row">
                                                                <button type='button' class="SemiAnnualAddHELOCBtn add-dependent" data-toggle="modal" data-target="#SemiAnnualHELOCModal">Add HELOC</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="equityLineOfCreditDetails" class="hidden" name="answer[200][equity_line_of_credit_records]">{{(!empty($answer["equity_line_of_credit_records"]))? $answer["equity_line_of_credit_records"]:null}}</textarea>
                                    </div>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="SemiAnnualHELOCModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD HELOC</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- steps form starts -->
                                                        <form role="form">
                                                            <!-- first step starts -->
                                                            <div class=" setup-content" id="step-1">
                                                                <div class="section-right">
                                                                    <!-- first step content starts here -->
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'For which property?') }}
                                                                        {{ Form::input('text','semi-annual-heloc-property-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCPropertyName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Street Address']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the total line of credit Amount?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','semi-annual-heloc-credit-amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualHELOCCreditAmount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the current balance?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','semi-annual-heloc-current-balance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualHELOCCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is your current monthly payment?') }}
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','semi-annual-heloc-current-month-payment',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control semiAnnualHELOCCurrentMonthPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i> </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- first step finsih -->

                                                            <!-- second step starts -->
                                                            <div class=" setup-content" id="step-2" style="display: none">
                                                                <div class="section-right">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the interest rate?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','semi-annual-heloc-interest-rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control semiAnnualHELOCInterestRate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <span class="input-group-addon">%</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                        <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                        <select style="position: absolute;" class="semiAnnualHELOCInterestRateBased"  id="helocothers" name="semi-annual-heloc--nterest-rate-based" required="" style="display: none;">
                                                                            <option selected="" disabled="" value="">SELECT</option>
                                                                            <option value="1">10-year Treasury</option>
                                                                            <option value="2">Prime</option>
                                                                            <option value="3">Libor</option>
                                                                            <option value="4">Unsure</option>
                                                                            <option value="5">Other</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left helocinterestrateothers" style="display: none;">
                                                                        {{ Form::label('label', 'Other?') }}
                                                                        {{ Form::input('text','answer[semi-annual-heloc-other-option]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCOtherOption', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'please describe']) }}
                                                                    </div>


                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'When did the loan originate?') }}
                                                                        {{ Form::input('text','answer[semi-annual-heloc-loan-originate]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker semiAnnualHELOCLoanOriginate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                    </div>

                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is the term of the loan (in years)?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','answer[semi-annual-heloc-loan-term]',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control semiAnnualHELOCLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <span class="input-group-addon">Years</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12"> 
                                                                        <!--id='SemiAnnualAddHELOCBtn'-->  
                                                                        <button type="button" class="backBtn"> <i class="fa fa-arrow-left"></i> back </button>
                                                                        <button id="SemiAnnualAddHELOCBtnnew" class='SemiAnnualAddHELOCBtn finishBtn pull-right' type="button">  finish <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- second step finsih -->
                                                            <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater" href="{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}">Save and return later</a>-->
                    {{ Form::close() }}
                </div> 
            </div> 
        </div>
    </div>
</div>
<!-- modal finsih -->
<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,802,'DebtLiabilities_review'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var SemiAnnualHELOCDetails = [];
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() && $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val()) {
                        if ($('.finishBtn').hasClass('edit-form')) {
                            SemiAnnualHELOCDetails[$(this).attr('data-index')] = {
                                semiAnnualHELOCPropertyName: $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(),
                                semiAnnualHELOCCreditAmount: $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(),
                                semiAnnualHELOCCurrentBalance: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(),
                                semiAnnualHELOCCurrentMonthPayment: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(),
                                semiAnnualHELOCInterestRate: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(),
                                semiAnnualHELOCOtherOption: $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(),
                                semiAnnualHELOCLoanOriginate: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(),
                                semiAnnualHELOCLoanTerm: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(),
                                semiAnnualHELOCInterestRateBased: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val()
                            };
                            $('#SemiAnnual-HELOCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() + '</td>  <td></td> <td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                        } else {
                            $('#SemiAnnual-HELOCInfo tbody').append('<tr data-index=' + SemiAnnualHELOCDetails.length + '><td>' + $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() + '</td>   <td> </td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                            SemiAnnualHELOCDetails.push({
                                semiAnnualHELOCPropertyName: $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(),
                                semiAnnualHELOCCreditAmount: $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(),
                                semiAnnualHELOCCurrentBalance: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(),
                                semiAnnualHELOCCurrentMonthPayment: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(),
                                semiAnnualHELOCInterestRate: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(),
                                semiAnnualHELOCOtherOption: $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(),
                                semiAnnualHELOCLoanOriginate: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(),
                                semiAnnualHELOCLoanTerm: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(),
                                semiAnnualHELOCInterestRateBased: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val()
                            });
                        }
                        $('#SemiAnnualHELOCModal').modal('hide');
                        $('#equityLineOfCreditDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
                    }
                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        if ($('#equityLineOfCreditDetails').val() != '') {
            SemiAnnualHELOCDetails = JSON.parse($('#equityLineOfCreditDetails').val());
        }
        $(document).on('change', '.semiAnnuaHELOCConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnuaHELOC-details-table').show();
            } else {
                $('.SemiAnnuaHELOC-details-table').hide();
                SemiAnnualHELOCDetails = [];
                $('#equityLineOfCreditDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
                $('.SemiAnnuaHELOC-details-table tbody tr').remove();
            }
        });

        $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.semiAnnualexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });


        $(document).on('click', '#SemiAnnual-HELOCInfo .fa-pencil', function () {
            $('#SemiAnnualHELOCModal').modal();
            $('#SemiAnnualHELOCModal #step-1').show();
            $('#SemiAnnualHELOCModal #step-2, #SemiAnnualHELOCModal #step-3').css('display', 'none');
            $('#SemiAnnualHELOCModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualHELOCDetailsList = SemiAnnualHELOCDetails[index];
            $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCPropertyName);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCCreditAmount);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCCurrentBalance);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCCurrentMonthPayment);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCInterestRate);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCOtherOption);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCLoanOriginate);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCLoanTerm);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCInterestRateBased).trigger('change');
            $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(SemiAnnualHELOCDetailsList.semiAnnualHELOCOtherOption);
            $('#SemiAnnualAddHELOCBtnnew').addClass('edit-form');
            $('#SemiAnnualAddHELOCBtnnew').attr('data-index', index);
        });

        $(document).on('click', '.SemiAnnualAddHELOCBtn', function () {
            $('#SemiAnnualAddHELOCBtnnew').removeClass('edit-form');
            $('#SemiAnnualHELOCModal input[type="text"], input[type="number"]').val('');
            $('#SemiAnnualHELOCModal select').val('').trigger('change');
            $('#SemiAnnualHELOCModal .error-alert').remove();
            $('#SemiAnnualHELOCModal #step-1').show();
            $('#SemiAnnualHELOCModal #step-2').hide();
        });

        $(document).on('click', '#SemiAnnual-HELOCInfo .fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                SemiAnnualHELOCDetails.splice(index_id, 1);
                $("#SemiAnnual-HELOCInfo tbody").empty();
                if (SemiAnnualHELOCDetails.length != 0) {
                    var tr = '';
                    $.each(SemiAnnualHELOCDetails, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.semiAnnualHELOCPropertyName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#SemiAnnual-HELOCInfo tbody").html(tr);
                }


                $('#equityLineOfCreditDetails').html(JSON.stringify(SemiAnnualHELOCDetails));
                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '5') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });

        if ($('#helocothers').val() === '5') {
            $('.helocinterestrateothers').show();
        } else {
            $('.helocinterestrateothers').hide();
        }
    });

</script>
@stop