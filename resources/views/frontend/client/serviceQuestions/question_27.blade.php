<style type="text/css">
    .fileUploadRow{border-bottom:1px solid rgb(187, 179, 179) !important}
</style>
<div class="w100per ">
    <div class="col-md-4 col-xs-12 section-left">  
        <p style="margin-bottom:0; text-transform: uppercase;"> Compensation Documents</p>
        <h2>Upload your executive compensation documents.</h2>
        <p style="margin-bottom:0;">Upload statements of all accounts for Executive Compensation. This may include : </p>
        <ul style="font-size: 13px; padding-left: 25px;">
            <li>Stock Option</li>
            <li>Deferred Comp Plan</li>
            <li> etc.</li>
        </ul>
    </div>
    <div class="col-xs-12 col-sm-6 col-sm-offset-1 section-right section-size">
        <div class='add-file'>
            <table style="width:100%; ">
                <thead>
                    <tr>
                        <td class="col-xs-6 col-sm-6 paddingLeft0" style="padding-top: 10px; padding-bottom: 10px;">File Name</td>
                        <td class="col-xs-1 col-sm-4 paddingLeft0" style="padding-top: 10px; padding-bottom: 10px;">&nbsp;</td>
                        <td  class="col-xs-4 col-sm-4 paddingLeft0" style="text-align:right; padding-right: 23px;">Actions</td>
                    </tr>
                </thead>
                <tbody class="add-input">
                    <tr  style="border:0"> 
                        <td class="new" colspan="3" style="padding:0">
                            <?php
                            if (!empty($answer) && array_key_exists('files', $answer)) {
                                foreach ($answer["files"] as $key => $file) {
                                    ?>
                            <tr  style="border-bottom:rgb(187, 179, 179) solid 1px" class="fileUploadRow">
                                <td class="col-sm-5 col-xs-4 paddingLeft0"> 
                                    <input name="answer[27][files][{{$key}}][file_name]" class="save" readonly="readonly" type="text" value='<?php if (array_key_exists('file_name', $file)) echo $file["file_name"]; ?>'>
                                    <input name="answer[27][files][{{$key}}][file_path]" type="hidden" value='<?php if (array_key_exists('file_path', $file)) echo $file["file_path"]; ?>'>
                                </td>
                                <td class="col-sm-5 col-xs-5 paddingLeft0"> 
                                    &nbsp;
                                </td>
                                <td class="col-sm-2 col-xs-3">
                                    <span class="pull-right"> 
                                        <i class="fa fa-check fileSave" aria-hidden="true" id="saveFile"></i> 
                                        <i title="Edit" class="fa fa-pencil editUploadRow" style="display:none;" aria-hidden="true"></i> 
                                        <i style="display:none;" class="fa fa-trash deleteUploadRow" aria-hidden="true"></i> 
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        ?>
                    <input data-attr="0"  name="answer[27][file_name]" readonly="readonly" type="hidden" value=''>
                <?php } ?>

                </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!--{{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
        <label class="radio-custom-label"> 
            {{ Form::input('hidden','questionName[27]','Upload your executive compensation documents.') }}
        </label>
        <div class="col-sm-12 save-file-section paddingLeft0" style="margin-top:0">
            <div class="col-sm-6 add-file-btn mt-20">
                <label for="files" class="btn btn-primary open_button" style="background:none;">Add File</label>
                <label for="files" class="btn btn-primary uploaderBtn no-action" style="background:none; display: none">Add File</label>
            </div>
        </div>



    </div>
</div>

<!--<script src="{{ asset('js/annual-portfolio-review.js') }}"></script>  -->

