<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name">OTHER ACCOUNTS</p>
        <h2>Cash on Hand</h2>
        <p>Aside from what will be seen on any statements uploaded, what is the value of cash on hand that is NOT going to be put into this investment account?</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Cash on hand?') }}
            <i class="fa fa-dollar" aria-hidden="true"></i>
            {{ Form::input('text','data[115][answer][Cash on hand]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
        </div>
    </div>
</div>