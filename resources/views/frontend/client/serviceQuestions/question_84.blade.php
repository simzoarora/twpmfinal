<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    .add-retirement-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .retirementInfo{
        position: relative;
    }
    .filename span{font-size: 14px !important}
</style>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',11) }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>We need to get a better understanding of your income tax profile.</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[84]','We need to get a better understanding of your income tax profile') }}
                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Filling status') }}
                                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 102px; z-index: 1; top: 42px; font-size: 16px;"></i>
                                       
                                        <select style="position: absolute;" class="incometax_fillingstatus" name="answer[84][filling_status]" style="display: none;" readonly="true"><option selected disabled value="">SELECT</option><option value="1"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 1) {
                                                echo "selected";
                                            }
                                            ?>>Single</option>
                                            <option value="2"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 2) {
                                                echo "selected";
                                            }
                                            ?>>Head of household</option>
                                            <option value="3"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 3) {
                                                echo "selected";
                                            }
                                            ?>>Qualifying Widow(er) with Dependent Child</option>
                                            <option value="4"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 4) {
                                                echo "selected";
                                            }
                                            ?>>Married filling jointly</option>
                                            <option value="5"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 5) {
                                                echo "selected";
                                            }
                                            ?>>Married Filing Separately</option>
                                            <option value="5"
                                            <?php
                                            if ($defaultData['taxFillingStatus'] == 6) {
                                                echo "selected";
                                            }
                                            ?>>Married Filing Separately (living apart)</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Deductions</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[841]','Deductions') }}
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label', 'Schedule A Deductions') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                            <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control deductions" data-rule-regex="false" required="" placeholder="" name="answer[841][deductions]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('deductions', $answer)) {
                                                echo $answer["deductions"];
                                            }
                                            ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Upload last year's return</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[842]','Upload last year\'s return') }}
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label', 'Click below to upload current investments and investment choices (highly recommended if available).') }}

                                        <div class="col-sm-12 paddingLeft0 inner-left paddingRight0 Upload-last-year-statement js">
                                            <input type="file" name="budget_statement_info" id="file-21" class="inputfile inputfile-2" onclick="this.value=null;"/>
                                            
                                            <!--data-multiple-caption="{count} files selected" multiple-->
                                            <label for="file-21" class="btn upload-statment upload-statement-click <?php
                                            if (!empty($answer["files"][0]) && array_key_exists('file_name', $answer["files"][0]) && $answer["files"][0]['file_name'] != "") {
                                                echo "hide";
                                            }
                                            ?>" style="color: #0991e1; border: 1px solid #0991e1; width: 45%;">
                                                <span style="font-size: 0"></span>Upload
                                            </label>

                                            <label  class="btn upload-statment-noclick <?php
                                            if (!empty($answer["files"][0]) && array_key_exists('file_name', $answer["files"][0]) && $answer["files"][0]['file_name'] != "") {
                                                echo "";
                                            } else {
                                                echo "hide";
                                            }
                                            ?>" style="color: #0991e1; border: 1px solid #0991e1; width: 45%; cursor: not-allowed;" >
                                                Upload
                                            </label>

                                            <div class="upload-last-year-info <?php
                                            if (!empty($answer["files"][0]) && array_key_exists('file_name', $answer["files"][0]) && $answer["files"][0]['file_name'] != "") {
                                                echo "";
                                            } else {
                                                echo "hide";
                                            }
                                            ?>">
                                                <div class="col-sm-12 row inner-left">
                                                    <table>
                                                        <thead>
                                                            <tr>
                                                                <td style="font-size: 13px;">Documents</td>

                                                                <td></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr style="border-bottom:0">
                                                                <td><span class="filename"><?php
                                                                        if (!empty($answer["files"][0]) && array_key_exists('file_path', $answer["files"][0]))
                                                                            echo $answer['files'][0]['file_name'];
                                                                        ?></span>
                                                                    <input type="hidden" name="answer[842][files][0][file_name]" id="uploadFileName" value="<?php
                                                                        if (!empty($answer["files"][0]) && array_key_exists('file_name', $answer["files"][0]))
                                                                            echo $answer["files"][0]['file_name'];
                                                                        ?>">
                                                                    <input type="hidden" name="answer[842][files][0][file_path]" value="<?php 
                                                                    if (!empty($answer["files"][0])&& array_key_exists('file_path', $answer["files"][0])  ) {
                                                                       echo $answer['files'][0]['file_path'];
                                                                    }
                                                                    ?>" id="uploadFilePath">
                                                                   
                                                                    
                                                                </td>
                                                                <td style="text-align:right;"><i title="Delete" class="fa fa-trash cursor-pointer"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<textarea id="educationalList"class="hidden" name="answer[842][educationalList]">{{(!empty($answer) && array_key_exists('educationalList',$answer))? $answer["educationalList"]:null}}  </textarea>-->
                                    </div>
                                </div>
                            </div>
                        </section> 
                    </fieldset>

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Do you usually pay extra tax or get a refund?</h2>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[843]','Do you usually pay extra tax or get a refund') }}
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label', 'Do you typically:') }}
                                        <div class="tax-refund">
                                            <div class="OptionSelection paddingLeft0 col-xs-6 col-sm-5 <?php
                                            if (!empty($answer) && array_key_exists('tax_refund_type', $answer) && $answer['tax_refund_type'] == 'owesingle') {
                                                echo "married-selected";
                                            }
                                            ?>"  for="owe-single">
                                                <label for="owe-single" class="label">Owe  <br>additional</label>
                                                {{ Form::radio('answer[843][tax_refund_type]', 'owesingle',(!empty($answer) && array_key_exists('tax_refund_type',$answer)) ? (($answer['tax_refund_type']=="owesingle")  ? true : false) :false,['class'=>'select-tax-refund',  'id' => 'owe-single', ' required' => true]) }}
                                            </div>
                                            <div class="OptionSelection paddingLeft0 col-xs-6 col-sm-5 <?php
                                            if (!empty($answer) && array_key_exists('tax_refund_type', $answer) && $answer['tax_refund_type'] == 'getrefund') {
                                                echo "married-selected";
                                            }
                                            ?>"  for="get-refund" >
                                                <label for="get-refund" class="label"> Get a<br> refund </label>
                                                {{ Form::radio('answer[843][tax_refund_type]', 'getrefund',(!empty($answer) && array_key_exists('tax_refund_type',$answer)) ? (($answer['tax_refund_type']=="getrefund")  ? true : false) :false,['class'=>'select-tax-refund',  'id' => 'get-refund', ' required'=>true]) }}
                                            </div>
                                        </div>

                                        <div class="col-sm-6 paddingLeft0 inner-left refund-amount" style="display: <?php
                                        if (!empty($answer) && array_key_exists('tax_refund_type', $answer) && $answer['tax_refund_type'] != "") {
                                            echo "block";
                                        } else {
                                            echo "none";
                                        }
                                        ?>;">
                                            {{ Form::label('label', 'What amount, usually?') }}
                                             <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                            <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control deductions" data-rule-regex="false" required="" placeholder="" name="answer[843][what_amount]" type="number" aria-required="true" value="<?php
                                            if (!empty($answer) && array_key_exists('what_amount', $answer)) {
                                                echo $answer["what_amount"];
                                            }
                                            ?>">
                                             </div>
                                        </div>
                                    </div>
                                </div>
                        </section>
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Carry forward losses</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[844]','Carry forward losses') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you have carry forward losses?</label>
                                        <label class="radio-custom-label">
                                            <input class="carry-forward-losses" required="required" name="answer[844][have_carry_forward_losses]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('have_carry_forward_losses', $answer) && ($answer['have_carry_forward_losses'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="carry-forward-losses" required="required" name="answer[844][have_carry_forward_losses]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('have_carry_forward_losses', $answer) && ($answer['have_carry_forward_losses'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left carry-forward-amount" style="display: <?php
                                    if (!empty($answer) && array_key_exists('have_carry_forward_losses', $answer) && ($answer['have_carry_forward_losses'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>">
                                        {{ Form::label('label', 'What amount?') }}
                                        <div class="service-input-group input-group">
                                            <span class="input-group-addon"> $ </span>
                                        <input data-validation="" class="borderLeft0 comprehensive-width custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="answer[844][carry_forward_losses_amount]" type="number" aria-required="true" value="<?php
                                        if (!empty($answer) && array_key_exists('carry_forward_losses_amount', $answer)) {
                                            echo $answer["carry_forward_losses_amount"];
                                        }
                                        ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>


                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Backup withholding</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right ">
                                    {{ Form::input('hidden','questionName[845]','Backup withholding') }}
                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Are you subject to any backup withholding of taxes?</label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[845][subject_to_backup_withholding_taxes]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('subject_to_backup_withholding_taxes', $answer) && ($answer['subject_to_backup_withholding_taxes'] == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="" required="required" name="answer[845][subject_to_backup_withholding_taxes]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('subject_to_backup_withholding_taxes', $answer) && ($answer['subject_to_backup_withholding_taxes'] == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0; font-size: 12px;">INCOME TAX</p>
                                    <h2>Special situations</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[846]','Special situations') }}
                                    <div class="col-sm-6 inner-left special_tax_situations">
                                        {{ Form::label('label', 'Describe any special tax situations.') }}
                                        <textarea style="width: 78%; border-radius: 0;" data-validation="" rows="4"  class="custom-validation form-control " data-rule-regex="false" required="" placeholder="explain" name="answer[846][special_tax_situations]" type="text" aria-required="true">{{(!empty($answer) && array_key_exists('special_tax_situations',$answer)) ? $answer["special_tax_situations"] :"" }}</textarea>

                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id,])}}";
    $(document).ready(function () {
        $('.incometax_fillingstatus').prop('disabled', true);
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        $("select").selectBoxIt();
        $('.tax-refund-type').on('click', function () {
            $(this).addClass('refund-selected').siblings().removeClass('refund-selected');
            consoloe.log('bbbb');
            $('#refundType').val($(this).attr('data-val'));
            if ($(this).hasClass('refund-selected')) {
                $('.refund-amount').show();
                $('.refund-amount input').val('');
            } else {
                $('.refund-amount').else();
            }
        });
        $('.carry-forward-losses').on('change', function () {
            if ($(this).val() == 'yes') {
                $('.carry-forward-amount, .carry-forward-amount input').show();
            } else {
                $('.carry-forward-amount, .carry-forward-amount input').hide().val('');
            }

        });
//        file upload
//        var filesComprehensive = [];
//        educationalList = [];
        $(document).each(function () {
            $(document).on('change', '.Upload-last-year-statement input[type=file]', function () {
                if ($('.Upload-last-year-statement .upload-statment span').text().length > 0) {
                    $('.Upload-last-year-statement .upload-last-year-info').removeClass('hide');
                    $('.Upload-last-year-statement .upload-last-year-info table tbody').html('<tr style="border-bottom:0"><td><span class="filename"></span><input type="hidden" name="answer[842][files][0][file_name]" id="uploadFileName"><input type="hidden" name="answer[842][files][0][file_path]" id="uploadFilePath"> </td></td>   <td style="text-align:right;"><i title="Delete" class="fa fa-trash cursor-pointer"></td></tr>');
                    $(".upload-last-year-info .filename").text($('.Upload-last-year-statement .upload-statment span').text());
                    $(".Upload-last-year-statement .upload-statement-click").addClass('hide');
                    $(".Upload-last-year-statement .upload-statment-noclick ").removeClass('hide').css('cursor', 'not-allowed');
                    $('#uploadFileName').val($('.Upload-last-year-statement .upload-statment span').text());
                    var UploadLastYearStatementInfo = new FormData();
                    UploadLastYearStatementInfo.append('file', $('[name=budget_statement_info]')[0].files[0]);
                    $.ajax({
                        type: "post",
                        url: ajaxUploadDocument,
                        async: true,
                        data: UploadLastYearStatementInfo,
                        cache: false,
                        contentType: false,
                        processData: false,
                        timeout: 60000,
                        success: function (response) {
                            $("#uploadFilePath").val(response.message);
//                             filesComprehensive.push(
//                            {
//                                "key": "files", "file_name": $('#uploadFileName').val()
//                                , "file_path": $('#uploadFilePath').val()  
//                            }
//                    );
//                    educationalList.push({
//                filesComprehensive : filesComprehensive
//            });
//             $('#educationalList').html(JSON.stringify(educationalList));
                        },
                        xhr: function () {
                            var myXhr = $.ajaxSettings.xhr();
                            return myXhr;
                        }

                    });

                }
            });
        });
        $('.Upload-last-year-statement table').on('click', '.fa-trash', function () {

            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this file!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                    .then((willDelete) => {
                        if (willDelete) {
                            $(this).closest('tr').remove();
                            $('#uploadFileName').val('');
                            $('#uploadFilePath').val('');
                            $('.upload-last-year-info').addClass('hide');
                            $(".Upload-last-year-statement .upload-statement-click").removeClass('hide');
                            $(".Upload-last-year-statement .upload-statment-noclick").addClass('hide').css('cursor', 'pointer');
                            $('.Upload-last-year-statement .upload-statment span').text('');
                            swal("Your file has been deleted!", {
                                icon: "success",
                            });
                        } else {
                            swal("Your file is safe!");
                        }
                    });
        });

        'use strict';
        ;
        (function (document, window, index)
        {
            var inputs = document.querySelectorAll('.inputfile');
            Array.prototype.forEach.call(inputs, function (input)
            {
                var label = input.nextElementSibling,
                        labelVal = label.innerHTML;

                input.addEventListener('change', function (e)
                {
                    var fileName = '';
                    if (this.files && this.files.length > 1)
                        fileName = (this.getAttribute('data-multiple-caption') || '').replace('{count}', this.files.length);
                    else
                        fileName = e.target.value.split('\\').pop();

                    if (fileName)
                        label.querySelector('span').innerHTML = fileName;
                    else
                        label.innerHTML = labelVal;
                });

                // Firefox bug fix
                input.addEventListener('focus', function () {
                    input.classList.add('has-focus');
                });
                input.addEventListener('blur', function () {
                    input.classList.remove('has-focus');
                });
            });
        }(document, window, 0));




        $(document).on('change', '.select-tax-refund', function () {
            $('.refund-amount').show();
            if ($(this).val() === 'owesingle') {
                $('div[for=owe-single]').addClass('married-selected');
                $('.refund-amount').show();
            } else {
                $('div[for=owe-single]').removeClass('married-selected');

            }
        });

        $(document).on('change', '.select-tax-refund', function () {
            $('.refund-amount').show();
            if ($(this).val() === 'getrefund') {
                $('div[for=get-refund]').addClass('married-selected');

            } else {
                $('div[for=get-refund]').removeClass('married-selected');

            }
        });

    });

</script>
<style type="text/css">
    .wrapper .sections .section-right .inner-left .OptionSelection label{height: auto; line-height: normal; padding: 20px 10px;}
    .OptionSelection.married-selected label{background-color: #2079ee; color: #fff;}
</style>
@stop
<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script>