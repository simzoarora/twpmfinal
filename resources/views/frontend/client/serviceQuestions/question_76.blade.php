                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     <style>
    
    table {
        width:100%;
        word-break: break-all;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    /*    table tbody td .fa-pencil {
            position: absolute;
            right: 17px;
            top: 20px;
        }*/
    .add-education-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .educationAccountInfo{
        position: relative;
    }
    .wrapper .sections .modal-content .section-right .inner-left input[type=number].estimated-start-year{text-align: left}
    .wrapper .sections .modal-content .section-right .inner-left input[type=number].estimated-start-year:focus{text-align: right}
</style>
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>Assets</h6>
        <h2>Education & Custodial Accounts</h2>
        <p>Please answer the following questions, so we can get the most accurate picture of your finances. If you have questions, feel free to <a href="#">contact us</a>.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[76]','Education & Custodial Accounts') }} 

        <div class="col-sm-6 inner-left ">
            <label for="label">Do you have any education or custodial accounts for children, grandchildren or others?</label>
            <label class="radio-custom-label">
                <input class="education-button table-confirmation" required="required" name="answer[76][educational_or_custodial_account]" type="radio" value="yes" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('educational_or_custodial_account', $answer) && ($answer['educational_or_custodial_account'] == "yes")) {
                    echo "checked";
                }
                ?>>Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                <input class="education-button table-confirmation" required="required" name="answer[76][educational_or_custodial_account]" type="radio" value="no" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('educational_or_custodial_account', $answer) && ($answer['educational_or_custodial_account'] == "no")) {
                    echo "checked";
                }
                ?>>No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-6 inner-left education-table "  style="display:<?php
        if (!empty($answer) && array_key_exists('educational_or_custodial_account', $answer) && ($answer['educational_or_custodial_account'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>">

            {{ Form::label('label', 'List all educational & custodial accounts.') }}

            <table id='educationAccountInfo' class="find-table-length">
                <thead>
                    <tr>
                        <td style="font-size: 13px;">Account name</td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($answer) && array_key_exists('educationalAccountRecords', $answer)) {
                        $account = json_decode($answer["educationalAccountRecords"]);
                        if (!empty($account)) {
                            foreach ($account as $key => $data) {
                                ?>
                                <tr data-index="{{$key}}">
                                    <td >{{$data->educationalAccountName}}</td>
                                    <td></td>
                                    <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
            <button type='button' class="add-education-account" data-toggle="modal" data-target="#eductaionAccountModal">Add account</button>
        </div>
    </div>
    <textarea id="educationalAccountDetails" class="hidden" name="answer[76][educationalAccountRecords]">{{(!empty($answer) && array_key_exists('educationalAccountRecords',$answer))? $answer["educationalAccountRecords"]:null}}</textarea>
</div>
<!-- new modal starts -->
<div class="sections">
    <!-- modal dialog starts -->
    <div id="eductaionAccountModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD EDUCATIONAL AND CUSTODIAL ACCOUNT</h4>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <div class="row">
                        <!-- first step starts -->
                        <div class=" setup-content">
                            <div class="section-right">
                                <div class="validation-alert">

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Give the account a name') }}
                                        {{ Form::input('text','eca_account_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control educational-account-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Who funded this account?') }}
                                        <i class="fa fa-angle-down selectArrow"></i>
                                        <select style="position: absolute;" style='width:40%;' class="whose-education-account" name="eca_Who_founded_this_account" style="display: none;" required>
                                            <option selected disabled value="">SELECT</option>
                                            <option value="1">{{explode(' ', Session::get('loggedInUserName'))[0] }}</option>
                                            <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value="2"><?php echo $spouse_name ;?></option><?php } ?>
                                            <option value="3">other</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-12 inner-left education-accountholder-name" style="display:none;">
                                        {{ Form::label('label','Accountholder name') }}
                                        {{ Form::input('text','eca_Accountholder_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control education-accountholder-input', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'accountholder name', 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left  js ret-upload-statement">
                                        {{ Form::label('label','Upload a recent statement(highly recommended).') }}
                                        <input required type="file" name="file" id="file-3" onclick="this.value=null;" class="inputfile inputfile-3" data-multiple-caption="{count} files selected" />
                                        <label for="file-3" class="upload-statment ret-upload-statement-click" style="width: auto !important; font-size:13px; padding: 3px 20px; border-radius: 4px;"> 
                                            <span style="font-size: 0"></span>Upload statement
                                        </label>
                                        <label class="upload-statment upload-statment-noclick hide" style="width: auto !important; font-size:13px; padding: 3px 20px; border-radius: 4px; font-weight: 700"> 
                                            Upload statement
                                        </label>
                                        <div class="statement-info hide "> 
                                            <div class="col-sm-12 row inner-left" style="display:block">
                                                <table>
                                                    <thead>
                                                        <tr>
                                                            <td style="font-size: 13px;">Documents</td>

                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>

                                        <!--                                        <div style="position: relative">
                                                                                    <label for="retirement-statement" style="width: auto !important; font-size: 13px; padding: 3px 20px; border-radius: 4px;" class="btn upload-statment">Upload statement</label>
                                                                                    <input id="retirement-statement"  style="-webkit-appearance: none;  opacity: 0; position: absolute;" typen="file">
                                                                                </div>-->
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        <label for="label">Is this account earmarked to pay for education?</label>
                                        <label class="radio-custom-label">
                                            <input class="earmarked-to-pay"  required="required"  aria-required="true"  name="eca_earmarked_to_pay_for_education" type="radio" value="yes">Yes
                                            <span class="radio-icon"></span> 
                                        </label> 
                                        <label class="radio-custom-label">
                                            <input class="earmarked-to-pay"  required="required"  aria-required="true"  name="eca_earmarked_to_pay_for_education" type="radio" value="no">No 
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>

                                    <div class="education-hide-section" style="display:none;">
                                        <div class="col-sm-12 inner-left " >
                                            {{ Form::label('label','Whose education?') }}
                                            {{ Form::input('text','eca_Whose_education',null,['data-validation'=> '' , 'class'=>'custom-validation form-control whose-education', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'student name', 'required'=>'required']) }}
                                        </div>

                                        <div class="col-sm-12 inner-left " >
                                            {{ Form::label('label','Estimated start year') }}
                                            {{ Form::input('number','eca_Estimated_start_year',null,['data-validation'=> '' , 'class'=>'custom-validation form-control  estimated-start-year', 'data-rule-regex' =>"false", 'required'=>true , 'value'=>'2018', 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="funds-detail-section" style="display:none;">
                                        <div class="col-sm-12 inner-left " >
                                            {{ Form::label('label','For whom are the funds to be used?') }}
                                            {{ Form::input('text','eca_For_whom_are_the_funds_to_be_used',null,['data-validation'=> '' , 'class'=>'custom-validation form-control funds', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'name', 'required'=>'required']) }}
                                        </div>
                                        <div class="col-sm-12 inner-left " >
                                            {{ Form::label('label','For what purpose are the funds to be used?') }}
                                            {{ Form::input('text','eca_Forwhatpurposearethefundstobeused',null,['data-validation'=> '' , 'class'=>'custom-validation form-control purpose-of-funds', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'description', 'required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        <button id='eductaionAccountForm'  class="modal-button eductaion-finishBtn"  type="button" style="border-radius: 3px; ">
                                            Save
                                        </button>
                                    </div> 

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- modal dialog finish -->
</div>
<!-- new modal finish -->

<script>
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
</script> 
