<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Durable Power of Attorney</h2>
        <p>It's never to early to consider your legacy. The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[216]','Durable Power of Attorney') }}
        <div class="col-sm-6 inner-left ">
            {{ Form::label('label', 'Does somone have a durable power of attorney for your spouse/partner?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[216][spouse-power-of-attorney]', 'yes',(!empty($answer) && array_key_exists('spouse-power-of-attorney',$answer)) ? (($answer['spouse-power-of-attorney']=="yes")  ? true : false):false,['class'=>'power-of-attorney' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>                                                                                                                       
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[216][spouse-power-of-attorney]', 'no',(!empty($answer) && array_key_exists('spouse-power-of-attorney',$answer)) ? (($answer['spouse-power-of-attorney']=="no")  ? true : false):false,['class'=>'power-of-attorney' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-12 inner-left spouse-durable-power-of-attorney" style="display:  <?php
        if (!empty($answer) && array_key_exists('spouse-power-of-attorney', $answer) && ($answer['spouse-power-of-attorney'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>;">
            {{ Form::label('label','Who is the Power of Attorney?') }}
            <i class="fa fa-angle-down" style="position: absolute; top: 45px; left: 251px; z-index: 10;"></i> 
                <select style="position: absolute;" class="business-type" name="answer[216][spouse_power_of_Attorney]" required="" aria-invalid="false">
                    <option selected="" disabled="" value="">Select</option>
                    <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                    <option value="1" <?php
                    if (array_key_exists('spouse_power_of_Attorney', $answer) && ($answer['spouse_power_of_Attorney'] == 1))
                    echo "selected";
                    ?>>Spouse</option>
                    <?php } ?>
                    <option value="2" <?php
                    if (array_key_exists('spouse_power_of_Attorney', $answer) && ($answer['spouse_power_of_Attorney'] == 2))
                    echo "selected";
                    ?>>Other</option>
                </select>
            </select>
        </div> 

        <div class="col-sm-6 inner-left spouse-attorney-their-name" style="display: <?php
             if (!empty($answer) && array_key_exists('spouse_power_of_Attorney', $answer) && ($answer['spouse_power_of_Attorney'] == 2)) {
                 echo 'block';
             } else {
                 echo 'none';
             }
             ?>">
            {{ Form::label('label', 'Their name') }}
            {{ Form::input('text','answer[216][spouse-their-name]',(!empty($answer) && array_key_exists('spouse-their-name',$answer)) ? $answer['spouse-their-name'] :null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Name of Power of Attorney']) }}
        </div>
    </div>
</div>