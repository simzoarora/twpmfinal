@extends('frontend.layouts.client')
@section('title')
@stop
@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',25) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Other non-business related debts.</h2>
                                    <p></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[83]','Do you have any other non-business related debts?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any other non-business related debts?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[83][CPMODConfirmation]', 'yes',(!empty($answer) && array_key_exists('CPMODConfirmation',$answer)) ? (($answer['CPMODConfirmation']=="yes")  ? true : false):false,['class'=>'CPMODConfirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[83][CPMODConfirmation]', 'no',(!empty($answer) && array_key_exists('CPMODConfirmation',$answer)) ? (($answer['CPMODConfirmation']=="no")  ? true : false):false,['class'=>'CPMODConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnualODDetailTable" style="display:<?php
                                    if (!empty($answer) && array_key_exists('CPMODConfirmation', $answer) && ($answer['CPMODConfirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <label> List other debts.</label>

                                                        <table id="CPMOdInfo" class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Item</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["nonBusinessRelatedDebtsRecords"])) {
                                                                    $loans = json_decode($answer["nonBusinessRelatedDebtsRecords"]);
                                                                    if (!empty($loans)) {
                                                                        foreach ($loans as $key => $data) {
                                                                            ?>
                                                                            <tr class="SemiAnnualCardInfo" data-index="{{$key}}">
                                                                                <td class="">{{$data->CPMODDebtNickname}}</td>
                                                                                <td style="text-align:right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="CPMODBtn add-dependent" style="cursor:pointer; background: transparent; border: 1px solid #048cdc; color: #018aff; margin-top: 24px; border-radius: 3px;" data-toggle="modal" data-target="#CPMOdModal">Add debt</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="CPMnonBusinessRelatedDebtsDetails" class="hidden" name="answer[83][nonBusinessRelatedDebtsRecords]">{{(!empty($answer["nonBusinessRelatedDebtsRecords"]))? $answer["nonBusinessRelatedDebtsRecords"]:null}}</textarea>
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="CPMOdModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD NON-BUSINESS DEBT</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class=" section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this debt a nickname') }}
                                                                    {{ Form::input('text','CPM-od-debt-nickname',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODDebtNickname', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this debt?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="status valid CPMODOnDebtName"  id="CPMODothers" name="CPM_od_on_debt_name" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                         <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php    if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                                        <option value="2">{{$spouse_name}}</option>
                                                                        <?php } ?>
                                                                        <option value="4">Other</option>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12 inner-left semiAnnualOdOtherNames" style="display: none;">
                                                                    {{ Form::label('label', 'Their Name?') }}
                                                                    {{ Form::input('text','CPMOD_on_debt_other_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODOnDebtOtherName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'For what were the proceeds of this loan used?') }}
                                                                    {{ Form::input('text','CPMODLoanUsedProceed',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODLoanUsedProceed', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->
                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Who is the lender?') }}
                                                                    {{ Form::input('text','lender',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODLender', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'When was this loan originated?') }}
                                                                    {{ Form::input('text','loan_originated',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker CPMODConsolidationLoanDate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the term of loan in months?') }}
                                                                    {{ Form::input('number','term_of_loan',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the rate of interest?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','rate_of_interest',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODInterestRate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- second step finsih -->
                                                        <!-- third step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class=" section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What was the original loan amount?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','original_loan_amount]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODOriginalLoanAmount borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','current_balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODCurrentBalance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','monthly_payment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODMinMonthlyPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','pay_per_month',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODPayPerMonth borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- third step finsih -->

                                                        <!-- fourth step starts --> 
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a revolving loan?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'yes',false,['class'=>'CPMODRevolvingLoan','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('revolving_loan', 'no',false,['class'=>'CPMODRevolvingLoan','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is there collateral pledged against this loan (i.e. house, car, etc.)?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'yes',false,['class'=>'CPMODCollateralPledge','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('pledged_against_loan', 'no',false,['class'=>'CPMODCollateralPledge','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CPMODCollateralDescribeBox" style="display: none">
                                                                    {{ Form::label('label', 'Describe the collateral') }}
                                                                    <!--<div class="input-group">-->
<!--                                                                        <span class="input-group-addon">
                                                                            <i class="fa fa-dollar"></i>    
                                                                        </span>-->
                                                                    {{ Form::textarea('collateral_description',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMODCollateralDescribe ','cols'=>'4','rows'=>'3', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    <!--</div>-->
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <!--id='SemiAnnualAddCardBtn'-->  
                                                                    <button  id="CPMODBtnNew" class='finishBtn semiAnnualCCBT pull-right' type="button">
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- fourth step finsih -->
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal finsih -->
<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var CPMODDetailsList = [];
        if ($('#CPMnonBusinessRelatedDebtsDetails').val() != '') {
            CPMODDetailsList = JSON.parse($('#CPMnonBusinessRelatedDebtsDetails').val());
        }
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        $('#CPMOdModal').modal('hide');
                        CPMODDetailsList[$(this).attr('data-index')] = {
                            CPMODDebtNickname: $('#CPMOdModal .CPMODDebtNickname').val(),
                            CPMODOnDebtName: $('#CPMOdModal .CPMODOnDebtName').val(),
                            CPMODOnDebtOtherName: $('#CPMOdModal .CPMODOnDebtOtherName').val(),
                            CPMODLoanUsedProceed: $('#CPMOdModal .CPMODLoanUsedProceed').val(),
                            
                        
                        CPMODLender: $('#CPMOdModal .CPMODLender').val(),
                            CPMODConsolidationLoanDate: $('#CPMOdModal .CPMODConsolidationLoanDate').val(),
                            CPMODLoanTerm: $('#CPMOdModal .CPMODLoanTerm').val(),
                            CPMODInterestRate: $('#CPMOdModal .CPMODInterestRate').val(),
                            
                            CPMODOriginalLoanAmount: $('#CPMOdModal .CPMODOriginalLoanAmount').val(),
                            CPMODCurrentBalance: $('#CPMOdModal .CPMODCurrentBalance').val(),
                            CPMODMinMonthlyPayment: $('#CPMOdModal .CPMODMinMonthlyPayment').val(),
                            CPMODPayPerMonth: $('#CPMOdModal .CPMODPayPerMonth').val(),
                            CPMODRevolvingLoan: $('#CPMOdModal .CPMODRevolvingLoan:checked').val(),
                            CPMODCollateralPledge: $('#CPMOdModal .CPMODCollateralPledge:checked').val(),
                            CPMODCollateralDescribe: $('#CPMOdModal .CPMODCollateralDescribe').val()
                        };
                        $('#CPMOdInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#CPMOdModal .CPMODDebtNickname').val() + '</td>  <td style="text-align:right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('#CPMOdInfo tbody').append('<tr data-index=' + CPMODDetailsList.length + '><td>' + $('#CPMOdModal .CPMODDebtNickname').val() + '</td>   <td style="text-align:right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        CPMODDetailsList.push({
                            CPMODDebtNickname: $('#CPMOdModal .CPMODDebtNickname').val(),
                            CPMODOnDebtName: $('#CPMOdModal .CPMODOnDebtName').val(),
                            CPMODOnDebtOtherName: $('#CPMOdModal .CPMODOnDebtOtherName').val(),
                            CPMODLoanUsedProceed: $('#CPMOdModal .CPMODLoanUsedProceed').val(),
                            CPMODLender: $('#CPMOdModal .CPMODLender').val(),
                            CPMODConsolidationLoanDate: $('#CPMOdModal .CPMODConsolidationLoanDate').val(),
                            CPMODLoanTerm: $('#CPMOdModal .CPMODLoanTerm').val(),
                            CPMODInterestRate: $('#CPMOdModal .CPMODInterestRate').val(),
                            CPMODOriginalLoanAmount: $('#CPMOdModal .CPMODOriginalLoanAmount').val(),
                            CPMODCurrentBalance: $('#CPMOdModal .CPMODCurrentBalance').val(),
                            CPMODMinMonthlyPayment: $('#CPMOdModal .CPMODMinMonthlyPayment').val(),
                            CPMODPayPerMonth: $('#CPMOdModal .CPMODPayPerMonth').val(),
                            CPMODRevolvingLoan: $('#CPMOdModal .CPMODRevolvingLoan:checked').val(),
                            CPMODCollateralPledge: $('#CPMOdModal .CPMODCollateralPledge:checked').val(),
                            CPMODCollateralDescribe: $('#CPMOdModal .CPMODCollateralDescribe').val()
                        });
                    }
                    $('#CPMOdModal').modal('hide');
                    $('#CPMnonBusinessRelatedDebtsDetails').html(JSON.stringify(CPMODDetailsList));
                }
            }
            return false;
        });

        $(document).on('click', '#CPMOdInfo .fa-pencil', function () {
            $('#CPMOdModal').modal();
            $('#CPMOdModal #step-1').show();
            $('#CPMOdModal #step-2, #CPMOdModal #step-3').css('display', 'none');
            $('#CPMOdModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            CPMODDetails = CPMODDetailsList[index];
            $('#CPMOdModal .CPMODDebtNickname').val(CPMODDetails.CPMODDebtNickname);
            $('#CPMOdModal .CPMODOnDebtName').val(CPMODDetails.CPMODOnDebtName).trigger('change');
            $('#CPMOdModal .CPMODOnDebtOtherName').val(CPMODDetails.CPMODOnDebtOtherName);
            $('#CPMOdModal .CPMODLoanUsedProceed').val(CPMODDetails.CPMODLoanUsedProceed);

            $('#CPMOdModal .CPMODLender').val(CPMODDetails.CPMODLender);
            $('#CPMOdModal .CPMODConsolidationLoanDate').val(CPMODDetails.CPMODConsolidationLoanDate);
            $('#CPMOdModal .CPMODLoanTerm').val(CPMODDetails.CPMODLoanTerm);
            $('#CPMOdModal .CPMODInterestRate').val(CPMODDetails.CPMODInterestRate);

            $('#CPMOdModal .CPMODOriginalLoanAmount').val(CPMODDetails.CPMODOriginalLoanAmount);
            $('#CPMOdModal .CPMODCurrentBalance').val(CPMODDetails.CPMODCurrentBalance);
            $('#CPMOdModal .CPMODMinMonthlyPayment').val(CPMODDetails.CPMODMinMonthlyPayment);
            $('#CPMOdModal .CPMODPayPerMonth').val(CPMODDetails.CPMODPayPerMonth);

            $('#CPMOdModal .CPMODRevolvingLoan[value=' + CPMODDetails.CPMODRevolvingLoan + ']').prop('checked', true);
            $('#CPMOdModal .CPMODCollateralPledge[value=' + CPMODDetails.CPMODCollateralPledge + ']').prop('checked', true);
            $('#CPMOdModal .CPMODCollateralDescribe').val(CPMODDetails.CPMODCollateralDescribe);

            $('#CPMODBtnNew').addClass('edit-form');
            $('#CPMODBtnNew').attr('data-index', index);

            if ($('.CPMODCollateralPledge:checked').val() === 'yes') {
                $('.CPMODCollateralDescribeBox ').show();
            } else {
                $('.CPMODCollateralDescribeBox').hide();
            }
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('change', '.CPMODConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnualODDetailTable').show();
            } else {
                $('.SemiAnnualODDetailTable').hide();
                CPMODDetailsList = [];
                $('#CPMnonBusinessRelatedDebtsDetails').html(JSON.stringify(CPMODDetailsList));
            }
        });

        $(document).on('change', '.CPMODCollateralPledge', function () {
            if ($(this).val() === 'yes' || $(this).prop('checked', true).val() === 'yes') {
                $('.CPMODCollateralDescribeBox ').show();
            } else {
                $('.CPMODCollateralDescribeBox').hide();
            }
        });

        if ($('.CPMODCollateralPledge:checked').val() === 'yes') {
            $('.CPMODCollateralDescribeBox ').show();
        } else {
            $('.CPMODCollateralDescribeBox').hide();
        }

        $(document).on('click', '.CPMODBtn', function () {
            $('#CPMOdModal #step-1').show();
            $('#CPMOdModal #step-2, #CPMOdModal #step-3, #CPMOdModal #step-4, .CPMODCollateralDescribeBox').hide();
            $('#CPMOdModal input[type="text"], input[type="number"]').val('');
            $('#CPMOdModal input[type="radio"].CPMODRevolvingLoan, #CPMOdModal input[type="radio"].CPMODCollateralPledge').prop('checked', false);
            $('#CPMOdModal .CPMODOnDebtName').val('').trigger('change');
            $('#CPMOdModal input[type="radio"]').prop('checked', false);
            $('#CPMODBtnNew').removeClass('edit-form');
        });

        $(document).on('click', '#CPMOdInfo .fa-trash', function () { // <-- changes


            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);
            $('.swal-button--danger').click(function () {
                $(this).closest('tr').remove();
                CPMODDetailsList.splice(index_id, 1);
                $("#CPMOdInfo tbody").empty();
                if (CPMODDetailsList.length != 0) {
                    var tr = '';
                    $.each(CPMODDetailsList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.CPMODDebtNickname + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                    });
                    $("#CPMOdInfo tbody").html(tr);
                }
                $('#CPMnonBusinessRelatedDebtsDetails').html(JSON.stringify(CPMODDetailsList));
                return false;
            });
        });

        $("#CPMODothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.semiAnnualOdOtherNames').show();
            } else {
                $('.semiAnnualOdOtherNames').hide();
            }
        });
    });
</script>
@stop