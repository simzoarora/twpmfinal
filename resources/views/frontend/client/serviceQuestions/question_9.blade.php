<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about your vehicle loan?</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[9][questionName]','Details about your vehicle loan?') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For which vehicle?') }}
            {{ Form::input('text','data[9][answer][vehicle-type]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Make/model or nickname']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Origination date of the loan?') }}
            {{ Form::input('date','data[9][answer][loan-date]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Term of loan (in months)?') }}
            {{ Form::input('text','data[9][answer][loan-term]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the interest rate?') }}
            {{ Form::input('text','data[9][answer][interest-rate]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the monthly payment?') }}
            {{ Form::input('text','data[9][answer][monthly-payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the remaining amount?') }}
            {{ Form::input('text','data[9][answer][remaining-amount]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
    </div>
</div>
