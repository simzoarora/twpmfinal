<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about you</h6>
        <h2>Let's get the rest of your personal information.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[218]','Let\s get the rest of your personal information') }}

        <div class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', 'Address line 1') }}
                {{ Form::input('text','answer[218][address_line_1]', isset($defaultData)? $defaultData['userData']['first_address']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'readonly' ]) }}
            </div>
        </div>

        <div class="col-sm-8">
            <div class="col-sm-12 paddingLeft0 paddingRight0">
                <div class="inner-left ">
                    {{ Form::label('label', 'Address line 2') }}
                    {{ Form::input('text','answer[218][address_line_2]',  isset($defaultData)? $defaultData['userData']['last_address']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'placeholder'=>'', 'readonly']) }}
                </div>
            </div>


        </div>

        <div class="col-sm-8">
            <div class="col-sm-9 paddingLeft0 paddingRight0">
                <div class="inner-left ">
                    {{ Form::label('label', 'City') }}
                    {{ Form::input('text','answer[218][city]',isset($defaultData)? $defaultData['userData']['city']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'', 'readonly']) }}
                </div>
            </div>
            <div class="col-sm-3 paddingLeft0 paddingRight0 select-state">
                <div class="inner-left ">
                    {{ Form::label('label', 'State') }}
                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"  style="top:45px! important;"></i>
                    <select style="position: absolute;" readonly class="status valid" name="answer[218][state]" required="" aria-invalid="false" style="width:100% !important; max-width:100% !important;">
                        <option selected value="{{ $defaultData['userData']['state']['id']}}">{{ $defaultData['userData']['state']['name'] }}</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-sm-6" style="clear: left;">
            <div class="inner-left ">
                {{ Form::label('label', 'Zip code') }}
                {{ Form::input('text','answer[218][zipcode]', isset($defaultData)? $defaultData['userData']['zip']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'','readonly']) }}
            </div>
        </div>
    </div>
</div>
