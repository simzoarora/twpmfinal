<style>
    table {
        width:100%;
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }

    table tbody td .fa{
        /*float: right;*/
        margin-left: 10px;
        font-size: 13px;
    }
    .add-cpma-rental-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .account-info{
        position: relative;
    }
</style>
@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',8) }}
                    {{ Form::input('hidden','subTopicId',40) }}
                    {{ Form::input('hidden','redirectPageName','assets-preview') }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>Assets</h6>
                                    <h2>Real Estate (Rental & Commercial)</h2>
                                    <p>Use this section to enter information on all residential & commercial properties that are rental or income properties. If you have questions, feel free to <a href="#">contact us</a>.</p>
                                </div>

                                <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
                                    {{ Form::input('hidden','questionName[217]','Checking accounts') }}

                                    <div class="col-sm-6 inner-left ">
                                        <label for="label">Do you own any residential or commercial rental properties?</label>
                                        <label class="radio-custom-label">
                                            <input class="cpma-rental-property-confirmation table-confirmation" required="required" name="answer[217][cpma_rental_property_confirmation]" type="radio" value="yes" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('cpma_rental_property_confirmation',
                                                    $answer) && ($answer['cpma_rental_property_confirmation']
                                                == "yes")) {
                                                echo "checked";
                                            }
                                            ?>>Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            <input class="cpma-rental-property-confirmation table-confirmation" required="required" name="answer[217][cpma_rental_property_confirmation]" type="radio" value="no" aria-required="true" <?php
                                            if (!empty($answer) && array_key_exists('cpma_rental_property_confirmation',
                                                    $answer) && ($answer['cpma_rental_property_confirmation']
                                                == "no")) {
                                                echo "checked";
                                            }
                                            ?>>No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 inner-left checking-account-table " style="display:<?php
                                    if (!empty($answer) && ($answer['cpma_rental_property_confirmation']
                                        == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'List all rental properties.') }}
                                        <table id='account-info'>
                                            <thead>
                                                <tr>
                                                    <td style="font-size: 13px;">Property name</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                if (!empty($answer["rentalPropertiesRecords"])) {
                                                    $data = json_decode($answer["rentalPropertiesRecords"]);
                                                    if (!empty($data)) {
                                                        foreach ($data as $key => $info) {
                                                            ?>
                                                            <tr data-index="{{$key}}">
                                                                <td>{{$info->cpmaRentalPropertyName}}</td>
                                                                <td valign="top"> </td>
                                                                <td valign="top" align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete"class="fa fa-trash" aria-hidden="true"></i> </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </tbody>
                                        </table>
                                        <button type='button' class="add-cpma-rental-account" data-toggle="modal" data-target="#cpmaRentalPropertyModal">Add property</button>
                                        <textarea id="rentalPropertiesData" class="hidden" name="answer[217][rentalPropertiesRecords]">{{(!empty($answer["rentalPropertiesRecords"]))? $answer["rentalPropertiesRecords"]:null}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="sections">
                                <!-- modal dialog starts -->
                                <div id="cpmaRentalPropertyModal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">ADD RENTAL PROPERTY</h4>
                                            </div>
                                            <div class="modal-body add-checking-account" style="overflow: hidden;">
                                                <div class="row">
                                                    <!-- first step starts -->
                                                    <div class=" setup-content" id="step-1">
                                                        <div class="section-right">
                                                            <div class="validation-alert">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Give this property a name') }}
                                                                    {{ Form::input('text','cpma_rental_property_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-rental-property-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Address') }}
                                                                    {{ Form::input('text','cpma_rental_property_address',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-rental-property-address', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'street address', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','City') }}
                                                                    {{ Form::input('text','cpma_rental_property_city',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-rental-property-city', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'city', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <div class="col-sm-6 inner-left" style="padding:0; width: 47%; float:left;">
                                                                        {{ Form::label('label','State') }}
                                                                        {{ Form::input('text','cpma_state',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-state', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'state', 'required'=>'required','style'=>'width:100%;']) }}
                                                                    </div>

                                                                    <div class="col-sm-6 inner-left" style="padding:0; width: 47%; float:right;">
                                                                        {{ Form::label('label','Zip code') }}
                                                                        {{ Form::input('number','cpma_zip_code',null,['data-validation'=> '' , 'style' => 'text-align:left', 'class'=>'custom-validation form-control cpma-zip-code', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'00000', 'required'=>'required']) }}
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','Estimated value of the property?') }}
                                                                    <div class="input-group">
                                                                         <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','cpma_estimated_value_of_the_house',null,['data-validation'=> '' , 'class'=>'borderLeft0 ustom-validation form-control cpma-estimated-value-of-the-house', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','When was this property purchased?') }}
                                                                    {{ Form::input('text','cpma_property_purchase_date',null,['data-validation'=> '' , 'class'=>'custom-validation datetimepicker form-control cpma-property-purchase-date', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','How much was paid for the property?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','cpma_paid_for_property',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-paid-for-property', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','After expenses, what is your net annual rental income, approximately?') }}
                                                                    <div class="input-group">
                                                                         <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','cpma_annual_rental_income',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-annual-rental-income', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','What is your current cost basis on this rental property?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','cpma_property_current_cost',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-property-current-cost', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Do you use a management company?') }}
                                                                    <label class="radio-custom-label">    
                                                                        {{ Form::radio('cpma_management_company', 'yes',false,['class'=>'cpma-management-company','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('cpma_management_company', 'no',false,['class'=>'cpma-management-company','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label','For how long do you intend to hold this property?') }}
                                                                    {{ Form::input('number','cpma_hold_property',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-hold-property', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Do you have a mortgage against this property?') }}
                                                                    <label class="radio-custom-label">    
                                                                        {{ Form::radio('cpma_mortgage_against_property', 'yes',false,['class'=>'cpma-mortgage-against-property','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label"> 
                                                                        {{ Form::radio('cpma_mortgage_against_property', 'no',false,['class'=>'cpma-mortgage-against-property','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <button id="cpma-finish-mortgage-new" class="modal-button  hide abc" type="button" style="border-radius: 3px;    " data-index="0"> Save </button>
                                                                    <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- first step finsih -->

                                                    <!-- second step starts -->
                                                    <div class=" setup-content" id="step-2" style="display: none;">
                                                        <div class="section-right">
                                                            {{ Form::input('hidden','questionName[199]','ADD MORTGAGE') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What was the original mortgage amount?') }}
                                                                <div class="input-group">
                                                                     <span class="input-group-addon">$</span>
                                                                    {{ Form::input('number','original_mortgage_amount',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-mortgage-original-amount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'When did the motrgage begin?') }}
                                                                {{ Form::input('text','motrgage_begin_dat',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker cpma-mortgage-begin-datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'What is current mortgage balance?') }}
                                                                <div class="input-group">
                                                                     <span class="input-group-addon">$</span>
                                                                    {{ Form::input('number','mortgage_balance', null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-mortgage-balance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>

                                      
                                                            <div class="col-sm-12">
                                                                        <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i>  back</button>
                                                                        <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- second step finsih -->
                                                    <!-- third step starts -->
                                                    <div class="setup-content" id="step-3" style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="mortgage-tabed mortgageSecondStep">
                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0">
                                                                    <label style="font-weight:normal;">What type of mortgage is it?</label>
                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-one', 'Fixed', ['class' => 'label', 'for' => 'option-one'])}}
                                                                        {{ Form::radio('selector', 'fixed',null,['class'=>'cpma-type-mortgage',  'id' => 'option-one', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-two', 'Variable', ['class' => 'label', 'for' => 'option-two'])}}
                                                                        {{ Form::radio('selector', 'variable',null,['class'=>'cpma-type-mortgage',  'id' => 'option-two', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        <label class="label" for="option-three" style="line-height: normal; padding: 14px 5px;">Interest <br> - only </label>
                                                                        <!--{{Form::label('option-three', 'Interest <br> - Only  ', ['class' => 'label', 'for' => 'option-three'])}}-->
                                                                        {{ Form::radio('selector', 'interestonly',null,['class'=>'cpma-type-mortgage',  'id' => 'option-three', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection paddingLeft0"> 
                                                                        {{Form::label('option-four', 'Other', ['class' => 'label', 'for' => 'option-four'])}}
                                                                        {{ Form::radio('selector', 'other',null,['class'=>'cpma-type-mortgage',  'id' => 'option-four', ' required'=>'required']) }}
                                                                    </div>  
                                                                </div>

                                                                <div class="tab-content row">
                                                                    <div class="col-sm-12 inner-left">
                                                                        {{ Form::label('label', 'What is interest rate?') }}
                                                                        <div class="input-group">
                                                                            {{ Form::input('number','mortgage interest_rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control mortgage-interest-rate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                            <span class="input-group-addon">%</span>
                                                                        </div>
                                                                    </div>

                                                                    <!-- fixed tab content starts --> 
                                                                    <div id="fixed" class="tab-pane fade">
                                                                    </div>
                                                                    <!-- fixed tab content finish -->
                                                                    <!-- variable tab content starts -->
                                                                    <div id="variable" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'In how many years after origination do the adjustments begin?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','adjustments_begin',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-adjustments-begin', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">Years</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'How often does it adjust?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','semi_annual_adjust_often_select_value',null,['data-validation'=> '' , 'class'=>'custom-validation borderRight0 form-control cpma-adjust-often-value', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon" style="position: relative;padding: 0 10px;">
                                                                                    <p style="position: absolute;margin: 0;padding: 0;top: 0;z-index: 100;width: 100%;left: 0;" class=" text-center"><i class="fa fa-angle-up"></i></p>
                                                                                    <input type="text" name="often_adjust" class="select-val cpma-adjust-often-select-value" readonly="" value="Years" style="text-transform: capitalize;outline: none;width: 50px!important;border: 0;text-align: center;float: left;padding: 0!important;height: auto;font-size: 14px;margin: 0;line-height: initial;margin: 0;">
                                                                                    <p style="margin:0;padding: 0;position: absolute;left: 0;bottom: 0;width: 100%;" class="col-xs-12 col-sm-12 text-center"><i class="fa fa-angle-down"></i></p>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Interest rate cap (leave blank if there is no rate cap).') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','interest_rate_cap',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-interest-rate-cap', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">%</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!-- variable tab content finish --> 
                                                                    <!-- interest tab content starts --> 
                                                                    <div id="interestOnly" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'For what number of years is the loan interest-only?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_years_interest',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-loan-years-interest', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">Years</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Is the interest rate fixed during the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'yes',false,['class'=>'cpma-fixed-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('fixed_interest_period', 'no',false,['class'=>'cpma-fixed-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Does the interest rate float after the interest-only period?') }}
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'yes',false,['class'=>'cpma-float-interest-confirmation','required'=>'required']) }}Yes
                                                                                <span class="radio-icon"></span>
                                                                            </label> 
                                                                            <label class="radio-custom-label">  
                                                                                {{ Form::radio('float_interest_period', 'no',false,['class'=>'cpma-float-interest-confirmation','required'=>'required']) }}No
                                                                                <span class="radio-icon"></span>
                                                                            </label>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                            <select style="position: absolute;" class="status valid cpma-mortgage-interest-dropdown" id="mortgageothersdropdown"  name="interest_rate_based" required="" aria-invalid="false" style="display: none;">
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">10-year Treasury</option>
                                                                                <option value="2">Prime</option>
                                                                                <option value="3">Libor</option>
                                                                                <option value="4">Unsure</option>
                                                                                <option value="5">Other</option>
                                                                            </select>
                                                                        </div> 

                                                                        <div class="col-sm-12 inner-left mortgageothers" style="display: none">
                                                                            {{ Form::label('label', 'Other') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','mortgage_other]',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-mortgage-other', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">%</span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-sm-12 inner-left small-dropdown">
                                                                            {{ Form::label('label','Does this loan negatively amortize?') }}
                                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                            <select style="position: absolute;" class="status valid cpma-negatively-amortize" name="negatively_amortize" required="" aria-invalid="false" >
                                                                                <option selected="" disabled="" value="">SELECT</option>
                                                                                <option value="1">Yes</option>
                                                                                <option value="2">No</option>
                                                                                <option value="3">I don't know</option></select>
                                                                        </div>
                                                                    </div>

                                                                    <div id="others" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter type of mortgage?') }}
                                                                            {{ Form::input('text','mortgage_type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cpma-mortgage-type', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Describe']) }}
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i>  back</button>
                                                                        <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                    <!-- third step finsih -->
                                                    <!-- fourth step starts -->
                                                    <div class="setup-content" id="step-4"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="mortgage-tabed loan-term-step">
                                                                <div class="col-sm-12 inner-left paddingLeft0 paddingRight0"> 
                                                                    <h2>What is loan term?</h2>
                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-five', '15 Yr', ['class' => 'label', 'for' => 'option-five'])}}
                                                                        {{ Form::radio('yearselection', 'fifteenyr',null,['class'=>'cpma-fixed-year-selection',  'id' => 'option-five', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-six', '30 Yr', ['class' => 'label', 'for' => 'option-six'])}}
                                                                        {{ Form::radio('yearselection', 'thirtyyear',null,['class'=>'cpma-fixed-year-selection',  'id' => 'option-six', ' required'=>'required']) }}
                                                                    </div>

                                                                    <div class="OptionSelection  paddingLeft0"> 
                                                                        {{Form::label('option-seven', 'Other', ['class' => 'label', 'for' => 'option-seven'])}}
                                                                        {{ Form::radio('yearselection', 'otheryear',null,['class'=>'cpma-fixed-year-selection',  'id' => 'option-seven', ' required'=>'required']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="tab-content row">
                                                                    <!-- fiftenyr tab content starts --> 
                                                                    <div id="fiftenyr" class="tab-pane fade in"></div>
                                                                    <!-- fiftenyr tab content finish -->
                                                                    <!-- thirtyyr tab content starts -->
                                                                    <div id="thirtyyr" class="tab-pane fade"></div>
                                                                    <!-- thirtyyr tab content finish --> 

                                                                    <div id="loantermothers" class="tab-pane fade">
                                                                        <div class="col-sm-12 inner-left">
                                                                            {{ Form::label('label', 'Enter loan term?') }}
                                                                            <div class="input-group">
                                                                                {{ Form::input('number','loan_term',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cpma-enter-loan-term', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                                <span class="input-group-addon">Years</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-sm-12">
                                                                        <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i>  back</button>
                                                                        <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i></button>
                                                                    </div>
                                                                </div>
                                                            </div> 
                                                            <!-- tabbed pannel finsih -->
                                                        </div>
                                                    </div>
                                                    <!-- fourth step finsih -->
                                                    <!-- fifth step starts -->
                                                    <div class="setup-content" id="step-5"  style="display: none">
                                                        <div class="section-right">
                                                            <div class="col-sm-12 inner-left priv-insurace">
                                                                {{ Form::label('label', 'Do you pay Private Mortgage Insurance (PMI) on this loan?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'yes',false,['class'=>'cpma-private-insurance','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('private_mortgage_insurance', 'no',false,['class'=>'cpma-private-insurance','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-pmi" style="display:none;">
                                                                {{ Form::label('label', 'Is PMI paid as a seperate itemized payment on your mortgage statement?') }}
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'yes',false,['class'=>'cpma-mortgage-pmi-confirmation','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label> 
                                                                <label class="radio-custom-label">  
                                                                    {{ Form::radio('mortgage_statement', 'no',false,['class'=>'cpma-mortgage-pmi-confirmation','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left mortgage-paid-monthly"  style="display:none;">
                                                                {{ Form::label('label', 'What amount is paid monthly?') }}
                                                                <div class="input-group">
                                                                   <span class="input-group-addon">$</span>
                                                                    {{ Form::input('number','mortgage_paid_monthly',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cpma-mortgage-paid-monthly-box', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <!--id='childrenForm'-->  
                                                                <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i>  back</button>
                                                                <button  class='nextBtn pull-right' type="button">  next  <i class="fa fa-arrow-right"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- fifth step finish -->

                                                    <!-- sixth step starts -->
                                                    <div class="setup-content mortgage-last-step" id="step-6"  style="display: none">
                                                        <div class="section-right">
                                                            <!-- tabbed pannel starts -->
                                                            <div class="latest-statement"> 
                                                                <div class="col-sm-12 inner-left">
                                                                    <h2>Please enter the following amounts from your latest statement</h2> 
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Principal', [ 'class' => 'single-line'] ) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">

                                                                        <div class="input-group">
                                                                           <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','prinicpal_amount',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 inlineinput custom-validation form-control cpma-mortgage-prinicpal-amount', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v0', 'placeholder'=>'']) }}
                                                                        </div>

                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Interest' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','loan_interest',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 cpma-mortgage-loan-interest inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'id' => 'v1', 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Property Tax' , [ 'class' => 'single-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                           <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','property_tax',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 cpma-mortgage-property-tax inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true ,  'id' => 'v2','placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Private mortgage Insurance' , [ 'class' => 'double-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','private_motor_insurance',null,['data-validation'=> '' , 'class'=>'borderLeft0 qty1 cpma-mortgage-private-motor-insurance inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v3', 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Other (CDD fees, etc.)', [ 'class' => 'double-line']) }}
                                                                    </div>
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon">$</span>
                                                                            {{ Form::input('number','others_cdd_fees',null,['data-validation'=> '' , 'class'=>'qty1 borderLeft0 cpma-mortgage-others-cdd-fees inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'id' => 'v4', 'required'=>true , 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <div class="col-sm-5 paddingRight0 paddingLeft0">
                                                                        {{ Form::label('label', 'Monthly total' , [ 'class' => 'single-line']) }}
                                                                    </div> 
                                                                    <div class="col-sm-7  paddingRight0">
                                                                        <div class="input-group">
                                                                            <span class="input-group-addon" style="border:0; border-top:solid 2px #000;">$</span>
                                                                            {{ Form::input(' number','monthly_total',null,['data-validation'=> '' ,'readonly' => 'readonly',  'class'=>'borderLeft0  cpma-mortgage-monthly-total inlineinput custom-validation form-control', 'style' => 'border:0; margin:0; border-top:solid 2px #000; text-align:right', 'data-rule-cpma-mortgage-monthly-total inlineinput custom-validation form-controlregex'=>"false", 'required'=>false , 'id' => 'result', 'placeholder'=>'']) }}
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'If property taxes, are not paid in the mortgage, what are the annual property taxes?', ['style'=>'height:auto;']) }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','tax_not_paidl',null,['data-validation'=> '', 'class'=>'borderLeft0 cpma-mortgage-tax-not-paid inlineinput custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn" type="button"><i class="fa fa-arrow-left"></i>  back</button>
                                                                    <button id="cpma-finish-mortgage-New" class='finishDRMortgage finishBtn pull-right' type="button"> finish <i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- sixth step finsih -->  

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <!-- modal dialog finish -->
                            </div>
                        </section>
                    </fieldset> 


                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
     var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,8,'assets-preview'])}}";
    $(document).ready(function () {


        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        var cpmaRentalPropertyList = [];
        if ($('#rentalPropertiesData').val() != '') {
            cpmaRentalPropertyList = JSON.parse($('#rentalPropertiesData').val());
        }
        // ------------------------------ form modal validation starts 
      
        $(document).on('change', '.cpma-mortgage-against-property', function () {
            if ($(this).val() === 'yes') {
                $('.abc').addClass('hide').removeClass('show');
                $('#step-1 .nextBtn').removeClass('hide').addClass('show');
            } else {
                $('#step-1 .nextBtn').addClass('hide').removeClass('show');
                $('#step-1 .abc').removeClass('hide').addClass('show');
            }
        });
        
    
        $(document).on('click', '.abc', function () {
            if ($(this).hasClass('show')) {
                $(this).on('click', function () {
                    var $this = $(this);
                    var form = $this.closest('form');
                    if (form.valid()) {
                        if ($(this).hasClass('edit-form')) {
                            cpmaRentalPropertyList[$(this).attr('data-index')] = {
                                cpmaRentalPropertyName: $('#cpmaRentalPropertyModal .cpma-rental-property-name').val(),
                                cpmaRentalPropertyAddress: $('#cpmaRentalPropertyModal .cpma-rental-property-address').val(),
                                cpmaRentalPropertyCity: $('#cpmaRentalPropertyModal .cpma-rental-property-city').val(),
                                cpmaState: $('#cpmaRentalPropertyModal .cpma-state').val(),
                                cpmaZipCode: $('#cpmaRentalPropertyModal .cpma-zip-code').val(),
                                cpmaEstimatedValueOftheHouse: $('#cpmaRentalPropertyModal .cpma-estimated-value-of-the-house').val(),
                                cpmaPropertyPurchaseDate: $('#cpmaRentalPropertyModal .cpma-property-purchase-date').val(),
                                cpmaPaidForProperty: $('#cpmaRentalPropertyModal .cpma-paid-for-property').val(),
                                cpmaAnnualRentalIncome: $('#cpmaRentalPropertyModal .cpma-annual-rental-income').val(),
                                cpmaPropertyCurrentCost: $('#cpmaRentalPropertyModal .cpma-property-current-cost').val(),
                                cpmaManagementCompany: $('#cpmaRentalPropertyModal .cpma-management-company:checked').val(),
                                cpmaHoldProperty: $('#cpmaRentalPropertyModal .cpma-hold-property').val(),
                                cpmaMortgageAgainstProperty: $('#cpmaRentalPropertyModal .cpma-mortgage-against-property:checked').val()
                            };
                            $('#account-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#cpmaRentalPropertyModal .cpma-rental-property-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>  <i title="Delete"class="fa fa-trash"  aria-hidden="true"></i> </td>');
                        } else {
                            $('#account-info tbody').append('<tr data-index=' + cpmaRentalPropertyList.length + '><td>' + $('#cpmaRentalPropertyModal .cpma-rental-property-name').val() + '</td>  <td valign="top"> </td><td valign="top" align="right">   <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete"class="fa fa-trash"  aria-hidden="true"></i> </td></tr>');
                            cpmaRentalPropertyList.push({
                                cpmaRentalPropertyName: $('#cpmaRentalPropertyModal .cpma-rental-property-name').val(),
                                cpmaRentalPropertyAddress: $('#cpmaRentalPropertyModal .cpma-rental-property-address').val(),
                                cpmaRentalPropertyCity: $('#cpmaRentalPropertyModal .cpma-rental-property-city').val(),
                                cpmaState: $('#cpmaRentalPropertyModal .cpma-state').val(),
                                cpmaZipCode: $('#cpmaRentalPropertyModal .cpma-zip-code').val(),
                                cpmaEstimatedValueOftheHouse: $('#cpmaRentalPropertyModal .cpma-estimated-value-of-the-house').val(),
                                cpmaPropertyPurchaseDate: $('#cpmaRentalPropertyModal .cpma-property-purchase-date').val(),
                                cpmaPaidForProperty: $('#cpmaRentalPropertyModal .cpma-paid-for-property').val(),
                                cpmaAnnualRentalIncome: $('#cpmaRentalPropertyModal .cpma-annual-rental-income').val(),
                                cpmaPropertyCurrentCost: $('#cpmaRentalPropertyModal .cpma-property-current-cost').val(),
                                cpmaManagementCompany: $('#cpmaRentalPropertyModal .cpma-management-company:checked').val(),
                                cpmaHoldProperty: $('#cpmaRentalPropertyModal .cpma-hold-property').val(),
                                cpmaMortgageAgainstProperty: $('#cpmaRentalPropertyModal .cpma-mortgage-against-property:checked').val()
                            });
                        }
                        $('#cpmaRentalPropertyModal').modal('hide');
                        $('#rentalPropertiesData').html(JSON.stringify(cpmaRentalPropertyList));
                    }
                    return false;
                });
            }
        });
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($(this).hasClass('edit-form')) {
                        cpmaRentalPropertyList[$(this).attr('data-index')] = {
                            cpmaRentalPropertyName: $('#cpmaRentalPropertyModal .cpma-rental-property-name').val(),
                            cpmaRentalPropertyAddress: $('#cpmaRentalPropertyModal .cpma-rental-property-address').val(),
                            cpmaRentalPropertyCity: $('#cpmaRentalPropertyModal .cpma-rental-property-city').val(),
                            cpmaState: $('#cpmaRentalPropertyModal .cpma-state').val(),
                            cpmaZipCode: $('#cpmaRentalPropertyModal .cpma-zip-code').val(),
                            cpmaEstimatedValueOftheHouse: $('#cpmaRentalPropertyModal .cpma-estimated-value-of-the-house').val(),
                            cpmaPropertyPurchaseDate: $('#cpmaRentalPropertyModal .cpma-property-purchase-date').val(),
                            cpmaPaidForProperty: $('#cpmaRentalPropertyModal .cpma-paid-for-property').val(),
                            cpmaAnnualRentalIncome: $('#cpmaRentalPropertyModal .cpma-annual-rental-income').val(),
                            cpmaPropertyCurrentCost: $('#cpmaRentalPropertyModal .cpma-property-current-cost').val(),
                            cpmaManagementCompany: $('#cpmaRentalPropertyModal .cpma-management-company:checked').val(),
                            cpmaHoldProperty: $('#cpmaRentalPropertyModal .cpma-hold-property').val(),
                            cpmaMortgageAgainstProperty: $('#cpmaRentalPropertyModal .cpma-mortgage-against-property:checked').val(),
                            cpmaPropertyName: $('#cpmaRentalPropertyModal .cpma-property-name').val(),
                            cpmaMortgageOriginalAmount: $('#cpmaRentalPropertyModal .cpma-mortgage-original-amount').val(),
                            cpmaMortgageBeginDatetimepicker: $('#cpmaRentalPropertyModal .cpma-mortgage-begin-datetimepicker').val(),
                            cpmaMortgageBalance: $('#cpmaRentalPropertyModal .cpma-mortgage-balance').val(),
                            cpmaMortgageInterestRate: $('#cpmaRentalPropertyModal .cpma-mortgage-balance').val(),
                            cpmaMortgageAdjustmentsBegin: $('#cpmaRentalPropertyModal .cpma-adjustments-begin').val(),
                            cpmaMortgageAdjustOften: $('#cpmaRentalPropertyModal .cpma-adjust-often').val(),
                            cpmaMortgageInterestRateCap: $('#cpmaRentalPropertyModal .cpma-interest-rate-cap').val(),
                            cpmaMortgageLoanYearsInterest: $('#cpmaRentalPropertyModal .cpma-loan-years-interest').val(),
                            cpmaMortgageFixedInterestConfirmation: $('#cpmaRentalPropertyModal .cpma-fixed-interest-confirmation:checked').val(),
                            cpmaMortgageFloatInterestConfirmation: $('#cpmaRentalPropertyModal .cpma-float-interest-confirmation:checked').val(),
                            cpmaMortgageInterestDropdown: $('#cpmaRentalPropertyModal .cpma-mortgage-interest-dropdown').val(),
                            cpmaMortgageOther: $('#cpmaRentalPropertyModal .cpma-mortgage-other').val(),
                            cpmaMortgageNegativelyAmortize: $('#cpmaRentalPropertyModal .cpma-negatively-amortize').val(),
                            cpmaMortgageTypeMortgage: $('#cpmaRentalPropertyModal .cpma-type-mortgage:checked').val(),
                            cpmaMortgageType: $('#cpmaRentalPropertyModal .cpma-mortgage-type').val(),
                            cpmaAdjustOftenSelectValue: $('#cpmaRentalPropertyModal .cpma-adjust-often-select-value').val(),
                            cpmaAdjustOftenValue: $('#cpmaRentalPropertyModal .cpma-adjust-often-value').val(),
                            cpmaAdjustOftenValueConcatenate: $('#cpmaRentalPropertyModal .cpma-adjust-often-value').val() +''+$('#cpmaRentalPropertyModal .cpma-adjust-often-select-value').val(),
                            
                            cpmaMortgageEnterLoanTerm: $('#cpmaRentalPropertyModal .cpma-enter-loan-term').val(),
                            cpmaMortgageFixedYearSelection: $('#cpmaRentalPropertyModal .cpma-fixed-year-selection:checked').val(),
                            cpmaMortgagePrivateInsurance: $('#cpmaRentalPropertyModal .cpma-private-insurance:checked').val(),
                            cpmaMortgagePmiConfirmation: $('#cpmaRentalPropertyModal .cpma-mortgage-pmi-confirmation:checked').val(),
                            cpmaMortgagePaidMonthly: $('#cpmaRentalPropertyModal .cpma-mortgage-paid-monthly-box').val(),
                            cpmaMortgagePrinicpalAmount: $('#cpmaRentalPropertyModal .cpma-mortgage-prinicpal-amount').val(),
                            cpmaMortgageMonthlyTotal: $('#cpmaRentalPropertyModal .cpma-mortgage-monthly-total').val(),
                            cpmaMortgageLoanInterest: $('#cpmaRentalPropertyModal .cpma-mortgage-loan-interest').val(),
                            cpmaMortgagePropertyTax: $('#cpmaRentalPropertyModal .cpma-mortgage-property-tax').val(),
                            cpmaMortgagePrivateMotorInsurance: $('#cpmaRentalPropertyModal .cpma-mortgage-private-motor-insurance').val(),
                            cpmaMortgageOthersCddFees: $('#cpmaRentalPropertyModal .cpma-mortgage-others-cdd-fees').val(),
                            cpmaMortgageTaxNotPaid: $('#cpmaRentalPropertyModal .cpma-mortgage-tax-not-paid').val()
                        };
                        $('#account-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#cpmaRentalPropertyModal .cpma-rental-property-name').val() + '</td>  <td valign="top"></td> <td valign="top" align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>  <i title="Delete"class="fa fa-trash"  aria-hidden="true"></i> </td>');
                    } else {
                        $('#account-info tbody').append('<tr data-index=' + cpmaRentalPropertyList.length + '><td>' + $('#cpmaRentalPropertyModal .cpma-rental-property-name').val() + '</td>  <td valign="top"> </td><td valign="top" align="right">   <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete"class="fa fa-trash"  aria-hidden="true"></i> </td></tr>');
                        cpmaRentalPropertyList.push({
                            cpmaRentalPropertyName: $('#cpmaRentalPropertyModal .cpma-rental-property-name').val(),
                            cpmaRentalPropertyAddress: $('#cpmaRentalPropertyModal .cpma-rental-property-address').val(),
                            cpmaRentalPropertyCity: $('#cpmaRentalPropertyModal .cpma-rental-property-city').val(),
                            cpmaState: $('#cpmaRentalPropertyModal .cpma-state').val(),
                            cpmaZipCode: $('#cpmaRentalPropertyModal .cpma-zip-code').val(),
                            cpmaEstimatedValueOftheHouse: $('#cpmaRentalPropertyModal .cpma-estimated-value-of-the-house').val(),
                            cpmaPropertyPurchaseDate: $('#cpmaRentalPropertyModal .cpma-property-purchase-date').val(),
                            cpmaPaidForProperty: $('#cpmaRentalPropertyModal .cpma-paid-for-property').val(),
                            cpmaAnnualRentalIncome: $('#cpmaRentalPropertyModal .cpma-annual-rental-income').val(),
                            cpmaPropertyCurrentCost: $('#cpmaRentalPropertyModal .cpma-property-current-cost').val(),
                            cpmaManagementCompany: $('#cpmaRentalPropertyModal .cpma-management-company:checked').val(),
                            cpmaHoldProperty: $('#cpmaRentalPropertyModal .cpma-hold-property').val(),
                            cpmaMortgageAgainstProperty: $('#cpmaRentalPropertyModal .cpma-mortgage-against-property:checked').val(),
                            cpmaPropertyName: $('#cpmaRentalPropertyModal .cpma-property-name').val(),
                            cpmaMortgageOriginalAmount: $('#cpmaRentalPropertyModal .cpma-mortgage-original-amount').val(),
                            cpmaMortgageBeginDatetimepicker: $('#cpmaRentalPropertyModal .cpma-mortgage-begin-datetimepicker').val(),
                            cpmaMortgageBalance: $('#cpmaRentalPropertyModal .cpma-mortgage-balance').val(),
                            cpmaMortgageInterestRate: $('#cpmaRentalPropertyModal .cpma-mortgage-balance').val(),
                            cpmaMortgageAdjustmentsBegin: $('#cpmaRentalPropertyModal .cpma-adjustments-begin').val(),
                            cpmaMortgageAdjustOften: $('#cpmaRentalPropertyModal .cpma-adjust-often').val(),
                            cpmaMortgageInterestRateCap: $('#cpmaRentalPropertyModal .cpma-interest-rate-cap').val(),
                            cpmaMortgageLoanYearsInterest: $('#cpmaRentalPropertyModal .cpma-loan-years-interest').val(),
                            cpmaMortgageFixedInterestConfirmation: $('#cpmaRentalPropertyModal .cpma-fixed-interest-confirmation:checked').val(),
                            cpmaMortgageFloatInterestConfirmation: $('#cpmaRentalPropertyModal .cpma-float-interest-confirmation:checked').val(),
                            cpmaMortgageInterestDropdown: $('#cpmaRentalPropertyModal .cpma-mortgage-interest-dropdown').val(),
                            cpmaMortgageOther: $('#cpmaRentalPropertyModal .cpma-mortgage-other').val(),
                            cpmaMortgageNegativelyAmortize: $('#cpmaRentalPropertyModal .cpma-negatively-amortize').val(),
                            cpmaMortgageType: $('#cpmaRentalPropertyModal .cpma-mortgage-type').val(),
                            cpmaAdjustOftenSelectValue: $('#cpmaRentalPropertyModal .cpma-adjust-often-select-value').val(),
                            cpmaAdjustOftenValue: $('#cpmaRentalPropertyModal .cpma-adjust-often-value').val(),
                            
                            cpmaAdjustOftenValueConcatenate: $('#cpmaRentalPropertyModal .cpma-adjust-often-value').val() +''+$('#cpmaRentalPropertyModal .cpma-adjust-often-select-value').val(),
                            cpmaMortgageFixedYearSelection: $('#cpmaRentalPropertyModal .cpma-fixed-year-selection:checked').val(),
                            cpmaMortgageTypeMortgage: $('#cpmaRentalPropertyModal .cpma-type-mortgage:checked').val(),
                            cpmaMortgageEnterLoanTerm: $('#cpmaRentalPropertyModal .cpma-enter-loan-term').val(),
                            cpmaMortgagePrivateInsurance: $('#cpmaRentalPropertyModal .cpma-private-insurance:checked').val(),
                            cpmaMortgagePmiConfirmation: $('#cpmaRentalPropertyModal .cpma-mortgage-pmi-confirmation:checked').val(),
                            cpmaMortgagePaidMonthly: $('#cpmaRentalPropertyModal .cpma-mortgage-paid-monthly-box').val(),
                            cpmaMortgagePrinicpalAmount: $('#cpmaRentalPropertyModal .cpma-mortgage-prinicpal-amount').val(),
                            cpmaMortgageMonthlyTotal: $('#cpmaRentalPropertyModal .cpma-mortgage-monthly-total').val(),
                            cpmaMortgageLoanInterest: $('#cpmaRentalPropertyModal .cpma-mortgage-loan-interest').val(),
                            cpmaMortgagePropertyTax: $('#cpmaRentalPropertyModal .cpma-mortgage-property-tax').val(),
                            cpmaMortgagePrivateMotorInsurance: $('#cpmaRentalPropertyModal .cpma-mortgage-private-motor-insurance').val(),
                            cpmaMortgageOthersCddFees: $('#cpmaRentalPropertyModal .cpma-mortgage-others-cdd-fees').val(),
                            cpmaMortgageTaxNotPaid: $('#cpmaRentalPropertyModal .cpma-mortgage-tax-not-paid').val()
                        });
                    }
                    $('#cpmaRentalPropertyModal').modal('hide');
                    $('#rentalPropertiesData').html(JSON.stringify(cpmaRentalPropertyList));
                }
            }
            return false;
        });
        // ------------------------------ form modal validation finish
        $('#account-info').on('click', '.fa-pencil', function () {
            $('#cpmaRentalPropertyModal').modal();
            $('#step-1').show();
            $('#step-2, #step-3, #step-4, #step-5, #step-6 ').hide();
            var index = $(this).closest('tr').attr('data-index');
            cpmaRentalPropertyListDetails = cpmaRentalPropertyList[index];
            $('#cpmaRentalPropertyModal .cpma-rental-property-name').val(cpmaRentalPropertyListDetails.cpmaRentalPropertyName);
            $('#cpmaRentalPropertyModal .cpma-rental-property-address').val(cpmaRentalPropertyListDetails.cpmaRentalPropertyAddress);
            $('#cpmaRentalPropertyModal .cpma-rental-property-city').val(cpmaRentalPropertyListDetails.cpmaRentalPropertyCity);
            $('#cpmaRentalPropertyModal .cpma-state').val(cpmaRentalPropertyListDetails.cpmaState);
            $('#cpmaRentalPropertyModal .cpma-zip-code').val(cpmaRentalPropertyListDetails.cpmaZipCode);
            $('#cpmaRentalPropertyModal .cpma-estimated-value-of-the-house').val(cpmaRentalPropertyListDetails.cpmaEstimatedValueOftheHouse);
            $('#cpmaRentalPropertyModal .cpma-property-purchase-date').val(cpmaRentalPropertyListDetails.cpmaPropertyPurchaseDate);
            $('#cpmaRentalPropertyModal .cpma-paid-for-property').val(cpmaRentalPropertyListDetails.cpmaPaidForProperty);
            $('#cpmaRentalPropertyModal .cpma-annual-rental-income').val(cpmaRentalPropertyListDetails.cpmaAnnualRentalIncome);
            $('#cpmaRentalPropertyModal .cpma-property-current-cost').val(cpmaRentalPropertyListDetails.cpmaPropertyCurrentCost);
            $('#cpmaRentalPropertyModal .cpma-management-company[value=' + cpmaRentalPropertyListDetails.cpmaManagementCompany + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-hold-property').val(cpmaRentalPropertyListDetails.cpmaHoldProperty);
            $('#cpmaRentalPropertyModal .cpma-mortgage-against-property[value=' + cpmaRentalPropertyListDetails.cpmaMortgageAgainstProperty + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-property-name').val(cpmaRentalPropertyListDetails.cpmaPropertyName);
            $('#cpmaRentalPropertyModal .cpma-mortgage-original-amount').val(cpmaRentalPropertyListDetails.cpmaMortgageOriginalAmount);
            $('#cpmaRentalPropertyModal .cpma-mortgage-begin-datetimepicker').val(cpmaRentalPropertyListDetails.cpmaMortgageBeginDatetimepicker);
            $('#cpmaRentalPropertyModal .cpma-mortgage-balance').val(cpmaRentalPropertyListDetails.cpmaMortgageBalance);
            $('#cpmaRentalPropertyModal .mortgage-interest-rate').val(cpmaRentalPropertyListDetails.cpmaMortgageInterestRate);
            $('#cpmaRentalPropertyModal .cpma-adjustments-begin').val(cpmaRentalPropertyListDetails.cpmaMortgageAdjustmentsBegin);
            $('#cpmaRentalPropertyModal .cpma-adjust-often').val(cpmaRentalPropertyListDetails.cpmaMortgageAdjustOften);
            $('#cpmaRentalPropertyModal .cpma-interest-rate-cap').val(cpmaRentalPropertyListDetails.cpmaMortgageInterestRateCap);
            $('#cpmaRentalPropertyModal .cpma-loan-years-interest').val(cpmaRentalPropertyListDetails.cpmaMortgageLoanYearsInterest);
            $('#cpmaRentalPropertyModal .cpma-fixed-interest-confirmation[value=' + cpmaRentalPropertyListDetails.cpmaMortgageFixedInterestConfirmation + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-float-interest-confirmation[value=' + cpmaRentalPropertyListDetails.cpmaMortgageFloatInterestConfirmation + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-mortgage-interest-dropdown').val(cpmaRentalPropertyListDetails.cpmaMortgageInterestDropdown).trigger('change');
            $('#cpmaRentalPropertyModal .cpma-mortgage-other').val(cpmaRentalPropertyListDetails.cpmaMortgageOther);
            $('#cpmaRentalPropertyModal .cpma-negatively-amortize').val(cpmaRentalPropertyListDetails.cpmaMortgageNegativelyAmortize).trigger('change');
            $('#cpmaRentalPropertyModal .cpma-mortgage-type').val(cpmaRentalPropertyListDetails.cpmaMortgageType);
            $('#cpmaRentalPropertyModal .cpma-adjust-often-select-value').val(cpmaRentalPropertyListDetails.cpmaAdjustOftenSelectValue);
            $('#cpmaRentalPropertyModal .cpma-adjust-often-value').val(cpmaRentalPropertyListDetails.cpmaAdjustOftenValue);
            $('#cpmaRentalPropertyModal .cpma-fixed-year-selection[value=' + cpmaRentalPropertyListDetails.cpmaMortgageFixedYearSelection + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-type-mortgage[value=' + cpmaRentalPropertyListDetails.cpmaMortgageTypeMortgage + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-enter-loan-term').val(cpmaRentalPropertyListDetails.cpmaMortgageEnterLoanTerm);
            $('#cpmaRentalPropertyModal .cpma-private-insurance[value=' + cpmaRentalPropertyListDetails.cpmaMortgagePrivateInsurance + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-mortgage-pmi-confirmation[value=' + cpmaRentalPropertyListDetails.cpmaMortgagePmiConfirmation + ']').prop('checked', true);
            $('#cpmaRentalPropertyModal .cpma-mortgage-paid-monthly-box').val(cpmaRentalPropertyListDetails.cpmaMortgagePaidMonthly);
            $('#cpmaRentalPropertyModal .cpma-mortgage-prinicpal-amount').val(cpmaRentalPropertyListDetails.cpmaMortgagePrinicpalAmount);
            $('#cpmaRentalPropertyModal .cpma-mortgage-loan-interest').val(cpmaRentalPropertyListDetails.cpmaMortgageLoanInterest);
            $('#cpmaRentalPropertyModal .cpma-mortgage-property-tax').val(cpmaRentalPropertyListDetails.cpmaMortgagePropertyTax);
            $('#cpmaRentalPropertyModal .cpma-mortgage-private-motor-insurance').val(cpmaRentalPropertyListDetails.cpmaMortgagePrivateMotorInsurance);
            $('#cpmaRentalPropertyModal .cpma-mortgage-others-cdd-fees').val(cpmaRentalPropertyListDetails.cpmaMortgageOthersCddFees);
            $('#cpmaRentalPropertyModal .cpma-mortgage-tax-not-paid').val(cpmaRentalPropertyListDetails.cpmaMortgageTaxNotPaid);
            $('#cpmaRentalPropertyModal .cpma-mortgage-monthly-total').val(cpmaRentalPropertyListDetails.cpmaMortgageMonthlyTotal);


            if ($('.cpma-mortgage-against-property:checked').val() === 'yes') {
                $('.abc').addClass('hide').removeClass('show');
                $('#step-1 .nextBtn').removeClass('hide').addClass('show');
            } else {
                $('#step-1 .nextBtn').addClass('hide').removeClass('show');
                $('#step-1 .abc').removeClass('hide').addClass('show');
            }

            if ($('.cpma-private-insurance:checked').val() === 'yes') {
                $('#cpmaRentalPropertyModal .error-alert').remove();
                $('.mortgage-pmi').show();
            } else {
                $('.mortgage-pmi').hide();
                $('.cpma-mortgage-pmi-confirmation').prop('checked', false);
            }

            if ($('.cpma-mortgage-pmi-confirmation:checked').val() === 'yes') {
                $('#cpmaRentalPropertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }

            if ($('.cpma-type-mortgage:checked').val() === 'fixed') {
                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }

            if ($('.cpma-type-mortgage:checked').val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }

            if ($('.cpma-type-mortgage:checked').val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }

            if ($('.cpma-type-mortgage:checked').val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }
            //  this is for the second step tab panne finsh

            //   this is for the third step tab panne
            if ($('.cpma-fixed-year-selection:checked').val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }

            if ($('.cpma-fixed-year-selection:checked').val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#loantermothers, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }

            if ($('.cpma-fixed-year-selection:checked').val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
            // this is the third  step finsih

            $('#cpmaRentalPropertyModal .checking-account[value=' + cpmaRentalPropertyListDetails.checkingAccount + ']').prop('checked', true);
            $('#cpma-finish-mortgage-New, #cpma-finish-mortgage-new').addClass('edit-form').attr('data-index', index);
            $('#cpmaRentalPropertyModal .error-alert').remove();
        });
        $('.add-cpma-rental-account').on('click', function () {
            $('.account-details, #step-2, #step-3, #step-4, #step-5, #step-6 ').hide();
            $('#step-1').show();
            $('#cpma-finish-mortgage-New, #cpma-finish-mortgage-new').removeClass('edit-form');
            $('.checking-account').removeClass('selected-option');
            $('#cpmaRentalPropertyModal .error-alert').remove();
            $('#cpmaRentalPropertyModal input[type=text], #cpmaRentalPropertyModal input[type=number]').val('');
            $('#cpmaRentalPropertyModal input[type="radio"]').prop('checked', false);
            $('#cpmaRentalPropertyModal .OptionSelection label').removeClass('married-selected');
            $('#cpmaRentalPropertyModal  .cpma-adjust-often-select-value').val('Years');
            $('.mortgage-tabed .OptionSelection label').removeClass('married-selected');
            $('#fixed, #interestOnly, #variable, #others, #loantermothers').removeClass('active in');
            $('#cpmaRentalPropertyModal .mortgage-paid-monthly, #cpmaRentalPropertyModal .mortgage-pmi').hide();
            $('#cpmaRentalPropertyModal .mortgageSecondStep .mortgage-nav .secondloan-li:first-child ').removeClass('active');
        });
        $('#account-info').on('click', '.fa-trash', function () {
            $(this).closest('tr').remove();
            cpmaRentalPropertyList.splice($(this).closest('tr').attr('data-index'), 1);
            
        $("#account-info tbody").empty();
            if (cpmaRentalPropertyList.length != 0) {
                var tr = '';
                $.each(cpmaRentalPropertyList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.cpmaRentalPropertyName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete"class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#account-info tbody").html(tr);
            }
            
            $('#rentalPropertiesData').html(JSON.stringify(cpmaRentalPropertyList));
            // need to paste on all pages finsih --
//  
        });
        $(document).on('change', '.cpma-rental-property-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('.checking-account-table').show();
            } else {
                $('.checking-account-table').hide();
            }
        });
        $("#mortgageothersdropdown").on('change', function () {
            if ($(this).val() === '5') {
                $('.mortgageothers').show();
            } else {
                $('.mortgageothers').hide();
            }
        });
        if ($('#mortgageothersdropdown').val() === '5') {
            $('.mortgageothers').show();
        } else {
            $('.mortgageothers').hide();
        }

        $(document).on('change', '.cpma-private-insurance', function () {
            if ($(this).val() === 'yes') {
                $('#cpmaRentalPropertyModal .error-alert').remove();
                $('.mortgage-pmi').show();
            } else {
                $('.cpma-mortgage-pmi-confirmation').prop('checked', false);
                $('.mortgage-pmi').hide();
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });
        $(document).on('change', '.cpma-mortgage-pmi-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('#cpmaRentalPropertyModal .error-alert').remove();
                $('.mortgage-paid-monthly').show();
            } else {
                $('.mortgage-paid-monthly').hide();
                $('.mortgage-paid-monthly input').val('');
            }
        });
        // calculate total
        $(document).on("change", ".qty1", function () {
            var sum = 0;
            $(".qty1").each(function () {
                sum += +$(this).val();
            });
            $(".cpma-mortgage-monthly-total").val(sum);
        });
        //   this is for the second step tab panne
        $(document).on('change', '.cpma-type-mortgage', function () {
            if ($(this).val() === 'fixed') {
                $('label[for=option-one]').addClass('married-selected');
                $('#fixed').addClass('active in');
                $('#others, #variable, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-one]').removeClass('married-selected');
            }
        });
        $(document).on('change', '.cpma-type-mortgage', function () {
            if ($(this).val() === 'variable') {
                $('label[for=option-two]').addClass('married-selected');
                $('#variable').addClass('active in');
                $('#others, #fixed, #interestOnly').removeClass('active in');
            } else {
                $('label[for=option-two]').removeClass('married-selected');
            }
        });
        $(document).on('change', '.cpma-type-mortgage', function () {
            if ($(this).val() === 'interestonly') {
                $('label[for=option-three]').addClass('married-selected');
                $('#interestOnly').addClass('active in');
                $('#others, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-three]').removeClass('married-selected');
            }
        });
        $(document).on('change', '.cpma-type-mortgage', function () {
            if ($(this).val() === 'other') {
                $('label[for=option-four]').addClass('married-selected');
                $('#others').addClass('active in');
                $('#interestOnly, #fixed, #variable').removeClass('active in');
            } else {
                $('label[for=option-four]').removeClass('married-selected');
            }
        });
        //   this is for the third step tab panne

        $(document).on('change', '.cpma-fixed-year-selection', function () {
            if ($(this).val() === 'fifteenyr') {
                $('label[for=option-five]').addClass('married-selected');
                $('#fiftenyr').addClass('active in');
                $('#thirtyyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-five]').removeClass('married-selected');
            }
        });
        $(document).on('change', '.cpma-fixed-year-selection', function () {
            if ($(this).val() === 'thirtyyear') {
                $('label[for=option-six]').addClass('married-selected');
                $('#thirtyyr').addClass('active in');
                $('#fiftenyr, #loantermothers').removeClass('active in');
            } else {
                $('label[for=option-six]').removeClass('married-selected');
            }
        });
        $(document).on('change', '.cpma-fixed-year-selection', function () {
            if ($(this).val() === 'otheryear') {
                $('label[for=option-seven]').addClass('married-selected');
                $('#loantermothers').addClass('active in');
                $('#thirtyyr, #fiftenyr').removeClass('active in');
            } else {
                $('label[for=option-seven]').removeClass('married-selected');
            }
        });
        $('.fa-angle-up').on('click', function () {
            $('.select-val').val('Months');
        });
        $('.fa-angle-down').on('click', function () {
            $('.select-val').val('Years');
        });
    });



</script> 
<style type="text/css">
    .modal-body .setup-content .OptionSelection{ width: calc(100% - 77%);}
    .modal-body .setup-content .OptionSelection:nth-child(5n){margin-right: 0}
    .modal-body .setup-content .loan-term-step .OptionSelection{width: calc(100% - 69.2%)}
    .modal-body .setup-content .loan-term-step .OptionSelection:nth-child(4n){margin-right: 0}
</style>
@stop 
