@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',15) }}
                    {{ Form::input('hidden','subTopicId',33) }}
                    {{ Form::input('hidden','redirectPageName','spouse-investment-experience-preview') }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</h6>
                                    <h2>Real estate</h2>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[198]','Real estate') }}

                                    <div class="col-sm-6 inner-left">
                                        {{ Form::label('label', 'Do you, or have you, invested in real estate (individual properties)?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[198][real_estate]', 'yes',(!empty($answer)&& array_key_exists('real_estate',$answer)) ?(($answer['real_estate']=="yes")  ? true : false):false,['class'=>'real-estate','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[198][real_estate]', 'no',(!empty($answer)&& array_key_exists('real_estate',$answer)) ?(($answer['real_estate']=="no")  ? true : false):false,['class'=>'real-estate','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 inner-left properties" style="display: <?php
                                         if (!empty($answer) && array_key_exists('real_estate', $answer) && ($answer['real_estate'] == "yes")) {
                                             echo 'block';
                                         } else {
                                             echo 'none';
                                         }
                                         ?>">
                                        {{ Form::label('label','When did you start investing in properties?') }}
                                        {{ Form::input('number','answer[198][investing_properties]',(!empty($answer) && array_key_exists('investing_properties',$answer)) ? $answer['investing_properties']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control investing-properties', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'YYYY', 'required'=>'required']) }}
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
   var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,15,'spouse-investment-experience-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $(document).on('change', '.real-estate', function () {
            if ($(this).val() == 'yes') {
                $('.properties,.properties input').show();
            } else {
                $('.properties,.properties input').hide().val('');
            }
        });

        if ($('.real-estate:checked').val() == 'yes') {
            $('.properties,.properties input').show();
        } else {
            $('.properties,.properties input').hide().val('');
        }

        $('.investing-properties').datetimepicker({
            format: 'YYYY'
        });


    });
</script>
@stop
