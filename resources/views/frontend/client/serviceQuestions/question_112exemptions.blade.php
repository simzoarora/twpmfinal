<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <p class="section-name"> TAX CERTIFICATION </p>
        <h4> Exemptions-payee code.</h4>
        <p>Descriptive/explanatory statements </p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have an exempt payee code?') }}
            <label class="radio-custom-label"> {{ Form::radio('data[1112][answer][exemptcode]', 'yes',false,[ 'class' => 'radio-value exemptpayee']) }} Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[1112][answer][exemptcode]', 'no',false,[ 'class' => 'radio-value exemptpayee']) }} No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-12 inner-left" id="exempt-box" style="display: none;">
            {{ Form::label('label', 'Exempt payee code') }}
            {{ Form::input('text','data[1112][answer][code][0]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control other', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'code']) }}
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.exemptpayee').on('change', function () {
            if ($(this).val() === 'yes') {
                $('#exempt-box').show();
            } else {
                $('#exempt-box').hide();
            }
        });
    });
</script>
