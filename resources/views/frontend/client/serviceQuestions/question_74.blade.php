
<style>
    table {
        width:100%; 
    }
    table tbody td {
        padding: 17px 10px;
        border-bottom: 1px solid grey;
        position: relative;
    }
    table thead td {
        border-bottom: 1px solid grey;
    }
    /*    table tbody td .fa-pencil {
            position: absolute;
            right: 17px;
            top: 20px;
        }*/
    .add-retiremement-account {
        background: transparent;
        border: 1px solid #1d99d4;
        font-size: 12px;
        margin-top: 30px;
        color: #1d99d4;
    }
    .nonRetirementInfo{
        position: relative;
    }
    .retirement-account-details{
        padding: 0px;
    }
</style>
<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>Assets</h6>
        <h2>CD Accounts</h2>
        <p>Please answer the following questions, so we can get the most accurate picture of your finances. If you have questions, feel free to <a href="#">contact us</a>.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
        {{ Form::input('hidden','questionName[74]','CD accounts') }}

        <div class="col-sm-6 inner-left ">
            <label for="label">Do you have non-Retirement (Not an IRA, Roth, etc.) CD Bank Accounts?</label>
            <label class="radio-custom-label">
                <input class="non-Retirement-button table-confirmation" required="required" name="answer[74][non_Retirement]" type="radio" value="yes" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('non_Retirement', $answer) && ($answer['non_Retirement'] == "yes")) {
                    echo "checked";
                }
                ?>>Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                <input class="non-Retirement-button table-confirmation" required="required" name="answer[74][non_Retirement]" type="radio" value="no" aria-required="true" <?php
                if (!empty($answer) && array_key_exists('non_Retirement', $answer) && ($answer['non_Retirement'] == "no")) {
                    echo "checked";
                }
                ?>>No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-6 inner-left non-Retirement-table "  style="display:<?php
        if (!empty($answer) && array_key_exists('non_Retirement', $answer) && ($answer['non_Retirement'] == "yes")) {
            echo 'block';
        } else {
            echo 'none';
        }
        ?>">

            <h5>List each non-Retirement account. </h5>

            <table id='nonRetirementInfo' class="find-table-length">
                <thead>
                    <tr>
                        <td style="font-size: 13px;">Account name</td>
                        <td></td>
                        <td></td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($answer) && array_key_exists('nonRetirementRecords', $answer)) {
                        $account = json_decode($answer["nonRetirementRecords"]);
                        if (!empty($account)) {
                            foreach ($account as $key => $data) {
                                ?>
                                <tr data-index="{{$key}}">
                                    <td>{{$data->cdAccountFirstName}}</td>
                                    <td></td>
                                    <td align="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i> <i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td>
                                </tr>
                                <?php
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>
            <button type='button' class="add-retiremement-account" data-toggle="modal" data-target="#nonRetirementModal">Add account</button>
        </div>
    </div>
    <textarea id="nonRetirementData" class="hidden" name="answer[74][nonRetirementRecords]">{{(!empty($answer) && array_key_exists('nonRetirementRecords',$answer))? $answer["nonRetirementRecords"]:null}}</textarea>
</div>


<!-- new modal starts -->
<div class="sections">
    <!-- modal dialog starts -->
    <div id="nonRetirementModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">ADD CD ACCOUNT</h4>
                </div>
                <div class="modal-body" style="overflow: hidden;">
                    <div class="row">
                        <!-- first step starts -->
                        <div class=" setup-content">
                            <div class="section-right">
                                <div class="validation-alert">
                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Give the account a name') }}
                                        {{ Form::input('text','add_cd_account_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cd-account-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'nickname', 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','Account type') }}
                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('individual-select', 'Individual', ['class' => 'label', 'for' => 'individual-select'])}}
                                            {{ Form::radio('add_cd_selector', 'individual',null,['class'=>'cd-checking-account',  'id' => 'individual-select', ' required'=>'required']) }}
                                        </div>

                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('joint-select', 'Joint', ['class' => 'label', 'for' => 'joint-select'])}}
                                            {{ Form::radio('add_cd_selector', 'joint',null,['class'=>'cd-checking-account',  'id' => 'joint-select', ' required'=>'required']) }}
                                        </div>

                                        <div class="OptionSelection paddingLeft0"> 
                                            {{Form::label('trust-select', 'Trust  ', ['class' => 'label', 'for' => 'trust-select'])}}
                                            {{ Form::radio('add_cd_selector', 'trust',null,['class'=>'cd-checking-account',  'id' => 'trust-select', ' required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 retirement-account-details" style="display:none;">
                                        <div class="col-sm-12 inner-left">
                                            <label><i>Name of trust</i></label>
                                            {{ Form::input('text','add_cd_name_of_trust',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cd-name-of-trust', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                        </div>

                                        <div class="col-sm-12 inner-left">
                                            <label><i>Trustee</i></label>
                                            {{ Form::input('text','add_cd_trustee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cd-account-trustee', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trustee name', 'required'=>'required']) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','What is the value of this account?') }}
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                $
                                            </span>
                                            {{ Form::input('number','add_cd_Average_monthly_banlance',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control cd-account-value', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>0]) }}
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','What is the term of CD in months?') }}
                                        {{ Form::input('number','add_cd_non-Retirement',null,['data-validation'=> '' , 'class'=>'custom-validation form-control cd-term-detail', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required','min'=>0]) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','When does it mature?') }}
                                        {{ Form::input('text','add_cd_maturity_date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker maturitydatetimepicker', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        {{ Form::label('label','What is the interest rate?') }}
                                        <div class="input-group">
                                            {{ Form::input('number','add_cd_rate-of-interest',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control cd-interest-rate', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>

                                    <div class="col-sm-12 inner-left">
                                        <button id='nonRetirementForm' class="modal-button  non-retirement-finishBtn" type="button" style="border-radius: 3px;">
                                            Save
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- modal dialog finish -->
</div>
<!-- new modal finish -->




<style type="text/css">
    /*.modal-body .setup-content .OptionSelection{ width: calc(100% - 74%);}*/
    .modal-body .setup-content .OptionSelection:nth-child(4n){margin-right: 0}
    .modal-body .setup-content .OptionSelection .label{height: 40px; line-height: 40px; border: solid 1px #c4c4c4; color:#9b9b9b; font-size: 12px; border-radius: 0}
    .modal-body .setup-content .OptionSelection .married-selected{border:solid 1px #2079ee; color:#fff;}
</style>