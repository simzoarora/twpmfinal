@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')

                <div class="col-sm-12 recommended-service-div">
                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                    {{ Form::input('hidden','serviceId',$currentService->id) }}

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h6>assets</h2>
                                        <h2>Let's talk about your assets.</h2>
                                        <p>Having this information help us provide you with the best guidance and support possible. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>bank accounts</h4>
                                        </div>
                                        <div class="status col-sm-4">                   
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,12,72])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(12, $subTopicInfo)) {
                                                    echo $subTopicInfo[12];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>real estate (non-rental)</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,13,90])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(13, $subTopicInfo)) {
                                                    echo $subTopicInfo[13];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>real estate (raw land)</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,14,91])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(14, $subTopicInfo)) {
                                                    echo $subTopicInfo[14];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>real estate (rental)</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,40,217])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(40, $subTopicInfo)) {
                                                    echo $subTopicInfo[40];
                                                }
                                                ?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>personal property</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,15,92])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(15, $subTopicInfo)) {
                                                    echo $subTopicInfo[15];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>vehicles</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,16,93])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(16, $subTopicInfo)) {
                                                    echo $subTopicInfo[16];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>future assets</h4>
                                        </div>
                                        <div class="status col-sm-4"> 
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,17,94])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(17, $subTopicInfo)) {
                                                    echo $subTopicInfo[17];
                                                }
                                                ?></a>
                                        </div> 
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8"> 
                                            <h4>businesses</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,8,18,95])}}"><?php
                                                if (!empty($subTopicInfo) && array_key_exists(18, $subTopicInfo)) {
                                                    echo $subTopicInfo[18];
                                                }
                                                ?></a>
                                        </div>
                                    </div>
                                    <div class="custon-continue-button">
                                        <a  href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Continue</a>
                                    </div>
                                    <div class="custon-return">
                                        <a href="{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),$currentService->id])}}">Save and return later</a>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset> 
                    <!--{{ Form::close() }}-->
                </div>
            </div>
        </div>
    </div>
</div>
@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
@stop 
