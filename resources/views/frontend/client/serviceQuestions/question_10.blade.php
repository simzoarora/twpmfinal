@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content') 
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',505) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Credit Cards</h2>
                                    <p>Do not include cards for which you payoff the full amount each month</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[10]','Do you have any credit cards on which you carry a balance?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any credit cards on which you carry a balance?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[10][semi-annual-cc-confirmation]', 'yes',(!empty($answer) && array_key_exists('semi-annual-cc-confirmation',$answer)) ? (($answer['semi-annual-cc-confirmation']=="yes")  ? true : false):false,['class'=>'DRConfirmation table-confirmation', 'required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[10][semi-annual-cc-confirmation]', 'no',(!empty($answer) && array_key_exists('semi-annual-cc-confirmation',$answer)) ? (($answer['semi-annual-cc-confirmation']=="no")  ? true : false):false,['class'=>'DRConfirmation table-confirmation', 'required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnualCCDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('semi-annual-cc-confirmation', $answer) && ($answer['semi-annual-cc-confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <h6> List all credit cards on which you carry a balance.</h6>

                                                        <table id='DRInfo' class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Credit Card</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["credit_card_recods"])) {
                                                                    $childs = json_decode($answer["credit_card_recods"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->DRCardName}}</td>
                                                                                <td class="right-align"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="DRCCBtn add-dependent" data-toggle="modal" data-target="#DRCCModal">Add Card</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="creditCardDetails" class="hidden" name="answer[10][credit_card_recods]">{{(!empty($answer["credit_card_recods"]))? $answer["credit_card_recods"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="DRCCModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD CREDIT CARD</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->
                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class="section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this card name') }}
                                                                    {{ Form::input('text','semi-annual-cc-card-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRCardName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Which bank issue this card?') }}
                                                                    {{ Form::input('text','semi-annual-cc-bank-issuer',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRBankIssuer', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Bank Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What type of card is it?') }}
                                                                    {{ Form::input('text','semi-annual-cc-card-type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRCardType', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Visa, Discover, Nordstorm']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    <label>Brand? <i>(may be store, airline, etc.)</i>
                                                                    </label>
                                                                    {{ Form::input('text','semi-annual-cc-card-brand',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRBrand', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'United Airlines, etc.']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="nextBtn" type="button" >next<i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a rewards points or cash back card?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-cash-back-card', 'yes',false,['class'=>'DRCashBackCard', 'required' => 'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-cash-back-card', 'no',false,['class'=>'DRCashBackCard', 'required' => 'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashBackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'What is the point balance?') }}
                                                                    {{ Form::input('number','semi-annual-cc-point-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRPointBalance', 'data-rule-regex'=>"false", 'required'=>true , 'min'=>0]) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashBackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'Can points be exchanged for cash?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-points-exchanged', 'yes',false,['class'=>'DRPointsExchanged', 'required' => 'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-points-exchanged', 'no',false,['class'=>'DRPointsExchanged', 'required' => 'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left exchangeforCasgYesOption" style="display: none">
                                                                    <!--<i class="fa fa-dollar fa-dollar-top"></i>-->
                                                                    {{ Form::label('label', 'What is the rate at which points may be exchanged for cash(Example: 1 to 0.01 would equal one cent per point)?') }}
                                                                    {{ Form::input('number','semi-annual-cc-exchangefor-cash',null,['data-validation'=> '' , 'class'=>'custom-validation form-control DRExchangeforCash', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0 ]) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','semi-annual-cc-interest-rate',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DRInterestRate', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                        <span class="input-group-addon" id="basic-addon1">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the current balance on card?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','semi-annual-cc-current-balance',null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control DRCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                        <span class="input-group-addon" id="basic-addon1">%</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button class="nextBtn" type="button" >next<i class="fa fa-arrow-right"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- second step finsih -->
                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-monthly-payment',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRMonthlyPayment', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">

                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-pay-per-month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRPayPerMonth', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Approximately how much in a new purchases do you put on the card each month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-purchase-per-month',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRPurchasePerMonth', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Are you using this card for cash flow?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-card-cash-flow', 'yes',false,['class'=>'DRCardCashFlow']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('semi-annual-cc-card-cash-flow', 'no',false,['class'=>'DRCardCashFlow']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the credit limit?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon" id="basic-addon1">$</span>
                                                                        {{ Form::input('number','semi-annual-cc-credit-limit',null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control DRCreditLimit', 'data-rule-regex'=>"false", 'required'=>true, 'min'=>0]) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this card?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true" ></i>
                                                                    <select style="position: absolute;" class="status valid DRNameThisCard" id="DROthers" name="semi-annual-cc-name-this-card" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?><option value ='2'>{{$spouse_name}}</option><?php } ?>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <!--id='DRCCBtn'-->  
                                                                    <button class="backBtn"  type="button" ><i class="fa fa-arrow-left"></i> back</button>
                                                                    <button id="DRCCBtnnew" class='DRCCBtn finishBtn pull-right' type="button">
                                                                        finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <!-- third step finsih -->
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    <!--<a class="returnLater"  href="{{route('frontend.client.openPreviewFile',[config('constant.subdomain'),$currentService->id]) }}">Save and return later</a>-->
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/services-questions.js') }}"></script>



<script>

var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
$(document).ready(function () {
    $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
        addRemoveHref();
    });
    addRemoveHref();


    if ($('.DRCashBackCard').prop('checked', true).val() == 'yes' || ($('.DRCashBackCard:checked').val() == 'yes' && $('.DRPointsExchanged:checked').val() == 'yes')) {
        $('.CashBackYesOption ').show();
        $('.exchangeforCasgYesOption ').show();
    } else {
        $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
    }
    if (($('.DRCashBackCard:checked').val() == 'yes' && $('.DRPointsExchanged:checked').val() == 'yes')) {
        $('.exchangeforCasgYesOption ').show();
    }

    if ($('.DRPointsExchanged').prop('checked', true).val() == 'yes' || ($(this).val() == 'yes' && $('.DRPointsExchanged:checked').val() == 'yes')) {
        $('.CashBackYesOption ').show();
    } else {
        $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
    }


    $("select").selectBoxIt();
    $('.datetimepicker').datetimepicker({
        format: 'MM/DD/YYYY'
    });
    var SemiAnnualCCDetailsList = [];

    if ($('#creditCardDetails').val() != '') {
        SemiAnnualCCDetailsList = JSON.parse($('#creditCardDetails').val());
    }
    $(document).on('change', '.DRConfirmation', function () {
        if ($(this).val() === 'yes') {
            $('.SemiAnnualCCDetailsTable').show();
        } else {
            $('.SemiAnnualCCDetailsTable').hide();
            SemiAnnualCCDetailsList = [];
            $('#creditCardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));
            $('.SemiAnnualCCDetailsTable tbody tr').remove();
        }
    });

    $(document).on('change', '.DRCashBackCard', function () {
        if ($(this).prop('checked', true).val() == 'yes' || ($(this).val() == 'yes')) {
            $('.CashBackYesOption ').show();
        } else {
            $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
        }
    });


    $(document).on('change', '.DRPointsExchanged', function () {
        if ($(this).val() == 'yes') {
            $('.exchangeforCasgYesOption ').show();
            $('.DRExchangeforCash ').show();
        } else {
            $('.exchangeforCasgYesOption').hide();
            $('.DRExchangeforCash').hide();
        }
    });





    $('.nextBtn, .finishBtn').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid() === true) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($('#DRCCModal .DRCardName').val() && $('#DRCCModal .DRBankIssuer').val()) {
                    if ($('.finishBtn').hasClass('edit-form')) {
                        SemiAnnualCCDetailsList[$(this).attr('data-index')] = {
                            DRCardName: $('#DRCCModal .DRCardName').val(),
                            DRBankIssuer: $('#DRCCModal .DRBankIssuer').val(),
                            DRCardType: $('#DRCCModal .DRCardType').val(),
                            DRBrand: $('#DRCCModal .DRBrand').val(),

                            DRCashBackCard: $('.DRCashBackCard:checked').val(),
                            DRPointBalance: $('#DRCCModal .DRPointBalance').val(),
                            DRPointsExchanged: $('.DRPointsExchanged:checked').val(),
                            DRExchangeforCash: $('#DRCCModal .DRExchangeforCash').val(),
                            DRInterestRate: $('#DRCCModal .DRInterestRate').val(),
                            DRCurrentBalance: $('#DRCCModal .DRCurrentBalance').val(),

                            DRMonthlyPayment: $('#DRCCModal .DRMonthlyPayment').val(),
                            DRPayPerMonth: $('#DRCCModal .DRPayPerMonth').val(),
                            DRPurchasePerMonth: $('#DRCCModal .DRPurchasePerMonth').val(),
                            DRCardCashFlow: $('.DRCardCashFlow:checked').val(),
                            DRCreditLimit: $('#DRCCModal .DRCreditLimit').val(),
                            DRNameThisCard: $('#DRCCModal .DRNameThisCard').val(),
                        };
                        $('#DRInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#DRCCModal .DRCardName').val() + '</td>  <td class="right-align"><i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-trash"  aria-hidden="true"></i></td>');

                    } else {
                        $('#DRInfo tbody').append('<tr data-index=' + SemiAnnualCCDetailsList.length + '><td>' + $('#DRCCModal .DRCardName').val() + '</td>   <td class="right-align"> <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        SemiAnnualCCDetailsList.push({
                            DRCardName: $('#DRCCModal .DRCardName').val(),
                            DRBankIssuer: $('#DRCCModal .DRBankIssuer').val(),
                            DRCardType: $('#DRCCModal .DRCardType').val(),
                            DRBrand: $('#DRCCModal .DRBrand').val(),

                            DRCashBackCard: $('.DRCashBackCard:checked').val(),
                            DRPointBalance: $('#DRCCModal .DRPointBalance').val(),
                            DRPointsExchanged: $('.DRPointsExchanged:checked').val(),
                            DRExchangeforCash: $('#DRCCModal .DRExchangeforCash').val(),
                            DRInterestRate: $('#DRCCModal .DRInterestRate').val(),
                            DRCurrentBalance: $('#DRCCModal .DRCurrentBalance').val(),

                            DRMonthlyPayment: $('#DRCCModal .DRMonthlyPayment').val(),
                            DRPayPerMonth: $('#DRCCModal .DRPayPerMonth').val(),
                            DRPurchasePerMonth: $('#DRCCModal .DRPurchasePerMonth').val(),
                            DRCardCashFlow: $('.DRCardCashFlow:checked').val(),
                            DRCreditLimit: $('#DRCCModal .DRCreditLimit').val(),
                            DRNameThisCard: $('#DRCCModal .DRNameThisCard').val(),

                        });

                    }
                    $('#DRCCModal').modal('hide');
                    $('#DRCCModal .DRCardName, #DRCCModal .DRCardType').val('');
                    $('#DRCCModal .DRConfirmation').prop('checked', false);
                    $('#creditCardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));

                    if ($('.DRPointsExchanged:checked').val() == 'yes') {
                        $('.exchangeforCasgYesOption ').show();
                        $('.DRExchangeforCash ').show();
                    } else {
                        $('.exchangeforCasgYesOption').hide();
                        $('.DRExchangeforCash').hide();
                    }
                }

            }
        }
        return false;
    });



    $(document).on('click', '#DRInfo .fa-pencil', function () {
        $('#DRCCModal').modal();
        $('#DRCCModal #step-1').show();
        $('#DRCCModal #step-2, #DRCCModal #step-3').css('display', 'none');
        $('#DRCCModal .inner-left input').parent().find('label.error').remove();
        var index = $(this).closest('tr').attr('data-index');
        SemiAnnualCCDetails = SemiAnnualCCDetailsList[index];
        $('#DRCCModal .DRCardName').val(SemiAnnualCCDetails.DRCardName);
        $('#DRCCModal .DRBankIssuer').val(SemiAnnualCCDetails.DRBankIssuer);
        $('#DRCCModal .DRCardType').val(SemiAnnualCCDetails.DRCardType);
        $('#DRCCModal .DRBrand').val(SemiAnnualCCDetails.DRBrand);

        $('#DRCCModal .DRCashBackCard[value=' + SemiAnnualCCDetails.DRCashBackCard + ']').prop('checked', true);
        $('#DRCCModal .DRPointBalance').val(SemiAnnualCCDetails.DRPointBalance);
        $('#DRCCModal .DRPointsExchanged[value=' + SemiAnnualCCDetails.DRPointsExchanged + ']').prop('checked', true);
        $('#DRCCModal .DRExchangeforCash').val(SemiAnnualCCDetails.DRExchangeforCash);
        $('#DRCCModal .DRInterestRate').val(SemiAnnualCCDetails.DRInterestRate);
        $('#DRCCModal .DRCurrentBalance').val(SemiAnnualCCDetails.DRCurrentBalance);

        $('#DRCCModal .DRMonthlyPayment').val(SemiAnnualCCDetails.DRMonthlyPayment);
        $('#DRCCModal .DRPayPerMonth').val(SemiAnnualCCDetails.DRPayPerMonth);
        $('#DRCCModal .DRPurchasePerMonth').val(SemiAnnualCCDetails.DRPurchasePerMonth);
        $('#DRCCModal .DRCardCashFlow[value=' + SemiAnnualCCDetails.DRCardCashFlow + ']').prop('checked', true);
        $('#DRCCModal .DRCreditLimit').val(SemiAnnualCCDetails.DRCreditLimit);
        $('#DRCCModal .DRNameThisCard').val(SemiAnnualCCDetails.DRNameThisCard).trigger('change');


        $('#DRCCBtnnew').addClass('edit-form');
        $('#DRCCBtnnew').attr('data-index', index);
    });

    $(document).on('click', '.DRCCBtn', function () {
        $('#DRCCBtn').removeClass('edit-form');
        $('#DRCCModal input[type="text"], #DRCCModal input[type="number"]').val('');
        $('#DRCCModal select').val('').trigger('change');
        $('#DRCCModal .error-alert').remove();
        $('#DRCCModal #step-1').show();
        $('#DRCCModal #step-2, #DRCCModal #step-3, .CashBackYesOption, .exchangeforCasgYesOption').hide();
    });

    $(document).on('click', '.fa-trash', function () { // <-- changes

        var index_id = $(this).closest('tr').attr('data-index');
        deleteRow(index_id);
        $('.swal-button--danger').click(function () {

            SemiAnnualCCDetailsList.splice(index_id, 1);

            $("#DRInfo tbody").empty();
            if (SemiAnnualCCDetailsList.length != 0) {
                var tr = '';
                $.each(SemiAnnualCCDetailsList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.DRCardName + '</td><td></td><td align="right"> <i class="fa fa-pencil" aria-hidden="true"></i><i class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#DRInfo tbody").html(tr);
            }


            $('#creditCardDetails').html(JSON.stringify(SemiAnnualCCDetailsList));
            return false;
        });
    });

    $("#DROthers").on('change', function () {
        if ($(this).val() === '4') {
            $('.DRInterestRateOthers').show();
        } else {
            $('.DRInterestRateOthers').hide();
        }
    });

    $(document).on('click', '.DRCCBtn', function () { // <-- changes.
        $('#DRCCModal').find('.error-alert').remove();
        $('#DRCCModal input[type="radio"]').prop('checked', false);

    });
});


</script>
@stop