<style type="text/css">
    .wrapper .sections .section-right .alignment .left-side .inner-left{width:100% !important;}
    @media screen and (min-width: 768px) {
         .citizen .selectArrow{top: 43px; right:30px}
    }

    @media screen and (max-width: 767px) {
       .citizen .selectArrow{top: 44px; right:30px}
    }
</style>

<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>about <?php echo $spouse_name ;?></h6>
        <h2>Let's get the more information about <?php echo $spouse_name ;?>.</h2>
        <p>In order to provide you the best guidance, we need to make sure we have the right information. Please confirm your details and add anything that may be missing. If you need help, simply <a href="" data-target="#get-started-modal" data-toggle='modal'>contact us</a>.</p>
    </div> 

    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
        {{ Form::input('hidden','questionName[221]','Let\s get the rest of your personal information') }}
        <div class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', 'Date of birth') }} 
                {{ Form::input('text','answer[221][spouse_date_of_birth]',(!empty($answer) && array_key_exists('spouse_date_of_birth', $answer)) ? $answer['spouse_date_of_birth']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
            </div> 
        </div>

        <div style="clear:left;" class="col-sm-6">
            <div class="inner-left" style="width:100% !important;">
                {{ Form::label('label', 'Gender') }}
                
                <label class="radio-custom-label">
                        <input class="spouse_gender table-confirmation" required="required" name="answer[221][spouse_gender]" type="radio" value="male" aria-required="true" <?php
                        if (!empty($answer) && array_key_exists('spouse_gender', $answer) && ($answer['spouse_gender'] == "male")) {
                            echo "checked";
                        }
                        ?>>Male
                        <span class="radio-icon"></span>
                    </label>
                   
                <label class="radio-custom-label">
                        <input class="spouse_gender table-confirmation" required="required" name="answer[221][spouse_gender]" type="radio" value="female" aria-required="true" <?php
                        if (!empty($answer) && array_key_exists('spouse_gender', $answer) && ($answer['spouse_gender'] == "female")) {
                            echo "checked";
                        }
                        ?>>Female
                        <span class="radio-icon"></span>
                    </label>
            </div>
        </div>

        <div style="clear:left;" class="col-sm-6 left-side">
            <div class="inner-left ">
                {{ Form::label('label', 'Last four digits of social security Number') }} 
                {{ Form::input('number','answer[221][spouse_social_security_number]',(!empty($answer) && array_key_exists('spouse_social_security_number', $answer)) ? $answer['spouse_social_security_number']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'XXXX', 'maxlength'=>4]) }}
            </div> 
        </div>

        <div style="clear:left;" class="col-sm-6 padding-left0 citizen">
            <div class="inner-left ">
                {{ Form::label('label', 'Citizenship status') }}
                <i class="fa fa-angle-down selectArrow" aria-hidden="true" style=""></i>
                <select style="position: absolute;" class="status valid" name="answer[221][spouse_citizen_status]" required="" aria-invalid="false">
                    <option selected="" disabled="" value="">Select</option>
                    <option value="1" <?php
                    if (array_key_exists('spouse_citizen_status', $answer) && ($answer['spouse_citizen_status'] == 1))
                    echo "selected";
                    ?>>U.S. Citizen / National</option>
                    <option value="2" <?php
                    if (array_key_exists('spouse_citizen_status', $answer) && ($answer['spouse_citizen_status'] == 2))
                    echo "selected";
                    ?>>non-U.S. Citizen</option>
                </select>
                
            </div>
        </div>


    </div> 
</div>
