@extends('frontend.layouts.client')
@section('title')
@stop
@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',13) }}
                    {{ Form::input('hidden','subTopicId',5) }}
                    {{ Form::input('hidden','redirectPageName','spouse-employment-income-retirement-preview') }}

                    <?php
                    if (!empty($answer) && !empty($answer["copy"])) {
                        foreach ($answer["copy"] as $key => $data) {
                            ?>
                            <?php
                            if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) {
                                ?>
                                <h3></h3>
                                <fieldset>
                                    <section class="sections">
                                        <div class="col-sm-12">
                                            <div class="col-md-4 col-xs-11 section-left">
                                                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{$spouse_name}} EXECUTIVE COMPENSATION</h6>
                                                <h2>Executive compensation</h2>
                                            </div>
                                            <div class="col-sm-6 col-sm-offset-1 section-right">
                                                {{ Form::input('hidden','questionName[215]','Do you receive executive compensation (Restricted stock, Stock Options)?') }}
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label', 'Do you receive executive compensation (Restricted stock, Stock Options)?') }}
                                                    <label class="radio-custom-label">
                                                        {{ Form::radio('answer[215][copy]['.$key.'][exe_compen_first_confirmation_stock_option_confirmation_'.$key.']', 'yes', (array_key_exists('exe_compen_first_confirmation_stock_option_confirmation_'.$key,$data) && $data['exe_compen_first_confirmation_stock_option_confirmation_'.$key] == "yes") ? true : false,['class'=>'exe-comp-res-first-stock-confirmation','required'=>'required']) }}Yes
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                    <label class="radio-custom-label">
                                                        {{ Form::radio('answer[215][copy]['.$key.'][exe_compen_first_confirmation_stock_option_confirmation_'.$key.']', 'no',(array_key_exists('exe_compen_first_confirmation_stock_option_confirmation_'.$key,$data) && $data['exe_compen_first_confirmation_stock_option_confirmation_'.$key] == "no") ? true : false,['class'=>'exe-comp-res-first-stock-confirmation','required'=>'required']) }}No
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </fieldset>
        <?php } ?>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                                            <h2>Executive compensation</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right">
                                            {{ Form::input('hidden','questionName[215]','Do you receieve Restricted stock?') }}
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Do you receieve Restricted stock?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][exe_compen_confirmation_stock_option_confirmation_'.$key.']', 'yes', (array_key_exists('exe_compen_confirmation_stock_option_confirmation_'.$key,$data) && $data['exe_compen_confirmation_stock_option_confirmation_'.$key] == "yes") ? true : false,['class'=>'exe-comp-res-stock-confirmation table-confirmation','required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][exe_compen_confirmation_stock_option_confirmation_'.$key.']', 'no',(array_key_exists('exe_compen_confirmation_stock_option_confirmation_'.$key,$data) && $data['exe_compen_confirmation_stock_option_confirmation_'.$key] == "no") ? true : false,['class'=>'exe-comp-res-stock-confirmation table-confirmation','required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                            <div class="col-sm-12 inner-left exe-compen-res-stock-detail-table" style="display: <?php
                                            if (array_key_exists('exe_compen_confirmation_stock_option_confirmation_'.$key,
                                                    $data) && ($data['exe_compen_confirmation_stock_option_confirmation_'.$key]
                                                == "yes" )) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>" >
                                                <div class="col-sm-12">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="add-child add-property restricted-stock">

                                                                    <table id='exe-compen-res-stock-property-info'>
                                                                        <thead>
                                                                            <tr>
                                                                                <td colspan="2">List your restricted stock grant information.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Grant name</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-copy="{{$key}}">
                                                                            <?php
                                                                            if (array_key_exists('ExeCompResStockRecords_'.$key,
                                                                                    $data)) {
                                                                                $detail
                                                                                    = json_decode($data["ExeCompResStockRecords_".$key]);
                                                                                if (!empty($detail)) {
                                                                                    foreach ($detail as $key1 => $records) {
                                                                                        ?>
                                                                                        <tr data-index="{{$key1}}">
                                                                                            <td>{{$records->executiveCompensationGrantName}}</td>
                                                                                            <td  style="text-align:right; width:80px;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                                        </tr>
                                                                                        <?php
                                                                                    }
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="col-md-5 ">
                                                                        <div class="row">
                                                                            <button data-copy="{{$key}}" type='button' class="add-dependent add-grant" data-toggle="modal" data-target="#exeCompResStockModal">Add grant</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <textarea id="ExeCompResStockData" class="hidden" name="answer[215][copy][<?php echo $key; ?>][ExeCompResStockRecords_<?php echo $key; ?>]">{{(!empty($data["ExeCompResStockRecords_".$key]))? $data["ExeCompResStockRecords_".$key]:null}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                                            <h2>Executive compensation</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right">
                                            {{ Form::input('hidden','questionName[215]','Do you receieve stock options?') }}
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Do you receieve stock options?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][stock_option_confirmation_'.$key.']', 'yes', (array_key_exists('stock_option_confirmation_'.$key,$data) && $data['stock_option_confirmation_'.$key] == "yes") ? true : false,['class'=>'stock-option-confirmation table-confirmation','required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][stock_option_confirmation_'.$key.']', 'no',(array_key_exists('stock_option_confirmation_'.$key,$data) && $data['stock_option_confirmation_'.$key] == "no") ? true : false,['class'=>'stock-option-confirmation table-confirmation','required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                            <div class="col-sm-12 inner-left stock-option-confirmation-table" style="display: <?php
                                            if (array_key_exists('stock_option_confirmation_'.$key,
                                                    $data) && ($data['stock_option_confirmation_'.$key]
                                                == "yes" )) {
                                                echo 'block';
                                            } else {
                                                echo 'none';
                                            }
                                            ?>" >
                                                <div class="col-sm-12  ">
                                                    <div class="row">
                                                        <div class="col-sm-10">
                                                            <div class="row">
                                                                <div class="add-child add-property stock-options">

                                                                    <table id='stock-option-property-info'>
                                                                        <thead>
                                                                            <tr>
                                                                                <td colspan="2">List your stock option grant information.</td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>Grant name</td>
                                                                                <td></td>
                                                                                <td></td>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody data-copy="{{$key}}">
                                                                            <?php
                                                                            if (array_key_exists('ExeCompStockOptionRecords_'.$key,
                                                                                    $data)) {
                                                                                $details
                                                                                    = json_decode($data["ExeCompStockOptionRecords_".$key]);
                                                                                if (!empty($details)) {
                                                                                    foreach ($details as $key2 => $info) {
                                                                                        ?>
                                                                                        <tr class="children-info" data-index="{{$key2}}">
                                                                                            <td class="">{{$info->stockOptionGrantName}}</td>
                                                                                            <td  style="text-align:right;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>  <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                                        </tr>
                    <?php
                }
            }
        }
        ?>
                                                                        </tbody>
                                                                    </table>
                                                                    <div class="col-md-5 ">
                                                                        <div class="row">
                                                                            <button data-copy="{{$key}}" type='button' class="add-dependent add-grant add-stock-option-grant" data-toggle="modal" data-target="#stockOptionModal">Add grant</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <textarea id="ExeCompenStockOptionData216" class="hidden" name="answer[215][copy][<?php echo $key; ?>][ExeCompStockOptionRecords_<?php echo $key; ?>]">{{(!empty($data["ExeCompStockOptionRecords_".$key]))? $data["ExeCompStockOptionRecords_".$key]:null}}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                                            <h2>Executive compensation</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right">
                                            {{ Form::input('hidden','questionName[215]','Do you participate in a Deferred Compensation Plan?') }}
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Do you participate in a Deferred Compensation Plan?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][deffered_amount_annually_'.$key.']', 'yes',(array_key_exists('deffered_amount_annually_'.$key,$data) && $data['deffered_amount_annually_'.$key] == "yes") ? true : false,['class'=>'deffered-amount-annually','required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][deffered_amount_annually_'.$key.']', 'no',(array_key_exists('deffered_amount_annually_'.$key,$data) && $data['deffered_amount_annually_'.$key] == "no") ? true : false,['class'=>'deffered-amount-annually','required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>
                                            <div class="col-sm-12 inner-left deffered-amount-annually-confirmation" style="display:  <?php
                                                 if (array_key_exists('deffered_amount_annually_'.$key,
                                                         $data) && ($data['deffered_amount_annually_'.$key]
                                                     == "yes" )) {
                                                     echo 'block';
                                                 } else {
                                                     echo 'none';
                                                 }
                                                 ?>" >
                                                <div class="col-sm-12  ">
                                                    <div class="row">
                                                        <div class="inner-left">
                                                            {{ Form::label('label', 'What percentage or dollar amount due you defer annually?') }}

                                                            <div class="input-group service-input-group coverage-input-group" style="margin-bottom:0 !important;">
                                                                {{ Form::input('number','answer[215][copy]['.$key.'][stock_option_stock_symbol_'.$key.']', (array_key_exists('stock_option_stock_symbol_'.$key,$data)) ? $data['stock_option_stock_symbol_'.$key] : null,['data-validation'=> '' , 'class'=>'borderRight0 full-width custom-validation form-control stock-option-stock-symbol', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'', 'style'=>'padding-right:0px! important;']) }}
                                                                <span class="input-group-addon" style="position: relative;padding: 0 10px;">
                                                                    <p style="position: absolute;margin: 0;padding: 0;top: 0;z-index: 100; left:31px;" class=" text-center"><i class="fa fa-angle-up"></i></p>

                                                                    {{ Form::input('text','answer[215][copy]['.$key.'][defered_symbol_'.$key.']', (array_key_exists('defered_symbol_'.$key,$data)) ? $data['defered_symbol_'.$key] : '$',['data-validation'=> '' , 'class'=>'select-val','style'=>'text-transform: capitalize;outline: none;width: 50px!important;border: 0;text-align: center;float: left;padding: 0!important;height: auto;font-size: 14px;margin: 0;line-height: initial;margin: 0;','readonly'=>true]) }}

                                                                    <p style="margin:0;padding: 0;position: absolute;left:31px;bottom:0px;" class="text-center"><i class="fa fa-angle-down"></i></p>
                                                                </span>
                                                            </div>
                                                        </div>
                                                         {{ Form::input('text','answer[215][copy]['.$key.'][concat_defered_symbol_'.$key.']', (array_key_exists('concat_defered_symbol_'.$key,$data)) ? $data['stock_option_stock_symbol_'.$key] . $data['defered_symbol_'.$key]  :null ,['class'=>'hidden concatenate']) }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </fieldset>
                            <h3></h3>
                            <fieldset>
                                <section class="sections">
                                    <div class="col-sm-12">
                                        <div class="col-md-4 col-xs-11 section-left">
                                            <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                                            <h2>Additional Executive Compensation</h2>
                                        </div>
                                        <div class="col-sm-6 col-sm-offset-1 section-right">
                                            {{ Form::input('hidden','questionName[215]','Do you have other Executive Compensation you want to include?') }}
                                            <div class="col-sm-12 inner-left">
                                                {{ Form::label('label', 'Do you have other Executive Compensation you want to include?') }}
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][other_executive_compensation_'.$key.']', 'yes',(array_key_exists('other_executive_compensation_'.$key,$data) && $data['other_executive_compensation_'.$key] == "yes") ? true : false,['class'=>'other-executive-compensation','required'=>'required']) }}Yes
                                                    <span class="radio-icon"></span>
                                                </label>
                                                <label class="radio-custom-label">
                                                    {{ Form::radio('answer[215][copy]['.$key.'][other_executive_compensation_'.$key.']', 'no',(array_key_exists('other_executive_compensation_'.$key,$data) && $data['other_executive_compensation_'.$key] == "no") ? true : false,['class'=>'other-executive-compensation','required'=>'required']) }}No
                                                    <span class="radio-icon"></span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </section>
                            </fieldset>
        <?php
    }
}
?>
                    <div id="modal-section" >
                        <div class="sections">
                            <div id="exeCompResStockModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD RESTRICTED STOCK GRANT</h4>
                                        </div>
                                        <div class="modal-body" style="overflow: hidden;">
                                            <div class="row">
                                                <!-- first step starts -->
                                                <div class="setup-content" id="step-1">
                                                    <div class="section-right">
                                                        <!-- tabbed pannel starts -->
                                                        <div class="latest-statement">

                                                            {{ Form::input('hidden','questionName[215]','ADD RESTRICTED STOCK GRANT') }}
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Grant Name') }}
                                                                {{ Form::input('text','grant_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control exe-compen-grant-name', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Grant date') }}
                                                                {{ Form::input('text','executive_compensation_grant_date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker exe-compen-grant-date', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Stock symbol') }}
                                                                {{ Form::input('text','stock_symbol', null,['data-validation'=> '' , 'class'=>'custom-validation form-control exe-compen-stock-symbol', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>


                                                            <div class="col-sm-12 inner-left"> 
                                                                {{ Form::label('label', 'Number of shares granted') }}
                                                                {{ Form::input('number','numbers_share_granted', null,['data-validation'=> '' , 'class'=>'custom-validation form-control exe-compen-numbers-share-granted', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Vesting schedule in number of years untill all grants have vested.') }}
                                                                <div class="input-group">
                                                                    {{ Form::input('number','grants_vested', null,['data-validation'=> '' , 'class'=>'borderRight0 custom-validation form-control exe-compen-vested-grants', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'']) }}
                                                                    <span class="input-group-addon">yrs.</span>
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Does the grant fullvest in case of death?') }}
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('grant_full_vest_death_case', 'yes',false,['class'=>'exe-compen-grant-full-vest-death-case','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('grant_full_vest_death_case', 'no',false,['class'=>'exe-compen-grant-full-vest-death-case','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <button data-number="" id="exe-compen-res-finish-new" class='finishBtn pull-right' type="button" style="border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px; padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-top: 40px; margin-bottom: 40px;">
                                                                    Finish
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="stockOptionModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD STOCK OPTION GRANT</h4>
                                        </div>
                                        <div class="modal-body" style="overflow: hidden;">
                                            <div class="row">
                                                <!-- first step starts -->
                                                <div class="setup-content">
                                                    <div class="section-right">
                                                        <!-- tabbed pannel starts -->
                                                        <div class="latest-statement">
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Grant name') }}
                                                                {{ Form::input('text','stock_option_grant_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control stock-option-grant-name', 'data-rule-regex'=>"true", 'required'=>true ]) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Grant date') }}
                                                                {{ Form::input('text','stock_option_grant_date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker stock-option-grant-date', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Stock symbol') }}
                                                                {{ Form::input('text','stock_option_stock_symbol', null,['data-validation'=> '' , 'class'=>'custom-validation form-control stock-option-stock-symbol', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Type') }}
                                                                <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 10%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                                <select style="position: absolute;" class="status valid stock-option-type"  name="stock-option-type" required="" aria-invalid="false">
                                                                    <option selected="" disabled="" value="">SELECT</option>
                                                                    <option value="1">ISO</option>
                                                                    <option value="2">NQO</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Number of options granted') }}
                                                                {{ Form::input('number','stock_option_numbers_option_granted', null,['data-validation'=> '' , 'class'=>'custom-validation form-control stock-option-numbers-option-granted', 'data-rule-regex'=>"true", 'required'=>true]) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Number of options exercised to date.') }}
                                                                {{ Form::input('number','stock_option_grants_vested', null,['data-validation'=> '' , 'class'=>'custom-validation form-control stock-option-vested-grants-date', 'data-rule-regex'=>"true", 'required'=>true]) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Expiration date of grant') }}
                                                                {{ Form::input('text','stock_option_expiration_grant_date',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker stock-option-expiration-grant-date', 'data-rule-regex'=>"true", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Grant price.') }}
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">$</span>
                                                                {{ Form::input('number','stock_option_grant_price', null,['data-validation'=> '' , 'class'=>'borderLeft0 custom-validation form-control stock-option-grant-price', 'data-rule-regex'=>"true", 'required'=>true]) }}
                                                                </div>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Vesting schedule:') }}
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('stock_option_vesting_schedule', 'vestingpercentage',false,['class'=>'stockOptionGrantVestingSchedule','required'=>'required']) }}%  per year
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('stock_option_vesting_schedule', 'vestingOther',false,['class'=>'stockOptionGrantVestingSchedule','required'=>'required']) }}Other
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12 inner-left">
                                                                {{ Form::label('label', 'Does the grant fullvest in case of death?') }}
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('stock_option_grant_full_vest_death_case', 'yes',false,['class'=>'stock-option-grant-full-vest-death-case','required'=>'required']) }}Yes
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                                <label class="radio-custom-label">
                                                                    {{ Form::radio('stock_option_grant_full_vest_death_case', 'no',false,['class'=>'stock-option-grant-full-vest-death-case','required'=>'required']) }}No
                                                                    <span class="radio-icon"></span>
                                                                </label>
                                                            </div>

                                                            <div class="col-sm-12">
                                                                <button data-number="" id="stock-option-res-finish-new" class='finishBtn pull-right' type="button" style="border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px; padding: 12px 35px; background-color: #2179EE; text-decoration: none; border: none; margin-top: 40px; margin-bottom: 40px;">
                                                                    Finish
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> 
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script id="spouseStep1" type="text/html">
    <section class="sections">
        <div class="col-sm-12">
            <div class="col-md-4 col-xs-11 section-left">
                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                <h2>Executive compensation</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right">
                {{ Form::input('hidden','questionName[215]','Do you receive executive compensation (Restricted stock, Stock Options)?') }}
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Do you receive executive compensation (Restricted stock, Stock Options)?') }}
                    <label class="radio-custom-label">
                        <input class="exe-comp-res-first-stock-confirmation" required="required" name="answer[215][copy][<%= copynum %>][exe_compen_first_confirmation_stock_option_confirmation_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="exe-comp-res-first-stock-confirmation" required="required" name="answer[215][copy][<%= copynum %>][exe_compen_first_confirmation_stock_option_confirmation_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="spouseStep2" type="text/html">
    <section class="sections">
        <div class="col-sm-12">
            <div class="col-md-4 col-xs-11 section-left">
                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                <h2>Executive compensation</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Do you receieve Restricted stock?') }}
                    <label class="radio-custom-label">
                        <input class="exe-comp-res-stock-confirmation table-confirmation" required="required" name="answer[215][copy][<%= copynum %>][exe_compen_confirmation_stock_option_confirmation_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="exe-comp-res-stock-confirmation table-confirmation" required="required" name="answer[215][copy][<%= copynum %>][exe_compen_confirmation_stock_option_confirmation_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-12 inner-left exe-compen-res-stock-detail-table" style="display: none;" >
                    <div class="col-sm-12  ">
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="row">
                                    <div class="add-child add-property restricted-stock">

                                        <table id='exe-compen-res-stock-property-info'>
                                            <thead>
                                                <tr>
                                                    <td colspan="2">List your restricted stock grant information.</td>
                                                </tr>
                                                <tr>
                                                    <td>Grant name</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody data-copy="<%= copynum %>" >

                                            </tbody>
                                        </table>
                                        <div class="col-md-5 ">
                                            <div class="row">
                                                <button type='button' data-copy="<%= copynum %>" class="add-dependent add-grant" data-toggle="modal" data-target="#exeCompResStockModal">Add grant</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <textarea id="ExeCompResStockData" class="hidden" name="answer[215][copy][<%= copynum %>][ExeCompResStockRecords_<%= copynum %>]"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="spouseStep3" type="text/html">
    <section class="sections">
        <div class="col-sm-12">
            <div class="col-md-4 col-xs-11 section-left">
                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                <h2>Executive compensation</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Do you receieve stock options?') }}
                    <label class="radio-custom-label">
                        <input class="stock-option-confirmation" required="required" name="answer[215][copy][<%= copynum %>][stock_option_confirmation_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="stock-option-confirmation" required="required" name="answer[215][copy][<%= copynum %>][stock_option_confirmation_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-12 inner-left stock-option-confirmation-table" style="display: none;" >
                    <div class="col-sm-12  ">
                        <div class="row">
                            <div class="col-sm-12 ">
                                <div class="row">
                                    <div class="add-child add-property stock-options">

                                        <table id='stock-option-property-info'>
                                            <thead>
                                                <tr>
                                                    <td colspan="2">List your stock option grant information.</td>
                                                </tr>
                                                <tr>
                                                    <td>Grant name</td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </thead>
                                            <tbody data-copy="<%= copynum %>">
                                            </tbody>
                                        </table>
                                        <div class="col-md-5 ">
                                            <div class="row">
                                                <button type='button' class="add-stock-option-grant add-dependent" data-toggle="modal" data-target="#stockOptionModal" data-copy="<%= copynum %>">Add grant</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <textarea id="ExeCompenStockOptionData216" class="hidden" name="answer[215][copy][<%= copynum %>][ExeCompStockOptionRecords_<%= copynum %>]"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
</script>

<script id="spouseStep4" type="text/html">


    <section class="sections">
        <div class="col-sm-12">
            <div class="col-md-4 col-xs-11 section-left">
                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                <h2>Executive compensation</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Do you participate in a Deferred Compensation Plan?') }}
                    <label class="radio-custom-label">
                        <input class="deffered-amount-annually" required="required" name="answer[215][copy][<%= copynum %>][deffered_amount_annually_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="deffered-amount-annually" required="required" name="answer[215][copy][<%= copynum %>][deffered_amount_annually_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-12 inner-left deffered-amount-annually-confirmation" style="display:  none;" >
                    <div class="col-sm-12  ">
                        <div class="row">
                            <div class="inner-left">
                                {{ Form::label('label', 'What percentage or dollar amount due you defer annually?') }} 



                                <div class="input-group service-input-group coverage-input-group" style="margin-bottom:0 !important;">
                                    <input type="number" name="answer[215][copy][<%= copynum %>][stock_option_stock_symbol_<%= copynum %>]" class="custom-validation form-control stock-option-stock-symbol full-width borderRight0" required='required' placeholder='' style="
                                           padding-right: 0px! important;
                                           ">
                                    <span class="input-group-addon" style="position: relative;padding: 0 10px;">
                                        <p style="position: absolute;margin: 0;padding: 0;top: 0;z-index: 100; left:31px;" class=" text-center"><i class="fa fa-angle-up"></i></p>

                                        <input type="text" name="answer[215][copy][<%= copynum %>][defered_symbol_<%= copynum %>]" class="select-val" required='required' style="text-transform: capitalize;outline: none;width: 50px!important;border: 0;text-align: center;float: left;padding: 0!important;height: auto;font-size: 14px;margin: 0;line-height: initial;margin: 0;" value="$" readonly="">
                                        <input type="text" name="answer[215][copy][<%= copynum %>][concat_defered_symbol_<%= copynum %>]" class="hidden concatenate" required='required' value="" readonly="">
                                        <p style="margin:0;padding: 0;position: absolute;left:31px;bottom:0px;" class="text-center"><i class="fa fa-angle-down"></i></p>
                                    </span>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</script>
<script id="spouseStep5" type="text/html">
    <section class="sections">
        <div class="col-sm-12">
            <div class="col-md-4 col-xs-11 section-left">
                <h6 style="margin-bottom: 0; text-transform: uppercase;">{{session::get('loggedInUserName')}} EXECUTIVE COMPENSATION</h6>
                <h2>Executive compensation</h2>
            </div>
            <div class="col-sm-6 col-sm-offset-1 section-right">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Do you have other Executive Compensation you want to include?') }}
                    <label class="radio-custom-label">
                        <input class="other-executive-compensation" required="required" name="answer[215][copy][<%= copynum %>][other_executive_compensation_<%= copynum %>]" type="radio" value="yes" aria-required="true" data-value="<%= copynum %>">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="other-executive-compensation" required="required" name="answer[215][copy][<%= copynum %>][other_executive_compensation_<%= copynum %>]" type="radio" value="no" aria-required="true" data-value="<%= copynum %>">No
                        <span class="radio-icon"></span>
                    </label>
                </div>
            </div>
        </div>
    </section>
</script>
<script>
    var ajaxUrl = "{{route('frontend.client.taxBracket', config('constant.subdomain'))}}";</script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,13,'spouse-employment-income-retirement-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var tempArray = <?php echo json_encode($answer); ?>;
        if (!($.isEmptyObject(tempArray.copy))) {
            copynumber = tempArray.copy.length - 1;
        } else {
            var copynumber = 0;
            appendSteps(copynumber);
        }

        function appendSteps(copynumber)
        {
            var plan1 = $('#spouseStep1').html();
            var plan2 = $('#spouseStep2').html();
            var plan3 = $('#spouseStep3').html();
            var plan4 = $('#spouseStep4').html();
            var plan5 = $('#spouseStep5').html();
            html1 = _.template(plan1);
            html2 = _.template(plan2);
            html3 = _.template(plan3);
            html4 = _.template(plan4);
            html5 = _.template(plan5);
            var cind = form.steps('getCurrentIndex');
            $("form").steps("add", {
                content: html1({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
            $("form").steps("add", {
                content: html2({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
            $("form").steps("add", {
                content: html3({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
            $("form").steps("add", {
                content: html4({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
            $("form").steps("add", {
                content: html5({count: $('#services-question-form fieldset').length + 1000, copynum: copynumber}),
            });
        }
        function removeSteps() {
            var copyind = form.steps('getCurrentIndex');
            var totalSteps = $('#services-question-form fieldset').length;
            if (totalSteps > copyind) {
                var len = totalSteps - copyind;
                for (var i = 1; i <= len; i++) {
                    var index = copyind + 1;
                    $("form").steps("remove", index);
                }
            }
        }

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        //-------------------- jquery for exectuive compensation restrict stock section[215] starts here ---------------------------------
        $(document).on('change', '.exe-comp-res-stock-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('.exe-compen-res-stock-detail-table').show();
            } else {
                $('.exe-compen-res-stock-detail-table').hide();
                //                exeCompenResStockList = [];
                //                $('#ExeCompResStockData').html(JSON.stringify(exeCompenResStockList));
            }
        });

        $('#exeCompResStockModal .nextBtn, #exeCompResStockModal .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    var exeCompenResStockList = [];
                    var copyVal = $(this).attr('data-number');
                    var exeCompenResStockListDataVal = $('textarea[name="answer[215][copy][' + copyVal + '][ExeCompResStockRecords_' + copyVal + ']"]').val();
                    if (exeCompenResStockListDataVal.length > 0) {
                        exeCompenResStockList = JSON.parse(exeCompenResStockListDataVal);
                    }
                    if ($(this).hasClass('edit-form')) {
                        exeCompenResStockList[$(this).attr('data-index')] = {
                            exeCompResStockConfirmationfirstCheck: $('.exe-comp-res-stock-confirmation:checked').val(),
                            executiveCompensationGrantName: $('#exeCompResStockModal .exe-compen-grant-name').val(),
                            executiveCompensationGrantDate: $('#exeCompResStockModal .exe-compen-grant-date').val(),
                            executiveCompensationStockSymbol: $('#exeCompResStockModal .exe-compen-stock-symbol').val(),
                            executiveCompensationNumbersShareGranted: $('#exeCompResStockModal .exe-compen-numbers-share-granted').val(),
                            executiveCompensationVestedGrants: $('#exeCompResStockModal .exe-compen-vested-grants').val(),
                            executiveCompensationGrantFullVestDeathCase: $('#exeCompResStockModal .exe-compen-grant-full-vest-death-case:checked').val()
                        };
                        $('.restricted-stock table tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#exeCompResStockModal .exe-compen-grant-name').val() + '</td>  <td></td><td valign="top"  style="text-align:right; width:80px" ><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('.restricted-stock table tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + exeCompenResStockList.length + '><td>' + $('#exeCompResStockModal .exe-compen-grant-name').val() + '</td> <td></td> <td valign="top"  style="text-align:right; width:80px"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        exeCompenResStockList.push({
                            exeCompResStockConfirmationfirstCheck: $('.exe-comp-res-stock-confirmation:checked').val(),
                            executiveCompensationGrantName: $('#exeCompResStockModal .exe-compen-grant-name').val(),
                            executiveCompensationGrantDate: $('#exeCompResStockModal .exe-compen-grant-date').val(),
                            executiveCompensationStockSymbol: $('#exeCompResStockModal .exe-compen-stock-symbol').val(),
                            executiveCompensationNumbersShareGranted: $('#exeCompResStockModal .exe-compen-numbers-share-granted').val(),
                            executiveCompensationVestedGrants: $('#exeCompResStockModal .exe-compen-vested-grants').val(),
                            executiveCompensationGrantFullVestDeathCase: $('#exeCompResStockModal .exe-compen-grant-full-vest-death-case:checked').val()
                        });
                    }
                    $('textarea[name="answer[215][copy][' + copyVal + '][ExeCompResStockRecords_' + copyVal + ']"]').html(JSON.stringify(exeCompenResStockList));
                }
                $('#exeCompResStockModal').modal('hide');
            }
            return false;
        });
        $(document).on('click', '#exe-compen-res-stock-property-info .fa-pencil', function () {
            $('#exeCompResStockModal').modal();
            var copyArr = $(this).closest('table tbody').attr('data-copy');
            $("#exe-compen-res-finish-new").attr("data-number", copyArr);
            exeCompenResStockList = JSON.parse($('textarea[name="answer[215][copy][' + copyArr + '][ExeCompResStockRecords_' + copyArr + ']"]').val());
            var index = $(this).closest('tr').attr('data-index');
            exeCompenResStockDetails = exeCompenResStockList[index];
            $('exe-comp-res-stock-confirmation:checked').val(exeCompenResStockDetails.exeCompResStockConfirmationfirstCheck);
            $('#exeCompResStockModal .exe-compen-grant-name').val(exeCompenResStockDetails.executiveCompensationGrantName);
            $('#exeCompResStockModal .exe-compen-grant-date').val(exeCompenResStockDetails.executiveCompensationGrantDate);
            $('#exeCompResStockModal .exe-compen-stock-symbol').val(exeCompenResStockDetails.executiveCompensationStockSymbol);
            $('#exeCompResStockModal .exe-compen-numbers-share-granted').val(exeCompenResStockDetails.executiveCompensationNumbersShareGranted);
            $('#exeCompResStockModal .exe-compen-vested-grants').val(exeCompenResStockDetails.executiveCompensationVestedGrants);
            $('#exeCompResStockModal .exe-compen-grant-full-vest-death-case[value=' + exeCompenResStockDetails.executiveCompensationGrantFullVestDeathCase + ']').prop('checked', true);
            $('#exe-compen-res-finish-new ').addClass('edit-form');
            $('#exe-compen-res-finish-new').attr('data-index', index);
        });

        $(document).on('click', '.add-grant', function () {
            var copyArr = $(this).attr('data-copy');
            $('#exeCompResStockModal .error-alert').remove();
            $('#exe-compen-res-finish-new').removeClass('edit-form');
            $('#exeCompResStockModal  .executiveCompensationGrantName, #exeCompResStockModal input[type="text"], #exeCompResStockModal input[type="number"]').val('');
            $('#exeCompResStockModal input[type="radio"]').prop('checked', false);
            $("#exe-compen-res-finish-new").attr("data-number", $(this).attr('data-copy'));
        });
        $(document).on('click', '#exe-compen-res-stock-property-info .fa-trash', function () {
            var copyArr = $(this).closest('table tbody').attr('data-copy');
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                exeCompenResStockList = JSON.parse($('textarea[name="answer[215][copy][' + copyArr + '][ExeCompResStockRecords_' + copyArr + ']"]').val());
                exeCompenResStockList.splice(index_id, 1);
                $(this).closest('table tbody').empty();
                if (exeCompenResStockList.length != 0) {
                    var tr = '';
                    $.each(exeCompenResStockList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.executiveCompensationGrantName + '</td><td></td><td valign="top"  style="text-align:right; width:80px;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>';
                    });
                    $(this).closest('table tbody').html(tr);
                }
                $('textarea[name="answer[215][copy][' + copyArr + '][ExeCompResStockRecords_' + copyArr + ']"]').html(JSON.stringify(exeCompenResStockList));
            });
        });

        $(document).on('change', '.stock-option-confirmation', function () {
            if ($(this).val() === 'yes') {
                $('.stock-option-confirmation-table').show();
            } else {
                $('.stock-option-confirmation-table').hide();
                //                stockOptionList = [];
                //                stockOptionList = JSON.parse($('#ExeCompenStockOptionData216').val());
            }
        });

        $('#stockOptionModal .nextBtn, #stockOptionModal .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid()) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    var stockOptionList = [];
                    var copyVal = $(this).attr('data-number');
                    var exeCompStockOptionVal = $('textarea[name="answer[215][copy][' + copyVal + '][ExeCompStockOptionRecords_' + copyVal + ']"]').val();
                    if (exeCompStockOptionVal.length > 0) {
                        stockOptionList = JSON.parse(exeCompStockOptionVal);
                    }
                    if ($(this).hasClass('edit-form')) {
                        stockOptionList[$(this).attr('data-index')] = {
                            stockOptionConfirmationFirstCheck: $('.stock-option-confirmation:checked').val(),
                            stockOptionGrantName: $('#stockOptionModal .stock-option-grant-name').val(),
                            stockOptionGrantDate: $('#stockOptionModal .stock-option-grant-date').val(),
                            stockOptionStockSymbol: $('#stockOptionModal .stock-option-stock-symbol').val(),
                            stockOptionType: $('#stockOptionModal .stock-option-type').val(),
                            stockOptionNumbersOptionGranted: $('#stockOptionModal .stock-option-numbers-option-granted').val(),
                            stockOptionVestedGrantsDate: $('#stockOptionModal .stock-option-vested-grants-date').val(),
                            stockOptionExpirationGrantDate: $('#stockOptionModal .stock-option-expiration-grant-date').val(),
                            stockOptionGrantPrice: $('#stockOptionModal .stock-option-grant-price').val(),
                            stockOptionGrantVestingSchedule: $('.stockOptionGrantVestingSchedule:checked').val(),
                            stockOptionGrantFullVestDeathCase: $('.stock-option-grant-full-vest-death-case:checked').val()
                        };
                        $('.stock-options table tbody[data-copy=' + copyVal + '] tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#stockOptionModal .stock-option-grant-name').val() + '</td>  <td valign="top"  style="text-align:right;"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                    } else {
                        $('.stock-options table tbody[data-copy=' + copyVal + ']').append('<tr data-index=' + stockOptionList.length + '><td>' + $('#stockOptionModal .stock-option-grant-name').val() + '</td>  <td valign="top"  style="text-align:right;"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                        stockOptionList.push({
                            stockOptionConfirmationFirstCheck: $('.stock-option-confirmation:checked').val(),
                            stockOptionGrantName: $('#stockOptionModal .stock-option-grant-name').val(),
                            stockOptionGrantDate: $('#stockOptionModal .stock-option-grant-date').val(),
                            stockOptionStockSymbol: $('#stockOptionModal .stock-option-stock-symbol').val(),
                            stockOptionType: $('#stockOptionModal .stock-option-type').val(),
                            stockOptionNumbersOptionGranted: $('#stockOptionModal .stock-option-numbers-option-granted').val(),
                            stockOptionVestedGrantsDate: $('#stockOptionModal .stock-option-vested-grants-date').val(),
                            stockOptionExpirationGrantDate: $('#stockOptionModal .stock-option-expiration-grant-date').val(),
                            stockOptionGrantPrice: $('#stockOptionModal .stock-option-grant-price').val(),
                            stockOptionGrantVestingSchedule: $('.stockOptionGrantVestingSchedule:checked').val(),
                            stockOptionGrantFullVestDeathCase: $('.stock-option-grant-full-vest-death-case:checked').val()
                        });
                    }
                    $('textarea[name="answer[215][copy][' + copyVal + '][ExeCompStockOptionRecords_' + copyVal + ']"]').html(JSON.stringify(stockOptionList));
                    $('#stockOptionModal').modal('hide');
                }
            }
            return false;
        });
        $(document).on('click', '#stock-option-property-info .fa-pencil', function () {
            $('#stockOptionModal').modal();
            var copyArr = $(this).closest('table tbody').attr('data-copy');
            $("#stock-option-res-finish-new").attr("data-number", copyArr);
            stockOptionList = JSON.parse($('textarea[name="answer[215][copy][' + copyArr + '][ExeCompStockOptionRecords_' + copyArr + ']"]').val());
            var index = $(this).closest('tr').attr('data-index');
            stockOptionDetails = stockOptionList[index];
            $('stock-option-confirmation:checked').val(stockOptionDetails.stockOptionConfirmationFirstCheck);
            $('#stockOptionModal .stock-option-grant-name').val(stockOptionDetails.stockOptionGrantName);
            $('#stockOptionModal .stock-option-grant-date').val(stockOptionDetails.stockOptionGrantDate);
            $('#stockOptionModal .stock-option-stock-symbol').val(stockOptionDetails.stockOptionStockSymbol);
            $('#stockOptionModal .stock-option-type').val(stockOptionDetails.stockOptionType).trigger('change');
            $('#stockOptionModal .stock-option-numbers-option-granted').val(stockOptionDetails.stockOptionNumbersOptionGranted);
            $('#stockOptionModal .stock-option-vested-grants-date').val(stockOptionDetails.stockOptionVestedGrantsDate);
            $('#stockOptionModal .stock-option-expiration-grant-date').val(stockOptionDetails.stockOptionExpirationGrantDate);
            $('#stockOptionModal .stock-option-grant-price').val(stockOptionDetails.stockOptionGrantPrice);
            $('#stockOptionModal .stockOptionGrantVestingSchedule[value=' + stockOptionDetails.stockOptionGrantVestingSchedule + ']').prop('checked', true);
            $('#stockOptionModal .stock-option-grant-full-vest-death-case[value=' + stockOptionDetails.stockOptionGrantFullVestDeathCase + ']').prop('checked', true);
            $('#stock-option-res-finish-new ').addClass('edit-form');
            $('#stock-option-res-finish-new').attr('data-index', index);
        });
        $(document).on('click', '.add-stock-option-grant', function () {
            $('#stockOptionModal .error-alert').remove();
            $('#stock-option-res-finish-new').removeClass('edit-form');
            $('#stockOptionModal  .semi-annual-mortgage-monthly-total, #stockOptionModal input[type="text"], #stockOptionModal input[type="number"]').val('');
            $('#stockOptionModal input[type="radio"]').prop('checked', false);
            $('#stockOptionModal .stock-option-type').val('').trigger('change');
            $("#stock-option-res-finish-new").attr("data-number", $(this).attr('data-copy'));
        });
        $(document).on('click', '#stock-option-property-info .fa-trash', function () {
            var copyArr = $(this).closest('table tbody').attr('data-copy');
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                stockOptionList = JSON.parse($('textarea[name="answer[215][copy][' + copyArr + '][ExeCompStockOptionRecords_' + copyArr + ']"]').val());
                stockOptionList.splice(index_id, 1);
                $(this).closest('table tbody').empty();
                if (stockOptionList.length != 0) {
                    var tr = '';
                    $.each(stockOptionList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.stockOptionGrantName + '</td> <td valign="top"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i></td> <td valign="top"><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>';
                    });
                    $(this).closest('table tbody').html(tr);
                }

                $('textarea[name="answer[215][copy][' + copyArr + '][ExeCompStockOptionRecords_' + copyArr + ']"]').html(JSON.stringify(stockOptionList));
            });
        });

        $('a[href="#finish"]').on('click', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-executive-compensation:checked').val() == 'no' || $('a[href="#finish"]').text() == 'Continue') {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('Finish');
            }
        });

        $(document).on('change', '.other-executive-compensation', function () {
            $("select").selectBoxIt();
            if ($(this).val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                removeSteps();

            }
        });

        $(document).on('click', 'a[href="#finish"]', function () {
            if ($(this).parent().parent().parent().parent().children('.content').find('fieldset').last().find('.other-executive-compensation:checked').val() == 'yes') {
                $('a[href="#finish"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                $('a[href="#next"]').text('next').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
                 copynumber++;
                appendSteps(copynumber);
                $(form).steps('next');
            } else {
                $('a[href="#finish"]').text('finish').append('<i style="background :#ffffff; font-size:15px; padding-left:6px; "  class="fa fa-arrow-right"></i>');
            }
        });

        $(document).on('click', '.deffered-amount-annually', function () {
            if ($(this).val() == "yes") {
                $('.deffered-amount-annually-confirmation').show();
            } else {
                $('.deffered-amount-annually-confirmation').hide();
            }
        });
        $(document).on('click', '.fa-angle-up', function () {
            $('.select-val').val('%');
            $(this).parent().parent().parent().children().attr({min: "0", max: "100", maxlength: "3"});
        });
        $(document).on('click', '.fa-angle-down', function () {
            $('.select-val').val('$');
            $(this).parent().parent().parent().children().removeAttr('min').removeAttr('max').removeAttr('maxlength');
        });
    });

</script>

@stop
