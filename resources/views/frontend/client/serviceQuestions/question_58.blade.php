<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Medical Directive</h2>
        <p>It's never to early to consider your legacy. The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[58]','Medical Directive') }}

        <div class="col-sm-6 inner-left ">

            {{ Form::label('label', 'Do you have a medical directive?') }}
            <label class="radio-custom-label">
                {{ Form::radio('answer[58][medical_directive]', 'yes',(!empty($answer) && array_key_exists('medical_directive',$answer)) ? (($answer['medical_directive']=="yes")  ? true : false):false,['class'=>'medical-directive' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[58][medical_directive]', 'no',(!empty($answer) && array_key_exists('medical_directive',$answer)) ? (($answer['medical_directive']=="no")  ? true : false):false,['class'=>'medical-directive' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>

        </div>
        <div class="spouse-section">
            <div class="col-sm-6 inner-left ">

                {{ Form::label('label', 'Does your spouse/partner have a medical directive?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('answer[58][spouse_medical_directive]', 'yes',(!empty($answer) && array_key_exists('spouse_medical_directive',$answer)) ? (($answer['spouse_medical_directive']=="yes")  ? true : false):false,['class'=>'spouse-medical-directive' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('answer[58][spouse_medical_directive]', 'no',(!empty($answer) && array_key_exists('spouse_medical_directive',$answer)) ? (($answer['spouse_medical_directive']=="no")  ? true : false):false,['class'=>'spouse-medical-directive' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
        </div>
    </div>
</div>