 @extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
@include('frontend.includes.contact')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',204) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <h2>Okay, let's talk about this goal.</h2>
                                    <p>This helps us understand your complete financial picture, so we can provide the best guidance.
                                        if you need help, simply <a href=""data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[40]','Okay, let\'s talk about this goal.') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Is this one time expense, or anually recurring?') }}
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[40][one_time_expense]', 'one-time',(!empty($answer) && array_key_exists('one_time_expense',$answer)) ? (($answer['one_time_expense']=="one-time")  ? true : false) :false ,['class'=>'one-time-expense','required'=>true ]) }}One-time
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">
                                            {{ Form::radio('answer[40][one_time_expense]', 'annual',(!empty($answer) && array_key_exists('one_time_expense',$answer)) ? (($answer['one_time_expense']=="annual")  ? true : false) :false,['class'=>'one-time-expense','required'=>true ]) }}Annual
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-8 inner-left need-per-year" style="display:<?php
                                    if (!empty($answer) && array_key_exists('one_time_expense', $answer) && ($answer['one_time_expense'] == "annual")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do you anticipate needing per year for this expense?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[40][per_year_expense]',(!empty($answer) && array_key_exists('per_year_expense',$answer)) ? $answer['per_year_expense'] : '',['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left number-of-years" style="display: <?php
                                    if (!empty($answer) && array_key_exists('one_time_expense', $answer) && ($answer['one_time_expense'] == "annual")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How many years?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[40][how_many_years]',(!empty($answer) && array_key_exists('how_many_years',$answer)) ? $answer['how_many_years']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left expense-amount" style="display:  <?php
                                    if (!empty($answer) && array_key_exists('one_time_expense', $answer) && ($answer['one_time_expense'] == "one-time")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>;">
                                        {{ Form::label('label', 'How much do you need for this one-time expense?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[40][need_for_one_time_expense]',(!empty($answer) && array_key_exists('need_for_one_time_expense',$answer)) ? $answer['need_for_one_time_expense']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control  borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'When do you expect to pay(or begin to pay) for this expense?') }}
                                        {{ Form::input('text','answer[40][expect_pay_for_expense]',(!empty($answer) && array_key_exists('expect_pay_for_expense',$answer)) ? $answer['expect_pay_for_expense']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control pay-for-expense-datetimepicker', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                    </div>
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'How much of your investment assets have you already earmarked just for this goal?') }}
                                        <div class="input-group service-input-group">
                                            <span class="input-group-addon"> $ </span>
                                            {{ Form::input('number','answer[40][investment_assets_earmarked]',(!empty($answer) && array_key_exists('investment_assets_earmarked',$answer)) ? $answer['investment_assets_earmarked']:null,['data-validation'=> '' , 'class'=>'custom-validation form-control borderLeft0 comprehensive-width', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>
                    <?php if (session::get('your_goal') == 0) { ?>
                        <h3></h3> 
                        <fieldset>
                            <section class="sections">
                                <div class="col-sm-12 ">
                                    <div class="col-md-4 col-xs-11 section-left">
                                        <h2>Planning for educational expenses.</h2>
                                        <p>You indicated that you are planning for education expenses. Enter the beginning year of each child and to which school do they plan
                                            to attend,if known.If you need help, simply <a href=""data-target="#get-started-modal" data-toggle='modal'>contact us.</a></p>
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-1 section-right section-size">
                                        {{ Form::input('hidden','questionName[38]','Planning for educational expenses') }}
                                        <div class="information-table">

                                            <table id='children-info1' class='information-table'>
                                                <thead>
                                                    <tr>
                                                        <td>Student</td>
                                                        <td>Age</td>
                                                        <td>Beginning year</td>
                                                        <td>Level</td>
                                                        <td>School Name</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (!empty($answer["educationalExpensesRecords"])) {
                                                        $expenseData = json_decode($answer["educationalExpensesRecords"]);
                                                        if (!empty($expenseData)) {
                                                            foreach ($expenseData as $key => $data) {
                                                                ?>
                                                                <tr data-index="{{$key}}">
                                                                    <td>{{$data->name}}</td>
                                                                    <td>{{$data->age}}</td>
                                                                    <td>{{$data->year}}</td>
                                                                    <td>{{$data->level}}</td>
                                                                    <td>{{$data->school}}</td>
                                                                    <td><i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i></td>
                                                                </tr>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <button type='button' class='add-dependent' data-toggle="modal" data-target="#myModal">Add Child</button>
                                        </div>
                                        <textarea id="educationalExpensesData" class="hidden" name="answer[38][educationalExpensesRecords]">{{(!empty($answer["educationalExpensesRecords"]))? $answer["educationalExpensesRecords"]:null}}</textarea>
                                    </div>
                                    <!--{{ Form::button('add dependent') }}-->
                                </div>


                            </section>
                        </fieldset>
                        <!--<a id="returnLater" href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}">Save and return later</a>-->

                    <?php } ?>
                    <div id="modal-section">
                        <div class="sections">
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD CHILD/DEPENDENT</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="setup-content">
                                                    <div class="section-right">
                                                        {{ Form::input('hidden','data[38][questionName]','ADD CHILD/DEPENDENT') }}
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Students\'s name/nickname') }}
                                                            <input data-validation="" class="custom-validation form-control student-name" data-rule-regex="false" required="" placeholder="name" name="child_name" type="text">
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Students\'s current age') }}
                                                            <input data-validation="" style="text-align: left;" class="text-left custom-validation form-control student-age" data-rule-regex="false" required="" placeholder="age" name="child_age" type="number" min="0" max="100">
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Beginning Year') }}
                                                            <input data-validation="" class="custom-validation form-control beginning-year" data-rule-regex="false" required="" placeholder="YYYY" name="beginning_year" type="text">
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Level') }}
                                                            <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                            <select style="position: absolute;" name="level_of_education" class='level' required>
                                                                <option value="0">SELECT</option>
                                                                <option value="Early Childhood">Early Childhood</option>
                                                                <option value="Primary">Primary</option>
                                                                <option value="Secondary">Secondary</option>
                                                                <option value="Trade school">Trade school</option>
                                                                <option value="Undergraduate">Undergraduate</option>
                                                                <option value="Graduate">Graduate</option>
                                                                <option value="Juris Doctor">Juris Doctor</option>
                                                                <option value="Medical school">Medical school</option>
                                                                <option value="Doctoral">Doctoral</option>
                                                                <option value="other">other</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'School name(if known)') }}
                                                            <input data-validation="" class="custom-validation form-control school-name" data-rule-regex="false" required="" placeholder="school name" name="school_name" type="text">
                                                        </div>

                                                        <div class="col-sm-12 inner-left">
                                                            <button id='studentform' class='' type="button" style="border-radius: 3px;
                                                                    color: #fff;
                                                                    font-family: Lato;
                                                                    font-size: 18px;
                                                                    line-height: 24px;
                                                                    text-align: center;
                                                                    width: 150px;
                                                                    padding: 12px 35px;
                                                                    background-color: #2179EE;
                                                                    text-decoration: none;
                                                                    border: none;
                                                                    margin-left: 62px;
                                                                    margin-bottom: 40px;">
                                                                Save
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>

@stop
@section('after-scripts')
<script src="{{ asset('js/goal-specific-financial-planning.js') }}"></script>
<script>


//        if ($("#services-question-form-p-0").is(':visible')) {
//            $('a[href="#previous"]').attr('onclick', "window.location.href='" + backUrl + "'");
//            $('a[href="#previous"]').attr('href', '#previous');
//        } else {
//            $('a[href="#previous"]').attr('href', '#previous');
//        }

</script>
<script>

    var backUrl = "{{route('frontend.client.openPreviewFile',[config('constant.subdomain'), $currentService->id])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();

        $('.pay-for-expense-datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY',
        });
        $('.beginning-year').datetimepicker({
            format: 'YYYY',
        });
        $(document).on('change', '.one-time-expense', function () {

            if ($(this).val() == 'annual') {
                $('.need-per-year').show();
                $('.number-of-years').show();
            } else {
                $('.need-per-year').hide();
                $('.number-of-years ').hide();
                $('.need-per-year input').val('');
                $('.number-of-years input').val('');

            }
        });
        $(document).on('change', '.one-time-expense', function () {

            if ($(this).val() == 'one-time') {
                $('.expense-amount').show();
            } else {
                $('.expense-amount').hide();
                $('.expense-amount input').val('');

            }
        });
  <?php if (session::get('your_goal') == 0) { ?>
        var studentList = [];
        if ($('#educationalExpensesData').val() != '') {
            studentList = JSON.parse($('#educationalExpensesData').val());
        }

        $(document).on('click', '#studentform', function (e) {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#myModal .student-name').val() && $('#myModal .student-age').val() && $('#myModal .beginning-year').val() && $('#myModal .level').val() && $('#myModal .school-name').val()) {
                        if ($(this).hasClass('edit-form')) {
                            studentList[$(this).attr('data-index')] = {
                                name: $('#myModal .student-name').val(),
                                age: $('#myModal .student-age').val(),
                                year: $('#myModal .beginning-year').val(),
                                level: $('#myModal .level').val(),
                                school: $('#myModal .school-name').val()
                            }
                            $('#children-info1 tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#myModal .student-name').val() + '</td> <td>' + $('#myModal .student-age').val() + '</td> <td>' + $('#myModal .beginning-year').val() + '</td> <td>' + $('#myModal .level').val() + '</td> <td>' + $('#myModal .school-name').val() + '</td> <td> <i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i> </td>');
                        } else {
                            $('#children-info1 tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#myModal .student-name').val() + '</td> <td>' + $('#myModal .student-age').val() + '</td> <td>' + $('#myModal .beginning-year').val() + '</td> <td>' + $('#myModal .level option:selected').text() + '</td> <td>' + $('#myModal .school-name').val() + '</td> <td> <i title="Edit" class="fa fa-pencil enable-input" aria-hidden="true"></i> </td> </tr>');
                            studentList.push({
                                name: $('#myModal .student-name').val(),
                                age: $('#myModal .student-age').val(),
                                year: $('#myModal .beginning-year').val(),
                                level: $('#myModal .level').val(),
                                school: $('#myModal .school-name').val()
                            });
                        }
                        $('#myModal .student-name,#mymodal .student-age ,#mymodal .beginning-year,#mymodal .level, #mymodal .school-name ').val('');
                        $('#myModal').modal('hide');
                        $('#educationalExpensesData').html(JSON.stringify(studentList));
                    }
                    e.preventDefault();
                }
            }
            return false;
        });

        $(document).on('click', '#children-info1 .fa', function () { 
            $('#myModal').modal();
            $("select").selectBoxIt();
            var index = $(this).closest('tr').attr('data-index');
            studentDetails = studentList[index];
            $('#myModal .student-name').val(studentDetails.name);
            $('#myModal .student-age').val(studentDetails.age);
            $('#myModal .beginning-year').val(studentDetails.year);
            $('#myModal .level').val(studentDetails.level).trigger('change');
            $('#myModal .school-name').val(studentDetails.school);
            $('#studentform').addClass('edit-form');
            $('#studentform').attr('data-index', index);
        });

        $(document).on('click', '.add-dependent', function (e) {
            $("select").selectBoxIt();
            $('#studentform').removeClass('edit-form');
            $('#myModal .level').val('').trigger('change');
            $('#myModal input[type="text"], #myModal input[type="number"]').val('');
            $('#myModal .error-alert').remove();
            e.preventDefault();
        });
  <?php } ?>
    });
</script>
@stop