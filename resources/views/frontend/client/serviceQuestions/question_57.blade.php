<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h6>estate planning</h6>
        <h2>Living Will</h2>
        <p>It's never to early to consider your legacy. The surest way to provide for the financial and emotional well-being of your heirs and beneficiaries is through comprehensive planning. Please answer the following questions, so your advisor can help you effectively manage your affairs.</p>
    </div>

    <div class="col-sm-6 col-sm-offset-1 section-right ">
        {{ Form::input('hidden','questionName[57]','Living Will') }}

        <div class="col-sm-6 inner-left ">

            {{ Form::label('label', 'Do you have a living will?') }} 
            <label class="radio-custom-label">
                {{ Form::radio('answer[57][living will]', 'yes',(!empty($answer) && array_key_exists('living will',$answer)) ? (($answer['living will']=="yes")  ? true : false):false,['class'=>'living-will' ,'required'=>'required']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">
                {{ Form::radio('answer[57][living will]', 'no',(!empty($answer) && array_key_exists('living will',$answer)) ? (($answer['living will']=="no")  ? true : false):false,['class'=>'living-will' ,'required'=>'required']) }}No 
                <span class="radio-icon"></span>
            </label>

        </div>
        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
        <div class="spouse-section">
             <div class="col-sm-6 inner-left ">

                {{ Form::label('label', 'Does your spouse/partner have a living will?') }}
                <label class="radio-custom-label">
                    {{ Form::radio('answer[57][spouse living will]', 'yes',(!empty($answer) && array_key_exists('spouse living will',$answer)) ? (($answer['spouse living will']=="yes")  ? true : false):false,['class'=>'spouse-living-will' ,'required'=>'required']) }}Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    {{ Form::radio('answer[57][spouse living will]', 'no',(!empty($answer) && array_key_exists('spouse living will',$answer)) ? (($answer['spouse living will']=="no")  ? true : false):false,['class'=>'spouse-living-will' ,'required'=>'required']) }}No 
                    <span class="radio-icon"></span>
                </label>

            </div>
        </div>
        <?php } ?>
    </div>
</div>