@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }} 
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    {{ Form::input('hidden','topicId',9) }}
                    {{ Form::input('hidden','subTopicId',23) }}
                    {{ Form::input('hidden','redirectPageName','liabilities-preview') }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Credit Cards</h2>
                                    <p>Do not include cards for which you payoff the full amount each month</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','questionName[81]','Do you have any credit cards on which you carry a balance?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any credt cards on which you carry a balance?') }}
                                        <label class="radio-custom-label">{{ Form::radio('answer[81][CPMcc-confirmation]', 'yes',(!empty($answer) && array_key_exists('CPMcc-confirmation',$answer)) ? (($answer['CPMcc-confirmation']=="yes")  ? true : false):false,['class'=>'CPMCCConfirmation table-confirmation','required'=>'required']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('answer[81][CPMcc-confirmation]', 'no',(!empty($answer) && array_key_exists('CPMcc-confirmation',$answer)) ? (($answer['CPMcc-confirmation']=="no")  ? true : false):false,['class'=>'CPMCCConfirmation table-confirmation','required'=>'required']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left CPMCCDetailsTable" style="display: <?php
                                    if (!empty($answer) && array_key_exists('CPMcc-confirmation', $answer) && ($answer['CPMcc-confirmation'] == "yes")) {
                                        echo 'block';
                                    } else {
                                        echo 'none';
                                    }
                                    ?>" >
                                        <div class="col-sm-12 ">
                                            <div class="col-sm-12 ">
                                                <div class="row">
                                                    <div class="add-child">
                                                        <label> List all credit cards on which you carry a balance.</label>

                                                        <table id="CPMCCInfo" class="find-table-length">
                                                            <thead>
                                                                <tr> 
                                                                    <td>Credit Card</td> 
                                                                    <td> </td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                if (!empty($answer["credit_card_recods"])) {
                                                                    $childs = json_decode($answer["credit_card_recods"]);
                                                                    if (!empty($childs)) {
                                                                        foreach ($childs as $key => $child) {
                                                                            ?>
                                                                            <tr class="children-info" data-index="{{$key}}">
                                                                                <td class="">{{$child->CPMCCCardName}}</td>
                                                                                <td style="text-align:right;"><i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>
                                                                            </tr>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            </tbody>
                                                        </table> 
                                                        <div class="col-md-4 ">
                                                            <div class="row">
                                                                <button type='button' class="semiAnnualAddCCBtn add-dependent" data-toggle="modal" data-target="#CPMCCModal">Add Card</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea id="CPMCreditCardDetails" class="hidden" name="answer[81][credit_card_recods]">{{(!empty($answer["credit_card_recods"]))? $answer["credit_card_recods"]:null}}</textarea>
                                    <!-- info table finsih -->
                                </div>
                                <!-- modal starts --> 
                                <div class="sections">
                                    <div id="CPMCCModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">ADD CREDIT CARD</h4>
                                                </div>
                                                <div class="modal-body" style="overflow: hidden;">
                                                    <div class="row">
                                                        <!-- Steps starts here -->
                                                        <!-- first step starts -->
                                                        <div class=" setup-content" id="step-1">
                                                            <div class=" section-right">
                                                                <!-- first step content starts here -->
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Give this card name') }}
                                                                    {{ Form::input('text','CPMcc-card-name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCCardName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Which bank issue this card?') }}
                                                                    {{ Form::input('text','CPMcc-bank-issuer',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCBankIssuer', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Bank Name']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What type of card is it?') }}
                                                                    {{ Form::input('text','CPMcc-card-type',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCCardType', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Visa, Discover, Nordstorm']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Brand? (may be store, airline, etc.)') }}
                                                                    {{ Form::input('text','CPMcc-card-brand',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCBrand', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'United Airlines, etc.']) }}
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- first step finsih -->

                                                        <!-- second step starts -->
                                                        <div class=" setup-content" id="step-2" style="display: none">
                                                            <div class="section-right">
                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Is this a rewards points or cash back card?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-cash-back-card', 'yes',false,['class'=>'CPMCCCashBackCard','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-cash-back-card', 'no',false,['class'=>'CPMCCCashBackCard','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashBackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'What is the point balance?') }}
                                                                    {{ Form::input('number','CPMcc-point-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCPointBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Points']) }}
                                                                </div>

                                                                <div class="col-sm-12 inner-left CashBackYesOption" style="display: none">
                                                                    {{ Form::label('label', 'Can points be exchanged for cash?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-points-exchanged', 'yes',false,['class'=>'CPMCCPointsExchanged','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-points-exchanged', 'no',false,['class'=>'CPMCCPointsExchanged','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left exchangeforCasgYesOption" style="display: none">
                                                                    {{ Form::label('label', 'What is the rate at which points may be exchanged for cash(Example: 1 to 0.01 would equal one cent per point)?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-exchangefor-cash',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCExchangeforCash borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the interest rate?') }}
                                                                    <div class="input-group">
                                                                        {{ Form::input('number','CPMcc-interest-rate',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCInterestRate borderRight0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                        <span class="input-group-addon">%</span>
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12 inner-left">

                                                                    {{ Form::label('label', 'What is the current balance on card?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-current-balance',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCCurrentBalance borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div> 
                                                                </div>

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <button  class='nextBtn pull-right' type="button">
                                                                        next
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <!-- second step finsih -->
                                                        <!-- third step starts -->
                                                        <div class=" setup-content" id="step-3" style="display: none">
                                                            <div class="section-right">

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the minimum monthly payment?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-monthly-payment',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCMonthlyPayment borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What do you typically pay per month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-pay-per-month',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCPayPerMonth borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Approximately how much in a new purchases do you put on the card each month?') }}
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-purchase-per-month',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCPurchasePerMonth borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'Are you using this card for cash flow?') }}
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-card-cash-flow', 'yes',false,['class'=>'CPMCCCardCashFlow','required'=>'required']) }}Yes
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                    <label class="radio-custom-label">{{ Form::radio('CPMcc-card-cash-flow', 'no',false,['class'=>'CPMCCCardCashFlow','required'=>'required']) }}No
                                                                        <span class="radio-icon"></span>
                                                                    </label>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'What is the credit limit?') }}
                                                                    <div class="input-group">
                                                                       <span class="input-group-addon">$</span>
                                                                        {{ Form::input('number','CPMcc-credit-limit',null,['data-validation'=> '' , 'class'=>'custom-validation form-control CPMCCCreditLimit borderLeft0', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                                    </div>
                                                                </div>

                                                                <div class="col-sm-12 inner-left">
                                                                    {{ Form::label('label', 'In whose name is this card?') }}
                                                                    <i class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                                                                    <select style="position: absolute;" class="status valid CPMCCNameThisCard" id="helocothers" name="CPMcc-name-this-card" required="" aria-invalid="false" style="display: none;">
                                                                        <option selected="" disabled="" value="">SELECT</option>
                                                                        <option value ='1'>{{session::get('loggedInUserName')}}</option>
                                                                        <?php if (session::get('tax_filing_status') != config('constant.tax_filing_status_inverse.single')) { ?>
                                                                            <option value ='2'><?php echo $spouse_name ;?></option>
                                                                        <?php } ?>
                                                                    </select>
                                                                </div> 

                                                                <div class="col-sm-12">
                                                                    <button  class='backBtn pull-left' type="button">
                                                                        <i class="fa fa-arrow-left"></i>
                                                                        back
                                                                    </button>
                                                                    <!--id='semiAnnualAddCCBtn'-->  
                                                                    <button id="semiAnnualAddCCBtnnew" class='semiAnnualAddCCBtn finishBtn pull-right' type="button">
                                                                        Finish
                                                                        <i class="fa fa-arrow-right"></i>
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <!-- third step finsih -->
                                                        <!-- steps form finsih -->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script>
    var backUrl = "{{route('frontend.client.subTopicPreview',[config('constant.subdomain'),$currentService->id,9,'liabilities-preview'])}}";
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        var CPMCCDetailsList = [];
        if ($('#CPMCreditCardDetails').val() != '') {
            CPMCCDetailsList = JSON.parse($('#CPMCreditCardDetails').val());
        }
        $('.nextBtn, .finishBtn').on('click', function () {
            var $this = $(this);
            var form = $this.closest('form');
            if (form.valid() === true) {
                var nextDiv = $this.closest('.setup-content').next('.setup-content');
                if (nextDiv.length) {
                    nextDiv.siblings().hide();
                    nextDiv.show();
                } else {
                    if ($('#CPMCCModal .CPMCCCardName').val() && $('#CPMCCModal .CPMCCCardType').val()) {

                        if ($('.finishBtn').hasClass('edit-form')) {
                            CPMCCDetailsList[$(this).attr('data-index')] = {
//                                CPMCCConfirmation: $('.CPMCCConfirmation:checked').val(),

                                CPMCCCardName: $('#CPMCCModal .CPMCCCardName').val(),
                                CPMCCBankIssuer: $('#CPMCCModal .CPMCCBankIssuer').val(),
                                CPMCCCardType: $('#CPMCCModal .CPMCCCardType').val(),
                                CPMCCBrand: $('#CPMCCModal .CPMCCBrand').val(),

                                CPMCCCashBackCard: $('.CPMCCCashBackCard:checked').val(),
                                CPMCCPointBalance: $('#CPMCCModal .CPMCCPointBalance').val(),
                                CPMCCPointsExchanged: $('.CPMCCPointsExchanged:checked').val(),
                                CPMCCExchangeforCash: $('#CPMCCModal .CPMCCExchangeforCash').val(),
                                CPMCCInterestRate: $('#CPMCCModal .CPMCCInterestRate').val(),
                                CPMCCCurrentBalance: $('#CPMCCModal .CPMCCCurrentBalance').val(),

                                CPMCCMonthlyPayment: $('#CPMCCModal .CPMCCMonthlyPayment').val(),
                                CPMCCPayPerMonth: $('#CPMCCModal .CPMCCPayPerMonth').val(),
                                CPMCCPurchasePerMonth: $('#CPMCCModal .CPMCCPurchasePerMonth').val(),
                                CPMCCCardCashFlow: $('.CPMCCCardCashFlow:checked').val(),
                                CPMCCCreditLimit: $('#CPMCCModal .CPMCCCreditLimit').val(),
                                CPMCCNameThisCard: $('#CPMCCModal .CPMCCNameThisCard').val()
                            };
                            $('#CPMCCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#CPMCCModal .CPMCCCardName').val() + '</td>  <td style="text-align:right;"><i  class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td>');

                        } else {
                            $('#CPMCCInfo tbody').append('<tr data-index=' + CPMCCDetailsList.length + '><td>' + $('#CPMCCModal .CPMCCCardName').val() + '</td>   <td style="text-align:right;" > <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer"  aria-hidden="true"></i></td></tr>');
                            CPMCCDetailsList.push({
//                                CPMCCConfirmation: $('.CPMCCConfirmation:checked').val(),

                                CPMCCCardName: $('#CPMCCModal .CPMCCCardName').val(),
                                CPMCCBankIssuer: $('#CPMCCModal .CPMCCBankIssuer').val(),
                                CPMCCCardType: $('#CPMCCModal .CPMCCCardType').val(),
                                CPMCCBrand: $('#CPMCCModal .CPMCCBrand').val(),

                                CPMCCCashBackCard: $('.CPMCCCashBackCard:checked').val(),
                                CPMCCPointBalance: $('#CPMCCModal .CPMCCPointBalance').val(),
                                CPMCCPointsExchanged: $('.CPMCCPointsExchanged:checked').val(),
                                CPMCCExchangeforCash: $('#CPMCCModal .CPMCCExchangeforCash').val(),
                                CPMCCInterestRate: $('#CPMCCModal .CPMCCInterestRate').val(),
                                CPMCCCurrentBalance: $('#CPMCCModal .CPMCCCurrentBalance').val(),

                                CPMCCMonthlyPayment: $('#CPMCCModal .CPMCCMonthlyPayment').val(),
                                CPMCCPayPerMonth: $('#CPMCCModal .CPMCCPayPerMonth').val(),
                                CPMCCPurchasePerMonth: $('#CPMCCModal .CPMCCPurchasePerMonth').val(),
                                CPMCCCardCashFlow: $('.CPMCCCardCashFlow:checked').val(),
                                CPMCCCreditLimit: $('#CPMCCModal .CPMCCCreditLimit').val(),
                                CPMCCNameThisCard: $('#CPMCCModal .CPMCCNameThisCard').val(),

                            });

                        }
                        $('#CPMCCModal').modal('hide');
                        $('#CPMCCModal .CPMCCCardName, #CPMCCModal .CPMCCCardType').val('');
                        $('#CPMCCModal .CPMCCConfirmation').prop('checked', false);
                        $('#CPMCreditCardDetails').html(JSON.stringify(CPMCCDetailsList));
                    }

                }
            }
            return false;
        });

        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

        $(document).on('change', '.CPMCCConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.CPMCCDetailsTable').show();
            } else {
                $('.CPMCCDetailsTable').hide();
                CPMCCDetailsList = [];
                $('#CPMCreditCardDetails').html(JSON.stringify(CPMCCDetailsList));
                $('.CPMCCDetailsTable tbody tr').remove();
            }
        });



        $(document).on('change', '.CPMCCCashBackCard', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
                $('.CPMCCPointsExchanged ').prop('checked', false);
            }
        });



        $(document).on('change', '.CPMCCPointsExchanged', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('click', '#CPMCCInfo .fa-pencil', function () {
            $('#CPMCCModal').modal();
            $('#CPMCCModal #step-1').show();
            $('#CPMCCModal #step-2, #CPMCCModal #step-3').css('display', 'none');
            $('#CPMCCModal .inner-left input').parent().find('label.error').remove();
            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualCCDetails = CPMCCDetailsList[index];
            $('.CPMCCConfirmation[value=' + SemiAnnualCCDetails.CPMCCConfirmation + ']').prop('checked', true);
            $('#CPMCCModal .CPMCCCardName').val(SemiAnnualCCDetails.CPMCCCardName);
            $('#CPMCCModal .CPMCCBankIssuer').val(SemiAnnualCCDetails.CPMCCBankIssuer);
            $('#CPMCCModal .CPMCCCardType').val(SemiAnnualCCDetails.CPMCCCardType);
            $('#CPMCCModal .CPMCCBrand').val(SemiAnnualCCDetails.CPMCCBrand);

            $('#CPMCCModal .CPMCCCashBackCard[value=' + SemiAnnualCCDetails.CPMCCCashBackCard + ']').prop('checked', true);
            $('#CPMCCModal .CPMCCPointBalance').val(SemiAnnualCCDetails.CPMCCPointBalance);
            $('#CPMCCModal .CPMCCPointsExchanged[value=' + SemiAnnualCCDetails.CPMCCPointsExchanged + ']').prop('checked', true);
            $('#CPMCCModal .CPMCCExchangeforCash').val(SemiAnnualCCDetails.CPMCCExchangeforCash);
            $('#CPMCCModal .CPMCCInterestRate').val(SemiAnnualCCDetails.CPMCCInterestRate);
            $('#CPMCCModal .CPMCCCurrentBalance').val(SemiAnnualCCDetails.CPMCCCurrentBalance);

            $('#CPMCCModal .CPMCCMonthlyPayment').val(SemiAnnualCCDetails.CPMCCMonthlyPayment);
            $('#CPMCCModal .CPMCCPayPerMonth').val(SemiAnnualCCDetails.CPMCCPayPerMonth);
            $('#CPMCCModal .CPMCCPurchasePerMonth').val(SemiAnnualCCDetails.CPMCCPurchasePerMonth);
            $('#CPMCCModal .CPMCCCardCashFlow[value=' + SemiAnnualCCDetails.CPMCCCardCashFlow + ']').prop('checked', true);
            $('#CPMCCModal .CPMCCCreditLimit').val(SemiAnnualCCDetails.CPMCCCreditLimit);
            $('#CPMCCModal .CPMCCNameThisCard').val(SemiAnnualCCDetails.CPMCCNameThisCard).trigger('change');


            $('#semiAnnualAddCCBtnnew').addClass('edit-form');
            $('#semiAnnualAddCCBtnnew').attr('data-index', index);

            if ($('.CPMCCCashBackCard:checked').val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption').hide();
                $('.exchangeforCasgYesOption').hide();
            }

            if ($('.CPMCCPointsExchanged:checked').val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }


        });

        $(document).on('click', '.semiAnnualAddCCBtn', function () {
            $('#semiAnnualAddCCBtnnew').removeClass('edit-form');
            $('#CPMCCModal input[type="text"], #CPMCCModal input[type="number"]').val('');
            $('#CPMCCModal select').val('').trigger('change');
            $('#CPMCCModal .error-alert').remove();
            $('#CPMCCModal #step-1').show();
            $('#CPMCCModal #step-2, #CPMCCModal #step-3, .CashBackYesOption, .exchangeforCasgYesOption').hide();
        });

        $(document).on('click', '.fa-trash', function () { // <-- changes
            var index_id = $(this).closest('tr').attr('data-index');
            deleteRow(index_id);

            $('.swal-button--danger').click(function () {
                CPMCCDetailsList.splice(index_id, 1);
                $("#CPMCCInfo tbody").empty();
                if (CPMCCDetailsList.length != 0) {
                    var tr = '';
                    $.each(CPMCCDetailsList, function (key, value) {
                        tr += '<tr data-index="' + key + '"><td>' + value.CPMCCCardName + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil cursor-pointer" aria-hidden="true"></i><i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i></td></tr>';
                    });
                    $("#CPMCCInfo tbody").html(tr);
                }
                $('#CPMCreditCardDetails').html(JSON.stringify(CPMCCDetailsList));
                return false;
            });
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });

        $(document).on('click', '.semiAnnualAddCCBtn', function () { // <-- changes
            $('#CPMCCModal').find('.error-alert').remove();
            $('#CPMCCModal input[type="radio"]').prop('checked', false);

        });
    });

</script>
@stop