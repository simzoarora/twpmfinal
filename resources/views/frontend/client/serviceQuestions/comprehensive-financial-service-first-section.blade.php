@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
//                $page_title = $currentService->title;
                $page_title = 'test';
                ?> 
                @include('frontend.includes.client_header') 
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.answerQuestions', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',1) }}                    
                    {{ Form::input('hidden','topicId',1) }}                    



                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_218')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_219')
                        </section>
                    </fieldset>

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_220')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_221')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_222')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_223')
                        </section>
                    </fieldset> 

                    <h3></h3>                
                    <fieldset>  
                        <section class="sections">                  
                            @include('frontend.client.serviceQuestions.question_224')
                        </section>
                    </fieldset> 

                    {{form :: close()}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>

<script>
    $(document).ready(function () {
        $(document).on('click', 'a[href="#next"], a[href="#previous"]', function () {
            addRemoveHref();
        });
        addRemoveHref();
        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });
</script>
<style type="text/css">
    .wrapper .sections .section-right .inner-left label.error{width:100% !important;     max-width: 263px !important;}
    .wrapper .sections .section-right .inner-left, .wrapper .sections .section-right .alignment .inner-left{width:100% !important;     max-width: 263px !important;}

</style>

<!--<script>
    var backUrl = "{{route('frontend.client.servicesQuestion',[config('constant.subdomain'),1])}}";
    
</script>
-->


@stop