<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about this non-business-related debt.</h2>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[18][questionName]','Details about this non-business-related debt.') }}

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Give this debt a nickname') }}
            {{ Form::input('text','data[18][answer][nickname]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Nickname']) }}
        </div>

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'In whose name is this debt?') }}
            {{ Form::select('data[18][answer][name]', ['SELECT','my name', 'spouse/partner','joint', 'other'],null,['class'=>'debt-name']) }}
        </div>
        <div class="col-sm-8 inner-left debt-other-name" style="display: none;">
            {{ Form::label('label', 'Their name') }}
            {{ Form::input('text','data[18][answer][other-name]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For what were the proceeds of this loan used?') }}
            {{ Form::input('text','data[18][answer][reason]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Who is the lender?') }}
            {{ Form::input('text','data[18][answer][lender]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true]) }}
        </div>

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'When was this loan originated?') }}
            {{ Form::input('date','data[18][answer][originated]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the term of the loan in months?') }}
            {{ Form::input('text','data[18][answer][loan-term]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true ]) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the interest rate?') }}
            {{ Form::input('text','data[18][answer][loan-interest]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What was the original loan amount??') }}
            {{ Form::input('text','data[18][answer][total-amount]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the current balance?') }}
            {{ Form::input('text','data[18][answer][card-balance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the minimum monthly payment?') }}
            {{ Form::input('text','data[18  ][answer][monthly-payment]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What do you typically pay per month?') }}
            {{ Form::input('text','data[18][answer][pay-per-month]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is this a revolving loan?') }}
            <label class="radio-custom-label">  {{ Form::radio('data[18][answer][Is this a revolving loan?]', 'yes') }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">  {{ Form::radio('data[18][answer][Is this a revolving loan?]', 'no') }}No
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Is their collateral pledged against this loan (i.e. house, car, etc.)?') }}
            <label class="radio-custom-label">  
                {{ Form::radio('data[18][answer][Is their collateral pledged against this loan (i.e. house, car, etc.)?]', 'yes',false,['class'=>'collateral-loan']) }}Yes
                <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label">  
                {{ Form::radio('data[18][answer][Is their collateral pledged against this loan (i.e. house, car, etc.)?]', 'no',false,['class'=>'collateral-loan']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>

        <div class="col-sm-8 inner-left collateral-other-input" style="display: none;">
            {{ Form::label('label', 'Describe the collateral') }}
            {{ Form::input('text','data[18][answer][Describe the collateral]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>

    </div>
</div>