@extends('frontend.layouts.client')

@section('title')
Client Login
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    <div id="body-content" class="container-fluid">
        <div class="container">
            <div class="inner-content">
                <h2 class='content-heading' style="color: #030303">Login</h2>
                <div class="login-form">
                    {{ Form::open(['route' =>'frontend.auth.login','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'client-login-form']) }}
                    <div class="form-group" id='basic-information'>
                        <div class="err-messages msg-err" id="client-login-error"></div>
                        <div class='basic-information-content'>
                            <span class="input input--hoshi <?php
                            if ($errors->first('email')) {
                                echo 'input-error';
                            }
                            ?>">
                                <input class="input__field input__field--hoshi" type="text" name="email" required>
                                <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                    <span class="input__label-content input__label-content--hoshi">Email Address</span>
                                </label>
                            </span>
                            <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                        </div>
                        <div class="password-head" style="margin-bottom: 40px;">
                            <span class="input input--hoshi <?php
                            if ($errors->first('password')) {
                                echo 'input-error';
                            }
                            ?>">
                                <input class="input__field input__field--hoshi" type="password" name="password" required>
                                <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                    <span class="input__label-content input__label-content--hoshi">Password</span>
                                </label>
                            </span>
                            <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
                        </div>
                        @if(!empty(request()->route()->parameters()['serviceId']))
                        {{ Form::hidden('serviceId',request()->route()->parameters()['serviceId']) }}
                        @endif

                        <div class="submit-btn" id='button-color'>
                            {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn contact-info-button']) }}
                        </div>
                        <div class="terms-services">
                            {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}
                        </div>   
                    </div>
                    {!! Form::close() !!}
                </div>

                <div id="verify-otp" style="display: none">
                    <div class="err-messages msg-err" id="otp-message"></div>
                    {{ Form::open(['route' =>'frontend.client.verifyLoginOtp', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post', 'id'=>'verify-otp-form']) }}
                    <span class="input input--hoshi">
                        {{ Form::input('hidden', 'userId', null,['id'=>'user-id']) }}
                        {{ Form::input('hidden', 'mobileNumber', null,['id'=>'mobile-number']) }}
                        @if(!empty(request()->route()->parameters()['serviceId']))
                        {{ Form::hidden('serviceId',request()->route()->parameters()['serviceId']) }}
                        @endif
                        {{ Form::text('otp', null, ['class' => 'input__field input__field--hoshi', 'id'=> 'otp-input', 'required']) }}
                        <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                            <span class="input__label-content input__label-content--hoshi">6 digit code</span>
                        </label>
                    </span>
                    <div class="err-messages msg-err"></div>
                    <div id='button-color'>
                        <button class="btn contact-info-button" type="submit">Verify OTP</button>
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts') 
<script src="{{ asset('js/client-login.js') }}"></script>
@stop