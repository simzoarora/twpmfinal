@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header') 

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.testing', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                
                    <h3></h3>                
                    <fieldset>     
                        <section class="sections">
                            @include('frontend.client.serviceQuestions.testing1-question') 
                        </section>
                    </fieldset>  
                    
                    
                    {{ Form::close() }}
                </div> 
            </div> 

            <!--OTHER PERSONAL INSURANCE MODAL-->
            <section class="sections"  >
                <div class="col-sm-12 recommended-service-div">
                    <div id="otherPersonalInsuranceModal" class="modal fade add-student-modal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="overflow:hidden;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">ADD FAMILY BENEFICIARIES</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>

                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                            {{ Form::input('hidden','data[63][questionName]','ADD a Student') }}
                                            <div class="col-sm-12  validation-alert">
  
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','First Name') }}
                                                    {{ Form::input('text','data[63][answer][first name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control insurance-first-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'First name', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Last Name') }}
                                                    {{ Form::input('text','data[63][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control last-name', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'last name', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Date of birth') }}
                                                    {{ Form::input('text','data[63][answer][dob]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control date-of-birth', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Percentage (max 100%)') }}
                                                    {{ Form::input('text','data[63][answer][last name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control percentage', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'', 'required'=>'required']) }}
                                                </div>

                                                <div class="col-sm-12 inner-left">
                                                    <div class="primary-option">
                                                        <ul> 
                                                            <p class="primary-select">Primary ?</p>
                                                            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                                                                    <input type="checkbox" class="primary-selected">
                                                                    <span class="checkbox-icon"></span>
                                                                    <span>
                                                                    </span>
                                                                </label>
                                                            </li>
                                                        </ul>
                                                    </div>    
                                                </div>   
                                                <div class="col-sm-12 inner-left">
                                                    <button id='otherPersonalInsuranceForm'  class="modal-button"type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>         
                                                </div>   
                                            </div>   
                                        </div>          
                                        <!--{{ Form::close() }}-->
                                    </div> 
                                    <div class="modal-footer">    
                                    </div>    
                                </div>   
                            </div>   
                        </div>
                    </div>
                    <!--OTHER PERSONAL INSURANCE MODAL END-->

                    <!-------CONTINGENT BENEFICIARY MODAL-------->
                    <div id="OtherContingentBeneficiaryModal" class="modal fade add-student-modal" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content" style="overflow:hidden;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">ADD CONTINGENT BENEFICIARIES</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>

                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                            {{ Form::input('hidden','data[63][questionName]','ADD a Student') }}
                                            <div class="col-sm-12  validation-alert">

                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">First Name</label>
                                                    <input data-validation="" class="custom-validation form-control other-contingent-first-name" data-rule-regex="false" required="required" placeholder="First name" name="data[63][answer][first name]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Last Name</label>
                                                    <input data-validation="" class="custom-validation form-control last-name" data-rule-regex="false" required="required" placeholder="last name" name="data[63][answer][last name]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Date of birth</label>
                                                    <input data-validation="" class="custom-validation form-control date-of-birth" data-rule-regex="false" required="required" placeholder="MM/DD/YYYY" name="data[63][answer][dob]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Percentage (max 100%)</label>
                                                    <input data-validation="" class="custom-validation form-control percentage" data-rule-regex="false" required="required" placeholder="" name="data[63][answer][last name]" type="text" aria-required="true">
                                                </div>

                                                <div class="col-sm-12 inner-left">
                                                    <button id="otherContingentBeneficiaryForm" class="modal-button" type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-------CONTINGENT BENEFICIARY MODAL END-------->


                    <!--//---------TERM LIFE MODAL-----------//-->

                    <div id="OtherTermLifeModal" class="modal fade" role="dialog" >
                        <div class="modal-dialog">
                            <div class="modal-content" style="overflow:hidden;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">TERM LIFE INSURANCE POLICY</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                            {{ Form::input('hidden','data[62][questionName]','TERM LIFE INSURANCE POLICY') }}
                                            <div class="col-sm-12  validation-alert">
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Date purchased</label>
                                                    <input data-validation="" class="custom-validation form-control termLifeInsurance" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="data[<%=count%>][answer][Date purchased]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Number of years in term</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Number of years]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Annual premium</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Amount of death benefit</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Amount of death benefit]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <button class="modal-button" type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--{{ Form::close() }}-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//---------TERM LIFE MODAL END-----------//-->


                    <!--//---------WHOLE LIFE MODAL-----------//-->

                    <div id="OtherWholeLifeModal" class="modal fade" role="dialog" >
                        <div class="modal-dialog">
                            <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">WHOLE LIFE INSURANCE POLICY</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                            {{ Form::input('hidden','data[62][questionName]','WHOLE LIFE') }}
                                            <div class="col-sm-12 validation-alert">
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Date purchased</label>
                                                    <input data-validation="" class="custom-validation form-control termLifeInsurance" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="data[<%=count%>][answer][Date purchased]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Annual premium</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Amount of death benefit</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Amount of death benefit]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current cash value</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Current cash value]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current loan value(if no loan, leave blank</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder=" " name="data[<%=count%>][answer][Current loan value]" type="number">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current surrender value</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <button class="modal-button" type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--{{ Form::close() }}-->
                                </div>
                            </div>
                        </div>   
                    </div>
                    <!--//---------WHOLE LIFE MODAL END-----------//-->


                    <!--//---------UNIVERSAL MODAL-----------//-->

                    <div id="OtherUniversalLifeModal" class="modal fade" role="dialog" >
                        <div class="modal-dialog">
                            <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">UNIVERSAL LIFE INSURANCE POLICY</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                            {{ Form::input('hidden','data[62][questionName]','WHOLE LIFE') }}
                                            <div class="col-sm-12 validation-alert">
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Date purchased</label>
                                                    <input data-validation="" class="custom-validation form-control termLifeInsurance" data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="data[<%=count%>][answer][Date purchased]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Annual target premium</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Annual target premium]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Amount of death benefit</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Amount of death benefit]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current cash value</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Current cash value]" type="number" aria-required="true">
                                                </div> 
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current loan value(if no loan, leave blank</label>                  
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder=" " name="data[<%=count%>][answer][Current loan value]" type="number">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current surrender value</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Interest on cash value(if any)</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="number">
                                                </div>
                                                <div class="col-sm-12 inner-left ">
                                                    <label for="label">Are there investment choices offered?</label>
                                                    <label class="radio-custom-label">
                                                        <input class="investment-choices" required="required" name="data[<%=count%>][answer][investment choices offered]" type="radio" value="yes" aria-required="true">Yes
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                    <label class="radio-custom-label">
                                                        <input class="investment-choices" required="required" name="data[<%=count%>][answer][investment choices offered]" type="radio" value="no" aria-required="true">No 
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                </div>
                                                <div class="col-sm-12 inner-left upload-choice" style="display:none;">
                                                    <label for="label">Click below to upload current investment choices.</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="file">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <button class="modal-button" type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--{{ Form::close() }}-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//---------UNIVERSAL MODAL END-----------//-->


                    <!--//---------OTHER MODAL-----------//-->

                    <div id="otherInsuranceLifeModal" class="modal fade" role="dialog" >
                        <div class="modal-dialog">
                            <div class="modal-content" style="overflow:hidden;padding-bottom: 32px;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">OTHER LIFE ISURANCE POLICY</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>
                                    <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3  section-right section-size">
                                            {{ Form::input('hidden','data[62][questionName]','OTHER LIFE ISURANCE POLICY') }}
                                            <div class="col-sm-12  validation-alert">
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">What is the name of insurance</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="data[<%=count%>][answer][What is the name of insurance]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Issued by what company?</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="MM/DD/YYYY" name="data[<%=count%>][answer][Issued by what company?]" type="text" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Annual premium or target premium</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Annual target premium]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Amount of death benefit</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Amount of death benefit]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current cash value</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder=" " name="data[<%=count%>][answer][Current cash value]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current loan value(if no loan, leave blank</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder=" " name="data[<%=count%>][answer][Current loan value]" type="number">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Current surrender value</label>
                                                    <i class="fa fa-dollar" aria-hidden="true"></i>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="number" aria-required="true">
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <label for="label">Interest on cash value(if any)</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="number">
                                                </div>
                                                <div class="col-sm-12 inner-left ">
                                                    <label for="label">Are there investment choices offered?</label>
                                                    <label class="radio-custom-label">
                                                        <input class="other-investment-choices" required="required" name="data[<%=count%>][answer][investment choices offered]" type="radio" value="yes" aria-required="true">Yes
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                    <label class="radio-custom-label">
                                                        <input class="other-investment-choices" required="required" name="data[<%=count%>][answer][investment choices offered]" type="radio" value="no" aria-required="true">No 
                                                        <span class="radio-icon"></span>
                                                    </label>
                                                </div>

                                                <div class="col-sm-12 inner-left other-upload-choice" style="display:none;">
                                                    <label for="label">Click below to upload current investment choices.</label>
                                                    <input data-validation="" class="custom-validation form-control " data-rule-regex="false" placeholder="" name="data[<%=count%>][answer][Current surrender value]" type="file">
                                                </div>

                                                <div class="col-sm-12 inner-left">
                                                    <button class="modal-button" type="button" style="border-radius: 3px;    ">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--{{ Form::close() }}-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//---------OTHER MODAL END-----------//-->




                    <!--//---------CLICK HERE OTHER PERSONAL MODAL -----------//-->
                    <div id="clickHereOtherInsurance" class="modal fade add-student-modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content" style="overflow:hidden;">
                                <div class="modal-header" style="background-color: #0972bd; color:#fff; border: none;">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">ADD TRUST</h4>
                                </div>
                                <div class="modal-body" style='border:none; float: left;'>
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3 section-right section-size">
                                            {{ Form::input('hidden','data[63][questionName]','add trust') }}
                                            <div class="col-sm-12  validation-alert">

                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Name of trust') }}
                                                    {{ Form::input('text','data[63][answer][Name of trust]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'trust name', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Trustee') }}
                                                    {{ Form::input('text','data[<%=count%>][answer][Trustee]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Successor trustee') }}
                                                    {{ Form::input('text','data[61][answer][Successor trustee]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control ', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                </div>

                                                <div class="col-sm-12 inner-left">
                                                    {{ Form::label('label','Primary beneficiary') }}
                                                    {{ Form::input('text','data[61][answer][Primary beneficiary]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control', 'data-rule-regex' =>"false", 'required'=>true , 'placeholder'=>'full name', 'required'=>'required']) }}
                                                </div>
                                                <div class="col-sm-12 inner-left">
                                                    <a class='other-append-beneficiary-input'>+Beneficiary</a>
                                                </div>
                                                <div class="col-sm-12 other-append-input inner-left">
                                                </div>

                                                <div class="col-sm-12 inner-left">
                                                    <button id='OtherClickHereForm' type="button" style="border-radius: 3px;
                                                            color: #fff;    
                                                            font-family: Lato;
                                                            font-size: 18px;
                                                            line-height: 24px;                                  
                                                            text-align: center;
                                                            width: 150px;
                                                            padding: 12px 35px;
                                                            background-color: #2179EE;
                                                            text-decoration: none;
                                                            border: none; 
                                                            margin-left: 62px;
                                                            margin-bottom: 40px;">
                                                        Save
                                                    </button>
                                                </div>
                                            </div>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--//---------CLICK HERE OTHER PERSONAL MODAL END-----------//-->
            </section>
        </div>
    </div>
</div> 



@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>
<script id="clickHereBeneficiary" type="text/html">
    <div class="primary-beneficiary-wrapper">
        <label for="label" >Beneficiary <span class="beneficiary-count">2</span></label>
        <div class="primary-option">
            <ul> 
                <p class="primary-select"><i>contingent</i></p>
                <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                        <input type="checkbox" >
                        <span class="checkbox-icon"></span>
                        <span>
                        </span>
                    </label>
                </li>
            </ul>
        </div>
        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="required" placeholder="full name" name="data[61][answer][Primary beneficiary]" type="text" aria-required="true" style="margin-top: 6px;">
    </div>
</div>
</script>
<script id="personalClickHereBeneficiary" type="text/html">
    <label for="label" >Beneficiary <span class="personale-insurance-beneficiary-count">2</span></label>
    <div class="primary-option">
        <ul> 
            <p class="primary-select"><i>contingent</i></p>
            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                    <input type="checkbox" >
                    <span class="checkbox-icon"></span>
                    <span>
                    </span>
                </label>
            </li>
        </ul>
    </div>   
    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="required" placeholder="full name" name="data[61][answer][Primary beneficiary]" type="text" aria-required="true">
</div>
</script>
<script id="otherClickHereBeneficiary" type="text/html">
    <label for="label" >Beneficiary <span class="other-beneficiary-count">2</span></label>
    <div class="primary-option">
        <ul> 
            <p class="primary-select"><i>contingent</i></p>
            <li style="display:inline; float: left; width: 50%;"> <label class="checkbox-custom-label">
                    <input type="checkbox" >
                    <span class="checkbox-icon"></span>                                                                                                                                                                                                                                     
                    <span>              
                    </span>           
                </label>
            </li> 
        </ul>
    </div>
    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="required" placeholder="full name" name="data[61][answer][Primary beneficiary]" type="text" aria-required="true">
</div>
</script>
<script id="insuranceDetails" type="text/html">
    <div class="col-sm-12 sections" data-callback='appendInsuranceFields'>
        <div class="col-md-4 col-xs-11 section-left">
            <h2>Other life insurance policies.</h2>
            <p> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
        </div>

        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
            {{ Form::input('hidden','data[63][questionName]','other life insurance policies.') }}

            <div class="alignment">
                <div class="col-sm-6 inner-left left-side">

                    {{ Form::label('label', 'Do you have any other personally-owned life insurance policies?') }}
                    <label class="radio-custom-label">
                        {{ Form::radio('data[<%=count%>][answer][personally owned life insurance]', 'yes',null,['class'=>'personal-life-insurance' ,'required'=>'required']) }}Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        {{ Form::radio('data[<%=count%>][answer][personally owned life insurance]', 'no',null,['class'=>'personal-life-insurance' ,'required'=>'required']) }}No 
                        <span class="radio-icon"></span>
                    </label>
                </div>
                <div class="col-sm-6 inner-left personal-medical-conditions right-side" style="display: none;">
                    {{ Form::label('label', 'Do you have any medical conditions that could make it hard for you to get life insurance?') }}
                    <label class="radio-custom-label" >
                        {{ Form::radio('data[<%=count%>][answer][medical conditions]', 'yes',null,['class'=>'' ,'required'=>'required']) }}Yes
                        <span class="radio-icon"></span>
                    </label> 
                    <label class="radio-custom-label">
                        {{ Form::radio('data[<%=count%>][answer][medical conditions]', 'no',null,['class'=>'' ,'required'=>'required']) }}No 
                        <span class="radio-icon"></span>           
                    </label>                                                   

                </div>                                           
            </div>
            <div class="personal-hide-section" style="display:none;"> 

                <div class="alignment"> 
                    <div class="col-sm-6 inner-left ">
                        <label for="label">Policy owner</label>
                        <select class="owner" required="" name="data[<%=count%>][answer][relationship]" style="display: none;" aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1">user first name</option><option value="2">Spouse name</option><option value="3">Someone else</option><option value="4">Trust</option></select>
                    </div>
                    <div class="col-sm-6 inner-left  right-side owner-full-name" style="display: none;">
                        <label for="label">Owner's full name</label>
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="full name" name="data[<%=count%>][answer][Owner's full name]" type="text" aria-required="true">
                    </div>
                    <div class="col-sm-6 inner-left  right-side trust-information" style="display: none; margin-top: 39px;">
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Trust information" name="data[<%=count%>][answer][trust information]" type="text" aria-required="true">
                    </div>
                </div>

                <div class="col-sm-6 inner-left table-width">

                    {{ Form::label('label', 'Beneficiaries') }} 
                    <p style="width:100%;"><i>If a trust is named beneficiary, click <a data-toggle='modal' data-target='#clickHereOtherInsurance'>here</a></i></p>

                    <table id='other-personal-insurance-info'>  
                        <thead> 
                            <tr>
                                <td>First name</td> 
                                <td>Last name</td> 
                                <td>Percentage</td>
                                <td>Primary</td> 
                            </tr> 
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <button type='button' class="other-personal-insurance-beneficiary" data-toggle="modal" data-target="#otherPersonalInsuranceModal">Add beneficiary</button>
                </div>
                <div class="col-sm-6 inner-left ">

                    {{ Form::label('label', 'Are there contingent beneficiaries?') }}
                    <label class="radio-custom-label">
                        {{ Form::radio('data[63][answer][contingent beneficiaries]', 'yes',null,['class'=>'other-contingent-beneficiaries' ,'required'=>'required']) }}Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        {{ Form::radio('data[63][answer][contingent beneficiaries]', 'no',null,['class'=>'other-contingent-beneficiaries' ,'required'=>'required']) }}No 
                        <span class="radio-icon"></span>
                    </label>

                </div>
                <div class="col-sm-6 inner-left other-contingent-beneficiaries-modal table-width" style="display:none;">

                    {{ Form::label('label', 'Beneficiaries') }}
                    <p style="width:100%;"><i>If a trust is named beneficiary, click <a>here</a></i></p>

                    <table id='other-other-contingent-beneficiary-info'>
                        <thead>
                            <tr>
                                <td>First name</td>
                                <td>Last name</td>
                                <td>Percentage</td>
                                <td></td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <button type='button' class="other-contingent-beneficiary" data-toggle="modal" data-target="#OtherContingentBeneficiaryModal">Add beneficiary</button>
                </div>

                <div class="col-sm-6 inner-left number-of-trustees table-width">
                    <label for="label">What type of policy is it?</label>

                    <div class="trustee termLife">
                        <a class="options" data-toggle="modal" data-target="#OtherTermLifeModal">
                            <div class="select-trustee">
                                <p>Term Life</p>    
                            </div>
                        </a>
                        <a class="options whole-life" data-toggle="modal" data-target="#OtherWholeLifeModal">
                            <div class="select-trustee">
                                <p>Whole life</p>    
                            </div>
                        </a>
                        <a class="options universal" data-toggle="modal" data-target="#OtherUniversalLifeModal">
                            <div class="select-trustee">
                                <p>Universal <br> (Flexible Premium)</p>    
                            </div>
                        </a>
                        <a class="options other-policy" data-toggle="modal" data-target="#otherInsuranceLifeModal">
                            <div class="select-trustee">
                                <p>Other</p>    
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</script>

<script id="DisabilityDetails" type="text/html">

    <div class="col-sm-12 sections" data-callback="appendDisabilityFields" >
        <div class="col-md-4 col-xs-11 section-left">
            <h2>Group disability insurance.</h2>
            <p> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
        </div>

        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
            {{ Form::input('hidden','data[65][questionName]','Group disability insurance.') }}

            <div class="col-sm-6 inner-left ">

                <label for="label">Does your employer offer another group disability insurance? (For example, an employer might sponsor a short-term and also a long-term policy)</label>
                <label class="radio-custom-label">
                    <input class="group-disability-insurance" required="required" name="data[<%=count%>][answer][group disability insurance]" type="radio" value="yes" aria-required="true">Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    <input class="group-disability-insurance" required="required" name="data[<%=count%>][answer][group disability insurance]" type="radio" value="no" aria-required="true">No 
                    <span class="radio-icon"></span>
                </label>
            </div>
            <div class=" hide-disability-info " style="display: none;">

                <div class="col-sm-6 inner-left ">
                    <label for="label">What is the tax status of premiums? (ask HR if unsure, as this is vitally important)</label>
                    <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 55%; z-index: 1; top: 61px; font-size: 16px;"></i>
                    <select class="" name="data[<%=count%>][answer][Citezenship status]" style="display: none;" required="" aria-required="true"><option selected="" disabled="" value="">SELECT</option><option value="1">before tax</option><option value="2">after tax</option></select>
                </div>
                <div class="col-sm-6 inner-left ">
                    <label for="label">Annual premium</label>
                    <i class="fa fa-dollar" aria-hidden="true"></i>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left ">
                    <label for="label">Benefit amount (Please provide both dollar amount and % of salary)</label>
                    <i class="fa fa-dollar fa-dollar-bottom" aria-hidden="true"></i>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                    <!--<i class="fa fa-percent" aria-hidden="true"></i>-->
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" style="margin-top:15px;" name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left number-of-trustees">
                    <label for="label">What type of disability insurance is it?</label>

                    <div class="trustee termLife">
                        <a class="options disability-insurance-term ">
                            <div class="select-trustee">
                                <p>Short Term</p>      
                            </div>  
                        </a>  
                        <a class="options disability-insurance-term ">
                            <div class="select-trustee">
                                <p>Long Term</p>    
                            </div>
                        </a>            
                    </div> 
                </div>
                <div class="col-sm-6 inner-left ">
                    <label for="label">Elimination Period(How long until it starts paying after the onset of disability</label>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="data[<%=count%>][answer][Elimination Period]" type="number" aria-required="true">
                </div>
                <div class="alignment">
                    <div class="col-sm-6 inner-left left-side ">

                        <label for="label">Payout period(how long the benefit will be paid)</label>
                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 12%; z-index: 1; top: 61px; font-size: 16px;"></i>
                        <select class="payout-period valid" name="data[<%=count%>][answer][Citezenship status]" style="display: none;" required="" aria-required="true" aria-invalid="false"><option selected="" disabled="" value="">SELECT</option><option value="1">total # of months</option><option value="2">until certain age</option></select>
                    </div>
                    <div class="col-sm-6 inner-left  right-side number-of-months" style="display: none;">
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Total # of months" style="position:absolute; top:  46px;" name="data[<%=count%>][answer][months]" type="text" aria-required="true">
                    </div>
                    <div class="col-sm-6 inner-left  right-side age" style="">
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Age" style="position:absolute; top:  46px;" name="data[<%=count%>][answer][age]" type="text" aria-required="true">
                    </div>
                </div>
            </div>
        </div>
    </div>

</script>

<script id="OtherDisabilityDetails" type="text/html">
    <div class="col-sm-12 sections" data-callback="personalDisabilityFields">
        <div class="col-md-4 col-xs-11 section-left">
            <h2>Personal disability insurance.</h2>
            <p> We’ll get all of your information first, then gather the same for <?php echo $spouse_name ;?>. </p>
        </div>

        <div class="col-sm-6 col-sm-offset-1 section-right section-size ">
            {{ Form::input('hidden','data[67][questionName]','Group disability insurance.') }}

            <div class="col-sm-6 inner-left ">

                <label for="label">Do you have any other personally owned disability insurance?</label>
                <label class="radio-custom-label">
                    <input class="other-personal-disability-insurance" required="required" name="data[<%=count%>][answer][personal disability insurance]" type="radio" value="yes" aria-required="true">Yes
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">
                    <input class="other-personal-disability-insurance" required="required" name="data[<%=count%>][answer][personal disability insurance]" type="radio" value="no" aria-required="true">No 
                    <span class="radio-icon"></span>
                </label>
            </div>
            <div class=" hide-other-personal-insurance-info " style="display: none;">

                <div class="col-sm-6 inner-left ">
                    <label for="label">Annual premium</label>
                    <i class="fa fa-dollar" aria-hidden="true"></i>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left ">
                    <label for="label">Benefit amount </label>
                    <i class="fa fa-dollar fa-dollar-bottom" aria-hidden="true"></i>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="" name="data[<%=count%>][answer][Annual premium]" type="number" aria-required="true">
                </div>
                <div class="col-sm-6 inner-left number-of-trustees">
                    <label for="label">What type of disability insurance is it?</label>

                    <div class="trustee termLife">
                        <a class="options disability-insurance-term ">
                            <div class="select-trustee">
                                <p>Short Term</p>    
                            </div>
                        </a>
                        <a class="options disability-insurance-term ">
                            <div class="select-trustee">
                                <p>Long Term</p>    
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-sm-6 inner-left ">
                    <label for="label">Elimination Period(How long until it starts paying after the onset of disability)</label>
                    <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Enter # of days" name="data[<%=count%>][answer][Elimination Period]" type="number" aria-required="true">
                </div>
                <div class="alignment">
                    <div class="col-sm-6 inner-left left-side ">

                        <label for="label">Payout period(how long the benefit will be paid)</label>
                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 12%; z-index: 1; top: 61px; font-size: 16px;"></i>
                        <select class="payout-period valid" name="data[<%=count%>][answer][Citezenship status]" style="display: none;" required="" aria-required="true" aria-invalid="false"><option selected="" disabled="" value="">SELECT</option><option value="1">total # of months</option><option value="2">until certain age</option></select>
                    </div>
                    <div class="col-sm-6 inner-left  right-side number-of-months" style="display: none;">
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Total # of months" style="position:absolute; top:  46px;" name="data[<%=count%>][answer][months]" type="text" aria-required="true">
                    </div>
                    <div class="col-sm-6 inner-left  right-side age" style="">
                        <input data-validation="" class="custom-validation form-control" data-rule-regex="false" required="" placeholder="Age" style="position:absolute; top:  46px;" name="data[<%=count%>][answer][age]" type="text" aria-required="true">
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>




<script>
    $(document).ready(function () {

        $("select").selectBoxIt();


        $('.estate-planning-review').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.spouse-estate-planning-review').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.dob-datePicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.termLifeInsurance').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.homeCareDatetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.maturitydatetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.mortgage-datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.heloc-datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.auto-datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });

    });


    $(document).on('change', '.comprehensive-marital-status', function () {
        if ($(this).val() == '1') {
            $('.auto-insurance-status').hide();
            $('.spouse-umbrella-policy').hide();
            $('.question-description').hide();
            $('.partner-charity').hide();
        } else {
            $('.auto-insurance-status').show();
            $('.spouse-umbrella-policy').show();
            $('.question-description').show();
            $('.partner-charity').show();
        }

    });

    $(document).on('change', '.will-confirmation', function () {
        if ($(this).val() == 'yes') {
            $('.execution-details').show();
        } else {
            $('.execution-details').hide();
        }


        $('.execution-date').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('.review-date').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });

    $(document).on('change', '.living-trust', function () {
        if ($(this).val() == 'yes') {
            $('.number-of-trustees, .type-of-trustee').show();
        } else {
            $('.number-of-trustees, .type-of-trustee').hide();
            //            $('.type-of-trustee:checked').prop('checked', false);
            $('.options').removeClass('selected');
        }

    });


    $(document).on('click', '.options', function () {
        $(this).addClass('select1ed').siblings().removeClass('selected');
    });
    $(document).on('click', '.medicare-parts ', function () {
        $(this).toggleClass('selected-option');
    });
    $(document).on('click', '.long-term-insurance-parts ', function () {
        $(this).toggleClass('selected-option');
    });
    $(document).on('click', '.insurance-type ', function () {
        $(this).toggleClass('selected-option');
    });
    $(document).on('click', '.other-option', function () {
        if ($(this).hasClass('selected-option')) {
            $('.explain-other').show();
        } else {
            $('.explain-other').hide();
        }
    });


    $(document).on('change', '.life-insurance-trust', function () {
        if ($(this).val() == 'yes') {
            $('.current-insurance, .name-of-trust').show();
        } else {
            $('.current-insurance, .name-of-trust').hide();
        }

    });
    $(document).on('change', '.spouse-life-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.spouse-current-insurance, .spouse-name-of-trust').show();
        } else {
            $('.spouse-current-insurance, .spouse-name-of-trust').hide();
        }

    });
    $(document).on('change', '.group-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.hide-content').show();
        } else {
            $('.hide-content').hide();
        }

    });
    $(document).on('change', '.owner', function () {
        if ($(this).val() == 4) {
            $('.trust-information').show();
        } else {
            $('.trust-information').hide();
        }

    });
    $(document).on('change', '.owner', function () {
        if ($(this).val() == 3) {
            $('.owner-full-name').show();
        } else {
            $('.owner-full-name').hide();
        }

    });
    $(document).on('change', '.maximum-coverage', function () {
        if ($(this).val() == 'no') {
            $('.maximum-available').show();
        } else {
            $('.maximum-available').hide();
        }

    });
    $(document).on('change', '.investment-choices', function () {
        if ($(this).val() == 'yes') {
            $('.upload-choice').show();
        } else {
            $('.upload-choice').hide();
        }

    });
    $(document).on('change', '.other-investment-choices', function () {
        if ($(this).val() == 'yes') {
            $('.other-upload-choice').show();
        } else {
            $('.other-upload-choice').hide();
        }

    });


    $(document).on('change', '.personal-life-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.personal-hide-section').show();
            $('.personal-medical-conditions').hide();
        } else {
            $('.personal-hide-section').hide();
            $('.personal-medical-conditions').show();
        }

    });
    $(document).on('change', '.payout-period', function () {
        if ($(this).val() == '1') {
            $('.number-of-months').show();
            $('.age').hide();
        } else {
            $('.number-of-months').hide();
            $('.age').show();
        }

    });
    $(document).on('change', '.group-disability-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.hide-disability-info').show();
        } else {
            $('.hide-disability-info').hide();
            $('.hide-disability-info input').val('');
            $('.hide-disability-info select').val('').trigger('change');
        }

    });
//    $(document).on('change', '.other-group-disability-insurance', function () {
//        if ($(this).val() == 'yes') {
//            $('.hide-other-disability-info').show();
//        } else {
//            $('.hide-other-disability-info').hide();
//            $('.hide-other-disability-info input').val('');
//            $('.hide-other-disability-info select').val('').trigger('change');
//        }
//
//    });
    $(document).on('change', '.personal-disability-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.hide-personal-insurance-info').show();
        } else {
            $('.hide-personal-insurance-info').hide();
        }

    });
//    $(document).on('change', '.other-personal-disability-insurance', function () {
//        if ($(this).val() == 'yes') {
//            $('.hide-other-personal-insurance-info').show();
//        } else {
//            $('.hide-other-personal-insurance-info').hide();
//            $('.hide-other-personal-insurance-info input , .hide-other-personal-insurance-info select').val('').trigger('change');
//        }
//
//    });
    $(document).on('change', '.Medicare-cover', function () {
        if ($(this).val() == 'yes') {
            $('.medicare-section').show();
            $('.medicare-parts').removeClass('selected-option');
        } else {
            $('.medicare-section').hide();
        }

    });
    $(document).on('change', '.auto-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.liability-coverage').show();
        } else {
            $('.liability-coverage').hide();
        }

    });
    $(document).on('change', '.home-care-option', function () {
        if ($(this).val() == 'yes') {
            $('.facility-care-percent').show();
        } else {
            $('.facility-care-percent').hide();
        }

    });
    $(document).on('change', '.other-home-care-option', function () {
        if ($(this).val() == 'yes') {
            $('.other-facility-care-percent').show();
        } else {
            $('.other-facility-care-percent').hide();
        }

    });


    $(document).on('change', '.malpractice-insurance', function () {
        if ($(this).val() == 'yes') {
            $('.malpractice-insurance-section').show();
        } else {
            $('.malpractice-insurance-section').hide();
            $('.insurance-type').removeClass('selected-option');
        }

    });
    $(document).on('change', '.checking-account-button', function () {
        if ($(this).val() == 'yes') {
            $('.checking-account-table').show();
        } else {
            $('.checking-account-table').hide();
        }
    });
    $(document).on('click', '.checking-account', function () {
        $(this).addClass('selected-option').siblings().removeClass('selected-option');
        $('.checking-account').val($(this).attr('data-value'));
        if ($(this).val() == 'trust') {
            $('.account-details').show();
        } else {
            $('.account-details').hide();
        }
    });
    $(document).on('change', '.saving-account-button', function () {
        if ($(this).val() == 'yes') {
            $('.saving-account-table').show();
        } else {
            $('.saving-account-table').hide();
        }
    });
    $(document).on('click', '.saving-account', function () {
        $(this).addClass('selected-option').siblings().removeClass('selected-option');
        $('.saving-account').val($(this).attr('data-value'));
        if ($(this).val() == 'trust') {
            $('.saving-account-details').show();
        } else {
            $('.saving-account-details').hide();
        }
    });



    $(document).on('change', '.custom-select', function () {
        if ($(this).val() == 4) {
            $('.custom-select').siblings().find('p').css('color', 'red');
        } else {
        }
    });

    $(document).on('click', '#OtherClickHereForm', function () {
        otherCount = 0;
        $('#clickHereOtherInsurance').modal('hide');
    });

    $(document).ready(function () {
        $(document).on('click', '.personal-insurance-beneficiary', function () {
            if ($('#personal-insurance-info .fa-check').length > 0) {
                $('.primary-selected').prop('disabled', true);
            } else {
                $('.primary-selected').prop('disabled', false);
            }

        });
    });
    $(document).on('click', '.other-personal-insurance-beneficiary', function () {
        if ($('#other-personal-insurance-info .fa-check').length > 0) {
            $('.primary-selected').prop('disabled', true);
        } else {
            $('.primary-selected').prop('disabled', false);
        }

    });








//    <---------PERSONAL INSURANCE MODAL (ques-62)-------->
    $(document).ready(function () {
        $("select").selectBoxIt();
        var personalList = [];
        $('#personalInsuranceForm').on('click', function () {
            $('#personalInsuranceModal .error-alert').remove();
            if ($('#personalInsuranceModal .insurance-first-name').val() && $('#personalInsuranceModal .last-name').val() && $('#personalInsuranceModal .percentage').val()) {

                if ($(this).hasClass('edit-form')) {
                    personalList[$(this).attr('data-index')] = {
                        InsuranceName: $('#personalInsuranceModal .insurance-first-name').val(),
                        lastName: $('#personalInsuranceModal .last-name').val(),
                        dateOfBirth: $('#personalInsuranceModal .date-of-birth').val(),
                        primary: $('#personalInsuranceModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#personalInsuranceModal .percentage').val()
                    }
                    if ($('.primary-selected').is(':checked')) {
                        $('#personal-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#personalInsuranceModal .insurance-first-name').val() + '</td> <td>' + $('#personalInsuranceModal .last-name').val() + '</td>   <td>' + $('#personalInsuranceModal .percentage ').val() + '</td> <td><i class="fa fa-check" aria-hidden="true"></i> </td> <td><i title="Edit" class="fa fa-pencil insurance-edit" aria-hidden="true"></i> </td>');
                    } else {
                        $('#personal-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#personalInsuranceModal .insurance-first-name').val() + '</td> <td>' + $('#personalInsuranceModal .last-name').val() + '</td>   <td>' + $('#personalInsuranceModal .percentage ').val() + '</td> <td> </td> <td><i title="Edit" class="fa fa-pencil insurance-edit" aria-hidden="true"></i> </td>');

                    }
                } else {
                    if ($('.primary-selected').is(':checked')) {
                        $('#personal-insurance-info tbody').append('<tr data-index=' + personalList.length + '><td>' + $('#personalInsuranceModal .insurance-first-name').val() + '</td><td>' + $('#personalInsuranceModal .last-name').val() + '</td> <td>' + $('#personalInsuranceModal .percentage ').val() + '</td>  <td><i class="fa fa-check" aria-hidden="true"></i> </td> <td> <i title="Edit" class="fa fa-pencil insurance-edit" aria-hidden="true"></i> </td></tr>');
                    } else {
                        $('#personal-insurance-info tbody').append('<tr data-index=' + personalList.length + '><td>' + $('#personalInsuranceModal .insurance-first-name').val() + '</td><td>' + $('#personalInsuranceModal .last-name').val() + '</td> <td>' + $('#personalInsuranceModal .percentage ').val() + '</td> <td></td> <td> <i title="Edit" class="fa fa-pencil insurance-edit" aria-hidden="true"></i> </td></tr>');
                    }
                    personalList.push({
                        InsuranceName: $('#personalInsuranceModal .insurance-first-name').val(),
                        lastName: $('#personalInsuranceModal .last-name').val(),
                        dateOfBirth: $('#personalInsuranceModal .date-of-birth').val(),
                        primary: $('#personalInsuranceModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#personalInsuranceModal .percentage').val()
                    });
                }
                $('#personalInsuranceModal').modal('hide');
                $('#personalInsuranceModal .insurance-first-name,#personalInsuranceModal .last-name, #personalInsuranceModal .percentage, #personalInsuranceModal .date-of-birth').val('');
            } else {

                //                alert('please fill values');
                $('#personalInsuranceModal input, #personalInsuranceModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });
        $('#personalInsuranceModal input, #personalInsuranceModal select').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val()) {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $('#personal-insurance-info').on('click', '.insurance-edit', function () {
            $('#personalInsuranceModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = personalList[index];
            $('#personalInsuranceModal .insurance-first-name').val(insuranceDetails.InsuranceName);
            $('#personalInsuranceModal .last-name').val(insuranceDetails.lastName);
            $('#personalInsuranceModal .date-of-birth').val(insuranceDetails.dateOfBirth);
            $('#personalInsuranceModal .percentage').val(insuranceDetails.percentage);
            if (insuranceDetails.primary) {
                $('#personalInsuranceModal .primary-selected').prop('checked', true);
            } else {
                $('#personalInsuranceModal .primary-selected').prop('checked', false);
            }
            if ($(this).closest('tr').find('.fa-check').length || $('#personal-insurance-info .fa-check').length == 0) {
                $('#personalInsuranceModal .primary-selected').prop('disabled', false);
            } else {
                $('#personalInsuranceModal .primary-selected').prop('disabled', true);
            }
            $('#personalInsuranceForm').addClass('edit-form').attr('data-index', index);
            $('#personalInsuranceModal .error-alert').remove();
        });

        $('.personal-insurance-beneficiary').on('click', function () {
            $('#personalInsuranceForm').removeClass('edit-form');
            $('#personalInsuranceModal .error-alert').remove();
            $('#personalInsuranceModal input[type="text"]').val('');
            $('#personalInsuranceModal input[type="number"]').val('');
            $('#personalInsuranceModal .primary-selected').prop('checked', false);
        });
        $('.date-of-birth').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });





//----OTHER INSURANCE MODAL-----

    $(document).ready(function () {
        $("select").selectBoxIt();
        var otherInsuranceList = [];
        $('#otherPersonalInsuranceForm').on('click', function () {
            $('#otherPersonalInsuranceModal .error-alert').remove();
            if ($('#otherPersonalInsuranceModal .insurance-first-name').val() && $('#otherPersonalInsuranceModal .last-name').val() && $('#otherPersonalInsuranceModal .percentage').val()) {

                if ($(this).hasClass('edit-form')) {
                    otherInsuranceList[$(this).attr('data-index')] = {
                        InsuranceName: $('#otherPersonalInsuranceModal .insurance-first-name').val(),
                        lastName: $('#otherPersonalInsuranceModal .last-name').val(),
                        dateOfBirth: $('#otherPersonalInsuranceModal .date-of-birth').val(),
                        primary: $('#otherPersonalInsuranceModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#otherPersonalInsuranceModal .percentage').val()
                    }
                    if ($('.primary-selected').is(':checked')) {
                        $('#other-personal-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#otherPersonalInsuranceModal .insurance-first-name').val() + '</td> <td>' + $('#otherPersonalInsuranceModal .last-name').val() + '</td>   <td>' + $('#otherPersonalInsuranceModal .percentage ').val() + '</td><td> <i class="fa fa-check" aria-hidden="true"></i></td> <td> <i title="Edit" class="fa fa-pencil other-insurance-edit" aria-hidden="true"></i> </td>');
                    } else {
                        $('#other-personal-insurance-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#otherPersonalInsuranceModal .insurance-first-name').val() + '</td> <td>' + $('#otherPersonalInsuranceModal .last-name').val() + '</td>   <td>' + $('#otherPersonalInsuranceModal .percentage ').val() + '</td><td></td><td> <i title="Edit" class="fa fa-pencil other-insurance-edit" aria-hidden="true"></i> </td>');
                    }
                } else {
                    if ($('.primary-selected').is(':checked')) {
                        $('#other-personal-insurance-info tbody').append('<tr data-index=' + otherInsuranceList.length + '><td>' + $('#otherPersonalInsuranceModal .insurance-first-name').val() + '</td><td>' + $('#otherPersonalInsuranceModal .last-name').val() + '</td> <td>' + $('#otherPersonalInsuranceModal .percentage ').val() + '</td><td> <i class="fa fa-check" aria-hidden="true"></i></td> <td> <i title="Edit" class="fa fa-pencil other-insurance-edit" aria-hidden="true"></i> </td></tr>');
                    } else {
                        $('#other-personal-insurance-info tbody').append('<tr data-index=' + otherInsuranceList.length + '><td>' + $('#otherPersonalInsuranceModal .insurance-first-name').val() + '</td><td>' + $('#otherPersonalInsuranceModal .last-name').val() + '</td> <td>' + $('#otherPersonalInsuranceModal .percentage ').val() + '</td> <td></td> <td> <i title="Edit" class="fa fa-pencil other-insurance-edit" aria-hidden="true"></i> </td></tr>');
                    }
                    otherInsuranceList.push({
                        InsuranceName: $('#otherPersonalInsuranceModal .insurance-first-name').val(),
                        lastName: $('#otherPersonalInsuranceModal .last-name').val(),
                        dateOfBirth: $('#otherPersonalInsuranceModal .date-of-birth').val(),
                        primary: $('#otherPersonalInsuranceModal .primary-selected:checked').val() ? true : false,
                        percentage: $('#otherPersonalInsuranceModal .percentage').val()
                    });
                }
                $('#otherPersonalInsuranceModal').modal('hide');
                $('#otherPersonalInsuranceModal input').val();
                $('#otherPersonalInsuranceModal input[type="checkbox"]').prop('checked', false);
                $('#otherPersonalInsuranceModal .insurance-first-name,#otherPersonalInsuranceModal .last-name, #otherPersonalInsuranceModal .percentage, #otherPersonalInsuranceModal .date-of-birth').val('');
            } else {

                //                alert('please fill values');
                $('#otherPersonalInsuranceModal input, #otherPersonalInsuranceModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });
        $('#otherPersonalInsuranceModal input, #otherPersonalInsuranceModal select').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val()) {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $(document).on('click', '.other-insurance-edit', function () {
            $('#otherPersonalInsuranceModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = otherInsuranceList[index];
            $('#otherPersonalInsuranceModal .insurance-first-name').val(insuranceDetails.InsuranceName);
            $('#otherPersonalInsuranceModal .last-name').val(insuranceDetails.lastName);
            $('#otherPersonalInsuranceModal .date-of-birth').val(insuranceDetails.dateOfBirth);
            if (insuranceDetails.primary) {
                $('#otherPersonalInsuranceModal .primary-selected').prop('checked', true);
            } else {
                $('#otherPersonalInsuranceModal .primary-selected').prop('checked', false);
            }
            if ($(this).closest('tr').find('.fa-check').length || $('#other-personal-insurance-info .fa-check').length == 0) {
                $('#otherPersonalInsuranceModal .primary-selected').prop('disabled', false);
            } else {
                $('#otherPersonalInsuranceModal .primary-selected').prop('disabled', true);
            }
            $('#otherPersonalInsuranceModal .percentage').val(insuranceDetails.percentage);
            $('#otherPersonalInsuranceForm').addClass('edit-form').attr('data-index', index);
            $('#otherPersonalInsuranceModal .error-alert').remove();
        });
        $(document).on('click', '.other-personal-insurance-beneficiary', function () {
            $('#otherPersonalInsuranceModal input[type="text"]').val('');
            $('#otherPersonalInsuranceModal input[type="number"]').val('');
            $('#otherPersonalInsuranceModal input[type="checkbox"]').prop('checked', false);
            $('#otherPersonalInsuranceForm').removeClass('edit-form');
            $('#otherPersonalInsuranceModal .error-alert').remove();
        });
        $('.date-of-birth').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });


//----OTHER INSURANCE MODAL END-----
//
//
//----OTHER CONTINGENT BENEFICIARY MODAL -----
//
//
    $(document).ready(function () {
        $("select").selectBoxIt();
        var studentList = [];
        $('#otherContingentBeneficiaryForm').on('click', function () {
            $('#OtherContingentBeneficiaryModal .error-alert').remove();
            if ($('#OtherContingentBeneficiaryModal .other-contingent-first-name').val() && $('#OtherContingentBeneficiaryModal .last-name').val() && $('#OtherContingentBeneficiaryModal .percentage').val()) {

                if ($(this).hasClass('edit-form')) {
                    studentList[$(this).attr('data-index')] = {
                        InsuranceName: $('#OtherContingentBeneficiaryModal .other-contingent-first-name').val(),
                        lastName: $('#OtherContingentBeneficiaryModal .last-name').val(),
                        dateOfBirth: $('#OtherContingentBeneficiaryModal .date-of-birth').val(),
                        percentage: $('#OtherContingentBeneficiaryModal .percentage').val()
                    }
                    $('#other-other-contingent-beneficiary-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#OtherContingentBeneficiaryModal .other-contingent-first-name').val() + '</td> <td>' + $('#OtherContingentBeneficiaryModal .last-name').val() + '</td>   <td>' + $('#OtherContingentBeneficiaryModal .percentage ').val() + '</td><td> <i title="Edit" class="fa fa-pencil other-beneficiary-edit" aria-hidden="true"></i> </td>');
                } else {

                    $('#other-other-contingent-beneficiary-info tbody').append('<tr data-index=' + studentList.length + '><td>' + $('#OtherContingentBeneficiaryModal .other-contingent-first-name').val() + '</td><td>' + $('#OtherContingentBeneficiaryModal .last-name').val() + '</td> <td>' + $('#OtherContingentBeneficiaryModal .percentage ').val() + '</td> <td> <i title="Edit" class="fa fa-pencil other-beneficiary-edit" aria-hidden="true"></i> </td></tr>');
                    studentList.push({
                        InsuranceName: $('#OtherContingentBeneficiaryModal .other-contingent-first-name').val(),
                        lastName: $('#OtherContingentBeneficiaryModal .last-name').val(),
                        dateOfBirth: $('#OtherContingentBeneficiaryModal .date-of-birth').val(),
                        percentage: $('#OtherContingentBeneficiaryModal .percentage').val()
                    });
                }
                $('#OtherContingentBeneficiaryModal').modal('hide');
                $('#OtherContingentBeneficiaryModal .other-contingent-first-name,#OtherContingentBeneficiaryModal .last-name, #OtherContingentBeneficiaryModal .percentage, #OtherContingentBeneficiaryModal .date-of-birth').val('');
            } else {

                //                alert('please fill values');
                $('#OtherContingentBeneficiaryModal input, #OtherContingentBeneficiaryModal select').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
            }
        });
        $('#OtherContingentBeneficiaryModal input, #OtherContingentBeneficiaryModal select').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val()) {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $('#other-other-contingent-beneficiary-info').on('click', '.other-beneficiary-edit', function () {
            $('#OtherContingentBeneficiaryModal').modal();
            var index = $(this).closest('tr').attr('data-index');
            insuranceDetails = studentList[index];
            $('#OtherContingentBeneficiaryModal .other-contingent-first-name').val(insuranceDetails.InsuranceName);
            $('#OtherContingentBeneficiaryModal .last-name').val(insuranceDetails.lastName);
            $('#OtherContingentBeneficiaryModal .date-of-birth').val(insuranceDetails.dateOfBirth);
            $('#OtherContingentBeneficiaryModal .percentage').val(insuranceDetails.percentage);
            $('#otherContingentBeneficiaryForm').addClass('edit-form').attr('data-index', index);
            $('#OtherContingentBeneficiaryModal .error-alert').remove();
        });
        $(document).on('click', '.other-contingent-beneficiary', function () {
            $('#otherContingentBeneficiaryForm').removeClass('edit-form');
            $('#OtherContingentBeneficiaryModal .error-alert').remove();
            $('#OtherContingentBeneficiaryModal input[type="text"]').val('');
            $('#OtherContingentBeneficiaryModal input[type="number"]').val('');
        });
        $('.date-of-birth').datetimepicker({
            format: 'MM/DD/YYYY'
        });
    });

//
//
//
//
//----OTHER BENEFICIARY MODAL END-----

    //----------INSURANCE MODAL------------//
    window['appendInsuranceFields'] = function appendInsuranceFields() {
        if ($(document).find('.personal-life-insurance:enabled:checked').val() == 'yes') {
            var insuranceForm = $('#insuranceDetails').html();
            html = _.template(insuranceForm);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY',
            });

        }
        OtherCount = 0;
        studentList = [];
        $(document).find('.personal-life-insurance:enabled:checked').parent().parent().find('.personal-life-insurance:enabled').prop('disabled', true);
    }
    window['appendDisabilityFields'] = function appendDisabilityFields() {
        if ($(document).find('.other-group-disability-insurance:enabled:checked').val() == 'yes') {
            var disabilityForm = $('#DisabilityDetails').html();
            html = _.template(disabilityForm);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY',
            });

        }

        $(document).find('.other-group-disability-insurance:enabled:checked').parent().parent().find('.other-group-disability-insurance:enabled').prop('disabled', true);
    }
    window['personalDisabilityFields'] = function personalDisabilityFields() {
        if ($(document).find('.other-personal-disability-insurance:enabled:checked').val() == 'yes') {
            var otherDisabilityForm = $('#OtherDisabilityDetails').html();
            html = _.template(otherDisabilityForm);
            var cind = form.steps('getCurrentIndex');

            $("form").steps("insert", cind, {
                content: html({count: $('#services-question-form fieldset').length + 1000})
            });
            $("form").steps('previous');
            $("select").selectBoxIt();
            $('.datetimepicker').datetimepicker({
                format: 'MM/DD/YYYY',
            });

        }

        $(document).find('.other-personal-disability-insurance:enabled:checked').parent().parent().find('.other-personal-disability-insurance:enabled').prop('disabled', true);
    }

//    $(window).load(function () {

    var OtherCount = 0;
    $(document).on('click', '.other-append-beneficiary-input', function () {
        OtherCount++;
        var otherBeneficiaryInput = $('#otherClickHereBeneficiary').html();
        html = _.template(otherBeneficiaryInput);
        $('.other-append-input').append(html);
        $('.other-beneficiary-count').last().text(OtherCount);
    });
    var personalInsuranceCount = 0;
    $(document).on('click', '.append-personal-insurance-input', function () {
        personalInsuranceCount++;
        var PersonalInsuranceBeneficiaryInput = $('#personalClickHereBeneficiary').html();
        html = _.template(PersonalInsuranceBeneficiaryInput);
        $('.personal-insurance-append-input').append(html);
        $('.personale-insurance-beneficiary-count').last().text(personalInsuranceCount);
        $('.personale-insurance-beneficiary-count').last().text(personalInsuranceCount);
    });


</script>

@stop