@extends('frontend.layouts.client')

@section('title') 
@stop

@section('meta_description')
@stop
<style>
    .hideStep {
        display: none !important;
    }
</style>
@section('after-styles') 
{{ Html::style(asset('css/single-service-questions.css')) }}
{{ Html::style(asset('css/signUpOne.css')) }} 
@stop

@section('content')
<div class="col-sm-12 header-bar">
    <div class="container">
        <a  href="{{config('constant.base_url')}}">   {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }} </a>
        <p>Risk Tolerance</p>
    </div>
</div> 
<div class="wrapper">
    <div class="container">
        <div class="step">
            <p>Step1: Define your investment goals </p>
            <div class="grey-div">
                <div class="blue-line blue-line-progress">
                </div>
            </div> 
        </div>
    </div>
</div>
<!--Assuming all text fields to be positive integers as per the sign up questions requirements-->
<div class="container wrapper">
    {{ Form::open(['route' => 'frontend.client.saveSignUpUser', 'id'=>'example-advanced-form']) }}
    <div class="col-sm-8 col-sm-offset-2 site-heading-1 steps-box">
        <p class="question-heading">LET'S GET TO KNOW YOU</p>
        @php $count = 0
        @endphp
        <?php //        echo '<pre>';print_r($questions->toArray()) ;die;?>

        @foreach ($questions as $key=> $question)
        <h3 class="<?php echo ($key > 1) ? 'hideStep' : ''; ?>"></h3> 

        <fieldset class="<?php echo ($key > 1) ? 'hideStep' : ''; ?>">
            @foreach ($question as $singleQuestion) 
            @if ($singleQuestion->type == config('constant.question_type.dropdown'))

            <div class="col-sm-12 site-heading-1">
                <div class="row">
                    <div class="investment-intake" id="customSelect">
                        {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }}
                        <?php
                        $selectbox = '<select required name="question[' . $count . '][answer][]" >';
                        $options = '<option value="0">choose one</option>';
                        foreach ($singleQuestion->signUpQuestionAnswer as $answer) {
                            $options = $options . '<option value="' . $answer->id . '">' . $answer->value . '</option>';
                        }
                        $selectbox = $selectbox . $options;
                        $selectbox = $selectbox . '</select>';
                        ?>
                        <h2>{!!str_replace("__",$selectbox,$singleQuestion->question)!!}</h2>
                    </div>
                </div> 
            </div> 

            @elseif ($singleQuestion->type == config('constant.question_type.mcsa'))
            <div class="col-sm-12 site-heading-1">
                {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }}
                <h2>{{$singleQuestion->question}}</h2>
                <ul class="options">
                    @foreach ($singleQuestion->signUpQuestionAnswer as $answer)
                    <li>
                        <label class="radio-custom-label">
                            {{ Form::radio('question['.$count.'][answer][]', $answer->id, null, ['required' => true]) }}
                            <span class="radio-icon"></span>
                            <span>
                                {{$answer->value}} 
                            </span>
                        </label>
                    </li>  
                    @endforeach 
                </ul>
            </div>

            @elseif ($singleQuestion->type == config('constant.question_type.mcma'))
            <div class="col-sm-12 site-heading-1">
                {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }}
                <h2>{{$singleQuestion->question}}</h2>
                <ul class="options">
                    @foreach ($singleQuestion->signUpQuestionAnswer as $answer)
                    <li> <label class="checkbox-custom-label">
                            {{ Form::checkbox('question['.$count.'][answer][]', $answer->id, null, ['required' => true]) }}
                            <span class="checkbox-icon"></span>
                            <span>
                                {{$answer->value}}
                            </span>
                        </label>
                    </li>
                    @endforeach
                </ul>
            </div>

            @elseif ($singleQuestion->type == config('constant.question_type.number'))
            <div class="col-sm-12 site-heading-1">
                <h2>{{$singleQuestion->question}}</h2>
                <?php
                $answer_text = null;
                if ($plan_details != null) {
                    foreach ($plan_details as $plan) {
                        if ($plan['id'] == $singleQuestion->id) {
                            $answer_text = $plan['answer'];
                        }
                    }
                }
                ?>
                {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }}
                {{ Form::text('question['.$count.'][answer][]employeeno', $answer_text, ['required'=>'true' , 'class'=>'add-comma' ] ) }}
                {{ Form::hidden('question['.$count.'][text_answer]', true) }}
            </div>  

            @elseif ($singleQuestion->type == config('constant.question_type.text'))
            <div class="col-sm-12 site-heading-1">
                <h2>{{$singleQuestion->question}}</h2>
                {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }}
                {{ Form::text('question['.$count.'][answer][]', null, ['required'=>true] ) }}
                {{ Form::hidden('question['.$count.'][text_answer]', true) }}    
            </div>  

            @elseif ($singleQuestion->type == config('constant.question_type.select_box'))
            <div class="col-sm-12 site-heading-1">
                {{ Form::hidden('question['.$count.'][id]', $singleQuestion->id) }} 
                {{ Form::hidden('question['.$count.'][answer][]', null, ['required'=>true,'id'=>'customSelection']) }}
                <h2>
                    {{$singleQuestion->question}}
                </h2>
                <div class="custom-row">
                    <div class="col-sm-4 col-xs-12 plans-wrapper" data-percentage="5">
                        <a>
                            <div class="annual-return">
                                <h4>Average annual return <br> 5%</h4>
                                <p>Worst case one-year scenario: -5%</p>    
                                <p>Best case one-year scenario: +15%</p>
                            </div>
                        </a>
                    </div>
                    <div class=" col-sm-4 col-xs-12  plans-wrapper" data-percentage="7.5">
                        <a>
                            <div class="annual-return">
                                <h4>Average annual return <br> 7.5%</h4>
                                <p>Worst case one-year scenario: -12%</p>
                                <p>Best case one-year scenario: +22%</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-12 plans-wrapper" data-percentage="10">
                        <a>
                            <div class="annual-return">
                                <h4>Average annual return <br> 10%</h4>
                                <p>Worst case one-year scenario: -25%</p>
                                <p>Best case one-year scenario: +40%</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            @endif
            @php $count++ @endphp
            @endforeach
        </fieldset>
        @endforeach

        <h3 class="hideStep"></h3>
        <fieldset class="hideStep loaderStep ">
            <div class="col-sm-12 site-heading-1">
                <h2>We're determining a potential allocation plan <br> for you, based on your risk profile...</h2>
                <div class="loader">
                    <div class="rotate half-size"></div>
                    <div class="top"></div>
                    <div class="bottom"></div>
                    <div class="top-small"></div>
                    <div class="bottom-small"></div>
                </div>
            </div>
        </fieldset>   

        <h3 class="hideStep"></h3>
        <fieldset class="hideStep">
            <div class="col-sm-12 credentials ">
                <h2>We have determined a basic allocation plan <br>for you to discuss with your personal <br>financial advisor.</h2>
                <p>Enter your email address and choose a password, so you can review this plan or change it at<br> a later date.
                <div class=''>
                    {{ Form::input('email','email', null, ['placeholder' => 'jane.doe@email.com', 'required'=>true]) }}
                    {{ Form::input('password','password', null, ['placeholder' => 'password(8 characters)', 'required'=>true]) }}
                </div>
            </div>
        </fieldset>
                </div>
    {{ Form::close() }}

    <div>  
        @include('frontend.includes.footer')
    </div> 
</div> 

<div id="debt-markup" class="hide">
    <div class="debt-status"> 
        <h5>Do you have debt?</h5> 
        <p> We recommend paying-off high interest debt before investing. 
            <span>
                <a data-toggle="modal" data-target="#termsModal1" href="#">Learn more</a>
            </span> 
        </p> 
    </div>
</div>
<div id="termsModal1" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content" style="overflow: hidden;">
            <div class="modal-header">
                <h4 class="modal-title">Learn More</h4> 
            </div>
            <div class="modal-body terms-content" >
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
                <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</p>
            </div>
        </div>
    </div>

</div>
@stop
@section('after-scripts')
<script src="{{ asset('js/sign-up-questions.js') }}"></script> 

<script>
$(document).ready(function () {


    $('input.add-comma').on('keyup', function (event) {
        // skip for arrow keys
        if (event.which >= 37 && event.which <= 40) {
            event.preventDefault();
        }
        var key = event.keyCode || event.which;
        key = String.fromCharCode(key);

        var regex = /[0-9]|\,/;
        if (!regex.test(key)) {
            event.returnValue = false;
            if (event.preventDefault)
                event.preventDefault();
        }
        $(this).val(function (index, value) {
            value = value.replace(/,/g, '');
            return numberWithCommas(value);
        });
    });

    function numberWithCommas(x) {
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        return parts.join(".");
    }






    $('ul.options li:last-child input[type="checkbox"]').on('change', function () {
        $('ul.options li:not(:last-child) input[type="checkbox"]').filter(':visible').attr('checked', false)
    });
    $('ul.options li:not(:last-child) input[type="checkbox"]').on('change', function () {
        $('ul.options li:last-child input[type="checkbox"]').filter(':visible').attr('checked', false)
    });
});








</script>


@endsection
