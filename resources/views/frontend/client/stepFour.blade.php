@extends('frontend.layouts.client')

@section('title')
Account Setup &#8226; About You
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    @include('frontend.includes.client_register_header')

    <!--step 2 started-->
    <div id="body-content" class="container-fluid">
        <div class="container">
            <p class='content-head'>Step 3: About You</p>
            <div class="col-sm-8 left-bottom-border"></div> 
            <div class="col-sm-4 right-bottom-border"></div>
            <div class="inner-content">
                <h2 class='content-heading'>About You</h2>
                <p class='heading-info'>Don't worry, we'll never sell your info to anyone.</p>

                {{ Form::open(['route' =>'frontend.client.stepFourRegister','class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) }}
                <input type="hidden" name="user_id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif"> 
                <div class="form-group" id='about-you'>
                    <div class='social-security'>
                        <span class="input input--hoshi <?php
                        if ($errors->first('ssn')) {
                            echo 'input-error';
                        }
                        ?>">
                             {{ Form::text('ssn', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Social security number</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('ssn'); ?></p>
                    </div>
                    <div class='social-security'>
                        <span class="input input--hoshi <?php
                        if ($errors->first('dob')) {
                            echo 'input-error';
                        }
                        ?>">
                             {{ Form::text('dob', null, ['class' => 'input__field input__field--hoshi','required']) }}
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Date of birth (MM/DD/YYYY)</span>
                            </label>
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('dob'); ?></p>
                    </div>
                    <div class='about-radio'>
                        <p>Gender</p>
                        <ul class="radio-cirle">
                            <li>
                                {{ Form::radio('sex', 1, false, ['id' => 'male']) }}
                                <label for="male">Male</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                {{ Form::radio('sex', 2, false, ['id' => 'female']) }}
                                <label for="female">Female</label>
                                <div class="check"></div>
                            </li>
                        </ul>
                        <p class="err-messages msg-err"><?php echo $errors->first('sex'); ?></p>
                    </div>
                    <div class="submit-btn" id='button-color'>
                        <button class="btn contact-info-button" type="submit">Continue</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/step-four.js') }}"></script>
@stop