<div id="serviceQuestions-first">
    @foreach ($questionData->serviceQuestion as $key => $questionArray)

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.mcma'))
    <div class="each-question" id="ques-{{ $questionArray->id }}">
        <p class="question">{{ $questionArray->question }}</p>
        <?php $count = 0 ?>
        @foreach ($questionArray->serviceQuestionOption as $optionArray)
        <div class="squaredTwo">
            {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
            {{ Form::checkbox('answer['.$key.'][value][]', $optionArray->id, null, ['id' => $key.$count.'[option_name]']) }}
            {{ Form::label($key.$count.'[option_name]', ' ') }}
            <span>{!! $optionArray->name !!}</span>
        </div>
        <?php $count++; ?>
        @endforeach
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.mcsa'))
    <div class="each-question" id="ques-{{ $questionArray->id }}">
        <p class="question">{{ $questionArray->question }}</p>
        <ul class="radio-cirle">
            <?php $count = 0 ?>
            @foreach ($questionArray->serviceQuestionOption as $optionArray)
            <li>
                {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
                {{ Form::radio('answer['.$key.'][value][]', $optionArray->id, $optionArray->id, ['id' => $key.$count.'[option_name]']) }}
                {{ Form::label($key.$count.'[option_name]',$optionArray->name , ['class' => '']) }}
                <div class="check"></div>
            </li>
            <?php $count++; ?>
            @endforeach
        </ul>
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.sla'))
    <div class="each-question-input" id="ques-{{ $questionArray->id }}">
        <span class="input input--hoshi">
            {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
            {{Form::text('answer['.$key.'][value][]',null,['class' => 'input__field input__field--hoshi','required'])}}
            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                <span class="input__label-content input__label-content--hoshi">{!! $questionArray->question; !!}</span>
            </label>
        </span>
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.mla'))
    <div class="each-question" id="ques-{{ $questionArray->id }}">
        <p class="question">{{ $questionArray->question }}</p>
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
        {{Form::textarea('answer['.$key.'][value][]',null,['class' => 'form-control hoshi-text','required', 'rows'=> '4'])}}
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.usl'))
    <div class="each-question" id="ques-{{ $questionArray->id }}">
        <p class="question">{{ $questionArray->question }}</p>
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
        {{ Form::hidden('answer['.$key.'][is_file]',true) }}
        {{ Form::file('answer['.$key.'][value][]',['required', 'class'=>'custom-file-input']) }}
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @if ($questionArray->type == config('constant.service_question_types_options_inverse.uml'))
    <div class="each-question" id="ques-{{ $questionArray->id }}">
        <p class="question">{{ $questionArray->question }}</p>
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
        {{ Form::hidden('answer['.$key.'][is_file]',true) }}
        {{ Form::file('answer['.$key.'][value][]', ['multiple'=>true,'required', 'class'=>'custom-file-input']) }}
        <span class="err-messages msg-err"></span>
    </div>
    @endif

    @endforeach
</div>

<div id="serviceQuestions-append"></div>

<div id="save-answer-out">
    {{ Form::submit('Save', ['class' => 'btn contact-info-button-small', 'id' => 'save-answer']) }}
    <i class="fa fa-spinner fa-spin"></i>
</div>

<script id="serviceQuestions-html" type="text/html">
    <% var key = 0; 
    _.each(serviceQuestion, function(v){ %>

    <% if(v.type == questionList.mcma){ %>
    <div class="each-question" id="ques-<%= v.id %>">
        <p class="question"><%= v.question %></p>
        <% var count = 0;
        for(j=0; j< v.service_question_option.length; j++){  %>
        <div class="squaredTwo">
            <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
            <input type="checkbox" name="answer[<%= key %>][value][]" value="<%= v.service_question_option[j]['id'] %>" id="<%= v.id+''+count %>[option_name]"/>
            <label for="<%= v.id+''+count %>[option_name]"></label>
            <span><%= v.service_question_option[j]['name'] %></span>
        </div>
        <% count++;
        } %>
        <span class="err-messages msg-err"></span>
    </div>
    <% } 

    if(v.type == questionList.mcsa){ %>
    <div class="each-question" id="ques-<%= v.id %>">
        <p class="question"><%= v.question %></p>
        <ul class="radio-cirle">
            <% var count = 0;
            for(j=0; j< v.service_question_option.length; j++){ %>
            <li>
                <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
                <input type="radio" name="answer[<%= key %>][value][]" value="<%= v.service_question_option[j]['id'] %>" id="<%= v.id+''+count %>[option_name]"/>
                <label for="<%= v.id+''+count %>[option_name]"><%= v.service_question_option[j]['name'] %></label>
                <div class="check"></div>
            </li>
            <% count++;
            } %>
        </ul>
        <span class="err-messages msg-err"></span>
    </div>
    <% }

    if(v.type == questionList.sla){ %>
    <div class="each-question-input" id="ques-<%= v.id %>">
        <span class="input input--hoshi">
            <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
            <input type="text" name="answer[<%= key %>][value][]" class="input__field input__field--hoshi" required/>
            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                <span class="input__label-content input__label-content--hoshi"><%= v.question %></span>
            </label>
        </span>
        <span class="err-messages msg-err"></span>
    </div>
    <% }

    if(v.type == questionList.mla){ %>
    <div class="each-question" id="ques-<%= v.id %>">
        <p class="question"><%= v.question %></p>
        <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
        <textarea name="answer[<%= key %>][value][]" class="form-control hoshi-text" id="" rows="4" required></textarea>
        <span class="err-messages msg-err"></span>
    </div>
    <% }

    if(v.type == questionList.usl){ %>
    <div class="each-question" id="ques-<%= v.id %>">
        <p class="question"><%= v.question %></p>
        <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
        <input type="hidden" name="answer[<%= key %>][is_file]"/>
        <input type="file" name="answer[<%= key %>][value][]" class="custom-file-input" required/>
        <span class="err-messages msg-err"></span>
    </div>
    <% }

    if(v.type == questionList.uml){ %>
    <div class="each-question" id="ques-<%= v.id %>">
        <p class="question"><%= v.question %></p>
        <input type="hidden" name="answer[<%= key %>][question_id]" value="<%= v.id %>"/>
        <input type="hidden" name="answer[<%= key %>][is_file]"/>
        <input type="file" name="answer[<%= key %>][value][]" class="custom-file-input" multiple="true" required/>
        <span class="err-messages msg-err"></span>
    </div>
    <% }
    key++; 
    });  %>
</script>