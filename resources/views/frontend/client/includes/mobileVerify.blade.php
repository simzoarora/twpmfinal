<div id='Verify-Mobile-Number'>
    {{ Form::open(['class' => 'form-horizontal', 'role' => 'form']) }}
    <input type="hidden" name="user_id" id="user-id" value ="@if( empty(old('user_id'))){{$userId}}@else{{old('user_id')}}@endif"> 
    <div class="form-group"> 
        <div class='mobile-number'>
            <span class="input input--hoshi">
                {{ Form::text('mobile_number', $mobileNumber->mobile_number, ['class' => 'input__field input__field--hoshi', 'id'=> 'mobile-number','required']) }}
                <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                    <span class="input__label-content input__label-content--hoshi">Enter your 10 digit mobile number</span>
                </label>
            </span>
        </div>

        <div class="send-code">
            <button class="btn btn-success send-code-button" type="submit" id="send-otp">Send SMS</button>
        </div>
        <div class="err-messages msg-err send-otp-error"></div>
        <div class="err-messages msg-err send-otp-success"></div>
        <div class="verify-inner-content">
            <p>You'll receive a text message containing a code in a moment please enter that code below.</p>
        </div>

        <div class='mobile-number second'>
            <span class="input input--hoshi">
                {{ Form::text('otp', null, ['class' => 'input__field input__field--hoshi', 'id'=> 'otp-code','required']) }}
                <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                    <span class="input__label-content input__label-content--hoshi">6 digit code</span>
                </label>
            </span>
        </div>
        <div class="send-code second">
            <button class="btn btn-success send-code-button" type="submit" id="verify-otp">Verify</button>
        </div>
        <div class="err-messages msg-err verify-otp-error"></div>
        <div class="err-messages msg-err verify-otp-success"></div>
    </div>
    {!! Form::close() !!}
</div>