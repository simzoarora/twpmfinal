@if ($questionArray->type == config('constant.service_question_types_options_inverse.mcma'))
<div class="each-question" id="ques-{{ $questionArray->id }}">
    <p class="question">{{ $questionArray->question }}</p>
    <?php $count = 50; ?>
    @foreach ($questionArray->serviceQuestionOption as $optionArray)
    <div class="squaredTwo">
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
        {{ Form::checkbox('answer['.$key.'][value][]', $optionArray->id, null, ['id' => $key.$count.'[option_name]']) }}
        {{ Form::label($key.$count.'[option_name]', ' ') }}
        <span>{!! $optionArray->name !!}</span>
    </div>
    <?php $count++; ?>
    @endforeach
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray->type == config('constant.service_question_types_options_inverse.mcsa'))
<div class="each-question" id="ques-{{ $questionArray->id }}">
    <p class="question">{{ $questionArray->question }}</p>
    <ul class="radio-cirle">
        <?php $count = 50 ?>
        @foreach ($questionArray->serviceQuestionOption as $optionArray)
        <li>
            {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
            {{ Form::radio('answer['.$key.'][value][]', $optionArray->id, null, ['id' => $key.$count.'[option_name]']) }}
            {{ Form::label($key.$count.'[option_name]',$optionArray->name , ['class' => '']) }}
            <div class="check"></div>
        </li>
        <?php $count++; ?>
        @endforeach
    </ul>
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray->type == config('constant.service_question_types_options_inverse.sla'))
<div class="each-question-input" id="ques-{{ $questionArray->id }}">
    <span class="input input--hoshi">
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
        {{Form::text('answer['.$key.'][value][]', null,['class' => 'input__field input__field--hoshi','required'])}}
        <label class="input__label input__label--hoshi input__label--hoshi-color-2">
            <span class="input__label-content input__label-content--hoshi">{!! $questionArray->question !!}</span>
        </label>
    </span>
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray->type == config('constant.service_question_types_options_inverse.mla'))
<div class="each-question" id="ques-{{ $questionArray->id }}">
    <p class="question">{{ $questionArray->question }}</p>
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
    {{Form::textarea('answer['.$key.'][value][]',null,['class' => 'form-control hoshi-text','required', 'rows'=> '4'])}}
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray->type == config('constant.service_question_types_options_inverse.usl'))
<div class="each-question" id="ques-{{ $questionArray->id }}">
    <p class="question">{{ $questionArray->question }}</p>
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
    {{ Form::hidden('answer['.$key.'][is_file]',true) }}
    {{ Form::file('answer['.$key.'][value][]',['required', 'class'=>'custom-file-input']) }}
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray->type == config('constant.service_question_types_options_inverse.uml'))
<div class="each-question" id="ques-{{ $questionArray->id }}">
    <p class="question">{{ $questionArray->question }}</p>
    {{ Form::hidden('answer['.$key.'][multiple]', true) }}
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray->id) }}
    {{ Form::hidden('answer['.$key.'][is_file]',true) }}
    {{ Form::file('answer['.$key.'][value][]', ['multiple'=>true,'required', 'class'=>'custom-file-input']) }}
    <span class="err-messages msg-err"></span>
</div>
@endif