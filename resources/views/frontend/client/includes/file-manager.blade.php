<style>

    #content .file-item img {
        max-width: 201px;
        max-height: 200px;
        padding: 10px 10px 35px 10px;
        margin-bottom: 16px;
    }
    #content .file-item .fa-file-pdf-o {
        padding: 10px 10px 35px 10px;
        margin-bottom: 16px;
        text-align: center;
    }
    #content .img-row {
        width: 20%;
    }
    #content .img-row .clickable{
        min-height: 206px;   
        border: 1px solid #cccccc;
    }
    #content .img-row .caption .btn-group {
        width: 100%;
    }
    #content .img-row .caption .btn-group .file-item {
        width: 87%;
        overflow: hidden;
        text-overflow: ellipsis;
    }
</style>



<div class="row fill">
    <div class="wrapper fill">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 right-nav">
            <div class="row">
                <div class="col-md-12">
                    @if($extension_not_found)
                    <div class="alert alert-warning"><i class="glyphicon glyphicon-exclamation-sign"></i> {{ Lang::get('laravel-filemanager::lfm.message-extension_not_found') }}</div>
                    @endif
                </div>
            </div>

            @if (isset($errors) && $errors->any())
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <ul>
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endif

            <div id="content" class="fill">

            </div>
        </div>
    </div>
</div>
<input type='hidden' name='working_dir' id='working_dir' value='{{$working_dir}}'>
<input type='hidden' name='show_list' id='show_list' value='{{ ($startup_view == 'list') ? 1 : 0 }}'>
<input type='hidden' name='type' id='type' value='{{$file_type}}'>
<div class="modal fade" id="fileViewModal" tabindex="-1" role="dialog" aria-labelledby="fileLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="fileLabel">{{ Lang::get('laravel-filemanager::lfm.title-view') }}</h4>
            </div>
            <div class="modal-body" id="fileview_body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ Lang::get('laravel-filemanager::lfm.btn-close') }}</button>
            </div>
        </div>
    </div>
</div>

