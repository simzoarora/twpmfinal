@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.mcma'))
<div class="each-question" id="ques-{{ $questionArray[0]->id }}">
    <p class="question">{{ $questionArray[0]->question }}</p>
    <?php $count = 0; ?>
    @foreach ($questionArray[0]->serviceQuestionOption as $optionArray)
    <div class="squaredTwo">
        <?php
        $value = null;
        foreach ($questionArray as $answer) {
            if ($optionArray->id == $answer->pivot->value) {
                $value = 'checked';
            }
        }
        ?>
        {{ Form::hidden('answer['.$key.'][checkbox]', $questionArray[0]->pivot->value) }}
        {{ Form::hidden('answer['.$key.'][answer_id]', $questionArray[0]->pivot->id) }}
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
        {{ Form::checkbox('answer['.$key.'][value][]', $optionArray->id, $value, ['id' => $key.$count.'[option_name]']) }}
        {{ Form::label($key.$count.'[option_name]', ' ') }}
        <span>{!! $optionArray->name !!}</span>
    </div>
    <?php $count++; ?>
    @endforeach
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.mcsa'))
<div class="each-question" id="ques-{{ $questionArray[0]->id }}">
    <p class="question">{{ $questionArray[0]->question }}</p>
    <ul class="radio-cirle">
        <?php $count = 0 ?>
        @foreach ($questionArray[0]->serviceQuestionOption as $optionArray)
        <li>
            <?php $value = $questionArray[0]->pivot->value == $optionArray->id ? 'checked' : null; ?>
            {{ Form::hidden('answer['.$key.'][answer_id]',$questionArray[0]->pivot->id) }}
            {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
            {{ Form::radio('answer['.$key.'][value][]', $optionArray->id, $value, ['id' => $key.$count.'[option_name]']) }}
            {{ Form::label($key.$count.'[option_name]',$optionArray->name , ['class' => '']) }}
            <div class="check"></div>
        </li>
        <?php $count++; ?>
        @endforeach
    </ul>
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.sla'))
<div class="each-question-input" id="ques-{{ $questionArray[0]->id }}">
    <span class="input input--hoshi">
        {{ Form::hidden('answer['.$key.'][answer_id]',$questionArray[0]->pivot->id) }}
        {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
        {{Form::text('answer['.$key.'][value][]', $questionArray[0]->pivot->value,['class' => 'input__field input__field--hoshi','required'])}}
        <label class="input__label input__label--hoshi input__label--hoshi-color-2">
            <span class="input__label-content input__label-content--hoshi">{!! $questionArray[0]->question !!}</span>
        </label>
    </span>
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.mla'))
<div class="each-question" id="ques-{{ $questionArray[0]->id }}">
    <p class="question">{{ $questionArray[0]->question }}</p>
    {{ Form::hidden('answer['.$key.'][answer_id]', $questionArray[0]->pivot->id) }}
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
    {{Form::textarea('answer['.$key.'][value][]',$questionArray[0]->pivot->value,['class' => 'form-control hoshi-text','required', 'rows'=> '4'])}}
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.usl'))
<div class="each-question" id="ques-{{ $questionArray[0]->id }}">
    <p class="question">{{ $questionArray[0]->question }}</p>
    {{ Form::hidden('answer['.$key.'][answer_id]',$questionArray[0]->pivot->id) }}
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
    {{ Form::hidden('answer['.$key.'][is_file]',true) }}
    {{ Form::file('answer['.$key.'][value][]',['class'=>'custom-file-input']) }}
    <a href="{{ route('frontend.client.downloadFile', ['client',$questionArray[0]->pivot->sharefile_item_id,$serviceId]) }}" target="_blank">{{ $questionArray[0]->pivot->value}}</a> 
    <span class="err-messages msg-err"></span>
</div>
@endif

@if ($questionArray[0]->type == config('constant.service_question_types_options_inverse.uml'))
<div class="each-question" id="ques-{{ $questionArray[0]->id }}">
    <p class="question">{{ $questionArray[0]->question }}</p>
    {{ Form::hidden('answer['.$key.'][answer_id]',$questionArray[0]->pivot->id) }}
    {{ Form::hidden('answer['.$key.'][multiple]', true) }}
    {{ Form::hidden('answer['.$key.'][question_id]',$questionArray[0]->id) }}
    {{ Form::hidden('answer['.$key.'][is_file]',true) }}
    {{ Form::file('answer['.$key.'][value][]', ['multiple'=>true, 'class'=>'custom-file-input']) }}

    <?php foreach ($questionArray as $answer) { ?>
        <a href="{{ route('frontend.client.downloadFile', ['client', $answer->pivot->sharefile_item_id, $serviceId]) }}" target="_blank">{{ $answer->pivot->value}}</a> 
    <?php } ?>
    <span class="err-messages msg-err"></span>
</div>
@endif