@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/signUpOne.css')) }}   
@stop

@section('content')
<div class="col-sm-12 header-bar">
    <div class="container">
        <div class="row">
            <img src="img/twpm-blue-logo.png" class="logo" alt="">
            <p>Risk Tolerance</p>
        </div>
    </div>
</div>

<div class="container-fluid wrapper">
    <div class="container">
        
        <div class="col-sm-8 col-sm-offset-2 site-heading-1 main-heading">
            <h2>We're determining a potential allocation plan <br> for you, based on your risk profile...</h2>

            

        </div>
        <div class="next">
            <p>(next)</p>

        </div>
        <div class="footer">
            <p class="description">FOOTER LINKS</p>
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div> 
    </div> 
</div> 

@endsection