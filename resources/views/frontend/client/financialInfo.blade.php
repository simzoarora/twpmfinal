@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/finish-account.css')) }}
@stop
@section('content')
<div class="wrapper">
    <div class="col-sm-12 header-bar">
        <div class="container">
            {{ HTML::image('img/twpm-blue-logo.png', null,['class'=>'logo']) }}
            <p>Finish Setup</p>
        </div>
    </div>
    <div class="container-fluid wrapper">
        <div class="container">
            <div class="step">
                <p>Step4: Financial background </p>
                <div class="blue-line col-sm-8">
                </div>
                <div class="grey-line col-sm-4">
                </div>
                <div class="container-first">
                </div> 
            </div>

            <div class="row">
                <div class="col-sm-10 col-sm-offset-1 sections">
                    <div class="col-sm-6 section-left">
                        <h2> Financial background</h2>
                        <p>As financial advisors, we need to ask a bit more about your <br> financial background so that we can be sure we are giving you <br> the best possible advice.</p>
                    </div>
                    <div class="col-sm-6 section-right">
                        {{ Form::open(['route' => 'frontend.client.saveFinancialInfo']) }}

                        <div class="col-sm-8 inner-left tax_filing_status">
                            <i class="fa fa-angle-down financial-info-arrow" aria-hidden="true"></i>
                            <label> Tax filing status </label>
                            {{ Form::select('filing_status', config('constant.tax_filing_status'),null, ['class' => 'field full-width', 'id'=>'tax_filing_status']) }}
                        </div>

                        <!--Used for spouse_has_twpm_account  visibilty-->
                        @php $value_filled = 'display: none;' @endphp
                        @if(session()->getOldInput('spouse_has_twpm_account'))
                        @php $value_filled = 'display: block;' @endphp
                        @endif
                        <!--Used for spouse_has_twpm_account and spouse_annual_income visibilty-->
                        @php $error_found = 'display: none;' @endphp
                        @if($errors->has('spouse_annual_income'))
                        @php $error_found = 'display: block;'; $value_filled = 'display: block;' @endphp
                        @endif

                        <div class="spouse-account-checkbox two-options-auth" style="{{ $value_filled }}">
                            <label class="col-sm-8 checkbox-custom-label "  >
                                {{ Form::input("checkbox",'spouse_has_twpm_account', 1, ['class'=>'form-control']) }}
                                <span class="checkbox-icon"></span>
                                <p>My spouse also has a Total Wealth account</p>
                            </label>
                        </div>

                        <div class="col-sm-8 inner-left"> 
                            <label> Your annual income </label>
                            {{ Form::input('number','annual_income', preg_replace('/[^0-9]/', '', $user_annual_income), ['placeholder' => '$', 'class'=>'form-control']) }}
                            @if($errors->has('annual_income'))
                            <div class="alert alert-danger">{{ $errors->first('annual_income') }}</div>
                            @endif
                        </div>

                        <div class="col-sm-8 inner-left two-options-auth" style="{{ $error_found }}">
                           <label for="label"> Spouse/partner's first name </label>
                            {{ Form::input('text','spouse_name', null, ['placeholder' => '', 'class'=>'spouse_name form-control']) }}
                            @if($errors->has('spouse_name'))
                            <div class="alert alert-danger">{{ $errors->first('spouse_name') }}</div>
                            @endif
                        </div>
                        <div class="col-sm-8 inner-left two-options-auth" style="{{ $error_found }}">
                            <label> Your spouse's annual income </label>
                            {{ Form::input('number','spouse_annual_income', null, ['placeholder' => '$', 'class'=>'spouse_annual_income form-control']) }}
                            @if($errors->has('spouse_annual_income')) 
                            <div class="alert alert-danger">{{ $errors->first('spouse_annual_income') }}</div>
                            @endif
                        </div>

                        <div class="site-heading-1"> 
                            <div class="next-page align-left col-sm-8">
                                <button type="submit" class="btn btn-info ">Continue</button> 
                                <!--<a> <p class="col-sm-8">Save and return later</p> </a>-->
                            </div>
                        </div> 
                        {{ Form::close() }}   
                    </div>
                </div>
            </div>

            @include('frontend.includes.footer')
        </div> 
    </div> 
</div> 

@section('after-scripts')
<script src="{{ asset('js/financial-information.js') }}"></script>

@endsection 