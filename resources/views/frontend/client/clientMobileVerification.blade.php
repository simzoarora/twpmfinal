@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="dashboard">

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                @include('frontend.includes.client_header')

                <div class="clearfix"></div>

                <div class="client">
                    <div id="body-content" class="container-fluid">
                        <div class="container">
                            <div class="inner-content">
                                <h2 class='content-heading'>Verify Mobile Number</h2>
                                <p class='heading-info'>For added security and timely notifications.</p>
                                @if($mobileNumber->mobile_verified == 1)
                                <div>
                                    {{ Form::label('mobile_number', $mobileNumber->mobile_number, ['class' => 'col-md-4 control-label']) }}
                                    <div class="err-messages send-otp-success" > Mobile Verified</div>
                                </div>
                                @else
                                <div class=" padding-0 client-verify-mobile">
                                    @include('frontend.client.includes.mobileVerify')
                                </div>
                                @endif
                                <div id="mobile-verify-mobile-success" style="display: none">
                                    {{ Form::label('mobile_number', $mobileNumber->mobile_number, ['class' => 'col-md-4 control-label']) }}
                                    <div class="err-messages send-otp-success"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script>
    var sendOtpUrl = '{{route("frontend.client.sendOtp")}}',
            verifyOtpUrl = '{{route("frontend.client.verifyOtp")}}';
</script>
<script src="{{ asset('js/step-three.js') }}"></script>
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop