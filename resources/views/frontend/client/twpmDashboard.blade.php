<!DOCTYPE html>
<html>
    <head>
        <title>Twpm Dashboard</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/twpmDashboard.css">
    </head>

    <?php dump($Services); ?>  
    <body> 
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1">

                </div>
                <div class="col-sm-11 get-started">
                    <h2>Welcome Jane</h2>
                    <p>This is your dashboard. Based on your financial goals, <br> we've made some recommendations to get you 
                        started.</p>
                    <p class="grey-active-service"><a href="#">Active Services</a><span>All services</span></p>
                    <div class="row service-name">

                        <div class="previous"> </div>

                        <div class="service-options owl-carousel"> 
                            <div class='col-sm-12 products-list'>
                                <div class="nested-div">
                                    <p><i>Recommended</i><br>Service<br>Name</p>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class="nested-div">
                                    <p><i>Recommended</i><br>Service<br>Name</p>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class="nested-div">
                                    <p><i>Recommended</i><br>Service<br>Name</p>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class="nested-div">
                                    <p class="p-inc-height">Service<br>Name</p>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class="nested-div">
                                    <p class="p-inc-height">Service<br>Name</p>
                                </div>
                            </div> 
                        </div>
                        <div class="next"> </div>
                    </div>

                    <div class="row open-service-name">
                        <div class="col-sm-12"> 
                            <div class="row"> 
                                <div class="left-div">  
                                    <i>Recommended</i>
                                    <h3>Service Name</h3>
                                    <h5>Description</h5>
                                    <p>Contrary to popular belief,Lorem ipsum is not simply random text. It has roots in a piece of
                                        classical Latin literature from</p>
                                    <h5>Recommended for</h5>
                                    <p>At least $250,000 in investable assets.</p>
                                    <h5>Cost</h5>
                                    <p> $150 for up to a one hour phone consultation.</p>
                                    <p> $250 for up to a one hour in person consultation.</p>
                                    <!--<a href="#" class="pd-left">Fee Schedule</a>-->
                                    <!--                                <h5>Helpful Links</h5>
                                                                    <p><a href="#">What to bring to your initial planning meeting</a></p>-->

                                    <button type="button" class="btn btn-default">Get Started</button>
                                </div>
                            </div>
                        </div>
                        <!--                        <div class="col-sm-5">
                                                    <div class="right-div">
                                                        <p>ENROLLMENT</p>
                                                        <p><i>Recommended Services</i></p>
                                                        <h5>Service Name<i>TBD</i></h5>
                                                        <h5 class="m-top">Service Name<i>$750</i></h5>
                                                        <h5 class="m-top line-h-1">Service Name<i>$250</i></h5>
                                                        <i>Other Services</i>
                                                        <p class="grey-para">None selected</p>
                                                        <p class="due-today">Due Today<span>$1000</span></p>
                                                        <a href="#" class="btn btn-enroll">ENROLL</a>
                                                    </div>
                                                </div>-->
                    </div>
                </div>
            </div>
        </div>

        <script src="{{ asset('js/signUp.js') }}"></script> 
                <!--<script src="owlcarousel/owl.carousel.min.js"></script>-->

        <script>
$(document).ready(function () {
    $(".service-options").owlCarousel({

        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            767: {
                items: 2,
                nav: false
            },
            991: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    });


});
        </script>
    </body>
</html>