@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/dashboard.css')) }}   
@stop 


@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.client_sidebar')

            <div class="right-content client-resources">
                <?php $page_title = 'Resources'; ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    <?php
                    if (!$topics->isEmpty()) {
                        foreach ($topics as $topic) {
                            ?>
                            <a href="{{ route('frontend.client.resource.details', [config('constant.subdomain'), $topic->id]) }}">
                                <div class="col-sm-3 service-outer">
                                    <div class="service-name">
                                        <p><?php echo $topic->title; ?></p>
                                    </div>
                                </div>
                            </a>
                            <?php
                        }
                    } else {
                        ?>
                        <div>No resources present.</div>
                    <?php }
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop