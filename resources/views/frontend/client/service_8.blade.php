@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(asset('css/single-service-questions.css')) }} 
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">                                                                                                                                                                                                                                                                                                                                                                                                           
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                @include('frontend.includes.contact')
                <div class="col-sm-12 recommended-service-div">
                    <!--<form id="services-question-form" action="" class= 'col-sm-11 col-xs-11'>-->
                        <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}-->
                        <!--{{ Form::input('hidden','serviceId',$currentService->id) }}-->

                            <section class="sections">
                                @include('frontend.client.serviceQuestions.comprehensive-final-review') 
                            </section> 
                        <!--<a href="{{route('frontend.client.recommendedServices',[config('constant.subdomain')]) }}" class="returnLater">Save and return later</a>-->
                        <!--{{ Form::close() }}-->
                    <!--</form>-->
                </div> 
            </div>
        </div>
    </div>
</div> 



@stop 
@section('after-scripts')
<script src="{{ asset('js/comprehensive-planning.js') }}"></script>

<script>

<?php if (session::get('tax_filing_status') == config('constant.tax_filing_status_inverse.single')) { ?>
        $('.marital-section').hide();
<?php } else { ?>
        $('.marital-section').show();
<?php } ?>
</script>
@stop