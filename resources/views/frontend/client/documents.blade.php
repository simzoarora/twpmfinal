@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles')
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.2.2/cropper.min.css">
<style>
    html, body {
        height: 100%;
    }

    .img-row {
        overflow: visible;
        margin-bottom: 10px;
    }

    .container {
        height: 100%;
        margin-left: 5px;
        margin-right: 5px;
        width: 99%;
    }

    .fill {
        height: 100%;
        min-height: 100%;
    }

    .wrapper {
        height: 100%;
    }

    #lfm-leftcol {
        min-height: 80%;
    }

    #right-nav {
        border-left: 1px solid silver;
        height: 90%;
        min-height: 90%;
    }

    #content {
        /*overflow: auto;*/
    }

    #tree1 {
        margin-left: 5px;
    }

    .pointer {
        cursor: pointer;
    }

    .img-preview {
        background-color: #f7f7f7;
        overflow: hidden;
        width: 100%;
        text-align: center;
        height: 200px;
    }

    .hidden {
        display: none;
    }
</style>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.css">
{{ Html::style(asset('css/dashboard.css')) }} 
@stop

@section('content') 
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')

            <div class="right-content">
                <?php $page_title = 'Documents'; ?>
                 @if(Session::has('confirmation_message'))
                <p class="alert {{ Session::get('alert-class', 'alert-info') }}">     {!! session()->get('confirmation_message')!!}      </p>
                @endif
                @include('frontend.includes.client_header')

                <div class="col-sm-12 products-list">
                    @include('frontend.client.includes.file-manager', array('working_dir'=> $working_dir, 'file_type' => $file_type, 'extension_not_found' => $extension_not_found,'startup_view' => 'list'))
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.0/jquery-migrate.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.3.0/bootbox.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cropperjs/1.2.2/cropper.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
@include('frontend.client.includes.file-manager-script')
<script src="{{ asset('js/dashboard.js') }}"></script>
@stop
