@extends('frontend.layouts.master')

@section('title')
Client Password Reset
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/client.css')) }}   
@stop

@section('content')
<div class="client">
    <div id="body-content" class="container-fluid">

        <div class="container">
            <div class="inner-content">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <h2 class='content-heading' style="color: #030303">{{ trans('labels.frontend.passwords.reset_password_box_title') }}</h2>

                {{ Form::open(['route' =>'frontend.auth.password.email.post','class' => 'form-horizontal abc', 'role' => 'form', 'method' => 'post']) }}
                <div class="form-group" id='basic-information'>
                    <div class='basic-information-content'>
                        <span class="input input--hoshi <?php
                        if ($errors->first('email')) {
                            echo 'input-error';
                        }
                        ?>">
<!--                            <input class="input__field input__field--hoshi" type="email" name="email" required>
                            <label class="input__label input__label--hoshi input__label--hoshi-color-2">
                                <span class="input__label-content input__label-content--hoshi">Email Address</span>
                            </label>-->
                            <!--{{ Form::label('mobile_number', 'Mobile Number') }}-->
                            {{ Form::input('email','email',null, ['class'=> 'form-control reset-email', 'placeholder'=>'Email']) }}
                        </span>
                        <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>
                    </div>

                    <div class="submit-btn" id='button-color'>
                        {{ Form::submit(trans('labels.frontend.passwords.send_password_reset_link_button'), ['class' => 'btn contact-info-button']) }}
                    </div>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
</div>
@endsection

@section('after-scripts') 
<script src="{{ asset('js/client-login.js') }}"></script>
@stop