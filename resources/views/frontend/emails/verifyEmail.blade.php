<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
<script src="https://use.fontawesome.com/b6b985d66e.js"></script>
<style type="text/css">
    *{
        font-family:'Lato',Arial, sans-serif;
    }
</style>
<div style="max-width:601px; min-width:300px;background-color: #fff;font-family:'Lato',Arial, sans-serif; margin:auto; color:#4c4b4b;">
    <div style="margin: auto;float: left;">
        <div style="padding: 0 30px 40px 30px;font-size:17px;float:left;border-bottom:1px solid #D2D7DA;width: 100%;box-sizing: border-box;"> 
            <div style="margin-top: 50px;">
                <div style="text-align:center; background-color: #187ab3;">
                    <img src="{{$message->embed(public_path().'/img/twpm-wht.png')}}" style="width:130px;  padding:7px 0;"/>
                </div>
                <p>Hello,</p>
                <h4>It’s time to confirm your email address.</h4>
                <p>Do you have the right email address for you? To confirm that you can get our emails, just click on the button below.</p>
                <p style="margin-top:40px; margin-bottom:40px; text-align: center;">
                    <a href="{{route('frontend.auth.account.confirm', $user->confirmation_code)}}" target="_other" style="background-color: #2179ee; color: #fff; text-decoration: none; border-radius: 3px; padding: 12px 21px;">Confirm email</a>
                </p>
                <hr>
                <p>Thanks,<br/>The Total Wealth Team</p>
            </div>
        </div>
        <div style="text-align: center; float:left; width: 100%; padding:20px 0px">
            <div style="margin:auto; text-align:center; display:inline-block; width: 100%; margin-bottom: 15px;">
                <a target='_blank' href="{{ route('frontend.legal') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;border-right: 1px solid #4c4b4b;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">Legal Disclosures</a>
                <a target='_blank' href="{{ route('frontend.privacy') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;border-right: 1px solid #4c4b4b;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">Privacy & Security</a>
                <a target='_blank' href="{{ route('frontend.terms') }}" style="color:#4c4b4b;text-decoration:none;padding:0 10px;font-size: 14px;display: inline-block;line-height: 11px;vertical-align: middle;">Terms of Use</a>
            </div>
            <h5 style="text-align:center">© 2016 Total Wealth Planning Management.</h5>
        </div>
    </div>
</div>