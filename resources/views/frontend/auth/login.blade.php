@extends('frontend.layouts.master')

@section('title')
Login
@stop

@section('meta_description')
@stop

@section('after-styles')
{{ Html::style(asset('css/clientLogin.css')) }}
@stop

@section('content')
<div class = 'wrapper container-fluid'> 
    <div class = 'center-div col-sm-4'>
        <h2>Welcome back</h2>
        {{ Form::open(['route' => 'frontend.auth.login', 'class' => '']) }}
        <div class="information" >
            @include('includes.partials.logged-in-as')
            @include('includes.partials.messages')
            {{ Form::label('email', 'Email', ['class' => '']) }}
            {{ Form::input('email', 'email', null, ['class' => 'form-control']) }}
            <p class="err-messages msg-err"><?php echo $errors->first('email'); ?></p>

            {{ Form::label('password', trans('validation.attributes.frontend.password'), ['class'=>'']) }}
            <p class="forgot">{{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}</p>

            {{ Form::input('password', 'password', null, ['class' => 'form-control']) }}
            <p class="err-messages msg-err"><?php echo $errors->first('password'); ?></p>
            <button type="submit" class="btn btn-primary loginBtn">Log in</button>

            <div class="signup-yet">
                Not a client yet?
                <a href="{{route('frontend.client.signUp', [config('constant.subdomain')])}}">Sign up</a>
            </div>
        </div>
        {{ Form::close() }}     
    </div>
</div>
@endsection