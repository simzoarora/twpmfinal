<!--Annual portfolio //-->
<div class="service-preview">
    <div class=" topic-section">

        <div class=" ">
            <h3>Annual Portfolio Review</h3>
        </div>
        <div class="col-sm-12 annual-portfolio-hide-topics topic-list-wrapper ">
            <ul class="col-sm-6 topic-list">
                <li class="single-topic annual-portfolio-topic-1">
                    confirm income/tax profile
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic annual-portfolio-topic-2">
                    debts/liabilities
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic annual-portfolio-topic-3">
                    investment statements
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
            </ul>
        </div>
    </div>


    <div class="annual-portfolio-confirm-income question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Confirm income/Tax profile
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Total income from all sources">Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][household_gross_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Taxable income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][estimated_taxable_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="select" name="answer[207][tax_filling_status]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income">Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][tax_bracket]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-debt-liabilities question-section" style="left: 200%;">
        <div class="sub-topic-section">
            <div class="sub-topic-heading" >
                <i class="fa fa-angle-left pull-left back-to-topics"></i>
                <h3>Debt/Liabilities
                    <i class="fa fa-angle-right pull-right"></i>
                </h3>
            </div>
            <div class="col-sm-12 annual-portfolio-hide-sub-topics sub-topic-list-wrapper ">
                <ul class="col-sm-6 sub-topic-list">
                    <li class="single-sub-topic annual-portfolio-sub-topic-1">
                        mortgage
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic annual-portfolio-sub-topic-2">
                        home equity line of credit
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic annual-portfolio-sub-topic-3">
                        vehicle loans
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic annual-portfolio-sub-topic-4">
                        credit cards
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic annual-portfolio-sub-topic-5">
                        student loans
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic annual-portfolio-sub-topic-6">
                        other debts(non-business)
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-mortgage question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Mortgage
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Mortgages on non-rental property</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[208][semiAnnualConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-heloc question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Equity line of credit (HELOC)
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any home equity lines of credit</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[209][semi_annual_heloc_confirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-auto-loans question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Auto loans
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Vehicle loans</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[210][semi-annual-vl-vechicle-loan]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-credit-cards question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Credit cards
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Credit cards on which you carry </p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[211][semi-annual-cc-confirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-student-loan question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Student loan
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any student loans </p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[212][semiAnnualSLConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="annual-portfolio-other-debts question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Other non-business related debts
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any other non-business related debts</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[213][semiAnnualODConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Annual Portfolio//-->




<!--semi annual //-->

<div class="service-preview">
    <div class=" topic-section">

        <div class=" ">
            <h3>Semi Annual 401(k)Review</h3>
        </div>
        <div class="col-sm-12 semi-annual-portfolio-hide-topics topic-list-wrapper ">
            <ul class="col-sm-6 topic-list">
                <li class="single-topic semi-annual-portfolio-topic-1">
                    confirm income/tax profile
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic semi-annual-portfolio-topic-2">
                    debts/liabilities
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic semi-annual-portfolio-topic-3">
                    investment statements
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
            </ul>
        </div>
    </div>


    <div class="semi-annual-portfolio-confirm-income question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Confirm income/Tax profile
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Total income from all sources">Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][household_gross_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Taxable income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][estimated_taxable_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="select" name="answer[207][tax_filling_status]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p  title="Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income">Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[207][tax_bracket]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-debt-liabilities question-section" style="left: 200%;">
        <div class="sub-topic-section">
            <div class="sub-topic-heading" >
                <i class="fa fa-angle-left pull-left back-to-topics"></i>
                <h3>Debt/Liabilities
                    <i class="fa fa-angle-right pull-right"></i>
                </h3>
            </div>
            <div class="col-sm-12 semi-annual-portfolio-hide-sub-topics sub-topic-list-wrapper ">
                <ul class="col-sm-6 sub-topic-list">
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-1">
                        mortgage
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-2">
                        home equity line of credit
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-3">
                        vehicle loans
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-4">
                        credit cards
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-5">
                        student loans
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                    <li class="single-sub-topic semi-annual-portfolio-sub-topic-6">
                        other debts(non-business)
                        <i class="fa fa-angle-right pull-right"></i>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-mortgage question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Mortgage
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Mortgages on non-rental property</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[208][semiAnnualConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-heloc question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Equity line of credit (HELOC)
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any home equity lines of credit</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[209][semi_annual_heloc_confirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-auto-loans question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Auto loans
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Vehicle loans</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[210][semi-annual-vl-vechicle-loan]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-credit-cards question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Credit cards
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Credit cards on which you carry </p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[211][semi-annual-cc-confirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-student-loan question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Student loan
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any student loans </p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[212][semiAnnualSLConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="semi-annual-portfolio-other-debts question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-annual-sub-topics"></i>
            <h3>Other non-business related debts
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Any other non-business related debts</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[213][semiAnnualODConfirmation]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!--semi annual end //-->



<script>

    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
        //        annual portfolio //

//        $(document).on('click', '.annual-portfolio-topic-1', function () {
//            $('.annual-portfolio-confirm-income').animate({left: '0'});
//            $('.annual-portfolio-hide-topics').hide(300);
//        });
//        $(document).on('click', '.back-to-topics', function () {
//            $('.annual-portfolio-confirm-income').animate({left: '200%'});
//            $('.annual-portfolio-hide-topics').show(300);
//        });
//        //        debt/liabilities//
//        $(document).on('click', '.annual-portfolio-topic-2', function () {
//            $('.annual-portfolio-debt-liabilities').animate({left: '0'});
//            $('.annual-portfolio-hide-topics').hide(300);
//        });
//        $(document).on('click', '.back-to-topics', function () {
//            $('.annual-portfolio-debt-liabilities').animate({left: '200%'});
//            $('.annual-portfolio-hide-topics').show(300);
//        });
//        //        mortgage //
//        $(document).on('click', '.annual-portfolio-sub-topic-1', function () {
//            $('.annual-portfolio-mortgage').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
//        $(document).on('click', '.back-to-annual-sub-topics', function () {
//            $('.annual-portfolio-mortgage').animate({left: '200%'});
//            $('.annual-portfolio-debt-liabilities').show(300);
//        });
//        //        heloc//
//        $(document).on('click', '.annual-portfolio-sub-topic-2', function () {
//            $('.annual-portfolio-heloc').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
//        $(document).on('click', '.back-to-annual-sub-topics', function () {
//            $('.annual-portfolio-heloc').animate({left: '200%'});
//            $('.annual-portfolio-debt-liabilities').show(300);
//        });
//        //        auto loans //
//        $(document).on('click', '.annual-portfolio-sub-topic-3', function () {
//            $('.annual-portfolio-auto-loans').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
//        $(document).on('click', '.back-to-annual-sub-topics', function () {
//            $('.annual-portfolio-auto-loans').animate({left: '200%'});
//            $('.annual-portfolio-debt-liabilities').show(300);
//        });
//        //        credit cards //
//        $(document).on('click', '.annual-portfolio-sub-topic-4', function () {
//            $('.annual-portfolio-credit-cards').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
//        $(document).on('click', '.back-to-annual-sub-topics', function () {
//            $('.annual-portfolio-credit-cards').animate({left: '200%'});
//            $('.annual-portfolio-debt-liabilities').show(300);
//        });
//        //student loan //
//        $(document).on('click', '.annual-portfolio-sub-topic-5', function () {
//            $('.annual-portfolio-student-loan').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
//        $(document).on('click', '.back-to-annual-sub-topics', function () {
//            $('.annual-portfolio-student-loan').animate({left: '200%'});
//            $('.annual-portfolio-debt-liabilities').show(300);
//        });
//        //other debts //
//        $(document).on('click', '.annual-portfolio-sub-topic-6', function () {
//            $('.annual-portfolio-other-debts').animate({left: '0'});
//            $('.annual-portfolio-debt-liabilities').hide(300);
//        });
        
        
        
        
        
        
//        semi-annual//
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-other-debts').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        $(document).on('click', '.semi-annual-portfolio-topic-1', function () {
            $('.semi-annual-portfolio-confirm-income').animate({left: '0'});
            $('.semi-annual-portfolio-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.semi-annual-portfolio-confirm-income').animate({left: '200%'});
            $('.semi-annual-portfolio-hide-topics').show(300);
        });
        //        debt/liabilities//
        $(document).on('click', '.semi-annual-portfolio-topic-2', function () {
            $('.semi-annual-portfolio-debt-liabilities').animate({left: '0'});
            $('.semi-annual-portfolio-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.semi-annual-portfolio-debt-liabilities').animate({left: '200%'});
            $('.semi-annual-portfolio-hide-topics').show(300);
        });
        //        mortgage //
        $(document).on('click', '.semi-annual-portfolio-sub-topic-1', function () {
            $('.semi-annual-portfolio-mortgage').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-mortgage').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        //        heloc//
        $(document).on('click', '.semi-annual-portfolio-sub-topic-2', function () {
            $('.semi-annual-portfolio-heloc').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-heloc').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        //        auto loans //
        $(document).on('click', '.semi-annual-portfolio-sub-topic-3', function () {
            $('.semi-annual-portfolio-auto-loans').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-auto-loans').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        //        credit cards //
        $(document).on('click', '.semi-annual-portfolio-sub-topic-4', function () {
            $('.semi-annual-portfolio-credit-cards').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-credit-cards').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        //student loan //
        $(document).on('click', '.semi-annual-portfolio-sub-topic-5', function () {
            $('.semi-annual-portfolio-student-loan').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-student-loan').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
        //other debts //
        $(document).on('click', '.semi-annual-portfolio-sub-topic-6', function () {
            $('.semi-annual-portfolio-other-debts').animate({left: '0'});
            $('.semi-annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.semi-annual-portfolio-other-debts').animate({left: '200%'});
            $('.semi-annual-portfolio-debt-liabilities').show(300);
        });
    });

</script>