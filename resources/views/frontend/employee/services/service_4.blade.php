<!--GOAL SPECIFIC AND FINANCIAL PLANNING-->
<div class="service-preview">
    <div class=" topic-section">

        <div class="  ">
            <h3>Goal-Specific Financial Planning</h3>
        </div>
        <div class="col-sm-12 goal-Specific-hide-topics topic-list-wrapper ">
            <ul class="col-sm-6 topic-list"> 
                <li class="single-topic goal-specific-topic-1">
                    about you and your family
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-2">
                    income & investments
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-3">
                    real estate assets
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-4">
                    goal details
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-5">
                    additional notes
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
            </ul>
        </div>
    </div>


    <div class="goal-specific-about-family question-section" style="left: 200%;">
       43
    </div>
    
    
    <div class="goal-specific-income-investment question-section" style="left: 200%;">
       42
    </div>
    
    
    

    <div class="goal-specific-real-estate question-section" style="left: 200%;">
        41
    </div>

    <!--goal details //-->

    <div class="goal-specific-goal-details question-section" style="left: 200%;">
       40
    </div>





    <div class="goal-specific-additional-notes question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Goal Details
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>One time expense or annually recurring</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="textarea" name="answer[39][additional_notes]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });










        //goal specific financial planning // 


        //about family //
        $(document).on('click', '.goal-specific-topic-1', function () {
            $('.goal-specific-about-family').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-about-family').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });

        //income and investment //
        $(document).on('click', '.goal-specific-topic-2', function () {
            $('.goal-specific-income-investment').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-income-investment').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });


        //real estate //
        $(document).on('click', '.goal-specific-topic-3', function () {
            $('.goal-specific-real-estate').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-real-estate').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });


        //goal details//
        $(document).on('click', '.goal-specific-topic-4', function () {
            $('.goal-specific-goal-details').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-goal-details').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });

        //        additional notes//
        $(document).on('click', '.goal-specific-topic-5', function () {
            $('.goal-specific-additional-notes').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-additional-notes').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);

        });
    });
</script>