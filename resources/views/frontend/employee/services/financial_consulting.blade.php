<table width="637" cellspacing="0" cellpadding="7">
    <colgroup><col width="77" /></colgroup><colgroup><col width="160" /></colgroup><colgroup><col width="27" /><col width="23" /></colgroup><colgroup><col width="132" /></colgroup><colgroup><col width="25" /></colgroup><colgroup><col width="95" /></colgroup>
    <tbody>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="10">
                <p><strong>Client Name:</strong></p>
            </td>
            <td colspan="4" bgcolor="#ffffff" width="317">
                <p><strong>TOTAL WEALTH PLANNING AND MANAGEMENT, INC.</strong></p>
            </td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="47">&nbsp;</td>
            <td colspan="4" bgcolor="#ffffff" width="317">&nbsp;</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="11">
                <p>Client Signature Date</p>
            </td>
            <td colspan="4" bgcolor="#ffffff" width="317">
                <p>Adviser Signature Date</p>
            </td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="42">&nbsp;</td>
            <td colspan="4" bgcolor="#ffffff" width="317">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="11">
                <p>Client#2 Signature (If Applicable) Date</p>
            </td>
            <td colspan="4" bgcolor="#ffffff" width="317">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" bgcolor="#ffffff" width="292" height="11">&nbsp;</td>
            <td colspan="4" bgcolor="#ffffff" width="317">&nbsp;</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td bgcolor="#ffffff" width="77" height="11">
                <p>Client Street Address:</p>
            </td>
            <td colspan="6" bgcolor="#ffffff" width="532">&nbsp;</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td bgcolor="#ffffff" width="77" height="11">
                <p>City:</p>
            </td>
            <td bgcolor="#ffffff" width="160">&nbsp;</td>
            <td colspan="2" width="64">
                <p>State:</p>
            </td>
            <td width="132">&nbsp;</td>
            <td bgcolor="#ffffff" width="25">
                <p>Zip:</p>
            </td>
            <td bgcolor="#ffffff" width="95">&nbsp;</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td bgcolor="#ffffff" width="77" height="10">
                <p>Phone:</p>
            </td>
            <td bgcolor="#ffffff" width="160">&nbsp;</td>
            <td colspan="2" bgcolor="#ffffff" width="64">
                <p>E-Mail(s):</p>
            </td>
            <td colspan="3" bgcolor="#ffffff" width="280">&nbsp;</td>
        </tr>
    </tbody>
</table>