<!--stock option exercise strategy and executive Compensation Analysis //-->
<div class="service-preview">
    <div class="stock-option-question-section" style="left: 200%;">
        <div class="topic-heading" >
            <h3>Stock Option Exercise Strategy and Executive Compensation Analysis
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Total income from all sources">Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="number" name="answer[26][household_gross_income]" value="{{(!empty($defaultData) && array_key_exists('housesholdIncome',$defaultData)) ? $defaultData["housesholdIncome"] :null}}">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="number" name="answer[26][estimated_taxable_income]" value="{{(!empty($answer) && array_key_exists("estimated_taxable_income",$answer)) ? $answer["estimated_taxable_income"] :null}}">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="select" name="answer[26][tax_filling_status]" >
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p  title="Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income">Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" name="answer[26][tax_bracket]" value="{{(!empty($answer) && array_key_exists("tax_bracket",$answer)) ? $answer["tax_bracket"] :null}}">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Value of other non-retirement investment account</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" name="answer[26][non_retirement_investment_account]" value="{{(!empty($answer) && array_key_exists("non_retirement_investment_account",$answer)) ? $answer["non_retirement_investment_account"] :null}}">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Name of the employer</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" name="answer[25][name_of_the_employer]" value="{{(!empty($answer) && array_key_exists("name_of_the_employer",$answer)) ? $answer["name_of_the_employer"] :null}}">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Company stock symbol</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" name="answer[25][company_stock_symbol]" value="{{(!empty($answer) && array_key_exists("company_stock_symbol",$answer)) ? $answer["company_stock_symbol"] :null}}">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    });

</script>