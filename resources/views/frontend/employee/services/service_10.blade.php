<div class="service-preview">
    <div class=" topic-section">

        <div class="">
            <h3>Life Insurance & Long Term Review</h3>
        </div>
        <div class="col-sm-12 life-insurance-hide-topics topic-list-wrapper ">
            <ul class="col-sm-6 topic-list">
                <li class="single-topic topic-1" data-topic="301" data-question="45">
                    income/marital status
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic life-insurance-topic-2" data-topic="302" data-question="34">
                    your assets
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic life-insurance-topic-3" data-topic="303" data-question="33">
                    your liabilities
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic life-insurance-topic-4" data-topic="304" data-question="32">
                    your family
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic" data-topic="305" data-question="28">
                    policy information
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
            </ul>
        </div>  
    </div>


    <div class="life-insurance-marital-questions question-section" id="45" style="left: 200%;"> 
        45
    </div> 
    <div class="life-insurance-your-assets question-section" id="34" style="left: 200%;">
        34
    </div>




    <div class="life-insurance-your-liabilities question-section" id="33" style="left: 200%;">
        33
    </div>




    <div class="life-insurance-your-family question-section" id="32" style="left: 200%;">
        32
    </div>



    <div class="life-insurance-children-modal question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-your-family"></i> 
            <h3>Children modal
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class=" col-sm-6" >
            <div class="question-list-wrapper">

                <table>
                    <thead>
                        <tr>
                            <th></th>
                        </tr>   
                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


<script>

    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
//life insurance  //

        //        marital status //
        $(document).on('click', '.topic-1', function () {
            var topic_id = $(this).attr('data-topic');
            $('.life-insurance-marital-questions').animate({left: '0'});
            $('.life-insurance-hide-topics').hide(300);
            $.ajax({
                url: '/getQuestions/' + topic_id,
                data: {},
                success: function () {
                }
            });
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.life-insurance-marital-questions').animate({left: '200%'});
            $('.life-insurance-hide-topics').show(300);
        });
        //        your assets //
        $(document).on('click', '.life-insurance-topic-2', function () {
            $('.life-insurance-your-assets').animate({left: '0'});
            $('.life-insurance-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.life-insurance-your-assets  ').animate({left: '200%'});
            $('.life-insurance-hide-topics').show(300);
        });
        //        your liabilities //
        $(document).on('click', '.life-insurance-topic-3', function () {
            $('.life-insurance-your-liabilities').animate({left: '0'});
            $('.life-insurance-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.life-insurance-your-liabilities').animate({left: '200%'});
            $('.life-insurance-hide-topics').show(300);
        });
        //        your family // 
        $(document).on('click', '.life-insurance-topic-4', function () {
            $('.life-insurance-your-family').animate({left: '0'});
            $('.life-insurance-hide-topics').hide(500);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.life-insurance-your-family').animate({left: '200%'});
            $('.life-insurance-hide-topics').show(300);
        });
        $(document).on('click', '.show-children-modal', function () {
            $('.life-insurance-children-modal').animate({left: '0'});
            $('.life-insurance-your-family').hide(300);
        });
        $(document).on('click', '.back-to-your-family', function () {
            $('.life-insurance-children-modal').animate({left: '200%'});
            $('.life-insurance-your-family').show(300);
        });
    });



</script>

