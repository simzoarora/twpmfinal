 <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Goal Details
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>One time expense or annually recurring</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[40][one_time_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Need for this one-time expense</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if one time" name="answer[40][need_for_one_time_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Anticipate needing per year for this expense?</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if annual " name="answer[40][per_year_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>How many years</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if annual" name="answer[40][how_many_years]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>When do you expect to pay</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="date" name="answer[40][expect_pay_for_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Earmarked just for this goal</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[40][investment_assets_earmarked]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>