<div class="topic-heading" >
    <i class="fa fa-angle-left pull-left back-to-topics"></i> 
    <h3>Your Assets
        <i class="fa fa-angle-right pull-right"></i>
    </h3>
</div>
<div class=" col-sm-6" >
    <div class="question-list-wrapper">
        <ul class="questions-list">
            <li class="question-answer">
                <div class="question-label col-sm-6" > 
                    <p for="label">Do you own or rent your house?</p>
                </div>
                <label class="radio-custom-label">    
                    {{ Form::radio('answer[34][own_or_rent_your_house]', 'own',false,['class'=>'own-house','required'=>'required']) }}Own
                    <span class="radio-icon"></span>
                </label>
                <label class="radio-custom-label">    
                    {{ Form::radio('answer[34][own_or_rent_your_house]', 'rent',false,['class'=>'own-house','required'=>'required']) }}Rent
                    <span class="radio-icon"></span>
                </label> 

            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Estimated house value</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[34][house_value]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Estimated total cash and investment </p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[34][total_of_cash_and_investment_assets]">
                </div>
            </li>

        </ul>

    </div>
</div>