<?php
$unserialized_answers_array = collect();
$question_count = $questions->count();
//echo $question_count;
$count = 0;
foreach ($questions as $key => $singleQuestion) {
//    echo $count;
//    echo'<PRE>';print_R($singleQuestion->is_spouse );
    if (!(isset($directQuestions) && $directQuestions && $singleQuestion->is_modal)) {
        if ($singleQuestion->is_spouse == 1 && $tax_filing_status == config('constant.tax_filing_status_inverse.single')) {
            continue;
        }
        if ($question_count > 6 && ceil($question_count / 2) == $count && !in_array($topicId, [config('constant.questions.comprehensive_life_insurance_id'), config('constant.questions.comprehensive_spouse_life_insurance_id')])) {
            ?>
            </ul> 
            <ul class="questions-list single-column {{@$question_section_class}}">
                <?php
            }
            $symbols_type = '';
            if (!empty($singleQuestion->symbol_type)) {
                $symbols_type = 'data-symbols="' . $singleQuestion->symbol_type . '"';
            }
            ?>
            <li class="question-answer type-{{$singleQuestion->type}}" data-parent-id="{{ $singleQuestion->parent_id }}" data-parent-key="{{$singleQuestion->parent_answer_key}}" data-id="{{ $singleQuestion->id }}" data-input-type="{{$singleQuestion->type}}" data-parent-answer="{{@$singleQuestion->parent->answers[0]->answer }}" data-topic-id="{{@$topicId}}" data-topic-name="{{@$title}}" {!!$symbols_type!!}>
                <?php
                $answer = '';

//                if ($singleQuestion->type != 'heading' ||($singleQuestion->type == 'heading' && isset($blank_answer) && $blank_answer)) {
                if ($singleQuestion->type != 'heading') {
                    $answer_string = $answer = $singleQuestion->answers->count() > 0 ? $singleQuestion->answers[0]->answer : '';

                    $disabled = 'disabled';
                    if (isset($blank_answer) && $blank_answer) {
                        $answer = '';
                        $disabled = '';
                    }
                    if ($answer)
                        $decoded_answer = json_decode($answer, true);

                    if (isset($answerIndex) && is_numeric($answerIndex) && isset($decoded_answer) && isset($decoded_answer[$answerIndex]['value']) && empty($answerIndexCopy) && isset($answer)) {
                        $answer = json_decode($answer, true);
                        $answer = (isset($answer) && isset($answer[$answerIndex]) && isset($answer[$answerIndex]['value'])) ? $answer[$answerIndex]['value'] : '';
                    } elseif (isset($answerIndexCopy) && is_numeric($answerIndexCopy) && isset($decoded_answer) && isset($decoded_answer[$answerIndexCopy]) && !isset($answerIndex) && isset($answer)) {
                        $answer = json_decode($answer, true);
                        if (!($singleQuestion->type == 'checkbox' && !empty($singleQuestion->options))) {
                            $answer = (isset($answer) && isset($answer[$answerIndexCopy]) && isset($answer[$answerIndexCopy]['value'])) ? $answer[$answerIndexCopy]['value'] : '';
                        }
                    } elseif (isset($answerIndexCopy) && is_numeric($answerIndexCopy) && isset($decoded_answer) && isset($decoded_answer[$answerIndexCopy]) && isset($answerIndex) && is_numeric($answerIndex) && isset($answer)) {
                        $answer = json_decode($answer, true);
                        $answer = (isset($answer) && isset($answer[$answerIndexCopy][$answerIndex]) && $answer[$answerIndexCopy][$answerIndex]['value']) ? $answer[$answerIndexCopy][$answerIndex]['value'] : '';
                    }

                    $unserialized_answer = @unserialize($answer);
                    if ($singleQuestion->is_multiple && $blank_answer)
                        $unserialized_answer = true;
                    if ($singleQuestion->options == config('constant.questions.states')) {
                        $options = $states;
                    } else {
                        $options = json_decode($singleQuestion->options, true);
                    }
//echo'<PRE>';                                        print_R($unserialized_answer);

                    $inputType = $singleQuestion->type;
                    if ($unserialized_answer === false) {
                        if ($inputType != 'file') {
                            ?>
                            <div class="question-label col-sm-6" >
                                <p>{{$singleQuestion->label}}</p>
                            </div>
                            <div class="answer col-sm-6">
                                <?php
                                $inputClass = '';
                                if (in_array($singleQuestion->id, config('constant.questions.read_only_question_ids'))) {
                                    $inputClass = 'readOnly';
                                } elseif (in_array($singleQuestion->id, config('constant.questions.not_required_question_ids'))) {
                                    $inputClass = 'notRequired';
                                } elseif (in_array($singleQuestion->id, config('constant.questions.disabled_dropdown_question_ids'))) {
                                    $inputClass = 'disalbedDropdown';
                                } elseif (in_array($singleQuestion->id, config('constant.questions.spouse_name_autofill_question_ids'))) {
                                    $inputClass = 'spouseNameAutofill';
                                }
                                switch ($inputType) {
                                    case 'radio':
                                        if (isset($options) && !empty($options)) {
                                            foreach ($options as $option_key => $option_value) {
                                                $selected = '';
                                                if ($answer == $option_key)
                                                    $selected = 'checked="checked"';
                                                ?>
                                                <label class="radio-custom-label">
                                                    <input type="radio" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> class="question-{{ $singleQuestion->id }} {{$inputClass}}"  id="question-{{ $singleQuestion->id }}"   data-type="{{$inputType}}"  name="{{$singleQuestion->id}}" value="{{$option_key}}" {{$disabled}} {{$selected}}>
                                                    {{$option_value}}<span class="radio-icon"></span>
                                                </label>
                                                <?php
                                            }
                                        }
                                        break;
                                    case 'checkbox':

                                        if (empty($singleQuestion->options)) {
                                            $checked = '';
                                            if (!empty($answer) && $answer != 'unchecked') {
                                                $checked = 'checked="checked"';
                                                $answer = 'checked';
                                            } else {
                                                $answer = 'unchecked';
                                            }
//                                            echo"<PRE>";
//                                            print_r($answer);
                                            ?><input type="checkbox"class="question-{{ $singleQuestion->id }} {{$inputClass}}"  id="question-{{ $singleQuestion->id }}"   data-type="{{$inputType}}"  name="{{$singleQuestion->id}}" value="{{$answer}}" {{$disabled}} {{$checked}}><?php
                                        } else {
                                            $answer = json_decode($answer, true);

                                            $decoded_options = json_decode($singleQuestion->options, true);
                                            foreach ($decoded_options as $single_question_key => $single_question_value) {
                                                $checked = '';
                                                if (!empty($answer) && in_array($single_question_key, $answer)) {
                                                    $checked = 'checked="checked"';
                                                }
                                                ?>
                                                <label>
                                                    {{$single_question_value}}
                                                </label><input type="checkbox"   id="question-{{ $singleQuestion->id }}" class="question-{{ $singleQuestion->id }} {{$inputClass}}"   data-type="{{$inputType}}"  name="{{$singleQuestion->id}}" value="{{$single_question_key}}" {{$disabled}} {{$checked}}>
                                                <?php
                                            }
                                        }
                                        break;
                                    case 'date':
                                        if (isset($answer) && !empty($answer)) {
                                            $answer = date('d-m-Y', strtotime($answer));
                                        } else {
                                            $answer = date('d-m-Y');
                                        }

                                        $answer_decoded_text = json_decode($answer, true);
                                        if (is_string($answer) && !is_array($answer_decoded_text)) {
                                            ?>
                                            <input type="text" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> class="question_datepicker {{$inputClass}}"  id="question-{{ $singleQuestion->id }}" data-type="{{$inputType}}" name="{{$singleQuestion->id}}" value="{{$answer}}" {{$disabled}} >
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        break;
                                    case 'text':
$income_class='';
                                        $answer_decoded_text = json_decode($answer, true);
                                        if (is_string($answer) && !is_array($answer_decoded_text)) {
                                     if (in_array($singleQuestion->id, config('constant.questions.tax_bracket_question_ids'))) {
                                                $income_class = 'TaxBracket';
                                            }         ?>
                                            <div style="display: flex;">

                                                <input type="text" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> id="question-{{ $singleQuestion->id }}" data-type="{{$inputType}}" name="{{$singleQuestion->id}}" value="{{$answer}}" {{$disabled}} class="{{$inputClass.' '. $income_class}}">
                                                <?php if (!empty($singleQuestion->symbol_type) && count(explode(',', $singleQuestion->symbol_type)) == 1) {
                                                    ?>
                                                    <span class="placeholder-symbol">{{$singleQuestion->symbol_type}} </span>

                                                <?php }
                                                ?>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        break;
                                    case 'year':

                                        $answer_decoded_text = json_decode($answer, true);
                                        if (is_string($answer) && !is_array($answer_decoded_text)) {
                                            ?>
                                            <input type="year" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> id="question-{{ $singleQuestion->id }}" data-type="{{$inputType}}" name="{{$singleQuestion->id}}" value="{{$answer}}" {{$disabled}}  class="question_yearpicker {{$inputClass}}"> 
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        break;
                                    case 'number':
                                        $answer_decoded_text = json_decode($answer, true);
                                        if (is_string($answer) && !is_array($answer_decoded_text)) {
                                            $income_class = '';
                                            if (in_array($singleQuestion->id, config('constant.questions.household_income_question_ids'))) {
                                                $answer = $annual_income;
                                                $income_class = 'HouseHoldIncome';
                                            }
                                            if (in_array($singleQuestion->id, config('constant.questions.taxable_income_question_ids'))) {
                                                $income_class = 'TaxableIncome';
                                            }
                                          
                                            ?>
                                            <div style="display: flex;">
                                                <input type="number" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> id="question-{{ $singleQuestion->id }}" data-type="{{$inputType}}" name="{{$singleQuestion->id}}" value="{{$answer}}" {{$disabled}} class="{{$inputClass.' '. $income_class}}" >
                                <?php if (!empty($singleQuestion->symbol_type) && count(explode(',', $singleQuestion->symbol_type)) == 1) { ?>
                                                    <span class="placeholder-symbol">{{$singleQuestion->symbol_type}} </span>
                                                <?php }
                                                ?>
                                            </div>
                                                <?php
                                            }
                                            ?>
                                        <?php
                                        break;
                                    case 'textarea':
                                        $answer_decoded_text = json_decode($answer, true);
                                        if (is_string($answer) && !is_array($answer_decoded_text)) {
                                            ?>
                                            <textarea  rows="4" cols="27" <?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> id="question-{{ $singleQuestion->id }}" data-type="{{$inputType}}" name="{{$singleQuestion->id}}"  {{$disabled}} class="{{$inputClass}}">{{$answer}}</textarea>
                                            <?php
                                        }
                                        break;
                                    case 'select':

                                        if (isset($options[1]) && in_array($options[1], ['User Name', 'user_name', 'User name', 'user name']))
                                            $options[1] = $userName;
                                        ?>
                                        <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="position: absolute;right: 23px;z-index: 9;top: 6px;font-size: 16px;"></i>   
                                        <select class="required {{$inputClass}}"<?php echo($inputClass != 'notRequired') ? 'required="required"' : ''; ?> id="question-{{ $singleQuestion->id }}"   data-type="{{$inputType}}"  name="{{$singleQuestion->id}}" {{$disabled}}>
                            <!--<option hidden disabled selected value=""><span>Select a Value</span></option>-->

                            <?php
                            $option_count = 0;
                            if (isset($options) && !empty($options)) {
                                foreach ($options as $option_key => $option_value) {
                                    $selected = '';
                                    if (
                                            ($answer == $option_key && !in_array($singleQuestion->id, config('constant.questions.tax_filling_status_group_ids'))
                                            ) ||
                                            ($tax_filing_status == $option_key && in_array($singleQuestion->id, config('constant.questions.tax_filling_status_ids'))) ||
                                            (
                                            (
                                            ( in_array($tax_filing_status, [1, 2]) && $option_key == 1
                                            ) ||
                                            ( in_array($tax_filing_status, [3, 4, 5, 6]) && $option_key == 2
                                            )) && in_array($singleQuestion->id, config('constant.questions.tax_filling_status_group_ids'
                                                    )
                                            )

                                            )
                                    ) {
                                        $selected = 'selected="selected"';
                                    }
                                    if (in_array($option_value, ['spouse_name', 'Spouse Name', 'spouse/partner', 'Spouse/Partner']))
                                        $option_value = $spouse_name;

//                                                    if($blank_answer && $option_count==0)     $selected = 'selected="selected"';
                                    ?>
                                                    <option  value="{{$option_key}}"  {{$selected}}><span>{{$option_value}}</span></option>
                                                    <?php
                                                    $option_count++;
                                                }
                                            }
                                            ?></select><?php
                                            break;

                                        default:
                                            break;
                                    }
                                    ?>
                            </div> <?php
                            }else {
//                            echo'<pre>';print_r($answer);
                                ?>
                            <input type = "hidden" name = "{{$singleQuestion->id}}" id = "fileParentHidden" class = "fileParentHidden">
                            <?php
//                      echo'<pre>';      print_R($answer);die;
                            if (!empty($answer) && !is_array($answer)) {
                                $answers_decoded = json_decode($answer, true);
                                $files_array_keys = array_keys(array_column($files_question, 'parent_id'), $singleQuestion->id);
                                $files_required_questions = array_intersect_key($files_question, array_flip($files_array_keys));
                                $files_required_questions = array_values($files_required_questions);
//                                    echo'<pre>';print_R($files_required_questions);

                                $files_required_questions_key_select = array_keys(array_column($files_required_questions, 'type'), 'select');
//                                    print_R($files_required_questions_key_select);
                                $files_required_questions_value_select = array_intersect_key($files_required_questions, array_flip($files_required_questions_key_select));
                                $options_select = [];
                                if (isset($files_required_questions_value_select) && !empty($files_required_questions_value_select)) {

                                    foreach ($files_required_questions_value_select as $single_question_select) {
                                        if (in_array($single_question_select['name'], ['file_type', 'fileholder_name'])) {
//                                                echo'innn';
                                            $options_select = json_decode($single_question_select['options'], true);
                                        }
                                    }
                                }
                                ?>
                                <table class="service-files-table">
                                    <tr>
                                        <th>File Name</th>
                        <?php if (isset($options_select) && !empty($options_select)) {
                            ?>
                                            <th>Type</th>
                                            <?php
                                        }
                                        ?>
                                        <th class="text-center">Download</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                        <?php
                        if (isset($answers_decoded) && !empty($answers_decoded) && !isset($answers_decoded[0]['value'])) {

//                                    dd($key);
//                                        echo"<PRE>";
//                                        print_R($answers_decoded);die;
                            foreach ($answers_decoded as $key => $single_answer) {
                                $answerLabel = '';
                                if (isset($single_answer["fileholder_name"]) && !empty($single_answer["fileholder_name"])) {
                                    if ($single_answer["fileholder_name"] == 'user_name') {
                                        $answerLabel = '[FileHolder :' . $userName . ']';
                                    } else {
                                        $answerLabel = '[FileHolder :' . $single_answer["fileholder_name"] . ']';
                                    }
                                } elseif (isset($single_answer["file_type"]) && !empty($single_answer["file_type"])) {
                                    $answerLabel = '[Type :' . $single_answer["file_type"] . ']';
                                }
                                $answerLabel = @$single_answer["file_name"] . ' ' . $answerLabel;
                                ?>

                                            <tr class="fileUploadRow" data-row-index="{{$key}}">
                                                <td class="file-name-input"  data-key-name="file_name"><input type="text" value="{{@$single_answer["file_name"]}}"  disabled="disabled" ></td>
                                <?php if (isset($options_select) && !empty($options_select)) {
                                    ?>
                                                    <td  class="file-select-type" data-key-name="<?php echo (isset($single_answer["fileholder_name"]) && !empty($single_answer["fileholder_name"])) ? 'fileholder_name' : 'file_type'; ?>">
                                                        <select data-key="file_name" {{$disabled}} class="file-type-dropdown">
                                    <?php
                                    $options_data = '';
                                    foreach ($options_select as $key_select => $value_select) {
                                        $selected_file = '';
                                        if (@$single_answer["fileholder_name"] == $value_select) {
                                            $selected_file = 'selected="selected"';
                                        }
                                        if ($value_select == 'user_name') {
                                            $value_select = $userName;
                                        } if ($value_select == 'spouse_name') {
                                            $value_select = 'Spouse Name';
                                        }
                                        $options_data .= '<option value="' . $key_select . '" ' . $selected_file . '>' . $value_select . '</option>';
                                    }
                                    echo $options_data;
                                    ?>
                                                        </select>
                                                    </td>                                    
                                <?php }
                                ?>

                                                <td class="text-center file-download"><a download="{{@$single_answer["file_path"]}}" href="files/{{$clientId}}/{{@$single_answer["file_path"]}}"> <i class="fa fa-download"></i></a></td>
                                                <td    class="text-center file-action">
                                                    <i title="Delete" class="fa fa-trash  cursor-pointer delete-file" ></i>
                                                </td>
                                            </tr>
                                <?php
                            }
                            ?>


                            <?php
                        }
                        ?>
                                </table>
                                <div class="col-sm-6 add-file-btn mt-20">
                                    <label for="files" class="btn btn-primary open_button" style="background:none;">Add File</label>
                                    <!--<label for="files" class="btn btn-primary uploaderBtn no-action" style="background:none; display: none">Add File</label>-->
                                </div>
                        <?php
                    } else {
                        ?>
                                <h4>Documents</h4>
                                <ul style="padding-left: 2px;"> 
                                    <li class="upload-statement">
                                        <input type="file" name="statement_file"  class="modal-upload-file-input" />
                                        <label class="upload-statement-button hide" style="width: auto !important; font-size:13px; padding: 3px 20px; border-radius: 4px;"> 
                                            <span style="font-size: 0"></span>Add File
                                        </label>
                        <?php // if (!empty($answer)) {
                        ?>
                                        <table class="uploaded-modal-file">
                                            <tr>
                                                <td class="modal-file-name">
                                                    <span>{{@$answer["file_name"] }} </span>
                                                </td>
                                                <td class="text-center hide" >
                                                    <a download="{{@$answer["file_path"]}}" href="files/{{$clientId}}/{{@$answer["file_path"]}}"> <i class="fa fa-download "></i></a>
                                                </td>
                                                <td class="text-center hide">
                                                    <i title="Delete" class="fa fa-trash cursor-pointer delete-modal-file"></i>
                                                </td>
                                            </tr>
                                        </table>                             

                        <?php // }
                        ?>

                                    </li>
                                </ul>
                        <?php
                    }
                }
                ?>
                        <?php
                    } else {
                        //To show inputs for the blank 
                        if (empty($unserialized_answer))
                            $unserialized_answer = true;
                        if ((isset($singleQuestion) && isset($singleQuestion->answers) && isset($singleQuestion->answers[0]))) {
                            $singleQuestion->answers[0]->answer = $unserialized_answer;
                        }


                        $unserialized_answers_array[] = $singleQuestion;
                        ?><?php
                    }
                }
                ?>
                <?php
//            $firstChildQuestion = (empty($singleQuestion->parent_id) || (!empty($singleQuestion->parent_id) && $singleQuestion->type == 'heading') && $singleQuestion->firstChild) ? $singleQuestion->firstChild : '';
                $firstChildQuestion = ( isset($firstLevelQuestions) && !empty($firstLevelQuestions) && $singleQuestion->firstChild) ? $singleQuestion->firstChild : '';


//               

                if (isset($firstChildQuestion) && !empty($firstChildQuestion) && ($firstChildQuestion->parent_answer_key == $answer) && !empty($firstChildQuestion->is_modal)) {
//                    echo"<PRE>";
//                    print_r($firstChildQuestion->toArray());
//                die;
                    $first_child_answers = $firstChildQuestion->answers->count() > 0 ? $firstChildQuestion->answers[0]->answer : '';
                    $first_child_answers_decoded = json_decode($first_child_answers, true);
                    if (empty($first_child_answers_decoded)) {
                        $first_child_answers_decoded = $first_child_answers;
                    }


//         echo"<pre>";
//         echo'jkdsa';
//         print_R($first_child_answers_decoded);
                    if (isset($topic_is_copy) && $topic_is_copy && isset($answerIndexCopy) && is_numeric($answerIndexCopy) && isset($first_child_answers_decoded[$answerIndexCopy]) && is_array($first_child_answers_decoded[$answerIndexCopy])) {
                        $first_child_answers_decoded = isset($first_child_answers_decoded[$answerIndexCopy]) ? $first_child_answers_decoded[$answerIndexCopy] : '';
                    }
//                    print_r($first_child_answers_decoded);
                    $first_child_answers_decoded_values = (is_array($first_child_answers_decoded)) ? array_column($first_child_answers_decoded, 'value') : '';

//                    print_R(array_column($first_child_answers_decoded, 'value'));
//                    print_r($first_child_answers_decoded_values);
                    if (empty($first_child_answers_decoded_values) && !is_array($first_child_answers_decoded)) {
                        $first_child_answers_decoded_values = $first_child_answers_decoded;
                    }

//                    if (isset($first_child_answers_decoded) && !empty($first_child_answers_decoded)) {
                    ?>
                    <table class="find-table-length modal-table">
                        <thead class="table-header-border">
                            <tr> 
                                <td colspan="2">{{$singleQuestion->label}} </td>
            <?php
            $data_questions_tab_copy_index_data = '';
            if ((!in_array($topicId, config('constant.questions.topic_ids_to_skip_add_icon')) || (in_array($topicId, config('constant.questions.topic_ids_to_skip_add_icon')) && empty($first_child_answers_decoded_values)))) {
                if ($blank_answer) {
                    $data_questions_tab_copy_index = $answerIndexCopy . '_' . config('constant.questions.new');
                    $data_questions_tab_copy_index_data = 'data-previous-slide-copy-index="' . $data_questions_tab_copy_index . '"';
                }
                ?>
                                    <td  data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}"  data-question-id="{{$singleQuestion->id}} "  data-index="<?php echo (!empty($first_child_answers_decoded_values)) ? count($first_child_answers_decoded_values) . '_' . config('constant.questions.new') : '0_' . config('constant.questions.new'); ?>" data-copy-index="{{$answerIndexCopy}}" {!!$data_questions_tab_copy_index_data!!} data-display-type="{{$displayType}}"> <i class="fa fa-plus-circle cursor-pointer showModalQuestions addNewModal"></i> </td>
                                <?php } ?>
                            </tr>
                        </thead> <tbody>

            <?php
            if (isset($first_child_answers_decoded) && !empty($first_child_answers_decoded)) {
                if (isset($first_child_answers_decoded_values) && !empty($first_child_answers_decoded_values)) {
                    if (is_array($first_child_answers_decoded_values)) {
                        foreach ($first_child_answers_decoded_values as $child_answer_key => $child_answer_value) {
                            if ($firstChildQuestion->type == 'select') {
                                $optionsOfFirstQuestion = json_decode($firstChildQuestion->options, true);
                                if (in_array($child_answer_value, array_flip($optionsOfFirstQuestion))) {
                                    $child_answer_value = $optionsOfFirstQuestion[$child_answer_value];
                                }
                            }
                            ?>
                                            <tr class="table-border  cursor-pointer"  data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}" data-index="{{$child_answer_key}}" data-question-id="{{$singleQuestion->id}}" data-copy-index="{{$answerIndexCopy}}" {{$data_questions_tab_copy_index_data}} data-display-type="{{$displayType}}">
                                                <td class="showModalQuestions">{{$child_answer_value}}</td>
                                                <td  class="deleteModal text-center">
                                                    <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i>
                                                </td>
                                                <td class="showModalQuestions right-arrow" valign="top" >
                                                    <i class="fa fa-angle-right " aria-hidden="true"></i>
                                                    <!--<i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>-->
                                                </td>
                                            </tr>
                            <?php
                        }
                    } else {
                        ?>

                                        <tr data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}"  data-question-id="{{$singleQuestion->id}} "  data-index="0"   class="table-border  cursor-pointer">
                                            <td class="showModalQuestions">{{$first_child_answers_decoded_values}}</td>
                                            <td valign="top" class="deleteModal text-center">
                                                <i title="Delete" class="fa fa-trash cursor-pointer " aria-hidden="true"></i>
                                            </td>
                                            <td class="showModalQuestions right-arrow"valign="top" >
                                                <i class="fa fa-angle-right " aria-hidden="true"></i>
                                                <!--<i title="Delete" class="fa fa-trash"  aria-hidden="true"></i>-->
                                            </td>
                                        </tr><?php
                    }
                }
            }
            ?>


                        </tbody> 
                    </table>


            <?php
        }
        ?>
            </li><?php
            }
            $count++;
        }
        $thead_data = '';
//    echo"<PRE>";
//    print_R($unserialized_answers_array->toArray());die;
        if (isset($unserialized_answers_array) && !empty($unserialized_answers_array) && $unserialized_answers_array->count() > 0) {
            ?> <li class="question-answer" data-parent-id="{{ $unserialized_answers_array[0]->parent_id }}" data-parent-key="{{$unserialized_answers_array[0]->parent_answer_key}}">
            <table class="contingent-beneficiary-table">
                <thead>

                    <tr>
    <?php
    foreach ($unserialized_answers_array as $key => $single_question) {
        $thead_data .= '<th>' . $single_question->label . '</th>';
    }
    ?>        {!!$thead_data!!}


                    </tr>
                </thead>
                <tbody>
    <?php
    $answer_count_unserialized = (isset($unserialized_answers_array[0]) && isset($unserialized_answers_array[0]->answers) && isset($unserialized_answers_array[0]->answers[0])) ? count($unserialized_answers_array[0]->answers[0]->answer) : 1;
    ;
    if (empty($answer_count_unserialized))
        $answer_count_unserialized = 1;
//    dd($answer_count_unserialized);
    if (isset($answer_count_unserialized) && !empty($answer_count_unserialized)) {
        for ($i = 0; $i < $answer_count_unserialized; $i++) {
            ?>
                            <tr>
                            <?php
                            $question_ids = [];
                            foreach ($unserialized_answers_array as $key => $single_question) {
//                                    echo'<PRE>';                                    print_r($single_question->toArray());
                                $question_ids[] = $single_question->id;
                                if ($single_question->type == 'checkbox') {
                                    $checked = '';
                                    if ($single_question->answers->count() > 0 && $single_question->answers[0]->answer[$i] == 'checked') {
                                        $checked = 'checked="checked"';
                                    }
                                    ?>
                                        <td>   <input type="checkbox" {{$checked}} value="1" name="{{$single_question->id}}[]" {{$disabled}} <?php echo (!in_array($single_question->id, config('constant.questions.not_required_question_ids'))) ? 'required="required"' : ''; ?> class="{{$inputClass}}">  </td>
                                        <?php
                                    } else {
                                        ?>
                                        <td>
                                            <input type="{{$single_question->type}}"  value="{{@$single_question->answers[0]->answer[$i]}}" name="{{$single_question->id}}[]" {{$disabled}} class="<?php echo ($single_question->type == 'year') ? 'question_yearpicker' : ''; ?>" <?php echo (!in_array($single_question->id, config('constant.questions.not_required_question_ids'))) ? 'required="required"' : ''; ?>> 
                                        </td>


                <?php }
                ?>

                                <?php } ?>
                                <td valign="top" class="deleteRow " data-index="{{$i}}" data-question-ids="<?php echo implode(',', $question_ids) ?>">
                                    <i title="Delete" class="fa fa-trash hide cursor-pointer " aria-hidden="true"></i>
                                </td>
                            </tr><?php
                }
            }
                        ?>
                </tbody>

            </table>
            <div class="addMoreRow add-more-row mt-20 ">
                <label  class="btn btn-primary hide" style="background:none;">Add More</label>
            </div>
        </li>
    <?php
}
if ($is_modal_count == count($questions) && isset($directQuestions) && $directQuestions) {
    $singleQuestion = $questions[0];
    ?><table class="find-table-length modal-table">
            <thead class="table-header-border">
                <tr> 
                    <td colspan="2">Modals List </td>
                </tr>
            </thead> <tbody>
    <?php
    $first_child_answers = $singleQuestion->answers->count() > 0 ? $singleQuestion->answers[0]->answer : '';

    $first_child_answers_decoded = json_decode($first_child_answers, true);
    $first_child_answers_decoded_values = (is_array($first_child_answers_decoded)) ? array_column($first_child_answers_decoded, 'value') : '';
    if (isset($first_child_answers_decoded_values) && !empty($first_child_answers_decoded_values)) {
        foreach ($first_child_answers_decoded_values as $child_answer_key => $child_answer_value) {
            ?>
                        <tr data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}" data-index="{{$child_answer_key}}" class="table-border  cursor-pointer">
                            <td   class="showModalQuestions">{{$child_answer_value}}</td>

                            <td valign="top" class="deleteModal text-center">
                                <i title="Delete" class="fa fa-trash cursor-pointer" aria-hidden="true"></i>
                            </td>
                            <td  class="showModalQuestions right-arrow" valign="top">
                                <i class="fa fa-angle-right " aria-hidden="true"></i>
                            </td>
                        </tr>


            <?php
        }
    }
    ?>

            </tbody> </table>
        <div  data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}"   data-index="<?php echo count($first_child_answers_decoded_values) . '_' . config('constant.questions.new') ?>" class="table-border  cursor-pointer">
            <div class="showModalQuestions">
                <div class="add-file-btn mt-20">
                    <label for="files" class="btn btn-primary" style="background:none;">Add More</label>
                </div>
            </div>


        </div>
    <?php
}
?>
    <button type="submit" class="btn-success validation-button hide" style="height:0px"></button>
    <script>
        $(document).ready(function () {
            var $blank_answer = '<?php echo (isset($blank_answer) && !empty($blank_answer)) ? $blank_answer : ''; ?>';
            if ($blank_answer) {
                $('.service-preview').find('input[type="text"] , input[type="number"] , input[type="radio"] , input[type="checkbox"], select , textarea')
                        .attr('disabled', false)
                        .css({"border-bottom": "1px solid #ccc", "background": "#fff"});
                $('.service-preview').find('.upload-statement .upload-statement-button').removeClass('hide');
            }
        });

    </script>
