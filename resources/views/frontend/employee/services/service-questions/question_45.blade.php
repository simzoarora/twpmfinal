<div class="topic-heading" >
    <i class="fa fa-angle-left pull-left back-to-topics"></i>
    <h3>Personal Information
        <i class="fa fa-angle-right pull-right"></i>
    </h3>
</div>
<div class="abc col-sm-6" >
    <div class="question-list-wrapper">
        <ul class="questions-list">
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Gross income</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" name="answer[45][household_gross_income]" value="{{(!empty($defaultData) && array_key_exists('housesholdIncome',$defaultData)) ? $defaultData["housesholdIncome"] :null}}">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Marital status</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="single" name="answer[45][marital_status]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Your gross income</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="single" name="answer[45][your_gross_income]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Your spouse/partner's gross income</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="single" name="answer[45][spouse_partner_gross_income]">
                </div>
            </li>
        </ul>
    </div>
</div>  