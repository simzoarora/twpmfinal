<div class="service-preview sub-topic-section serviceSubTopics " style="left: 200%;">
<div class="sub-topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Documents</h3>
        </div>
<?php if (isset($files_answers) && !empty($files_answers)) {
    ?>
    <table class="view-documents-table col-sm-5">
           <thead>
            <tr><th> File Name</th><th>Download</th></tr>
        </thead>

        <tbody>
            <?php
            $final_files_array = [];
            foreach ($files_answers as $single_file) {
                $single_answer = $single_file['answer'];
                $single_answer_decoded = json_decode($single_answer);
                if (isset($single_answer_decoded) && !empty($single_answer_decoded)) {
                    foreach ($single_answer_decoded as $single_answer_obj) {
                        if (isset($single_answer_obj->value) && !empty($single_answer_obj->value)) {
                            $final_files_array[] = (array)$single_answer_obj->value;
                        } elseif (!isset($single_answer_obj->value)) {
                            $final_files_array[] = (array)$single_answer_obj;
                        }
                    }
                } 
            } 
            if (isset($final_files_array) && !empty($final_files_array)) {
                foreach ($final_files_array as $singleFile) {
         if (isset($singleFile) && !empty($singleFile) && isset($singleFile['file_name']) && !empty($singleFile['file_name']) && isset($singleFile['file_path']) && !empty($singleFile['file_path'])) {
                        ?>
             
            <tr><td>{{$singleFile['file_name']}}</td><td class="text-center file-download"><a download="{{$singleFile["file_path"]}}" href="files/{{$clientId}}/{{$singleFile["file_path"]}}"> <i class="fa fa-download"></i></a></td></tr>
                            <?php
                    }
                }
            }
            ?>
           
            <?php ?>

        </tbody>
    </table>
<?php }
?>
 </div>