<?php
if (isset($questions) && !empty($questions) && $questions->count() > 0) {


    $is_modal_count = 0;
    foreach ($questions as $key => $singleQuestion) {
        if ($singleQuestion->is_modal)
            $is_modal_count++;
    }
    $class = '';

    $serviceTitleElement = '<div class=" topic-section">
                <h3>' . $serviceTitle . '</h3></div>
';
    if (empty($directDependentQuestions)) {
        $class = 'question-section';
        $serviceTitleElement = '';
    }
    ?> 
    <div class="service-preview {{$class}} service-modal-questions serviceModalQuestions" style="left: 200%;">
        {!!$serviceTitleElement!!}
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-questions cursor-pointer"></i>
            <h3>{{$title}} 
            </h3>
            <?php
            // echo $blank_answer;die('e');
//            $isUser = strstr($_SERVER['HTTP_REFERER'], 'recommendedServices');
//            if (!$isUser) {
                $style = '';
                $style_pencil = 'style="display:none;"';
                if (!$blank_answer) {
                    $style = 'style="display:none;"';
                    $style_pencil = '';
                }
                ?>
                <i title="Edit" class="fa fa-pencil active-inputs cursor-pointer" title="Edit" <?php echo $style_pencil; ?>></i>

                <i class="fa fa-check save-new-modal-answers updateAnswers cursor-pointer" title="Save"  data-class="save-new-modal-answers" aria-hidden="true" <?php echo $style; ?> data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}" data-index="{{@$answerIndex}}"  data-copy-index="{{@$answerIndexCopy}}" data-topic-is-child="<?php echo (isset($topic_is_child) && !empty($topic_is_child)) ? 1 : 0 ?>" data-previous-slide-copy-index="{{@$data_previous_slide_copy_index}}" data-display-type="{{$displayType}}"></i>   
                <?php
//            }
            ?>

        </div>
        <div class="abc col-md-12 " >
            <div class="question-list-wrapper">
                <form id="updateModalAnswerForm">   
                    <ul class="questions-list single-column">
                        @include('frontend.employee.services.service-questions.viewQuestionsTemplate', ['questions' => $questions,'question_modal_id'=>@$question_modal_id,'is_modal'=>true,'answerIndex'=>$answerIndex,'is_modal_count'=>$is_modal_count])
                    </ul>
                </form>
            </div>
        </div>
    </div>
    <?php
}
?>
<style>
    .answer select{
        max-width: -webkit-fill-available;  
        max-width: -moz-available;
        max-width: stretch;
    }
</style>  