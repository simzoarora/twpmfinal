 <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Income & Investments
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Total income from all sources">Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][household_gross_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[42][tax_filling_status]" readonly="">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p title="Ask your tax advisor or leave blank and we will estimate based upon your Estimated taxable income">Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][estimated_taxable_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Non retirement investment</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][non_retirement_investment_assets]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Retirement investment</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][retirement]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>