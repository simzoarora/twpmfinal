<div class="topic-heading" >
    <i class="fa fa-angle-left pull-left back-to-topics"></i> 
    <h3>Your Liabilities
        <i class="fa fa-angle-right pull-right"></i>
    </h3>
</div>
<div class=" col-sm-6" >
    <div class="question-list-wrapper">
        <ul class="questions-list">
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p for="label">Estimated mortgages.</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[33][estimated_total_mortgages]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Estimated other debts</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[33][estimated_total_other_debts]">
                </div>
            </li>
        </ul>
    </div>
</div>