<?php if (isset($subTopics) && !empty($subTopics) && $subTopics->count() > 0) { ?>

    <div class="service-preview sub-topic-section serviceSubTopics " style="left: 200%;">
        <div class="sub-topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>{{$topicName}}
            </h3>
        </div>

        <div class="col-sm-12 semi-annual-portfolio-hide-sub-topics sub-topic-list-wrapper serviceSubTopicsWrapper">
            <ul class="col-sm-6 sub-topic-list">

                <?php
                $sub_topics_count = $subTopics->count();
                foreach ($subTopics as $key => $singleSubTopic) {
                    if ($sub_topics_count > 6 && ceil($sub_topics_count / 2) == $key) {
                        ?></ul>  <ul class="col-sm-6 sub-topic-list">
                        <?php
                    }
                    ?>
                    <li class="single-sub-topic " data-topic="{{$singleSubTopic->id}}" data-topic-name="{{$singleSubTopic->name}}" data-copy-index="<?php echo (!empty($singleSubTopic->is_copy) && (!in_array($singleSubTopic->id, [config('constant.questions.comprehensive_life_insurance_id'), config('constant.questions.comprehensive_spouse_life_insurance_id')])) ) ? '0' : ''; ?>">
                        {{$singleSubTopic->name}}

                        <i class="fa fa-angle-right pull-right"></i>
                    </li>                       <?php }
                ?>

            </ul>
        </div>
    </div>


<?php }
?>




