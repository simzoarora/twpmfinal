<?php
$question_section = 'question-section';
$is_modal_count = 0;
if (isset($questions) && !empty($questions)) {
    foreach ($questions as $key => $singleQuestion) {
        if ($singleQuestion->is_modal)
            $is_modal_count++;
    } 
} 
if ((isset($directQuestions) && !empty($directQuestions)) || (isset($directTopicQuestions) && !empty($directTopicQuestions))) {
    $question_section = '';
    ?>
<?php }
?>    

<div class="service-preview {{$question_section}} serviceQuestions" style="left: 200%;">
    <div class="<?php echo ($directQuestions) ? 'topic-section pre-topic-questions ' : 'topic-heading'; ?>" >
        <?php
        if (empty($directQuestions)) {
            ?>
            <i class="fa fa-angle-left pull-left back-to-sub-topics"></i>
        <?php } ?>
        <h3>{{$title}}
        </h3>
        <?php 
//                $isUser=strstr($_SERVER['HTTP_REFERER'],'recommendedServices');

        if ((empty($directQuestions) || ($is_modal_count != count($questions) && isset($directQuestions) && $directQuestions)) ) {
            
             $style = '';
                $style_pencil = 'style="display:none;"';
                if (!$blank_answer) {
                    $style = 'style="display:none;"';
                    $style_pencil = '';
                }?>
            <i title="Edit" class="fa fa-pencil active-inputs cursor-pointer" title="Edit" <?php echo $style_pencil; ?>></i>
            <i class="fa fa-check save-new-answers updateAnswers cursor-pointer" title="Save" data-class="save-new-answers" aria-hidden="true" <?php echo $style; ?> data-topic-id="{{$topicId}}" data-service-id="{{$serviceId}}" data-index="{{@$answerIndex}}"  data-copy-index="{{@$answerIndexCopy}}"></i>
        <?php } ?>
    </div>
    <div class="abc col-md-12 question-list-wrapper" >
        <?php
        //Personal, Group tabs for Disability Insurance and 88,94 ids for first tab with no questions of life insurance
        if (in_array($topicId, [config('constant.questions.comprehensive_disablity_insurance_id'), config('constant.questions.comprehensive_spouse_disablity_insurance_id'), config('constant.questions.comprehensive_life_insurance_id'), config('constant.questions.comprehensive_spouse_life_insurance_id')])) {
            ?>
            <div id="main-tabset" class="main-tabset container">	
                <ul class="nav nav-pills">

                    <?php if (in_array($topicId, [config('constant.questions.comprehensive_disablity_insurance_id'), config('constant.questions.comprehensive_spouse_disablity_insurance_id')])) {
                        ?>
                        <!--Disability Insurance-->
                        <li  data-display-type="{{config('constant.questions.general')}}" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-copy-index="0" class=" single-sub-topic main-tabset-item active"><a href="javascript:;">Group</a></li>
                        <li  data-display-type="{{config('constant.questions.personal')}}" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-copy-index="0"  class="single-sub-topic main-tabset-item"><a href="javascript:;">Personal</a></li>

                        <?php
                    } else {
                        ?>
                        <!--life insurance-->
                        <li  data-display-type="{{config('constant.questions.general_life')}}" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-copy-index="" class=" single-sub-topic main-tabset-item active"><a href="javascript:;">Group</a></li>
                        <li  data-display-type="{{config('constant.questions.general')}}" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-copy-index="0"  class="single-sub-topic main-tabset-item"><a href="javascript:;">Other</a></li>
                    <?php }
                    ?>
                </ul>
            </div><?php
        }
        $question_section_class = '';
        if (isset($topic_is_copy) && !empty($topic_is_copy) && isset($answerIndexCopy) && $questions->count() > 1) {
            $key_to_be_checked = '';
            foreach ($questions as $question_key => $question_value) {
                if (isset($question_value) && !empty($question_value) && isset($question_value->answers) && isset($question_value->answers[0]) && isset($question_value->answers[0]->answer) && !empty($question_value->answers[0]->answer)) {
                    $decoded_single_answer = json_decode($question_value->answers[0]->answer, true);
                    if (isset($decoded_single_answer) && !empty($decoded_single_answer) && is_array($decoded_single_answer)) {
                        $key_to_be_checked = $question_key;
                        break;
                    }
                }
            }
//            echo($key_to_be_checked);
//            echo'<PRE>';print_r($questions[$key_to_be_checked]->toArray());
            if (is_numeric($key_to_be_checked)) {
                $second_child_answer = (
                        isset($questions[$key_to_be_checked]->answers[0]) && isset($questions[$key_to_be_checked]->answers[0]->answer)
//                        && isset($questions[$key_to_be_checked]->parent)
//                        && isset($questions[$key_to_be_checked]->parent->answers[0]) 
//                        && isset($questions[$key_to_be_checked]->parent->answers[0]->answer)
//                        &&($questions[$key_to_be_checked]->parent->answers[0]->answer==$questions[$key_to_be_checked]->parent_answer_key)
                        ) ? $questions[$key_to_be_checked]->answers[0]->answer : '';
                $second_child_answer_decoded = json_decode($second_child_answer, true);
//              echo $key_to_be_checked;
//                echo'<PRE>';
//                print_r($questions[$key_to_be_checked]->toArray());
//                print_R($second_child_answer_decoded);
//                die;
                $count_answers_decoded = count($second_child_answer_decoded);
                if (isset($count_answers_decoded) && !empty($count_answers_decoded)) {
                    $question_section_class = 'tabs-section-border';
                    ?> 
                    <div id="exTab1" class="container">	
                        <ul  class="nav nav-pills">
                            <?php
                            foreach ($second_child_answer_decoded as $single_key => $single_value) {
                                $class = '';

                                if ($single_key == 0 && $count_answers_decoded!=$answerIndexCopy) {
                                    $class = 'selected-tab';
                                }
                                ?>
                                <li class="single-sub-topic copy-tabs {{$class}}" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-display-type="{{$displayType}}" data-copy-index="{{$single_key}}"><a href="javascript:;">Plan {{$single_key+1}}</a></li>
                                <?php
                            }
//                            if (!$isUser) { ?>
                                <li class="single-sub-topic copy-tabs <?php echo ($count_answers_decoded==$answerIndexCopy) ? 'selected-tab':'';?>" data-topic="{{$topicId}}" data-topic-name="{{$title}}" data-display-type="{{$displayType}}" data-copy-index="<?php echo $count_answers_decoded . '_' . config('constant.questions.new') ?>"><a href="javascript:;"> <i class="fa fa-plus-circle cursor-pointer"></i> Plan {{$count_answers_decoded+1}}</a></li>
                            <?php // } ?>
                        </ul>
                    </div>
                    <?php
                }
            }
        }
        ?>
        <form id="updateAnswerForm" >

            <ul class="questions-list single-column {{$question_section_class}}">
                <?php
                if (isset($questions) && !empty($questions) && $questions->count() > 0) {
                    ?>
                    @include('frontend.employee.services.service-questions.viewQuestionsTemplate', ['questions' => $questions,'question_modal_id'=>@$question_modal_id,'is_modal_count'=>$is_modal_count])
                    <?php
                }
                ?>
            </ul>
        </form>
    </div>
</div>
<style>
    .answer select{
        max-width: -webkit-fill-available;  
        max-width: -moz-available;
        max-width: stretch;
    }
</style>  




