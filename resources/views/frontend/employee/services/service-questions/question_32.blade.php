<div class="topic-heading" >
    <i class="fa fa-angle-left pull-left back-to-topics"></i> 
    <h3>Your Family
        <i class="fa fa-angle-right pull-right"></i>
    </h3>
</div>
<div class=" col-sm-6" >
    <div class="question-list-wrapper">
        <ul class="questions-list">
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p for="label">Your age</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[32][age]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Health conditions</p>
                </div>
                <div class="answer col-sm-6" >
                    
                       <label class="radio-custom-label">
                        <input class="" required="required" name="answer[32][serious_health_conditions]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="" required="required" name="answer[32][serious_health_conditions]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>
                    
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Amount of Group Life insurance </p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[32][amount_of_group_life_insurance]">
                </div>
            </li>



            <!--spouse section--> 

            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p for="label">(spouse first name) age</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[322][spouse_partner_age]">
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>(spouse first name) health conditions</p>
                </div>
                <div class="answer col-sm-6" >
                    
                      <label class="radio-custom-label">
                        <input class="defined-contribution-retirement" required="required" name="answer[322][spouse_have_serious_health_conditions]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="defined-contribution-retirement" required="required" name="answer[322][spouse_have_serious_health_conditions]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>
                    
                </div>
            </li>
            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Amount of Group Life insurance for (spouse first name)</p>
                </div>
                <div class="answer col-sm-6" >
                    <input type="text" value="dummy" name="answer[322][amount_group_life_insurance_spouse]">
                </div>
            </li>
            <!--spouse section end--> 



            <li class="question-answer">
                <div class="question-label col-sm-6" >
                    <p>Do you have children</p>
                </div>
                <div class="answer col-sm-6" >

                    <label class="radio-custom-label">
                        <input class="children-confirmation table-confirmation" required="required" name="answer[321][have_children]" type="radio" value="yes" aria-required="true">Yes
                        <span class="radio-icon"></span>
                    </label>
                    <label class="radio-custom-label">
                        <input class="children-confirmation table-confirmation" required="required" name="answer[321][have_children]" type="radio" value="no" aria-required="true">No
                        <span class="radio-icon"></span>
                    </label>


                    <i class="fa  fa-plus-circle pull-right show-children-modal"></i>
                    <!--<input type="text" value="dummy" name="answer[321][have_children]">-->
                </div>
            </li>
        </ul>
    </div> 
</div>