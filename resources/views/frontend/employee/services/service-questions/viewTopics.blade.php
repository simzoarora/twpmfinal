<?php
if (isset($topics) && !empty($topics) && $topics->count() > 0) {
    $is_modal_count = 0;
    if (isset($preTopicQuestions) && !empty($preTopicQuestions)) {

        foreach ($preTopicQuestions as $key => $singleQuestion) {
            if ($singleQuestion->is_modal)
                $is_modal_count++;
        }
    }
    ?>
    <div class="service-preview serviceTopics" >
        <div class=" topic-section">
            <div class="pre-topic-questions">
                <h3>{{$serviceTitle}}</h3>
                <?php
//                $isUser = strstr($_SERVER['HTTP_REFERER'], 'recommendedServices');
                if (isset($preTopicQuestions) && !empty($preTopicQuestions) && $preTopicQuestions->count() > 0) {
                    ?><i title="Edit" class="fa fa-pencil active-inputs cursor-pointer" title="Edit" cursor-pointer></i>
                    <i class="fa fa-check save-new-topic-answers updateAnswers cursor-pointer " title="Save" data-class="save-new-topic-answers" aria-hidden="true" style="display:none" data-topic-id="{{@$topicId}}" data-service-id="{{$serviceId}}" data-index="{{@$answerIndex}}"  data-copy-index="{{@$answerIndexCopy}}"></i>
                <?php } ?>
            </div>
            <?php
            if (isset($preTopicQuestions) && !empty($preTopicQuestions) && $preTopicQuestions->count() > 0) {
                ?>
                <div class="abc col-sm-12 col-md-12 question-list-wrapper preTopicQuestions"  >
                    <form id="updateTopicAnswerForm">
                        <ul class="questions-list single-column">
                            @include('frontend.employee.services.service-questions.viewQuestionsTemplate', ['questions' => $preTopicQuestions,'question_modal_id'=>@$question_modal_id,'is_modal_count'=>$is_modal_count])
                        </ul>
                    </form>
                </div>
                <?php
            }
            ?>
            <div class = "col-sm-12 annual-portfolio-hide-topics topic-list-wrapper serviceTopicsWrapper">
                <ul class = "single-column topic-list">
                    <?php
                    $topics_count = $topics->count();
                    foreach ($topics as $key => $singleTopic) {
                        if (empty($singleTopic->is_spouse) || (!empty($singleTopic->is_spouse) && $tax_filing_status != config('constant.tax_filing_status_inverse.single'))) {
                            if ($topics_count > 6 && ceil($topics_count / 2) == $key) {
                                ?></ul>   <ul class = "single-column topic-list">
                                <?php
                            }
                            ?> 
                            <li class="single-topic annual-portfolio-topic-{{$key+1}}" data-topic="{{$singleTopic->id}}" data-topic-name="{{$singleTopic->name}}">
                                {{$singleTopic->name}}
                                <i class="fa fa-angle-right pull-right"></i>
                            </li>
                            <?php
                        }
                    }
                    if (isset($documentsTab) && !empty($documentsTab)) {
                        ?>

                        <li class="single-topic " data-documents="true">
                            Documents
                            <i class="fa fa-angle-right pull-right"></i>
                        </li>
                    </ul>
                <?php } ?>
            </div>
        </div>
    </div>
<?php }
?>




