COMPREHENSIVE PLANNING AND MANAGEMENT

<script>

    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    });

</script>