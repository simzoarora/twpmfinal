<li>Clients</li>
<?php if (!$all_clients->isEmpty()) { ?>
    @php
        $i= 0;
    @endphp
    
    @foreach ($all_clients as $client)
    @if (isset($client->Profile))
    @php
        $i++;
    @endphp
    
    @if($i == 1)
        <li class="client-item active" data-id="{{ $client->id }}">
            {{ $client->Profile->first_name . ' ' . $client->Profile->middle_name . ' ' . $client->Profile->last_name }}
        </li>
    @else  
        <li class="client-item" data-id="{{ $client->id }}">
            {{ $client->Profile->first_name . ' ' . $client->Profile->middle_name . ' ' . $client->Profile->last_name }}
        </li>
    @endif    
    
    @elseif ($client->name)
    <li class="client-item" data-id="{{ $client->id }}"> {{ $client->name }}</li> 
    @endif
    @endforeach
<?php } else { ?>
    <li>No clients found.</li>
<?php } ?>