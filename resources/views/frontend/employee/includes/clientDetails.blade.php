<div class="col-sm-12 user-details">
    @if (isset($client->Profile))
    <h2>
        {{ $client->Profile->first_name . ' ' . $client->Profile->middle_name . ' ' . $client->Profile->last_name }}
        <!--<h4>{{$employeeName}}</h4>-->
    </h2> 
    @elseif ($client->name)
    <h2> {{ $client->name }}</h2>
    <!--<h6>{{$employeeName}}</h6>-->
    @endif
</div>
<div class="profile-nav employee-profile-nav">
    <ul class="list-inline">
        <li class="active"><a href="#profile-inner">Profile</a></li>
        <li><a href="#documents-inner">Documents</a></li>
        <li><a href="#services-inner">Services</a></li>
        <li><a href="#managed-account-info">Managed Account Info</a></li>
    </ul>
</div>


<!--profile tab-->
<div id="profile-inner" class="pro-doc-full">
    <div class="col-sm-6 padding-0">
        <div id="all-profile" class="col-sm-12">
            @if($client->profile)
            <h2>
                Personal Profile
            </h2>
            <div id="client-inner" class="personal-profile">
                <div class="details">
                    <p class="title">Name</p>
                    <p class="value">{{ $client->profile->first_name.' '.$client->profile->middle_name.' '.$client->profile->last_name }}</p>
                </div>
                <div class="details">
                    <p class="title">Email</p>
                    <p class="value">{{ $client->email }}</p>
                </div>
                <div class="details">
                    <p class="title">Address</p>
                    <p class="value">{{ $client->profile->first_address . ' ' . $client->profile->last_address }}</p>
                </div>
                <div class="details">
                    <p class="title">City</p>
                    <p class="value">{{ $client->profile->city }}</p>
                </div>
                <div class="details">
                    <p class="title">State</p>
                    <p class="value">{{ $client->profile->state->name }}</p>
                </div>
                <div class="details">
                    <p class="title">Zip code</p>
                    <p class="value">{{ $client->profile->zip }}</p>
                </div>
                <div class="details">
                    <p class="title">Phone</p>
                    <p class="value">{{ $client->profile->phone_number }}</p>
                </div>
                <div class="details">
                    <p class="title">Date of birth</p>
                    <p class="value">{{ date('d M Y', strtotime($client->profile->dob)) }}</p>
                </div>
                <div class="details">
                    <p class="title">Gender</p>
                    <p class="value">
                        <?php
                        if ($client->profile->sex == 1) {
                            echo "Male";
                        } else if ($client->profile->sex == 2) {
                            echo "Female";
                        }
                        ?>
                    </p>
                </div>

            </div>
            @endif
        </div>

        <div id="all-profile" class="col-sm-12">
            @if($client->financialProfile)
            <h2>
                Financial profile
            </h2>

            <div id="client-inner" class="personal-profile">
                <div class="details">
                    <p class="title">Tax filing status</p>
                    <p class="value">
                        <?php
                        echo config('constant.tax_filing_status')[$client->financialProfile->tax_filing_status];
                        ?>
                    </p>
                </div>
                <div class="details">
                    <p class="title">Your annual income</p>
                    <p class="value">
                        <?php
                        echo $client->financialProfile->annual_income;
                        ?>
                    </p>
                </div>
                <?php
                if ($client->financialProfile->spouse_annual_income > 0) {
                    ?>
                    <div class="details">
                        <p class="title">Your spouse's annual income</p>
                        <p class="value">
                            <?php echo $client->financialProfile->spouse_annual_income; ?>
                        </p>
                    </div>
                <?php } ?>
            </div>
            @endif
        </div>

        <div id="all-profile" class="col-sm-12">
            @if($client->financialProfile)
            <h2>
                Employment profile
            </h2>
            <div id="client-inner" class="personal-profile">
                <div class="details">
                    <p class="title">Employment status</p>
                    <p class="value">
                        <?php
                        echo config('constant.employement_status')[$client->financialProfile->employment_status];
                        ?>
                    </p>
                </div>
            </div>
            @endif
        </div>

        @foreach($all_profiles as $key => $profiles)
        <div id="all-profile" class="col-sm-12">
            <h2>{{ $key }}</h2>
            @foreach($profiles as $profileQuestion)
            <div class="details">
                <p class="title">{{ $profileQuestion['question'] }}</p>
                <p class="value">{{ !empty($profileQuestion['answer'])?$profileQuestion['answer']:'..' }}</p>
            </div>
            @endforeach
        </div>
        @endforeach

    </div>

    <div class="col-sm-6 padding-0">
        <div id="all-profile" class="col-sm-12 mag-0">
            <div id="commentry">
                <h2>Notes
                    <span class="" id="notesModal" data-toggle="modal" data-target="#addNotesModal"> <i class="fa fa-plus-circle"></i> </span></h2>
                <div class="notes-data " data-clientId="{{$client->Profile->user_id}}" data-pageno ="2"> 
                    <?php if (!empty($all_notes)) { ?>
                        @foreach ($all_notes as $note)
                        <div class="notes-details">
                            <p class="date">{{ $note['date'] }}</p>
                            <p class="note-info">{{ $note['note'] }}</p>
                        </div>
                        <hr>
                        <div class="clearfix"></div>
                        @endforeach
                    <?php } else { ?>
                        <div class="no-notes">
                            No notes yet
                        </div>
                    <?php } ?>
                </div>

                <?php if ($totalRecords > 15) { ?>
                    <div class="load-more-btn">
                        <a href="javascript:void(0)" id="loadMore" > show more </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!--document tab-->
<div id="documents-inner" class="pro-doc-full">
    <div class="documents-nav" style="display:flex">
        <ul class="list-inline">
            @foreach($documentCategories as $key=>$documentCategory)
            <li class="{{ $key == 1 ? 'active' : ''}}"><a href="#{{$documentCategory}}" document-type="{{$key}}">{{$documentCategory}}</a></li>
            @endforeach
        </ul>
        <button type="button" class="btn uplode-doc-button" id="uploadDocs" data-toggle="modal" data-target="#uploadDocsModal"> Upload</button></h2>
    </div>
    <div class="w-100 padding-0 documents-type-table" id="statements" style="display:block;">
        <div id="document-activity">   
            <div class="documents-table-container" >
                <div class="table-responsive">
                    <table id="documents_table" class="table">
                        <thead>
                            <tr>
                                <th class="w10">Date</th>
                                <th>Description</th>
                                <th class="w10">Download</th>
                                <th class="w10">Action</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                            if (!empty($allDocumentsStatements)) {
                                $url = url('/');
                                ?> 
                                @foreach($allDocumentsStatements as $document)
                                <tr id="doc-id-{{$document['id'] }}">
                                    <td>{{ $document['updated_at'] }}</td>
                                    <td>{{ $document['description']}} </td>
                                    <td><a download  href="/uploadedClientDocuments/{{ $document['document_path']}}">{{ $document['original_name']}}</a></td>
                                    <td class="action-button">
                                        <!--<a href="javascript:void(0)" class="signDocument" data-id="{{ $document['id'] }}"><i class="fa fa-envelope" data-toggle="tooltip" data-placement="top" title="Email"></i></a>-->
                                        <!--<a href="{{config('constant.adobe_eSignature.API_URL') }}/public/oauth?redirect_uri=<?php // echo $url                     ?>/documentStatusCallback&response_type=code&client_id={{config('constant.adobe_eSignature.CLIENT_ID') }}&scope=user_read+user_write+user_login+agreement_read+agreement_write+library_read+workflow_read+workflow_write+webhook_read+webhook_write+webhook_retention"  data-id="{{ $document['id'] }}"><i class="fa fa-envelope" data-toggle="tooltip" data-placement="top" title="Email"></i></a>-->
                                        <a href="javascript:void(0)" class="editDocument" data-id="{{ $document['id'] }}"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                        <span  data-id="{{ $document['id'] }}" class="action-button delete-document"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                                    </td>
                                </tr>
                                @endforeach
                            <?php } else { ?>
                            <p> No document available yet </p>
                        <?php } ?>
                        </tbody>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>

    <div class="w-100 padding-0 documents-type-table"  class="w-100 padding-0" id="taxforms">
        <div id="document-activity">   
            <div class="documents-table-container" >
                <div class="table-responsive">
                    <table id="documents_table" class="table">
                        <thead>
                            <tr>
                                <th class="w10">Date</th>
                                <th>Description</th>
                                <th class="w10">Download</th>
                                <th class="w10">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($allDocumentsTaxForms)) { ?> 
                                @foreach($allDocumentsTaxForms as $document)
                                <tr id="doc-id-{{$document['id'] }}">
                                    <td>{{ $document['updated_at'] }}</td>
                                    <td>{{ $document['description']}} </td>
                                    <td><a download  href="/uploadedClientDocuments/{{ $document['document_path']}}">{{ $document['original_name']}}</a></td>
                                    <td class="action-button">
                                        <a href="javascript:void(0)" class="editDocument" data-id="{{ $document['id'] }}"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                        <span  data-id="{{ $document['id'] }}" class="action-button delete-document"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                                    </td>
                                </tr>
                                @endforeach
                            <?php } else { ?>
                            <p> No document available yet </p>
                        <?php } ?>
                        </tbody>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>

    <div class="w-100 padding-0 documents-type-table"  id="insuranceforms" >
        <div id="document-activity">   
            <div class="documents-table-container" >
                <div class="table-responsive">
                    <table id="documents_table" class="table">
                        <thead>
                            <tr>
                                <th class="w10">Date</th>
                                <th>Description</th>
                                <th class="w10">Download</th>
                                <th class="w10">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($allDocumentsInsuranceForms)) { ?> 
                                @foreach($allDocumentsInsuranceForms as $document)
                                <tr id="doc-id-{{$document['id'] }}">
                                    <td>{{ $document['updated_at'] }}</td>
                                    <td>{{ $document['description']}} </td>
                                    <td><a download  href="/uploadedClientDocuments/{{ $document['document_path']}}">{{ $document['original_name']}}</a></td>
                                    <td class="action-button">
                                        <a href="javascript:void(0)" class="editDocument" data-id="{{ $document['id'] }}"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                        <span  data-id="{{ $document['id'] }}" class="action-button delete-document"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                                    </td>
                                </tr>
                                @endforeach
                            <?php } else { ?>
                            <p> No document available yet </p>
                        <?php } ?>
                        </tbody>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>
    <div class="w-100 padding-0 documents-type-table" id="otherdocs">
        <div id="document-activity">   
            <div class="documents-table-container" >
                <div class="table-responsive">
                    <table id="documents_table" class="table">
                        <thead>
                            <tr>
                                <th class="w10">Date</th>
                                <th>Description</th>
                                <th class="w10">Download</th>
                                <th class="w10">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (!empty($allDocumentsOtherDocs)) { ?> 
                                @foreach($allDocumentsOtherDocs as $document)
                                <tr id="doc-id-{{$document['id'] }}">
                                    <td>{{ $document['updated_at'] }}</td>
                                    <td>{{ $document['description']}} </td>
                                    <td><a download  href="/uploadedClientDocuments/{{ $document['document_path']}}">{{ $document['original_name']}}</a></td>
                                    <td class="action-button">
                                        <a href="javascript:void(0)" class="editDocument" data-id="{{ $document['id'] }}"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                        <span  data-id="{{ $document['id'] }}" class="action-button delete-document"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                                    </td>
                                </tr>
                                @endforeach
                            <?php } else { ?>
                            <p> No document available yet </p>
                        <?php } ?>
                        </tbody>
                    </table>
                </div><!--table-responsive-->
            </div>
        </div>
    </div>
</div>

<!--services tab-->
<div id="services-inner" class="pro-doc-full">
    <div class="col-sm-12 padding-0">
        @if(($services->count()>0))
        <div class="row service-name all-services">
            <div class="previous"> </div>
            <div class=" service-options owl-carousel" >
                @foreach ($services as  $key => $service)
                <div class="item">
                    <div class='col-sm-12 products-list {{($key ==0)? 'selected' :''}}' data-id="{{$service->id}}">
                        <div class="nested-div">
                            <div class="position-absolute">
                                <h4 class="service-title">{{$service->title}}</h4>
                            </div>
                        </div>
                    </div>
                </div> 
                @endforeach
            </div>
            <div class="next"> </div>
        </div>
        <div id="document-activity" class="service-preview-file">
        </div>
        @else
        <p>No service availed yet</p>
        @endif
    </div>
</div>

<div id="managed-account-info" class="pro-doc-full" style="display: none;">

    <div class="col-sm-6 padding-0">
        <!--third party code-->
        <div class="container-fluid wrapper">
            <div class="col-sm-12 recommended-service-div">
                {{ Form::open([ 'route' => 'frontend.employee.userAccountInfo', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                <input type="hidden" name="client_id" value="{{$client_id}}">
                <h3></h3> 
                <fieldset>
                    <section class="sections">
                        <div class="col-sm-12">
                            <div class="col-sm-6 col-sm-offset-1 section-right">
                                {{ Form::input('hidden','questionName[]','Mortgages on non-rental property') }}
                                <div class="col-sm-12  ">
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <div class="row">
                                                <div class="add-child add-property">
                                                    <table id='property-info' class="find-table-length">
                                                        <thead>
                                                            <tr> 
                                                                <td colspan="2">
                                                                    <h5> 
                                                                        List of all Third Party 
                                                                    </h5> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($user_account_info["third_party_data"])) {
                                                                $mortgage = json_decode($user_account_info["third_party_data"], true);

                                                                if (!empty($mortgage)) {
                                                                    foreach ($mortgage as $key => $data) {
                                                                        ?>
                                                                        <tr data-index="{{$key}}">
                                                                            <td>{{$data['authorized_third_party']}}</td>
                                                                            <td valign="top"> </td>
                                                                            <td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table> 
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <button type='button' class="add-dependent add-mortgage" data-toggle="modal" data-target="#propertyModal">Add Third Party</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="nonRentalMortgagesData" class="hidden" name="answer[third_party_data]">{{(!empty($user_account_info["third_party_data"]))? $user_account_info["third_party_data"]:null}}</textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-6 col-sm-offset-1 section-right">

                                <div class="col-sm-12  ">
                                    <div class="row">
                                        <div class="col-sm-12 ">
                                            <div class="row">
                                                <div class="add-child add-property">
                                                    <table id='property-info1' class="find-table-length">
                                                        <thead>
                                                            <tr> 
                                                                <td colspan="2">
                                                                    <h5> 
                                                                        List of all Identification of Accounts
                                                                    </h5> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                            if (!empty($user_account_info["accounts_data"])) {
                                                                $mortgage1 = json_decode($user_account_info["accounts_data"], true);
                                                                if (!empty($mortgage1)) {
                                                                    foreach ($mortgage1 as $key => $data) {
                                                                        ?>
                                                                        <tr data-index="{{$key}}">
                                                                            <td>{{$data['account_number']}}</td>

                                                                            <td valign="top"> </td>
                                                                            <td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i>
                                                                                <i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>
                                                                        </tr>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>
                                                        </tbody>
                                                    </table> 
                                                    <div class="col-md-5">
                                                        <div class="row">
                                                            <button type='button' class="add-dependent add-mortgage1" data-toggle="modal" data-target="#propertyModal1">Add Account</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="nonRentalMortgagesData1" class="hidden" name="answer[accounts_data]">{{(!empty($user_account_info["accounts_data"]))? $user_account_info["accounts_data"]:null}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sections">
                            <div id="propertyModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD Third Party</h4>
                                        </div>
                                        <div class="modal-body" style="overflow: hidden;">
                                            <div class="row">
                                                <!-- first step starts -->
                                                <div class=" setup-content" id="step-1">
                                                    <div class="section-right ">
                                                        {{ Form::input('hidden','questionName[]','ADD MORTGAGE') }}
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'The Authorized Third Party for the Account is:') }}
                                                            {{ Form::input('text','authorized_third_party',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name1', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Mailing Address:') }}
                                                            {{ Form::input('email','mailing_address',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name2 ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Telephone:') }}
                                                            {{ Form::input('number','telephone',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name3', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <button id="finishDRMortgagenew" class='finishDRMortgage finishBtn pull-right' type="button" >
                                                                finish<i class="fa fa-arrow-right" style="    font-size: 15px;    font-weight: normal;    margin-left: 4px;"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                            <div id="propertyModal1" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">ADD Identification of Accounts</h4>
                                        </div>
                                        <div class="modal-body" style="overflow: hidden;">
                                            <div class="row">
                                                <!-- first step starts -->
                                                <div class=" setup-content" id="step-1">
                                                    <div class="section-right ">
                                                        {{ Form::input('hidden','questionName[]','ADD Identification of Accounts ') }}

                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Account Number:') }}
                                                            {{ Form::input('number','account_number',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name1', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Registration Name') }}
                                                            {{ Form::input('text','registration_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name2 ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Custodian Name') }}
                                                            {{ Form::input('text','custodian_name',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name3 ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Fee if different from Negotiated Rate') }}
                                                            {{ Form::input('number','fee',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name4', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Discrtion') }}
                                                            {{ Form::input('number','discrtion',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name5', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Non-Discrtion') }}
                                                            {{ Form::input('number','non_discrtion',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name6', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Debit') }}
                                                            {{ Form::input('number','debit',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name7', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>
                                                        <div class="col-sm-12 inner-left">
                                                            {{ Form::label('label', 'Bill') }}
                                                            {{ Form::input('number','bill',null,['data-validation'=> '' , 'class'=>'custom-validation form-control property-name8', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'']) }}
                                                        </div>

                                                        <div class="col-sm-12">
                                                            <button id="finishDRMortgagenew1" class='finishDRMortgage1 finishBtn pull-right' type="button" >
                                                                finish<i class="fa fa-arrow-right" style="    font-size: 15px;    font-weight: normal;    margin-left: 4px;"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </section>
                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
        <!--third party code end-->
    </div>

</div>

<!--modal-->
<div class="modal" id="addNotesModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Notes</b></h4>
                <h6>Hit Enter button to save a note.</h6>
            </div>
            <div class="add-note-input">
                {{ Form::open(['route' =>'frontend.employee.addNotes', 'method' => 'post' , 'id'=>'notesForm']) }}
                <div id="form-response"></div>
                <span>
                    {{ Form::hidden('client_id', $client->Profile->user_id) }}
                    {{ Form::hidden('employee_id', auth()->user()->id) }}
                    <!--{{ Form::label('client_note', 'Notes') }}-->
                    {{ Form::textarea('notes',null,['class'=>'form-control', 'rows' => 1, 'cols' => 40, 'placeholder'=>'Add note']) }}
                </span>
                {!! Form::close() !!}
            </div> 
            <div class="modal-body">
                <div class="previous-notes" data-clientId="{{$client->Profile->user_id}}" data-pageno ="2">

                    <?php if (!empty($all_notes)) { ?>
                        @foreach ($all_notes as $note)
                        <div class="modal-notes-details">
                            <p class="date">{{ $note['date'] }}</p>
                            <p class="employee-name">{{ $employeeName }}</p>
                            <p class="note-info">{{ $note['note'] }}</p>
                        </div>
                        @endforeach
                    <?php } else { ?>
                        <div class="no-notes">
                            No notes yet
                        </div>
                    <?php } ?>

                </div>

                <?php if ($totalRecords > 15) { ?>
                    <div class="loader">
                        <div class="rotate half-size"></div>
                        <div class="top"></div>
                        <div class="bottom"></div>
                        <div class="top-small"></div>
                        <div class="bottom-small"></div>
                    </div>
                <?php } ?>
                <div class="continue-btn" style="display: none; ">
                    {{ Form::submit('Continue', ['class' => 'btn btn-info center-block employee-note-button']) }}

                </div> 
            </div>
        </div>

    </div>
</div>

<input type="hidden" id="employee-id" value={{Auth::user()->id}}>
<input type="hidden" id="client-id" value={{$client->id}}>
@foreach($notificationId as $notiId)
<input type="hidden" id="notification_id" value="{{$notiId->id}}">
@endforeach
<div class="modal" id="uploadDocsModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Upload File</b></h4>
            </div>
            <div class="modal-body">
                <input type="hidden" id="connection-id" value="">
                <input type="hidden" id="document-id" value="">

                <form class="" id="doc-upload-form" enctype='multipart/form-data'>
                    <div class="form-group">
                        <label for="documentCategorySelect">Document Category</label>
                        <select class="form-control" id="documentCategorySelect" name="documentCategory">
                            @foreach($documentCategories as $key=>$documentCategory)
                            <option  value="{{$key}}">{{$documentCategory}}</option>
                            @endforeach
                        </select>
                        <i style="position: absolute; left: 63%; top: 30px; z-index: 0;" class="fa fa-angle-down selectArrow" aria-hidden="true"></i>
                    </div>
                    <div class="form-group" id="testdrop">
                        <label for="documentDesc">Document Description</label>
                        <textarea class="form-control" id="documentDesc" rows="3" name="documentDesc" placeholder="Add a brief description of document file."></textarea>
                    </div>
                    <div class="form-group" id="select_template_or_upload_new_div" style="display:none;">
                        <label for="documentCategorySelect">Document Type</label>
                        <div class="radio">
                            <label><input type="radio" class="select_template_or_upload_new" name="select_template_or_upload_new" value="upload_new" checked>Upload New</label>
                        </div><div class="radio">
                            <label><input type="radio" class="select_template_or_upload_new" name="select_template_or_upload_new" value="select_template" >Select Contract</label>
                        </div>

                    </div>
                    <div id="file_upload_div">
                        <div id="file-upload-box">
                            <div id='file-dropzone'>
                                <img id="type-doc" src="/img/cloud-backup-up-arrow.png" width="50px" height="50px">
                                Drag Files Here or <a href="javascript:void(0);">Browse</a>
                            </div>
                        </div>
                        <input type="file" class="form-control"  name="file" id="file_uploader" accept=".xlsx, .xls,.doc, .docx, .pdf" >
                        <div class="file-preview-container">
                            <div id="type-pdf" width="45%" height="270px">
                                <canvas id="pdf-canvas"></canvas>
                            </div>
                            <img id="type-doc" src="/img/doc-icon.png" width="45%" height="270px">
                            <img id="type-docx" src="/img/docx-icon.png" width="45%" height="270px">
                            <img id="type-xls" src="/img/xls-icon.png" width="45%" height="270px">
                            <a href="javascript:void(0)" id="remove-file-button">Clear</a>
                            <div class="file-name-conatiner">
                                <p class="uploaded-doc-name">test</p>
                                <a href="javascript:void(0)" id="edit-file-name">Edit</a>
                            </div>
                        </div>
                        <div class="file-info-container">
                            <input class="" id="file-name-input" value="" >
                            <a href="javascript:void(0)" id="done-file-name">Done</a>
                        </div>
                    </div>
                    <div id="template_dropdown" style="display:none;">
                        <div class="form-group">
                            <label for="templateCategorySelect">Contract Category</label>
                            <select class="form-control" id="contractCategorySelect" name="templateCategory">
                                <option value="1">Financial Consulting</option>
                            </select>
                        </div>  
                        <div class="bottom-container">
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="client-signature">
                                <label class="form-check-label" for="client-signature"> Required Signature</label>
                            </div>
                        </div>
                    </div>

                    <div class="bottom-container">
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="notify-client-check">
                            <label class="form-check-label" for="notify-user-check"> Notify client</label>
                        </div>
                    </div>
                </form>
                <div class="upload-button-div">
                    <button id="upload-form" class="btn btn-primary bottom-upload-button" >Upload</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('js/comprehensive-planning.js') }}"></script> 
<script>
$(document).ready(function () {

    $(".service-options").owlCarousel({
        responsiveClass: true,
        navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            767: {
                items: 2,
                nav: false
            },
            991: {
                items: 4,
                nav: true,
                loop: false
            }
        }
    })

    var selectedServiceId = '<?php echo @$services[0]["id"] ?>';
    var comprehensive_life_insurance_id = '<?php echo config('constant.questions.comprehensive_life_insurance_id') ?>';
    var comprehensive_spouse_life_insurance_id = '<?php echo config('constant.questions.comprehensive_spouse_life_insurance_id') ?>';
    var stockServiceId = '<?php echo config('constant.questions.stock_service_id') ?>';
    var annualServivceId = '<?php echo config('constant.questions.annual_service_id') ?>';
    var semiAnnualServiceId = '<?php echo config('constant.questions.semi_annual_service_id') ?>';
    var lifeInsuranceServiceId = '<?php echo config('constant.questions.life_insurance_service_id') ?>';
    var comprehensive_life_insurance_ids = [comprehensive_life_insurance_id, comprehensive_spouse_life_insurance_id];
    var your_liabilities = '<?php echo config('constant.questions.your_liabilities') ?>';
    var clientId = $('#client-id').val();
    var ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
    var incomeTaxTopicId = "{{config('constant.questions.income_tax_topic_id') }}";
    var userName = "{{ $client->profile->first_name.' '.$client->profile->middle_name.' '.$client->profile->last_name }}";


    if (typeof selectedServiceId != "undefined" && selectedServiceId) {
        serviceClick(clientId, selectedServiceId);
    }
    function serviceClick(clientId, serviceId, doNotAnimate = false) {
        $.ajax({
            type: 'get',
            url: "/getTopicsOrQuestions/" + serviceId + "/" + clientId,
            success: function (data) {
                if (data) {
                    $('.service-preview-file').html(data);
                    if ($('.service-preview-file').find('.serviceQuestions').length == 1) {
                        if (doNotAnimate) {
                            $('.serviceQuestions').css({left: '0'});
                        } else {
                            $('.serviceQuestions').animate({left: '0'});
                        }
                    }
                    fixListHeight();
                    showHideQuestions();
                }

            }
        })
    }
    function onServiceClick() {

        $('.products-list').on('click', function () {
            var serviceId = $(this).attr('data-id');
            var clientId = $('#client-id').val();
            serviceClick(clientId, serviceId);
            fixListHeight()
        });
    }
    function onTopicClick() {
        $(document).on('click', '.single-topic', function (e) {
            e.stopImmediatePropagation();
            if ($('.pre-topic-questions').find('.active-inputs').length > 0) {
                $('.pre-topic-questions').find('.active-inputs').hide();
                $('.pre-topic-questions').find('.updateAnswers').hide();
            }
            var question = $(this).attr('data-question');
            var serviceId = $('.products-list.selected').attr('data-id');
            var topicId = $(this).attr('data-topic');
            var clientId = $('#client-id').val();
            var topicName = $(this).attr('data-topic-name');
            if ($(this).attr('data-documents')) {
                var url = "/getServiceDocuments/" + serviceId + "/" + clientId;
            } else {
                var url = "/getSubTopicsOrQuestions/" + serviceId + "/" + clientId + "/" + topicId;
            }
            TopicClick(serviceId, topicId, clientId, topicName, url, $(this));
        });
    }

    function TopicClick(serviceId, topicId, clientId, topicName, url, $this) {
        $.ajax({
            type: "post",
            data: {topicName: topicName},
            url: url,
            success: function (data) {
                if (data) {
                    $('.service-preview-file .serviceSubTopics').remove();
                    $('.service-preview-file .serviceQuestions').remove();
                    $('.service-preview-file').append(data);
                    if (!$('.TaxableIncome').is(':visible') && $('.HouseHoldIncome').is(':visible')) {
                        var houseHoldIncome = $('.HouseHoldIncome').val();
                        getTaxBracket(houseHoldIncome, $this);
                    } else if ($('.TaxableIncome').is(':visible')) {
                        var taxableIncome = $('.TaxableIncome').val();
                        getTaxBracket(taxableIncome, $this);
                    }
                    if ($('.service-preview-file').find('.serviceQuestions').length == 1) {
                        $('.serviceQuestions').animate({left: '0'});
                        $('.serviceTopicsWrapper').hide();
                        if (topicId == incomeTaxTopicId) {
                            if ($('tr.fileUploadRow').length != 0) {
                                $('.add-file-btn').hide();
                            }
                        }
                        $(document).off('click', '.back-to-sub-topics');
                        $(document).on('click', '.back-to-sub-topics', function () {
                            backToTopicsSubTopics();
                            return false;
                        });
                    } else {
                        showSlide();
                        hideSlide();
                    }
                    $('.preTopicQuestions').hide();
                    fixListHeight();
                    showHideQuestions();
                }
            }
            ,
            error: function (err) {
            }
        });
    }
    function backToTopicsSubTopics(divToBeShown = false) {
        if ($('.pre-topic-questions').find('.active-inputs').length > 0) {
            $('.pre-topic-questions').find('.active-inputs').show();
        }
        $('.serviceQuestions').animate({left: '200%'});
        if (divToBeShown) {
            $('.' + divToBeShown).show(300);
        } else {
            $('.serviceTopicsWrapper').show(300);
        }
        $('.preTopicQuestions').show();
    }
    function onSubTopicClick() {
        $(document).on('click', '.single-sub-topic', function (e) {
            e.stopImmediatePropagation();
            var copyTabCurrent = $(this);
            var serviceId = $('.products-list.selected').attr('data-id');
            var topicId = $(this).attr('data-topic');
            var clientId = $('#client-id').val();
            var topicName = $(this).attr('data-topic-name');
            var copyIndex = $(this).attr('data-copy-index');
            var displayType = $(this).attr('data-display-type');
            subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType);
        });
    }
    function subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, doNotAnimate = false, divToBeShown = false) {
        var url = "/getQuestionsOfTopicOrSubTopic/" + serviceId + "/" + clientId + "/" + topicId;
        $.ajax({
            type: "post",
            data: {topicName: topicName, copyIndex: copyIndex, displayType: displayType},
            url: url,
            success: function (data) {
                if (data) {
                    $('.service-preview-file .serviceQuestions').remove();
                    $('.serviceTopics').append(data);
                    $(".copy-tabs[data-copy-index='" + copyIndex + "']").addClass('selected-tab').siblings().removeClass('selected-tab');
                    $(".main-tabset-item[data-display-type='" + displayType + "']").addClass('active').siblings().removeClass('active');
                    var isCopy = false;
                    if (copyTabCurrent.hasClass('copy-tabs')) {
                        isCopy = true;
                    }
                    showQuestionsSlide(isCopy, doNotAnimate);
                    hideQuestionsSlide(isCopy, doNotAnimate, divToBeShown);
                    fixListHeight();
                }
            }
            ,
            error: function (err) {
            }
        });
    }
    function showSlide() {
        $('.serviceSubTopics').animate({left: '0'});
        $('.serviceTopicsWrapper').hide();
    }
    function hideSlide() {
        $(document).off('click', '.back-to-topics');
        $(document).on('click', '.back-to-topics', function () {
            $('.service-preview-file .serviceSubTopics').animate({left: '200%'});
            $('.serviceTopicsWrapper').show(500);
            $('.preTopicQuestions').show();
        });
        showHideQuestions()
    }
    function showQuestionsSlide(isCopy = false, doNotAnimate = false) {
        if (!isCopy && !doNotAnimate) {
            $('.serviceQuestions').animate({left: '0'});
        } else {
            $('.serviceQuestions').css({left: '0'});
        }
        $('.serviceSubTopics').hide(300);
        fixListHeight()
        showHideQuestions()
    }
    function hideQuestionsSlide(isCopy = false, doNotAnimate = false, divToBeShown = false) {
        $(document).off('click', '.back-to-sub-topics');
        $(document).on('click', '.back-to-sub-topics', function () {
            if (doNotAnimate) {
                backToTopicsSubTopics(divToBeShown);
            } else {
                if (!isCopy) {
                    $('.serviceQuestions').animate({left: '200%'});
                } else {
                    $('.serviceQuestions').css({left: '200%'});
                }
                $('.serviceSubTopics').show(300);
                $('.serviceTopicsWrapper').hide();
//                $('.serviceTopics').show();  
            }
            fixListHeight();
        });
    }
    function showSubQuestionsSlide() {
        $('.serviceModalQuestions').animate({left: '0'});
        $('.serviceQuestions').hide(300);
        showHideQuestions();
        fixListHeight()
    }
    function hideSubQuestionsSlide(copyIndex = false) {
        $(document).off('click', '.back-to-questions');
        $(document).on('click', '.back-to-questions', function () {

            $('.serviceModalQuestions').animate({left: '200%'});
            $('.serviceSubTopics').hide();
            var serviceId = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-service-id');
            var topicId = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-topic-id');
            var copyIndex = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-previous-slide-copy-index');
            var displayType = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-display-type');
            if (!copyIndex) {
                var copyIndex = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-copy-index');
            }
            var topicIsChild = $(this).closest('.topic-heading').find('.updateAnswers').attr('data-topic-is-child');
            var clientId = $('#client-id').val();
            if (topicId) {


                if (topicIsChild == '1') {
                    var copyTabCurrent = $('.single-sub-topic[data-topic="' + topicId + '"]');
                    var divToBeShown = 'serviceSubTopics';
                } else {

                    var copyTabCurrent = $('.single-topic[data-topic="' + topicId + '"]');
                    var divToBeShown = 'serviceTopicsWrapper';
                }
                var topicName = copyTabCurrent.attr('data-topic-name');
                if (!copyIndex) {
                    var copyIndex = copyTabCurrent.attr('data-copy-index');
                }
                if (!displayType) {
                    var displayType = copyTabCurrent.attr('data-display-type');
                }
                subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, 'doNotAnimate', divToBeShown);
                fixListHeight();
            } else {
                serviceClick(clientId, serviceId, 'doNotAnimate');
            }
        });
    }
    function onModalClick() {

        $(document).on('click', '.showModalQuestions', function (e) {
            $(this).closest('.service-preview').find('.active-inputs').show();
            $(this).closest('.service-preview').find('.updateAnswers').hide();
            e.stopImmediatePropagation();
            var serviceId = $(this).parent().attr('data-service-id');
            var topicId = $(this).parent().attr('data-topic-id');
            var answerIndex = $(this).parent().attr('data-index');
            var questionId = $(this).parent().attr('data-question-id');
            var copyIndex = $(this).parent().attr('data-copy-index');
            var data_previous_slide_copy_index = $(this).parent().attr('data-previous-slide-copy-index');
            var displayType = $(this).parent().attr('data-display-type');
            var clientId = $('#client-id').val();
            var url = "/getDependentQuestions/" + serviceId + "/" + clientId;
            if (topicId) {
                var url = "/getDependentQuestions/" + serviceId + "/" + clientId + "/" + topicId;
            }
            $.ajax({
                type: 'post',
                url: url,
                data: {answerIndex: answerIndex, questionId: questionId, copyIndex: copyIndex, data_previous_slide_copy_index: data_previous_slide_copy_index, displayType: displayType},
                success: function (data) {
                    if (data) {
                        $('.service-preview-file .serviceModalQuestions').remove();
                        if ($('.service-preview-file').find('.serviceTopics').length != 1) {
                            $('.service-preview-file').append(data);
                        } else {
                            $('.serviceTopics').append(data);
                        }
                        // Init datepicker
                        $('.question_datepicker').datetimepicker({
                            format: 'DD-MM-YYYY',
                            useCurrent: true,
                        })
                        // Init yearpicker
                        $(".question_yearpicker").datetimepicker({
                            viewMode: "years",
                            format: "YYYY",
                            useCurrent: true,
                        });

                        showSubQuestionsSlide();
                        hideSubQuestionsSlide(copyIndex);
                        showHideQuestions();
                        fixListHeight()

                        $(this).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                            let symbols = $(element).attr('data-symbols');
                            let symbolsArray = symbols.split(',');
                            let inputValue = $(element).find('input').val();
                            if (symbolsArray.length > 1) {
                                if (inputValue) {
                                    var symbolValue = symbolsArray[0]
                                    var newInputValue = ''
                                }
                                let blank = '';
                                let selected = 'selected="selected"';
                                const selectedFirst = symbolValue == symbolsArray[0] ? selected : blank;
                                const selectedSecond = symbolValue == symbolsArray[1] ? selected : blank;
                                let newHtml = '<div class="symbol-elements" style="display: flex">' +
                                        '<input type="text" class="new-input activeInputs"  value="' + newInputValue + '" required>' +
                                        '<select class="new-symbol-type activeInputs">' +
                                        '<option value="' + symbolsArray[0] + '" ' + selectedFirst + '>' + symbolsArray[0] + '</option>' +
                                        '<option value="' + symbolsArray[1] + '" ' + selectedSecond + '>' + symbolsArray[1] + '</option>' +
                                        '<select>' +
                                        '</div>'
                                $(element).find('input').hide();
                                $(element).find('.answer').append(newHtml);
                            }
                        })
                    }
                }
            })
        });
    }
    function onDeleteModalClick() {

        $(document).on('click', '.deleteModal', function (e) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this Modal Data!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $(this).closest('.service-preview').find('.active-inputs').show();
                    $(this).closest('.service-preview').find('.updateAnswers').hide();
                    var current_element = $(this);
                    e.stopImmediatePropagation();
                    var serviceId = $(this).parent().attr('data-service-id');
                    var topicId = $(this).parent().attr('data-topic-id');
                    var answerIndex = $(this).parent().attr('data-index');
                    var questionId = $(this).parent().attr('data-question-id');
                    var copyIndex = $(this).parent().attr('data-copy-index');
                    var clientId = $('#client-id').val();
                    var url = "/deleteAnswers/" + serviceId + "/" + clientId;
                    if (topicId) {
                        var url = "/deleteAnswers/" + serviceId + "/" + clientId + "/" + topicId;
                    }
                    $.ajax({
                        type: 'post',
                        url: url,
                        data: {answerIndex: answerIndex, questionId: questionId, copyIndex: copyIndex},
                        success: function (data) {
                            if (data.success == true) {
                                $('tr[data-question-id="' + questionId + '"]').each(function (index, childElement) {
                                    $(childElement).attr('data-index', index); // update table rows indexes
                                });
                                $('tr[data-question-id="' + questionId + '"]')
                                        .closest('.modal-table')
                                        .find('.addNewModal')
                                        .parent().attr('data-index',
                                        parseInt($('tr[data-question-id="' + questionId + '"]').length) - parseInt(1) + '_new');
                                current_element.parent().remove();
                            } else {
                                toastr.warning('Error Occured');
                            }
                        }
                    })
                } else {
                    swal("Your information is safe!");
                }
            });
        });
    }


    onServiceClick();
    onTopicClick();
    onSubTopicClick();
    onModalClick();
    onDeleteModalClick();
    fixListHeight()

    function updateDatepicker(element, value) {
        $(element).datepicker('update', new Date(value));
    }

    // Show hide dependent questions(visible) accroding to their parent values
    function showHideQuestions() {
        $('li.question-answer').each(function (index, data) {

            let element = $(this);
            let parentId = element.attr('data-parent-id');
            let questionId = element.attr('data-id');
            let parentKey = element.attr('data-parent-key');
            let topicId = element.attr('data-topic-id');
            let topicName = element.attr('data-topic-name');
            showHideSingleQuestion(element, parentId, parentKey, topicId, topicName);
        });
    }

    // to check value is string or json data.
    function isJson(item) {
        item = typeof item !== "string"
                ? JSON.stringify(item)
                : item;
        try {
            item = JSON.parse(item);
        } catch (e) {
            return false;
        }

        if (typeof item === "object" && item !== null) {
            return true;
        }

        return false;
    }

    // Divide list items in 2 coulumns if more then 6 items
    function fixListHeight() {
        $("select").selectBoxIt();
//            if ($('ul.questions-list li:visible').length > 6) {
//                $('ul.questions-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.questions-list').addClass('single-column').removeClass('double-column');
//            }
//            if ($('ul.topic-list li').length > 6) {
//                $('ul.topic-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.topic-list').addClass('single-column').removeClass('double-column');
//            }
//            if ($('ul.sub-topic-list li').length > 6) {
//                $('ul.sub-topic-list').removeClass('single-column').addClass('double-column');
//            } else {
//                $('ul.sub-topic-list').addClass('single-column').removeClass('double-column');
//            }
    }


//        <-------EDITABLE-------->


//        Toggles edit and save buttons
    function editAndSave() {
        $(document).off('click', '.active-inputs');
        $(document).on('click', '.active-inputs', function () {
            $('.question_datepicker').datetimepicker({
                format: 'DD-MM-YYYY',
            });
            // Init yearpicker
            $(".question_yearpicker").datetimepicker({
                viewMode: "years",
                format: "YYYY",
                useCurrent: true,
            });
            $(this).closest('.service-preview').find('input[type="text"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill), input[type="year"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill)  , input[type="number"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , input[type="radio"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , input[type="checkbox"]:not(.readOnly,.disalbedDropdown,.spouseNameAutofill), select:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , textarea:not(.readOnly,.disalbedDropdown,.spouseNameAutofill) , span.placeholder-symbol')
                    .attr('disabled', false)
                    .css({"border-bottom": "1px solid #ccc", "background": "#fff"});
            $(this).closest('.service-preview').find('.active-inputs').hide();
            $(this).closest('.service-preview').find('.updateAnswers').show();
            $(this).closest('.service-preview').find('.selectboxit-btn').removeClass('selectboxit-disabled');
            $(this).closest('.service-preview').find('.upload-statement .hide').removeClass('hide');
            $(this).closest('.service-preview').find('.addMoreRow .hide').removeClass('hide');
            $(this).closest('.service-preview').find('.deleteRow .hide').removeClass('hide');
            $(this).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                let symbols = $(element).attr('data-symbols');
                let symbolsArray = symbols.split(',');
                let inputValue = $(element).find('input').val();
                if (symbolsArray.length > 1) {
                    if (inputValue) {
                        if (/^[a-zA-Z0-9- ]*$/.test(inputValue) == false) {
                            var symbolValue = inputValue.substr(inputValue.length - 1);
                            var newInputValue = inputValue.split(symbolValue)[0];
                        } else {
                            if (inputValue.indexOf('Years') != -1) {
                                var symbolValue = 'Years';
                                var newInputValue = $(element).find('input').val().split('Years')[0];

                            } else {
                                var symbolValue = 'Months';
                                var newInputValue = $(element).find('input').val().split('Months')[0];

                            }
                        }
                    } else {
                        var symbolValue = symbolsArray[0]
                        var newInputValue = ''
                    }
                    let blank = '';
                    let selected = 'selected="selected"';
                    const selectedFirst = symbolValue == symbolsArray[0] ? selected : blank;
                    const selectedSecond = symbolValue == symbolsArray[1] ? selected : blank;
                    let newHtml = '<div class="symbol-elements" style="display: flex">' +
                            '<input type="text" class="new-input activeInputs"  value="' + newInputValue + '" required>' +
                            '<select class="new-symbol-type activeInputs">' +
                            '<option value="' + symbolsArray[0] + '" ' + selectedFirst + '>' + symbolsArray[0] + '</option>' +
                            '<option value="' + symbolsArray[1] + '" ' + selectedSecond + '>' + symbolsArray[1] + '</option>' +
                            '<select>' +
                            '</div>'
                    $(element).find('input').hide();
                    $(element).find('.answer').append(newHtml);
                } else {
//                        var symbolValue = inputValue.substr(inputValue.length - 1);
//                        inputValue = inputValue.split(symbolValue)[0];
//                        let newHtml = '<div class="symbol-elements" style="display: flex">' +
//                                '<input type="number" class="new-input" style="' + styleForNewHtml + '" value="' + inputValue + '">' +
//                                '<span class="new-symbol-type" style="padding: 5px;"> ' + symbolsArray[0] + ' </span>' +
//                                '</div>'
//                        $(element).find('input').hide();
//                        $(element).find('.answer').append(newHtml);
                }
            })
        });
    }
//        Toggles edit and save buttons ENDS

    function updateAnswers($this) {
        $this.closest('.service-preview').find('input[type="text"], input[type="year"]  , input[type="number"] , input[type="radio"] ,input[type="number"] , input[type="radio"] , input[type="checkbox"], select , span.placeholder-symbol')
                .attr('disabled', true)
                .css({"border-bottom": "none", "background": "transparent"});
        $this.closest('.service-preview').find('textarea').attr('disabled', true).css({"background": "transparent"});
        $this.closest('.service-preview').find('.updateAnswers').hide();
        $this.closest('.service-preview').find('.active-inputs').show();
        $this.closest('.service-preview').find('.selectboxit-btn').addClass('selectboxit-disabled');
        $this.closest('.service-preview').find('.upload-statement .modal-upload-file-input').addClass('hide');
        $this.closest('.service-preview').find('.upload-statement .upload-statement-button').addClass('hide');
        $this.closest('.service-preview').find('.upload-statement .delete-modal-file').addClass('hide');
        $this.closest('.service-preview').find('.addMoreRow .btn').addClass('hide');
        $this.closest('.service-preview').find('.deleteRow .fa').addClass('hide');

    }

    editAndSave();
    //Validation
    $(document).off('click', '.validation-button');
    function validateForm() {
        $(document).on('click', '.validation-button', function (e) {
            var formId = $(this).closest('.service-preview:visible').find('form').attr('id');
            if (!formId) {
                return false;
            }
            var saveButtonClicked = $(this).closest('.service-preview:visible').find('.updateAnswers');
            var saveButtonClickedClass = $(this).closest('.service-preview:visible').find('.updateAnswers').attr('data-class');
            $('label.service-input-validate').remove();
            let filesArray = [];
            $('#' + formId).find('.service-files-table tr.fileUploadRow').each(function (index, childElement) {

                let $row = $(childElement)
                let filePath = $row.find('td.file-download a').attr('download');
                let fileName = $row.find('td.file-name-input input').val();
                let fileNameKey = $row.find('td.file-name-input').attr('data-key-name');
                let fileType = $row.find('td.file-select-type select.file-type-dropdown').val();
                let fileTypeKey = $row.find('td.file-select-type').attr('data-key-name');
                let fileData = {}
                fileData[fileNameKey] = fileName;
                fileData['file_path'] = filePath;
                if (fileType) {
                    fileData[fileTypeKey] = fileType;
                }
                filesArray.push(fileData);
            })

            if (filesArray.length > 0) {
                $('#fileParentHidden').val(JSON.stringify(filesArray));
            }


            // To Check validation for checkboxes.............................
            function checkboxValidations(formId) {
                let isErrorFound = false;
                $('.type-checkbox').each(function (index, childElement) {
                    let dataId = $(childElement).attr('data-parent-id');
                    let parent_answer = $('li[data-id="' + dataId + '"] input[type=radio]:checked').val();
                    let $this = $('li[data-parent-id="' + dataId + '"] input[type="checkbox"]');
                    let notCheckedCount = [];
                    $this.each(function (innerIndex, element) {

                        if ($(element).prop('checked') == false && $(childElement).attr('data-parent-key') == parent_answer) {
                            notCheckedCount.push(false);
                        }
                        if (innerIndex + 1 == $this.length) {
                            if (notCheckedCount.length == $this.length) {
                                isErrorFound = true;
                                $(element).parent('.answer').find('.error').remove();
                                $(element).parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                            }
                        }
                    });
                });
                return isErrorFound;
            }

            function contigentTableValidation() {
                let isErrorFound = false;
                $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
                    $(childElement).closest('tbody').find('.error').remove();
                    if ($(childElement).find('td input').val() == '' && $(childElement).is(':visible')) {
                        $(childElement).closest('tbody').append('<label class="error error-alert table-single-error">fields can not be blank.</label>')
                        isErrorFound = true;
                    }
                });
                return isErrorFound;
            }
            $('#' + formId).validate({
                submitHandler: function (form) {
                    $('#' + formId).closest('.service-preview').find('li[data-symbols]').each(function (index, element) {
                        let inputValue = $(element).find('.symbol-elements input.new-input').val();
                        let inputSymbol = $(element).find('.symbol-elements select.new-symbol-type').val();
                        if (inputValue && inputSymbol) {
                            $(element).find('input:not(.new-input)').val(String(inputValue) + '' + String(inputSymbol)).show();
                            $(element).find('.symbol-elements').remove();
                        }
                    });
                    var inputs = $('#' + formId).serializeArray();

                    if (checkboxValidations(formId)) {
                        return false;
                    } // Because serializeArray() ignores unset checkboxes and radio buttons: /
                    if (contigentTableValidation()) {

                        return false;
                    } // To check inputs in contigent table validations

                    inputs = inputs.concat(
                            jQuery('#' + formId + ' input[type=checkbox]:not(:checked)').map(
                            function () {
                                return {"name": this.name, "value": 'unchecked'}
                            }).get()
                            );
                    // updated selected checkboxs value in data.
                    jQuery('#' + formId + ' input[type=checkbox]:checked').map(
                            function () {
                                let index = inputs.findIndex(el => el.name == this.name)
                                inputs[index] = {"name": this.name, "value": 'checked'}
                            }).get();

                    updateAnswer(saveButtonClicked, inputs, saveButtonClickedClass);
                },
                invalidHandler: function (form) {
                    console.log('invalid');
                    return false;
                },
                errorPlacement: function errorPlacement(error, element) {
                    var errorCount = $(element).closest('.answer').find('.service-input-validate').length
                    if (errorCount == 0) {
                        $(element).parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                        if ($('input[type="radio"]')) {
                            $(element).parent().parent('.answer').last().append('<label class="error error-alert service-input-validate">This field is required</label>');
                        }
                    } else {
//                          $('label.service-input-validate').css('display', 'block');
                    }

                },
                rules: {
                    required: true
                }
            });
            if ($('.service-input-validate').length > 0) {
                e.preventDefault();
            }
        });
    }
    validateForm();
    $(document).on('click', '.save-new-answers , .save-new-modal-answers , .save-new-topic-answers', function (e) {
        $(this).closest('.service-preview').find('.validation-button').trigger('click');
        e.stopImmediatePropagation();
    });
    function updateAnswer($this, inputs, $class) {
        var formObj = {};
        $.each(inputs, function (i, input) {
            if (input.name.indexOf("[]") != -1) {  // to check array type values
                let newKey = input.name.split('[]')[0];
                if (formObj[newKey] === undefined) {
                    formObj[newKey] = [input.value];
                } else {
                    formObj[newKey].push(input.value);
                }
            } else {
                formObj[input.name] = input.value; //  for normal values
            }
        });
        var serviceId = $this.attr('data-service-id');
        var topicId = $this.attr('data-topic-id');
        var answerIndex = $this.attr('data-index');
        var copyIndex = $this.attr('data-copy-index');
        var clientId = $('#client-id').val();
        var url = "/updateAnswers/" + serviceId + "/" + clientId;
        if (topicId) {
            var url = "/updateAnswers/" + serviceId + "/" + clientId + "/" + topicId;
        }
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: url,
            data: {answerIndex: answerIndex, copyIndex: copyIndex, data: formObj},
            success: function (data) {
                if (data.success == true) {
                    toastr.success('Updated Successfuly');
                    updateAnswers($this);
                    if (copyIndex && $class == 'save-new-answers') {
                        var copyTabCurrent = $('.single-sub-topic[data-topic="' + topicId + '"]');
                        var divToBeShown = 'serviceSubTopics';
                        var topicName = copyTabCurrent.attr('data-topic-name');
                        if ($this.closest('.service-preview').children().find('#main-tabset').length == 1) {
                            var displayType = $('.service-preview').children().find('#main-tabset').find('li.active').attr('data-display-type');
                        }
                        if (!displayType) {
                            var displayType = copyTabCurrent.attr('data-display-type');
                        }
                        subTopicClick(copyTabCurrent, serviceId, topicId, clientId, topicName, copyIndex, displayType, 'doNotAnimate', divToBeShown);
                        fixListHeight();
                    }
                    $('.updateAnswers').hide();
                } else {
                    toastr.warning('poor you');
                }
            }
        });
    }
    function onChangeShowHideQuestions() {
        $(document).on('change', 'input[type="radio"], select, input[type="checkbox"]', function () {
            var parent_li = $(this).closest('li');
            let element = $(parent_li);
            let questionId = element.attr('data-id');
            let parentValue = $(this).val();
            if (parentValue == null && $(this).attr('data-type') == 'select') {
                parentValue = $(this).find('option:first').attr('value');
            }
            if ($(this).attr('data-type') == 'checkbox') {
                if ($(this).is(':checked')) {
                    $(this).val(1);
                } else {
                    $(this).val(0);
                }
            }
            $("li.question-answer[data-parent-id='" + questionId + "']").each(function (index, childElement) {

                let parentKey = $(childElement).attr('data-parent-key');
//if (questionType == 'checkbox') {
//                    if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
//                        var val = [];
//                        $('.question-' + parentId + ':checked').each(function (i) {
//                            val[i] = $(this).val();
//                        });
//                    }
//                }
                if (isJson(parentKey)) {
                    parentKeyArray = Object.keys(JSON.parse(parentKey));
                    let keyIndex = parentKeyArray.findIndex(key => key == parentValue)
                    if (keyIndex > -1 && element.is(':visible')) {
                        $(childElement).show();
                        $(childElement).find('.answer').children(':first').attr('required', 'required');
                    } else {
                        $(childElement).hide();
                    }
                } else {
                    if (parentValue == parentKey && element.is(':visible')) {
                        $(childElement).show();
                        $(childElement).find('.answer').children(':first').attr('required', 'required');
                    } else {
                        $(childElement).hide();
                    }
//                    }
                }

                let dataInputType = $(childElement).attr('data-input-type');
                if (dataInputType == 'radio') {
                    $(childElement).find(':input').prop('checked', false).trigger('change');
                } else if (dataInputType == 'checkbox') {
                    $(childElement).find(':input').prop('checked', false).trigger('change');
                } else if (dataInputType == 'select') {
                    $(childElement).find('.answer select').val('').trigger('change');
                } else {
                    $(childElement).find(':input').val('');
                }
            });
        }
        );
    }

    onChangeShowHideQuestions();

    function showHideSingleQuestion(element, parentId, parentKey, topicId, topicName) {
        if (parentId && parentKey) {

//                    if ($('#question-' + parentId).parent().parent().css('display') != 'none') {
            let questionType = $('#question-' + parentId).attr('data-type')
            let parentValue = '';
            if (questionType == 'radio') {
                if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
                    parentValue = $('.question-' + parentId + ':checked').val();
                }
            } else if (questionType == 'checkbox') {
                if ($('.question-' + parentId + ':checked').closest('li').is(':visible')) {
                    var val = [];
                    $('.question-' + parentId + ':checked').each(function (i) {
                        val[i] = $(this).val();
                    });
                }
            } else {
                if ($('#question-' + parentId).closest('li').is(':visible')) {
                    parentValue = $('#question-' + parentId).val();
                }
            }
            if (!parentValue && ($.inArray(topicId, comprehensive_life_insurance_ids) !== -1 || topicName.toLowerCase() == your_liabilities)) {
                parentValue = element.attr('data-parent-answer');
            }

            let parentKeyArray = [];
            if (isJson(parentKey)) {
                parentKeyArray = Object.keys(JSON.parse(parentKey));
                let keyIndex = parentKeyArray.findIndex(key => key == parentValue)
                if (keyIndex > -1) {
                    element.show();
                } else {
                    element.hide();
                    element.find('.answer').children(':first').removeAttr('required');
                }
            } else {
                if (val && $.isArray(val) && ($.inArray(parentKey, val) !== -1)) {
                    element.show();
                } else {
                    if (parentValue == parentKey) {
                        element.show();
                    } else {
                        element.hide();
                        element.find('.answer').children(':first').removeAttr('required');
                    }
                }

            }
//                var elementType = element.attr('data-input-type');
//                console.log('elementType', elementType);
//                if (elementType == 'radio') {
//                    console.log('triggered' + elementType);
//                    element.find(':input').trigger('change');
//                } else if (elementType == 'checkbox') {
//                    console.log('triggered' + elementType);
//                    element.find(':input').trigger('change');
//                } else if (elementType == 'select') {
//                    console.log('triggered' + elementType);
//                    element.find('.answer select').trigger('change');
//                }
//                    }

        }
    }



    // get file size while uploading file..................
    $(document).on('change', 'input[type=file]', function () {
        if ($('.bfd-files').find('.bfd-info').length == 1) {
            $(".bfd-dropfield, .bfd-dropfield-inner").off('click');
            $('.bfd-dropfield, .bfd-dropfield-inner').css('cursor', 'not-allowed');
        }
        $('.fileNameSaver').removeClass('hide');
        if ($(this)[0].files[0]) {
            var iSize = ($(this)[0].files[0].size / 1024);
            if (iSize / 1024 > 1)
            {
                if (((iSize / 1024) / 1024) > 1)
                {
                    iSize = (Math.round(((iSize / 1024) / 1024) * 100) / 100);
                    $("#lblSize").html(iSize + "Gb");
                } else
                {
                    iSize = (Math.round((iSize / 1024) * 100) / 100)
                    $("#lblSize").html(iSize + "Mb");
                }
            } else
            {
                iSize = (Math.round(iSize * 100) / 100)
                $("#lblSize").html(iSize + "kb");
            }
        }
    });
    // File name validation in file uploader modal.................
    $(document).on('keyup change', '.bfd-files input', function () {
        $(this).closest('.inner-left').find('.error-alert').remove();
        $('.bfd-ok').removeAttr('disabled');
        var inputs = $(".file-name-input input");
        for (var i = 0; i < inputs.length; i++) {
            if ($(this).val() && $('#newName').val() == $(inputs[i]).val()) {
                $('.bfd-ok').prop("disabled", "true");
                $(this).closest('.inner-left').append('<label class="error error-alert modal-error">This name already exists.</label>');
                return;
            } else if (!$(this).val()) {
                $(this).closest('.inner-left').append('<label class="error error-alert modal-error">This field is required.</label>');
                $('.bfd-ok').prop("disabled", "true");
            }
        }
    });
    // check file name validation in files table..............
    $(document).off('keyup change', 'td.file-name-input input');
    $(document).on('keyup change', 'td.file-name-input input', function () {

        let rowIndex = $(this).closest('.fileUploadRow').attr('data-row-index')
        let $parentRow = $('tr[data-row-index="' + rowIndex + '"]');
        $parentRow.next('.file-name-error').remove();
        $parentRow.find('.save-file').css('pointer-events', 'all');
        $parentRow.closest('.service-preview').find('.save-new-answers').css('pointer-events', 'all');
        var inputs = $(".file-name-input input");
        for (var i = 0; i < inputs.length; i++) {
            if (i != rowIndex) {
                if ($(this).val() && $(this).val() == $(inputs[i]).val()) {
                    $parentRow.find('.file-name-error').remove();
                    $parentRow.find('.save-file').css("pointer-events", "none");
                    $parentRow.closest('.service-preview').find('.save-new-answers').css("pointer-events", "none");
                    $parentRow.after('<label class="error file-name-error">This name already exists.</label>');
                    return;
                } else if (!$(this).val()) {

                    $parentRow.next('.file-name-error').remove();
                    $parentRow.after('<label class="error file-name-error">This field is required.</label>');
                    $parentRow.closest('.service-preview').find('.save-new-answers').css("pointer-events", "none");
                    $parentRow.find('.save-file').css("pointer-events", "none");
                }
            }
        }
    });
    // File uploader.......................
    $(document).off('click', '.open_button');
    $(document).on('click', '.open_button', function () {



        let $this = $(this);

        let fileTypeKey = $this.closest('.question-answer').find('select.file-type-dropdown:last').parent().attr('data-key-name');
        let fileNameKey = $this.closest('.question-answer').find('table.service-files-table').find('tr:last td:first').attr('data-key-name');
        if (!fileNameKey) {
            fileNameKey = 'file_name';
        }
        let ajaxUploadDocument = "{{route('frontend.client.serviceUploadFile', config('constant.subdomain'))}}";
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
            var topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
        if (serviceId == annualServivceId || serviceId == semiAnnualServiceId) {
            var dropDownList = '<select data-key="file_name" disabled="disabled" class="file-type-dropdown" style="display: none;">' +
                    '<option value="401(k)">401(k)</option>' +
                    '<option value="SEP">SEP</option>' +
                    '<option value="SRA Account">SRA Account</option>' +
                    '</select>';
        } else if (serviceId == lifeInsuranceServiceId) {
            var dropDownList = '<select data-key="file_name" disabled="disabled" class="file-type-dropdown" style="display: none;">' +
                    '<option value="user_name">' + userName + '</option>' +
                    '<option value="spouse_name">Spouse Name</option>' +
                    '</select>';
        } else {
            var dropDownList = '';
        }
        var fileTypeKeyVar = '';
        if (fileTypeKey) {
            fileTypeKeyVar = 'data-key-name="' + fileTypeKey + '"';

        }
        $.FileDialog({multiple: false}).on('files.bs.filedialog', function (ev) {
            var numbersOfTr = $('.fileUploadRow').length,
                    files = ev.files,
                    formData = new FormData(),
                    i = 0;
            if (files.length != 0) {
                files.forEach(function (f) {
                    let dropDownEl = '';
                    if (serviceId != stockServiceId && dropDownList != '') {
                        dropDownEl = '<td class="file-select-type" ' + fileTypeKeyVar + '></td>'
                    }
                    let newTr = '<tr class="fileUploadRow" data-row-index="' + numbersOfTr + '"> \
                                    <td class="file-name-input" data-key-name="' + fileNameKey + '"><input type="text" value=""  disabled="disabled" ></td> \
                                    ' + dropDownEl + ' \
                                    <td class="file-download text-center"></td> \
                                    <td class="text-center file-action"><i class="fa fa-check save-file"></i></td> \
                                </tr>'
                    $this.closest('.question-answer').find('table.service-files-table').append(newTr);
                    formData.append("file", f, f.name);
                    i++;
                });
            } else {
                toastr.warning(' Please Upload Single File.');
                return false;
            }
            if (dropDownList.length > 0) {
                $('tr[data-row-index="' + numbersOfTr + '"] .file-select-type').append(dropDownList)
                $('tr[data-row-index="' + numbersOfTr + '"] .file-select-type select').show();
            }
            $this.after('<div class="lds-dual-ring"></div>');

//                var clientId = $('#client-id').val();
            $.ajax({
                type: "post",
                url: ajaxUploadDocument + '/' + clientId,
                async: true,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                timeout: 60000,
                success: function (response) {
                    $('.lds-dual-ring').remove();

                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-name-input input')
                            .removeAttr("disabled", "")
                            .css({'border-bottom': '1px solid #ccc', 'background': '#fff'});
                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-select-type select')
                            .removeAttr("disabled", "")
                            .css({'width': '80%', 'background': '#fff'});
                    $('tr[data-row-index="' + numbersOfTr + '"] td.file-action')
                            .append('<input type="hidden" name="file_path" class="new-file-path" value="' + response.message + '">')
                    $this.closest('.service-preview').find('.save-new-answers, .active-inputs').hide();
                    $this.closest('.question-answer').find('.save-file').removeClass('hide');
                },
                xhr: function () {
                    var myXhr = $.ajaxSettings.xhr();
                    return myXhr;
                },
                error: function (error) {
                    console.log(error);
                }
            });
        }).on('cancel.bs.filedialog', function (ev) {
//                     alert('here');
        });
        $('.bfd-dropfield-inner').attr('id', 'flUpload');
        $('.bfd-files').append("<span class='inner-left' style='position:relative;'><span class='col-xs-12 paddingLeft0 paddingRight0'> <input class='form-control fileNameSaver hide' id='newName' required='required' placeholder='Enter File Name'></span><label id='lblSize' class='file-size'></label></span>");
        if ($('.fileNameSaver').val() == "") {
            $('.bfd-ok').prop("disabled", "true");
        }
        $('.file-size').text('');
          $(document).on('click', '.modal-footer .bfd-ok', function () {
        $('.fileUploadRow:last td.file-name-input input').val($('.fileNameSaver').val());
        if (topicId == incomeTaxTopicId) {
            if ($('tr.fileUploadRow').length != 0) {
                $('.add-file-btn').hide();
            }
        }
    });
    });
  
    // File upload action.........
    $(document).off('click', '.save-file');
    $(document).on('click', '.save-file', function () {

        $this = $(this).closest('tr');
        let filePath = $this.find('td.file-action .new-file-path').val();
        if (!filePath) {
            toastr.warning('Upload Single File.');
return false;
        }
        let fileName = $this.find('td.file-name-input input').val();

        let fileNameKey = $this.find('td.file-name-input').attr('data-key-name');
        let fileType = $this.find('td.file-select-type select.file-type-dropdown').val();
        let fileTypeKey = $this.find('td.file-select-type').attr('data-key-name');
        let questionId = $('#fileParentHidden').attr('name');
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
        let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
        let clientId = $('#client-id').val();
        let filesArray = [{}]


        if (topicId == incomeTaxTopicId) {
            filesArray[0]['file_name'] = fileName;

        } else {
            filesArray[0][fileNameKey] = fileName;
        }
        filesArray[0]['file_path'] = filePath;
        if (fileType) {
            filesArray[0][fileTypeKey] = fileType;
        }

        let ajaxUrl = "/addFiles/" + serviceId + "/" + clientId;
        if (topicId) {
            ajaxUrl = "/addFiles/" + serviceId + "/" + clientId + "/" + topicId;
        }

        // Ajax request init
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: ajaxUrl,
            data: {questionId: questionId, data: filesArray},
            success: function (data) {
                if (data.success == true) {
                    toastr.success('File uploaded successfully.');
                    if (fileType) {
                        filesArray[0][fileTypeKey] = fileType;
                        $this.find('td.file-select-type select.file-type-dropdown')
                                .attr('disabled', 'disabled')
                                .css('background', 'transparent').selectBoxIt();
                    }
                    $this.find('td.file-name-input input')
                            .attr('disabled', 'disabled')
                            .css({'border-bottom': 'none', 'background': 'transparent'});
                    $this.find('td.file-download')
                            .append('<a download="' + filePath + '" href="files/' + clientId + '/' + filePath + '"> <i class="fa fa-download"></i></a>')
                    $this.find('td.file-action .fa')
                            .removeClass('fa-check save-file')
                            .addClass('fa-trash delete-file');
                    $this.closest('.service-preview').find('.active-inputs').show();
                } else {
                    toastr.warning('Something went wrong.');
                }
            }
        });
    });
    // delete file action..............
    $(document).off('click', '.delete-file');
    $(document).on('click', '.delete-file', function () {
        $this = $(this).parent();
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                let dataIndex = $this.closest('tr').attr('data-row-index');
                let questionId = $('#fileParentHidden').attr('name');
                let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
                let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
                let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteFiles/" + serviceId + "/" + clientId;
                if (topicId) {
                    ajaxUrl = "/deleteFiles/" + serviceId + "/" + clientId + "/" + topicId;
                }

                // Ajax request init
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionId: questionId, fileIndex: dataIndex},
                    success: function (data) {
                        if (data.success == true) {
                            if (topicId == incomeTaxTopicId) {
                                if ($('tr.fileUploadRow').length != 0) {
                                    $('tr[data-row-index="' + dataIndex + '"]')
                                            .closest('.question-answer')
                                            .find('.add-file-btn').show();
                                }
                            }
                            toastr.success('File deleted successfully.');
                            $('tr[data-row-index="' + dataIndex + '"]').remove();
                            $('tr.fileUploadRow').each(function (index, childElement) {
                                $(childElement).attr('data-row-index', index); // update table rows indexes
                            })
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        });
    });
    $(document).off('change', '.upload-statement input[type=file]');
    $(document).on('change', '.upload-statement input[type=file]', function () {
        let $this = $(this);
        let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
        let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
        let clientId = $('#client-id').val();
        let questionId = $this.closest('.question-answer').find('.fileParentHidden').attr('name');
        $this.after('<div class="lds-dual-ring"></div>');
        let uploadStatementFile = new FormData();
        uploadStatementFile.append('file', $this[0].files[0]);
        $.ajax({
            type: "post",
            url: ajaxUploadDocument + '/' + clientId,
            async: true,
            data: uploadStatementFile,
            cache: false,
            contentType: false,
            processData: false,
            timeout: 60000,
            success: function (response) {
                $('.lds-dual-ring').remove();
                let filePath = "files/" + clientId + "/" + response.message;
                let filesArray = new Object();
                filesArray['file_name'] = $this[0].files[0].name;
                filesArray['file_path'] = response.message;
                $this.closest('.question-answer').find('.fileParentHidden').val(JSON.stringify(filesArray));
//                    $this.closest('.upload-statement').find('.upload-statement-button, .modal-upload-file-input').hide();
                let fileName = '<span>' + response.message + '</span>';
                $this.closest('.upload-statement').find('table.uploaded-modal-file').removeClass('hide')
                $this.closest('.upload-statement').find('table.uploaded-modal-file').find('td.modal-file-name').html(fileName);
            },
            xhr: function () {
                var myXhr = $.ajaxSettings.xhr();
                return myXhr;
            }

        });
    });
    $(document).off('click', '.upload-statement .delete-modal-file')
    $(document).on('click', '.upload-statement .delete-modal-file', function () {
        $this = $(this);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this file!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                let questionId = $this.closest('.question-answer').find('.fileParentHidden').attr('name');
                let modalIndex = $this.closest('.service-preview').find('.updateAnswers').attr('data-index')
                let serviceId = $this.closest('.service-preview').find('.updateAnswers').attr('data-service-id');
                let topicId = $this.closest('.service-preview').find('.updateAnswers').attr('data-topic-id');
                let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteSingleFile/" + serviceId + "/" + clientId;
                if (topicId) {
                    ajaxUrl = "/deleteSingleFile/" + serviceId + "/" + clientId + "/" + topicId;
                }
                // Ajax request init
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionId: questionId, answerIndex: modalIndex},
                    success: function (data) {
                        if (data.success == true) {
                            toastr.success('File deleted successfully.');
                            $this.closest('.upload-statement').find('table.uploaded-modal-file').addClass('hide')
                            $this.closest('.upload-statement').find('table.uploaded-modal-file').find('td.modal-file-name').html('');
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        })
    });

    $(document).off('click', '.addMoreRow')
    $(document).on('click', '.addMoreRow', function () {
        let tableElement = $(this).closest('.question-answer').find('.contingent-beneficiary-table');

        let tableRow = $(this).closest('.question-answer').find('.contingent-beneficiary-table tbody tr:first-child').clone();
        tableElement.find('tbody').append(tableRow);
        tableElement.find('tbody tr:last-child').find(':input').val('');
        $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
            $(childElement).find('td.deleteRow').attr('data-index', index); // update table rows indexes
        });
        $(".question_yearpicker").datetimepicker({
            viewMode: "years",
            format: "YYYY",
            useCurrent: true,
        });
    });

    $(document).off('click', '.deleteRow')
    $(document).on('click', '.deleteRow', function () {
        var $this = $(this);
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {

                var deleteIndex = $this.attr('data-index');
                let answerIndex = $this.closest('.service-preview').find('.updateAnswers').attr('data-index')

                var questionIds = $this.attr('data-question-ids');
                let clientId = $('#client-id').val();
                let ajaxUrl = "/deleteTableData";
                $.ajax({
                    type: 'post',
                    dataType: 'json',
                    url: ajaxUrl,
                    data: {questionIds: questionIds, answerIndex: answerIndex, clientId: clientId, deleteIndex: deleteIndex},
                    success: function (data) {
                        if (data.success == true) {
                            toastr.success('Deleted successfully.');
                            $this.parent().remove();
                            $('.contingent-beneficiary-table tbody tr').each(function (index, childElement) {
                                $(childElement).find('td.deleteRow').attr('data-index', index); // update table rows indexes
                            });
                        } else {
                            toastr.warning('Something went wrong');
                        }
                    }
                });
            }
        })
    });
    $(document).off('click', '.signDocument');

    $(document).on('click', '.signDocument', function () {
        var id = $(this).attr('data-id');
        let clientId = $('#client-id').val();

        $.ajax({
            type: 'post',
            url: '/signDocument',
            dataType: 'json',
            data: {id: id, clientId: clientId},
            success: function (data) {
                console.log(data);
            }
        });
    });
//        third party js code
    var childrenList = [];
    if ($('#nonRentalMortgagesData').val() != '') {
        childrenList = JSON.parse($('#nonRentalMortgagesData').val());
    }
    $('.finishDRMortgage').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid()) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($(this).hasClass('edit-form')) {
                    childrenList[$(this).attr('data-index')] = {
                        authorized_third_party: $('#propertyModal .property-name1').val(),
                        mailing_address: $('#propertyModal .property-name2').val(),
                        telephone: $('#propertyModal .property-name3').val()

                    };
                    $('#property-info tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#propertyModal .property-name1').val() + '</td><td valign="top"> </td><td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                } else {
                    $('#property-info tbody').append('<tr data-index=' + childrenList.length + '><td>' + $('#propertyModal .property-name1').val() + '</td><td valign="top"> </td><td valign="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    childrenList.push({
                        authorized_third_party: $('#propertyModal .property-name1').val(),
                        mailing_address: $('#propertyModal .property-name2').val(),
                        telephone: $('#propertyModal .property-name3').val()

                    });
                }
                $('#nonRentalMortgagesData').html(JSON.stringify(childrenList));
                $('#propertyModal').modal('hide');
            }

        }
        return false;
    });


    $(document).on('click', '#property-info .fa-pencil', function () {
        $('#propertyModal').modal();
        $('#step-1').show();
        $('#step-2, #step-3, #step-4, #step-5').hide();
        var index = $(this).closest('tr').attr('data-index');
        childrenList = JSON.parse($('#nonRentalMortgagesData').val());
        propertyDetails = childrenList[index];
        $('#propertyModal .property-name1').val(propertyDetails.authorized_third_party);
        $('#propertyModal .property-name2').val(propertyDetails.mailing_address);
        $('#propertyModal .property-name3').val(propertyDetails.telephone);

        $('#finishDRMortgagenew ').addClass('edit-form');
        $('#finishDRMortgagenew').attr('data-index', index);
    });
    $(document).on('click', '.add-mortgage', function () {
        $('#step-1').show();
        $('#step-2, #step-3, #step-4, #step-5').hide();
        $('#finishDRMortgagenew').removeClass('edit-form');
        $('#propertyModal input[type="text"], #propertyModal input').val('');
    });
    $(document).on('click', '#property-info .fa-trash', function () { // <-- changes
        var index_id = $(this).closest('tr').attr('data-index');

        deleteRow(index_id);
        $('.swal-button--danger').click(function () {

            childrenList.splice(index_id, 1);
            $("#property-info tbody").empty();
            if (childrenList.length != 0) {
                var tr = '';
                $.each(childrenList, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.authorized_third_party + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#property-info tbody").html(tr);
            }

            $('#nonRentalMortgagesData').html(JSON.stringify(childrenList));
            return false;
        });
    });

//third party js code ends
//account code js 
    var childrenList1 = [];

    if ($('#nonRentalMortgagesData1').val() != '') {
        childrenList1 = JSON.parse($('#nonRentalMortgagesData1').val());
    }
    $('.finishDRMortgage1').on('click', function () {
        var $this = $(this);
        var form = $this.closest('form');
        if (form.valid()) {
            var nextDiv = $this.closest('.setup-content').next('.setup-content');
            if (nextDiv.length) {
                nextDiv.siblings().hide();
                nextDiv.show();
            } else {
                if ($(this).hasClass('edit-form')) {
                    childrenList1[$(this).attr('data-index')] = {
                        account_number: $('#propertyModal1 .property-name1').val(),
                        registration_name: $('#propertyModal1 .property-name2').val(),
                        custodian_name: $('#propertyModal1 .property-name3').val(),
                        fee: $('#propertyModal1 .property-name4').val(),
                        discrtion: $('#propertyModal1 .property-name5').val(),
                        non_discrtion: $('#propertyModal1 .property-name6').val(),
                        debit: $('#propertyModal1 .property-name7').val(),
                        bill: $('#propertyModal1 .property-name8').val()

                    };
                    $('#property-info1 tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#propertyModal1 .property-name1').val() + '</td><td valign="top"> </td><td valign="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td>');
                } else {
                    $('#property-info1 tbody').append('<tr data-index=' + childrenList1.length + '><td>' + $('#propertyModal1 .property-name1').val() + '</td><td valign="top"> </td><td valign="right"><i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    childrenList1.push({
                        account_number: $('#propertyModal1 .property-name1').val(),
                        registration_name: $('#propertyModal1 .property-name2').val(),
                        custodian_name: $('#propertyModal1 .property-name3').val(),
                        fee: $('#propertyModal1 .property-name4').val(),
                        discrtion: $('#propertyModal1 .property-name5').val(),
                        non_discrtion: $('#propertyModal1 .property-name6').val(),
                        debit: $('#propertyModal1 .property-name7').val(),
                        bill: $('#propertyModal1 .property-name8').val()

                    });
                }
                $('#nonRentalMortgagesData1').html(JSON.stringify(childrenList1));
                $('#propertyModal1').modal('hide');
            }

        }
        return false;
    });
    $(document).on('click', '#property-info1 .fa-pencil', function () {
        $('#propertyModal1').modal();
        $('#step-1').show();
        $('#step-2, #step-3, #step-4, #step-5').hide();
        var index = $(this).closest('tr').attr('data-index');
        childrenList1 = JSON.parse($('#nonRentalMortgagesData1').val());
        propertyDetails1 = childrenList1[index];
        $('#propertyModal1 .property-name1').val(propertyDetails1.account_number);
        $('#propertyModal1 .property-name2').val(propertyDetails1.registration_name);
        $('#propertyModal1 .property-name3').val(propertyDetails1.custodian_name);
        $('#propertyModal1 .property-name4').val(propertyDetails1.fee);
        $('#propertyModal1 .property-name5').val(propertyDetails1.discrtion);
        $('#propertyModal1 .property-name6').val(propertyDetails1.non_discrtion);
        $('#propertyModal1 .property-name7').val(propertyDetails1.debit);
        $('#propertyModal1 .property-name8').val(propertyDetails1.bill);

        $('#finishDRMortgagenew1 ').addClass('edit-form');
        $('#finishDRMortgagenew1').attr('data-index', index);
    });
    $(document).on('click', '.add-mortgage1', function () {
        $('#step-1').show();
        $('#step-2, #step-3, #step-4, #step-5').hide();
        $('#finishDRMortgagenew1').removeClass('edit-form');
        $('#propertyModal1 input[type="text"], #propertyModal1 input[type="number"]').val('');
    });
    $(document).on('click', '#property-info1 .fa-trash', function () { // <-- changes
        var index_id = $(this).closest('tr').attr('data-index');

        deleteRow(index_id);
        $('.swal-button--danger').click(function () {

            childrenList1.splice(index_id, 1);
            $("#property-info1 tbody").empty();
            if (childrenList1.length != 0) {
                var tr = '';
                $.each(childrenList1, function (key, value) {
                    tr += '<tr data-index="' + key + '"><td>' + value.account_number + '</td><td></td><td align="right"> <i title="Edit" class="fa fa-pencil" aria-hidden="true"></i><i title="Delete" class="fa fa-trash" aria-hidden="true"></i></td></tr>';
                });
                $("#property-info1 tbody").html(tr);
            }

            $('#nonRentalMortgagesData1').html(JSON.stringify(childrenList1));
            return false;
        });
    });
    $('.prev-btn-link').css('display', 'none');
    setTimeout(function () {
        $('.alert-success').fadeOut('fast');
    }, 3000);

    $(document).off('keyup', '.TaxableIncome');
    $(document).on('keyup', '.TaxableIncome', function () {
        $this = $(this);
        var taxFilling = $(this).val();
        if (taxFilling) {
            getTaxBracket(taxFilling, $this);
        }


    });
    function getTaxBracket(taxFilling, $this) {
        let clientId = $('#client-id').val();

        $.ajax({
            type: 'GET',
            url: 'taxBracket/' + clientId,
            data: {
                taxableIncome: taxFilling
            },
            success: function (resp) {
                $this.closest('.questions-list').find('.TaxBracket').val(resp);
                $('.service-preview-file').find('.TaxBracket').val(resp);
                $('.lds-dual-ring').hide();
            }
        });
    }
});

</script>
