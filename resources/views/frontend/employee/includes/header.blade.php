<div class="client-header col-sm-12">
    <div class="client-list-naviagtion" style="display:none;">
        <a href="javascript:void(0)" class="pull-left">
            <i class="fa fa-arrow-left"></i>Client list
        </a>
    </div>
    <div class="heading pull-left">Welcome to TWPM </div>
    <ul class="actions-list list-unstyled pull-right list-inline">
<!--                <li id="noti-open">
                    <i class="fa fa-bell-o"></i>
                    <span class="noti-red hide"></span>
                </li>-->

    <li id="noti-open">
            <i class="fa fa-bell-o"></i>  
            <span class="noti-red hide"></span>
            <div id="notificationContainer">
                <div id="notificationTitle">
                    <span>Notifications</span>
                    <span class="out">
                        <img src="/img/logout.png">
                    </span></div>
                <div id="notificationsBody" class="notifications">
                    <div class="list-inline">
                        <a href='#allNotifications' class="notification-type active" >All</a>
                        <a href='#unreadNotifications' class="notification-type" >Unread</a>
                    </div>
                    <ul id="allNotifications" class="notifications-list" style="display:block;">

                    </ul>
                    <ul id="unreadNotifications" class="notifications-list" style="display: none;">
                        
                    </ul>
                </div>
                <!--<div id="notificationFooter"><a href="#">See All</a></div>-->
            </div>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ Session::get('loggedInUserName') }} <i class="fa fa-angle-down"></i></a>
            <ul class="dropdown-menu">
                <!--<li><a href="{{-- route('frontend.client.profile', config('constant.subdomain')) --}}"> <i class="fa fa-user"></i> My Profile</a></li>-->
                <!--<li><a href="#"> <i class="fa fa-question-circle"></i> Help</a></li>-->
                <li><a href="{{ route('frontend.auth.logout') }}"> <i class="fa fa-sign-out"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</div>
<script>

</script>
