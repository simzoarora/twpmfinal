@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/profile.css')) }}   
@stop

@section('content')
<div class="dashboard"> 

    <div id="dashboard-content">
        <div class="container-fluid">

            @include('frontend.includes.employee_sidebar')

            <div class="right-content">
                @include('frontend.includes.client_header')
                EMployee Home

             


            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script>
    var changePasswordRoute = '{{route("frontend.auth.password.change")}}';
</script>
<script src="{{ asset('js/dashboard.js') }}"></script>
<script src="{{ asset('js/client-profile.js') }}"></script>
@stop