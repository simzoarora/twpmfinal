@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/profile.css')) }}   
{{ Html::style(asset('css/documents.css')) }} 
@stop

@section('content')
<div class="dashboard"> 
    <div id="dashboard-content">
        <div class="container-fluid">

            <?php $activeClass2 = 'active'; ?>
            @include('frontend.includes.employee_sidebar')

            <div id="clients-list">
                <i class="fa fa-search"></i>
                <input type="text" name="search" placeholder="Click to Search"/>

                <ul class="list-unstyled">
                    @include('frontend.employee.includes.clientListItems', ['all_clients' => $all_clients])
                </ul>
            </div>

            <div class="right-content col-sm-12">
                @include('frontend.employee.includes.header')
                <input type="hidden" id="auth_user_id" value="{{Auth::user()->id}}">
                @include('includes.partials.messages')
                <div class="profile_inner profile-lists"></div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')

<script>
    var getAllClientsRoute = '{{route("frontend.employee.getAllClients","text")}}',
            getClientDetailsRoute = '{{route("frontend.employee.clientDetails","text")}}';
</script>
 <!--<script src="{{ asset('js/sign-up-questions.js') }}"></script>--> 
{{ Html::script("js/backend/plugin/pdfobject/pdf.js") }}
{{ Html::script("js/backend/plugin/pdfobject/pdf.worker.js") }}
<script src="{{ asset('js/employee.js') }}"></script>


@stop 