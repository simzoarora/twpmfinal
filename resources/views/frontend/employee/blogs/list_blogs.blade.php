@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop

@section('after-styles') 
{{ Html::style(asset('css/profile.css')) }}   
@stop

@section('content')
<div class="dashboard"> 
    <div id="dashboard-content">
        <div class="container-fluid">

            <?php $activeClass2 = 'active'; ?>
            @include('frontend.includes.employee_sidebar')

            <div class="right-content blogs">
                @include('frontend.employee.includes.header')

                @include('includes.partials.messages')

                <div class="blog-list_inner">
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Articles</h3>

                            <div class="box-tools pull-right">
                                <a href="{{route('frontend.employee.createArticle')}}"><span class="btn btn-primary btn-xs">Add New Article</span></a>
                            </div><!--box-tools pull-right-->
                        </div><!-- /.box-header -->

                        <div class="box-body">
                            <div class="table-responsive">
                                <table id="articles_table" class="table table-bordered table-hover dataTable">
                                    <thead>
                                        <tr>
                                            <th class="w10 text-center">S.No.</th>
                                            <th>Title</th>
                                            <th class="w80 text-center">Publish On</th>
                                            <th class="w10 text-center">Published</th>
                                            <th class="w10 text-center">{{ trans('labels.general.actions') }}</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($articles as $key => $article)
                                        <tr>
                                            <th class="text-center">{{($key+1)}}</th>
                                            <th><?php echo (strlen($article->title) > 75) ? substr(strip_tags($article->title), 0, 75) . '...' : $article->title; ?></th>
                                            <th class="text-center">{{$article->publish_at}}</th>
                                            <th class="text-center">
                                                @if($article->status)
                                                <small class="label bg-green">Yes</small>
                                                @else
                                                <small class="label bg-gray">No</small>
                                                @endif
                                            </th>
                                            <th class="text-center">
                                                <a href="{{route('frontend.employee.editArticle',$article->id)}}" class="btn btn-xs btn-primary"><i title="Edit" class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>
                                                <span data-target="#deleteModal" data-toggle="modal" data-id="{{$article->id}}" class="btn btn-xs btn-danger deleteArticle"><i title="Delete" class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="Delete"></i></span>
                                            </th>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div><!--table-responsive-->
                        </div><!-- /.box-body -->
                    </div><!--box-->
                    <!-- Modals -->
                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                {{ Form::open(['id'=>'deleteArticleForm']) }}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="deleteModalLabel">Delete Article</h4>
                                </div>
                                <div class="modal-body">
                                    Are you sure you want to delete this Article?
                                    {{ Form::hidden('id',null,['id' => 'hiddenArticleId']) }}
                                </div>
                                <div class="modal-footer text-center">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                    {{ Form::submit('Yes', ['class' => 'btn btn-success submit_form']) }}
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection

@section('after-scripts')
<script src="{{ asset('js/employee.js') }}"></script>
<script>
    $(function () {
        $('#articles_table').on('click', '.deleteArticle', function () {
            $('#hiddenArticleId').val($(this).data('id'));
        });

        $('#deleteArticleForm').submit(function (e) {
            e.preventDefault();
        });

        $('.submit_form').on('click', function (e) {
            e.preventDefault();
            $.ajax({
                url: '{{route("frontend.employee.deleteArticle")}}',
                type: 'post',
                data: {
                    id: $('#hiddenArticleId').val()
                },
                success: function (response)
                {
                    window.location.replace('{{route("frontend.employee.articleList")}}');
                },
                error: function (err) {
                }
            });
        });
    });
</script>
@stop