<!--GOAL SPECIFIC AND FINANCIAL PLANNING-->
<!--<div class="service-preview">
    <div class=" topic-section">

        <div class=" ">
            <h3>Goal-Specific Financial Planning</h3>
        </div>
        <div class="col-sm-12 goal-Specific-hide-topics topic-list-wrapper ">
            <ul class="col-sm-6 topic-list">
                <li class="single-topic goal-specific-topic-1">
                    about you and your family
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-2">
                    income & investments
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-3">
                    real estate assets
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-4">
                    goal details
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
                <li class="single-topic goal-specific-topic-5">
                    additional notes
                    <i class="fa fa-angle-right pull-right"></i>
                </li>
            </ul>
        </div>
    </div>


    <div class="goal-specific-about-family question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>About you and your family
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Marital status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="single" name="answer[43][marital_status]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Your age</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[43][your_age]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Do you have children</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radios" name="answer[43][your_age]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="goal-specific-income-investment question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Income & Investments
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][household_gross_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div> 
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[42][tax_filling_status]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][estimated_taxable_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Non retirement investment</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][non_retirement_investment_assets]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Retirement investment</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[42][retirement]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="goal-specific-real-estate question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Real Estate Assets
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Home value</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[41][home_value]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Total of mortgages</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[41][total_of_mortgages]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Net values of properties</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[41][net_values_of_properties]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Annual net rental income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[41][annual_net_rental_income]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    goal details //

    <div class="goal-specific-goal-details question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Goal Details
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>One time expense or annually recurring</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="radio" name="answer[40][one_time_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Need for this one-time expense</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if one time" name="answer[40][need_for_one_time_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Anticipate needing per year for this expense?</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if annual " name="answer[40][per_year_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>How many years</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="if annual" name="answer[40][how_many_years]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>When do you expect to pay</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="date" name="answer[40][expect_pay_for_expense]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Earmarked just for this goal</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[40][investment_assets_earmarked]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>





    <div class="goal-specific-additional-notes question-section" style="left: 200%;">
        <div class="topic-heading" >
            <i class="fa fa-angle-left pull-left back-to-topics"></i>
            <h3>Goal Details
                <i class="fa fa-angle-right pull-right"></i>
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>One time expense or annually recurring</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="textarea" name="answer[39][additional_notes]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>



stock option exercise strategy and executive Compensation Analysis //
<div class="service-preview">
    <div class="stock-option-question-section" style="left: 200%;">
        <div class="topic-heading" >
            <h3>Stock Option Exercise Strategy and Executive Compensation Analysis
            </h3>
        </div>
        <div class="abc col-sm-6" >
            <div class="question-list-wrapper">
                <ul class="questions-list">
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Household gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[26][household_gross_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Gross income</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[26][estimated_taxable_income]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax filling status</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="select" name="answer[26][tax_filling_status]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Tax bracket</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[26][tax_bracket]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Value of other non-retirement investment account</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[26][non_retirement_investment_account]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Name of the employer</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[25][name_of_the_employer]">
                        </div>
                    </li>
                    <li class="question-answer">
                        <div class="question-label col-sm-6" >
                            <p>Company stock symbol</p>
                        </div>
                        <div class="answer col-sm-6" >
                            <input type="text" value="dummy" name="answer[25][company_stock_symbol]">
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>-->






<script>

    $(document).ready(function () {
        $(".service-options").owlCarousel({
            responsiveClass: true,
            navText: ["<i class='fa fa-chevron-left slide-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right slide-right' aria-hidden='true'></i>"],
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                767: {
                    items: 2,
                    nav: false
                },
                991: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });










        //goal specific financial planning // 


        //about family //
        $(document).on('click', '.goal-specific-topic-1', function () {
            $('.goal-specific-about-family').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-about-family').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });

        //income and investment //
        $(document).on('click', '.goal-specific-topic-2', function () {
            $('.goal-specific-income-investment').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-income-investment').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });


        //real estate //
        $(document).on('click', '.goal-specific-topic-3', function () {
            $('.goal-specific-real-estate').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-real-estate').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });


        //goal details//
        $(document).on('click', '.goal-specific-topic-4', function () {
            $('.goal-specific-goal-details').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-goal-details').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);
        });

        //        additional notes//
        $(document).on('click', '.goal-specific-topic-5', function () {
            $('.goal-specific-additional-notes').animate({left: '0'});
            $('.goal-Specific-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.goal-specific-additional-notes').animate({left: '200%'});
            $('.goal-Specific-hide-topics').show(300);

        });



        //        annual portfolio //

        $(document).on('click', '.annual-portfolio-topic-1', function () {
            $('.annual-portfolio-confirm-income').animate({left: '0'});
            $('.annual-portfolio-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.annual-portfolio-confirm-income').animate({left: '200%'});
            $('.annual-portfolio-hide-topics').show(300);
        });


        //        debt/liabilities//
        $(document).on('click', '.annual-portfolio-topic-2', function () {
            $('.annual-portfolio-debt-liabilities').animate({left: '0'});
            $('.annual-portfolio-hide-topics').hide(300);
        });
        $(document).on('click', '.back-to-topics', function () {
            $('.annual-portfolio-debt-liabilities').animate({left: '200%'});
            $('.annual-portfolio-hide-topics').show(300);
        });



        //        mortgage //
        $(document).on('click', '.annual-portfolio-sub-topic-1', function () {
            $('.annual-portfolio-mortgage').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-mortgage').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });

        //        heloc//
        $(document).on('click', '.annual-portfolio-sub-topic-2', function () {
            $('.annual-portfolio-heloc').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-heloc').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });


        //        auto loans //
        $(document).on('click', '.annual-portfolio-sub-topic-3', function () {
            $('.annual-portfolio-auto-loans').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-auto-loans').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });


//        credit cards //
        $(document).on('click', '.annual-portfolio-sub-topic-4', function () {
            $('.annual-portfolio-credit-cards').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-credit-cards').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });


//student loan //
        $(document).on('click', '.annual-portfolio-sub-topic-5', function () {
            $('.annual-portfolio-student-loan').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-student-loan').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });
     

//other debts //
$(document).on('click', '.annual-portfolio-sub-topic-6', function () {
            $('.annual-portfolio-other-debts').animate({left: '0'});
            $('.annual-portfolio-debt-liabilities').hide(300);
        });
        $(document).on('click', '.back-to-annual-sub-topics', function () {
            $('.annual-portfolio-other-debts').animate({left: '200%'});
            $('.annual-portfolio-debt-liabilities').show(300);
        });


    });

</script>