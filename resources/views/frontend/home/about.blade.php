@extends('frontend.layouts.master')

@section('title')
{{$aboutPageContent['seo_title']}}
@stop

@section('meta_description')
{{$aboutPageContent['seo_description']}} 
@stop

@section('after-styles') 
{{ Html::style(asset('css/about.css')) }}
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $aboutPageContent['seo_title']; ?>">
<meta itemprop="description" content="<?php echo $aboutPageContent['seo_description']; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@TWPM">
<meta name="twitter:title" content="<?php echo $aboutPageContent['seo_title']; ?>">
<meta name="twitter:description" content="<?php echo $aboutPageContent['seo_description']; ?>">
<meta name="twitter:creator" content="@TWPM">
<meta name="twitter:image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $aboutPageContent['seo_title']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/backend/aboutpage/' . $aboutPageContent['seo_img']); ?>" />
<meta property="og:description" content="<?php echo $aboutPageContent['seo_description']; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="1713884215569225"/> 
@stop 

@section('content')
<div class="col-sm-12 about-us">

    <div class="hide-content">
        <div class="header row"> 
            <div class='container page-heading'> 
                <div class="home-heading">{!!$aboutPageContent['slide_1_heading']?$aboutPageContent['slide_1_heading']:''!!}</div>
                <div class="tag-line"> 
                    <p>We're here to serve you, not to sell you.</p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="headline container nav-change">

                <div class="row">
                    <div class="col-sm-12 service-oriented-wrapper">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="headline-heading">{!!$aboutPageContent['slide_2_heading']?$aboutPageContent['slide_2_heading']:''!!}</div>
                            </div>
                            <div class="col-sm-8">
                                <p>{!!$aboutPageContent['slide_2_description']?$aboutPageContent['slide_2_description']:''!!}</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="our-mission row">
        <div class="hide-content">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-12 our-process-wrapper">
                        <div class="row">
                            <div class="col-sm-4 ">
                                <div class="our-mission-heading">{!!$aboutPageContent['slide_3_heading']?$aboutPageContent['slide_3_heading']:''!!}</div>
                            </div>
                            <div class="col-sm-8">
                                <div class="our-mission-description">{!!$aboutPageContent['slide_3_description']?$aboutPageContent['slide_3_description']:''!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row over-hidden">
            <div id="team-member-outer">
                <div id="all-team-members">
                    <div class="team-list container">
                        <div class="row">
                            <div class="col-sm-12 our-team-wrapper">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <h3 class='text-center'>Meet Your <br>Executive Team</h3>
                                    </div>

                                    <div class="col-sm-8">
                                        <?php foreach ($employees as $employee) { ?>
                                            <div class="col-sm-4 single-employee">
                                                <div class='inner-div'>
                                                    {!! HTML::image('img/backend/aboutpage/employees/'.$employee->image,'Team Member',array('class'=>'img-responsive')) !!}
                                                    <h4>{{$employee->name}} <span class='pull-right show-employee-details'><i class="fa fa-ellipsis-v" aria-hidden="true"></i></span></h4>
                                                    <p>{{$employee->title}}</p>
                                                </div>
                                                <div class='employee-details'>
                                                    <div class='employee-header'>
                                                        <h4>{{$employee->name}}</h4>
                                                        <h5>{{$employee->title}}</h5>
                                                        <button type="button" class="close hide-employee-details"><span aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class='employee-body'>
                                                        <p>
                                                            {{strip_tags($employee->description)}}
                                                        </p>
                                                    </div>
                                                    <div class='employee-footer'>
                                                        <div class='col-xs-6 text-center'>
                                                            <a class="full-bio-check">Full Bio</a>
                                                            <div class="full-employee-details" style="display:none;">
                                                                <div class="col-sm-12 full-bio">
                                                                    <div class="container">
                                                                        <a class="back-arrow"><span class="services-arrow-left"></span>Back to Team</a>
                                                                        <div class="col-sm-3">
                                                                            {!! HTML::image('img/backend/aboutpage/employees/'.$employee->image,'Team Member',array('class'=>'img-responsive')) !!}
                                                                        </div>
                                                                        <div class="col-sm-9">
                                                                            <h1>{{$employee->name}}</h1>
                                                                            <h4>{{$employee->title}}</h4>
                                                                            <div class="contact-options">
                                                                                @if($employee->email)
                                                                                <a href="mailto:{{$employee->email}}" target="_top"><i class="fa fa-envelope"></i></a>
                                                                                @endif
                                                                                @if($employee->phone)
                                                                                <a title="tel:{{$employee->phone}}"><i class="fa fa-phone"></i></a>
                                                                                @endif
                                                                                @if($employee->linkedin)
                                                                                <a href="{{$employee->linkedin}}"><i class="fa fa-linkedin"></i></a>
                                                                                @endif
                                                                                @if($employee->twitter)
                                                                                <a href="{{$employee->twitter}}"><i class="fa fa-twitter"></i></a>
                                                                                @endif
                                                                            </div>
                                                                            <div class="description">
                                                                                {!! $employee->description  !!}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class='col-xs-6 text-center contact-modal-show'>
                                                            <a href='#'>Contact</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="single-team-member" class="full-bio">

            </div>
        </div>
    </div>
    <div class="hide-content">
        <div class="our-mission row">
            <div class="container relative">
                <div class="row">
                    <div class="col-sm-12 our-mission-wrapper">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="our-mission-heading">{!!$aboutPageContent['slide_5_heading']?$aboutPageContent['slide_5_heading']:''!!}</div>
                            </div>
                            <div class="col-sm-8">
                                <div class="our-mission-description">{!!$aboutPageContent['slide_5_description']?$aboutPageContent['slide_5_description']:''!!}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="questions row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-md-4 text-center cta">
                        <div class="questions-heading">{!! isset($footer->heading_subfooter)?$footer->heading_subfooter:''!!}</div>
                        <span> {!! isset($footer->phone)?$footer->phone:''!!}</span>
                    </div>
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <p>Proud supporter of</p>
                        <a href="{{ $aboutPageContent['sponsor_link'] }}" target="_blank">
                            {{ HTML::image('img/backend/aboutpage/'.$aboutPageContent['sponsor_logo']) }}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <hr>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/about.js') }}"></script>
@stop