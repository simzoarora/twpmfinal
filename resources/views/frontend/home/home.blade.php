                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                @extends('frontend.layouts.master')

@section('title')
{{$homePageContent['seo_title']}}
@stop

@section('meta_description')
{{$homePageContent['seo_description']}}
@stop
@section('after-styles')
{{ Html::style(asset('css/home2.css')) }}
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $homePageContent['seo_title']; ?>">
<meta itemprop="description" content="<?php echo $homePageContent['seo_description']; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/backend/homepage/'.$homePageContent['seo_img']); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@TWPM">
<meta name="twitter:title" content="<?php echo $homePageContent['seo_title']; ?>">
<meta name="twitter:description" content="<?php echo $homePageContent['seo_description']; ?>">
<meta name="twitter:creator" content="@TWPM">
<meta name="twitter:image" content="<?php echo URL::asset('img/backend/homepage/'.$homePageContent['seo_img']); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $homePageContent['seo_title']; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/backend/homepage/'.$homePageContent['seo_img']); ?>" />
<meta property="og:description" content="<?php echo $homePageContent['seo_description']; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="1713884215569225"/> 
@stop

@section('content')
<?php

use Carbon\Carbon;

if ($homePageContent) {
    $logonumber = sizeof($homePageContent['logoBox']);
    if ($logonumber > 3) {
        $logonumber = 3;
    }
}
?>

<!--slide1-->
<div class="col-sm-12" id="slide1" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['bg_image_1']; ?>')">
    <div class="black-bg">
        <div class="container">
            <div class="hero-lines">
                <div class="hero-heading"><?php
                    echo $homePageContent['heading_1'] ? $homePageContent['heading_1']
                            : '';
                    ?></div>
                <?php
                echo $homePageContent['subheading_1'] ? '<h3>'.$homePageContent['subheading_1'].'<h3>'
                        : '';
                ?>
            </div>   
        </div>   
        <div class="arrow-down" id="scroll-to-div" data-scrollto="#slide2"></div>
    </div>
</div>
<!--slide2-->
<div class="col-sm-12 nav-change" id="slide2">
    <div class="container">
        <div class="row">
            <?php
            if ($homePageContent) {
                foreach ($homePageContent['logoBox'] as $logoBoxSingle) {
                    ?>
                    <div class="col-sm-3 box">
                        <div class="box-img">
                            {{ HTML::image('img/backend/homepage/logobox/'.$logoBoxSingle['box_logo']) }}
                        </div>
                        <div class="box-title">{{$logoBoxSingle['box_heading']?$logoBoxSingle['box_heading']:''}}</div>
                        <div class="box-description">{{$logoBoxSingle['box_description']?$logoBoxSingle['box_description']:''}}</div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

<div class="col-sm-12" id="slide3">
    <div class="container">
        <div class="row">  
            <div class="col-sm-6 p0">
                <div class="col-sm-9 txt-center">

                    <h2><?php
                        echo $homePageContent['heading_3'] ? $homePageContent['heading_3']
                                : '';
                        ?></h2>
                    <p class="sub-para"> <?php
                        echo $homePageContent['subheading_3'] ? $homePageContent['subheading_3']
                                : '';
                        ?></p>
                    <a class="btn btn-blue" href="{{route('frontend.client.signUp', [config('constant.subdomain')])}}">Get Started</a>
                </div>  
            </div>
            <div class="col-sm-6 search-img">
                {{ HTML::image('img/backend/homepage/'.$homePageContent['image_3'] ,'a pic', array('class' => 'img-responsive')) }}
            </div>
        </div>  
    </div>  
</div>

<!--slide4-->
<div class="col-sm-12" id="slide4">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 left-div">
                <div class="decades-img" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['image_4']; ?>')"></div>
            </div>
            <div class="col-sm-6">
                <div class="right-div">
                    <div class='heading-l'>
                        <?php
                        echo $homePageContent['heading_4'] ? $homePageContent['heading_4']
                                : '';
                        ?>
                    </div>
                    <p>{{$homePageContent['description_4']?$homePageContent['description_4']:''}}</p>
                    <a class="meet-team" href="{{route('frontend.about')}}#ourTeam">{{$homePageContent['linktext_4']?$homePageContent['linktext_4']:''}} 
                        <i class="fa fa-arrow-right" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>  
</div>

@include('frontend.includes.signup_starting_form')

<!--slide6-->
<div class="col-sm-12" id="slide6" style="background-image: url('img/backend/homepage/<?php echo $homePageContent['bg_image_6']; ?>')">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 right-div">
                <h1>{{$homePageContent['heading_6']?$homePageContent['heading_6']:''}}</h1>
                <p>{{$homePageContent['description_6']?$homePageContent['description_6']:''}}</p>
            </div>
        </div>
    </div>
</div>

<!--slide7--> 
<div class="col-sm-12" id="slide7">
    <div class="container">
        <div class="row m-40">
            <h1>{{$homePageContent['heading_7']?$homePageContent['heading_7']:''}}</h1>
            <?php foreach ($articles as $article) { ?>
                <div class="col-sm-4">
                    <a href="{{ route('frontend.single-blog',$article->slug) }}">
                        <div class="commentary-box" style="background-image: url('img/backend/blogs/articles/{{ $article->image }}')">
                            <div class="commentary-bg">
                                <p>
                                    {{Carbon::createFromTimeStamp(strtotime($article->publish_at))->diffForHumans()}}
                                    <?php foreach ($article->blogCategories as $category) { ?>
                                        <span>{{$category->title}}</span>
                                    <?php } ?>
                                </p>
                                <h3>{{$article->title}}</h3>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<!--slide8-->
@include('frontend.includes.contact_bar')

@endsection

@section('after-scripts')
<script src="{{ asset('js/home.js') }}"></script>
@stop