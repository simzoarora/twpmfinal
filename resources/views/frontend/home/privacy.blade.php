@extends('frontend.layouts.master')

@section('title')
Privacy & Security &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(asset('css/privacy.css')) }}
@stop
@section('content')
<div class="col-sm-12" id="privacy">
    <div class="col-sm-12 blue-bg  nav-change">
        <h3>Privacy & Security</h3>
    </div>
    <div class="col-sm-12 privacy-content">
        <div class="container">
            {!! $content !!}
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/privacy.js') }}"></script>
@stop