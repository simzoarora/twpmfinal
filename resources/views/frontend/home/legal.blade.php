@extends('frontend.layouts.master')

@section('title')
Legal Disclosures &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(asset('css/legal.css')) }}
@stop
@section('content')
<div class="col-sm-12" id="legal">
    <div class="col-sm-12 blue-bg  nav-change"> 
        <h3>Legal Disclosures</h3>
    </div> 
    <div class="col-sm-12 legal-content">
        <div class="container">
        {!! $content !!}
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/privacy.js') }}"></script>
@stop
