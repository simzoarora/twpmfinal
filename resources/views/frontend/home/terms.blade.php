@extends('frontend.layouts.master')

@section('title')
Terms of Use &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(asset('css/terms.css')) }}
@stop
@section('content')
<div class="col-sm-12" id="terms">
    <div class="col-sm-12 blue-bg  nav-change">
        <h3>Terms of Use</h3>
    </div>
    <div class="col-sm-12 terms-content">
        <div class="container">
            {!! $content !!}
        </div>
    </div>
</div>
@endsection

@section('after-scripts')
<script src="{{ asset('js/terms.js') }}"></script>
@stop