<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title', app_name())</title>
        <!-- favicons start -->
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />
        <link href="img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
        <link href="img/apple-touch-icon-167x167.png" rel="apple-touch-icon" sizes="167x167" />
        <link href="img/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
        <link href="img/icon-hires.png" rel="icon" sizes="192x192" />
        <link href="img/icon-normal.png" rel="icon" sizes="128x128" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset("img/favicon.png")}}">
        <!-- favicons end -->
        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', '')">
        <meta name="author" content="@yield('meta_author', '')">
        <!--Font-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
        @yield('meta')
        <!-- Styles -->
        @yield('before-styles')
        @yield('after-styles')
        <!-- Scripts -->
        <script>
            window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>
        </script>

        <!--<script src="//sdk.engage.co/sdk.js" data-widget="totalwpm-site"></script>-->
        <script>
//            var engage = new EngageSDK("DBFD9200-6BCF-2219-6609-F2987CA9641C");
//            engage.drawWidget({
//                type: "toolbar",
//                options: {
//                    tabPlacement: "bottom-right-tab",
//                    onlineLabel: "Online! Chat Now!",
//                    offlineLabel: "Meet Our Team!",
//                    backgroundColor: "#00d80d",
//                    labelColor: "#fff"
//                }
//            }, function (toolbar) {
//                toolbar.setVisibility(false);
//                $('.trigger-chat').on('click', function () {
//                    toolbar.setVisibility(true);
//                    toolbar.openDrawer();
//                });
//                $('#get-started-modal').on('hidden.bs.modal', function () {
//                    toolbar.setVisibility(false);
//                });
//            });
//        </script>
<!--        <script type="text/javascript">
            var Chatstack = {server: 'twpmdemo.us', embedded: true};
            (function (d, undefined) {
                // JavaScript
                Chatstack.e = [];
                Chatstack.ready = function (c) {
                    Chatstack.e.push(c);
                }
                var b = d.createElement('script');
                b.type = 'text/javascript';
                b.async = true;
                b.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + Chatstack.server + '/livehelp/scripts/js.js';
                var s = d.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(b, s);
            })(document);
        </script>-->
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                @include('frontend.includes.navbar')
                @yield('content')
                @include('frontend.includes.footer')
            </div>
        </div>
        <!-- Scripts -->
          <script src="{{ asset('js/app.js') }}"></script>
        @yield('before-scripts')

        @yield('after-scripts')

        @include('includes.partials.ga')
        <script>
            @if(Auth::check())
                var userid = {{Auth::user()->id}};
            @endif
        </script>
        <!--<script src="{{ asset('js/sockets.js') }}"></script>-->
    </body>
</html>