<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <meta name="_user_id" content="{{ auth()->check() ? auth()->user()->id : 0 }}">
        <!--<title>@yield('title', app_name())</title>-->
        <title>Total Wealth Planning and Management</title>
        <!-- favicons start -->
        <link href="img/apple-touch-icon.png" rel="apple-touch-icon" />
        <link href="img/apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />
        <link href="img/apple-touch-icon-167x167.png" rel="apple-touch-icon" sizes="167x167" />
        <link href="img/apple-touch-icon-180x180.png" rel="apple-touch-icon" sizes="180x180" />
        <link href="img/icon-hires.png" rel="icon" sizes="192x192" />
        <link href="img/icon-normal.png" rel="icon" sizes="128x128" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon.png')}}?ver=<?php echo Date("Y.m.d.G.i.s") ?>">
        <link rel="shortcut icon" href="{{ asset('img/favicon.png')}}?ver=<?php echo Date("Y.m.d.G.i.s") ?>" />
        <!-- favicons end -->
        <!-- Meta -->
        <meta name="description" content="@yield('meta_description', '')">
        <meta name="author" content="@yield('meta_author', '')">
        <!--Font-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">

        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"> 
        @yield('meta')
        <!-- Styles -->
        @yield('before-styles')
        @yield('after-styles')
        <!-- Scripts -->
        <script>
            window.Laravel = <?php
echo json_encode([
    'csrfToken' => csrf_token(),
]);
?>
        </script>

        <!--<script src="//sdk.engage.co/sdk.js" data-widget="totalwpm-site"></script>-->
        <script>
//            var engage = new EngageSDK("DBFD9200-6BCF-2219-6609-F2987CA9641C");
//            engage.drawWidget({
//                type: "toolbar",
//                options: {
//                    tabPlacement: "bottom-right-tab",
//                    onlineLabel: "Online! Chat Now!",
//                    offlineLabel: "Meet Our Team!",
//                    backgroundColor: "#00d80d",
//                    labelColor: "#fff"
//                }
//            }, function (toolbar) {
//                toolbar.setVisibility(false);
//                $('.trigger-chat').on('click', function () {
//                    toolbar.setVisibility(true);
//                    toolbar.openDrawer();
//                });
//                $('#get-started-modal').on('hidden.bs.modal', function () {
//                    toolbar.setVisibility(false);
//                });
//            });
        </script> 
    </head>
    <body>
        <div class="container-fluid">
            <div class="row" id="twpm">
                @yield('content')
            </div>
        </div>
        @yield('routes')
        <!-- Scripts -->

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

        <script src="{{ asset('js/sockets.js') }}"></script>
        @yield('before-scripts')
        @yield('after-scripts')

        @include('includes.partials.ga')
    </body>
</html>