<div id="header">
    <div class="container-fluid account-setup-header">
        <div class="container">
            <div class="header-logo">
                {{ HTML::image('img/twpm-blue-logo.png') }}
            </div>
            <p>Account Setup</p>
        </div>
    </div>
</div>