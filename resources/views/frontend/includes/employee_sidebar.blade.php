<div class="client-sidebar">
    <div class="site-logo">
         <a  href="https://client.<?php echo  env('MAIN_DOMAIN')?>/employeeClients"> {{ HTML::image('img/twpm-white-logo.png') }}</a>
    </div>
    <ul class="list-unstyled">
        <li class="<?php echo isset($activeClass2)?'active':''; ?>">
            <a href="{{ route('frontend.employee.allClients') }}">
                <div class="icon"><i class="fa fa-briefcase"></i> </div>
                Clients
            </a>
        </li>
    </ul>
</div>