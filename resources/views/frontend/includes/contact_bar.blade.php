<div class="col-sm-12" id="slide8">
    <div class="container">
        <?php if (!empty($footer)) { ?>
            <h2 class="cta"><?php echo strip_tags($footer->heading_subfooter); ?><span>{{ $footer->phone }}</span></h2>
            <h2 class="mobile-cta">{!! $footer->heading_subfooter !!}<span>{{ $footer->phone }}</span></h2>
        <?php } ?>
    </div>
</div>