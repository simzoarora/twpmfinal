<!--slide5-->
<div class="col-sm-12" id="slide5">
    <div class="container">
        <div class="row">
            <h1>Start your plan today</h1>
            <div class="investment-intake">
                <div class="response"></div>
                {{ Form::open(['route'=>'frontend.investmentIntake','id' => 'investment-intake-form']) }}
                <div class="dialogue-1">
                    <span>I am</span>
                    <div class="investment_wrapper">
                         {{ Form::number('question[answer1]', null, ['required'=>true, 'class'=>'select-input select-consent' , 'min' => '0'] ) }}
                    </div>
                    <span> years old and my annual household income is </span>
                    <div class="investment_wrapper">
                         {{ Form::number('question[answer2]', null, ['required'=>true, 'class'=>'select-input' , 'min'=> '0'] ) }}
                    </div>
                    <span>. I </span>
                    <div class="investment_wrapper">
                        <span class="select-input select-query_type"></span>
                        <ul class="query_type select-list" data-input="select-query_type">
                            <li data-val="am">am</li> 
                            <li data-val="am not">am not</li>
                        </ul>
                    </div>
                    <span> currently investing, and I consider myself </span>
                    <div class="investment_wrapper">
                        <span class="select-input select-query_type2"></span>
                        <ul class="query_type select-list" data-input="select-query_type2">
                            <li data-val="a novice">a beginner</li>
                            <li data-val="very good">very efficient</li>
                            <li data-val="need improvement">need improvement</li>
                            <li data-val="okay">okay</li>
                        </ul>
                    </div>
                    <span> when it comes to my finances. </span>
                    <div class="proceed">
                        <!--<a href="#" class="btn btn-blue">Get Started</a>-->
                        {{ Form::submit('Get Started', ['class' => 'btn btn-blue submit_form']) }}

                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>