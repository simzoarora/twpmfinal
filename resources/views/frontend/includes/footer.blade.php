@include('frontend.includes.contact')
<!--login modal-->
<style>
.modal-header-text{
   margin-top: 0px !important;
}
</style>
<div class="modal fade" id="loginModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h2 class"modal-header-text">Welcome</h2>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <div class="text-center">
                        <p class="">Log in to your TWPM account here. Clicking the Raymond James button will take you to the Raymond James website.</p>   
                    </div>
                    <div class="row button-section">
                        <div class="col-md-6">
                            <a href="{{route('frontend.auth.login')}}" class="btn btn-blue login-blue-btn dashboard-link"> Go to TWPM Dashboard</a>
                        </div>
                        <div class="col-md-6">
                            <a href="https://clientaccess.rjf.com" class="btn btn-blue login-blue-btn raymond-james-link"> Go to Raymond James</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--login modal END-->

<div class="col-sm-12" id="footer"> 
    <div class="container">
        <ul class="footer-links">
            <li class="hide"><a href="https://www.facebook.com/">Like <span class="facebook-icon"></span></a></li>
            <li class="hide"><a href="https://twitter.com/">Follow <span class="twitter-icon"></span></a></li>
            <li><a href="{{ route('frontend.legal') }}">Legal Disclosures</a></li>
            <li><a href="{{ route('frontend.privacy') }}">Privacy & Security</a></li>
            <li><a href="{{ route('frontend.terms') }}">Terms of Use</a></li>
            <li><a href="" class="contact-modal-show">Contact</a></li>
        </ul>
        <div class="footer-text">
            <?php echo (!empty($footer) ? $footer->footer_text : '') ?>
        </div>
    </div>
</div>  