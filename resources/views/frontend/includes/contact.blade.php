<style>
    .form-group{
        margin-bottom: 25px;
    }
</style> 
<div id="get-started-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">  
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body"> 
                <h3>Contact Us</h3> 
                <div class="col-sm-6 left-side"> 
                    <div class="col-sm-12 call">
                        <div class="icon"><span><i class="fa fa-phone" aria-hidden="true"></i></span>call us</div>
                        <p><span><?php echo (!empty($contact) ? $contact->phone : '') ?></span></p>
                    </div>
                    <div class="col-sm-12 chat">
                        <div class="icon"><span><i class="fa fa-inbox" aria-hidden="true"></i></span>live chat</div>
                        <a class="btn-green trigger-chat">LET'S TALK</a> 
                    </div>
                    <div class="col-sm-12 offices">
                        <div class="icon"><span><i class="fa fa-map-marker" aria-hidden="true"></i></span>our offices</div>
                        <?php echo (!empty($contact) ? $contact->address : '') ?>
                    </div>
                </div>
                <div class="col-sm-6 right-side" id="contact-modal-home">
                    <div class="contact-response" ></div>
                    {{Form::open(['route' => 'frontend.postContact','id'=>'contact-us','class' => 'contact-form', 'role' => 'form', 'method' => 'POST'])}}
                    <div class="form-group inner-left">
                          <!--{{ Form::select('query_type', ['Other'=>'Other'], null, ['class' => 'form-control select-type service-question-custom-select', 'placeholder' => 'How can we help?', 'required' , "style" => ""]) }}-->
                         <i class="fa fa-angle-down selectArrow" aria-hidden="true" style="position: absolute;right: 34px;z-index: 0;top: 25px;font-size: 16px;" ></i>
                        <select required aria-required="true" style="position: absolute;" class="form-control select-type service-question-custom-select selectboxit" name="query_type" id="query_type" style="display: none;">
                            <option disabled="" value="">How Can We Help?</option>
                            <option value="I have general questions about investing or financial matters">I have general questions about investing or financial matters</option>
                            <option value="I need help with managing my money and debts">I need help with managing my money and debts</option>
                            <option value="I need help planning for college">I need help planning for college</option>
                            <option value="I need help planning for retirement">I need help planning for retirement</option>
                            <option value="I need help with my 401(k) choices">I need help with my 401(k) choices</option>
                            <option value="I would like to discuss my portfolio">I would like to discuss my portfolio</option>
                            <option value="Other">Other</option>
                        </select>
                    </div>
                    <div class="form-group inner-left">
                        {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Your name', 'required']) }}
                    </div>
                    <div class="form-group inner-left"> 
                        {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Your email', 'required','id'=>'myEmail','pattern'=>'[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$', 'size'=>'30']) }}
                    </div>
                    <div class="form-group inner-left">
                        {{ Form::number('phone', null, ['class' => 'form-control', 'placeholder' => 'Your phone', 'required']) }}
                    </div>
                    {{ Form::submit('Submit', ['class' => 'form-control btn-green']) }}
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div> 