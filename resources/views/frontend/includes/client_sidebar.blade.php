<div class="client-sidebar">
    <div class="site-logo">
        <a  href="https://client.<?php echo  env('MAIN_DOMAIN')?>/dashboard"> {{ HTML::image('img/twpm-white-logo.png') }} </a>
    </div>
    <ul class="list-unstyled">
        <li class="<?php echo (Route::current()->getName()=='frontend.client.dashboard')?'active':''; ?>">
            <a href="{{ route('frontend.client.dashboard', config('constant.subdomain')) }}">
                <div class="icon"> 
                   {{ HTML::image('img/HOME_icon.svg') }}
                </div>
                Home
            </a>
        </li>
        <li class="<?php echo (Route::current()->getName()=='frontend.client.recommendedServices')?'active':''; ?>">
            <a href="{{ route('frontend.client.recommendedServices', config('constant.subdomain')) }}">
                <div class="icon">{{ HTML::image('img/SERVICES_icon.svg') }}</div>
                Services
            </a>
        </li>
<!--        <li class="<?php // echo (Route::current()->getName()=='frontend.client.plan')?'active':''; ?>">
            <a href="{{ route('frontend.client.plan', config('constant.subdomain')) }}">
                <div class="icon"><i class="fa fa-briefcase"></i> </div>
                My Plan
            </a>
        </li>-->
        <li class="<?php echo (Route::current()->getName()=='frontend.client.documents')?'active':''; ?>">
            <a href="{{ route('frontend.client.documents', config('constant.subdomain')) }}">
                <div class="icon">{{ HTML::image('img/DOCUMENTS_icon.svg') }}<span class="noti-red hide"></span> </div>
                Documents
            </a>
        </li>
        <li class="<?php echo (Route::current()->getName()=='frontend.client.resources.categories')?'active':''; ?>">
            <a href="{{ route('frontend.client.resources.categories', config('constant.subdomain')) }}">
                <div class="icon">{{ HTML::image('img/RESOURCES_icon.svg') }}</div>
                Resources
            </a>
        </li>
    </ul>
</div>