<nav class="navbar home-navbar">
    <div class="container">
        <div class="navbar-header"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#frontend-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>    
            </button>
            <a href='{{route('frontend.index')}}' class="navbar-brand">{{HTML::image('/img/Logo-wht.png','alt',['class'=>"logo-img"])}}</a>
        </div>
        <div class="collapse navbar-collapse" id="frontend-navbar-collapse">
            <ul class="nav navbar-nav">
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.about'?'active':''}}'>{{ link_to_route('frontend.about', trans('navbar.home.who_we')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.services'?'active':''}}'>{{ link_to_route('frontend.services', trans('navbar.home.services')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.resources'?'active':''}}'>{{ link_to_route('frontend.resources', trans('navbar.home.resources')) }}</li>
                <li class='{{Route::getCurrentRoute()->getName()=='frontend.blog'?'active':''}}'>{{ link_to_route('frontend.blog', trans('navbar.home.commentary')) }}</li>
            </ul>
            <ul class="nav navbar-right">
                <?php if ($logged_in_user) { ?>
                    <li><a href="{{route('frontend.auth.logout')}}">Logout</a></li>
                    <?php
                    $route = route('frontend.client.dashboard', [config('constant.subdomain')]);

                    if (access()->hasRole(config('access.roles.employee_role'))) {
                        $route = route('frontend.employee.allClients');
                    }
                    ?>
                    <li class="getstart-bg "> <a href="{{$route}}">Dashboard</a></li>
                <?php } else { ?>
                    <li><a href="#" data-toggle="modal" data-target="#loginModal">Login</a></li>
                        <?php if (!empty($footer)) { ?>
                        <li class="getstart-bg"> <a href="{{route('frontend.client.signUp', [config('constant.subdomain')])}}">Get Started</a></li>
                        <?php } ?>
<?php } ?>
            </ul>
        </div>
    </div>
</nav>