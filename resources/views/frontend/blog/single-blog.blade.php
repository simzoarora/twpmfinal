@extends('frontend.layouts.master')

@section('title')
{{$article->title}}
@stop

@section('meta_description')
{{$article->excerpt}}
@stop

@section('meta_author')
{{$article->articleAuthor->name}}
@stop

@section('after-styles')
{{ Html::style(asset('css/single-blog.css')) }}
<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="<?php echo $article->title; ?>">
<meta itemprop="description" content="<?php echo $article->excerpt; ?>">
<meta itemprop="image" content="<?php echo URL::asset('img/backend/blogs/articles/'.$article->image); ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="product">
<meta name="twitter:site" content="@TWPM">
<meta name="twitter:title" content="<?php echo $article->title; ?>">
<meta name="twitter:description" content="<?php echo $article->excerpt; ?>">
<meta name="twitter:creator" content="@TWPM">
<meta name="twitter:image" content="<?php echo URL::asset('img/backend/blogs/articles/'.$article->image); ?>">

<!-- Open Graph data -->
<meta property="og:title" content="<?php echo $article->title; ?>" />
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo Request::fullUrl(); ?>" />
<meta property="og:image" content="<?php echo URL::asset('img/backend/blogs/articles/'.$article->image); ?>" />
<meta property="og:description" content="<?php echo $article->excerpt; ?>" /> 
<meta property="og:site_name" content="<?php echo url(''); ?>" />
<meta property="fb:app_id" content="1713884215569225"/> 

@stop 
@section('content')
<div class='container-fluid header'>
    <div class="container">
        <div class="home-heading">
            <p>WHERE INVESTORS <br>GO TO <span>GROW</span></p>
        </div>
        <div class="proceed">
            <a href="#" class="btn btn-blue" id="subscribe-newsletter">SUBSCRIBE</a>
        </div>
    </div>
</div>
<div class="col-sm-12 single-blog">
    <div class="container">
        <div class="col-sm-12 blog-article">
            <a href="{{ route('frontend.blog') }}" class="back-commentary">
                <span class="services-arrow-left"></span>
                Return to Commentary</a>
            <h2 class="blog-title">{{$article->title}}</h2>
            <div class="blog-publishment">
                By <span class="blog-author">{{$article->articleAuthor->name}}</span>
                <span class="blog-time">
                    {{date('F j, Y',strtotime($article->publish_at))}}
                    <?php foreach ($article->blogCategories as $category) { ?>
                        <span>{{$category->title}}</span>
                    <?php } ?>
                </span>
            </div>
            <div class="blog-img">
                {{ HTML::image('img/backend/blogs/articles/'.$article->image) }}
            </div>
            <div class="blog-content">
                <?php echo $article->body; ?>
            </div>
            <div class="nav-arrows">
                <?php if ($previousArticle) { ?>
                    <a href='{{route('frontend.single-blog',$previousArticle->slug)}}' class="arrow-prev">
                        <span class="blog-arrow-left"></span>
                        <span class="text">PREV</span>
                    </a>
                <?php } if ($nextArticle) { ?>
                    <a href='{{route('frontend.single-blog',$nextArticle->slug)}}' class="arrow-next">
                        <span class="text">NEXT</span>
                        <span class="blog-arrow-right"></span>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!--social-media-share-div-->
<div class="social-media-share">
    <ul>
        <a id="facebook-share">
            <li class="facebook">
                <i class="fa fa-facebook" aria-hidden="true">
                    <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $article->excerpt; ?>"></div>
                </i>
                <!--<span>623</span>-->
            </li>
        </a> 
        <!--<a><li class="tumbler"><i class="fa fa-tumblr" aria-hidden="true"></i><span>62</span></li></a>--> 
        <a id="twitter-share">
            <li class="twitter">
                <i class="fa fa-twitter" aria-hidden="true">
                    <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $article->excerpt; ?>"></div>
                </i>
                <!--<span>63</span>-->
            </li>
        </a> 
        <a id="linkedin-share">
            <li class="linkedin">
                <i class="fa fa-linkedin" aria-hidden="true">
                    <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $article->excerpt; ?>"></div>
                </i>
                <!--<span>63</span>-->
            </li>
        </a> 
        <a id="google-share">
            <li class="add-more">
                <i class="fa fa-plus" aria-hidden="true">
                    <div data-url="<?php echo Request::fullUrl(); ?>" data-text="<?php echo $article->excerpt; ?>"></div>
                </i>
                <!--<span>1.2K</span>-->
            </li>
        </a> 
        <a id="open-email-share" data-target="#share-modal" data-toggle="modal">
            <li class="print">
                <i class="fa fa-print" aria-hidden="true">
                </i>
                <!--<span>3</span>-->
            </li>
        </a> 
    </ul>
</div> 
<!-- Modal -->
<div id="subscribe-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body">
                <p>Get the latest news and insights, delivered right to your inbox</p>
                {{ Form::open(array('url' => route('frontend.subscribe'),'method'=>'POST','id'=>'subscriptionForm')) }}
                {{ Form::email('email_address', '', ['class' => 'input-field', 'placeholder'=>'Your email']) }}
                {{ Form::submit('Submit', array('class' => 'btn-green')) }}
                {{ Form::close() }}
                <div class="server-response">
                </div> 
            </div>
        </div>
    </div> 
</div>
<div id="share-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body">
                {{ Form::open(array('url' => route('frontend.share'),'method'=>'POST','id'=>'shareForm')) }}
                {{ Form::label('email', "Friend's Email") }}
                {{ Form::email('email', '', ['class' => 'input-field form-control', 'placeholder'=>'friends@email.com']) }}
                {{ Form::label('message', "Message") }}
                {{ Form::textarea('message', '', ['class' => 'textarea-field form-control', 'placeholder'=>'Message']) }}
                <div class="col-sm-12 text-right p0">
                    {{ Form::submit('Send', array('class' => 'btn-green')) }}
                </div>
                {{ Form::close() }}
                <div class="server-response">
                </div>
            </div>
        </div>
    </div> 
</div>
@endsection
@section('after-scripts')
<script src="{{ asset('js/single-blog.js') }}"></script>
@stop