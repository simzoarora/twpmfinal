@extends('frontend.layouts.master')

@section('title')
Commentary &#8226; Total Wealth Planning Management
@stop

@section('after-styles')
{{ Html::style(asset('css/blog.css')) }}
@stop 

@section('content')
<?php

use Carbon\Carbon;
?>
<div class='container-fluid header'>
    <div class="container">
        <div class="home-heading">
            <p>WHERE INVESTORS <br>GO TO <span>GROW</span></p>
        </div>
        <div class="proceed">
            <a href="#" class="btn btn-blue" id="subscribe-newsletter">SUBSCRIBE</a>
        </div>
    </div>
</div>

<div class="container blog-news">
    <h3 class="col-sm-12 heading p-bottom">Latest News</h3>
    @if(isset($firstThreeArticles))
    @foreach ($firstThreeArticles as $article)
    <div class="col-sm-4">
        <div class="blog-img">
            <a href="{{ route('frontend.single-blog',$article->slug) }}">
                <div class="first-three-images" style="background-image: url('img/backend/blogs/articles/{{ $article->image }}')"></div>
            </a>
            <div>
                <p class="category">
                    @foreach ($article->blogCategories as $category)
                    <span>{{$category->title}}</span>
                    @endforeach
                </p>
                <a href="{{ route('frontend.single-blog',$article->slug) }}">
                    <h5>{{$article->title}}</h5>
                </a>
                <p>
                    <span>
                        @php
                        $excerpt = explode( "\n", wordwrap( $article->excerpt,140));
                        @endphp
                        {{ $excerpt[0] }}
                    </span>
                    <?php if (isset($excerpt[1]) && !empty($excerpt[1])) {
                        ?>

                        <span class="excerpt-second">
                            {{ $excerpt[1] }}
                        </span>

                    <?php } ?>
                    <span class="excerpt-handler" data-current="more">
                        more
                    </span>
                </p>
                <span class="author-name">
                    {{$article->articleAuthor->name}}
                </span>
                <span class="date">
                    {{Carbon::createFromTimeStamp(strtotime($article->publish_at))->diffForHumans()}}
                </span>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endforeach
    @endif
</div>

<div class="container blog-news">
    <div class="col-sm-12">
        <h3 class="heading">By Topic</h3>
        <ul class="blog-isotope-links p-bottom">
            <li class="active"><a data-filter="*" href="#">ALL</a></li>
            @foreach ($categories as $singleCategory)
            <li>
                @php
                $category_trim = trim($singleCategory['title']);
                $category = str_replace('.', '', $category_trim);  
                $category = str_replace(' ', '-', $category);
                @endphp
                <a data-filter=".{{$category}}" href="#">
                    {{$singleCategory['title']}}
                </a>
            </li>
            @endforeach
        </ul>
        <div class="row blog-isotope-container">
            @if(isset($fiveArticles))
            @foreach ($fiveArticles as $article)
            <div class="col-sm-4 <?php
            foreach ($article->blogCategories as $category) {
                $category_trim = trim($category->title);
                $category = str_replace('.', '', $category_trim);
                echo str_replace(' ', '-', $category) . ' ';
            }
            ?>" >
                <a class="blog-img" href="{{ route('frontend.single-blog',$article->slug) }}">
                    {{ HTML::image('img/backend/blogs/articles/'.$article->image, '', array('class' => 'img-responsive')) }}
                    <div>
                        <p class="category">
                            @foreach ($article->blogCategories as $category)
                            <span>{{$category->title}}</span>
                            @endforeach
                        </p>
                        <h5>{{$article->title}}</h5>
                        <p>
                            {{ $article->excerpt }}
                        </p>
                        <span class="author-name">
                            {{$article->articleAuthor->name}}
                        </span>
                        <span class="date">
                            {{Carbon::createFromTimeStamp(strtotime($article->publish_at))->diffForHumans()}}
                        </span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="subscribe-modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <span class="close" data-dismiss="modal">&times;</span>
            <div class="modal-body">
                <p>Get the latest news and insights, delivered right to your inbox</p>
                {{ Form::open(array('url' => route('frontend.subscribe'),'method'=>'POST','id'=>'subscriptionForm')) }}
                {{ Form::email('email_address', '', ['class' => 'input-field', 'placeholder'=>'Your email']) }}
                {{ Form::submit('Submit', array('class' => 'btn-green')) }}
                {{ Form::close() }}
                <div class="server-response">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('after-scripts')
<script src="{{ asset('js/blog.js') }}"></script>
@stop