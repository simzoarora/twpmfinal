<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Navbar Language Lines
      |--------------------------------------------------------------------------
      |
      | The following language lines are used in menu items throughout the system.
      | Regardless where it is placed, a menu item can be listed here so it is easily
      | found in a intuitive way.
      |
     */

    'home' => [
        'who_we' => 'WHO WE ARE',
        'services' => 'SERVICES',
        'resources' => 'RESOURCES',
        'commentary' => 'COMMENTARY',
        'client_login' => 'Client Login',
        'broker_check' => 'Broker Check',
    ],
    'services' => [
        'over_top' => 'eOver the Top service',
        'traditional_top' => 'Traditional Over the Top Service',
        'veteran_discount' => "Veteran's Discount Available ",
    ]
];
