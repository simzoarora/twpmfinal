var elixir = require('laravel-elixir');

//require('events').EventEmitter.prototype._maxListeners = 100;
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    /**
     * Copy needed files from /node directories
     * to /public directory.
     */
    mix
//            .copy(
//                    'node_modules/font-awesome/fonts',
//                    'public/build/fonts/font-awesome'
//                    )
//            .copy(
//                    'node_modules/bootstrap-sass/assets/fonts/bootstrap',
//                    'public/build/fonts/bootstrap'
//                    )

//        /**
//         * Process frontend SCSS stylesheets
//         */
//           
//          
////
////        /**
////         * Home page SCSS stylesheets
////         */

//            

//            
//            
//            
//            
////        /**
////  
//       
//////        /**
//////       
//            
//            
//          
//////        //        /**
//////       
////         
//          
//      
          
//           
//     
            
//        
//         
           

       
           
       

//
});

elixir(function (mix) {
    /**
     * Apply version control
     */
    mix.version([
//        "public/css/home2.css",
//        "public/js/home.js",
//        "public/css/services.css",
//        "public/js/services.js",
//        "public/css/about.css",
//        "public/js/about.js",
//        "public/css/resources.css",
//        "public/js/resources.js",
//        "public/css/blog.css",
//        "public/js/blog.js",
//        "public/css/single-blog.css",
//        "public/js/single-blog.js",
//        "public/css/privacy.css",
//        "public/js/privacy.js",
//        "public/css/legal.css",
//        "public/js/legal.js",
//        "public/css/terms.css",
//        "public/js/terms.js",
//        "public/css/backend.css",
//        "public/js/backend.js",
//        "public/css/clientLogin.css",
        "public/css/dashboard.css",
//        "public/css/service-questions.css",
        "public/js/dashboard.js",
//        "public/js/signUp.js",
//        "public/css/profile.css",
        "public/css/documents.css",
//        "public/css/preview.css",
//        "public/css/signUpOne.css",
//        "public/css/signUpSix.css",
//        "public/css/signUpSixteen.css",
        "public/css/servicesQuestion.css",
//        "public/css/selectedService.css",
//        "public/css/twpmDashboard.css",
//        "public/css/finish-account.css",
//        "public/css/single-service-questions.css",
        "public/css/employee-dashboard-services.css",
//        "public/js/step-one.js",
//        "public/js/step-two.js",
//        "public/js/step-three.js",
//        "public/js/step-four.js",
//        "public/js/client-login.js",
//        "public/js/step-five.js",
//        "public/js/client-profile.js",
//        "public/js/step-payment.js",
        "public/js/employee.js",
//        "public/js/documents.js",
//        "public/js/sign-up-questions.js",
        "public/js/services-questions.js",
//        "public/js/service-enrollment.js",
//        "public/js/contact-information.js",
        "public/js/life-insurance.js",
        "public/js/educational-consultation.js",
        "public/js/annual-portfolio-review.js",
        "public/js/comprehensive-planning.js",
        "public/js/goal-specific-financial-planning.js"
    ]);
});