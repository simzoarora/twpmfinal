<?php

/**
 * Frontend Controllers
 * All route names are prefixed with 'frontend.'
 */
//Route::get('/', 'FrontendController@index')->name('index');
//Route::get('macros', 'FrontendController@macros')->name('macros');
Route::get('/test', 'TestController@signatureRequestFromTemplate')->name('sign');
Route::get('/anchor', 'TestController@getClientAuthorization')->name('anchor');
Route::get('test-broadcast', function() {
    broadcast(new \App\Events\PublicNotification);
});
Route::get('/', 'HomeController@index')->name('index');
Route::get('/service', 'HomeController@services')->name('services');
Route::get('/services_old', 'HomeController@services_old')->name('services_old');
Route::get('/service/{id}', 'HomeController@getServiceDetails')->name('getServiceDetails');
Route::post('/contact', 'HomeController@postContact')->name('postContact');
Route::get('/resource/{id}', 'HomeController@getResourceTopics')->name('getResourceTopics');
Route::get('/about', 'HomeController@about')->name('about');
Route::get('/resource', 'HomeController@resources')->name('resources');
Route::get('/blogs', 'HomeController@blog')->name('blog');
Route::get('/blog/{slug?}', 'HomeController@singleBlog')->name('single-blog');
Route::get('/moreBlogs/{skip?}', 'HomeController@getMoreBlogs')->name('getMoreBlogs');
Route::get('/privacy-and-security', 'HomeController@privacy')->name('privacy');
Route::get('/terms-of-use', 'HomeController@terms')->name('terms');
Route::get('/legal-disclosures', 'HomeController@legal')->name('legal');
Route::post('/subscribe', 'HomeController@subscribe')->name('subscribe');
Route::post('/share', 'HomeController@share')->name('share');
Route::post('/investmentIntake', 'HomeController@investmentIntake')->name('investmentIntake');
Route::get('handleSharefileResponse', 'ClientOldController@handleSharefileResponse')->name('client.handleSharefileResponse');
Route::get('/preview', 'ClientOldController@preview')->name('previewpage');
//Route::get('/signUpOne', 'ClientOldController@signUpOne')->name('signUpOne');
//Route::get('/signUpTwo', 'ClientOldController@signUpTwo')->name('signUpTwo');
//Route::get('/signUpThree', 'ClientOldController@signUpThree')->name('signUpThree');
//Route::get('/signUpFour', 'ClientOldController@signUpFour')->name('signUpFour');
//Route::get('/signUpFive', 'ClientOldController@signUpFive')->name('signUpFive');
//Route::get('/signUpSix', 'ClientOldController@signUpSix')->name('signUpSix');
//Route::get('/signUpEight', 'ClientOldController@signUpEight')->name('signUpEight');
//Route::get('/signUpNine', 'ClientOldController@signUpNine')->name('signUpNine');
//Route::get('/signUpTen', 'ClientOldController@signUpTen')->name('signUpTen');
//Route::get('/signUpEleven', 'ClientOldController@signUpEleven')->name('signUpEleven');
//Route::get('/signUpTwelve', 'ClientOldController@signUpTwelve')->name('signUpTwelve');
//Route::get('/calculatingPlan', 'ClientOldController@calculatingPlan')->name('calculatingPlan');
//Route::get('/loginScreen', 'ClientOldController@loginScreen')->name('loginScreen');

Route::post('/checkEmailAvailability', 'ClientController@checkEmailAvailability')->name('checkEmailAvailability');



Route::group(['as' => 'client.'], function () {
    // new routes

    Route::get('test', 'ClientController@test')->name('test');
    Route::post('sendOtp', 'ClientController@sendOtp')->name('sendOtp');
    Route::post('verifyOtp', 'ClientController@verifyOtp')->name('verifyOtp');
    Route::post('saveSignUpUser', 'ClientController@saveSignUpUser')->name('saveSignUpUser');

    Route::post('submitVerify', 'ClientOldController@submitVerify')->name('submitVerify');

    Route::post('verifyLoginOtp', 'Auth\LoginController@verifyLoginOtp')->name('verifyLoginOtp');
    Route::post('personalProfileSave', 'FrontendController@personalProfileSave')->name('personalProfileSave');
    Route::post('financeProfileSave', 'FrontendController@financeProfileSave')->name('financeProfileSave');
    Route::post('employmentProfileSave', 'FrontendController@employmentProfileSave')->name('employmentProfileSave');
    Route::post('userNameSave', 'FrontendController@userNameSave')->name('userNameSave');
       Route::any('signDocument', 'EmployeeController@signDocument')->name('employee.signDocument');

});
//subdomain routes
$appRoutes = function() {
    Route::group(['as' => 'client.'], function () {

        Route::get('signupStepPayment', 'ClientOldController@signupStepPayment')->name('signupStepPayment');

        Route::get('signUp', 'ClientController@signUp')->name('signUp');
        Route::group(['middleware' => 'guest'], function () {
            Route::get('results', 'ClientController@results')->name('results');
        });

        Route::group(['middleware' => ['auth', 'checkRole:' . config('access.users.default_role')]], function () {

            //previous routes
//            Route::group(['middleware' => 'profileCheck'], function () {
//                Route::get('dashboard', 'ClientOldController@clientDashboard')->name('dashboard');
//                Route::get('clientResourcesCategories', 'ClientOldController@clientResourcesCategories')->name('resources.categories');
//                Route::get('clientResources/{categoryId}', 'ClientOldController@clientResources')->name('resources');
//                Route::get('clientResourceDetails/{resouceId}', 'ClientOldController@clientResourceDetails')->name('resource.details');
//                Route::get('clientProfile', 'ClientOldController@clientProfile')->name('profile');
            Route::get('clientResourcesCategories', 'ClientController@clientResourcesCategories')->name('resources.categories');
            Route::get('clientResources/{categoesryId}', 'ClientController@clientResources')->name('resources');
            Route::get('clientResourceDetails/{resouceId}', 'ClientController@clientResourceDetails')->name('resource.details');
            Route::get('clientProfile', 'ClientController@clientProfile')->name('profile');

            Route::get('mobileVerify', 'ClientOldController@clientMobileVerify')->name('mobileVerify');
//                Route::get('clientServices', 'ClientOldController@clientServices')->name('services');
//                Route::get('services', 'ClientController@services')->name('services');
            Route::get('recommendedServices/{serviceId?}', 'ClientController@recommendedServices')->name('recommendedServices');

//                Route::get('showClientServices/{id}', 'ClientController@showClientServices')->name('showClientServices');


            Route::get('servicesQuestion/{id}', 'ClientController@servicesQuestion')->name('servicesQuestion');
            Route::get('servicesQuestion/{id}/topic/{topicId}/preview/{pageName}', 'ClientController@openSubTopicPreviewPage')->name('subTopicPreview');
            Route::get('servicesQuestion/{id}/topics', 'ClientController@openPreviewFile')->name('openPreviewFile');

            // for goal specific routes
            Route::get('servicesQuestion/{id}/goalSpecificFinancialPlanning/{topicId}/question/{questionId}', 'ClientController@goalSpecificPlanning')->name('goalSpecificPlanning');
            Route::get('servicesQuestion/{id}/fetchAnswers/{topicId}/question/{questionId}', 'ClientController@fetchAnswers')->name('fetchAnswers');
            Route::get('servicesQuestion/{id}/fetchAnswersSubTopic/{topicId}/{subtopicID}/question/{questionId}', 'ClientController@fetchAnswersSubTopic')->name('fetchAnswersSubTopic');
            Route::get('serviceQuestion/{id}/SemiAnnualOtherDebtsNonbusiness', 'ClientController@SemiAnnualOtherDebtsNonbusiness')->name('SemiAnnualOtherDebtsNonbusiness');
            Route::get('selectedService/{id}', 'ClientController@selectedService')->name('selectedService');
//            Route::get('selectedService/{id}/assets','ClientController@assets')->name('selectedService.asset');  
            Route::get('serviceEnrollment', 'ClientController@serviceEnrollment')->name('serviceEnrollment');

            Route::get('clientPlan', 'ClientController@clientPlan')->name('plan');
            Route::get('clientsDocuments', 'FrontendController@clientDocuments')->name('documents');
            Route::get('documentFiles/{serviceId}', 'FrontendController@clientDocumentFiles')->name('documentFiles');
//                Route::get('serviceQuestions/{serviceId}', 'ClientOldController@clientServiceQuestions')->name('serviceQuestion');
//             });
            Route::get('downloadClientFile/{fileName}/{serviceId}', 'ClientOldController@downloadClientFile')->name('downloadFile');
            Route::get('showAnalysis', 'ClientController@showAnalysis')->name('showAnalysis');
            Route::get('finishAccountSetup', 'ClientController@finishAccountSetup')->name('finishAccountSetup');
            Route::get('contactInfo', 'ClientController@contactInfo')->name('contactInfo');
            Route::get('identityVerification', 'ClientController@identityVerification')->name('identityVerification');
            Route::get('employmentInfo', 'ClientController@employmentInfo')->name('employmentInfo');
            Route::get('financialInfo', 'ClientController@financialInfo')->name('financialInfo');
            Route::get('securityInfo', 'ClientController@securityInfo')->name('securityInfo');
            Route::get('dashboard', 'ClientController@clientDashboard')->name('dashboard');
            Route::get('json', 'ClientController@json')->name('json');
            Route::get('docusign/{questionId}', 'ClientController@sendDocumentForDigitalSignature')->name('docusign');
            Route::get('checkEnvelopeStatus', 'ClientController@checkStatus')->name('checkEnvelopeStatus');
//            Route::get('taxBracket', 'ClientController@calculateTaxBracket')->name('taxBracket');
        });
        Route::get('notification', 'ClientController@notification')->name('notification');
        Route::post('serviceUploadFile/{clientId?}', 'ClientController@serviceUploadFile')->name('serviceUploadFile');
    });
};
//Route::group(array('domain' => '{domain}.twpmdemo.us'), $appRoutes);
//Route::group(array('domain' => '{domain}.twpm.rahulbehl.me'), $appRoutes);
Route::group(array('domain' => "{domain}." . env('MAIN_DOMAIN')), $appRoutes);


/**
 * These frontend controllers require the user to be logged in
 * All route names are prefixed with 'frontend.'
 */
Route::group(['middleware' => 'auth'], function () {
    Route::group(['namespace' => 'User', 'as' => 'user.'], function() {
        /**
         * User Dashboard Specific
         */
//        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        /**
         * User Account Specific
         */
        Route::get('account', 'AccountController@index')->name('account');

        /**
         * User Profile Specific
         */
        Route::patch('profile/update', 'ProfileController@update')->name('profile.update');
    });

    Route::group(['as' => 'client.'], function () {
                    Route::get('confirm_mail/{token}', 'ClientController@confirm_mail')->name('confirm_mail');

        Route::post('changeTwoFactorAuth', 'ClientOldController@changeTwoFactorAuth')->name('changeTwoFactorAuth');
        Route::post('updateServiceQuestionAnswer', 'ClientOldController@updateServiceQuestionAnswer')->name('updateServiceQuestionAnswer');
        Route::post('deleteClientAnswerFile/{id}', 'ClientOldController@deleteClientAnswerFile')->name('deleteClientFile');
        Route::post('updateClientProfileData', 'FrontendController@updateClientProfileData')->name('updateClientProfileData');

        //new Routes
        Route::post('saveContactInfo', 'ClientController@saveContactInfo')->name('saveContactInfo');
        Route::post('saveIdentityVerification', 'ClientController@saveIdentityVerification')->name('saveIdentityVerification');
        Route::post('saveEmploymentInfo', 'ClientController@saveEmploymentInfo')->name('saveEmploymentInfo');
        Route::post('saveFinancialInfo', 'ClientController@saveFinancialInfo')->name('saveFinancialInfo');
        Route::post('saveSecurityInfo', 'ClientController@saveSecurityInfo')->name('saveSecurityInfo');
        Route::post('saveServiceQuestionAnswer', 'ClientController@saveServiceQuestionAnswer')->name('saveServiceQuestionAnswer');
        Route::post('answerQuestions', 'ClientController@answerQuestions')->name('answerQuestions');
        Route::post('stripeCardPayment', 'ClientController@stripeCardPayment')->name('stripeCardPayment');
        Route::post('stripePayment', 'ClientController@stripePayment')->name('stripePayment');
        Route::post('stripeCardRemove', 'ClientController@stripeCardRemove')->name('stripeCardRemove');
        Route::post('isReadTrue', 'ClientController@isRead')->name('isReadTrue');
    });

    //Employee

    Route::group(['middleware' => ['checkRole:' . config('access.roles.employee_role')]], function () {
        Route::get('employeeHome', 'EmployeeController@home')->name('employee.home');
        Route::get('employeeClients', 'EmployeeController@allClients')->name('employee.allClients');
        Route::get('testClients', 'EmployeeController@testClients')->name('employee.testClients');
        Route::get('clientDetails/{id}', 'EmployeeController@getClientDetails')->name('employee.clientDetails');
        Route::get('employeeClients/{id}', 'EmployeeController@getAllClients')->name('employee.getAllClients');
        Route::get('articleList', 'EmployeeController@articleList')->name('employee.articleList');
        Route::post('deleteArticle', 'EmployeeController@deleteArticle')->name('employee.deleteArticle');
        Route::get('createArticle', 'EmployeeController@createArticle')->name('employee.createArticle');
        Route::post('saveArticle', 'EmployeeController@saveArticle')->name('employee.saveArticle');
        Route::get('editArticle/{id}', 'EmployeeController@editArticle')->name('employee.editArticle');
        Route::post('updateArticle/{id}', 'EmployeeController@updateArticle')->name('employee.updateArticle');
        Route::post('generateSlug', 'EmployeeController@autoGenerateSlug')->name('employee.generateSlug');
        Route::post('checkSlugDuplicacy', 'EmployeeController@checkSlugDuplicacy')->name('employee.checkSlugDuplicacy');
        Route::post('addEmployeeNote', 'EmployeeController@addEmployeeClientNotes')->name('employee.addNotes');
        Route::get('getEmployeeNote', 'EmployeeController@getEmployeeClientNotes')->name('employee.getNotes');
        Route::post('saveDocument', 'DocumentController@saveDocument')->name('saveDocument');
        Route::get('editDocument/{id}', 'DocumentController@editDocument')->name('editDocument');
        Route::post('getConnectionData', 'DocumentController@getConnectionData')->name('getConnectionData');
        Route::post('updateDocument/{id}', 'DocumentController@updateDocument')->name('updateDocument');
        Route::post('deleteDocument/{id}', 'DocumentController@DeleteDocument')->name('deleteDocument');
        Route::get('getNotesData', 'EmployeeController@getNotesData')->name('employe.getNotesData');
        Route::get('currentServiceView/{serviceId}/{clientId}', 'EmployeeController@currentServicePage')->name('employe.currentServicePage');
        Route::get('answerView/{serviceId}/{topicId}/{qustionNo}/{clientId}', 'EmployeeController@getServiceTopicAnswers')->name('employe.getServiceTopicAnswers');
        Route::get('getQuestions/{topicId}', 'EmployeeController@getQuestions')->name('employee.getQuestions');
    
        
            Route::post('userAccountInfo', 'EmployeeController@userAccountInfo')->name('employee.userAccountInfo');

    });
    Route::post('updateAnswers/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@updateAnswers')->name('employee.updateAnswers');
    Route::post('deleteAnswers/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@deleteAnswers')->name('employee.deleteAnswers');
    Route::post('addFiles/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@addFiles')->name('employee.addFiles');
    Route::post('deleteFiles/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@deleteFiles')->name('employee.deleteFiles');
    Route::post('deleteSingleFile/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@deleteSingleFile')->name('employee.deleteSingleFile');
    Route::post('getServiceDocuments/{serviceId}/{clientId}', 'EmployeeController@getServiceDocuments')->name('employee.getServiceDocuments');
    Route::post('deleteTableData', 'EmployeeController@deleteTableData')->name('employee.deleteTableData');


    Route::get('getTopicsOrQuestions/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@getTopicsOrQuestions')->name('employe.getTopicsOrQuestions');
    Route::post('getSubTopicsOrQuestions/{serviceId}/{clientId}/{topicId}', 'EmployeeController@getSubTopicsOrQuestions')->name('employee.getSubTopicsOrQuestions');
    Route::post('getQuestionsOfTopicOrSubTopic/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@getQuestionsOfTopicOrSubTopic')->name('employee.getQuestionsOfTopicOrSubTopic');
    Route::post('getDependentQuestions/{serviceId}/{clientId}/{topicId?}', 'EmployeeController@getDependentQuestions')->name('employee.getDependentQuestions');
 
    Route::get('documentStatusCallback', 'EmployeeController@documentStatusCallback')->name('employee.documentStatusCallback');
    Route::get('generatePdf', 'DocumentController@generatePdf')->name('employee.generatePdf');
   Route::get('taxBracket/{clientId?}', 'ClientController@calculateTaxBracket')->name('client.taxBracket');

//    Route::get('client/dashboard', 'ClientOldController@clientDashboard')->name('dashboard');
//    new route set for manipulation of document categories types 
//    Route::get('documentCategories','DocumentController@getCategories')->name('employe.getDocumentCategories');
//       new route set for manipulation of document categories types 
//    Notifications
//    Route::get('unreadNotification', 'DocumentController@unreadNotification')->name('unreadNotification');
    //get notes data ajax function route
});
