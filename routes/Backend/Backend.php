<?php

/**
 * All route names are prefixed with 'admin.'
 */
Route::get('home', 'HomeController@index')->name('home');
Route::post('home', 'HomeController@saveHomepage')->name('saveHomepage');

Route::get('footer', 'FooterController@index')->name('footer');
Route::post('footer', 'FooterController@saveFooter')->name('saveFooter');

Route::get('seo', 'SeoController@index')->name('seo');
Route::post('seo', 'SeoController@saveSeo')->name('saveSeo');

Route::get('contact', 'ContactController@index')->name('contact');
Route::post('contact', 'ContactController@saveContact')->name('saveContact');

Route::get('legalDisclosures', 'PagesController@legalDisclosures')->name('legalDisclosures');
Route::post('legalDisclosures', 'PagesController@saveLegalDisclosures')->name('saveLegalDisclosures');

Route::get('privacy', 'PagesController@privacy')->name('privacy');
Route::post('privacy', 'PagesController@savePrivacy')->name('savePrivacy');

Route::get('terms', 'PagesController@terms')->name('terms');
Route::post('terms', 'PagesController@saveTerms')->name('saveTerms');

Route::resource('about', 'AboutController');

Route::resource('profiletags', 'ProfileTagsController');
Route::post('profiletags/delete', 'ProfileTagsController@delete')->name('profiletags.delete');

Route::resource('employee', 'EmployeeController');
Route::post('editEmployeeOrder', 'EmployeeController@editOrder')->name('employees.order.edit');

Route::get('services', 'ServicesController@index')->name('services');
Route::get('services/data', 'ServicesController@createSlides')->name('service.data');
Route::post('services/data', 'ServicesController@saveSlides')->name('service.data');
Route::get('services/create', 'ServicesController@createService')->name('services.create');
Route::post('services/create', 'ServicesController@saveService')->name('services.create');
Route::post('services/delete', 'ServicesController@deleteService')->name('services.delete');
Route::get('services/{id}/edit', 'ServicesController@editService')->name('services.edit');
Route::post('services/{id}/edit', 'ServicesController@updateService')->name('services.edit');
Route::post('editServiceOrder', 'ServicesController@editOrder')->name('services.order.edit');

Route::get('services/categories', 'ServicesController@categories')->name('services.categories');
Route::get('services/categories/create', 'ServicesController@createCategory')->name('services.categories.create');
Route::post('services/categories/create', 'ServicesController@saveCategory')->name('services.categories.create');
Route::post('services/categories/delete', 'ServicesController@deleteCategory')->name('services.categories.delete');
Route::get('services/categories/{id}/edit', 'ServicesController@editCategory')->name('services.categories.edit');
Route::post('services/categories/{id}/edit', 'ServicesController@updateCategory')->name('services.categories.edit');

Route::get('services/{id}/questions', 'ServicesController@allServiceQuestions')->name('services.questions');
Route::get('services/{id}/questions/create', 'ServicesController@createQuestion')->name('services.questions.create');
Route::post('services/questions/create', 'ServicesController@saveQuestion')->name('services.questions.saveQuestion');
Route::get('services/{id}/questions/{questionId}/edit', 'ServicesController@editQuestion')->name('services.questions.edit');
Route::post('services/questions/update', 'ServicesController@updateQuestion')->name('services.questions.updateQuestion');
Route::post('services/questions/delete', 'ServicesController@deleteServiceQuestion')->name('services.questions.delete');
Route::post('services/questions/editOrder', 'ServicesController@editServiceQuestionOrder')->name('services.questions.order.edit');
Route::post('services/questions/checkOrder', 'ServicesController@checkQuestionOrderStatus')->name('services.questions.order.check');


Route::get('resources/create', 'ResourcesController@createResource')->name('resources.create');
Route::post('resources/create', 'ResourcesController@saveResource')->name('resources.create');
Route::get('resources/categories', 'ResourcesController@categories')->name('resources.categories');
Route::get('resources/categories/create', 'ResourcesController@createCategory')->name('resources.categories.create');
Route::post('resources/categories/create', 'ResourcesController@saveCategory')->name('resources.categories.create');
Route::post('resources/categories/delete', 'ResourcesController@deleteCategory')->name('resources.categories.delete');
Route::get('resources/categories/{id}/edit', 'ResourcesController@editCategory')->name('resources.categories.edit');
Route::post('resources/categories/{id}/edit', 'ResourcesController@updateCategory')->name('resources.categories.edit');
Route::post('editResourceCategoryOrder', 'ResourcesController@editResourceCategoryOrder')->name('resources.categories.order.edit');

Route::get('resources/topics', 'ResourcesController@topics')->name('resources.topics');
Route::get('resources/topics/create', 'ResourcesController@createTopic')->name('resources.topics.create');
Route::post('resources/topics/create', 'ResourcesController@saveTopic')->name('resources.topics.create');
Route::post('resources/topics/delete', 'ResourcesController@deleteTopic')->name('resources.topics.delete');
Route::get('resources/topics/{id}/edit', 'ResourcesController@editTopic')->name('resources.topics.edit');
Route::post('resources/topics/{id}/edit', 'ResourcesController@updateTopic')->name('resources.topics.edit');
Route::post('editTopicOrder', 'ResourcesController@editOrder')->name('resources.topics.order.edit');

Route::get('blogs/articles', 'BlogController@index')->name('blogs.articles');
Route::get('blogs/articles/create', 'BlogController@createArticle')->name('blogs.articles.create');
Route::post('blogs/articles/create', 'BlogController@saveArticle')->name('blogs.articles.create');
Route::post('blogs/articles/delete', 'BlogController@deleteArticle')->name('blogs.articles.delete');
Route::get('blogs/articles/{id}/edit', 'BlogController@editArticle')->name('blogs.articles.edit');
Route::post('blogs/articles/{id}/edit', 'BlogController@updateArticle')->name('blogs.articles.edit');
Route::post('blogs/articles/generateSlug', 'BlogController@autoGenerateSlug')->name('blogs.articles.generateSlug');
Route::post('blogs/articles/checkSlugDuplicacy', 'BlogController@checkSlugDuplicacy')->name('blogs.articles.checkSlugDuplicacy');

Route::get('blogs/categories', 'BlogController@categories')->name('blogs.categories');
Route::get('blogs/categories/create', 'BlogController@createCategory')->name('blogs.categories.create');
Route::post('blogs/categories/create', 'BlogController@saveCategory')->name('blogs.categories.create');
Route::post('blogs/categories/delete', 'BlogController@deleteCategory')->name('blogs.categories.delete');
Route::get('blogs/categories/{id}/edit', 'BlogController@editCategory')->name('blogs.categories.edit');
Route::post('blogs/categories/{id}/edit', 'BlogController@updateCategory')->name('blogs.categories.edit');

Route::get('uploader', 'HomeController@uploader')->name('uploader');
Route::post('uploader', 'HomeController@saveUploader')->name('saveUploader');

Route::get('assignClients', 'Access\User\UserController@assignClientsToEmployee')->name('assignClients');
Route::post('searchEmployee', 'Access\User\UserController@searchEmployee')->name('searchEmployee');
Route::post('searchClient', 'Access\User\UserController@searchClient')->name('searchClient');
Route::post('storeClientsToEmployee', 'Access\User\UserController@storeClientsToEmployee')->name('storeClientsToEmployee');
Route::get('allclientEmployeeList', 'Access\User\UserController@allclientEmployeeList')->name('allclientEmployeeList');
Route::post('removeClientEmployeeRelation/{id}', 'Access\User\UserController@removeClientEmployeeRelation')->name('removeClientEmployeeRelation');

$middleware = array_merge(\Config::get('lfm.middlewares'), ['\Unisharp\Laravelfilemanager\middleware\MultiUser']);
$prefix = \Config::get('lfm.prefix', 'laravel-filemanager');
$as = 'unisharp.lfm.';
$namespace = '\Unisharp\Laravelfilemanager\controllers';

// make sure authenticated
Route::group(compact('middleware', 'prefix', 'as', 'namespace'), function () {

    // Show LFM
    Route::get('/', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\LfmController@show',
        'as' => 'show'
    ]);

    // upload
    Route::any('/upload', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\UploadController@upload',
        'as' => 'upload'
    ]);

    // list images & files
    Route::get('/jsonitems', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ItemsController@getItems',
        'as' => 'getItems'
    ]);

    // folders
    Route::get('/newfolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getAddfolder',
        'as' => 'getAddfolder'
    ]);
    Route::get('/deletefolder', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getDeletefolder',
        'as' => 'getDeletefolder'
    ]);
    Route::get('/folders', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\FolderController@getFolders',
        'as' => 'getFolders'
    ]);
// crop
    Route::get('/crop', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCrop',
        'as' => 'getCrop'
    ]);
    Route::get('/cropimage', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\CropController@getCropimage',
        'as' => 'getCropimage'
    ]);

    // rename
    Route::get('/rename', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\RenameController@getRename',
        'as' => 'getRename'
    ]);

    // scale/resize
    Route::get('/resize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@getResize',
        'as' => 'getResize'
    ]);
    Route::get('/doresize', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\ResizeController@performResize',
        'as' => 'performResize'
    ]);

    // download
    Route::get('/download', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DownloadController@getDownload',
        'as' => 'getDownload'
    ]);

    // delete
    Route::get('/delete', [
        'uses' => '\Unisharp\Laravelfilemanager\controllers\DeleteController@getDelete',
        'as' => 'getDelete'
    ]);

    Route::get('/demo', '\Unisharp\Laravelfilemanager\controllers\DemoController@index');
});
