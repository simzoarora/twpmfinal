<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMobileNumberColumnAndRenamePhoneVerifiedColumnToMobileVerifiedColumnInProfilesTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->bigInteger('mobile_number')->nullable()->after('phone_type')->comment('User mobile number');
            $table->renameColumn('phone_verified', 'mobile_verified');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('mobile_number');
            $table->renameColumn('mobile_verified', 'phone_verified');
        });
    }

}
