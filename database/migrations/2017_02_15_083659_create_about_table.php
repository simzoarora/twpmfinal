<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAboutTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('about', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('seo_title');
			$table->text('seo_description', 65535);
			$table->string('seo_img');
			$table->text('slide_1_heading', 65535);
			$table->string('slide_2_heading');
			$table->text('slide_2_description', 65535);
			$table->string('slide_3_heading');
			$table->text('slide_3_description', 65535);
			$table->string('slide_5_heading');
			$table->text('slide_5_description', 65535);
			$table->text('sponsor_logo', 65535)->nullable();
			$table->string('sponsor_link');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('about');
	}

}
