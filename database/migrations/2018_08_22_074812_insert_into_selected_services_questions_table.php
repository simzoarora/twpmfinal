<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoSelectedServicesQuestionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('selected_service_questions',
            function (Blueprint $table) {
            DB::table('selected_service_questions')->truncate();
            DB::table('selected_service_questions')->insert(array(
                [
                    'service_id' => '9',
                    'question_id' => '25',
                ],
                [
                    'service_id' => '9',
                    'question_id' => '26',
                ],
                [
                    'service_id' => '9',
                    'question_id' => '27',
                ],
                [
                    'service_id' => '2',
                    'question_id' => '44',
                ],
                [
                    'service_id' => '5',
                    'question_id' => '1',
                ],
                [
                    'service_id' => '4',
                    'question_id' => '36',
                ],
            ));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('selected_service_questions',
            function (Blueprint $table) {
            //
        });
    }
}