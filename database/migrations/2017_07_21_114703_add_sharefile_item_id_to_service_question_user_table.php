<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSharefileItemIdToServiceQuestionUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_question_user', function (Blueprint $table) {
            $table->string('sharefile_item_id')->nullable()->after('value')->comment('Item id returned by share file api on file upload success');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_question_user', function (Blueprint $table) {
             $table->dropColumn('sharefile_item_id');
        });
    }
}
