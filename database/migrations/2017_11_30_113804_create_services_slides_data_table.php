<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesSlidesDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services_slides_data', function (Blueprint $table) {
            $table->increments('id');
            $table->string('heading_1')->nullable();
            $table->text('subheading_1')->nullable();
            $table->string('image_1')->nullable();
            $table->string('heading_2')->nullable();
            $table->text('subheading_2')->nullable();
            $table->string('heading_3')->nullable();
            $table->text('subheading_3')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services_slides_data');
    }
}
