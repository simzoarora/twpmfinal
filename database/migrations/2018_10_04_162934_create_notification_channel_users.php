<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationChannelUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_channel_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notification_channel_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('last_read');
            $table->timestamps();
            $table->foreign('notification_channel_id')->references('id')->on('notification_channels');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_channel_participants');
    }
}
