<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void 
     */
    public function up()
    {
        Schema::table('services', function (Blueprint $table) {
            DB::table('services')->truncate();
            DB::table('services')->insert(array(
            [
                'id' => '1',
                'title'=>'Comprehensive Planning & Management',
                'service_number'=>'8',
                'title_bg_color'=>'#002d62',
                'order'=>'1',
                'image'=>'6-01-17-07-57-05-Comprehensive Planning Management.png',
                'description'=>'<p>This is our premier service. We begin by doing a detailed financial&nbsp;plan and revising it as often as needed. This is a two-step process. First, we have&nbsp;an initial planning meeting to gather all the needed information. Next, we create&nbsp;a net-worth based&nbsp;program that attempts to optimize both sides of our&nbsp;clients&rsquo; balance sheets. Finally, we design a portfolio that attempts to achieve&nbsp;our clients&rsquo; near and long-term goals, attempting to minimize risk&nbsp;while aiming&nbsp;to achieve those goals. Any and all of our other services are included as&nbsp;needed.</p>',
                'recommended_for'=>'<p>At least $250,000 in investable assets.</p>',
                'cost'=>'<p>Our typical clients will pay an annualized fee (1/4th&nbsp;deducted quarterly) based upon the value of the assets we actively manage. &nbsp;The fee is usually between 0.9% and 1.35%, depending upon the value of the assets managed. &nbsp;The one comprehensive&nbsp;fee covers all trading costs, planning and other services.&nbsp; You may request the RJFSA firm brochure for a more complete description of fees and services.</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '2',
                'title'=>'Goal-Specific Financial Planning',
                'service_number'=>'4',
                'title_bg_color'=>'#002d62',
                'order'=>'4',
                'image'=>'16-01-17-08-02-22-Goal-Specific Financial Planning.png',
                'description'=>'<p>This is our service to advise a client on a specific goal or need, such as college planning, vacation home purchase, credit card or other debt reduction, etc. &nbsp;Please, bring all relevant statements and documentation to the meeting.</p>',
                'recommended_for'=>'<p>Individuals, Families</p>',
                'cost'=>'<p>$150 fee for most engagements.&nbsp; Should there be any additional cost, the client wold be informed up front.</p>

<p>Veterans and first responders may receive a 50% discount with proof of service.</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '3',
                'title'=>'Life Insurance & Long-Term Review',
                'service_number'=>'10',
                'title_bg_color'=>'#002d62',
                'order'=>'9',
                'image'=>'16-01-17-08-07-47-Life Insurance Review.png',
                'description'=>'<p>Total&nbsp;Wealth Planning and Management, Inc. does not sell life insurance or Long-Term Care insurance. &nbsp;Since we do not sell insurance, our advisors can give an objective analysis of needs, costs and types of insurance and give you specific and independent recommendations. &nbsp;We will review needs as well as any current policies and quotes.</p>',
                'recommended_for'=>'<p>Individuals, Couples</p>',
                'cost'=>'<p>One-time, $200 per individual.</p>

<p>Veterans and first responders may receive a 50% discount with proof of service.</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '4',
                'title'=>'Educational Consultation',
                'service_number'=>'6',
                'title_bg_color'=>'#002d62',
                'order'=>'8',
                'image'=>'23-01-17-06-09-22-Edu consultation.png',
                'description'=>'<p>This service is available to those seeking general or specific financial information, knowledge or second opinion.&nbsp; The purpose is purely to answer questions and to help others better understand financial matters, investing, markets, financial products, etc.</p>',
                'recommended_for'=>'<p>Individuals who are just beginning or wanting to begin investing and need some direction or who are wanting to learn more about a financial subject.</p>

<p>Individuals who have financial questions but do not have access to another financial professional.</p>',
                'cost'=>'<p>$150 for up to a one hour phone consultation.</p>

<p>$250 for up to a one hour in person consultation.</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '5',
                'title'=>'Debt Reduction Consultation',
                'service_number'=>'7',
                'title_bg_color'=>'#002d62',
                'order'=>'10',
                'image'=>'12-04-17-07-31-15-checkbook-688352_1920.jpg',
                'description'=>'<p>This service is designed to help those who wish to devise a plan to get out of debt.</p>',
                'recommended_for'=>'<p>Anyone who has too much debt and wishes to have a plan to get out of debt.</p>',
                'cost'=>'<p>$100 one time</p>',
                'fee_schedule'=>'',
                'amount'=>'100'
            ],
              [
                'id' => '6',
                'title'=>'Investment Management',
                'service_number'=>'1',
                'title_bg_color'=>'#002d62',
                'order'=>'3',
                'image'=>'16-01-17-07-58-03-Investment Management.png	',
                'description'=>'<p>This is our service for those that do not need the Comprehensive Planning, but are specifically in need of investment planning and management. This might&nbsp;also be clients who have smaller amounts to invest or are just getting started. Statements of investment and&nbsp;bank accounts should be brought to the initial meeting. &nbsp;This service will be provided on an asset-based fee.</p>',
                'recommended_for'=>'<p>Individuals, Families, New Investors, Trusts, Endowments, Non-profits</p>',
                'cost'=>'<p>Varies based upon needs and requirements. A comprehensive annual fee ranges from 1.50% - 1.00%.&nbsp; Non-profits may receive reduced rates.</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '7',
                'title'=>'Annual Portfolio Reviews',
                'service_number'=>'2',
                'title_bg_color'=>'',
                'order'=>'0 ',
                'image'=>'22-12-16-11-34-13-images (5).jpg',
                'description'=>'<h2>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum,&quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</h2>
',
                'recommended_for'=>'<h2>What is Lorem Ipsum?</h2>

<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
',
                'cost'=>'<h2>What is Lorem Ipsum?</h2>

<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>',
                'fee_schedule'=>'<h2><strong><a href="https://en.wikipedia.org/wiki/Andrew_Sledd" title="Andrew Sledd">Andrew Sledd</a></strong>&nbsp;(1870&ndash;1939) was an American&nbsp;<a href="https://en.wikipedia.org/wiki/Theologian" title="Theologian">theologian</a>, university professor and university president. A native of&nbsp;<a href="https://en.wikipedia.org/wiki/Virginia" title="Virginia">Virginia</a>, he was&nbsp;<a href="https://en.wikipedia.org/wiki/Ordination" title="Ordination">ordained</a>as a&nbsp;<a href="https://en.wikipedia.org/wiki/Methodist" title="Methodist">Methodist</a>&nbsp;minister after earning his master&#39;s degree; he later earned a doctorate at&nbsp;<a href="https://en.wikipedia.org/wiki/Yale" title="Yale">Yale</a>. After teaching for several years, Sledd became the last president of the&nbsp;<a href="https://en.wikipedia.org/wiki/History_of_the_University_of_Florida" title="History of the University of Florida">University of Florida at Lake City</a>&nbsp;from 1904 to 1905, and the first president of what is now the&nbsp;<a href="https://en.wikipedia.org/wiki/University_of_Florida" title="University of Florida">University of Florida</a>&nbsp;from 1905 to 1909. He was president of&nbsp;<a href="https://en.wikipedia.org/wiki/Birmingham-Southern_College" title="Birmingham-Southern College">Southern University</a>from 1910 to 1914, and became a professor and an influential biblical scholar at&nbsp;<a href="https://en.wikipedia.org/wiki/Emory_University" title="Emory University">Emory University</a>&#39;s&nbsp;<a href="https://en.wikipedia.org/wiki/Candler_School_of_Theology" title="Candler School of Theology">Candler School of Theology</a>&nbsp;from 1914 to 1939. Bibliographies highlight his 1902 magazine article advocating better legal and social treatment of&nbsp;<a href="https://en.wikipedia.org/wiki/African_Americans" title="African Americans">African Americans</a>, his role in founding the modern University of Florida, his scholarly analysis of biblical texts as literature, his call for an end to racial violence, and his influence on a generation of&nbsp;</h2>
',
                'amount'=>''
            ],
              [
                'id' => '8',
                'title'=>'Semi-Annual 401(k) Review',
                'service_number'=>'5',
                'title_bg_color'=>'#002d62',
                'order'=>'5',
                'image'=>'16-01-17-08-03-49-Semi-annual 401(k) review.png',
                'description'=>'<p>This is a service for those whose assets are held in a 401(k) plan and would like independent advice on their choices offered, allocation and strategy. &nbsp;Please, bring the account statement(s),&nbsp;list of investment choices and the Summary Plan Document. &nbsp;If you have other investment or savings accounts, those statements&nbsp;would be helpful as well. &nbsp;This service is also available If your plan is a 403(b), 457 or other employer-sponsored retirement plan.</p>',
                'recommended_for'=>'<p>Individuals and couples whose largest investment asset is the employer-sponsored plan(s).</p>',
                'cost'=>'<p>With an annual contract, $100 per six-month review, collected at the time of review.</p>

<p>One-time, $250 for the first person, $100 for the second person, such as a spouse or partner. &nbsp;</p>

<p>&nbsp;</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
              [
                'id' => '9',
                'title'=>'Stock Option Exercise Strategy and Executive Compensation Analysis',
                'service_number'=>'9',
                'title_bg_color'=>'#002d62',
                'order'=>'7',
                'image'=>'16-01-17-08-05-58-Stock Option Exercise Strategy.png',
                'description'=>'<p>This service is for those who are compensated with stock options, restricted stock units, deferred compensation, etc. &nbsp;Please, bring plan details and statements. &nbsp;This is usually a two session process. &nbsp;The first session will consist of information gathering. &nbsp;The plan will then be analyzed with consideration given to the individual situation and a detailed strategy recommendation will be provided.</p>',
                'recommended_for'=>'<p>Executives</p>',
                'cost'=>'<p>One-time, $500 per individual</p>',
                'fee_schedule'=>'',
                'amount'=>''
            ],
             ));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('services', function (Blueprint $table) {
            //
            
        });
    }
}
