<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertIntoTopicQuestions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('topic_questions', function (Blueprint $table) {
            DB::table('topic_questions')->truncate();
            DB::table('topic_questions')->insert(array(
                [
                    'id' => '1',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '47',
                ],
                [
                    'id' => '2',
                    'service_id' => '1',
                    'topic_id' => '2',
                    'question_id' => '0',
                ],
                [
                    'id' => '3',
                    'service_id' => '1',
                    'topic_id' => '3',
                    'question_id' => '49',
                ],
                [
                    'id' => '4',
                    'service_id' => '1',
                    'topic_id' => '3',
                    'question_id' => '491',
                ],
                [
                    'id' => '5',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '84',
                ],
                [
                    'id' => '6',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '85',
                ],
                [
                    'id' => '7',
                    'service_id' => '2',
                    'topic_id' => '201',
                    'question_id' => '43',
                ],
                [
                    'id' => '8',
                    'service_id' => '2',
                    'topic_id' => '202',
                    'question_id' => '42',
                ],
                [
                    'id' => '9',
                    'service_id' => '2',
                    'topic_id' => '203',
                    'question_id' => '41',
                ],
                [
                    'id' => '10',
                    'service_id' => '2',
                    'topic_id' => '204',
                    'question_id' => '40',
                ],
                [
                    'id' => '11',
                    'service_id' => '2',
                    'topic_id' => '205',
                    'question_id' => '39',
                ],
                [
                    'id' => '12',
                    'service_id' => '3',
                    'topic_id' => '301',
                    'question_id' => '45',
                ],
                [
                    'id' => '13',
                    'service_id' => '3',
                    'topic_id' => '302',
                    'question_id' => '34',
                ],
                [
                    'id' => '14',
                    'service_id' => '3',
                    'topic_id' => '303',
                    'question_id' => '33',
                ],
                [
                    'id' => '15',
                    'service_id' => '3',
                    'topic_id' => '304',
                    'question_id' => '32',
                ],
//                Models inside models
                [
                    'id' => '16',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '841',
                ],
                [
                    'id' => '17',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '842',
                ],
                [
                    'id' => '18',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '843',
                ],
                [
                    'id' => '19',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '844',
                ],
                [
                    'id' => '20',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '845',
                ],
                [
                    'id' => '21',
                    'service_id' => '1',
                    'topic_id' => '11',
                    'question_id' => '846',
                ],
                [
                    'id' => '22',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '851',
                ],
                [
                    'id' => '23',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '852',
                ],
                [
                    'id' => '24',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '853',
                ],
                [
                    'id' => '25',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '854',
                ],
                [
                    'id' => '26',
                    'service_id' => '1',
                    'topic_id' => '12',
                    'question_id' => '855',
                ],
                [
                    'id' => '27',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '471',
                ],
                [
                    'id' => '28',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '472',
                ],
                [
                    'id' => '29',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '473',
                ],
                [
                    'id' => '30',
                    'service_id' => '1',
                    'topic_id' => '4',
                    'question_id' => '50',
                ],
                [
                    'id' => '31',
                    'service_id' => '5',
                    'topic_id' => '501',
                    'question_id' => '2',
                ],
                [
                    'id' => '32',
                    'service_id' => '5',
                    'topic_id' => '502',
                    'question_id' => '3',
                ],
                [
                    'id' => '33',
                    'service_id' => '5',
                    'topic_id' => '503',
                    'question_id' => '7',
                ],
                [
                    'id' => '34',
                    'service_id' => '5',
                    'topic_id' => '504',
                    'question_id' => '8',
                ],
                [
                    'id' => '35',
                    'service_id' => '5',
                    'topic_id' => '505',
                    'question_id' => '10',
                ],
                [
                    'id' => '36',
                    'service_id' => '5',
                    'topic_id' => '506',
                    'question_id' => '14',
                ],
                [
                    'id' => '37',
                    'service_id' => '5',
                    'topic_id' => '507',
                    'question_id' => '17',
                ],
                [
                    'id' => '38',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '52',
                ],
                [
                    'id' => '39',
                    'service_id' => '7',
                    'topic_id' => '701',
                    'question_id' => '207',
                ],
                [
                    'id' => '40',
                    'service_id' => '3',
                    'topic_id' => '305',
                    'question_id' => '28',
                ],
                [
                    'id' => '41',
                    'service_id' => '8',
                    'topic_id' => '803',
                    'question_id' => '205',
                ],
                [
                    'id' => '42',
                    'service_id' => '7',
                    'topic_id' => '703',
                    'question_id' => '214',
                ],
                [
                    'id' => '43',
                    'service_id' => '8',
                    'topic_id' => '801',
                    'question_id' => '23',
                ],
                [
                    'id' => '44 ',
                    'service_id' => '3',
                    'topic_id' => '304',
                    'question_id' => '321',
                ],
                [
                    'id' => '45',
                    'service_id' => '3',
                    'topic_id' => '304',
                    'question_id' => '322',
                ],
                [
                    'id' => '46',
                    'service_id' => '2',
                    'topic_id' => '204',
                    'question_id' => '38',
                ],
                [
                    'id' => '47',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '53',
                ],
                [
                    'id' => '48',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '54',
                ],
                [
                    'id' => '49',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '55',
                ],
                [
                    'id' => '50',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '56',
                ],
                [
                    'id' => '51',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '57',
                ],
                [
                    'id' => '52',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '58',
                ],
                [
                    'id' => '53',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '59',
                ],
                [
                    'id' => '54',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '60',
                ],
                [
                    'id' => '55',
                    'service_id' => '1',
                    'topic_id' => '7',
                    'question_id' => '216',
                ],
                [
                    'id' => '56',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '218',
                ],
                [
                    'id' => '57',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '219',
                ],
                [
                    'id' => '58',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '220',
                ],
                [
                    'id' => '59',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '221',
                ],
                [
                    'id' => '60',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '222',
                ],
                [
                    'id' => '61',
                    'service_id' => '1',
                    'topic_id' => '1',
                    'question_id' => '223',
                ],
            ));
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('topic_questions', function (Blueprint $table) {
            //
        });
    }

}
