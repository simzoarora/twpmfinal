<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToArticleBlogCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('article_blog_categories', function(Blueprint $table)
		{
			$table->foreign('article_id', 'article_blog_categories_ibfk_1')->references('id')->on('articles')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('blog_category_id', 'article_blog_categories_ibfk_2')->references('id')->on('blog_categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('article_blog_categories', function(Blueprint $table)
		{
			$table->dropForeign('article_blog_categories_ibfk_1');
			$table->dropForeign('article_blog_categories_ibfk_2');
		});
	}

}
