<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttempedQuestionInputsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attemped_question_inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_input_id')->unsigned();
            $table->foreign('question_input_id')->references('id')->on('question_inputs')->onDelete('cascade');
            $table->text('answer');
            $table->timestamps(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('attemped_question_inputs');
    }

}
