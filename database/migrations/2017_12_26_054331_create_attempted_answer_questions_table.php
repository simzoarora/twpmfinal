<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttemptedAnswerQuestionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('attempted_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('attempted_question_id')->unsigned();
            $table->foreign('attempted_question_id')->references('id')->on('attempted_questions')->onDelete('cascade');
            $table->integer('question_answer_id')->unsigned();
            $table->foreign('question_answer_id')->references('id')->on('question_answers')->onDelete('cascade');
            $table->string('question_answer_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('attempted_answer_questions');
    }

}
