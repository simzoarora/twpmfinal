<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_topics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->string('title_bg_color', 7);
			$table->integer('order');
			$table->string('image');
			$table->integer('resource_category_id')->unsigned()->index('resource_category_id');
			$table->text('content', 65535);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_topics');
	}

}
