<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateResourceTopicsFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_topics_files', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resource_topic_id')->unsigned()->index('resource_topic_id');
			$table->string('file_text');
			$table->string('upload_file');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('resource_topics_files');
	}

}
