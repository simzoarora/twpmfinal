<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProfileTagIdToServiceQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_questions', function (Blueprint $table) {
            $table->integer('profile_tag_id')->nullable()->unsigned()->after('service_id')->comment('store extra profile info type ');
            $table->foreign('profile_tag_id')->references('id')->on('profile_tags')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_questions', function (Blueprint $table) {
            //
        });
    }
}
