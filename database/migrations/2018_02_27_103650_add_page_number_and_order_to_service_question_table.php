<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPageNumberAndOrderToServiceQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('service_question', function (Blueprint $table) {
            $table->tinyInteger('page_number')->after('question_id')->nullable();
            $table->tinyInteger('order')->after('page_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('service_question', function (Blueprint $table) {
            $table->dropColumn('page_number');
            $table->dropColumn('order');
        });
    }
}
