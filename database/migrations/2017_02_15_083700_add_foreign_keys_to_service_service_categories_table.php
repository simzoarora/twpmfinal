<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToServiceServiceCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('service_service_categories', function(Blueprint $table)
		{
			$table->foreign('service_id', 'service_service_categories_ibfk_1')->references('id')->on('services')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('service_category_id', 'service_service_categories_ibfk_2')->references('id')->on('service_categories')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('service_service_categories', function(Blueprint $table)
		{
			$table->dropForeign('service_service_categories_ibfk_1');
			$table->dropForeign('service_service_categories_ibfk_2');
		});
	}

}
