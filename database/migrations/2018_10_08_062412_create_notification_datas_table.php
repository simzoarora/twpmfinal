<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notification_room_id')->references('id')->on('notification_rooms');
            $table->integer('sender_id')->references('id')->on('users');
            $table->integer('receiver_id')->references('id')->on('users');
            $table->text('notification');
            $table->text('notification');
            $table->boolean('is_read');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_datas');
    }
}
