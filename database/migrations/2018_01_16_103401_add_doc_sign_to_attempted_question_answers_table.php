<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDocSignToAttemptedQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attempted_question_answers', function (Blueprint $table) {
            $table->tinyInteger('doc_sign')->nullable()->comment('NULL => Not Required, 0 => Not Sign, 1 => Signed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attempted_question_answers', function (Blueprint $table) {
            //
        });
    }
}
