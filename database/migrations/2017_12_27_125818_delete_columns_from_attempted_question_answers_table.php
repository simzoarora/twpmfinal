<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteColumnsFromAttemptedQuestionAnswersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('attempted_question_answers', function (Blueprint $table) {
            $table->dropColumn(['question_answer_id', 'question_answer_text', 'answer_type']);
            $table->string('answer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('attempted_question_answers', function (Blueprint $table) {
            //
        });
    }

}
