<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('seo', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('home_seo_title');
			$table->text('home_seo_description', 65535);
			$table->string('home_seo_img');
			$table->string('about_seo_title');
			$table->text('about_seo_description', 65535);
			$table->string('about_seo_img');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('seo');
	}

}
