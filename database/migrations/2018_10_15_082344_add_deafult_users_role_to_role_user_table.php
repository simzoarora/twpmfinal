<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Access\User\User;

class AddDeafultUsersRoleToRoleUserTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_user',
            function (Blueprint $table) {
            $users    = User::has('profile')->with('profile')->get();
            $roleUser = DB::table('role_user')->select('user_id')->get();
            $roleName = DB::table('roles')->select('id')->where('name',
                    config('access.users.default_role'))->first();
            $roles    = [];
            if (!empty($roleUser)) {
                foreach ($roleUser as $info) {
                    array_push($roles, $info->user_id);
                }
            }
            $inserts = [];
            if (!empty($users)) {
                foreach ($users as $data) {
                    $id = $data->profile->user_id;
                    if (!in_array($id, $roles)) {
                        $temp['user_id'] = $data->profile->user_id;
                        $temp['role_id'] = $roleName->id;
                        array_push($inserts, $temp);
                    }
                }
            }
            if (!empty($inserts)) {
                $records = DB::table('role_user')->insert($inserts);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_user',
            function (Blueprint $table) {
            //
        });
    }
}