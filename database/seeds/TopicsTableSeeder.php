<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
//        "vendor/docusign/esign-client/autoload.php"
         DB::table('topics')->insert(array(
            [
                'id' => '1',
                'service_id' => '35',
                'name' => 'Liabilities',
            ],
             [
                'id' => '2',
                 'service_id' => '35',
                'name' => 'Income Tax',
            ],
             [
                'id' => '3',
                 'service_id' => '35',
                'name' => 'Living Expenses & Savings',
            ],
             [
                'id' => '4',
                 'service_id' => '35',
                'name' => 'Personal Information',
            ],
             [
                'id' => '5',
                 'service_id' => '35',
                'name' => 'Family Information',
            ],
             [
                'id' => '6',
                 'service_id' => '35',
                'name' => 'Charities',
            ],
             [
                'id' => '7',
                 'service_id' => '35',
                'name' => 'Employment, Income, Retirement Plans',
            ],
             [
                'id' => '8',
                 'service_id' => '35',
                'name' => 'Estate Planning',
            ],
             [
                'id' => '9',
                 'service_id' => '35',
                'name' => 'Insurance',
            ],
             [
                'id' => '10',
                 'service_id' => '35',
                'name' => 'Assets',
            ],
             [
                'id' => '11',
                 'service_id' => '35',
                'name' => 'Investment Experience',
            ],
             [
                'id' => '12',
                 'service_id' => '36',
                'name' => 'About You & Your Family',
            ],
             [
                'id' => '13',
                 'service_id' => '36',
                'name' => 'Income & Investment',
            ],
             [
                'id' => '14',
                 'service_id' => '36',
                'name' => 'Real Estate Assets',
            ],
             [
                'id' => '15',
                 'service_id' => '36',
                'name' => 'Goal Details',
            ],
             [
                'id' => '16',
                'service_id' => '36',
                'name' => 'Additional Notes',
            ],
             [
                'id' => '17',
                'service_id' => '13',
                'name' => 'Select Account Type',
            ],
             [
                'id' => '18',
                'service_id' => '13',
                'name' => 'Account Qwners Information',
            ],
             [
                'id' => '19',
                'service_id' => '13',
                'name' => 'Combined Income/ Net Worth',
            ],
             [
                'id' => '20',
                'service_id' => '13',
                'name' => 'Tax Exemption',
            ],
             [
                'id' => '21',
                'service_id' => '13',
                'name' => 'Investment Policy Statment',
            ],
             [
                'id' => '22',
                'service_id' => '13',
                'name' => 'Deposits',
            ],
             [
                'id' => '23',
                'service_id' => '13',
                'name' => 'Other Accounts',
            ],
             
             ));
    }
}
