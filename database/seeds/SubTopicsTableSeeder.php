<?php

use Illuminate\Database\Seeder;

class SubTopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('sub_topics')->insert(array(
            [
                'id' => '1',
                'topic_id' => '1',
                'name' => 'Credit score',
            ],
             ));
    }
}
