<?php

use Illuminate\Database\Seeder;

class SelectedServiceQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('selected_service_questions')->insert(array(
            [
                'id' => '1',
                'service_id' => '',
                'question_id' => '',
            ],
             ));
    }
}
