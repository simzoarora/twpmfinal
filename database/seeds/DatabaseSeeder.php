<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DatabaseSeeder
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(AccessTableSeeder::class);
        $this->call(HistoryTypeTableSeeder::class);
        $this->call(TopicsQuestionsTableSeeder::class);
        $this->call(SubTopicsQuestionsTableSeeder::class);
        $this->call(SelectedServiceQuestionsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);

        Model::reguard();
    }
}
