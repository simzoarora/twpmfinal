<?php

use Illuminate\Database\Seeder;

class TopicsQuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('topic_questions')->insert(array(
            [
                'id' => '1',
                'service_id' => '1',
                'topic_id' => '1',
                'question_id' => '47',
            ],
            [
                'id' => '2',
                'service_id' => '1',
                'topic_id' => '2',
                'question_id' => '0',
            ],
            [
                'id' => '3',
                'service_id' => '1',
                'topic_id' => '3',
                'question_id' => '49',
            ],
            [
                'id' => '4',
                'service_id' => '1',
                'topic_id' => '3',
                'question_id' => '49_1',
            ],
            [
                'id' => '5',
                'service_id' => '1',
                'topic_id' => '11',
                'question_id' => '84',
            ],
            [
                'id' => '6',
                'service_id' => '1',
                'topic_id' => '12',
                'question_id' => '85',
            ],



             ));
    }
}
