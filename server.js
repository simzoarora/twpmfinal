var app = require('express')();
var server = require('http').Server(app);
global.io = require('socket.io')(server);
var redis = require('redis');
server.listen(8890);
users = {};
//server.listen(3000, function () {
//    console.log('Listening on Port 3000');
//});
var redisClient = redis.createClient();

//if you set a password for your redis server
/*
 redisClient.auth('password', function(err){
 if (err) throw err;
});
 */

redisClient.subscribe('message');

// redisClient.on("message", function(channel, data) {
// 	var data = JSON.parse(data);
// 	if(data.client_id in users){
// 		if(data.conversation_id in users[data.client_id]){
// 			users[data.client_id][data.conversation_id].emit("message", {"conversation_id":data.conversation_id,"msg":data.text});
// 		}
// 	}
// });
redisClient.on("message", function (channel, data) {
    var data = JSON.parse(data);
    if (data.client_id in users) {
        if (data.notification_id in users[data.client_id]) {
            users[data.client_id][data.notification_id].emit("message", {"notification_id": data.notification_id, "msg": data.text});
        }
    }
});
io.on('connection', function (socket) {

    socket.on("adduser", function (data) {
        if (!(data.client in users)) {
            
            users[data.client] = {};
        }
        users[data.client][data.notification] = socket;
        
        socket.user_id = data.client;
        socket.notification_id = data.notification;
    });

    socket.on('disconnect', function () {
        if (!(socket.user_id in users))
            return;
        if (!(socket.notification_id in users[socket.user_id]))
            return;

        delete users[socket.user_id][socket.notification_id];
        if (Object.keys(users[socket.user_id]).length === 0) {
            delete users[socket.user_id];
        }
    });

});