<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FinancialProfile extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'tax_filing_status',
        'annual_pretax_income',
        'dependents',
        'employment_status',
        'household_investable_assets'
    ];

    public function user() {
        return $this->belongsTo('App\Models\Access\User\User');
    }

}
