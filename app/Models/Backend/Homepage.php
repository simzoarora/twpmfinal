<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Backend\LogoBox;

class Homepage extends Model
{

    protected $fillable = ['bg_image_1', 'heading_1', 'subheading_1',
        'heading_3', 'subheading_3','image_3', 'image_4', 'heading_4', 'description_4',
        'linktext_4', 'bg_image_6', 'heading_6', 'description_6', 'linktext_6',
        'heading_7','seo_title','seo_description','seo_img'];
    protected $table = 'homepage';
    public $timestamps = true;

}
