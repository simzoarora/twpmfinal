<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{

    protected $fillable = [
        'home_seo_title', 
        'home_seo_description',
        'home_seo_img', 
        'about_seo_title', 
        'about_seo_description', 
        'about_seo_img'
        ];
    protected $table = 'seo';
    public $timestamps = true;

}
