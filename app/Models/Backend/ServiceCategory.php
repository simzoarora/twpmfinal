<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceCategory extends Model
{

    protected $fillable = ['name', 'logo'];
    protected $table = 'service_categories';
    public $timestamps = true;

    use SoftDeletes;

}
