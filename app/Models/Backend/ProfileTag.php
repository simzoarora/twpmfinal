<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Backend\ServiceQuestion;
use Auth;

class ProfileTag extends Model {

    use SoftDeletes;

    protected $table = 'profile_tags';
    protected $dates = ['deleted_at'];
    protected $fillable = ['name'];

    public function serviceQuestionWithAnswers()
    {
        return $this->hasMany(ServiceQuestion::class)->whereHas('service', function($q) {
                    $q->whereHas('users', function($q) {
                        $q->where('users.id', '=', Auth::id());
                    });
                })->with(['serviceQuestionUser' => function($subQuery) {
                        $subQuery->where('user_id', Auth::id());
                    }]);
    }

    public function serviceQuestions()
    {
        return $this->hasMany(ServiceQuestion::class, 'profile_tag_id', 'id');
    }

}
