<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;

class ClientToEmployeeUser extends Model {

    protected $table = 'client_to_employee_users';
    protected $fillable = ['employee_user_id', 'client_user_id'];

    public function clientUsers()
    {
        return $this->belongsTo(User::class, 'client_user_id');
    }

    public function employeeUser()
    {
        return $this->belongsTo(User::class, 'employee_user_id');
    }

}
