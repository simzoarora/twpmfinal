<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BlogCategory extends Model
{

    protected $fillable = ['title', 'description'];
    public $timestamps = true;

    use SoftDeletes;
    
}
