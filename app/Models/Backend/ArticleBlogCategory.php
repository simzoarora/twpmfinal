<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ArticleBlogCategory extends Model
{

    protected $fillable = ['article_id', 'blog_category_id'];
    protected $table = 'article_blog_categories';
    public $timestamps = true;
    
}
