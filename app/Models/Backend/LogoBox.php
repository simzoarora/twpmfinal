<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class LogoBox extends Model
{

    protected $fillable = ['box_logo', 'box_heading', 'box_description'];
    protected $table = 'logo_boxes';
    public $timestamps = true;

    public static function saveContent($input)
    {
        $logoBoxArray = self::_getLogoBoxArray($input);
        self::_clearOldData();

        foreach ($logoBoxArray as $logoBox) {
            self::create([
                'box_logo' => $logoBox['box_logo'],
                'box_heading' => $logoBox['box_heading'],
                'box_description' => $logoBox['box_description']
            ]);
        }
    }

    private static function _clearOldData()
    {
        $checkEmpty = self::get()->count();
        if ($checkEmpty) {
            self::truncate();
        }
    }

    private static function _getLogoBoxArray($input)
    {
//        dd($input);
        $dataArray = [];
        foreach ($input['box_logo'] as $key => $boxLogo) {
            if ($boxLogo['name'] != "") {
                $image_name = date('d-m-y-h-i-s-') . $boxLogo['name'];
                $img = explode(',', $boxLogo['data'])[1];
                self::_uploadBase64Image(public_path() . '/img/backend/homepage/logobox', $image_name, base64_decode($img));
                $dataArray[$key]['box_logo'] = $image_name;
            } else {
                $dataArray[$key]['box_logo'] = $input['box_logo_old'][$key];
            }
        }
        foreach ($input['box_heading'] as $key => $boxHeading) {
            if (empty($boxHeading))
                continue;
            $dataArray[$key]['box_heading'] = $boxHeading;
        }
        foreach ($input['box_description'] as $key => $boxDescription) {
            if (empty($boxDescription))
                continue;
            $dataArray[$key]['box_description'] = $boxDescription;
        } 
        return $dataArray;
    }

    private static function _uploadBase64Image($path, $file, $imgdata)
    {
        $handle = fopen($path . DS . $file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

}
