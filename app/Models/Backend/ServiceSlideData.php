<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of ServiceSlideData
 *
 * @author pc8
 */
class ServiceSlideData extends Model
{
    //put your code here
    protected $fillable = ['heading_1', 'subheading_1','image_1','heading_2', 'subheading_2', 'heading_3', 'subheading_3'];
    protected $table    = 'services_slides_data';

}