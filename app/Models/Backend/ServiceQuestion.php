<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\User;
use App\Models\Backend\ProfileTag;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceQuestionUser;

class ServiceQuestion extends Model {

    protected $fillable = ['service_id', 'question', 'type','order'];
    protected $table = 'service_questions';
    protected $dates = ['deleted_at'];

    use SoftDeletes;

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
    
    public function serviceQuestionOption()
    {
        return $this->hasMany('App\Models\Backend\ServiceQuestionOption');
    }
    
    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    
    public function profileTags()
    {
        return $this->belongsTo(ProfileTag::class,'profile_tag_id');
    }
    
    public function serviceQuestionUser()
    {
        return $this->hasMany(ServiceQuestionUser::class);
    }
    
    
}
