<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceServiceCategory extends Model
{

    protected $fillable = ['service_id', 'service_category_id'];
    protected $table = 'service_service_categories';
    public $timestamps = true;
    
}
