<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{

    protected $fillable = ['content'];
    protected $table = 'terms';
    public $timestamps = true;

}