<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;

class EmployeeNote extends Model
{
    protected $table    = 'employee_notes';
    protected $fillable = ['client_id', 'employee_id','note'];

    public function clientUsers()
    {
        return $this->belongsTo(User::class, 'client_id');
    }
    
    public function employeeUser()
    {
        return $this->belongsTo(User::class, 'employee_id');
    }
}