<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceQuestionOption extends Model {

    protected $fillable = ['service_question_id', 'name'];
    protected $table = 'service_question_options';
    protected $dates = ['deleted_at'];

    use SoftDeletes;

    public function serviceQuestion()
    {
        return $this->belongsTo('App\Http\Models\Backend\ServiceQuestion');
    }

}
