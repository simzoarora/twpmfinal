<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ResourceTopic extends Model
{

    protected $fillable = ['title','title_bg_color','order', 'is_client', 'image', 'resource_category_id','content'];
    protected $table = 'resource_topics';
    public $timestamps = true;

    use SoftDeletes;
    
    public function resourceCategories(){
        return $this->belongsTo('App\Models\Backend\ResourceCategory','resource_category_id');
    }

    public function resourceTopicFiles(){
        return $this->hasMany('App\Models\Backend\ResourceTopicFile');
    }    
}
