<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;

class ServiceQuestionUser extends Model {

    protected $table = 'service_question_user';
    
    public function users()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
