<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{

    protected $fillable = ['author', 'slug', 'title','image','body','excerpt','status','publish_at'];
    public $timestamps = true;

    use SoftDeletes;
    
    public function blogCategories(){
        return $this->belongsToMany('App\Models\Backend\BlogCategory','article_blog_categories');
    }
    
    public function articleAuthor(){
        return $this->belongsTo('App\Models\Access\User\User','author');
    }
    
}
