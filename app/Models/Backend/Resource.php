<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Resource
 *
 * @author pc8
 */
class Resource extends Model
{
    //put your code here
    protected $fillable = ['heading_1', 'subheading_1', 'heading_2', 'subheading_2'];
    protected $table    = 'resources';

}