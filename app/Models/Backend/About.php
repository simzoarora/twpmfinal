<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use App\Models\Backend\LogoBox;

class About extends Model {
    protected $fillable = [
        'slide_1_heading',
        'slide_1_description',
        'slide_1_image',
        'slide_2_heading',
        'slide_2_description',
        'slide_3_heading',
        'slide_3_description',
        'slide_5_heading',
        'slide_5_description',
        'sponsor_logo',
        'sponsor_link','seo_title','seo_description','seo_img'
    ];
    protected $table = 'about';
    public $timestamps = true;
}
