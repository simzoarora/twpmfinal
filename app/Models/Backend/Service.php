<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Backend\ServiceFile;
use App\Models\Backend\ServiceServiceCategory;
use App\Models\Access\User\User;
use App\Models\Backend\ServiceQuestion;

class Service extends Model {

    protected $fillable = ['title', 'title_bg_color', 'order', 'image', 'description', 'fee_schedule', 'recommended_for', 'cost', 'amount'];
    protected $table = 'services';
    public $timestamps = true;

    use SoftDeletes;

    public function serviceCategories() {
        return $this->belongsToMany('App\Models\Backend\ServiceCategory', 'service_service_categories');
    }

    public function serviceFiles() {
        return $this->hasMany('App\Models\Backend\ServiceFile');
    }

    public function serviceQuestion() {
        return $this->hasMany('App\Models\Backend\ServiceQuestion');
    }

    public function users() {
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function questions() {
        return $this->belongsToMany('App\Models\Question', 'service_question')->withPivot('page_number', 'order')
                        ->addSelect('questions.*', 'service_question.page_number', 'service_question.order as page_question_order');
    }

    public function attemptedQuestions() {
        return $this->hasMany('App\Models\AttemptedQuestion');
    }

    public function serviceTopics() {
                return $this->hasMany('App\Models\Topics');

    }
    public function serviceUsers() {
                return $this->hasMany('App\Models\Backend\ServiceUser');

    }

}
