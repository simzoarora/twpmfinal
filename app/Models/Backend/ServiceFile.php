<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ServiceFile extends Model
{

    protected $fillable = ['service_id', 'file_text', 'upload_file'];
    protected $table = 'services_files';
    public $timestamps = true;
    
    public function services(){
        return $this->belongsTo('App\Models\Backend\Service');
    }

}
