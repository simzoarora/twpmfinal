<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'user_id',
        'first_name',
        'middle_name',
        'last_name',
        'first_address',
        'last_address',
        'city',
        'state_id',
        'zip',
        'phone_number',
        'phone_type',
        'sex',
        'dob',
        'ssn',
        'profile_complete_step',
        'mobile_number'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State');
    }

}
