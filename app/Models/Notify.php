<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notify extends Model
{
     protected $fillable = [
        'text'
    ];
     public function notification()
    {
        return $this->belongsTo('App\Models\Notification');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }
}
