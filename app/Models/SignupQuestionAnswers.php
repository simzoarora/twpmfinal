<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SignupQuestion;
use App\Models\UserSignupQuestionAnswer;
use Illuminate\Database\Eloquent\SoftDeletes;

class SignupQuestionAnswers extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'signup_question_answers';
    protected $dates = ['deleted_at'];

    use SoftDeletes;

    public function signUpQuestion()
    {
        return $this->belongsTo(SignupQuestion::class);
    } 

    public function userSignUpQuestionAnswer()
    {
        return $this->hasMany(UserSignupQuestionAnswer::class);
    }

}
