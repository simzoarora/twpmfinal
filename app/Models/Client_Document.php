<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\Document_Category;
class Client_Document extends Model
{
    public function user(){
           return $this->belongsTo(User::class);
    }
    public function documentCategory(){
                   return $this->belongsTo(Document_Category::class);

    }
}
