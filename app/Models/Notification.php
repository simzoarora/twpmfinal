<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{

    //
    public function notifies()
    {
        return $this->hasMany('App\Models\Notify')->orderBy('created_at', 'asc');
    }

    public function sender()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'sender');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'receiver');
    }

}
