<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserAccountInfo extends Model
{
     protected $table = 'user_account_info';
    
    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }
}
