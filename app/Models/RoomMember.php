<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomMember extends Model
{

    public $timestamps = false;

	protected $fillable = [
	'notification_room_id', 'user_ids'
	];
        public function notificationRoom()
 	{
 		return $this->belongsTo('App\Models\Notification_room');
 	}

 	public function getUserIdsAttribute($value)
 	{
 		return unserialize($value);
 	}

 	public function scopeMembers($query)
 	{
 		return $query->where('active', 1);
 	}
}
