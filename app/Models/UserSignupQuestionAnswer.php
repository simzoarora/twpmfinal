<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SignupQuestionAnswers;
use App\Models\UserSignupQuestion;

class UserSignupQuestionAnswer extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_signup_question_answers';

    public function userSignUpQuestion()
    {
        return $this->belongsTo(UserSignupQuestion::class);
    }

    public function signUpQuestionAnswer()
    {
        return $this->belongsTo(SignupQuestionAnswers::class,'signup_question_answer_id');
    } 

}
