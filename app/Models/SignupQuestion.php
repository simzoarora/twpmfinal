<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\SignupQuestionAnswers;
use App\Models\UserSignupQuestion;
use Illuminate\Database\Eloquent\SoftDeletes;

class SignupQuestion extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'signup_questions';
    protected $dates = ['deleted_at'];

    use SoftDeletes;

    public function signUpQuestionAnswer()
    {
        return $this->hasMany(SignupQuestionAnswers::class);
    }

    public function userSignupQuestion()
    {
        return $this->hasMany(UserSignupQuestion::class);
    } 

}
