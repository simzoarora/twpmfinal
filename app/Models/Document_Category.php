<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Document_Category;

class Document_Category extends Model
{
     public function documentCategories(){
        return $this->hasMany(Client_Document::class);
    }
}
