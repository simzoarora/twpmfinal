<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionInput extends Model
{
    protected $table = 'inputs';
     public function questions() {
        return $this->belongsToMany('App\Models\Question','question_input', 'input_id', 'question_id');
//        return $this->belongsToMany('App\Models\Question','attempted_question_answer_questions', 'question_input_id', 'question_id');
    }
}
