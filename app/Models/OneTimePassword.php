<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OneTimePassword extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'one_time_passwords';

    public function user()
    {
        return $this->belongsTo('App\Models\Access\User\User');
    }

}
