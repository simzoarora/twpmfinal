<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification_room extends Model
{

    protected $fillable = [
        'room_type', 'user_ids'
    ];

    /**
     * Get the messages of a chat room
     */
    public function notifications()
    {
        return $this->hasMany('App\Models\Notification_data', 'notification_room_id')->with('sender');
    }

}
