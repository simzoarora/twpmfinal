<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TestQuestions extends Model {

    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function answers() {
        return $this->hasMany('App\Models\TestQuestionAnswers','test_question_id');
    }

}
