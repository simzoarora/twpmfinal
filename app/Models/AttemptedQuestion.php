<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttemptedQuestion extends Model
{
   public function service() {
       return $this->belongsTo('App\Models\Backend\Service');
   }

   public function question() {
       return $this->belongsTo('App\Models\Question');
   }
   
   public function attemptedQuestionAnswer() {
      return $this->hasOne('App\Models\AttemptedQuestionAnswer');
   }
}
