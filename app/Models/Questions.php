<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

class Questions extends Model {

    protected $guarded = ['id', 'created_at', 'updated_at'];
   

    public function answers() {
        return $this->hasMany('App\Models\QuestionAnswers', 'question_id');
    }

    public function topic() {
        return $this->belongsTo('App\Models\Topics', 'topic_id');
    }

    public function children() {
        return $this->hasMany(static::class, 'parent_id');
    }
    public function parent() {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function firstChild() { 
        return $this->hasOne(Questions::class, 'parent_id')->orderBy('id', 'asc');
    }

    public function getAllChildren() {
        $questions = new Collection();

        foreach ($this->children as $question) {
            $questions->push($question);
            $questions = $questions->merge($question->getAllChildren());
        }

        return $questions;
    }

    public function recursiveChildren() {
        $clientId = Session::get('clientId');
        return $this->children()->with(
                        [
                            'recursiveChildren.answers' => function($query) use ($clientId) {
                                $query->where('user_id', $clientId);
                            }, 'parent.answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }
        ]);
    }

}
