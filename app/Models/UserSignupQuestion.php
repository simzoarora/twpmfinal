<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\UserSignupQuestionAnswer;
use App\Models\SignupQuestion;
use App\Models\Access\User\User;

class UserSignupQuestion extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_signup_questions';

    public function userSignupQuestionAnswer()
    {
        return $this->hasMany(UserSignupQuestionAnswer::class);
    }

    public function signUpQuestion()
    {
        return $this->belongsTo(SignupQuestion::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    } 

}
