<?php

namespace App\Models\Access\User;

use Illuminate\Notifications\Notifiable;
use App\Models\Access\User\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\User\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\User\Traits\UserSendPasswordReset;
use App\Models\Access\User\Traits\Attribute\UserAttribute;
use App\Models\Access\User\Traits\Relationship\UserRelationship;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceQuestion;
use App\Models\UserSignupQuestion;
use App\Models\Client_Document;
use App\Models\Notification;


/**
 * Class User
 * @package App\Models\Access\User
 */
class User extends Authenticatable {

    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'status', 'confirmation_code', 'confirmed', 'two_factor_auth'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }

    public function profile() {
        return $this->hasOne('App\Models\Profile');
    }

    public function financialProfile() {
        return $this->hasOne('App\Models\FinancialProfile');
    }

    public function services() {
        return $this->belongsToMany(Service::class)->withTimestamps();
    }

    public function serviceQuestions() {
        return $this->belongsToMany(ServiceQuestion::class)->withTimestamps();
    }

    public function profileServiceQuestions() {
        return $this->belongsToMany(ServiceQuestion::class)->where('profile_tag_id', '!=', NULL)->with('profileTags')->withPivot('id', 'value');
    }

    public function serviceQuestionsWhereFile() {
        return $this->belongsToMany(ServiceQuestion::class)->withPivot('id', 'value', 'sharefile_item_id', 'service_question_id')->wherePivot('sharefile_item_id', '!=', NULL);
    }

    public function serviceQuestionWithFile($serviceId) {
        return $this->belongsToMany(ServiceQuestion::class)->where('service_id', $serviceId)->withPivot('id', 'value', 'sharefile_item_id', 'service_question_id')->wherePivot('sharefile_item_id', '!=', NULL);
    }

    public function serviceQuestionsList() {
        return $this->belongsToMany(ServiceQuestion::class)->pluck('service_question_id');
    }

    public function serviceQuestionsAnswers($serviceId) {
        return $this->belongsToMany(ServiceQuestion::class)->where('service_id', $serviceId)->where('profile_tag_id', NULL)->withPivot('id', 'value', 'sharefile_item_id', 'service_question_id');
    }

    public function serviceQuestionsAnswersById($id) {
        return $this->belongsToMany(ServiceQuestion::class)->withPivot('id', 'sharefile_item_id')->wherePivot('id', (int) $id);
    }

    public function userSignUpQuestions() {
        return $this->belongsToMany(UserSignupQuestion::class);
    }

    /**
     * Returns Requested User Deatils
     * @param type $user_id
     * @return type array
     */
    public static function userData($user_id) {
        $userFields = ['id', 'email', 'two_factor_auth'];
        $profileFields = ['id', 'user_id', 'sex', 'first_name', 'middle_name', 'last_name', 'first_address', 'last_address', 'city', 'state_id',
            'zip', 'phone_number', 'phone_type', 'phone_verified', 'dob', 'ssn'];
        $userData = User::with(['profile' => function($q) use ($profileFields) {
                        $q->select($profileFields)->with('state');
                    }])->where('id', $user_id)->first($userFields);
        if (!$userData) {
            abort(404, 'Not Found.');
        }
        return $userData;
    }
      public function clientDocuments(){
        return $this->hasMany(Client_Document::class);
    }
      public function notifications()
    {
        return \App\Models\Notification::where('sender',$this->id)->orWhere('receiver',$this->id)->get();
    }
     public function notyfies()
    {
        return $this->hasMany('App\Models\Notify');
    }


}
