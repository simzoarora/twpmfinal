<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function services() {
        return $this->belongsToMany('App\Models\Backend\Service','service_question')->withPivot('page_number', 'order');
    }
    
    public function questionInputs() {
        return $this->belongsToMany('App\Models\QuestionInput','question_input', 'question_id', 'input_id');
//        return $this->belongsToMany('App\Models\QuestionInput','attempted_question_answer_questions', 'question_id', 'question_input_id');
    }
}
