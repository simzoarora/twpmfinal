<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttemptedQuestionAnswer extends Model
{
   public function attemptedQuestion() {
       return $this->belongsTo('App\Models\AttemptedQuestion');
   }
}
