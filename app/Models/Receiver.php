<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;

class Receiver extends Model
{

    public $timestamps = false;

    public function receiver()
    {
        return $this->hasOne(User::class, 'id', 'receiver_id');
    }

}
