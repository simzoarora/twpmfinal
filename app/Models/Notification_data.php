<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Access\User\User;
use App\Models\Receiver;

class Notification_data extends Model
{
    protected $dates = ['created_at', 'updated_at'];

    public function sender()
    {
        return $this->hasOne(User::class, 'id', 'sender_id');
    }


    public function receivers()
    {
        return $this->hasMany(Receiver::class);
    }

}
