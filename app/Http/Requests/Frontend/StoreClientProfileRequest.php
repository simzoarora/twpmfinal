<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class StoreClientProfileRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    } 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3|regex:/^[(a-zA-Z\s)]+$/u',
            'middle_name' => 'regex:/^[(a-zA-Z\s)]+$/u',
            'last_name' => 'required|min:3|regex:/^[(a-zA-Z\s)]+$/u',
            'first_address' => 'required|min:5',
            //'last_address' => 'required|min:5',
            'city' => 'required',
            'zip' => 'required|min:5|max:7',
            'phone_number' => 'required|min:8|max:20|unique:profiles,phone_number',
            'phone_type' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'first_name.required' => trans('alerts.frontend.client.firstName'),
            'first_name.regex' => trans('alerts.frontend.client.realName'),
            'first_name.min' => trans('alerts.frontend.client.shortName'),
            'middle_name.regex' => trans('alerts.frontend.client.middleName'),
            'last_name.regex' => trans('alerts.frontend.client.realName'),
            'last_name.required' => trans('alerts.frontend.client.lastName'),
            'last_name.min' => trans('alerts.frontend.client.shortName'),
            'first_address.required' => trans('alerts.frontend.client.addressRequired'),
            'first_address.min' => trans('alerts.frontend.client.shortAddress'),
            'city.required' => trans('alerts.frontend.client.cityRequired'),
            'state.required' => trans('alerts.frontend.client.stateRequired'),
            'zip.required' => trans('alerts.frontend.client.zipRequired'),
            'zip.min' => trans('alerts.frontend.client.zipInvalid'),
            'zip.max' => trans('alerts.frontend.client.zipInvalid'),
            'phone_number.required' => trans('alerts.frontend.client.phoneRequired'),
            'phone_number.min' => trans('alerts.frontend.client.phoneInavlid'),
            'phone_number.max' => trans('alerts.frontend.client.phoneInavlid'),
            'phone_number.unique' => trans('alerts.frontend.client.uniquePhone'),
            'phone_type.required' => trans('alerts.frontend.client.phoneTypeRequired'),
        ];
    }
}
