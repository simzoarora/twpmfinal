<?php

namespace App\Http\Requests\Frontend\Auth;

use App\Http\Requests\Request;
use Illuminate\Validation\Rule;

/**
 * Class RegisterRequest
 * @package App\Http\Requests\Frontend\Access
 */
class RegisterRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required|email|max:255|unique:users,email',
            'password' => 'required|string|min:8',
//            'serviceId' => 'required',
//            'g-recaptcha-response' => 'required_if:captcha_status,true|captcha',
//            'question'.'*'.'answer' => 'required',
//            'question'.'*'.'answer'.'*' => 'required'
        ];
    }

    /**
     * @return array
     */
    public function messages() {
        return [
            'email.unique' => 'Hmm, it looks like that email is already in use.',
            'password.min' => 'Your password needs to be at least 8 characters.',
        ];
    }

    public function all() {
        $data = parent::all();
//        $data['email'] = $data['user']['email'];
//        $data['password'] = $data['user']['password'];
        return $data;
    }

}
