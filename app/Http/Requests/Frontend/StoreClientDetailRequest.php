<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class StoreClientDetailRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    } 

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sex' => 'required',
            'dob' => 'required|date|before:' . Carbon::parse('-18 year')->toDateString(),
//            'ssn' => 'required|min:9|max:9|unique:profiles,ssn'
        ];
    }

    public function messages()
    {
        return [
            'sex.required' => trans('alerts.frontend.client.sexRequired'),
            'dob.required' => trans('alerts.frontend.client.dobRequired'),
            'dob.date' => trans('alerts.frontend.client.dobDate'),
            'dob.before' => trans('alerts.frontend.client.dobBefore'),
            'ssn.min' => trans('alerts.frontend.client.ssnInavlid'),
            'ssn.max' => trans('alerts.frontend.client.ssnInavlid'),
            'ssn.required' => trans('alerts.frontend.client.ssnRequired'),
            'ssn.unique' => trans('alerts.frontend.client.ssnUnique'),
        ];
    }

}
