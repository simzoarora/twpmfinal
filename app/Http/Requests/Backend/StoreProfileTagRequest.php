<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfileTagRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:profile_tags,name',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Profile type tag name required',
            'name.unique' => 'Profile type tag name already exist',
        ];
    }

}
