<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SaveSeoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'home_seo_title' => 'max:255|required',
            'home_seo_description' => 'required',
            'about_seo_title' => 'max:255|required', 
            'about_seo_description' => 'required'
        ];
    }
    
    public function messages()
    {
        return [
            'home_seo_title' => 'Home SEO title is required',
            'home_seo_description' => 'Home SEO description is required',
            'about_seo_title' => 'About SEO title is required', 
            'about_seo_description' => 'About SEO description is required'
        ]; 
    }
}
