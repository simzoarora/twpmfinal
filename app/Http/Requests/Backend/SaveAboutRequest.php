<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class SaveAboutRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slide_1_heading' => 'required',
            'slide_2_heading' => 'required|max:255',
            'slide_2_description' => 'required',
            'slide_3_heading' => 'required|max:255',
            'slide_3_description' => 'required',
            'slide_5_heading' => 'required|max:255',
            'slide_5_description' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'slide_1_heading.required' => 'Heading is required.',
            'slide_2_heading.required' => 'Heading is required.',
            'slide_2_description.required' => 'Description is required.',
            'slide_3_heading.required' => 'Heading is required.',
            'slide_3_description.required' => 'Descriptionis required.',
            'slide_5_heading.required' => 'Heading is required.',
            'slide_5_description.required' => 'Description is required.'
        ]; 
    }

}
