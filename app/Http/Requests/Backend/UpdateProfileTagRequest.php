<?php

namespace App\Http\Requests\Backend;

use App\Http\Requests\Request;

class UpdateProfileTagRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required',
            'name' => 'required|unique:profile_tags,name,' . $this->id,
        ];
    }

    public function messages()
    {
        return [
            'id' => 'Invalid Profile Tag Type',
            'name.required' => 'Profile type tag name required',
            'name.unique' => 'Profile type tag name already exist',
        ];
    }

}
