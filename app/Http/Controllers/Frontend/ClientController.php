<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\SignupQuestion;
use App\Models\UserSignupQuestion;
use App\Models\UserSignupQuestionAnswer;
use App\Models\Access\User\User;
use App\Models\Profile;
use App\Models\FinancialProfile;
use App\Models\OneTimePassword;
use App\Models\State;
use App\Models\SecurityQuestion;
use App\Models\Question;
use App\Models\AttemptedQuestion;
use App\Models\AttemptedQuestionAnswer;
use App\Models\ActiveService;
use App\Models\SelectedServiceQuestion;
use App\Models\SelectedServiceQuestionAnswer;
use App\Models\TopicQuestion;
use App\Models\TopicQuestionAnswer;
use App\Models\SubTopicQuestion;
use App\Models\SubTopicQuestionAnswer;
use App\Models\Backend\ServiceUser;
use App\Models\QuestionInput;
use App\Models\Notify;
use App\Models\Notification;
use App\Models\Notification_data;
use App\Models\Backend\Article;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
//footer
use App\Models\Backend\Footer;
/* Contact */
use App\Models\Backend\Contact;
use Session;
Use Auth;
use DB;
use Twilio;
use Mail;
use App\Mail\mailConfirmationOnServiceEnrollment;
use App\Mail\mailConfirmationOnEditServiceData;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Http\Requests\Frontend\StoreClientProfileRequest;
use App\Http\Requests\Frontend\StoreClientDetailRequest;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Models\Backend\ProfileTag;
use App\Models\Backend\ResourceTopic;
use App\Models\Backend\ResourceCategory;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceQuestion;
use App\Models\Access\Role\Role;
use App\Models\Backend\ClientToEmployeeUser;
use App\Events\PrivateEvent;
use App\Models\Notification_room;
use App\Models\Questions;
use App\Models\QuestionAnswers;
use App\Models\Topics;
use App\Models\ServiceDocument;
use App\Models\UserAccountInfo;
use View;

class ClientController extends Controller {

    public function __construct() {
        $this->middleware(function ($request, $next) {
            $spouse_name = FinancialProfile::where('user_id', Auth::id())->value('spouse_name');
            $spouse_dob = QuestionAnswers::where(['user_id' => Auth::id(), 'question_id' => 1580])->value('answer');
            $dob = new \DateTime(date('Y-m-d', strtotime($spouse_dob)));
            $current_date = new \DateTime(date('Y-m-d'));

            $spouse_age = $current_date->diff($dob);

            $user = User::where('id', Auth::id())->first();
            if (!empty($user) && empty($user->confirmed)) {
                $msg = 'Kindly Verify Your Email, <a href="' . route('frontend.client.confirm_mail', $user->confirmation_code) . '"> Click to resend.  </a> ';
                Session::flash('confirmation_message', $msg);
                Session::flash('alert-class', 'alert-danger container-fluid flash-message');
            }

            View::share(['spouse_name' => $spouse_name, 'spouse_age' => $spouse_age->y]);
            return $next($request);
        });
    }

    public function signUp(Request $request) {
        $questions = SignupQuestion::with(['signUpQuestionAnswer' => function($q) {
                        $q->select(['id', 'signup_question_id', 'value']);
                    }])->orderBY('order', 'ASC')->get(['id', 'question', 'type', 'order',
                    'page_no'])->groupBy('page_no');

        //plan details
        $plan_details = NULL;
        if (isset($_GET['answer1'])) {
            $plan_details[0]['id'] = 3;
            $plan_details[0]['answer'] = $_GET['answer1'];
        }
        if (isset($_GET['answer2'])) {
            $plan_details[1]['id'] = 4;
            $plan_details[1]['answer'] = $_GET['answer2'];
        }

        $userAnswers = NULL;
        if (Auth::check()) {
            $userAnswers = UserSignupQuestion::where('user_id', Auth::id())->with('userSignupQuestionAnswer')->get();
        }
        return view('frontend.client.signUp')->with(['questions' => $questions, 'userAnswers' => $userAnswers,
                    'plan_details' => $plan_details, 'footer' => Footer::first(), 'contact' => Contact::first()]);
    }

    public function saveSignUpUser(RegisterRequest $request) {
        $data = $request->all();
//        echo'<PRE>';print_R($data);die;
        DB::beginTransaction();
        $saveSignUpUser = new User;
        $saveSignUpUser->email = $data['email'];
        $saveSignUpUser->password = bcrypt($data['password']);
        $saveSignUpUser->confirmation_code = md5(uniqid(mt_rand(), true));
        $saveSignUpUser->save();
        $userid = $saveSignUpUser->id;
        $this->_sendSignUpDocumentForSigning($data['email']);
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            foreach ($data['question'] as $question) {
                $saveQuestionId = new UserSignupQuestion;
                $saveQuestionId->user_id = $userid;
                $saveQuestionId->signup_question_id = $question['id'];
                $saveQuestionId->save();
                $userSingupQuestionId = $saveQuestionId->id;
                if (isset($question['answer'])) {
                    foreach ($question['answer'] as $answer) {
                        $saveAnswer = new UserSignupQuestionAnswer;
                        $saveAnswer->user_signup_question_id = $userSingupQuestionId;
                        if (isset($question['text_answer'])) {
                            $saveAnswer->signup_question_answer_text = $answer;
                            if ($question['id'] == 4) {
                                session()->push('user_annual_income', $answer);
                            }
                        } else {
                            $saveAnswer->signup_question_answer_id = $answer;
                        }
                        $saveAnswer->save();
                    }
                }
            }
            //assign default user role on signup process
            $role = Role::where('name', config('access.users.default_role'))->first();
            $saveSignUpUser->roles()->attach($role->id);
            DB::commit();
            // send email-verification email to user
            Mail::send('frontend.emails.verifyEmail', ['user' => $saveSignUpUser], function($message)use ($saveSignUpUser) {
                $message->to($saveSignUpUser->email)->subject('Welcome to Total Wealth Planning & Management!');
            });
            return response()->json(['url' => route('frontend.client.showAnalysis', config('constant.subdomain'))]);
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    public function showAnalysis() {
        $analysis = UserSignupQuestion::where('user_id', Auth::id())
                ->select('id', 'user_id', 'signup_question_id')
                ->with('userSignupQuestionAnswer.signUpQuestionAnswer')
                ->get();
        $totalPoints = 0;

        foreach ($analysis as $userSignupQuestionAnswers) {
            foreach ($userSignupQuestionAnswers->userSignupQuestionAnswer as $userSignupQuestionAnswer) {
                if ($userSignupQuestionAnswer->signup_question_answer_text == null) {
                    $totalPoints += $userSignupQuestionAnswer->signUpQuestionAnswer->points;
                }
            }
        }
        if ($totalPoints >= 0 && $totalPoints <= 24) {
            $result = [];
            $result['portfolioType'] = 'Conservative (and Short Term)';
            $result['portfolioDescription'] = 'Preservation of capital is the primary concern of conservative and short-term investors. They tend to prefer fixed income, short-term, and tax-advantaged investments, but also allocate some assets to stocks as part of a welldiversified portfolio.';
            $result['Cash'] = '0%';
            $result['Bonds'] = '80%';
            $result['Stocks'] = '20%';
            $result['Alternative'] = '0%';
        }
        if ($totalPoints >= 25 && $totalPoints <= 39) {
            $result = [];
            $result['portfolioType'] = 'Moderately Conservative';
            $result['portfolioDescription'] = 'These investors covet current income, safety and stability, but also see the need for some longterm growth if for no other reason than to offset the effects of inflation. They are more heavily-weighted in fixed income investments, but also hold a diversified portfolio of stocks.';
            $result['Cash'] = '0%';
            $result['Bonds'] = '60%';
            $result['Stocks'] = '40%';
            $result['Alternative'] = '0%';
        }
        if ($totalPoints >= 40 && $totalPoints <= 64) {
            $result = [];
            $result['portfolioType'] = 'Moderate';
            $result['portfolioDescription'] = 'These are generally long-term oriented investors with little or no need for taxable current income. They prefer reasonable but stable investment growth. Some fluctuations are tolerable, but they typically diversify with other investments to help mitigate volatility from their stock holdings';
            $result['Cash'] = '0%';
            $result['Bonds'] = '40%';
            $result['Stocks'] = '60%';
            $result['Alternative'] = '0%';
        }
        if ($totalPoints >= 65 && $totalPoints <= 87) {
            $result = [];
            $result['portfolioType'] = 'Moderately Aggressive';
            $result['portfolioDescription'] = 'These investors are also focused on the long term, and are looking for good growth, but have little or no need for taxable currrent income. Some risk with part of the portfolio is acceptable in return for the long-term growth potential that the stock market offers.';
            $result['Cash'] = '2%';
            $result['Bonds'] = '15%';
            $result['Stocks'] = '78%';
            $result['Alternative'] = '5%';
        }
        if ($totalPoints >= 88 && $totalPoints <= 100) {
            $result = [];
            $result['portfolioType'] = 'Aggressive ';
            $result['portfolioDescription'] = 'Aggressive investors tend to be long-term oriented and are interested in the maximum possible growth of assets. They have little or no interest in taxable income. Some year to year volatility is acceptable in exchange for potentially high long-term returns.';
            $result['Cash'] = '0%';
            $result['Bonds'] = '15%';
            $result['Stocks'] = '85%';
            $result['Alternative'] = '0%';
        }
        return view('frontend.client.showAnalysis', compact('result'))->with(['footer' => Footer::first(),
                    'contact' => Contact::first()]);
    }

    public function contactInfo() {
        $states = State::pluck('name', 'id');
        return view('frontend.client.contactInfo')->with(['states' => $states, 'footer' => Footer::first(),
                    'contact' => Contact::first()]);
    }

    public function saveContactInfo(StoreClientProfileRequest $request) {
        DB::beginTransaction();
        $profile = new Profile;
        $profile->user_id = Auth::id();
        $profile->first_name = $request->first_name;
        $profile->middle_name = $request->middle_name;
        $profile->last_name = $request->last_name;
        $profile->first_address = $request->first_address;
        $profile->last_address = $request->last_address;
        $profile->city = $request->city;
        $profile->state_id = $request->state_id;
        $profile->zip = $request->zip;
        $profile->phone_number = $request->phone_number;
        $profile->phone_type = $request->phone_type;
        $profile->profile_complete_step = 1;

        $phoneVerified = 0;
        $phone = OneTimePassword::where('user_id', Auth::id())->first();
        if (isset($phone)) {
            if ($phone->active == 0) {
                $phoneVerified = 1;
            }
        }
        $profile->phone_verified = $phoneVerified;

        if ($profile->save()) {
            DB::commit();
            return redirect()->route('frontend.client.identityVerification', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    /**
     * send otp to user and save otp value in database
     * @param Request $request
     * @return json
     */
    public function sendOtp(Request $request) {
        $data = $request->all();
        if (!isset($data['phone_number']) || empty($data['phone_number']) || strlen($data['phone_number']) != 10) {
            return response()->json(['message' => trans('alerts.frontend.client.phoneRequired')], 500);
        }

        $userId = isset($data['user_id']) ? (!empty($data['user_id']) ? $data['user_id'] : NULL) : (Auth::check() ? Auth::id() : NULL);
        if (empty($userId) || (!User::where('id', $userId)->exists())) {
            throw new GeneralException('Forbidden');
        }

        if (OneTimePassword::where('user_id', $userId)->whereDate('created_at', '=', Carbon::today()->toDateString())->count() > 2) {
            return response()->json(['message' => trans('alerts.frontend.client.otpLimit')], 500);
        } else {
            if ($this->_generateOtp($userId, $data['phone_number'])) {
                return response()->json(['message' => 'Otp sent successfully.'], 200);
            } else {
                die('otp not send');
                return response()->json(['message' => trans('alerts.frontend.client.otpFail')], 500);
            }
        }
    }

    /**
     * function to generate otp for phone verification
     * @param type $user
     * @param type $data
     * @param type $otpMessage
     * @return boolean
     */
    private function _generateOtp($userId, $phoneNumber) {
        $otp = mt_rand(111111, 999999);

        $one_time_password = new OneTimePassword;
        $one_time_password->user_id = $userId;
        $one_time_password->phone_number = $phoneNumber;
        $one_time_password->otp = $otp;
        $message = trans('alerts.frontend.client.welcomeMsg') . $otp;

        //expiring all previous OTP's active for the user
        OneTimePassword::where('user_id', $userId)->update(['active' => 0]);
        $one_time_password->save();
        return true;
    }

    /**
     * Verifies OTP enterd by user
     * @param Request $request
     * @return json
     */
    public function verifyOtp(Request $request) {
        $data = $request->all();
        if (!isset($data['phone_number']) || empty($data['phone_number']) || strlen($data['phone_number']) != 10) {
            throw new GeneralException('Forbidden');
        }

        if (OneTimePassword::where('phone_number', $data['phone_number'])->where('otp', $data['verify_otp'])->where('active', 1)->exists()) {
            if (OneTimePassword::where('phone_number', $data['phone_number'])->update([
                        'active' => 0])) {
                return response()->json(['message' => trans('alerts.frontend.client.phoneVerifySuccess')], 200);
            }
            return response()->json(['message' => trans('alerts.frontend.client.phoneVerifyError')], 500);
        }
        return response()->json(['message' => trans('alerts.frontend.client.invalidOtp')], 500);
    }

    public function identityVerification() {
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 0) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 1) {
            return view('frontend.client.identitityVerification')->with(['footer' => Footer::first(),
                        'contact' => Contact::first()]);
        }
        if ($access['profile_complete_step'] == 2) {
            return redirect()->route('frontend.client.employmentInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 3) {
            return redirect()->route('frontend.client.financialInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 4) {
            return redirect()->route('frontend.client.securityInfo', config('constant.subdomain'));
        } else {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
    }

    public function saveIdentityVerification(StoreClientDetailRequest $request) {
        DB::beginTransaction();
        if (Profile::where('user_id', Auth::id())->update(['sex' => $request->sex,
                    'dob' => Carbon::parse($request->dob)->toDateString(), 'ssn' => $request->ssn,
                    'profile_complete_step' => 2])) {
            DB::commit();
            return redirect()->route('frontend.client.employmentInfo', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    public function employmentInfo() {
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 0) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 1) {
            return redirect()->route('frontend.client.identityVerification', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 2) {
            return view('frontend.client.employmentInfo')->with(['footer' => Footer::first(),
                        'contact' => Contact::first()]);
        }
        if ($access['profile_complete_step'] == 3) {
            return redirect()->route('frontend.client.financialInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 4) {
            return redirect()->route('frontend.client.securityInfo', config('constant.subdomain'));
        } else {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
    }

    public function saveEmploymentInfo(Request $request) {
        $this->validate($request, [
            'employment_status' => 'required'
        ]);
        DB::beginTransaction();
        $employmentInfo = new FinancialProfile;
        $employmentInfo->user_id = Auth::id();
        $employmentInfo->employment_status = $request->employment_status;

        if ($employmentInfo->save()) {
            Profile::where('user_id', Auth::id())->update(['profile_complete_step' => 3]);
            DB::commit();
            return redirect()->route('frontend.client.financialInfo', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    public function financialInfo() {
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        $user_annual_income = session()->get('user_annual_income')[0];
        if ($access['profile_complete_step'] == 0) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        } if ($access['profile_complete_step'] == 1) {
            return redirect()->route('frontend.client.identityVerification', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 2) {
            return redirect()->route('frontend.client.employmentInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 3) {
            return view('frontend.client.financialInfo')->with(['footer' => Footer::first(),
                        'contact' => Contact::first(), 'user_annual_income' => $user_annual_income]);
        }
        if ($access['profile_complete_step'] == 4) {
            return redirect()->route('frontend.client.securityInfo', config('constant.subdomain'));
        } else {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
    }

    public function saveFinancialInfo(Request $request) {
        $this->validate($request, [
            'filing_status' => 'required',
            'annual_income' => 'required|integer',
            'spouse_annual_income' => 'integer'
        ]);
        $id = Auth::id();
        DB::beginTransaction();
        if (FinancialProfile::where('user_id', $id)->update([
                    'tax_filing_status' => $request->filing_status,
                    'annual_income' => $request->annual_income,
                    'spouse_name' => (isset($request->spouse_name) && $request->spouse_name != '') ? $request->spouse_name : null,
                    'spouse_annual_income' => (isset($request->spouse_annual_income) && $request->spouse_annual_income != '') ? $request->spouse_annual_income : 0,
                    'spouse_has_twpm_account' => isset($request->spouse_has_twpm_account) ? $request->spouse_has_twpm_account : 0
                ])) {
            Profile::where('user_id', $id)->update(['profile_complete_step' => '4']);
            DB::commit();
            Session::set('tax_filing_status', $request->filing_status);
            return redirect()->route('frontend.client.securityInfo', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    public function securityInfo() {
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 0) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        } else if ($access['profile_complete_step'] == 1) {
            return redirect()->route('frontend.client.identityVerification', config('constant.subdomain'));
        } else if ($access['profile_complete_step'] == 2) {
            return redirect()->route('frontend.client.employmentInfo', config('constant.subdomain'));
        } else if ($access['profile_complete_step'] == 3) {
            return redirect()->route('frontend.client.financialInfo', config('constant.subdomain'));
        } else if ($access['profile_complete_step'] == 4) {
            return view('frontend.client.securityInfo')->with(['footer' => Footer::first(),
                        'contact' => Contact::first()]);
        } else {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
    }

    public function saveSecurityInfo(Request $request) {
        $questions = $request->all();
        $id = Auth::id();
        DB::beginTransaction();
        $saved = 0;
        foreach ($questions['security_question'] as $question) {
            $securityQuestion = new SecurityQuestion();
            $securityQuestion->user_id = $id;
            $securityQuestion->question_id = $question['question_id'];
            $securityQuestion->answer = $question['answer'];
            $securityQuestion->type = $question['type'];
            $securityQuestion->save();
            $saved ++;
        }
        if ($saved != 0) {
            Profile::where('user_id', $id)->update(['profile_complete_step' => '5']);
            DB::commit();
            session()->push('user_first_time', true);
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    public function clientDashboard() {
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 0) {
            return redirect()->route('frontend.client.contactInfo', config('constant.subdomain'));
        } if ($access['profile_complete_step'] == 1) {
            return redirect()->route('frontend.client.identityVerification', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 2) {
            return redirect()->route('frontend.client.employmentInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 3) {
            return redirect()->route('frontend.client.financialInfo', config('constant.subdomain'));
        }if ($access['profile_complete_step'] == 4) {
            return redirect()->route('frontend.client.securityInfo', config('constant.subdomain'));
        }
        if ($access['profile_complete_step'] == 5) {
            $articles = Article::where('status', config('constant.backend.blogs.active'))->orderBy('publish_at', 'desc')->take(2)->get(['id', 'slug', 'title', 'excerpt', 'publish_at']);

            $serviceData = User::find(Auth::id())->services()
                    ->where('service_user.status', 1)
                    ->orderBy('service_user.created_at', 'DESC')
                    ->get(['services.id', 'services.title', 'services.image', 'services.title_bg_color']);

            $recommendedServices = $this->_sortingServices();
            $activeServices = User::where('id', Auth::id())->with(['services' => function($query) {
                            $query->where('status', 1)->with(['questions' => function($query) {
                                    $query->orderBY('page_number')->orderBy('page_question_order')->with('questionInputs');
                                }])->with(['attemptedQuestions' => function($query) {
                                    $query->where('user_id', Auth::id())->with('attemptedQuestionAnswer');
                                }]);
                        }])->first();
//                    dump($activeServices->toArray());
            $status = FinancialProfile::where('user_id', Auth::id())->select('tax_filing_status')->first();
            Session::put('tax_filing_status', $status->tax_filing_status);


            return view('frontend.client.dashboard')->with(['commentaryData' => $articles,
                        'serviceData' => $serviceData, 'access' => $access, 'recommendedServices' => $recommendedServices,
                        'activeServices' => $activeServices]);
        } else {
            //return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.detailsAlreadyFilled'));
        }
    }

    public function clientProfile() {
        return view('frontend.client.profile')->with([
                    'userData' => User::userData(Auth::id()),
                    'states' => State::pluck('name', 'id'),
                    'financialData' => FinancialProfile::where('user_id', Auth::id())->first(),
                    'profileData' => ProfileTag::whereHas('serviceQuestionWithAnswers')->with([
                        'serviceQuestionWithAnswers'])->get()
        ]);
    }

    public function recommendedServices() {
        $user_first_time = false;
        if (session()->get('user_first_time') != null) {
            $user_first_time = session()->get('user_first_time')[0];
        }
        $access = Profile::where('user_id', Auth::id())->first();
        if (empty($access)) {
            return redirect()->route('frontend.client.dashboard', config('constant.subdomain'));
        }
        $recommendedServices = $this->_sortingServices();
//        $activeServices = User::where('id', Auth::id())->with(['services' => function($query) {
//                        $query->where('status', 1)->with(['attemptedQuestions' => function($query) {
//                                $query->where('user_id', Auth::id())->with('attemptedQuestionAnswer');
//                            }]);
//                    }])->first();
        $activeServices = User::where('id', Auth::id())->with(['services' => function($query) {
                        $query->where('status', 1)->with(['questions' => function($query) {
                                $query->orderBy('page_number')->orderBy('page_question_order')->with('questionInputs');
                            }, 'serviceUsers' => function($query) {
                                $query->where('user_id', Auth::user()->id);
                            }])->with(['attemptedQuestions' => function($query) {
                                $query->where('user_id', Auth::id())->with('attemptedQuestionAnswer');
                            }]);
                    }])->first();
//                                            dump($activeServices->toArray());

        $submittedDocuments = [];
        if ($activeServices->services->count()) {
            $activeServiceIds = $activeServices->services->pluck('id')->toArray();
            $submittedDocuments = AttemptedQuestionAnswer::where('status', 0)->where('doc_sign', 1)->whereHas('attemptedQuestion', function($query) use($activeServiceIds) {
                        $query->whereIn('service_id', $activeServiceIds)->where('user_id', auth()->user()->id)->whereHas('question', function($query) {
                            $query->where('type', config('constant.service_question_types_options_inverse.usl'));
                        });
                    })->get();
        }
//        echo'<PRE>';print_R($activeServices);die;
        //setting loggedInUserName
        Session::set('loggedInUserName', $access->first_name . ' ' . $access->last_name);
        return view('frontend.client.services', compact('recommendedServices', 'activeServices', 'submittedDocuments', 'access', 'user_first_time'));
    }

    private function _services() {
        $questions = SignupQuestion::where('service_recommendation_question', 1)->get();
        $questionIds = [];
        $analysis = [];
        foreach ($questions as $question) {
            $questionIds[] = $question->id;
        }
        $conditions = UserSignupQuestion::where('user_id', Auth::id())->with('userSignupQuestionAnswer.SignupQuestionAnswer')->get()->toArray();
        foreach ($conditions as $condition) {
            foreach ($questionIds as $id) {
                if ($condition['signup_question_id'] == $id) {
                    foreach ($condition['user_signup_question_answer'] as $finalCondition) {
                        $analysis[] = ($finalCondition['signup_question_answer']['value']);
                    }
                }
            }
        }
        $count = count($analysis);
        $services = Service::all()->toArray();
        $recommendedServices = [];
        $session_service_ids = [];
        if (!empty(session('services'))) {
            foreach (session('services') as $service) {
                $session_service_ids[] = $service['id'];
            }
        }
        foreach ($services as $service) {
            if (in_array($service['id'], $session_service_ids)) {
                continue;
            }
            if ($service['title'] == config('constant.services.1')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.9') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.15') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.16') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.17') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.18') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.20') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.21') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.2')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.9') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.21') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.3')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.1') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.2') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.5') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.14') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.20') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.13') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.12') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.17') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.15') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.3') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.19') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.16') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.18') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.4') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.4')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.5') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.9') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.14') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.20') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.13') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.17') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.15') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.3') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.4') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.5')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.22') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.15') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.16') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.6')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.22') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.15') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.16') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.7')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.19') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.18') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.8')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.3') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.9')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.1') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.2') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.5') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.14') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.7') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.9') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.6') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.12') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.8') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.10') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.3') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.11') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.4') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
            if ($service['title'] == config('constant.services.10')) {
                for ($i = 0; $i < $count; $i++) {
                    if ((config('constant.pre_defined_options_for_service_recommended_questions.1') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.2') == $analysis[$i]) || (config('constant.pre_defined_options_for_service_recommended_questions.5') == $analysis[$i])) {
                        $recommendedServices[] = $service;
                        break;
                    }
                }
            }
        }
        $recommendedServicesIds = [];
        foreach ($recommendedServices as $service) {
            $recommendedServicesIds[] = $service['id'];
        }
        return $recommendedServicesIds;
    }

    private function _sortingServices() {
        $recommendedServicesIds = $this->_services();
        $recommendedServices = [];
        $allServices = Service::with(
                        ['serviceUsers' => function($query) {
                                $query->where('user_id', Auth::user()->id);
                            }]
                )->get()
                ->toArray();

        foreach ($allServices as $service) {
            if (in_array($service['id'], $recommendedServicesIds)) {
                $service['recommended'] = 1;
                $recommendedServices[] = $service;
            } else {
                $service['recommended'] = 0;
                $recommendedServices[] = $service;
            }
        }
        usort($recommendedServices, function($a, $b) {
            return $b['recommended'] <=> $a['recommended'];
        });
        return $recommendedServices;
    }

    public function servicesQuestion($client = null, $id) {
//
//        Session::forget('services');
        //checking if we are adding service
//        if (!empty(session('services'))) {
//            foreach (session('services') as $service) {
//                if ($id == $service['id']) {
//                    return redirect()->back()->withFlashWarning('You have already added this service.');
//                }
//            }
//        }
        //checking if this service is bought by user
        $activeService = User::find(Auth::id())->services()
                ->where('service_user.status', 1)
                ->get(['services.id']);

        if (!empty($activeService)) {
            foreach ($activeService as $service) {
                if ($id == $service['id']) {
                    return redirect()->back()->withFlashWarning('You have already bought this service.');
                }
            }
        }
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $id)->first();
//        dump($currentService['service_number']);
        $selectedServiceAnswers = SelectedServiceQuestionAnswer::where('service_id', $id)->where('user_id', Auth::id())->get();
        $answer = [];
        if (!empty($selectedServiceAnswers)) {
            foreach ($selectedServiceAnswers as $key => $data) {
                $answerInfo = json_decode($data['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
        }
        $status = FinancialProfile::where('user_id', Auth::id())->select('tax_filing_status')->first();
        Session::set('tax_filing_status', $status->tax_filing_status);
        $defaultData = $this->getSignupQuestionValueForService();
        $topicsInfo = $this->getTopicData($id);
        $contact = $this->_getContactDetails();
        $user_signup_question_id = UserSignupQuestion::where(['user_id' => Auth::id(), 'signup_question_id' => 4])->value('id');
        $gross_income = UserSignupQuestionAnswer::where('user_signup_question_id', $user_signup_question_id)->value('signup_question_answer_text');
        $gross_income = preg_replace("/[^0-9]/", "", $gross_income);
        return view('frontend.client.service_' . $currentService['service_number'], compact('currentService', 'defaultData', 'answer', 'topicsInfo', 'contact', 'gross_income'));
    }

    private function _getContactDetails() {
        $contact = Contact::first();
        return $contact;
    }

    private function getTopicData($serviceId) {
        $topics = TopicQuestion::select('topic_id')->where('service_id', $serviceId)->get();
        $topicData = [];
        $serviceCompleted = true;
        if (!empty($topics)) {
            foreach ($topics as $topic) {
                $answer = TopicQuestionAnswer::select('answer')->where(['service_id' => $serviceId,
                            'topic_id' => $topic["topic_id"], 'user_id' => Auth::id()])->first();
                if (!empty($answer)) {
                    $topicData[$topic["topic_id"]] = 'Revisit';
                } else {
                    $serviceCompleted = false;
                    $topicData[$topic["topic_id"]] = 'Begin';
                }
            }
        }

        $subTopics = SubTopicQuestion::where('service_id', $serviceId)->get();
        $data = $subTopics->groupBy('topic_id');
        $info = [];
        foreach ($data as $key => $subTopic) {

            $subTopicsData = SubTopicQuestionAnswer::where(['service_id' => $serviceId,
                        'topic_id' => $key,
                        'user_id' => Auth::id()])->get();

            if ($subTopicsData->isNotEmpty()) {
                $multiplied = $subTopicsData->map(function ($item, $key) {
                    if ($item->answer != null) {
                        return 'true';
                    }
                });
                if ($key == 8) {
//                echo'<PRE>';print_R($subTopic->toArray());
//                print_R($subTopicsData->toArray());
//                print_R($multiplied->count() );
//                echo'fdafdlk';
//                print_R($subTopic->count());
//                die;
                }
                if ($multiplied->count() == $subTopic->count()) {
                    $topicData[$key] = 'Revisit';
                } else {
                    $topicData[$key] = 'In progress';
                    $serviceCompleted = false;
                }
            } else {
                $topicData[$key] = 'Begin';
                $serviceCompleted = false;
            }
        }
        $topicData['serviceCompleted'] = $serviceCompleted;
        return $topicData;
    }

    private function getSignupQuestionValueForService() {
        $housholdValue = 0;
        $taxFillingStatus = '';
        $firstName = '';
        $lastName = '';
        $middleName = '';
        $taxFillingStatusInfo = FinancialProfile::where('user_id', Auth::id())->first();
        $personalInformation = Profile::where('user_id', Auth::id())->first();

        if (!empty($taxFillingStatusInfo)) {
            $taxFillingStatus = $taxFillingStatusInfo['tax_filing_status'];
            $housholdValue = $taxFillingStatusInfo['annual_income'];
        }
        if (!empty($personalInformation)) {
            $firstName = $personalInformation['first_name'];
            $middleName = $personalInformation['middle_name'];
            $lastName = $personalInformation['last_name'];
        }

        $values = [];
        $values['housesholdIncome'] = $housholdValue;
        $values['taxFillingStatus'] = $taxFillingStatus;
        $values['firstName'] = $firstName;
        $values['lastName'] = $lastName;
        $values['middleName'] = $middleName;
        return $values;
    }

    public function calculateTaxBracket(Request $request, $clientId = null) {
        $this->validate($request, [
            'taxableIncome' => 'required',
        ]);
        if (!is_int($clientId)) {
            $clientId = Auth::id();
        }

        $taxFillingStatusInfo = FinancialProfile::where('user_id', $clientId)->first();

        if (empty($taxFillingStatusInfo)) {
            return 'Error';
        }

        $taxFillingStatus = $taxFillingStatusInfo['tax_filing_status'];

        $income = $request['taxableIncome'];

        $taxBracket = 0;
        if ($taxFillingStatus == 1) {
            if ($income <= 9525) {
                $taxBracket = '10%';
            } elseif (($income >= 9526) && ($income <= 38700)) {
                $taxBracket = '12%';
            } elseif (($income >= 38701) && ($income <= 82500)) {
                $taxBracket = '22%';
            } elseif (($income >= 82501) && ($income <= 157500)) {
                $taxBracket = '24%';
            } elseif (($income >= 157501) && ($income <= 200000)) {
                $taxBracket = '32%';
            } elseif (($income >= 200001) && ($income <= 500000)) {
                $taxBracket = '35%';
            } elseif (($income > 500000)) {
                $taxBracket = '37%';
            } else {
                
            }
        } elseif ($taxFillingStatus == 2) {
            if ($income <= 13600) {
                $taxBracket = '10%';
            } elseif (($income >= 13601) && ($income <= 51800)) {
                $taxBracket = '12%';
            } elseif (($income >= 51800) && ($income <= 82500)) {
                $taxBracket = '22%';
            } elseif (($income >= 82501) && ($income <= 157500)) {
                $taxBracket = '24%';
            } elseif (($income >= 157501) && ($income <= 200000)) {
                $taxBracket = '32%';
            } elseif (($income >= 200001) && ($income <= 500000)) {
                $taxBracket = '35%';
            } elseif (($income > 500000)) {
                $taxBracket = '37%';
            } else {
                
            }
        } elseif ($taxFillingStatus == 4) {

            if ($income <= 19050) {
                $taxBracket = '10%';
            } elseif (($income >= 19051) && ($income <= 77400)) {
                $taxBracket = '12%';
            } elseif (($income >= 77401) && ($income <= 165000)) {
                $taxBracket = '22%';
            } elseif (($income >= 165001) && ($income <= 315000)) {
                $taxBracket = '24%';
            } elseif (($income >= 315001) && ($income <= 400000)) {
                $taxBracket = '32%';
            } elseif (($income >= 400001) && ($income <= 600000)) {
                $taxBracket = '35%';
            } elseif (($income > 600000)) {
                $taxBracket = '37%';
            } else {
                
            }
        } elseif ($taxFillingStatus == 5 || $taxFillingStatus == 6) {

            if ($income <= 9525) {
                $taxBracket = '10%';
            } elseif (($income >= 9526) && ($income <= 38700)) {
                $taxBracket = '12%';
            } elseif (($income >= 38701) && ($income <= 82500)) {
                $taxBracket = '22%';
            } elseif (($income >= 82501) && ($income <= 157500)) {
                $taxBracket = '24%';
            } elseif (($income >= 157501) && ($income <= 200000)) {
                $taxBracket = '32%';
            } elseif (($income >= 200001) && ($income <= 300000)) {
                $taxBracket = '35%';
            } elseif (($income > 300000)) {
                $taxBracket = '37%';
            } else {
                
            }
        } elseif ($taxFillingStatus == 3) {
            if ($income <= 19050) {
                $taxBracket = '10%';
            } elseif (($income >= 19051) && ($income <= 77400)) {
                $taxBracket = '12%';
            } elseif (($income >= 77401) && ($income <= 165000)) {
                $taxBracket = '22%';
            } elseif (($income >= 165001) && ($income <= 315000)) {
                $taxBracket = '24%';
            } elseif (($income >= 315001) && ($income <= 400000)) {
                $taxBracket = '32%';
            } elseif (($income >= 400001) && ($income <= 600000)) {
                $taxBracket = '35%';
            } elseif (($income > 600000)) {
                $taxBracket = '37%';
            } else {
                
            }
        } else {
            
        }
        return $taxBracket;
    }

    public function saveServiceQuestionAnswer(Request $request) {
        $data = $request->all();
//                                        dd($data);
        DB::beginTransaction();

        foreach ($data['data'] as $questionData) {
            if (array_key_exists('questionName', $questionData) && (array_key_exists('answer', $questionData))) {
                $attemptedQuestion = new AttemptedQuestion;
                $attemptedQuestion->user_id = Auth::id();
                $attemptedQuestion->service_id = $data['serviceId'];
//                                            $attemptedQuestion->question_id = $questionData['questionid'];
                $attemptedQuestion->question_name = $questionData['questionName'];

                if ($attemptedQuestion->save()) {

                    $QuestionId = $attemptedQuestion->id;
                    $attemptedQuestionAnswer = new AttemptedQuestionAnswer;
                    $attemptedQuestionAnswer->attempted_question_id = $QuestionId;
                    $attemptedQuestionAnswer->answer = json_encode($questionData['answer']);

                    if ($attemptedQuestionAnswer->save()) {

                        $serviceUser = new ServiceUser;
                        $serviceUser->user_id = Auth::id();
                        $serviceUser->service_id = $data['serviceId'];

                        if ($serviceUser->save()) {

                            DB::commit();
                        } else {
                            DB::rollBack();
                            return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
                        }
                    } else {
                        DB::rollBack();
                        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
                    }
                } else {
                    DB::rollBack();
                    return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
                }
            }
        }
        return redirect()->route('frontend.client.selectedService', [config('constant.subdomain'), $data['serviceId']]);
    }

    public function selectedService($client = null, $id) {
        $addedService = Service::where('id', $id)->first();
        $serviceUser = ServiceUser::where('user_id', Auth::id())->where('service_id', $id)->first();

        if (empty($serviceUser)) {
            $serviceUser = new ServiceUser;
        }
        $serviceUser->user_id = Auth::id();
        $serviceUser->service_id = $addedService->id;


        $serviceUser->save();
        if (empty(session('services'))) {
            session()->put('services', [$addedService]);
        } else {
            session()->push('services', $addedService);
            $uniqueServices = array_unique(session('services'));
//                    dump(session('services'));
//            dump($uniqueServices);
            session()->put('services', $uniqueServices);
//        dd(session('services'));
        }
        $recommendedServices = $this->_sortingServices();
        foreach ($recommendedServices as $key => $val) {
            if ($val['id'] == $id) {
                unset($recommendedServices[$key]);
            }
        }

        return view('frontend.client.selectedService', compact('recommendedServices', 'addedService'));
    }

    public function assets($id) {
        dd('hlelo');
        return view('frontend.client.serviceQuestions.assets-preview');
    }

    public function serviceEnrollment() {
        $dueAmount = 0;
        foreach (session('services') as $service) {
            $dueAmount = $dueAmount + (int) $service['amount'];
        }
        //retrive all cards
        $customer = User::where('id', Auth::id())->first();
        $allCards = null;
        $user_confirmed = $customer->confirmed;
        if ($customer->stripe_customer_id != null) {
            $stripe = new Stripe(config('services.stripe.secret'));
            $allCards = $stripe->cards()->all($customer->stripe_customer_id);
        }
        return view('frontend.client.serviceEnrollment', compact('dueAmount', 'allCards', 'user_confirmed'));
    }

    public function stripeCardPayment(Request $request) {
        $dueAmount = 0;
        foreach (session('services') as $service) {
            $dueAmount = $dueAmount + (int) $service['amount'];
        }
        $dueAmount = $dueAmount * 100;

        $data = [
            'stripe_token' => $request->stripe_token,
            'amount' => $dueAmount,
            'email' => Auth::user()->email];
        $stripe = new Stripe(config('services.stripe.secret'));
        try {
            //check if customer already created for given email id then retrieve customer id from db else create customer
            $customerId = User::where('id', Auth::id())->first();
            if ($customerId->stripe_customer_id == null) {
                $customer = $stripe->customers()->create([
                    'email' => $data['email'],
                ]);
                User::where('id', Auth::id())->update(['stripe_customer_id' => $customer['id']]);
                $stripe_customer_id = $customer['id'];
            } else {
                $stripe_customer_id = $customerId->stripe_customer_id;
                $customer = $stripe->customers()->find($customerId->stripe_customer_id);
            }
            //customer creation failed
            if (!isset($customer['id'])) {
                return response()->json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            }
            //adding new card to customer
            $card = $stripe->cards()->create($stripe_customer_id, $data['stripe_token']);
            if (!isset($card['id'])) {
                //failed to add the card
                return response()->json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            } else {
                return response()->json(['message' => 'Card added successfully.'], 200);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function stripePayment(Request $request) {
        $customerId = User::where('id', Auth::id())->first();
        if (empty($request->card_id)) {
            return response()->json(['message' => 'Please select a card. Then pay.'], 500);
        }

        $dueAmount = 0;
        foreach (session('services') as $service) {
            $dueAmount = $dueAmount + (int) $service['amount'];
        }
        $dueAmount = $dueAmount * 100;
        if ($dueAmount == 0) {
            return response()->json(['message' => 'Please enroll in services first.'], 500);
        }

        $stripe = new Stripe(config('services.stripe.secret'));
        try {

            $charge = $stripe->charges()->create([
                'customer' => $customerId->stripe_customer_id,
                'currency' => 'USD',
                'amount' => $dueAmount,
            ]);
            if ($charge['status'] == 'succeeded') {
                //Changing all service statuses to active
                if (!empty(session('services'))) {
                    foreach (session('services') as $service) {
                        ServiceUser::where('service_id', $service['id'])
                                ->where('user_id', Auth::id())
                                ->update(['status' => 1]);

                        if (!in_array($service['id'], config('constant.questions.services_containing_select_inputs')))
                        //send Document for Signing
                            $this->_sendDocumentForSigning($service['id'], $customerId->email);
                    }
                }
                //if charge succees, send emails to notify user of service enrollments
                $this->_sendServiceEnrollmentEmail(session('services'));
                $this->_notifyEmployeeOnSubscribe(session('services'));

                //services destroy from session
                session(['services' => []]);
                //Payment successed
                return response()->json(['message' => 'Thank you for your payment. We will contact you soon to schedule your consultation.',
                            'link' => route('frontend.client.recommendedServices', config('constant.subdomain'))], 200);
            } else {
                //Payment failed
                return response()->json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            }
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    private function _sendServiceEnrollmentEmail($sessionServices) {
        $loggedInUser = User::where('id', Auth::id())->with('profile')->first();
        foreach ($sessionServices as $service) {
            Mail::send('frontend.emails.serviceEnrollmentEmail', ['user' => $loggedInUser, 'service' => $service], function($message) use($loggedInUser, $service) {
                $message->to($loggedInUser->email)->subject('You are now registered for ' . $service['title']);
            });
            if (count(Mail::failures()) > 0) {
                \Log::warning('Service Enrollment Email not sent to user(' . Auth::id() . ') for service(' . $service['id'] . ')-amount(' . $service['amount'] . ')');
            }
        }
    }

    private function _notifyEmployeeOnSubscribe($sessionServices) {
        $clientId = Auth::user()->id;
        $client_employee = ClientToEmployeeUser::where('client_user_id', $clientId)->with(['employeeUser' => function($query) {
                        $query->select('id', 'name');
                    }])->first();
        if (isset($client_employee) && !empty($client_employee)) {
            $employeeId = $client_employee->employee_user_id;
            $userInfo = User::where('id', $clientId)->with(['profile' => function($query) {
                            $query->select('id', 'user_id', 'first_name');
                        }])->first();
            $name = "";
            if ($userInfo->name != '') {
                $name = $userInfo->name;
            } else {
                $name = $userInfo->profile->first_name;
            }
            $employee_name = $client_employee->employeeUser->name;
            foreach ($sessionServices as $service) {
                $text = "Hi " . $employee_name . ", Your Client '" . $name . "' has Successfuly Subscribed to  Service " . $service['title'];
                $notificationData = new Notification_data;
                $notificationData->notification_room_id = -1;
                $notificationData->sender_id = $clientId;
                $notificationData->notification = $text;
                $notificationData->receiver_id = $employeeId;
                $notificationData->is_read = 0;
                $notificationData->save();
                $notification = Notification_data::with('sender')->find($notificationData->id);
                broadcast(new PrivateEvent($notification))->toOthers();
            }
        }
    }

    public function stripeCardRemove(Request $request) {
        $stripe = new Stripe(config('services.stripe.secret'));
        $current_user = User::where('id', Auth::id())->first();
        if (!empty($current_user)) {
            $stripe->cards()->delete($current_user->stripe_customer_id, $request->card_id);
        }
        return response()->json(['message' => 'Card removed successfully.'], 200);
    }

    public function clientPlan() {
        return view('frontend.client.services')->with(['serviceData' => User::find(Auth::id())->services()
                            ->where('service_user.status', 1)
                            ->orderBy('service_user.created_at', 'DESC')
                            ->get(['services.id', 'services.title', 'services.image', 'services.title_bg_color'])]);
    }

    public function clientResourcesCategories() {
        return view('frontend.client.resources_categories')->with(['resourceCategory' => ResourceCategory::orderBy('order')->get()]);
    }

    public function clientResources($client, $id) {
        $topics = ResourceTopic::where('resource_category_id', $id)->orderBy('id', 'desc')->get();
        return view('frontend.client.resources', compact('topics'));
    }

    public function clientResourceDetails($client, $id) {
        $topic = ResourceTopic::with('resourceTopicFiles')->find($id);
        $categories = ResourceCategory::get()->pluck('title', 'id');
        return view('frontend.client.resources_details', compact('topic', 'categories'));
    }

    public function json() {
        $input = [];
        $input[0]['description'] = 'Household Income';
        $input[0]['validation'] = 'validation';
        $input[1]['description'] = 'Household take home pay';
        $input[1]['validation'] = 'validation';
        $input[2]['description'] = 'Annual income refund if any';
        $input[2]['validation'] = 'validation';
        $input = json_encode($input);

        $a = new QuestionInput;
        $a->inputs = $input;
        $a->count = 1;
    }

    private function _getDocusignAccountInformation($docusign_email, $docusign_password, $integratorKey) {
        $header = "<DocuSignCredentials><Username>" . $docusign_email . "</Username><Password>" . $docusign_password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";
        $url = "https://demo.docusign.net/restapi/v2/login_information";
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("X-DocuSign-Authentication: $header"));

        $json_response = curl_exec($curl);
        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            return 'error';
        }
        $response = json_decode($json_response, true);
        curl_close($curl);
        return $response;
    }

    public function sendDocumentForDigitalSignature($client, $attempted_question_id = null) {
        if ($attempted_question_id == null) {
            return 'question id null';
        }
        $checkIfLoggedInUserAttemptedTheQuestion = AttemptedQuestion::where('user_id', auth()->user()->id)->where('id', $attempted_question_id)->exists();
        $AttemptedQuestionAnswerData = AttemptedQuestionAnswer::where('attempted_question_id', $attempted_question_id)->select(['id', 'attempted_question_id', 'answer',
                    'envelope_id'])->first();
        if ($AttemptedQuestionAnswerData != null && $checkIfLoggedInUserAttemptedTheQuestion) {
            $AttemptedQuestionAnswerData = $AttemptedQuestionAnswerData->toArray();
            if ($AttemptedQuestionAnswerData['envelope_id'] == null) {
                $docusign_email = 'mk4677070@gmail.com';
                $docusign_password = 'manpreet@docusign';
                $integratorKey = '2045e376-db32-4fbe-b292-c274718e65af';
                $header = "<DocuSignCredentials><Username>" . $docusign_email . "</Username><Password>" . $docusign_password . "</Password><IntegratorKey>" . $integratorKey . "</IntegratorKey></DocuSignCredentials>";
                $user_info = Profile::where('user_id', Auth::id())->select('first_name')->first();
                $response = $this->_getDocusignAccountInformation($docusign_email, $docusign_password, $integratorKey);
                if ($response != 'error') {
                    $accountId = $response["loginAccounts"][0]["accountId"];
                    $baseUrl = $response["loginAccounts"][0]["baseUrl"];
                    $recipient_email = Auth::user()->email;
                    $recipient_name = $user_info->first_name;
                    $recipientId = Auth::id();
                    if (!empty($AttemptedQuestionAnswerData['answer'])) {
                        $file_name = explode("/", $AttemptedQuestionAnswerData['answer']);
                        if (count($file_name) != 4) {
                            return response()->json('invalid document', 400);
                        }
                        $document_name = $file_name[3];
                        $documentFileName = public_path($AttemptedQuestionAnswerData['answer']);
                        //get email body
                        $data = $this->_getDocusignEmailContent($document_name, $recipient_email, $recipient_name, $recipientId);
                        $file_contents = file_get_contents($documentFileName);

                        $requestBody = $this->_getDocusignRequestBody($data, $file_contents);

                        $curl = curl_init($baseUrl . "/envelopes");
                        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($curl, CURLOPT_POST, true);
                        curl_setopt($curl, CURLOPT_POSTFIELDS, $requestBody);
                        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                            'Content-Type: multipart/form-data;boundary=myboundary',
                            'Content-Length: ' . strlen($requestBody),
                            "X-DocuSign-Authentication: $header")
                        );

                        $json_response = curl_exec($curl);
                        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                        if ($status != 201) {
                            return response()->json($json_response, $status);
                        }

                        $result = json_decode($json_response, true);
                        $envelopeId = $result["envelopeId"];
                        DB::beginTransaction();
                        if (AttemptedQuestionAnswer::where('id', $AttemptedQuestionAnswerData['id'])->update(['envelope_id' => $envelopeId])) {
                            DB::commit();
                            return response()->json('Request Sent', 200);
                        }
                        DB::rollBack();
                    } else {
                        return response()->json('No document found', 400);
                    }
                }
                return response()->json('Inavlid', 401);
            } else {
                return response()->json('The request is already sent', 401);
            }
        } else {
            return response()->json('No document', 400);
        }
    }

    private function _getDocusignEmailContent($document_name, $recipient_email, $recipient_name, $recipientId) {
        $data = "{
                \"emailBlurb\":\"This comes from PHP\",
                \"emailSubject\":\"DocuSign API - Please Sign This Document...\",
                \"documents\":[
                  {
                    \"documentId\":\"1\",
                    \"name\":\"" . $document_name . "\"
                  }
                ],
                \"recipients\":{
                  \"signers\":[
                    {
                      \"email\":\"$recipient_email\",
                      \"name\":\"$recipient_name\",
                      \"recipientId\":\"$recipientId\",
                      \"tabs\":{
                        \"signHereTabs\":[
                          {
                            \"AnchorTabString\":\"Signature:\",
                            \"anchorXOffset\":\"5000\",
                            \"anchorYOffset\":\"4000\",
                            \"documentId\":\"1\",
                            \"IgnoreIfNotPresent\":\"True\",
                            \"pageNumber\":\"1\"
                          }
                        ]
                      }
                    }
                  ]
                },
                \"status\":\"sent\"
              }";
        return $data;
    }

    private function _getDocusignRequestBody($data, $file_contents) {
        $requestBody = "\r\n"
                . "\r\n"
                . "--myboundary\r\n"
                . "Content-Type: application/json\r\n"
                . "Content-Disposition: form-data\r\n"
                . "\r\n"
                . "$data\r\n"
                . "--myboundary\r\n"
                . "Content-Type:application/pdf\r\n"
                . "Content-Disposition: file; filename=\"order_form.pdf\"; documentid=1 \r\n"
                . "\r\n"
                . "$file_contents\r\n"
                . "--myboundary--\r\n"
                . "\r\n";
        return $requestBody;
    }

    public function serviceUploadFile(Request $request, $client, $clientId = null) {
        $this->validate($request, [
            'file' => 'required'
        ]);

        if ($request->hasFile('file')) {
            $user_id = Auth::user()->id;
            if (isset($clientId) && !empty($clientId)) {
                $user_id = $clientId;
            }
            $directoryUni = public_path('/') . 'files' . '/' . $user_id;
            $file = $request['file'];
            $directoryPath = 'services';
            $directory = public_path('/') . $directoryPath;
            $fileName = date('d-m-y-h-i-s-') . preg_replace('/\s+/', '-', trim($file->getClientOriginalName()));

            if (!file_exists($directory)) {
                mkdir($directory, 0777, true);
            }
            if (!file_exists($directoryUni)) {
                mkdir($directoryUni, 0777, true);
            }
            if ($file->move($directory, $fileName)) {

//                Mail::send(new mailConfirmationUserDocumentUpload($name, $email, $fileName));
                $clientId = Auth::user()->id;
                copy($directory . '/' . $fileName, $directoryUni . '/' . $fileName);
                $client_employee = ClientToEmployeeUser::where('client_user_id', $clientId)->with(['employeeUser' => function($query) {
                                $query->select('id', 'name');
                            }])->first();
                if (isset($client_employee) && !empty($client_employee)) {
                    $employeeId = $client_employee->employee_user_id;

                    $userInfo = User::where('id', $clientId)->with(['profile' => function($query) {
                                    $query->select('id', 'user_id', 'first_name');
                                }])->first();
                    $name = "";
                    if ($userInfo->name != '') {
                        $name = $userInfo->name;
                    } else {
                        $name = $userInfo->profile->first_name;
                    }
                    $employee_name = $client_employee->employeeUser->name;
                    $text = "Hi " . $employee_name . ", Your Client '" . $name . "' has Successfuly Uploaded Document " . $fileName . " to your  profile";
                    $notificationData = new Notification_data;
                    $notificationData->notification_room_id = -1;
                    $notificationData->sender_id = $clientId;
                    $notificationData->notification = $text;
                    $notificationData->receiver_id = $employeeId;
                    $notificationData->is_read = 0;
                    $notificationData->save();
                    $notification = Notification_data::with('sender')->find($notificationData->id);
                    broadcast(new PrivateEvent($notification))->toOthers();
                }



                return response()->json(['message' => $fileName], 200);
            }
            return response()->json(['message' => 'Error while uploadinig the file.'], 500);
        }
        return response()->json(['message' => 'Invalid file.'], 500);
    }

    public function answerQuestions(Request $request) {
        $this->validate($request, [
            'questionName' => 'required',
            'answer' => 'required',
        ]);

        $data = $request->all();
        $services = $data['serviceId'];
        $serviceInfo = Service::select('title')->where('id', $services)->first();
        $userInfo = User::where('id', Auth::id())->with('profile')->first();
        $name = "";
        if ($userInfo->name != '') {
            $name = $userInfo->name;
        } else {
            $name = $userInfo->profile->first_name;
        }
//        echo'<PRE>';
//        print_r($data);
//        die;
        $edit_count = 0;
        if (isset($data['topicId'])) {
            $topicid = $data['topicId'];
            if (isset($data['subTopicId'])) {
                $subtopicid = $data['subTopicId'];
//                print_r(['track_id' => $subtopicid, 'service_id' => $services]);
                $parent_topic_id = Topics::where([
                                    'track_id' => $topicid,
                                    'service_id' => $services
                                ])
                                ->whereNull('parent_id')->pluck('id')->first();
                $topic_id = Topics::where(
                                ['track_id' => $subtopicid,
                                    'service_id' => $services,
                                    'parent_id' => $parent_topic_id
                        ])->pluck('id')->first();
//                echo $topic_id;die;

                foreach ($data['questionName'] as $key => $question) {
                    $checkData = SubTopicQuestion::where('service_id', $services)->where('topic_id', $topicid)->where('sub_topic_id', $subtopicid)->where('question_id', $key)->count();
                    if ($checkData != 0) {
                        $data1 = SubTopicQuestionAnswer::where('user_id', Auth::id())->where('question_id', $key)->count();
                        if (array_key_exists($key, $data['answer'])) {
                            if ($data1 == 0) {

                                $insertData = new SubTopicQuestionAnswer();
                                $insertData->user_id = Auth::id();
                                $insertData->service_id = $services;
                                $insertData->topic_id = $topicid;
                                $insertData->sub_topic_id = $subtopicid;
                                $insertData->question_id = $key;
                                $insertData->answer = json_encode($data['answer'][$key]);
                                $insertData->save();
//                                $this->_sendEmailConfirmationOnServiceEnrollment($serviceInfo->title, $name, $userInfo->email);
                            } else {
                                $subTopicData = SubTopicQuestionAnswer::where('user_id', Auth::id())->where(['service_id' => $services,
                                            'question_id' => $key, 'sub_topic_id' => $subtopicid,
                                            'topic_id' => $topicid])->update(['answer' => json_encode($data['answer'][$key])]);
                                $edit_count++;
                            }
                        }
                    }

                    if (isset($data['answer'][$key]) && !empty($data['answer'][$key])) {

                        $all_answers = $data['answer'][$key];
                        if (isset($all_answers) && !empty($all_answers)) {
                            $this->_formatAnswers($all_answers, $topic_id, $services, $key);
                        }
                    }
                }
//                die('hhhh');

                if ($edit_count) {
                    $this->_sendEmailOnEditService($serviceInfo->title, $name, $userInfo->email);
                    $this->_notifyEmployeeOnServiceUpdate($serviceInfo->title);
                }
                if ($request->has('redirectPageName')) {
                    return redirect()->route('frontend.client.subTopicPreview', [config('constant.subdomain'), $services,
                                $topicid,
                                $data['redirectPageName']])->withFlashSuccess('Record added successfully.');
                }
            } else {
                $topic_id = Topics::where(['track_id' => $topicid, 'service_id' => $services])->whereNull('parent_id')->pluck('id')->first();
//                dd(['track_id'=> $topicid,'service_id'=>$services]);
                echo $topic_id;
//die;
                foreach ($data['questionName'] as $key => $question) {
                    $checkData = TopicQuestion::where('service_id', $services)->where('topic_id', $topicid)->where('question_id', $key)->count();
                    if ($checkData != 0) {
                        $data1 = TopicQuestionAnswer::where('user_id', Auth::id())->where(['service_id' => $services,
                                    'question_id' => $key, 'topic_id' => $topicid])->count();
                        if (array_key_exists($key, $data['answer'])) {
                            if ($data1 == 0) {
                                $insertData = new TopicQuestionAnswer();
                                $insertData->user_id = Auth::id();
                                $insertData->service_id = $services;
                                $insertData->topic_id = $topicid;
                                $insertData->question_id = $key;
                                $insertData->answer = json_encode($data['answer'][$key]);
                                $insertData->save();
//                                $this->_sendEmailConfirmationOnServiceEnrollment($serviceInfo->title, $name, $userInfo->email);
                            } else {
                                TopicQuestionAnswer::where('user_id', Auth::id())->where([
                                    'service_id' => $services,
                                    'question_id' => $key, 'topic_id' => $topicid])->update([
                                    'answer' => json_encode($data['answer'][$key])]);
                                $edit_count++;
                            }
                        }
                    } else {
                        return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $services])->withFlashDanger('Form does not exists.');
                    }
                    if (isset($data['answer'][$key]) && !empty($data['answer'][$key])) {

                        $all_answers = $data['answer'][$key];
                        if (isset($all_answers) && !empty($all_answers)) {
                            $this->_formatAnswers($all_answers, $topic_id, $services);
                        }
                    }
                }

                if ($edit_count) {
                    $this->_sendEmailOnEditService($serviceInfo->title, $name, $userInfo->email);
                    $this->_notifyEmployeeOnServiceUpdate($serviceInfo->title);
                }

                if ($services == 2 || $services == 5) {
                    return redirect()->route('frontend.client.openPreviewFile', [config('constant.subdomain'), $services]);
                }
                return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $services])->withFlashSuccess('Record added successfully.');
            }
        } else {

            foreach ($data['questionName'] as $key => $question) {

                $checkData = SelectedServiceQuestion::where('service_id', $services)->where('question_id', $key)->count();
                if ($checkData != 0) {
                    $data1 = SelectedServiceQuestionAnswer::where(['service_id' => $services,
                                'user_id' => Auth::id()])->where('question_id', $key)->count();
                    if (array_key_exists($key, $data['answer'])) {
                        if ($data1 == 0) {
                            $insertData = new SelectedServiceQuestionAnswer();
                            $insertData->user_id = Auth::id();
                            $insertData->service_id = $services;
                            $insertData->question_id = $key;
                            $insertData->answer = json_encode($data['answer'][$key]);
                            $insertData->save();
//                            $this->_sendEmailConfirmationOnServiceEnrollment($serviceInfo->title, $name, $userInfo->email);
                        } else {
                            SelectedServiceQuestionAnswer::where('user_id', Auth::id())->where('question_id', $key)->update([
                                'answer' => json_encode($data['answer'][$key])]);
                            $edit_count++;
                        }
                    }
                } else {
                    return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $services])->withFlashDanger('The question does not exists in database.');
                }

                if (isset($data['answer'][$key]) && !empty($data['answer'][$key])) {
                    $all_answers = $data['answer'][$key];
                    if (isset($all_answers) && !empty($all_answers)) {
                        if (isset($all_answers['files']) && !empty($all_answers['files'])) {
                            $answer_encoded = json_encode($all_answers['files']);
//                        print_r($answer_encoded);
                            $question_id = Questions::where(['service_id' => $services, 'name' => 'files'])->whereNull('topic_id')->pluck('id')->first();
//                        print_r(['topic_id'=> $topic_id,'service_id'=> $services,'name'=> 'files']);
//                            dd($question_id);
                            if (isset($question_id) && !empty($question_id) && !is_null($answer_encoded) && $answer_encoded != '') {
                                $this->_saveAnswer($question_id, $answer_encoded);
                            }
                        } else {
                            foreach ($all_answers as $single_answer_key => $single_answer) {
                                $single_json_answers = json_decode($single_answer, true);
//                                print_r($single_json_answers);
                                if (is_array($single_json_answers)) {
                                    if (isset($single_json_answers) && !empty($single_json_answers)) {
                                        if (count($single_json_answers) > 0) {
                                            $single_json_answers = $this->_combineAnswersOfQuestion($single_json_answers);
                                        }
//                                    print_r($single_json_answers);


                                        foreach ($single_json_answers as $single_json_key => $single_json_value) {
                                            if (!is_string($single_json_value)) {
                                                $single_json_value = (array) $single_json_value;
//                                    print_R($single_json_value);
                                                foreach ($single_json_value as $single_json_answer_key => $single_json_answer_value) {
//                                                print_r($single_json_answer_key);
                                                    $question_id = Questions::whereNull('topic_id')->where(['service_id' => $services, 'name' => $single_json_answer_key])->pluck('id')->first();
//                                        dd(v)
//                                        echo $question_id;
                                                    if (isset($question_id) && !empty($question_id) && !is_null($single_json_answer_value) && $single_json_answer_value != '') {
                                                        $this->_saveAnswer($question_id, $single_json_answer_value);
                                                    }
                                                }
                                            } else {
                                                $question_id = Questions::whereNull('topic_id')->where('service_id', $services)->where('name', $single_json_key)->pluck('id')->first();
                                                if (isset($question_id) && !empty($question_id) && !is_null($single_json_value) && $single_json_value != '') {
                                                    $this->_saveAnswer($question_id, $single_json_value);
                                                }
                                            }
                                        }
                                    }
                                } else {

                                    $question_id = Questions::whereNull('topic_id')->where('service_id', $services)->where('name', $single_answer_key)->pluck('id')->first();
                                    if (isset($question_id) && !empty($question_id) && !is_null($single_answer) && $single_answer != '') {
                                        $this->_saveAnswer($question_id, $single_answer);
                                    }
                                }
                            }
                        }
                    }
                }
            }
//                die('else');

            if ($edit_count) {
                $this->_sendEmailOnEditService($serviceInfo->title, $name, $userInfo->email);
                $this->_notifyEmployeeOnServiceUpdate($serviceInfo->title);
            }
            if ($services == 2) {
                Session::put('your_goal', $data['answer'][44]['your_goal']);
                return redirect()->route('frontend.client.openPreviewFile', [config('constant.subdomain'), $services]);
            } else if ($services == 5) {
                return redirect()->route('frontend.client.openPreviewFile', [config('constant.subdomain'), $services]);
            } else if (in_array($services, [4, 9])) {
                return redirect()->route('frontend.client.selectedService', [config('constant.subdomain'), $services]);
            } else {
                return redirect()->route('frontend.client.recommendedServices', [config('constant.subdomain')]);
            }
        }
    }

    private function _sendEmailConfirmationOnServiceEnrollment($serviceName, $userName, $userEmail) {
        Mail::send(new mailConfirmationOnServiceEnrollment($serviceName, $userName, $userEmail));
    }

    private function _sendEmailOnEditService($serviceName, $userName, $userEmail) {
        Mail::send(new mailConfirmationOnEditServiceData($serviceName, $userName, $userEmail));
    }

    public function openSubTopicPreviewPage($client = null, $id, $topicId, $pageName) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $id)->first();
        $subTopics = SubTopicQuestion::select('sub_topic_id', 'topic_id')->where([
                    'topic_id' => $topicId, 'service_id' => $id])->get();
        $subTopicsGroup = $subTopics->groupBy('sub_topic_id');
        $subTopicInfo = [];
        $contact = $this->_getContactDetails();
        foreach ($subTopicsGroup as $key => $subTopicData) {
            $answer = SubTopicQuestionAnswer::where(['topic_id' => $topicId, 'service_id' => $id,
                        'sub_topic_id' => $key, 'user_id' => Auth::id()])->first();
            if (!empty($answer)) {
                $subTopicInfo[$key] = 'Revisit';
            } else {
                $subTopicInfo[$key] = 'Begin';
            }
        }
        $defaultData = $this->getSignupQuestionValueForService();
        return view('frontend.client.serviceQuestions.' . $pageName, compact('currentService', 'defaultData', 'subTopicInfo', 'contact'));
    }

    public function openPreviewFile($client = null, $serviceId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $serviceId)->first();
        $topicsInfo = $this->getTopicData($serviceId);
        $contact = $this->_getContactDetails();
        if ($serviceId == 5) {
            return view('frontend.client.serviceQuestions.DebtReduction-preview', compact('currentService', 'topicsInfo', 'contact'));
        } elseif ($serviceId == 2) {
            return view('frontend.client.serviceQuestions.goal-specific-preview', compact('currentService', 'topicsInfo', 'contact'));
        } else {
            return redirect()->back();
        }
    }

    public function fetchAnswers($client = null, $id, $topicId, $questionId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $id)->first();
        $defaultData = $this->getSignupQuestionValueForService();

        $topicAnswer = TopicQuestionAnswer::where(['topic_id' => $topicId, 'service_id' => $id])->where('user_id', Auth::id())->get();
        $contact = $this->_getContactDetails();
        $answer = [];
        if ($id == 3 && $topicId == 303) {
            $fetchExtraFieldAnswer = TopicQuestionAnswer::where(['topic_id' => 302,
                        'service_id' => $id])->where('user_id', Auth::id())->first();
            if (!empty($fetchExtraFieldAnswer)) {
                $answerData = json_decode($fetchExtraFieldAnswer['answer'], true);

                if (array_key_exists('own_or_rent_your_house', $answerData)) {
                    $answer['own'] = $answerData['own_or_rent_your_house'];
                }
            }
        }
        if (!empty($topicAnswer)) {
            foreach ($topicAnswer as $key => $topic) {
                $answerInfo = json_decode($topic['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
            $userFields = ['id', 'email', 'two_factor_auth'];

            $profileFields = ['id', 'user_id', 'sex', 'first_name', 'middle_name', 'last_name', 'first_address', 'last_address', 'city', 'state_id',
                'zip', 'phone_number', 'phone_type', 'phone_verified', 'dob', 'ssn'];
            $states = State::pluck('name', 'id');
            $userData = Profile::with('state')->where('user_id', Auth::id())->first($profileFields)->toArray();
            $defaultData['userData'] = $userData;
            return view('frontend.client.serviceQuestions.question_' . $questionId, compact('currentService', 'answer', 'defaultData', 'contact', 'states'));
        }

        return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $id])->withFlashDanger('Unable to open the question of this service.');
    }

    public function getEducationInformation($client = null) {
        $serviceInformation = SelectedServiceQuestionAnswer::select('answer')->where([
                    'question_id' => 44, 'service_id' => 2])->where('user_id', Auth::id())->first();
        if (!empty($serviceInformation)) {
            $answerInfo = json_decode($serviceInformation['answer'], true);
            $data['your_goal'] = $answerInfo['your_goal'];
            $response['success'] = $data;
            return $response;
        }
        return 'empty';
    }

    public function fetchAnswersSubTopic($client = null, $id, $topicId, $subtopicId, $questionId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $id)->first();
        $defaultData = $this->getSignupQuestionValueForService();
        $subTopicAnswer = SubTopicQuestionAnswer::where(['service_id' => $id, 'topic_id' => $topicId,
                    'sub_topic_id' => $subtopicId])->where('user_id', Auth::id())->get();
        $contact = $this->_getContactDetails();
        $answer = [];
        if (!empty($subTopicAnswer)) {
            foreach ($subTopicAnswer as $key => $subTopic) {
                $answerInfo = json_decode($subTopic['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
            return view('frontend.client.serviceQuestions.question_' . $questionId, compact('currentService', 'answer', 'defaultData', 'contact'));
        }

        return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $id])->withFlashDanger('Unable to open the question of this service.');
    }

    public function goalSpecificPlanning($client = null, $id, $topicId, $questionId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $id)->first();
        $topicQuestion = TopicQuestion::where(['topic_id' => $topicId, 'question_id' => $questionId])->first();
        $contact = $this->_getContactDetails();
        if (!empty($topicQuestion)) {
            $new = TopicQuestionAnswer::where(['topic_id' => $topicId])->where('user_id', Auth::id())->first();
            $answer = [];

            if (!empty($new)) {
                $new3 = json_decode($new['answer'], true);
                foreach ($new3 as $key1 => $value) {
                    $d = array_values($value);
                    $answer[$key1] = $d[0];
                }

                return view('frontend.client.serviceQuestions.question_' . $questionId, compact('currentService', 'answer', 'contact'));
            }
        }

        return redirect()->route('frontend.client.servicesQuestion', [config('constant.subdomain'), $id])->withFlashDanger('Unable to open the question of this service.');
    }

    public function testPages($client = null) {
        return view('frontend.client.serviceQuestions.comprehensive-financial-service-first-section');
    }

    public function notification(Request $request) {


        $notificationData = Notification_data::where('receiver_id', $request->userId)->orderBy('created_at', 'DESC')->get()->toArray();
        $notificationDataUnread = Notification_data::where('receiver_id', $request->userId)->where('is_read', 0)->orderBy('created_at', 'DESC')->get()->toArray();
        return response()->json(['data' => $notificationData, 'unReadData' => $notificationDataUnread], 200);
//            $query->select("text");
//        }])->
//        
    }

    public function isRead(Request $request) {

        $notificationData = Notification_data::where('receiver_id', $request->userId)->update(['is_read' => 1]);
        return response()->json(['data' => $notificationData], 200);
    }

    private function _notifyEmployeeOnServiceUpdate($serviceTitle) {
        $clientId = Auth::user()->id;
        $client_employee = ClientToEmployeeUser::where('client_user_id', $clientId)->with(['employeeUser' => function($query) {
                        $query->select('id', 'name');
                    }])->first();
        if (isset($client_employee) && !empty($client_employee)) {
            $employeeId = $client_employee->employee_user_id;


            $userInfo = User::where('id', $clientId)->with(['profile' => function($query) {
                            $query->select('id', 'user_id', 'first_name');
                        }])->first();
            $name = "";
            if ($userInfo->name != '') {
                $name = $userInfo->name;
            } else {
                $name = $userInfo->profile->first_name;
            }
            $employee_name = $client_employee->employeeUser->name;
            $text = "Hi " . $employee_name . ", Your Client '" . $name . "' has Successfuly Updated Service " . $serviceTitle;
            $notificationData = new Notification_data;
            $notificationData->notification_room_id = -1;
            $notificationData->sender_id = $clientId;
            $notificationData->notification = $text;
            $notificationData->receiver_id = $employeeId;
            $notificationData->is_read = 0;
            $notificationData->save();
            $notification = Notification_data::with('sender')->find($notificationData->id);
            broadcast(new PrivateEvent($notification))->toOthers();
        }
    }

    private function _saveAnswer($question_id, $answer) {
        $answerRecord = QuestionAnswers::where(['user_id' => Auth::id(), 'question_id' => $question_id])->with('question')->first();
        if (empty($answerRecord)) {
            $answerRecord = new QuestionAnswers;
        }
//        echo'here<br>';
//        print_r($answer);


        if (is_array($answer) && count($answer) > 0) {
            $answer = json_encode($answer);
        }
//echo'<pre>';print_r($answer);die;
        $answerRecord->user_id = Auth::id();
        $answerRecord->question_id = $question_id;
        $answerRecord->answer = $answer;

//        print_r($answerRecord);
//        die;
        $answerRecord->save();
    }

    private function _combineAnswersOfQuestion($single_json_answers) {
        $answer_to_return = [];
//        print_R($single_json_answers);
//        echo'kdfjsalfjdas';
        if (isset($single_json_answers) && !empty($single_json_answers)) {
            foreach ($single_json_answers as $single_key => $single_json_answer) {

                if (isset($single_json_answer) && !empty($single_json_answer)) {
                    foreach ($single_json_answer as $single_json_answer_key => $single_json_answer_value) {
                        if (is_array($single_json_answer_value) && $single_json_answer_key != 'filesComprehensive') {
//                           
                            $answer_to_return[0][$single_json_answer_key][]['value'] = serialize($single_json_answer_value);
                        } elseif (is_array($single_json_answer_value) && $single_json_answer_key == 'filesComprehensive') {
//                           echo'iiin';
//                            print_R($single_json_answer_value);
                            $array_final = [];
                            foreach ($single_json_answer_value as $single_file) {
                                if (isset($single_file['file_path']) && !empty($single_file['file_path'])) {
                                    $array_final[$single_file['key']]['value'] = ['file_name' => $single_file['file_name'], 'file_path' => $single_file['file_path']];
                                }
                            }
                            $answer_to_return[0][$single_json_answer_key][$single_key] = $array_final;
                        } else {

                            $answer_to_return[0][$single_json_answer_key][]['value'] = $single_json_answer_value;
                        }
                    }
                }
            }
        }
//                                    print_r($answer_to_return);
//
//        die;
        return $answer_to_return;
    }

    private function _formatAnswers($all_answers, $topic_id, $services, $key = false) {

        if (isset($all_answers['files']) && !empty($all_answers['files'])) {
            $answer_encoded = json_encode($all_answers['files']);
//                        print_r($answer_encoded);
            $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => 'files'])->pluck('id')->first();
//                        print_r(['topic_id'=> $topic_id,'service_id'=> $services,'name'=> 'files']);
//                            dd($question_id);
            if (isset($question_id) && !empty($question_id) && !is_null($answer_encoded) && $answer_encoded != '') {
                $this->_saveAnswer($question_id, $answer_encoded);
            }
            unset($all_answers['files']);
        }
        if (isset($all_answers['copy']) && !empty($all_answers['copy'])) {
            $copy_array = $all_answers['copy'];
            $final_answers = [];
            if (isset($copy_array) && !empty($copy_array)) {
                foreach ($copy_array as $single_key => $single_value) {
                    if (isset($single_value) && !empty($single_value)) {
                        foreach ($single_value as $single_key_nested => $single_value_nested) {
                            $single_key_nested = preg_replace('/[^a-z_]/i', '', $single_key_nested);
                            $final_answers[$single_key_nested][] = $single_value_nested;
                        }
                    }
                }
                $this->_formatNestedAnswersSets($final_answers, $topic_id, $services);
            }
            unset($all_answers['copy']);
        }
        if (isset($all_answers['copypersonal']) && !empty($all_answers['copypersonal'])) {
            echo'<PRE>';
            $copy_array = $all_answers['copypersonal'];
            $final_answers = [];
            if (isset($copy_array) && !empty($copy_array)) {
                foreach ($copy_array as $single_key => $single_value) {
                    if (isset($single_value) && !empty($single_value)) {
                        foreach ($single_value as $single_key_nested => $single_value_nested) {
                            $single_key_nested = preg_replace('/[^a-z_]/i', '', $single_key_nested);
                            $final_answers[$single_key_nested][] = $single_value_nested;
                        }
                    }
                }
                $this->_formatNestedAnswersSets($final_answers, $topic_id, $services, 'personal');
            }
            unset($all_answers['copypersonal']);
        }
//        echo'<PRE>';print_r($all_answers);
        foreach ($all_answers as $single_answer_key => $single_answer) {

            if (!is_array($single_answer)) {
                $single_answer_decoded = json_decode($single_answer, true);
                if (is_string($single_answer) && !is_array($single_answer_decoded)) {
                    print_R(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key])
                    ;
                    $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key])->pluck('id')->first();
                    if (isset($question_id) && !empty($question_id) && !is_null($single_answer) && $single_answer != '') {
                        $this->_saveAnswer($question_id, $single_answer);
                    }
                }
            }
        }
        $this->_formatNestedAnswers($all_answers, $topic_id, $services);
    }

    private function _mergeComprehensiveFiles($single_json_answer_value) {
        $final_array = [];
        if (isset($single_json_answer_value) && !empty($single_json_answer_value)) {
            foreach ($single_json_answer_value as $key => $single_upload) {
                if (isset($single_upload) && !empty($single_upload)) {
                    foreach ($single_upload as $single_key => $single_value) {
                        $final_array[$single_key][$key] = $single_value;
                    }
                }
            }
        }
        return $final_array;
    }

    private function _formatNestedAnswers($all_answers, $topic_id, $services, $is_copy_modal = false, $personal = false) {
        $display_type = config('constant.questions.general');
        if ($personal) {
            $display_type = config('constant.questions.personal');
        }



        if ($is_copy_modal) {
            $single_copy_answers = $this->_combineAnswersCopiesOfQuestion($all_answers);

            foreach ($single_copy_answers as $single_copy_answers_key => $single_copy_answers_value) {
//                echo $single_copy_answers_key;
                $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_copy_answers_key, 'display_type' => $display_type])->whereNotNull('parent_id')->pluck('id')->first();
                if (isset($question_id) && !empty($question_id) && !is_null($single_copy_answers_value) && $single_copy_answers_value != '') {
                    $this->_saveAnswer($question_id, $single_copy_answers_value);
                }
            }
        } else {
            if (in_array($topic_id, [config('constant.questions.comprehensive_life_insurance_id'), config('constant.questions.comprehensive_spouse_life_insurance_id')])) {
                $display_type = config('constant.questions.general_life');
            }
            foreach ($all_answers as $single_answer_key => $single_answer) {
                if (in_array($single_answer_key, ['files', 'copy', 'copypersonal'])) {
                    continue;
                }
                if (!is_array($single_answer)) {
                    $single_json_answers = json_decode($single_answer, true);
                    if (is_array($single_json_answers)) {
                        if (isset($single_json_answers) && !empty($single_json_answers)) {
                            if (count($single_json_answers) > 0) {
                                $single_json_answers = $this->_combineAnswersOfQuestion($single_json_answers);
                            }
                            foreach ($single_json_answers as $single_json_key => $single_json_value) {
                                if (!is_string($single_json_value)) {
                                    $single_json_value = (array) $single_json_value;
                                    foreach ($single_json_value as $single_json_answer_key => $single_json_answer_value) {
                                        if ($single_json_answer_key == 'filesComprehensive') {
                                            $single_json_answer_value = $this->_mergeComprehensiveFiles($single_json_answer_value);

                                            foreach ($single_json_answer_value as $single_json_answer_files_key => $single_json_answer_files_value) {
                                                $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_json_answer_files_key, 'display_type' => $display_type])->whereNotNull('parent_id')->pluck('id')->first();
//                                        echo $single_json_answer_files_key;echo'<BR>';
//echo $question_id;
                                                if (isset($question_id) && !empty($question_id) && !is_null($single_json_answer_files_value) && $single_json_answer_files_value != '') {
                                                    $this->_saveAnswer($question_id, $single_json_answer_files_value);
                                                }
                                            }
                                        } else {
                                            $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_json_answer_key, 'display_type' => $display_type])->whereNotNull('parent_id')->pluck('id')->first();


                                            if (isset($question_id) && !empty($question_id) && !is_null($single_json_answer_value) && $single_json_answer_value != '') {
                                                $this->_saveAnswer($question_id, $single_json_answer_value);
                                            }
                                        }
                                    }
                                } else {
                                    $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_json_key, 'display_type' => $display_type])->pluck('id')->first();

                                    if (isset($question_id) && !empty($question_id) && !is_null($single_json_value) && $single_json_value != '') {
                                        $this->_saveAnswer($question_id, $single_json_value);
                                    }
                                }
                            }
                        }
                    } else {
                        $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key, 'display_type' => $display_type])->pluck('id')->first();
                        if (isset($question_id) && !empty($question_id) && !is_null($single_answer) && $single_answer != '') {
                            $this->_saveAnswer($question_id, $single_answer);
                        }
                    }
                } elseif (is_array($single_answer) && $single_answer_key) {
                    $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key, 'display_type' => $display_type])->whereNotNull('parent_id')->pluck('id')->first();
                    if (isset($question_id) && !empty($question_id) && !is_null($single_answer) && $single_answer != '') {
                        $this->_saveAnswer($question_id, $single_answer);
                    }
                }
            }
        }
    }

    private function _formatNestedAnswersSets($all_answers, $topic_id, $services, $personal = false) {
        $is_copy_modal = false;
//              echo'<PRE>';print_r($all_answers);die;
        $display_type = config('constant.questions.general');
        if ($personal) {
            $display_type = config('constant.questions.personal');
        }
        foreach ($all_answers as $single_answer_key => $single_answer) {

            if (is_array($single_answer)) {
                if (isset($single_answer) && !empty($single_answer)) {
                    $single_first_answers_json = json_decode($single_answer[0], true);


                    if (!is_array($single_first_answers_json)) {

                        $json_answers = $this->_combineAnswersSetsOfQuestion($single_answer_key, $single_answer);
                        $answer_value_single = '';
                        if (isset($json_answers) && !empty($json_answers)) {
                            $answer_value_single = $json_answers[$single_answer_key];
                        }

                        $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key, 'display_type' => $display_type])->pluck('id')->first();
//                        echo $question_id;
                        if (isset($question_id) && !empty($question_id) && !is_null($answer_value_single) && $answer_value_single != '') {
                            $this->_saveAnswer($question_id, $answer_value_single);
                        }
                    } else {
                        if (in_array($single_answer_key, ['IBRecords_', 'CBRecords_', 'clickHereFormRecords_', 'clickHereFormRecordsC_', 'ExeCompStockOptionRecords_', 'ExeCompResStockRecords_', 'clickHereFormRecordsOther'])) {
                            $is_copy_modal = true;
                        }
                        $this->_formatNestedAnswers($single_answer, $topic_id, $services, $is_copy_modal, $personal);
                    }
                }
            } else {
                $question_id = Questions::where(['topic_id' => $topic_id, 'service_id' => $services, 'name' => $single_answer_key, 'display_type' => $display_type])->pluck('id')->first();
                if (isset($question_id) && !empty($question_id) && !is_null($single_answer) && $single_answer != '') {
                    $this->_saveAnswer($question_id, $single_answer);
                }
            }
        }
    }

    private function _combineAnswersSetsOfQuestion($single_answer_key, $single_answer) {
        $answer_to_return = [];
        if (isset($single_answer)) {
            foreach ($single_answer as $single_key_nested => $single_answer_nested) {
                if (isset($single_answer_nested) && !empty($single_answer_nested)) {
                    $answer_to_return[$single_answer_key][$single_key_nested]['value'] = $single_answer_nested;
                }
            }
        }
        return $answer_to_return;
    }

    private function _combineAnswersCopiesOfQuestion($all_answers) {
        $answer_to_return = [];
        if (isset($all_answers) && !empty($all_answers)) {
            foreach ($all_answers as $single_key_answer_copy => $single_value_answer_copy) {
                $single_copy_answers = json_decode($single_value_answer_copy, true);
                if (is_array($single_copy_answers)) {
                    if (isset($single_copy_answers) && !empty($single_copy_answers)) {
                        foreach ($single_copy_answers as $single_key_nested => $single_answer_nested) {
                            if (isset($single_answer_nested) && !empty($single_answer_nested)) {
                                if (is_array($single_answer_nested)) {
                                    foreach ($single_answer_nested as $single_answer_deep_nested_key => $single_answer_deep_nested_value) {

                                        if (!is_array($single_answer_deep_nested_value)) {
                                            $answer_to_return[$single_answer_deep_nested_key][$single_key_answer_copy][$single_key_nested]['value'] = $single_answer_deep_nested_value;
                                        } else {
                                            $answer_to_return[$single_answer_deep_nested_key][$single_key_answer_copy][$single_key_nested]['value'] = serialize($single_answer_deep_nested_value);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return $answer_to_return;
    }

    protected function _sendDocumentForSigning($service_id, $email) {


        $url = config('constant.adobe_eSignature.API_URL') . '/public/oauth?redirect_uri=<?php echo $url ?>/documentStatusCallback&response_type=code&client_id=' . config('constant.adobe_eSignature.CLIENT_ID') . '&scope=user_read+user_write+user_login+agreement_read+agreement_write+library_read+workflow_read+workflow_write+webhook_read+webhook_write+webhook_retention';
        $api_access_point = config('constant.adobe_eSignature.api_access_point');
//
        $client = new \GuzzleHttp\Client(['base_uri' => $api_access_point]);

        $refresh_token = config('constant.adobe_eSignature.REFRESH_TOKEN');

//
        $refresh_token_data = $client->request('POST', '/oauth/refresh', [
                    'form_params' => [
                        'grant_type' => 'refresh_token',
                        'client_id' => config('constant.adobe_eSignature.CLIENT_ID'),
                        'client_secret' => config('constant.adobe_eSignature.CLIENT_SECRET'),
                        'refresh_token' => $refresh_token
                    ]
                ])->getBody()->getContents();
        $refresh_token_data = json_decode($refresh_token_data, true);
        $access_token = $refresh_token_data['access_token'];

        $upload_document_url = 'api/rest/v6/transientDocuments';
        $agreement_send_url = 'api/rest/v6/agreements';


        $opts = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $serviceDocuments = ServiceDocument::where('service_id', $service_id)->first();
        if (!empty($serviceDocuments)) {
            $serviceDocuments = $serviceDocuments->toArray();
        } else {
            return true;
        }
        $document_name = $serviceDocuments['document_name'];
        $file_path = config('constant.adobe_eSignature.document_upload_url') . $document_name;

        $output = $client->request('POST', $upload_document_url, [
                    'multipart' => [
                        [
                            'name' => "File",
                            'contents' => fopen($file_path, 'rb', false, stream_context_create($opts)),
                            'headers' => [
                                'Content-Type' => 'multipart/form-data',
                                'Content-Transfer-Encoding' => "binary",
                            ]
                        ],
                    ],
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                    ]
                        ]
                )->getBody()->getContents();

        $transientDocumentResponse = json_decode($output, true);
        $transientDocumentId = $transientDocumentResponse['transientDocumentId'];
        $agreement_data = [
            'fileInfos' =>
            [
                [
                    'transientDocumentId' => $transientDocumentId,
                ]
            ],
            'name' => 'Agreement',
            'participantSetsInfo' =>
            [
                [
                    'memberInfos' =>
                    [
                        [
                            'email' => $email
                        ]
                    ],
                    'order' => 1,
                    'role' => 'SIGNER',
                ],
                [
                    'memberInfos' =>
                    [
                        [
                            'email' => config('constant.adobe_eSignature.approver_email')
                        ]
                    ],
                    'order' => 2,
                    'role' => 'APPROVER',
                ]
            ],
            'signatureType' => 'ESIGN',
            'message' => "Please review and sign this document.",
            'state' => 'AUTHORING',
        ];
        $json_array = json_encode($agreement_data);
        $output_agreement = $client->request('POST', $agreement_send_url, [
                    \GuzzleHttp\RequestOptions::JSON => $agreement_data,
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                        'Content-Type' => 'application/json',
                    ]
                        ]
                )->getBody()->getContents();
        $output_agreement_decoded = json_decode($output_agreement, true);
        if (isset($output_agreement_decoded['id']) && !empty($output_agreement_decoded['id'])) {

            $output_agreement_decoded_id = $output_agreement_decoded['id'];
            sleep(5);
            $agreement_get_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';

            $agreement_get_url = $api_access_point . $agreement_get_url;
            $agreeement_data_adobe = $client->request('GET', $agreement_get_url, [
                'headers' => [
                    'Authorization' => "Bearer " . $access_token,
                ]
            ]);
            $etag = $agreeement_data_adobe->getHeader('ETag')[0];









            $agreement_member_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/members';

            $agreement_member_url = $api_access_point . $agreement_member_url;
            $agreeement_member_data = $client->request('GET', $agreement_member_url, [
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                        ]
                    ])->getBody()->getContents();
            $agreeement_member_data_decoded = json_decode($agreeement_member_data, true);
            foreach ($agreeement_member_data_decoded['participantSets'] as $single) {
                if ($single['role'] == 'SIGNER')
                    $participant_id = $single['id'];
                if ($single['role'] == 'APPROVER')
                    $approver_id = $single['id'];
            }


//die;






            $agreement_fields_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';
            $fields_array = [];

            $document_data = json_decode($serviceDocuments['data'], true);
//            echo'<PRE>';print_R($document_data);die;
            foreach ($document_data as $single) {
                if (isset($single['is_approver']) && !empty($single['is_approver'])) {
                    $single['assignee'] = $approver_id;
                    unset($single['is_approver']);
                } else {
                    $single['assignee'] = $participant_id;
                    if (isset($single['name']) && !empty($single['name']) && $single['name'] == 'client_email') {
                        $single['defaultValue'] = $email;
                    }
                    if (isset($single['displayFormatType']) && !empty($single['displayFormatType']) && $single['displayFormatType'] == 'DATE') {
                        $single['defaultValue'] = date('d/m/Y');
                    }
                }
                $fields_array[] = $single;
            }
//            echo'<PRE>';

            $agreement_fields_data = [
                'fields' =>
                $fields_array
            ];
            $json_array = json_encode($agreement_fields_data);

            $agreement_fields_output = $client->request('PUT', $agreement_fields_url, [
                        \GuzzleHttp\RequestOptions::JSON => $agreement_fields_data,
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                            'Content-Type' => 'application/json',
                            'If-Match' => $etag
                        ]
                            ]
                    )->getBody()->getContents();
//                    print_R(json_decode($agreement_fields_output));
//                    die;
        }
        return true;
    }

    protected function _sendSignUpDocumentForSigning($email) {



        $url = config('constant.adobe_eSignature.API_URL') . '/public/oauth?redirect_uri=<?php echo $url ?>/documentStatusCallback&response_type=code&client_id=' . config('constant.adobe_eSignature.CLIENT_ID') . '&scope=user_read+user_write+user_login+agreement_read+agreement_write+library_read+workflow_read+workflow_write+webhook_read+webhook_write+webhook_retention';
        $api_access_point = config('constant.adobe_eSignature.api_access_point');
//
        $client = new \GuzzleHttp\Client(['base_uri' => $api_access_point]);
        $refresh_token = config('constant.adobe_eSignature.REFRESH_TOKEN');

//
        $refresh_token_data = $client->request('POST', '/oauth/refresh', [
                    'form_params' => [
                        'grant_type' => 'refresh_token',
                        'client_id' => config('constant.adobe_eSignature.CLIENT_ID'),
                        'client_secret' => config('constant.adobe_eSignature.CLIENT_SECRET'),
                        'refresh_token' => $refresh_token
                    ]
                ])->getBody()->getContents();
        $refresh_token_data = json_decode($refresh_token_data, true);
        $access_token = $refresh_token_data['access_token'];

        $upload_document_url = 'api/rest/v6/transientDocuments';
        $agreement_send_url = 'api/rest/v6/agreements';


        $opts = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );
        $serviceDocuments = ServiceDocument::where('document_name', 'ElectronicDeliveryAgreement.docx')->first()->toArray();
        $document_name = $serviceDocuments['document_name'];
        $file_path = config('constant.adobe_eSignature.document_upload_url') . $document_name;

        $output = $client->request('POST', $upload_document_url, [
                    'multipart' => [
                        [
                            'name' => "File",
                            'contents' => fopen($file_path, 'rb', false, stream_context_create($opts)),
                            'headers' => [
                                'Content-Type' => 'multipart/form-data',
                                'Content-Transfer-Encoding' => "binary",
                            ]
                        ],
                    ],
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                    ]
                        ]
                )->getBody()->getContents();

        $transientDocumentResponse = json_decode($output, true);
        $transientDocumentId = $transientDocumentResponse['transientDocumentId'];
        $agreement_data = [
            'fileInfos' =>
            [
                [
                    'transientDocumentId' => $transientDocumentId,
                ]
            ],
            'name' => 'Agreement',
            'participantSetsInfo' =>
            [
                [
                    'memberInfos' =>
                    [
                        [
                            'email' => $email
                        ]
                    ],
                    'order' => 1,
                    'role' => 'SIGNER',
                ]
            ],
            'signatureType' => 'ESIGN',
            'message' => "Please review and sign this document.",
            'state' => 'AUTHORING',
        ];
        $json_array = json_encode($agreement_data);
        $output_agreement = $client->request('POST', $agreement_send_url, [
                    \GuzzleHttp\RequestOptions::JSON => $agreement_data,
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                        'Content-Type' => 'application/json',
                    ]
                        ]
                )->getBody()->getContents();
        $output_agreement_decoded = json_decode($output_agreement, true);
        if (isset($output_agreement_decoded['id']) && !empty($output_agreement_decoded['id'])) {

            $output_agreement_decoded_id = $output_agreement_decoded['id'];
            sleep(5);
            $agreement_get_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';

            $agreement_get_url = $api_access_point . $agreement_get_url;
            $agreeement_data_adobe = $client->request('GET', $agreement_get_url, [
                'headers' => [
                    'Authorization' => "Bearer " . $access_token,
                ]
            ]);
            $etag = $agreeement_data_adobe->getHeader('ETag')[0];

            $agreement_member_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/members';

            $agreement_member_url = $api_access_point . $agreement_member_url;
            $agreeement_member_data = $client->request('GET', $agreement_member_url, [
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                        ]
                    ])->getBody()->getContents();
            $agreeement_member_data_decoded = json_decode($agreeement_member_data, true);
            foreach ($agreeement_member_data_decoded['participantSets'] as $single) {
                if ($single['role'] == 'SIGNER')
                    $participant_id = $single['id'];
                if ($single['role'] == 'APPROVER')
                    $approver_id = $single['id'];
            }

            $agreement_fields_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';
            $fields_array = [];

            $document_data = json_decode($serviceDocuments['data'], true);
            foreach ($document_data as $single) {
                if (isset($single['is_approver']) && !empty($single['is_approver'])) {
                    $single['assignee'] = $approver_id;
                    unset($single['is_approver']);
                } else {
                    $single['assignee'] = $participant_id;
                    if (isset($single['name']) && !empty($single['name']) && $single['name'] == 'client_email') {
                        $single['defaultValue'] = $email;
                    }
                    if (isset($single['displayFormatType']) && !empty($single['displayFormatType']) && $single['displayFormatType'] == 'DATE') {
                        $single['defaultValue'] = date('d/m/Y');
                    }
                }
                $fields_array[] = $single;
            }

            $agreement_fields_data = [
                'fields' =>
                $fields_array
            ];
            $json_array = json_encode($agreement_fields_data);

            $agreement_fields_output = $client->request('PUT', $agreement_fields_url, [
                        \GuzzleHttp\RequestOptions::JSON => $agreement_fields_data,
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                            'Content-Type' => 'application/json',
                            'If-Match' => $etag
                        ]
                            ]
                    )->getBody()->getContents();
//                    print_R(json_decode($agreement_fields_output));
//                    die;
        }
        return true;
    }

    public function confirm_mail(Request $request, $confirmation_code) {
        $user = Auth::user();
        Mail::send('frontend.emails.verifyEmail', ['user' => $user], function($message)use ($user) {
            $message->to($user->email)->subject('Verify Your Email');
        });
        return redirect()->back()->withFlashSuccess('Verification mail has been sent, please verify.');
    }

}

