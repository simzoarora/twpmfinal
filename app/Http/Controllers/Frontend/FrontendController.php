<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Profile;
use App\Models\FinancialProfile;
use App\Models\Access\User\User;
use Cartalyst\Stripe\Stripe;
use Auth;
use Response;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceQuestionUser;
use Session;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class FrontendController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('frontend.home');
    }

    public function about() {
        return view('frontend.about');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function macros() {
        return view('frontend.macros');
    }

    public function personalProfileSave(Request $request) {
        $data = $request->all();
        $profile_data['user_id'] = Auth::id();
        $profile_data['first_address'] = $data['first_address'];
        $profile_data['last_address'] = $data['last_address'];
        $profile_data['city'] = $data['city'];
        $profile_data['state_id'] = $data['state_id'];
        $profile_data['zip'] = $data['zip'];
        $profile_data['phone_number'] = $data['phone_number'];
        $profile_data['sex'] = $data['sex'];
        $profile_data['dob'] = $data['dob'];
        //$profile_data['phone_type'] = $data['phone_type'];
        if (Profile::where('user_id', Auth::id())->update($profile_data)) {
            return Response::json(['message' => 'Your personal profile has been updated.'], 200);
        } else {
            return Response::json(['message' => 'There has been a error. Please try again.'], 500);
        }
        die;
    }

    public function financeProfileSave(Request $request) {
        $data = $request->all();
        $profile_data['user_id'] = Auth::id();
        $profile_data['tax_filing_status'] = $data['tax_filing_status'];
        $profile_data['annual_income'] = $data['annual_income'];
        $profile_data['spouse_annual_income'] = $data['spouse_annual_income'];
        $profile_data['spouse_has_twpm_account'] = isset($data['spouse_has_twpm_account']) ? $data['spouse_has_twpm_account'] : 0;
        $profile_data['spouse_name'] = $data['spouse_name']; 
        if (FinancialProfile::where('user_id', Auth::id())->update($profile_data)) {
            Session::set('tax_filing_status', $data['tax_filing_status']);
            return Response::json(['message' => 'Your financial profile has been updated.'], 200);
        } else {
            return Response::json(['message' => 'There has been a error. Please try again.'], 500);
        }
        die;
    }

    public function employmentProfileSave(Request $request) {
        $data = $request->all();
        $profile_data['user_id'] = Auth::id();
        $profile_data['employment_status'] = $data['employment_status'];

        if (FinancialProfile::where('user_id', Auth::id())->update($profile_data)) {
            return Response::json(['message' => 'Your employment status has been updated.'], 200);
        } else {
            return Response::json(['message' => 'There has been a error. Please try again.'], 500);
        }
        die;
    }

    public function userNameSave(Request $request) {
        $data = $request->all();
        $profile_data['first_name'] = $data['first_name'];
        $profile_data['middle_name'] = $data['middle_name'];
        $profile_data['last_name'] = $data['last_name'];

        if (Profile::where('user_id', Auth::id())->update($profile_data)) {
            return Response::json(['message' => 'Your personal profile has been updated.'], 200);
        } else {
            return Response::json(['message' => 'There has been a error. Please try again.'], 500);
        }
        die;
    }

    public function clientDocuments() {
        $user = User::where('id', Auth::id())->first();
        if (empty($user->confirmed)) {
            $msg = 'Kindly Verify Your Email, <a href="' . route('frontend.client.confirm_mail', $user->confirmation_code) . '"> Click to resend.  </a> ';
            Session::flash('confirmation_message', $msg);
            Session::flash('alert-class', 'alert-danger container-fluid flash-message');
        }
        $working_dir = '/';
        $working_dir .= config('lfm.files_dir') . $this->getUserSlug();
        $extension_not_found = !extension_loaded('gd') && !extension_loaded('imagick');
        $file_type = 'Files';

        $serviceData = User::find(Auth::id())->services()
                ->where('service_user.status', 1)
                ->orderBy('service_user.created_at', 'DESC')
                ->get(['services.id', 'services.title', 'services.image', 'services.title_bg_color']);
        return view('frontend.client.documents', compact(['serviceData', 'working_dir', 'extension_not_found', 'file_type']));
    }

    public function getUserSlug() {
        return empty(auth()->user()) ? '' : \Auth::user()->id;
    }

    public function clientDocumentFiles($client, $serviceId = null) {
        $user = User::find(Auth::id());
        if (!$serviceId || !(Service::where('id', $serviceId)->exists()) || !($user->services()->where('service_id', $serviceId)->exists())) {
            return redirect()->route('frontend.client.documents', config('constant.subdomain'));
        }
        $userFiles = $user->serviceQuestionWithFile($serviceId)->get();
        return view('frontend.client.documentFiles')->with(['userFiles' => $userFiles]);
    }

    /**
     * create a new customer and make a charge.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function stripePayment(Request $request) {
        $data = ['card_no' => $request->get('card_number'),
            'exp_month' => $request->get('expires_month'),
            'exp_year' => $request->get('expires_year'),
            'cvv_no' => $request->get('cvv'),
            'amount' => '100',
            'email' => 'baneet@luminoguruz.com'];

        $stripe = new Stripe(config('services.stripe.secret'));
        try {
            //check if customer already created for given email id then retrieve customer id from db else create customer
            $customer = $stripe->customers()->create([
                'email' => $data['email'],
            ]);

            if (!isset($customer['id'])) {
                //The Stripe customer creation failed
                return Response::json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            }
            //save customer id logic here
            //adding new card to stripe dashboard
            $card = $stripe->cards()->create($customer['id'], [
                'number' => $data['card_no'],
                'exp_month' => $data['exp_month'],
                'exp_year' => $data['exp_year'],
                'cvc' => $data['cvv_no'],
            ]);

            if (!isset($card['id'])) {
                //failed to add the card
                return Response::json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            }

            $charge = $stripe->charges()->create([
                'customer' => $customer['id'],
                'currency' => 'USD',
                'amount' => $data['amount'],
            ]);
            if ($charge['status'] == 'succeeded') {
                //Payment successed
                return Response::json(['message' => 'Your payment for service has been made successfully.', 'link' => route('frontend.client.signupStepFive', config('constant.subdomain'))], 200);
            } else {
                //Payment failed
                return Response::json(['message' => 'There has been a error while doing your payment. Please try again.'], 500);
            }
        } catch (Exception $e) {
            dd($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\CardErrorException $e) {
            dd($e->getMessage());
        } catch (\Cartalyst\Stripe\Exception\MissingParameterException $e) {
            dd($e->getMessage());
        }
    }

    public function updateClientProfileData(Request $request) {
        $data = $request->all();

        if (isset($data['userdata']) && !empty($data['userdata'])) {
            foreach ($data['userdata'] as $profileDataArray) {

                if (isset($profileDataArray['id']) && !empty($profileDataArray['id'])) {
                    $status = ServiceQuestionUser::where('user_id', Auth::id())
                            ->where('service_question_id', $profileDataArray['service_question_id'])
                            ->where('id', $profileDataArray['id'])
                            ->update(['value' => $profileDataArray['value']]);
                } else {
//            For saving a new profile
                    $newProfile = new ServiceQuestionUser();
                    $newProfile['user_id'] = Auth::id();
                    $newProfile['service_question_id'] = $profileDataArray['service_question_id'];
                    $newProfile['value'] = $profileDataArray['value'];
                    $status = $newProfile->save();
                }

                if (!$status) {
                    return Response::json(['message' => 'There has been a error. Please try again.'], 500);
                }
            }

            return Response::json(['message' => 'Your details updated successfully.'], 200);
        }
        return Response::json(['message' => 'There has been a error. Please try again.'], 500);
    }

}
