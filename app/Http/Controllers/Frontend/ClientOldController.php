<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests\Frontend\StoreClientProfileRequest;
use App\Http\Requests\Frontend\StoreClientDetailRequest;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Profile;
use App\Models\FinancialProfile;
use App\Models\OneTimePassword;
use App\Models\Backend\Service;
use App\Models\Backend\Article;
use App\Models\Backend\ResourceTopic;
use App\Models\Backend\ResourceCategory;
use App\Models\Backend\ProfileTag;
use Response;
use Twilio;
use Carbon\Carbon;
use Session;
Use Auth;
use DB;
use Log;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use File;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class ClientOldController extends Controller {
 
    /**
     * @return \Illuminate\View\View
     */
    
    public function preview(){
         return view('frontend.client.preview');
    }
    
    public function signUpOne(){
//        die('signUpOne');
         return view('frontend.client.signUpOne');
    }
    public function signUpTwo(){
         return view('frontend.client.signUpTwo');
    }
    public function signUpThree(){
         return view('frontend.client.signUpThree');
    }
    public function signUpFour(){
         return view('frontend.client.signUpFour');
    } 
    public function signUpFive(){
         return view('frontend.client.signUpFive'); 
    }
    public function signUpSix(){
         return view('frontend.client.signUpSix'); 
    }
    public function signUpEight(){
         return view('frontend.client.signUpEight'); 
    }
    public function signUpNine(){
         return view('frontend.client.signUpNine');
    }
    public function signUpTen(){
         return view('frontend.client.signUpTen');
    }
    public function signUpEleven(){
         return view('frontend.client.signUpEleven');
    }
    public function signUpTwelve(){
         return view('frontend.client.signUpTwelve');
    }
    public function calculatingPlan(){
         return view('frontend.client.calculatingPlan');
    }
    public function loginScreen(){
         return view('frontend.client.loginScreen');
    }
    public function finishAccountInfo(){
         return view('frontend.client.finishAccountInfo'); 
    } 
    
    public function signupStepOne($client, $serviceId = null) {
        if (!$serviceId || !(Service::where('id', $serviceId)->exists())) {
            return redirect()->route('frontend.services');
        }
        return view('frontend.client.stepOne')->with(['serviceId' => $serviceId]);
    }

    public function signupStepTwo() {
        $userId = $this->_checkUserId();
        if (!$userId) {
            return redirect()->route('frontend.index');
        }

        $signupstatus = $this->_checkSignupProcessStatus();
        if ($signupstatus) {
            return redirect()->route(config('constant.profile_completion_steps_routes')[$signupstatus->profile_complete_step + 1], config('constant.subdomain'));
        }

        return view('frontend.client.stepTwo')->with(['userId' => $userId]);
    }

    public function signupStepThree() {
        $userId = $this->_checkUserId();
        if (!$userId) {
            return redirect()->route('frontend.index');
        }

        $signupstatus = $this->_checkSignupProcessStatus();
        if ($signupstatus && $signupstatus->profile_complete_step != config('constant.profile_completion_steps_routes_inverse.signupStepTwo')) {
            return redirect()->route(config('constant.profile_completion_steps_routes')[$signupstatus->profile_complete_step + 1], config('constant.subdomain'));
        }

        return view('frontend.client.stepThree')->with(['mobileNumber' => Profile::where('user_id', $userId)->first(['mobile_number']), 'userId' => $userId]);
    }

    public function submitVerify(Request $request) {
        Session::set('userId', $request->user_id);
        if (!$request->user_id) {
            return redirect()->route('frontend.index');
        }
        if (!Profile::where('user_id', $request->user_id)->update(['profile_complete_step' => 2])) {
            return redirect()->back()->withFlashDanger('Oops!! Something went wrong. Please try again.');
        }
        return redirect()->route('frontend.client.signupStepFour', config('constant.subdomain'));
    }

    public function signupStepFour() {
        $userId = $this->_checkUserId();
        if (!$userId) {
            return redirect()->route('frontend.index');
        }
        $signupstatus = $this->_checkSignupProcessStatus();
        if ($signupstatus && $signupstatus->profile_complete_step != config('constant.profile_completion_steps_routes_inverse.signupStepThree')) {
            return redirect()->route(config('constant.profile_completion_steps_routes')[$signupstatus->profile_complete_step + 1], config('constant.subdomain'));
        }
        return view('frontend.client.stepFour')->with(['userId' => $userId]);
    }

    public function signupStepFive($client, $serviceId = null) {
        $userId = $this->_checkUserId();
        if (!$userId) {
            return redirect()->route('frontend.index');
        }
        $user = User::find($userId);
        if (!$user) {
            return redirect()->route('frontend.index');
        }

        if (!empty($serviceId)) {
            if (!(Service::where('id', $serviceId)->exists())) {
                return redirect()->route('frontend.services');
            }
            Session::set('selectedServiceId', $serviceId);
        }

        $signupstatus = $this->_checkSignupProcessStatus();
        if ($signupstatus && $signupstatus->profile_complete_step != config('constant.profile_completion_steps_routes_inverse.signupStepFour')) {
            return redirect()->route(config('constant.profile_completion_steps_routes')[$signupstatus->profile_complete_step + 1], config('constant.subdomain'));
        }
        if (Session::has('selectedServiceId')) {
            //saving client service
            $user->services()->syncWithoutDetaching([Session::get('selectedServiceId') => ['status' => 1]]);
        }
        $questionData = $this->_getServiceQuestions($user, Session::get('selectedServiceId'));
        if ($questionData->serviceQuestion->isEmpty()) {
            Session::forget('selectedServiceId');
        }
        return view('frontend.client.stepFive')->with(['userId' => $userId, 'questionData' => $questionData]);
    }

    public function signupStepSix() {
        $signupstatus = $this->_checkSignupProcessStatus();
        if ($signupstatus && $signupstatus->profile_complete_step != config('constant.profile_completion_steps_routes_inverse.signupStepFive')) {
            return redirect()->route(config('constant.profile_completion_steps_routes')[$signupstatus->profile_complete_step + 1], config('constant.subdomain'));
        }
        return view('frontend.client.stepSix');
    }

    private function _checkUserId() {
        if (Auth::check()) {
            return Auth::id();
        } else if (Session::has('userId')) {
            return Session::get('userId');
        }
        return FALSE;
    }

    /**
     * check if user is logged in and have completed his profile
     * @return boolean 
     */
    private function _checkSignupProcessStatus() {
        if (Auth::check()) {
            $profile = Profile::where('user_id', Auth::id())->first(['profile_complete_step']);
            if ($profile && ($profile->profile_complete_step > 0 && $profile->profile_complete_step < 4 )) {
                return $profile;
            }
        }
        return false;
    }

    /**
     * save user profile details
     * @param StoreClientProfileRequest $request
     * @return type redirect
     */
    public function storeSignupStepTwo(StoreClientProfileRequest $request) {
        $data = $request->all();
        $userId = $this->_checkUserId();

        if (!($userId == $data['user_id'])) {
            return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.invalidUser'));
        }

        $profile = new Profile();
        $data['profile_complete_step'] = 1;
        $profile->fill($data);

        if ($profile->save()) {

            return redirect()->route('frontend.client.signupStepThree', config('constant.subdomain'))->with('userId', $profile->user_id);
        }
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    /**
     * save user profile details which were 
     * @param StoreClientDetailRequest $request
     * @return type
     */
    public function storeSignupStepFour(StoreClientDetailRequest $request) {
        $data = $request->all();
        $userId = $data['user_id'];

        if (Auth::check()) {
            $userId = Auth::id();
        }

        if (Session::has('selectedServiceId')) {
            $selectedService = Service::where('id', Session::get('selectedServiceId'))->first(['id']);
            if (empty($selectedService)) {
                $selectedService = User::find($userId)->services()->where('status', 1)->first();
            }
        } else {
            $selectedService = User::find($userId)->services()->where('status', 1)->first();
        }
        DB::beginTransaction();
//Enable this when sharfile started working
//        if (Profile::where('user_id', $data['user_id'])->update(['sex' => $data['sex'], 'dob' => Carbon::parse($data['dob'])->toDateString(), 'ssn' => $data['ssn'], 'profile_complete_step' => 3]) && $this->_createClientUser($data['user_id'], $selectedService->id)) {
        if (Profile::where('user_id', $data['user_id'])->update(['sex' => $data['sex'], 'dob' => Carbon::parse($data['dob'])->toDateString(), 'ssn' => $data['ssn'], 'profile_complete_step' => 3])) {
            DB::commit();
            return redirect()->route('frontend.client.signupStepFive', config('constant.subdomain'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.frontend.client.saveFail'));
    }

    /**
     * Store user answers when signing up for first time
     * @param Request $request
     * @return type json
     */
    public function storeSignupStepFive(Request $request) {
        $data = $request->all();
        $user = User::userData($data['user_id']);

        if ($user) {
            if (Auth::check() && Auth::id() != $user->id) {
                return Response::json(['message' => 'Invalid User'], 500);
            }

            if (Session::has('selectedServiceId')) {
                $selectedService = Service::where('id', Session::get('selectedServiceId'))->first(['id']);
                if (empty($selectedService)) {
                    return Response::json(['message' => 'Invalid Service'], 500);
                }
            } else {
                $selectedService = $user->services()->where('status', 1)->first();
            }
            $error = [];
            DB::beginTransaction();
            foreach ($data['answer'] as $answerArray) {
                if (isset($answerArray['value']) && !empty($answerArray['value'])) {
                    foreach ($answerArray['value'] as $value) {
                        if (!empty($value)) {
                            if (isset($answerArray['is_file'])) {
                                if ($value->getClientOriginalExtension() == 'pdf') {
                                    $value = $this->_uploadFiles($value, config('constant.client_file_upload_path') . $data['user_id'], $user, $selectedService->id);
                                    $user->serviceQuestions()->syncWithoutDetaching([$answerArray['question_id'] => ['value' => $value['fileName'], 'sharefile_item_id' => $value['shareFileId']]]);
                                } else {
                                    $error[$answerArray['question_id']] = 'Please upload pdf files only.';
                                }
                            } else {
                                $user->serviceQuestions()->syncWithoutDetaching([$answerArray['question_id'] => ['value' => $value]]);
                            }
                        } else {
                            $error[$answerArray['question_id']] = 'Required';
                        }
                    }
                } else {
                    $error[$answerArray['question_id']] = 'Required';
                }
            }
            if ($error) {
                DB::rollBack();
                return Response::json(['message' => $error], 500);
            }
            DB::commit();
            $questionData = $this->_getServiceQuestions($user, Session::get('selectedServiceId'));
            if ($questionData->serviceQuestion->isEmpty()) {
                Session::forget('selectedServiceId');
            }
            return Response::json(['message' => 'Success', 'userId' => $data['user_id'], 'questionData' => $questionData], 200);
        }
        return Response::json(['message' => 'Invalid User'], 500);
    }

    public function storeSignupStepSix(Request $request) {
        $data = $request->all();
        $user = User::with(['profile' => function($q) {
                        $q->select(['id', 'user_id', 'first_name', 'last_name']);
                    }])->where('id', $data['user_id'])->first();

                if ($user) {
                    if (Profile::where('user_id', $user->id)->update([ 'profile_complete_step' => 4])) {
                        //login user to dashboard 
                        Auth::login($user);
                        Session::set('loggedInUserName', $user->profile->first_name . ' ' . $user->profile->last_name);
                        return Response::json(['message' => 'Success'], 200);
                    }
                    return Response::json(['message' => 'Failed to update user profile.'], 500);
                }
                return Response::json(['message' => 'Invalid User.'], 500);
            }

            /**
             * Upload files and retutn file name
             * @param array $file : File object to be uploaded
             * @param string $directory : directory path where files are stored
             * @param array $userData : User details
             * @param int $serviceId : service id for which file is to be uploaded
             * @return boolean|string : false on fail/saved file name on success
             */
            private function _uploadFiles($file, $directory, $userData, $serviceId) {
                $client = $this->_getShareFileAuthorization();

                if ($client) {

                    $clientServiceFolder = $this->_getClientServiceFolder($userData, $serviceId, $client);
                    $token = $this->_getClientAuthorization($userData, true);

                    If ($clientServiceFolder && $token) {

                        $fileName = date('d-m-Y-h-i-s-') . preg_replace('/\s+/', '-', trim($file->getClientOriginalName()));
                        if (!file_exists($directory)) {
                            mkdir($directory, 0777, true);
                        }

                        if ($file->move($directory, $fileName)) {
                            if ($this->_uploadFileCurl($token, $clientServiceFolder, $directory . '/' . $fileName)) {

                                $baseFolder = '/Personal Folders/' . env('SHARE_FILE_CLIENT_FILES_FOLDER_NAME');
                                $itemId = $this->_getItemByPath($client, $baseFolder . '/' . $userData->id . '/' . $serviceId . '/' . $fileName);


                                if ($itemId['status'] == '200' && File::Delete($directory . '/' . $fileName)) {
                                    return ['fileName' => $fileName, 'shareFileId' => $itemId['response']['Id']];
                                }
                            }
                        }
                    }
                }
                return False;
            }

            /**
             * send otp to user and save otp value in database
             * @param Request $request
             * @return json
             */
            public function sendOtp(Request $request) {
                $data = $request->all();

                if (!isset($data['mobile_number']) || empty($data['mobile_number']) || strlen($data['mobile_number']) != 10) {
                    return Response::json(['message' => trans('alerts.frontend.client.phoneRequired')], 500);
                }

                if (!isset($data['user_id']) || empty($data['user_id'])) {
                    throw new GeneralException('Forbidden');
                }
                $userId = $data['user_id'];

                if (Profile::where('mobile_number', $data['mobile_number'])->where('user_id', '!=', $userId)->first()) {
                    return Response::json(['message' => trans('alerts.frontend.client.uniquePhone')], 500);
                }

                if (OneTimePassword::where('user_id', $userId)->whereDate('created_at', '=', Carbon::today()->toDateString())->count() > 2) {

                    return Response::json(['message' => trans('alerts.frontend.client.otpLimit')], 500);
                } else {

                    if ($this->_generateOtp($userId, $data['mobile_number'])) {
                        return Response::json(['message' => 'Otp sent successfully.'], 200);
                    } else {
                        return Response::json(['message' => trans('alerts.frontend.client.otpFail')], 500);
                    }
                }
            }

            /**
             * function to generate otp for phone verification
             * @param type $user
             * @param type $data
             * @param type $otpMessage
             * @return boolean
             */
            private function _generateOtp($userId, $phoneNumber) {
                $otp = mt_rand(111111, 999999);

                $one_time_password = new OneTimePassword;
                $one_time_password->user_id = $userId;
                $one_time_password->phone_number = $phoneNumber;
                $one_time_password->otp = $otp;

                $message = trans('alerts.frontend.client.welcomeMsg') . $otp;

                //expiring all previous OTP's active for the user 
                OneTimePassword::where('user_id', $userId)->update(['active' => 0]);
                //sending message and saving otp and saving mobile number in profile table
                if ((Twilio::message(env('SMS_COUNTRY_PHONE_CODE') . $phoneNumber, $message)) && $one_time_password->save() && Profile::where('user_id', $userId)->update(['mobile_number' => $phoneNumber])) {
                    return true;
                }
                return false;
            }

            /**
             * Verifies OTP enterd by user
             * @param Request $request
             * @return json
             */
            public function verifyOtp(Request $request) {
                $data = $request->all();

                if (!isset($data['user_id']) || empty($data['user_id']) || !isset($data['mobile_number']) || empty($data['mobile_number']) || strlen($data['mobile_number']) != 10) {
                    throw new GeneralException('Forbidden');
                }

                $userId = $data['user_id'];
                $profileUpdateArray = [
                    'mobile_verified' => 1,
                ];
                if (isset($data['profile_process']) && $data['profile_process'] == 'true') {
                    $profileUpdateArray += [
                        'profile_complete_step' => 2,
                    ];
                }

                if (OneTimePassword::where('user_id', $userId)->where('phone_number', $data['mobile_number'])->where('otp', $data['otp'])->where('active', 1)->first()) {
                    if (Profile::where('user_id', $userId)->update($profileUpdateArray) && OneTimePassword::where('user_id', $userId)->update(['active' => 0])) {
                        return Response::json(['message' => trans('alerts.frontend.client.phoneVerifySuccess')], 200);
                    }
                    return Response::json(['message' => trans('alerts.frontend.client.phoneVerifyError')], 500);
                }
                return Response::json(['message' => trans('alerts.frontend.client.invalidOtp')], 500);
            }

            public function clientProfile() {
                return view('frontend.client.profile')->with(['userData' => User::userData(Auth::id()),
                            'financialData' => FinancialProfile::where('user_id', Auth::id())->first(),
                            'profileData' => ProfileTag::whereHas('serviceQuestionWithAnswers')->with(['serviceQuestionWithAnswers'])->get()]);
            }

            public function clientResourcesCategories() {
                return view('frontend.client.resources_categories')->with(['resourceCategory' => ResourceCategory::orderBy('order')->get()]);
            }

            public function clientResources($client, $id) {
//                $topics = ResourceTopic::where('is_client', 1)->where('resource_category_id', $id)->orderBy('id', 'desc')->get();

                $topics = ResourceTopic::where('resource_category_id', $id)->orderBy('id', 'desc')->get();
                return view('frontend.client.resources', compact('topics'));
            }

            public function clientResourceDetails($client, $id) {
                $topic = ResourceTopic::with('resourceTopicFiles')->find($id);
                $categories = ResourceCategory::get()->pluck('title', 'id');
                return view('frontend.client.resources_details', compact('topic', 'categories'));
            }

//            public function clientDashboard() {
//                $articles = Article::where('status', config('constant.backend.blogs.active'))->orderBy('publish_at', 'desc')->take(2)->get(['id', 'slug', 'title', 'excerpt', 'publish_at']);
//                if (Session::has('selectedServiceId')) {
//                    //redirect to service question for that service id
//                    return redirect()->route(config('constant.profile_completion_steps_routes')[4], config('constant.subdomain'));
//                }
//                $serviceData = User::find(Auth::id())->services()
//                        ->where('service_user.status', 1)
//                        ->orderBy('service_user.created_at', 'DESC')
//                        ->get(['services.id', 'services.title', 'services.image', 'services.title_bg_color']);
//
//                return view('frontend.client.dashboard')->with(['commentaryData' => $articles, 'serviceData' => $serviceData]);
//            }

            public function clientMobileVerify() {
                return view('frontend.client.clientMobileVerification')->with(['mobileNumber' => Profile::where('user_id', Auth::id())->first(['mobile_number', 'mobile_verified']), 'userId' => Auth::id()]);
            }

            public function changeTwoFactorAuth(Request $request) {
                $data = $request->all();
                if (isset($data['change'])) {
                    $userId = Auth::id();
                    if (Profile::where('user_id', $userId)->where('mobile_verified', 1)->exists()) {
                        if (User::where('id', $userId)->update(['two_factor_auth' => $data['change']])) {
                            return Response::json(['message' => 'Two-factor authentication status Changed.'], 200);
                        } else {
                            return Response::json(['message' => 'Failed to enable two-factor authentication.', 'status' => '1'], 500);
                        }
                    }
                }
                return Response::json(['message' => 'Please verify your mobile number first to enable two-factor authentication.', 'status' => '2'], 200);
            }

            public function clientServices() {
                return view('frontend.client.services')->with(['serviceData' => User::find(Auth::id())->services()
                                    ->where('service_user.status', 1)
                                    ->orderBy('service_user.created_at', 'DESC')
                                    ->get(['services.id', 'services.title', 'services.image', 'services.title_bg_color'])]);
            }

            public function clientDocuments() {
                return view('frontend.client.documents');
            }

            public function testLogin(Request $request) {
                dump('yo');
                dd($request);
            }

//************ Share File API related functions begin here**************//

            /**
             * Common function used for making all guzzle request
             * @param array $client : Authorized Guzzle client object
             * @param string $method : Request Method type(GET/POST/PUT/PATCH)
             * @param string $url : Url where request will hit
             * @param string $paramType : Param Type(Form/query)
             * @param array $params : Parameters to be submitted with request
             * @return array : Response recieved
             */
            private function _commonGuzzleRequest($client, $method, $url, $paramType = null, $params = null) {
                try {
                    $response = $client->request($method, $url, [$paramType => $params]);
                    return ['response' => json_decode($response->getBody()->getContents(), true), 'status' => $response->getStatusCode()];
                } catch (ClientException $e) {
                    return ['response' => json_decode($e->getResponse()->getBody()->getContents(), true), 'status' => $e->getCode()];
                }
            }

            /**
             * Common function to create folders on ShareFile API
             * @param array $client : Authorized Guzzle client object
             * @param string $folderName : Name of the folder to be created
             * @param string $folderPath : Path where folder will be created
             * @return boolean|array : false on fail/array respons eon success
             */
            private function _createFolder($client, $folderName, $folderPath) {
                $createFolderArray = [
                    'overwrite' => 'false',
                    'passthrough' => 'false',
                    'Name' => $folderName,
                    'Description' => "Folder to keep All files."];

                $createFolder = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_API_LINK') . 'Items(' . $folderPath . ')/Folder', 'form_params', $createFolderArray);
                if ($createFolder['status'] == '200') {
                    return $createFolder;
                }

                Log::error('CLIENT FOLDER CREATION ERROR : ' . $createFolder['response']['message']['value'] . ' for Folder Name: ' . $folderName . ' on sharefile client creation');
                return FALSE;
            }

            /**
             * Get Empolyee Authorization for ShareFile API
             * @return boolean|Client : False on fail/ Authorized Client object on success
             */
            private function _getShareFileAuthorization() {
                $client = new Client(['headers' => ['Content-Type' => 'application/x-www-form-urlencoded']]);

                $params = [
                    'grant_type' => 'password',
                    'client_id' => env('SHARE_FILE_CLIENT_ID'),
                    'client_secret' => env('SHARE_FILE_CLIENT_SECRET'),
                    'username' => env('SHARE_FILE_CLIENT_USERNAME'),
                    'password' => env('SHARE_FILE_CLIENT_PASSWORD')
                ];

                $authentication = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_SUBDOMAIN') . 'oauth/token', 'form_params', $params);
                if ($authentication['status'] == '200') {

                    return new Client(['headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'Bearer ' . $authentication['response']['access_token'],
                    ]]);
                }
                Log::error('MASTER USER AUTHENTICATION ERROR : ' . $authentication['response']['error_description'] . ' on sharefile client creation');
                return FALSE;
            }

            /**
             * Get Item by path from ShareFile API
             * @param array $client : Authorized Guzzle client object
             * @param string $itemPath : Item which is to be found
             * @return array : Response recieved from ShareFile API
             */
            private function _getItemByPath($client, $itemPath) {
                $folderPathArray = [
                    'path' => $itemPath,
                ];
                return $this->_commonGuzzleRequest($client, 'GET', env('SHARE_FILE_API_LINK') . 'Items/ByPath', 'query', $folderPathArray);
            }

            /**
             * Create ShareFile Client user
             * @param int $userId : Id of the user which has to be created on shareFile API platform
             * @param int $serviceId : Service id for whcih a folder is to be created and shared with User
             * @return boolean : true/false
             */
            private function _createClientUser($userId, $serviceId) {
                $client = $this->_getShareFileAuthorization();

                if ($client) {

                    $baseFolder = '/Personal Folders/' . env('SHARE_FILE_CLIENT_FILES_FOLDER_NAME');

                    $userData = User::userData($userId);
                    $masterFolderPath = $this->_getShareFileMasterFolderPath($client, $baseFolder);

                    if (!$masterFolderPath) {
                        return FALSE;
                    }

                    $clientFolder = $this->_getItemByPath($client, $baseFolder . '/' . $userData->id);
                    if ($clientFolder['status'] != '200') {

                        $clientFolder = $this->_createFolder($client, $userData->id, $masterFolderPath);
                        if (!$clientFolder) {
                            return FALSE;
                        }
                    }

                    if (isset($userData->sahrefile_api_user_id) && !empty($userData->sahrefile_api_user_id)) {
                        $clientUser = $userData->sahrefile_api_user_id;
                    } else {
                        $createClientUser = $this->_createShareFileClientUser($client, $userData);
                        $clientUser = $createClientUser['response']['Id'];
                    }

                    if ($clientUser && (User::where('id', $userId)->update(['sahrefile_api_user_id' => $clientUser]))) {

                        $shareWithClientUser = $this->_shareItemWithClient($client, $clientUser, $clientFolder['response']['Id']);

                        if (!$shareWithClientUser) {
                            return FALSE;
                        }

                        $ClientServiceFolder = $this->_getItemByPath($client, $baseFolder . '/' . $userData->id . '/' . $serviceId);
                        if ($ClientServiceFolder['status'] != '200') {
                            $ClientServiceFolder = $this->_createFolder($client, $serviceId, $clientFolder['response']['Id']);
                        }

                        if (!$ClientServiceFolder) {
                            return FALSE;
                        }

                        return TRUE;
                    }
                    return FALSE;
                }
                return FALSE;
            }

            /**
             * Create ShareFile Client user API CALL
             * @param array $client : Authorized Guzzle client object
             * @param array $userData : User Details
             * @return boolean|array : false/ Client user creation success response from API
             */
            private function _createShareFileClientUser($client, $userData) {
                $userArray = [
                    'Email' => $userData->email,
                    'FirstName' => $userData->profile->first_name,
                    'LastName' => $userData->profile->last_name,
                    'Password' => ucfirst(strtolower($userData->profile->first_name)) . '%&^' . $userData->profile->ssn,
                    'Preferences' => [
                        'CanResetPassword' => FALSE,
                        'CanViewMySettings' => FALSE,
                    ],
                ];
//                Baneet%&^753159000

                $createClientUser = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_API_LINK') . 'Users', 'form_params', $userArray);

                if ($createClientUser['status'] == '200') {

                    $client = $this->_getClientAuthorization($userData);
                    If ($client) {

                        $confirmation = $this->_confirmShareFileClient($client, $userData->id, $createClientUser['response']['Id']);

                        if (!$confirmation) {
                            return FALSE;
                        }
                    } else {
                        return FALSE;
                    }

                    return $createClientUser;
                }
                Log::error('SHARE FILE USER CREATION ERROR : ' . $createClientUser['response']['message']['value']);
                return FALSE;
            }

            /**
             * Get Client User Authorization for ShareFile API
             * @param array $userData : Clinet user detils for authorization
             * @param boolean $clientHeader 
             * @return boolean|Client : False on fail/ Authorized Client object on successful authorization
             */
            private function _getClientAuthorization($userData, $clientHeader = NULL) {
                $client = new Client(['headers' => ['Content-Type' => 'application/x-www-form-urlencoded']]);

                $params = [
                    'grant_type' => 'password',
                    'client_id' => env('SHARE_FILE_CLIENT_ID'),
                    'client_secret' => env('SHARE_FILE_CLIENT_SECRET'),
                    'username' => $userData->email,
                    'password' => ucfirst(strtolower($userData->profile->first_name)) . '%&^' . $userData->profile->ssn
                ];

                $authentication = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_SUBDOMAIN') . 'oauth/token', 'form_params', $params);

                if ($authentication['status'] == '200') {
                    if ($clientHeader) {
                        return ["Authorization: Bearer " . $authentication['response']['access_token']];
                    }

                    return new Client(['headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded',
                            'Authorization' => 'Bearer ' . $authentication['response']['access_token'],
                    ]]);
                }
                Log::error('CLIENT AUTHORIZATION FAILED: ' . $authentication['response']['error_description'] . ' for Client Name: ' . $userData->profile->first_name . ' and User ID : ' . $userData->id);
                return FALSE;
            }

            /**
             * Automatically confirm newly created client user on ShareFile API platform
             * @param array $client : Authorized Guzzle client object
             * @param int $userId : Local User Id
             * @param string $shareFileApiUserId : ShareFile User Id
             * @return boolean
             */
            private function _confirmShareFileClient($client, $userId, $shareFileApiUserId = null) {
                $userInfo = $this->_commonGuzzleRequest($client, 'GET', env('SHARE_FILE_API_LINK') . 'Users(' . $shareFileApiUserId . ')');
                if ($userInfo['status'] == 200 && $userInfo['response']['IsConfirmed'] == true) {
                    return TRUE;
                } else {
                    $params = [
                        'FirstName' => '',
                        'LastName' => '',
                        'Company' => '',
                        'Password' => '',
                        'SecurityQuestion' => '',
                        'SecurityQuestionAnswer' => '',
                        'DayLightName' => '',
                        'UTCOffset' => '',
                        'DateFormat' => '',
                        'TimeFormat' => '',
                        'EmailInterval' => '0',
                        'UserNotificationLocale' => '',
                    ];
                    $confirmation = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_API_LINK') . 'Users/Confirm', 'form_params', $params);
                    if ($confirmation['response'] == NULL) {
                        return TRUE;
                    } else {
                        Log::error('CLIENT CONFIRMATION FAILED: ' . $confirmation['response']['message']['value'] . 'for UserID: ' . $userId);
                        return False;
                    }
                }
            }

            /**
             * Share an item with a given user on ShareFile API
             * @param array $client : Authorized Guzzle client object
             * @param string $sahreFileUserId : User Id with whom An item is to be shared
             * @param string $itemId : Item Id to be shared
             * @return boolean|array : false/ share success result from API
             */
            private function _shareItemWithClient($client, $sahreFileUserId, $itemId) {
                $accessControlArray = [
                    'Principal' => [
                        'url' => env('SHARE_FILE_API_LINK') . '/Users(' . $sahreFileUserId . ')'
                    ],
                    'CanUpload' => true,
                    'CanDownload' => true,
                    'CanView' => true,
                    'CanDelete' => false,
                    'CanManagePermissions' => false
                ];

                $shareItem = $this->_commonGuzzleRequest($client, 'POST', env('SHARE_FILE_API_LINK') . 'Items(' . $itemId . ')/AccessControls', 'json', $accessControlArray);
                if ($shareItem['status'] == '200') {
                    return $shareItem;
                }
                Log::error('SHARE ITEM WITH USER FAIL : ' . $shareItem['response']['message']['value'] . ' ITEM ID: ' . $itemId . ' SHARE FILE USER ID : ' . $sahreFileUserId);
                return FALSE;
            }

            /**
             * Returns Base folder id
             * @param type $client : Authorized Guzzle client object
             * @param type $baseFolder : Base folder path
             * @return boolean|string : false/master folder id
             */
            private function _getShareFileMasterFolderPath($client, $baseFolder) {
                $clientMasterFolderPath = $this->_getItemByPath($client, $baseFolder);

                if ($clientMasterFolderPath['status'] == '200') {

                    return $clientMasterFolderPath['response']['Id'];
                } else {
                    $parent = $this->_commonGuzzleRequest($client, 'GET', env('SHARE_FILE_API_LINK') . 'Items');

                    $createMasterClientFolder = $this->_createFolder($client, env('SHARE_FILE_CLIENT_FILES_FOLDER_NAME'), $parent['response']['Id']);

                    if (!$createMasterClientFolder) {
                        return FALSE;
                    }
                    return $createMasterClientFolder['response']['Id'];
                }
            }

            /**
             * Uploads a File using the Standard upload method with a multipart/form mime encoded POST.
             * @param string $header : header acquired after authorization
             * @param string $folderId : where to upload the file
             * @param string $localPath : the full path of the file to upload, like "c:\\path\\to\\file.name"
             * @return boolean :File Upload Success/Fail
             */
            private function _uploadFileCurl($header, $folderId, $localPath) {
                $uri = env('SHARE_FILE_API_LINK') . "Items(" . $folderId . ")/Upload";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $uri);
                curl_setopt($ch, CURLOPT_TIMEOUT, 300);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

                $curlResponse = curl_exec($ch);

                $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                $uploadConfig = json_decode($curlResponse);

                if ($httpCode == 200) {
                    $post["File1"] = new \CurlFile($localPath);
                    curl_setopt($ch, CURLOPT_URL, $uploadConfig->ChunkUri);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                    curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    curl_setopt($ch, CURLOPT_HEADER, true);

                    $uploadResponse = curl_exec($ch);

                    if (curl_getinfo($ch, CURLINFO_RESPONSE_CODE) == 200) {
                        curl_close($ch);
                        return TRUE;
                    }
                    $curlResponseErrorNumber = curl_errno($ch);

                    if ($curlResponseErrorNumber) {
                        Log::error(curl_error($ch) . ' CURL ERROR NUMBER:' . $curlResponseErrorNumber . ' SHAREFILE FILE UPLOAD RESPONSE : ' . $uploadResponse);
                    } else {
                        Log::error('SHAREFILE FILE UPLOAD ERROR : ' . $uploadResponse);
                    }
                }

                $curlErrorNumber = curl_errno($ch);

                if ($curlErrorNumber) {
                    Log::error(curl_error($ch) . ' CURL ERROR NUMBER:' . $curlErrorNumber . ' SHAREFILE FILE UPLOAD RESPONSE : ' . $curlResponse);
                } else {
                    Log::error('SHAREFILE FILE UPLOAD ERROR : ' . $curlResponse);
                }

                curl_close($ch);
                return FALSE;
            }

            /**
             * Return client service folder id where files will be uploaded on shareFile
             * @param array $userData : User Details
             * @param int $serviceId : service id for which file is to be uploaded
             * @return boolean|string : false on fail/client service folder id on success
             */
            private function _getClientServiceFolder($userData, $serviceId, $client) {
                $baseFolder = '/Personal Folders/' . env('SHARE_FILE_CLIENT_FILES_FOLDER_NAME');
                $clientFolder = $this->_getItemByPath($client, $baseFolder . '/' . $userData->id);

                if ($clientFolder['status'] != '200') {
                    $masterFolderPath = $this->_getShareFileMasterFolderPath($client, $baseFolder);

                    if (!$masterFolderPath) {
                        return FALSE;
                    }

                    $clientFolder = $this->_createFolder($client, $userData->id, $masterFolderPath);
                    if (!$clientFolder) {
                        return FALSE;
                    }
                }

                if (!$this->_shareItemWithClient($client, $userData->sahrefile_api_user_id, $clientFolder['response']['Id'])) {
                    return FALSE;
                }

                $ClientServiceFolder = $this->_getItemByPath($client, $baseFolder . '/' . $userData->id . '/' . $serviceId);

                if ($ClientServiceFolder['status'] != '200') {
                    $ClientServiceFolder = $this->_createFolder($client, $serviceId, $clientFolder['response']['Id']);

                    if (!$ClientServiceFolder) {
                        return FALSE;
                    }
                }

                return $ClientServiceFolder['response']['Id'];
            }

            /**
             * Downloads a single Item. If downloading a folder the local_path name should end in .zip.
             *
             * @param string $token - json token acquired from authenticate function
             * @param string $item_id - the id of the item to download
             * @param string $filename -filename to be saved on local system"
             */
            private function _downloadItem($headers, $itemId, $filename) {
                $uri = env('SHARE_FILE_API_LINK') . "Items(" . $itemId . ")/Download";

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . $filename);
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $uri);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_VERBOSE, FALSE);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                $curlResponse = curl_exec($ch);
                echo $curlResponse;
            }

            public function downloadClientFile($client, $filename, $serviceId) {
                if ($filename && $serviceId) {
                    $headers = $this->_getClientAuthorization(User::userData(Auth::id()), true);
                    $this->_downloadItem($headers, $filename, $filename);
                }
                abort(404, 'Not Found.');
            }

            //************ Share File API related functions ends here**************//



            public function signupStepPayment() {
                return view('frontend.client.step_payment')->with(['userId' => $this->_checkUserId()]);
            }

            public function clientServiceQuestions($client, $serviceId = null) {
                $user = User::find(Auth::id());
                if (!$serviceId || !(Service::where('id', $serviceId)->exists()) || !($user->services()->where('service_id', $serviceId)->exists())) {
                    return redirect()->route('frontend.client.services', config('constant.subdomain'));
                }

                $questions = $user->serviceQuestionsAnswers($serviceId)->with(['serviceQuestionOption' => function($q) {
                                $q->select(['id', 'service_question_id', 'name']);
                            }])->get();

                        return view('frontend.client.service_questions')->with(['questions' => $questions->groupBy('id'), 'unAnsweredQuestions' => $this->_getServiceQuestions($user, $serviceId, true), 'serviceId' => $serviceId]);
                    }

                    public function updateServiceQuestionAnswer(Request $request) {
                        $data = $request->all();

                        $user = User::userData(Auth::id());

                        if ($user && $user->services()->where('status', 1)->where('service_id', $data['service_id'])->exists()) {
                            DB::beginTransaction();
                            foreach ($data['answer'] as $answerArray) {
                                if (isset($answerArray['value']) && !empty($answerArray['value'])) {

                                    if (isset($answerArray['checkbox'])) {
                                        $user->serviceQuestions()->detach([$answerArray['question_id']]);
                                    }
                                    foreach ($answerArray['value'] as $value) {

                                        if (isset($answerArray['answer_id']) && !empty($answerArray['answer_id']) && (!isset($answerArray['is_file']) && !isset($answerArray['checkbox']))) {

                                            $saveArray = [
                                                'value' => $value
                                            ];

                                            $user->serviceQuestions()->updateExistingPivot($answerArray['question_id'], $saveArray);
                                        } else {
                                            if (isset($answerArray['is_file']) && $value->getClientOriginalExtension() != 'pdf') {
                                                DB::rollBack();
                                                return redirect()->back()->withFlashDanger('Please upload pdf files only.');
                                            }

                                            $this->_createNewServiceQuestionAnswer($user, $answerArray, $data['service_id'], $value);
                                        }
                                    }
                                }
                            }
                            DB::commit();
                            return redirect()->back()->withFlashSuccess('Answers Updated Successfully.');
                        }
                        return redirect()->back()->withFlashDanger('Invalid User.');
                    }

                    private function _createNewServiceQuestionAnswer($user, $answerArray, $serviceId, $value) {
                        if (isset($answerArray['is_file'])) {

                            $value = $this->_uploadFiles($value, config('constant.client_file_upload_path') . $user->id, $user, $serviceId);
                            if (!$value) {
                                return False;
                            }
                            $saveArray = [
                                'value' => $value['fileName'],
                                'sharefile_item_id' => $value['shareFileId']
                            ];

                            if (!isset($answerArray['multiple']) && isset($answerArray['answer_id'])) {
                                return $this->deleteClientAnswerFile($answerArray['answer_id']) && $user->serviceQuestions()->updateExistingPivot($answerArray['question_id'], $saveArray);
                            }
                        } else {
                            $saveArray = [
                                'value' => $value
                            ];
                        }
                        return $user->serviceQuestions()->attach($answerArray['question_id'], $saveArray);
                    }

                    public function deleteClientAnswerFile($id = null) {
                        $user = User::userData(Auth::id());
                        $guzzleClient = $this->_getClientAuthorization($user);
                        $shareFileItemId = $user->serviceQuestionsAnswersById($id)->first();

                        if ($user && $guzzleClient && $shareFileItemId) {
                            $deleteItem = $this->_commonGuzzleRequest($guzzleClient, 'DELETE', env('SHARE_FILE_API_LINK') . 'Items(' . $shareFileItemId->pivot->sharefile_item_id . ')?singleversion=false&forceSync=false');

                            if ($deleteItem['response'] != NULL || !($user->serviceQuestions()->detach([$id]))) {
                                Log::error('SHAREFILE FILE DELETE ERROR : ' . $deleteItem['response']['message']['value']);
                                return Response::json(['message' => 'Failed to delete'], 500);
                            }
                        }
                        return Response::json(['message' => 'Invalid data'], 500);
                    }

                    /**
                     * returns all unanswered question for a service by a user.
                     * @param type $user
                     * @return array
                     */
                    private function _getServiceQuestions($user, $serviceId = null, $get = null) {
                        $serviceQuestionFields = ['id', 'service_id', 'question', 'type', 'order'];
                        $query = $user->services()->select(['services.id'])
                                ->with(['serviceQuestion' => function($q) use ($serviceQuestionFields, $user, $get) {

                                $query = $q->select($serviceQuestionFields)
                                        ->whereNotIn('id', $user->serviceQuestionsList())
                                        ->where('profile_tag_id', NULL)
                                        ->orderBy('order', 'ASC')->with(['serviceQuestionOption' => function($q) {
                                        $q->select(['id', 'service_question_id', 'name']);
                                    }]);
                                        if (empty($get)) {
                                            $query->take('5');
                                        } else {
                                            $query->get();
                                        }
                                    }]);
                                        if (empty($serviceId)) {
                                            return $query->where('status', 1)->first();
                                        }
                                        return $query->where('status', 1)->where('service_id', $serviceId)->first();
                                    }

                                }
                                