<?php

namespace App\Http\Controllers\Frontend;

use Mail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//homepage
use App\Models\Backend\Homepage;
use App\Models\Backend\LogoBox;
//about page
use App\Models\Backend\About;
use App\Models\Backend\Employee;
use App\Models\Backend\BlogCategory;
//services page
use App\Models\Backend\ServiceCategory;
use App\Models\Backend\ServiceSlideData;
use App\Models\Backend\Service;
//resources page
use App\Models\Backend\ResourceCategory;
use App\Models\Backend\Resource;
use App\Models\Backend\ResourceTopic;
//blog page
use App\Models\Backend\Article;
//footer
use App\Models\Backend\Footer;
/* Legal Disclosures */
use App\Models\Backend\LegalDisclosure;
/* Privacy & Security */
use App\Models\Backend\Privacy;
/* Terms of Use */
use App\Models\Backend\Term;
/* Contact */
use App\Models\Backend\Contact;
/* Seo */
use App\Models\Backend\Seo;
use App\Models\UserSignupQuestion;
use App\Models\UserSignupQuestionAnswer;
use App\Models\Backend\Newsletter;

//use Session;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class HomeController extends Controller {

    public function index() {
        $oldDate = date("Y-m-d", strtotime("-1 year", time()));
        $homePageContent = $this->_getHomeContent();
        $services = Service::with('serviceCategories')->orderBy('order')->take(3)->get();
        $articles = Article::with(['blogCategories'])
                        ->where('status', config('constant.backend.blogs.active'))
                        ->whereDate('publish_at', '<=', date('Y-m-d'))->whereDate('publish_at', '>', $oldDate)
                        ->orderBy('publish_at', 'desc')
                        ->take(3)->get();
        return view('frontend.home.home', compact('homePageContent', 'services', 'articles'))->with($this->_getFooterData());
    }

    public function services() {
        $services = Service::with('serviceCategories')->orderBy('order')->get();
        $slides = [];
        if (ServiceSlideData::get()->count()) {
            $slides = ServiceSlideData::first()->toArray();
        }
        $categories = ServiceCategory::get();
        return view('frontend.home.services', ['services' => $services, 'categories' => $categories, 'slides' => $slides])->with($this->_getFooterData());
    }

    public function getServiceDetails($id) {
        return Service::where('id', $id)->with('serviceFiles')->first();
    }

    public function about() {
        $aboutPageContent = About::all()->first();
        $employees = Employee::orderBy('order')->get();
        return view('frontend.home.about', compact('aboutPageContent', 'employees'))->with($this->_getFooterData());
    }

    public function resources() {
        $resource = [];
        if (ResourceCategory::get()->count()) {
            $resource = Resource ::first()->toArray();
        }
        $categories = ResourceCategory::orderBy('order')->get();
        return view('frontend.home.resources', ['categories' => $categories, 'resource' => $resource])->with($this->_getFooterData());
    }

    public function getResourceTopics($id) {
        return ResourceTopic::with('resourceTopicFiles')->where('resource_category_id', $id)->orderBy('order')->get();
    }

    public function blog() {
        $oldDate = date("Y-m-d", strtotime("-1 year", time()));
        $categories = BlogCategory::get(['title', 'id'])->toArray();
        $articles = Article::with(['blogCategories', 'articleAuthor'])->where('status', config('constant.backend.blogs.active'))->whereDate('publish_at', '<=', date('Y-m-d'))->whereDate('publish_at', '>', $oldDate)->take(8);

        $firstThreeArticles = $articles->orderBy('publish_at', 'desc')->take(3)->get();
        $fiveArticles = Article::with(['blogCategories', 'articleAuthor'])->where('status', config('constant.backend.blogs.active'))->whereDate('publish_at', '<=', date('Y-m-d'))->orderBy('publish_at', 'desc')->get();
        return view('frontend.blog.blog', compact('firstThreeArticles', 'fiveArticles', 'categories'))->with($this->_getFooterData());
    }

    public function singleBlog($slug) {
        $article = Article::where('slug', $slug)->with(['blogCategories',
                    'articleAuthor'])->first();
        $previousArticle = Article::where('publish_at', '>', $article->publish_at)->where('status', config('constant.backend.blogs.active'))->orderBy('publish_at', 'asc')->first(['slug']);
        $nextArticle = Article::where('publish_at', '<', $article->publish_at)->where('status', config('constant.backend.blogs.active'))->orderBy('publish_at', 'desc')->first(['slug']);
        return view('frontend.blog.single-blog', compact('article', 'previousArticle', 'nextArticle'))->with($this->_getFooterData());
    }

    public function postContact(Request $request) {
        $data = $request->all();
        Mail::send('frontend.emails.contact', ['data' => $data], function($message)use ($data) {
            $message->to('richard.tomes@totalwpm.com')
                    ->replyTo($data['email'], $data['name'])
                    ->subject($data['query_type']);
        });
        return response()->json(array('message' => 'Contact request successfully sent.'), 200);
    }

    public function getMoreBlogs($skip) {
        $oldDate = date("Y-m-d", strtotime("-1 year", time()));
        return Article::with(['blogCategories', 'articleAuthor'])->where('status', config('constant.backend.blogs.active'))->whereDate('publish_at', '<=', date('Y-m-d'))->whereDate('publish_at', '>', $oldDate)->orderBy('publish_at', 'desc')->skip($skip)->take(5)->get();
    }

    public function terms() {
        $term = Term::first();
        $content = NULL;
        if (!empty($term)) {
            $content = $term->content;
        }

        return view('frontend.home.terms', compact('content'))->with($this->_getFooterData());
    }

    public function privacy() {
        $privacy = Privacy::first();
        $content = NULL;
        if (!empty($privacy)) {
            $content = $privacy->content;
        }
        return view('frontend.home.privacy', compact('content'))->with($this->_getFooterData());
    }

    public function legal() {
        $legal = LegalDisclosure::first();
        $content = NULL;
        if (!empty($legal)) {
            $content = $legal->content;
        }
        return view('frontend.home.legal', compact('content'))->with($this->_getFooterData());
    }

    public function share(Request $request) {
        $data = $request->all();
        Mail::send('frontend.emails.share', ['data' => $data], function($message)use ($data) {
            $message->to($data['email'])
                    ->replyTo($data['email'])
                    ->subject('Share');
        });
        return response()->json(array('message' => 'Email Successfully sent'), 200);
    }

    public function investmentIntake(Request $request) {
        $data = $request->all();
        $get_string = '';
        foreach ($data['question'] as $key => $question) {
            $get_string = $get_string . $key . '=' . $question . '&';
        }
        return response()->json(array('href' => route('frontend.client.signUp', [config('constant.subdomain'), $get_string])), 200);
    }

    public function subscribe(Request $request) {
        $data = $request->all();
        $email = $data['email_address'];
        $mailchimp_settings = array(
            'apikey' => '37ae3102b523dc9bd2ccd00a0a465e9b-us14',
            'list_id' => '6b48b5fa7a',
            'account_email' => 'casey@speakeasy.media'
        );


        $mailchimp_result = $this->_ibMailchimpSubscribe(['email' => $email], $mailchimp_settings);
        $mailchimp_result = json_decode($mailchimp_result);
        if ($mailchimp_result->status == 'subscribed') {
            $newsletter_data = Newsletter::where('email', $email)->first();
            if (empty($newsletter_data)) {
                $newsletter_data = new Newsletter;
            }
            $newsletter_data->email = $email;
            $newsletter_data->status = '1';
            $newsletter_data->save();
            return response()->json(array('message' => 'GREAT!! Thanks for subscribing.'), 201);
        } else {
            if ($mailchimp_result->title == 'Member Exists') {
                return response()->json(array('message' => 'OOPS!! You have already subscribed.'), 500);
            } else {
                return response()->json(array('message' => 'OOPS!! ' . $mailchimp_result->title), 500);
            }
        }
        die();
    }

    private static function _getFooterData() {
        return ['footer' => Footer::first(), 'contact' => Contact::first()];
    }

    private static function _getHomeContent() {
        $checkEmpty = Homepage::get()->count();
        if (!$checkEmpty) {
            $homePageContent = $checkEmpty;
        } else {
            $data = Homepage::first()->toArray();
            $logoBoxes = LogoBox::get()->toArray();
            foreach ($logoBoxes as $key => $logoBox) {
                $data['logoBox'][$key]['box_logo'] = $logoBox['box_logo'];
                $data['logoBox'][$key]['box_heading'] = $logoBox['box_heading'];
                $data['logoBox'][$key]['box_description'] = $logoBox['box_description'];
            }
            $homePageContent = $data;
        }
        return $homePageContent;
    }

    public function _ibMailchimpSubscribe($data = false, $MC_VARS) {

        $key = explode('-', $MC_VARS['apikey']);

        $api = array
            (
            'login' => $MC_VARS['account_email'],
            'key' => $MC_VARS['apikey'],
            'url' => 'https://' . $key[1] . '.api.mailchimp.com/3.0/'
        );
        $type = 'PUT';
        $target = 'lists/' . $MC_VARS['list_id'] . '/members/' . md5($data['email']);

        $ch = curl_init($api['url'] . $target);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array
            (
            'Content-Type: application/json',
            'Authorization: ' . $api['login'] . ' ' . $api['key'],
        ));

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'PHP-MCAPI/3.0');


        $postData = array(
            "email_address" => $data['email'],
            "status" => 'subscribed',
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
