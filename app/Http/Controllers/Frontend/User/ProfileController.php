<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Models\Access\User\User;
Use Auth;
/**
 * Class ProfileController
 * @package App\Http\Controllers\Frontend
 */
class ProfileController extends Controller
{
	/**
	 * @var UserRepository
	 */
	protected $user;

	/**
	 * ProfileController constructor.
	 * @param UserRepository $user
	 */
	public function __construct(UserRepository $user) {
		$this->user = $user;
	}

	/**
	 * @param UpdateProfileRequest $request
	 * @return mixed
	 */
	public function update(UpdateProfileRequest $request)
    {
        $this->user->updateProfile(access()->id(), $request->all());
        return redirect()->route('frontend.user.account')->withFlashSuccess(trans('strings.frontend.user.profile_updated'));
    }
    
    
    public function index($user_id = null)
    {
        if (empty($user_id)) {
            $user_id = Auth::id();
        }
        return view('frontend.client.profile')->with(['user' => User::userData($user_id)]);
    }

}