<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Models\Access\User\User;
use App\Models\Profile;
use Response;
use Session;
Use Auth;
use App\Models\Access\Role\Role;
use App\Models\Backend\Article;
use App\Models\Backend\BlogCategory;
use App\Models\Backend\ArticleBlogCategory;
use App\Http\Requests\Backend\SaveArticleRequest;
use App\Http\Requests\Backend\SaveBlogCategoryRequest;
use App\Models\Backend\EmployeeNote;
use App\Models\Backend\ClientToEmployeeUser;
use Carbon\Carbon;
use App\Models\DocumentType;
use App\Models\Notification;
use App\Models\Document_Category;
use App\Models\Client_Document;
use App\Models\AttemptedQuestionAnswer;
use App\Models\SubTopicQuestionAnswer;
use App\Models\TopicQuestionAnswer;
use App\Models\SelectedServiceQuestionAnswer;
use App\Models\Backend\Service;
use DB;
use App\Models\FinancialProfile;
use App\Models\Questions;
use App\Models\QuestionAnswers;
use App\Models\Topics;
use App\Models\State;
use App\Models\ServiceDocument;
use App\Models\UserAccountInfo;
use App\Events\PrivateEvent;
use App\Models\Backend\ServiceUser;
use App\Models\Notification_data;

/**
 * Class FrontendController
 * @package App\Http\Controllers
 */
class EmployeeController extends Controller {

    public function home() {
        return view('frontend.employee.home');
    }

    //make this func get only clents of current employee  -> DONE
    public function allClients() {
        $user_id = Auth::user()->id;
        $employee_clients = ClientToEmployeeUser::where('employee_user_id', $user_id)->with(['clientUsers.profile'])->get()->transform(function ($employee, $key) {
            return $employee->clientUsers;
        });
        return view('frontend.employee.all_clients')->with(['all_clients' => $employee_clients]);
//        $all_clients = User::has('profile')->with('profile')->get();
    }

    public function getClientDetails($id = null) {
        $details = User::where('id', $id)->with(['profile', 'financialProfile',
                    'serviceQuestionsWhereFile', 'profileServiceQuestions'])->first();
        $employee_info = ClientToEmployeeUser::where('client_user_id', $id)->has('employeeUser')->with([
                    'employeeUser' => function($subQuery) {
                        $subQuery->select('id', 'name');
                    }])->first();
        $employeeName = '';
        if (!empty($employee_info)) {
            $employeeName = $employee_info->employeeUser->name;
        }

        $notesDetail = $this->getNotesRecords($id, auth()->user()->id);


        $activeServices = User::where('id', $id)->with(['services' => function($query) use($id) {
                        $query->where('status', 1)->with(['questions' => function($query) {
                                $query->orderBY('order', 'ASC')->with('questionInputs');
                            }])->with(['attemptedQuestions' => function($query) use($id) {
                                $query->where('user_id', $id)->with('attemptedQuestionAnswer');
                            }]);
                    }])->first();

        $allProfiles = [];
        if ($details->profileServiceQuestions->isNotEmpty()) {
            foreach ($details->profileServiceQuestions as $key => $allProfilesArray) {
                $allProfiles[$allProfilesArray->profileTags->name][$key]['question'] = $allProfilesArray->question;
                $allProfiles[$allProfilesArray->profileTags->name][$key]['answer'] = $allProfilesArray->pivot->value;
            }
        }
        $submittedDocuments = [];
        if ($activeServices->services->count()) {
            $activeServiceIds = $activeServices->services->pluck('id')->toArray();
            $submittedDocuments = AttemptedQuestionAnswer::where('doc_sign', [1, 0])->whereHas('attemptedQuestion', function($query) use($activeServiceIds) {
                        $query->whereIn('service_id', $activeServiceIds)->where('user_id', auth()->user()->id)->whereHas('question', function($query) {
                            $query->where('type', config('constant.service_question_types_options_inverse.usl'));
                        });
                    })->get();
        }
//        getting all documents type from db 
        $getDocumentCategories = Document_Category::pluck('document_category_name', 'id')->toArray();
        $allDocumentsStatements = Client_Document::where('user_id', $id)->where('document__categories_id', 1)->get()->toArray();
        $allDocumentsTaxForms = Client_Document::where('user_id', $id)->where('document__categories_id', 2)->get()->toArray();
        $allDocumentsInsuranceForms = Client_Document::where('user_id', $id)->where('document__categories_id', 3)->get()->toArray();
        $allDocumentsOtherDocs = Client_Document::where('user_id', $id)->where('document__categories_id', 4)->get()->toArray();
        $notificationId = Notification::where('sender', $id)->orWhere('receiver', $id)->get();
//        $clientServices = $this->_getClientSpecificServices($id);
        $clientServices = Service::select('id', 'service_number', 'title')->whereHas('serviceUsers', function($query) use ($id) {
                    $query->where(['user_id' => $id, 'status' => 1]);
                })->get();

        $user_account_info = UserAccountInfo::where('user_id', $id)->first();

//   var $getDocumentCategories plucked data categories and id 
        return Response::view('frontend.employee.includes.clientDetails', ['client' => $details, 'all_notes' => $notesDetail['notes'], 'activeServices' => $activeServices,
                    'all_profiles' => $allProfiles, 'submittedDocuments' => $submittedDocuments,
                    'user_id' => Auth::id(), 'employeeName' => $employeeName, 'totalRecords' => $notesDetail['total'],
                    'documentCategories' => $getDocumentCategories, 'allDocumentsStatements' => $allDocumentsStatements,
                    'allDocumentsTaxForms' => $allDocumentsTaxForms, 'allDocumentsInsuranceForms' => $allDocumentsInsuranceForms,
                    'allDocumentsOtherDocs' => $allDocumentsOtherDocs, 'notificationId' => $notificationId,
                    'services' => $clientServices,
                    'user_account_info' => $user_account_info,
                    'client_id' => $id
                        ], 200);
    }

//make this func get only clents of current employee -> DONE
    public function getAllClients($search_text = null) {
//        $all_clients = User::whereHas('roles', function($q) {
//                    $q->where('name', 'User');
//                })->whereHas('profile', function($q) use($search_text) {
//                    $q->where('first_name', 'like', '%' . $search_text . '%')
//                    ->orWhere('middle_name', 'like', '%' . $search_text . '%')
//                    ->orWhere('last_name', 'like', '%' . $search_text . '%');
//                })
//                ->with('profile')
//                ->get();


        $user_id = Auth::user()->id;
        $all_clients = ClientToEmployeeUser::where('employee_user_id', $user_id)->with(['clientUsers.profile' => function($q) use($search_text) {
                        $q->where('first_name', 'like', '%' . $search_text . '%')
                                ->orWhere('middle_name', 'like', '%' . $search_text . '%')
                                ->orWhere('last_name', 'like', '%' . $search_text . '%');
                    }])->whereHas('clientUsers.profile', function($q) use($search_text) {
                    $q->where('first_name', 'like', '%' . $search_text . '%')
                            ->orWhere('middle_name', 'like', '%' . $search_text . '%')
                            ->orWhere('last_name', 'like', '%' . $search_text . '%');
                })->get()->transform(function ($employee, $key) {
            return $employee->clientUsers;
        });
        return Response::view('frontend.employee.includes.clientListItems', ['all_clients' => $all_clients], 200);
    }

    //Article
    public function articleList() {
        $articles = Article::orderBy('publish_at', 'desc')->get();
        return view('frontend.employee.blogs.list_blogs', compact('articles'));
    }

    public function createArticle() {
        $categories = BlogCategory::get()->pluck('title', 'id')->toArray();
        $users = User::whereHas('roles', function($q) {
                    $q->where('name', 'employee');
                })->get()->pluck('name', 'id')->toArray();
        return view('frontend.employee.blogs.create_blog', compact('categories', 'users'));
    }

    public function saveArticle(SaveArticleRequest $request) {
        $files = $request->file();
        $input = $request->input();
        $image = $this->_uploadImages($files['image'], 'img/backend/blogs/articles');
        $slug = $this->_validateSlug($input['slug']);
        $article = Article::create([
                    'author' => $input['author'],
                    'slug' => $slug,
                    'title' => $input['title'],
                    'image' => $image,
                    'excerpt' => $input['excerpt'],
                    'body' => $input['body'],
                    'status' => (isset($input['status'])) ? $input['status'] : config('constant.backend.blogs.default_status'),
                    'publish_at' => (!empty($input['publish_at']) ? $input['publish_at'] : date('Y-m-d'))
        ]);
        $article_id = $article->id;
        foreach ($input['blog_category_id'] as $blogCategoryId) {
            $article->blogCategories()->attach(['blog_category_id' => $blogCategoryId]);
        }
        return redirect()->route('frontend.employee.articleList')->withFlashSuccess('Article successfully created.');
    }

    private function _validateSlug($slug) {
        $count = Article::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        return $count ? ($slug . '-' . ($count + 1)) : $slug;
    }

    public function makeSlugFromTitle($title) {
        $slug = Str::slug($title);
        $count = Article::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        return $count ? ($slug . '-' . ($count + 1)) : $slug;
    }

    public function autoGenerateSlug(Request $request) {
        $input = $request->input();
        $slug = $this->makeSlugFromTitle($input['title']);
        return response()->json(['slug' => $slug], 200);
    }

    public function checkSlugDuplicacy(Request $request) /* slug editing disabled */ {
        $input = $request->input();
        $duplicate = Article::where('slug', $input['slug'])->count();
        $message = ($duplicate) ? 'This slug is already in use. Please choose another slug.' : 'This slug is available.';
        $status = ($duplicate) ? 'duplicate' : 'unique';
        return response()->json(['message' => $message, 'status' => $status], 200);
    }

    private function _uploadImages($file, $directory) {
        $fileName = date('d-m-y-h-i-s-') . $file->getClientOriginalName();
        $file->move($directory, $fileName);
        return $fileName;
    }

    public function editArticle($id) {
        $categories = BlogCategory::get()->pluck('title', 'id')->toArray();
        $article = Article::where('id', $id)->with(['blogCategories', 'articleAuthor'])->first();
        $users = User::get()->pluck('name', 'id')->toArray();
        return view('frontend.employee.blogs.create_blog', compact('categories', 'article', 'users'));
    }

    public function updateArticle(SaveArticleRequest $request) {
        $files = $request->file();
        $input = $request->input();
        $article = Article::find($input['id']);
        $image = (isset($files['image'])) ? $this->_uploadImages($files['image'], 'img/backend/blogs/articles') : $article->image;
        $slug = $this->_validateSlug($input['slug']);
        $article->update([
            'author' => $input['author'],
            'slug' => $slug,
            'title' => $input['title'],
            'image' => $image,
            'excerpt' => $input['excerpt'],
            'body' => $input['body'],
            'status' => (isset($input['status'])) ? $input['status'] : config('constant.backend.blogs.default_status'),
            'publish_at' => (!empty($input['publish_at']) ? $input['publish_at'] : date('Y-m-d'))
        ]);
        $article->blogCategories()->sync($input['blog_category_id']);
        return redirect()->route('frontend.employee.articleList')->withFlashSuccess('Article successfully updated.');
    }

    public function deleteArticle(Request $request) {
        $input = $request->input();
        $article = Article::find($input['id']);
        $article->blogCategories()->detach();
        $article->delete();
        return $request->session()->flash('flash_success', 'The Article has been successfully deleted.');
    }

    //add notes function
    public function addEmployeeClientNotes(Request $request) {
        $this->validate($request, [
            'employee_id' => 'required',
            'client_id' => 'required',
            'notes' => 'required'
        ]);

        $employee = new EmployeeNote();

        $response = $employee->create([
            'employee_id' => $request['employee_id'],
            'client_id' => $request['client_id'],
            'note' => $request['notes']
        ]);
        if ($response) {
            $data = $this->getNotesRecords($request['client_id'], $request['employee_id']);
            return Response::json(['message' => 'Notes addedd.', 'notes' => $data['notes'],
                        'employeeName' => $data['userName'], 'totalRecords' => $data['total']], 200);
        }
        return Response::json(['message' => 'Error.'], 500);
    }

    private function getNotesRecords($clientId, $employeeId) {
        $response = [];
        $response['notes'] = EmployeeNote::where(['employee_id' => $employeeId,
                    'client_id' => $clientId])->orderBy('created_at', 'desc')->select('created_at', 'note')->paginate(15)->map(function ($item) {
                    $arr['note'] = $item->note;
                    $arr['date'] = Carbon::parse($item->created_at)->format('m/d/Y g:i A');
                    return $arr;
                })->toArray();
        $userInfo = User::where('id', $employeeId)->select('name')->first();
        $response['userName'] = $userInfo->name;
        $response['total'] = EmployeeNote::where(['employee_id' => $employeeId,
                    'client_id' => $clientId])->count();
        return $response;
    }

    public function getNotesData(Request $request) {
        $this->validate($request, [
            'client_id' => 'required',
        ]);
        $data = $this->getNotesRecords($request['client_id'], auth()->user()->id);
        return Response::json(['notes' => $data['notes'], 'employeeName' => $data['userName'],
                    'totalRecords' => $data['total']], 200);
    }

    private function _getClientSpecificServices($clientId) {
        $service1 = [];
        $service2 = [];
        $service3 = [];
        $services = [];
        $subTopicsData = SubTopicQuestionAnswer::select('service_id')->where('user_id', $clientId)->where('answer', '!=', null)->distinct()->get();
        if (!empty($subTopicsData)) {
            $service1 = array_keys($subTopicsData->groupBy('service_id')->toArray());
        }
        $topicAnswer = TopicQuestionAnswer::select('service_id')->where('user_id', $clientId)->where('answer', '!=', null)->distinct()->get();
        if (!empty($topicAnswer)) {
            $service2 = array_keys($topicAnswer->groupBy('service_id')->toArray());
        }
        $selectedServiceAnswers = SelectedServiceQuestionAnswer::select('service_id')->where('user_id', $clientId)->where('answer', '!=', null)->distinct()->get();
        if (!empty($selectedServiceAnswers)) {
            $service3 = array_keys($selectedServiceAnswers->groupBy('service_id')->toArray());
        }
        $allServicesIds = array_unique(array_merge($service3, $service2, $service1));

        if (!empty($allServicesIds)) {
            $services = Service::select('id', 'service_number', 'title')->whereIn('id', $allServicesIds)->get();
        }
        return $services;
    }

    public function currentServicePage($serviceId, $clientId) {
        $selectedServiceAnswers = SelectedServiceQuestionAnswer::where('service_id', $serviceId)->where('user_id', $clientId)->get();
        $defaultData = $this->_getDefaultUserInfo($clientId);
        $answer = [];
        if (!empty($selectedServiceAnswers)) {
            foreach ($selectedServiceAnswers as $key => $data) {
                $answerInfo = json_decode($data['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
        }
        $service = Service::select('service_number')->where('id', $serviceId)->first();

        if (!empty($service)) {
            return view('frontend.employee.services.service_' . $service->service_number, compact('answer', 'defaultData'));
        }

        return response()->json(['message' => 'no sevice exists']);
    }

    public function getServiceTopicAnswers($serviceId, $topicId, $questionNo, $clientId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $serviceId)->first();
        $defaultData = $this->_getDefaultUserInfo($clientId);

        $topicAnswer = TopicQuestionAnswer::where(['topic_id' => $topicId, 'service_id' => $serviceId,
                    'question_id' => $questionNo])->where('user_id', $clientId)->get();
        $answer = [];
        if ($serviceId == 3 && $topicId == 303) {
            $fetchExtraFieldAnswer = TopicQuestionAnswer::where(['topic_id' => 302,
                        'service_id' => $serviceId])->where('user_id', $clientId)->first();
            if (!empty($fetchExtraFieldAnswer)) {
                $answerData = json_decode($fetchExtraFieldAnswer['answer'], true);

                if (array_key_exists('own_or_rent_your_house', $answerData)) {
                    $answer['own'] = $answerData['own_or_rent_your_house'];
                }
            }
        }
        if (!empty($topicAnswer)) {
            foreach ($topicAnswer as $key => $topic) {
                $answerInfo = json_decode($topic['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
        }
        return view('frontend.employee.services.service-questions.question_' . $questionNo, compact('answer', 'defaultData'));
//        return $answer;
    }

    public function getServiceSubTopicQuestionAnswers($serviceId, $topicId, $subTopicId, $clientId) {
        $currentService = Service::select('id', 'service_number', 'title')->where('id', $serviceId)->first();
        $defaultData = $this->_getDefaultUserInfo($clientId);
        $subTopicAnswer = SubTopicQuestionAnswer::where(['service_id' => $id, 'topic_id' => $topicId,
                    'sub_topic_id' => $subTopicId, 'question_id' => $questionNo])->where('user_id', $clientId)->get();
        $answer = [];
        if (!empty($subTopicAnswer)) {
            foreach ($subTopicAnswer as $key => $subTopic) {
                $answerInfo = json_decode($subTopic['answer'], true);
                foreach ($answerInfo as $key => $value) {
                    $answer[$key] = $value;
                }
            }
        }
        return $answer;
    }

    private function _getDefaultUserInfo($id) {
        $housholdValue = 0;
        $taxFillingStatus = '';
        $firstName = '';
        $lastName = '';
        $middleName = '';
        $taxFillingStatusInfo = FinancialProfile::where('user_id', $id)->first();
        $personalInformation = Profile::where('user_id', $id)->first();
        if (!empty($taxFillingStatusInfo)) {
            $taxFillingStatus = $taxFillingStatusInfo['tax_filing_status'];
            $housholdValue = $taxFillingStatusInfo['annual_income'];
        }
        if (!empty($personalInformation)) {
            $firstName = $personalInformation['first_name'];
            $middleName = $personalInformation['middle_name'];
            $lastName = $personalInformation['last_name'];
        }

        $values = [];
        $values['housesholdIncome'] = $housholdValue;
        $values['taxFillingStatus'] = $taxFillingStatus;
        $values['firstName'] = $firstName;
        $values['lastName'] = $lastName;
        $values['middleName'] = $middleName;
        return $values;
    }

    public function updateClientAnswers() {
        
    }

    public function getQuestions($topicId) {
        $questions_answers = \App\Models\Questions::where('topic_id', $topicId)->with('answers')->get();
        echo'<PRE>';
        print_r($questions_answers);
        die;
    }

    //END

    public function testClients() {
        $user_id = Auth::user()->id;
        $employee_clients = ClientToEmployeeUser::where('employee_user_id', $user_id)->with(['clientUsers.profile'])->get()->transform(function ($employee, $key) {
            return $employee->clientUsers;
        });
        return view('frontend.employee.test_clients')->with(['all_clients' => $employee_clients]);
//        $all_clients = User::has('profile')->with('profile')->get();
    }

    public function getTopicsOrQuestions(Request $request, $serviceId, $clientId, $topicId = null) {
        $service = Service::where('id', $serviceId)->with(['serviceTopics' => function($query) {
                        $query->whereNull('parent_id');
                    }])->first();
        $spouse_name = FinancialProfile::where('user_id', $clientId)->value('spouse_name');
        if (!empty($service)) {
//            $service->serviceTopics = NULL;
            $serviceTitle = $service->title;
            if (isset($service->serviceTopics) && !empty($service->serviceTopics) && $service->serviceTopics->count() > 0) {

                $topics = $service->serviceTopics;

                $financial_profile = FinancialProfile::where('user_id', $clientId)->select(['tax_filing_status','annual_income'])->first();
                $tax_filing_status = $financial_profile->tax_filing_status;
                $annual_income = ($financial_profile) ? $financial_profile->annual_income : '';
                $preTopicQuestions = Questions::where('service_id', $serviceId)->whereNull('topic_id')->with(['answers' => function($query ) use ($clientId) {
                                $query->where('user_id', $clientId);
                            }])->get();

                $serviceHasFiles = Questions::where(['service_id' => $serviceId, 'type' => 'file'])->pluck('id');
                $documentsTab = false;
                if ($serviceHasFiles->count() >= 1)
                    $documentsTab = true;

                return view('frontend.employee.services.service-questions.viewTopics', compact('topics', 'serviceTitle', 'preTopicQuestions', 'serviceId', 'tax_filing_status', 'documentsTab', 'spouse_name', 'annual_income'));
            }
            return $this->getQuestionsOfTopicOrSubTopic($request, $serviceId, $clientId, $topicId, $serviceTitle, true);
        }

        return response()->json(['message' => 'no sevice exists']);
    }

    public function getSubTopicsOrQuestions(Request $request, $serviceId, $clientId, $topicId) {
        $service = Service::where('id', $serviceId)->with(['serviceTopics' => function($query) use ($topicId) {
                        $query->where('parent_id', $topicId);
                    }])->first();
//dd($service);
        if (!empty($service)) {
//            $service->serviceTopics=NULL;
            $serviceTitle = $service->title;
            $topicName = ucwords($request->input('topicName'));

            if (isset($service->serviceTopics) && !empty($service->serviceTopics) && $service->serviceTopics->count() > 0) {
                $subTopics = $service->serviceTopics;
                $financial_profile = FinancialProfile::where('user_id', $clientId)->select('tax_filing_status')->first();
                $tax_filing_status = $financial_profile->tax_filing_status;

                $spouse_name = FinancialProfile::where('user_id', $clientId)->value('spouse_name');
                $annual_income = ($financial_profile) ? $financial_profile->annual_income : '';

                return view('frontend.employee.services.service-questions.viewSubTopics', compact('subTopics', 'serviceTitle', 'topicName', 'tax_filing_status', 'spouse_name' . 'annual_income'));
            }
            return $this->getQuestionsOfTopicOrSubTopic($request, $serviceId, $clientId, $topicId, $topicName, false, true);
        }
    }

    public function getQuestionsOfTopicOrSubTopic(Request $request, $serviceId, $clientId, $topicId = null, $title = '', $directQuestions = false, $directTopicQuestions = false) {
//        dd(session()->all());

        $displayType = config('constant.questions.general');

        //First Tab -> General Life for life insurance in comprehensive Check starts
        if (in_array($topicId, [config('constant.questions.comprehensive_life_insurance_id'), config('constant.questions.comprehensive_spouse_life_insurance_id')])) {
            $displayType = config('constant.questions.general_life');
        }
        //First Tab -> General Life for life insurance in comprehensive Check Ends


        if ($request->has('displayType')) {
            $displayType = $request->input('displayType');
        }
//        print_r($displayType);die;
        $conditions = ['service_id' => $serviceId, 'display_type' => $displayType];
        if ($topicId) {
            $conditions = ['service_id' => $serviceId, 'display_type' => $displayType, 'topic_id' => $topicId];
        }

        $profile = Profile::where('user_id', $clientId)->first(['first_name', 'last_name']);
        $userName = $profile->first_name . ' ' . $profile->last_name;
//        print_r($conditions);
        $questions = Questions::where($conditions)->orderByRaw('COALESCE(parent_id, id), parent_id IS NOT NULL, id')
                        ->with(['answers' => function($query ) use ($clientId) {
                                $query->where('user_id', $clientId);
                            }, 'firstChild.answers' => function($query ) use ($clientId) {
                                $query->where('user_id', $clientId);
                            }, 'parent.answers' => function($query ) use ($clientId) {
                                $query->where('user_id', $clientId);
                            }])->get()->filter(function ($question, $key) {
            if ((empty($question->is_modal) ||
                    ($question->is_modal && empty($question->parent_id))
                    ) &&
                    !in_array($question->name, ['file_name', 'file_path', 'fileholder_name', 'file_type']
                    )
            ) {
                return $question;
            }
        });
        if ($request->has('topicName')) {
            $title = ucwords($request->input('topicName'));
        }


//        echo'<PRE>';
//        print_r($questions->toArray());
//        die;
        $topic_is_copy = Topics::where('id', $topicId)->pluck('is_copy')->first();
        $answerIndexCopy = '';

        if ($topic_is_copy) {
            $answerIndexCopy = 0;
        }
        if ($request->has('copyIndex')) {
            $answerIndexCopy = $request->input('copyIndex');
        } else {
            $answerIndexCopy = '';
        }


        $blank_answer = false;
        if (strstr($answerIndexCopy, config('constant.questions.new'))) {
            $answerIndexCopy = explode('_' . config('constant.questions.new'), $answerIndexCopy)[0];
//    dd($answerIndexCopy);
//    $answerIndex='';
            $blank_answer = true;
        }

        $firstLevelQuestions = true;


        $states = State::pluck('name', 'id')->toArray();
        $financial_profile = FinancialProfile::where('user_id', $clientId)->select(['tax_filing_status', 'annual_income'])->first();

        $tax_filing_status = ($financial_profile) ? $financial_profile->tax_filing_status : '';
        $annual_income = ($financial_profile) ? $financial_profile->annual_income : '';
        $files_question = Questions::whereIn('name', ['file_name', 'file_path', 'fileholder_name', 'file_type'])->get()->toArray();
        $spouse_name = FinancialProfile::where('user_id', $clientId)->value('spouse_name');

        return view('frontend.employee.services.service-questions.viewQuestions', compact('questions', 'title', 'directQuestions', 'directTopicQuestions', 'question_modal_id', 'serviceId', 'topicId', 'modal_question_answers_users', 'userName', 'clientId', 'answerIndexCopy', 'topic_is_copy', 'displayType', 'firstLevelQuestions', 'blank_answer', 'states', 'files_question', 'tax_filing_status', 'spouse_name', 'annual_income'));
    }

    public function getDependentQuestions(Request $request, $serviceId, $clientId, $topicId = null, $returnQuestions = false) {
        Session::set('clientId', $clientId);

        $conditions = ['service_id' => $serviceId, 'is_modal' => '1'];
        $topic_is_child = '';
        if ($topicId) {
            $conditions['topic_id'] = $topicId;
            $topic = Topics::where('id', $topicId)->select('id', 'parent_id')->first();
            $topic_is_child = $topic->parent_id;
        }
        $directDependentQuestions = false;
        if ($request->has('questionId')) {
            $questionId = $request->input('questionId');
            $conditions['parent_id'] = $questionId;
        }

        if ($topicId) {
//            \DB::enableQueryLog();
            $questionsData = Questions::where($conditions)->whereNotNull('parent_id')->with(['answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }, 'recursiveChildren.answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }
                        , 'parent.answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }])
                    ->get();
//            dd(\DB::getQueryLog());
        } else {
            $directDependentQuestions = true;
            $questionsData = Questions::where($conditions)->whereNull('parent_id')->with(['answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }, 'recursiveChildren.answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }, 'parent.answers' => function($query ) use ($clientId) {
                            $query->where('user_id', $clientId);
                        }])->get();
        }
        $profile = Profile::where('user_id', $clientId)->first(['first_name', 'last_name']);
        $userName = $profile->first_name . ' ' . $profile->last_name;
        $title = 'Sub Questions';
        $answerIndex = '';
        $answerIndexCopy = '';
        $data_previous_slide_copy_index = '';
        if ($request->has('answerIndex')) {
            $answerIndex = $request->input('answerIndex');
        }
        if ($request->has('copyIndex')) {
            $answerIndexCopy = $request->input('copyIndex');
        }
        if ($request->has('data_previous_slide_copy_index')) {
            $data_previous_slide_copy_index = $request->input('data_previous_slide_copy_index');
        }
//        echo'<PRE>';
//        print_R($questionsData->toArray());die;
        $questions = collect();
        $this->_flattenArray($questionsData, $questions);
        if ($returnQuestions) {
            return $questions;
        }
        $blank_answer = false;
        if (strstr($answerIndex, config('constant.questions.new'))) {
            $answerIndex = explode('_' . config('constant.questions.new'), $answerIndex)[0];
//    dd($answerIndex);
//    $answerIndex='';
            $blank_answer = true;
        }
//        echo'<PRE>';
//        print_R($questions->toArray());
//        die;
        $states = State::pluck('name', 'id')->toArray();

        $service = Service::where('id', $serviceId)->first(['title']);
        $serviceTitle = $service->title;

        $displayType = '';
        if ($request->has('displayType')) {
            $displayType = $request->input('displayType');
        }
        $files_question = Questions::whereIn('name', ['file_name', 'file_path', 'fileholder_name', 'file_type'])->get()->toArray();

        $financial_profile = FinancialProfile::where('user_id', $clientId)->select(['tax_filing_status', 'annual_income'])->first();
        $tax_filing_status = $financial_profile->tax_filing_status;
        $annual_income = ($financial_profile) ? $financial_profile->annual_income : '';

        $spouse_name = FinancialProfile::where('user_id', $clientId)->value('spouse_name');

        return view('frontend.employee.services.service-questions.viewModalQuestions', compact('questions', 'title', 'answerIndex', 'userName', 'clientId', 'serviceTitle', 'directDependentQuestions', 'serviceId', 'topicId', 'answerIndexCopy', 'blank_answer', 'topic_is_child', 'states', 'files_question', 'data_previous_slide_copy_index', 'displayType', 'tax_filing_status', 'spouse_name', 'annual_income'));
    }

    protected function _flattenArray($questionsData, &$questions) {
        foreach ($questionsData as $value) {
            $questions[] = $value;
            if ($value->recursiveChildren->count() > 0) {
                $this->_flattenArray($value->recursiveChildren, $questions);
            }
        }
    }

    public function updateAnswers(Request $request, $serviceId, $clientId, $topicId = null) {
        $answerIndex = $request->input('answerIndex');
        $copyIndex = $request->input('copyIndex');
        $data = $request->input('data');
//        echo'<PRE>';
//        print_R($request->all());
//        die; 
        $update_count = 0;
        if (isset($data) && !empty($data)) {
            foreach ($data as $question_id => $answer) {
                $question_answer_record = QuestionAnswers::where(['question_id' => $question_id, 'user_id' => $clientId])->select('id', 'answer')->first();
                if (is_array($answer)) {
                    if (isset($question_answer_record) && !empty($question_answer_record)) {


                        $answer_unserialized = json_decode($question_answer_record->answer);
//                    print_R($answer_unserialized);die;
                        if (isset($answer_unserialized) && !empty($answer_unserialized)) {
                            $new_answer = [];
                            $new_answer = serialize($answer);
                            if (!isset($answer_unserialized[$answerIndex]))
                                $answer_unserialized[$answerIndex] = (object) [];
                            $answer_unserialized[$answerIndex]->value = $new_answer;
//                      print_R($answer_unserialized);
//                      print_R(json_encode($answer_unserialized));die;
                            $question_answer_record->answer = json_encode($answer_unserialized);
                            if ($question_answer_record->save()) {
                                $update_count++;
                            }
                        }
                    } else {
                        $answer_unserialized = [];
                        $new_answer = [];
                        $new_answer = serialize($answer);
                        if (!isset($answer_unserialized[$answerIndex]))
                            $answer_unserialized[$answerIndex] = (object) [];
                        $answer_unserialized[$answerIndex]->value = $new_answer;
                        $question_answer_record = new QuestionAnswers();
                        $question_answer_record->user_id = $clientId;
                        $question_answer_record->question_id = $question_id;
                        $question_answer_record->answer = json_encode($answer_unserialized);
                        if ($question_answer_record->save()) {
                            $update_count++;
                        }
                    }
                } else {
                    $answer_decoded = json_decode($answer, true);

                    if (isset($answer_decoded) && !empty($answer_decoded) && is_array($answer_decoded)) {
                        if (!empty($question_answer_record) && $question_answer_record->count() > 0) {
                            $saved_answer = $question_answer_record->answer;
                            $saved_answer_decoded = json_decode($saved_answer, true);
                        }
//                        print_R($saved_answer_decoded);
//                        die;
                        if (empty($question_answer_record)) {
                            $question_answer_record = new QuestionAnswers();
                            $question_answer_record->user_id = $clientId;
                            $question_answer_record->question_id = $question_id;
                        }

                        if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {

                            $saved_answer_decoded[$answerIndex]['value'] = $answer_decoded;
                        } elseif ($copyIndex != '' && is_numeric($copyIndex) && !is_numeric($answerIndex)) {
                            $saved_answer_decoded[$copyIndex]['value'] = $answer_decoded;
                        } elseif ($copyIndex != '' && is_numeric($copyIndex) && $answerIndex != '' && is_numeric($answerIndex)) {
                            $saved_answer_decoded[$copyIndex][$answerIndex]['value'] = $answer_decoded;
                        } else {
                            $saved_answer_decoded = $answer_decoded;
                        }
//echo'<PRE>';
//print_r($saved_answer_decoded);
//print_r(json_encode($saved_answer_decoded));die;
                        $question_answer_record->answer = json_encode($saved_answer_decoded);
                        if ($question_answer_record->save()) {
                            $update_count++;
                        }
                    } else {

                        if (isset($question_answer_record) && !empty($question_answer_record)) {
                            $saved_answer = $question_answer_record->answer;
                            $saved_answer_decoded = json_decode($saved_answer, true);


                            if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                                if (is_array($saved_answer_decoded)) {
                                    if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {
                                        $saved_answer_decoded[$answerIndex]['value'] = $answer;
                                    } elseif ($copyIndex != '' && is_numeric($copyIndex) && !is_numeric($answerIndex)) {
                                        $saved_answer_decoded[$copyIndex]['value'] = $answer;
                                    } elseif ($copyIndex != '' && is_numeric($copyIndex) && $answerIndex != '' && is_numeric($answerIndex)) {
                                        $saved_answer_decoded[$copyIndex][$answerIndex]['value'] = $answer;
                                    }

//                            die;

                                    $question_answer_record->answer = json_encode($saved_answer_decoded);
                                } else {
                                    if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {
                                        $new_answer_decoded = [];
                                        $new_answer_decoded[$answerIndex]['value'] = $answer;
                                        $question_answer_record->answer = json_encode($new_answer_decoded);
                                    } else {
                                        $question_answer_record->answer = $answer;
                                    }
                                }
                            } else {

                                if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {
                                    echo'ithe';
                                    $new_answer_decoded = [];
                                    $new_answer_decoded[$answerIndex]['value'] = $answer;
                                    $question_answer_record->answer = json_encode($new_answer_decoded);
                                } else {
                                    $question_answer_record->answer = $answer;
                                }
                            }


                            if ($question_answer_record->save()) {
                                $update_count++;
                            }
                        } else {
                            $question_answer_record = new QuestionAnswers();
                            $question_answer_record->user_id = $clientId;
                            $question_answer_record->question_id = $question_id;
                            $answer_to_be_saved = '';
                            $answer_array_to_be_saved = [];
                            if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {
                                $answer_array_to_be_saved[$answerIndex]['value'] = $answer;
                                $answer_to_be_saved = json_encode($answer_array_to_be_saved);
                            } elseif ($copyIndex != '' && is_numeric($copyIndex) && !is_numeric($answerIndex)) {
                                $answer_array_to_be_saved[$copyIndex]['value'] = $answer;
                                $answer_to_be_saved = json_encode($answer_array_to_be_saved);
                            } elseif ($copyIndex != '' && is_numeric($copyIndex) && $answerIndex != '' && is_numeric($answerIndex)) {
                                $answer_array_to_be_saved[$copyIndex][$answerIndex]['value'] = $answer;
                                $answer_to_be_saved = json_encode($answer_array_to_be_saved);
                            } elseif (!is_numeric($answerIndex) && !is_numeric($copyIndex)) {
                                $answer_to_be_saved = $answer;
                            }
//                    echo'here';
//                    print_R($answer_to_be_saved);die;
                            $question_answer_record->answer = $answer_to_be_saved;
                            if ($question_answer_record->save()) {
                                $update_count++;
                            }
                        }
                    }
                }
            }
        }
        if (($update_count && !empty($data)) || (empty($data))) {
            if ($clientId != Auth::user()->id) {
                $currentService = Service::select('id', 'service_number', 'title')->where('id', $serviceId)->first();
                $client_employee = ClientToEmployeeUser::where('client_user_id', $clientId)->with(['employeeUser' => function($query) {
                                $query->select('id', 'name');
                            }])->first();
                if (isset($client_employee) && !empty($client_employee)) {
                    $employeeId = $client_employee->employee_user_id;
                    $userInfo = User::where('id', $clientId)->with(['profile' => function($query) {
                                    $query->select('id', 'user_id', 'first_name');
                                }])->first();
                    $name = "";
                    if ($userInfo->name != '') {
                        $name = $userInfo->name;
                    } else {
                        $name = $userInfo->profile->first_name;
                    }
                    $employee_name = $client_employee->employeeUser->name;

                    $text = "Hi " . $name . ", Your Employee '" . $employee_name . "' has Successfuly Updated Your Service " . $currentService['title'];
                    $notificationData = new Notification_data;
                    $notificationData->notification_room_id = -1;
                    $notificationData->sender_id = $employeeId;
                    $notificationData->notification = $text;
                    $notificationData->receiver_id = $clientId;
                    $notificationData->is_read = 0;
                    $notificationData->save();
                    $notification = Notification_data::with('sender')->find($notificationData->id);
                    broadcast(new PrivateEvent($notification))->toOthers();
                }
            }
            return response()->json([
                        'success' => true,
                        'status' => 200,
                        'message' => 'Updated'
            ]);
        }

        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function deleteAnswers(Request $request, $serviceId, $clientId, $topicId = null) {
        $answerIndex = $request->input('answerIndex');
        $copyIndex = $request->input('copyIndex');
        $questionId = $request->input('questionId');
        $questions = $this->getDependentQuestions($request, $serviceId, $clientId, $topicId, 'true');
//        echo'<PRE>';
//        print_R($questions);
//        die;
        $delete_count = 0;

        if (isset($questions) && !empty($questions)) {
            foreach ($questions as $single_question) {
                $questionId = $single_question->id;
                $question_answer_record = QuestionAnswers::where(['question_id' => $questionId, 'user_id' => $clientId])->select('id', 'answer')->first();
//                                 echo"<PRE>";print_R(['question_id' => $questionId, 'user_id' => $clientId]);

                if (isset($question_answer_record)) {
                    $saved_answer = $question_answer_record->answer;
                    $saved_answer_decoded = json_decode($saved_answer, true);
//                    print_R($saved_answer);
//                    print_R($saved_answer_decoded);
//                        echo'<PRE>';print_R($saved_answer_decoded);
//                        echo'he';
                    if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                        if (is_array($saved_answer_decoded)) {

//print_R($saved_answer_decoded);die;
                            if ($answerIndex != '' && is_numeric($answerIndex) && !is_numeric($copyIndex)) {
//                                $saved_answer_decoded[$answerIndex]['value']='';
                                unset($saved_answer_decoded[$answerIndex]);
                            }
//                          
                            elseif ($copyIndex != '' && is_numeric($copyIndex) && !is_numeric($answerIndex)) {
                                unset($saved_answer_decoded[$copyIndex]);
//                                                                $saved_answer_decoded[$copyIndex]['value']='';
                            }
//                            
                            elseif ($copyIndex != '' && is_numeric($copyIndex) && $answerIndex != '' && is_numeric($answerIndex)) {
                                unset($saved_answer_decoded[$copyIndex][$answerIndex]);
//                                                                                                $saved_answer_decoded[$copyIndex][$answerIndex]['value']='';
                            }
                            $saved_answer_decoded = array_values($saved_answer_decoded);
                            if (empty($saved_answer_decoded)) {
                                $question_answer_record->answer = '';
                            } else {
                                $question_answer_record->answer = json_encode($saved_answer_decoded);
                            }
                        } else {
                            $question_answer_record->answer = '';
                        }
                    } else {
                        $question_answer_record->answer = '';
                    }
//print_R($saved_answer_decoded);
//print_R(json_encode($saved_answer_decoded));
//die;
                    if ($question_answer_record->save()) {
                        $delete_count++;
                    }
                }
            }
        }
//        echo'<PRE>';
//        print_R($data);
//        die;
//              
        if ($delete_count) {
            return response()->json([
                        'success' => true,
                        'status' => 200,
                        'message' => 'Updated'
            ]);
        }
        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function addFiles(Request $request, $serviceId, $clientId, $topicId = null) {
//        
        $data = $request->input('data');
//        echo'<PRE>';
//        print_R($request->all());
//        die;
//      
        $questionId = $request->input('questionId');
        $add_count = 0;
        if (isset($data) && !empty($data) && !empty($questionId)) {
            foreach ($data as $key_answer => $answer) {
//                if ($answer != '') {
                $question_answer_record = QuestionAnswers::where(['question_id' => $questionId, 'user_id' => $clientId])->select('id', 'answer')->first();
                if (isset($question_answer_record) && !empty($question_answer_record)) {
                    $saved_answer = $question_answer_record->answer;
                    $saved_answer_decoded = json_decode($saved_answer, true);
                    if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                        if (is_array($saved_answer_decoded)) {
                            $saved_answer_decoded[count($saved_answer_decoded)] = $answer;
                            $question_answer_record->answer = json_encode($saved_answer_decoded);
                        }
                    } else {
                        $saved_answer_decoded[0] = $answer;
                        $question_answer_record->answer = json_encode($saved_answer_decoded);
                    }


                    if ($question_answer_record->save()) {
                        $add_count++;
                    }
                } else {
                    $question_answer_record = new QuestionAnswers();
                    $question_answer_record->user_id = $clientId;
                    $question_answer_record->question_id = $questionId;
                    $answer_array_to_be_saved = [];
                    $answer_array_to_be_saved[0] = $answer;
                    $question_answer_record->answer = json_encode($answer_array_to_be_saved);
                    if ($question_answer_record->save()) {
                        $add_count++;
                    }
                }
//                }
            }
            if ($add_count) {
                return response()->json([
                            'success' => true,
                            'status' => 200,
                            'message' => 'Files Updated'
                ]);
            }
        }
        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function deleteFiles(Request $request, $serviceId, $clientId, $topicId = null) {
        $questionId = $request->input('questionId');
        $deleteFileIndex = $request->input('fileIndex');
        $delete_count = 0;
        if (!empty($questionId) && is_numeric($deleteFileIndex)) {
            $question_answer_record = QuestionAnswers::where(['question_id' => $questionId, 'user_id' => $clientId])->select('id', 'answer')->first();
            if (isset($question_answer_record) && !empty($question_answer_record)) {
                $saved_answer = $question_answer_record->answer;
                $saved_answer_decoded = json_decode($saved_answer, true);
                if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                    if (is_array($saved_answer_decoded)) {
                        unset($saved_answer_decoded[$deleteFileIndex]);
                        $saved_answer_decoded = array_values($saved_answer_decoded);
                        $question_answer_record->answer = json_encode($saved_answer_decoded);
                    }
                }


                if ($question_answer_record->save()) {
                    $delete_count++;
                }
            }

            if ($delete_count) {
                return response()->json([
                            'success' => true,
                            'status' => 200,
                            'message' => 'File Deleted'
                ]);
            }
        }
        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function deleteSingleFile(Request $request, $serviceId, $clientId, $topicId = null) {
        $questionId = $request->input('questionId');
        $answerIndex = $request->input('answerIndex');
        $delete_count = 0;

        if (!empty($questionId) && is_numeric($answerIndex)) {
            $question_answer_record = QuestionAnswers::where(['question_id' => $questionId, 'user_id' => $clientId])->select('id', 'answer')->first();
            if (isset($question_answer_record) && !empty($question_answer_record)) {
                $saved_answer = $question_answer_record->answer;
                $saved_answer_decoded = json_decode($saved_answer, true);

                if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                    if (is_array($saved_answer_decoded)) {
                        unset($saved_answer_decoded[$answerIndex]);
                        $saved_answer_decoded = array_values($saved_answer_decoded);
                        $question_answer_record->answer = json_encode($saved_answer_decoded);
                        if ($question_answer_record->save()) {
                            $delete_count++;
                        }
                    }
                }
            }

            if ($delete_count) {
                return response()->json([
                            'success' => true,
                            'status' => 200,
                            'message' => 'File Deleted'
                ]);
            }
        }
        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function deleteTableData(Request $request) {
        $questionIds = $request->input('questionIds');
        $answerIndex = $request->input('answerIndex');
        $deleteIndex = $request->input('deleteIndex');
        $clientId = $request->input('clientId');
        $delete_count = 0;
        if (!empty($questionIds) && is_numeric($answerIndex) && is_numeric($deleteIndex)) {
            $question_ids_array = explode(',', $questionIds);
            if (isset($question_ids_array) && !empty($question_ids_array)) {
                foreach ($question_ids_array as $single_question) {
                    $question_answer_record = QuestionAnswers::where(['question_id' => $single_question, 'user_id' => $clientId])->select('id', 'answer')->first();
                    if (isset($question_answer_record) && !empty($question_answer_record)) {
                        $saved_answer = $question_answer_record->answer;
                        $saved_answer_decoded = json_decode($saved_answer, true);


                        if (isset($saved_answer_decoded) && !empty($saved_answer_decoded)) {
                            if (is_array($saved_answer_decoded) && isset($saved_answer_decoded[$answerIndex]) && isset($saved_answer_decoded[$answerIndex]['value'])) {
                                $unserialized_answer = unserialize($saved_answer_decoded[$answerIndex]['value']);
                                if (isset($unserialized_answer) && !empty($unserialized_answer)) {
                                    unset($unserialized_answer[$deleteIndex]);
                                    $unserialized_answer = array_values($unserialized_answer);

                                    $serialized_answer = serialize($unserialized_answer);
                                    $saved_answer_decoded[$answerIndex]['value'] = $serialized_answer;
                                    $question_answer_record->answer = json_encode($saved_answer_decoded);
                                    if ($question_answer_record->save()) {
                                        $delete_count++;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if ($delete_count) {
                return response()->json([
                            'success' => true,
                            'status' => 200,
                            'message' => 'Deleted Successfully'
                ]);
            }
        }
        return response()->json([
                    'success' => false,
                    'status' => 200,
                    'message' => 'An Error Occured'
        ]);
    }

    public function getServiceDocuments(Request $request, $serviceId, $clientId) {
        $files_answers = QuestionAnswers::where('user_id', $clientId)->whereHas(
                        'question', function($question) use($serviceId) {
                    $question->where(['service_id' => $serviceId, 'type' => 'file']);
                }
                )->get()->toArray();
        return view('frontend.employee.services.service-questions.viewDocumentsTemplate', compact('files_answers', 'clientId'));
    }

    public function signDocument(Request $request) {

        $body = array('xAdobeSignClientId' => config('constant.adobe_eSignature.CLIENT_ID'));

        return response(json_encode($body), 200)
                        ->header('Content-Type', 'application/json');
    }

    public function documentStatusCallback(Request $request) {
        echo'<pre>';
//        die('testing');
        $code = $request->input('code');
        $api_access_point = $request->input('api_access_point');
        $web_access_point = $request->input('web_access_point');
        if (!empty($code) && !empty($api_access_point) && !empty($web_access_point)) {

            $serviceDocuments = ServiceDocument::where('service_id', 4)->first()->toArray();
            $document_data = json_decode($serviceDocuments['data'], true);
            $ch = curl_init();
            $url = $api_access_point . 'oauth/token?code=' . $code . '&client_id=' . config('constant.adobe_eSignature.CLIENT_ID') . '&client_secret=' . config('constant.adobe_eSignature.CLIENT_SECRET') . '&redirect_uri=' . url('/') . '/documentStatusCallback&grant_type=authorization_code';
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            $server_output = curl_exec($ch);
            curl_close($ch);
            $server_output_decoded = json_decode($server_output, true);
            echo'<PRE>';
            
            echo $url;
            print_R($server_output_decoded);die; 
            if (isset($server_output_decoded['access_token']) && !empty($server_output_decoded['access_token'])) {
                $access_token = $server_output_decoded['access_token'];
                $refresh_token = $server_output_decoded['refresh_token'];
                $upload_document_url = 'api/rest/v6/transientDocuments';
                $agreement_send_url = 'api/rest/v6/agreements';


                $opts = array(
                    "ssl" => array(
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                    ),
                );


                $file_path = config('constant.adobe_eSignature.document_upload_url') . 'ConsultingAgreement.docx';
                $client = new \GuzzleHttp\Client(['base_uri' => $api_access_point]);
                $output = $client->request('POST', $upload_document_url, [
                            'multipart' => [
                                [
                                    'name' => "File",
                                    'contents' => fopen($file_path, 'rb', false, stream_context_create($opts)),
                                    'headers' => [
                                        'Content-Type' => 'multipart/form-data',
                                        'Content-Transfer-Encoding' => "binary",
                                    ]
                                ],
                            ],
                            'headers' => [
                                'Authorization' => "Bearer " . $access_token,
                            ]
                                ]
                        )->getBody()->getContents();
                $transientDocumentResponse = json_decode($output, true);
                $transientDocumentId = $transientDocumentResponse['transientDocumentId'];
                $agreement_data = [
                    'fileInfos' =>
                    [
                        [
                            'transientDocumentId' => $transientDocumentId,
                        ]
                    ],
                    'name' => 'Agreement',
                    'participantSetsInfo' =>
                    [
                        [
                            'memberInfos' =>
                            [
                                [
                                    'email' => $email
                                ]
                            ],
                            'order' => 1,
                            'role' => 'SIGNER',
                        ],
//                        [
//                            'memberInfos' =>
//                            [
//                                [
//                                    'email' => 'simrataarora.luminoguru@gmail.com'
//                                ]
//                            ],
//                            'order' => 1,
//                            'role' => 'SIGNER',
//                        ],
                        [
                            'memberInfos' =>
                            [
                                [
                                    'email' => config('constant.adobe_eSignature.approver_email')
                                ]
                            ],
                            'order' => 2,
                            'role' => 'APPROVER',
                        ]
                    ],
                    'signatureType' => 'ESIGN',
                    'message' => "Please review and sign this document.",
                    'state' => 'AUTHORING',
                ];
                $json_array = json_encode($agreement_data);
                $output_agreement = $client->request('POST', $agreement_send_url, [
                            \GuzzleHttp\RequestOptions::JSON => $agreement_data,
                            'headers' => [
                                'Authorization' => "Bearer " . $access_token,
                                'Content-Type' => 'application/json',
                            ]
                                ]
                        )->getBody()->getContents();
                $output_agreement_decoded = json_decode($output_agreement, true);
                if (isset($output_agreement_decoded['id']) && !empty($output_agreement_decoded['id'])) {

//                    die;
                    $output_agreement_decoded_id = $output_agreement_decoded['id'];
                    sleep(5);
                    $agreement_get_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';

                    $agreement_get_url = $api_access_point . $agreement_get_url;
                    $agreeement_data_adobe = $client->request('GET', $agreement_get_url, [
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                        ]
                    ]);
                    $etag = $agreeement_data_adobe->getHeader('ETag')[0];









                    $agreement_member_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/members';

                    $agreement_member_url = $api_access_point . $agreement_member_url;
                    $agreeement_member_data = $client->request('GET', $agreement_member_url, [
                                'headers' => [
                                    'Authorization' => "Bearer " . $access_token,
                                ]
                            ])->getBody()->getContents();
                    $agreeement_member_data_decoded = json_decode($agreeement_member_data, true);


//                    die;
//                    $approver_id = $agreeement_member_data_decoded['participantSets'][0]['id'];
//                    $participant_id = $agreeement_member_data_decoded['participantSets'][1]['id'];
                    foreach ($agreeement_member_data_decoded['participantSets'] as $single) {
                        if ($single['role'] == 'SIGNER')
                            $participant_id = $single['id'];
                        if ($single['role'] == 'APPROVER')
                            $approver_id = $single['id'];
                    }


//die;






                    $agreement_fields_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';
                    $fields_array = [];
                    foreach ($document_data as $single) {
                        if (isset($single['is_approver']) && !empty($single['is_approver'])) {
                            $single['assignee'] = $approver_id;
                            unset($single['is_approver']);
                        } else {
                            $single['assignee'] = $participant_id;
                            if (isset($single['name']) && !empty($single['name']) && $single['name'] == 'client_email') {
                                $single['defaultValue'] = $email;
                            }
                            if (isset($single['displayFormatType']) && !empty($single['displayFormatType']) && $single['displayFormatType'] == 'DATE') {
                                $single['defaultValue'] = date('d/m/Y');
                            }
                        }
                        $fields_array[] = $single;
                    }

                    $fields_array = [
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 5,
                                    'top' => 420,
                                    'left' => 85,
                                    'width' => 60,
                                    'height' => 23,
                                ]
                            ],
                            'name' => 'client_reciept',
                            'inputType' => 'SIGNATURE',
                            'contentType' => 'SIGNER_INITIALS',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            "required" => 1,
                        ], [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 660,
                                    'left' => 120,
                                    'width' => 145,
                                    'height' => 30,
                                ]
                            ],
                            'name' => 'client_signature',
                            'inputType' => 'SIGNATURE',
                            'contentType' => 'SIGNATURE',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 660,
                                    'left' => 360,
                                    'width' => 145,
                                    'height' => 30,
                                ]
                            ],
                            'name' => 'advisor_signature',
                            'inputType' => 'SIGNATURE',
                            'contentType' => 'SIGNATURE',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 595,
                                    'left' => 120,
                                    'width' => 145,
                                    'height' => 30,
                                ]
                            ],
                            'name' => 'trustee_signature',
                            'inputType' => 'SIGNATURE',
                            'contentType' => 'SIGNATURE',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 510,
                                    'left' => 150,
                                    'width' => 300,
                                    'height' => 22,
                                ]
                            ],
                            'name' => 'client_full_name',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 485,
                                    'left' => 150,
                                    'width' => 300,
                                    'height' => 22,
                                ]
                            ],
                            'name' => 'client_full_name2',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 460,
                                    'left' => 150,
                                    'width' => 300,
                                    'height' => 22,
                                ]
                            ],
                            'name' => 'client_street_address',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 435,
                                    'left' => 150,
                                    'width' => 120,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_city',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 435,
                                    'left' => 335,
                                    'width' => 90,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_state',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 435,
                                    'left' => 475,
                                    'width' => 75,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_zip',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 410,
                                    'left' => 150,
                                    'width' => 120,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_cell_phone1',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 410,
                                    'left' => 335,
//                                   310
                                    'width' => 200,
//                                   250
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_email',
                            'inputType' => 'TEXT_FIELD',
                            'contentType' => 'SIGNER_EMAIL',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 385,
                                    'left' => 335,
//                                   310
                                    'width' => 200,
//                                   250
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'client_email2',
                            'inputType' => 'TEXT_FIELD',
//                            'contentType' => 'SIGNER_EMAIL',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 385,
                                    'left' => 150,
                                    'width' => 120,
                                    'height' => 22,
                                ]
                            ],
                            'name' => 'client_cell_phone2',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 6,
                                    'top' => 360,
                                    'left' => 150,
                                    'width' => 120,
                                    'height' => 22,
                                ]
                            ],
                            'name' => 'home_phone',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 600,
                                    'left' => 250,
                                    'width' => 200,
                                    'height' => 15,
                                ]
                            ],
                            'name' => 'personal_info_1',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 580,
                                    'left' => 250,
                                    'width' => 200,
                                    'height' => 15,
                                ]
                            ],
                            'name' => 'personal_info_2',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 560,
                                    'left' => 250,
                                    'width' => 200,
                                    'height' => 15,
                                ]
                            ],
                            'name' => 'personal_info_3',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 540,
                                    'left' => 250,
                                    'width' => 200,
                                    'height' => 15,
                                ]
                            ],
                            'name' => 'personal_info_4',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 490,
                                    'left' => 350,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_1',
                            'validation' => 'NUMBER',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 470,
                                    'left' => 410,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_2',
                            'validation' => 'NUMBER',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 450,
                                    'left' => 300,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_3',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 435,
                                    'left' => 410,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_4',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 415,
                                    'left' => 410,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_5',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 400,
                                    'left' => 280,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_6',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 385,
                                    'left' => 350,
                                    'width' => 100,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'income_info_7',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 10,
                                    'top' => 325,
                                    'left' => 230,
                                    'width' => 60,
                                    'height' => 13,
                                ]
                            ],
                            'name' => 'negotiated_rate',
                            'validation' => 'NUMBER',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 670,
                                    'left' => 270,
                                    'width' => 180,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'third_party_account1',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 640,
                                    'left' => 270,
                                    'width' => 180,
                                    'height' => 25,
                                ]
                            ],
                            'name' => 'third_party_address1',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 607,
                                    'left' => 270,
                                    'width' => 180,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'third_party_telephone1',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 535,
                                    'left' => 270,
                                    'width' => 150,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'third_party_account2',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 500,
                                    'left' => 270,
                                    'width' => 150,
                                    'height' => 25,
                                ]
                            ],
                            'name' => 'third_party_address2',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 11,
                                    'top' => 470,
                                    'left' => 270,
                                    'width' => 150,
                                    'height' => 20,
                                ]
                            ],
                            'name' => 'third_party_telephone2',
                            'inputType' => 'TEXT_FIELD',
                            'validation' => 'NUMBER',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 275,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 255,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 240,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 220,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 200,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'investment_objective',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "The next two years",
                                "Three to five years",
                                "Six to ten years",
                                "Ten to fifteen years",
                                "More than fifteen years"
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 7,
                                    'top' => 150,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 130,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 7,
                                    'top' => 110,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'beyond_10_years',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "Very important that my investments grow in value",
                                "Somewhat important that my investments grow in value",
                                "Unimportant whether or not my investments grow in value",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 705,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 685,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 670,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 650,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'knowledge_of_investments',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "None",
                                "Limited",
                                "Good",
                                "Extensive",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 595,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 565,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 535,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'how_to_invest_money,',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "losing value",
                                "losing or gaining value",
                                "gaining value",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 465,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 450,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 435,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 420,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'investment_returns',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "Very concerned about short-term losses",
                                "Somewhat concerned about short-term losses",
                                "Only mildly concerned about short-term losses",
                                "Unconcerned about short-term losses",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 350,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'owned_in_past1',
                            'inputType' => 'CHECKBOX',
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            'required' => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 330,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'owned_in_past2',
                            'inputType' => 'CHECKBOX',
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            'required' => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 315,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'owned_in_past3',
                            'inputType' => 'CHECKBOX',
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            'required' => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 300,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'owned_in_past4',
                            'inputType' => 'CHECKBOX',
                            'fontSize' => '12',
                            'alignment' => 'RIGHT',
                            'required' => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 8,
                                    'top' => 235,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 215,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 200,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 8,
                                    'top' => 185,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'markets_dropped_by_25%',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "Sell all my shares",
                                "Sell some of my shares",
                                "Keep all my shares",
                                "Buy more shares",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 9,
                                    'top' => 670,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 9,
                                    'top' => 600,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                                [
                                    'pageNumber' => 9,
                                    'top' => 530,
                                    'left' => 130,
                                    'width' => 11,
                                    'height' => 11,
                                ],
                            ],
                            'name' => 'portfolio ',
                            'inputType' => 'RADIO',
                            'displayLabel' => '',
                            'visible' => true,
                            'radioCheckType' => 'CIRCLE',
                            'displayFormat' => '',
                            'displayFormatType' => 'DEFAULT',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                            "masked" => false,
                            "maskingText" => "*",
                            "hiddenOptions" => [
                                "Portfolio A",
                                "Portfolio B",
                                "Portfolio C",
                            ],
                            "conditionalAction" => [
                                "anyOrAll" => "ANY",
                                "action" => "SHOW"
                            ],
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 80,
                                    'width' => 60,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'account_number',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 160,
                                    'width' => 110,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'registration_name',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 280,
                                    'width' => 90,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'custodian_name',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 380,
                                    'width' => 40,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'fee',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 430,
                                    'width' => 25,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'discrtion',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 460,
                                    'width' => 20,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'non_discrtion',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 490,
                                    'width' => 15,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'debit',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                        [
                            'locations' =>
                            [
                                [
                                    'pageNumber' => 12,
                                    'top' => 500,
                                    'left' => 510,
                                    'width' => 15,
                                    'height' => 19,
                                ]
                            ],
                            'name' => 'bill',
                            'inputType' => 'TEXT_FIELD',
                            'assignee' => $participant_id,
                            'fontSize' => '12',
                            'alignment' => 'LEFT',
                            "required" => 1,
                        ],
                    ];
                    $agreement_fields_data = [
                        'fields' =>
                        $fields_array
                    ];
                    $json_array = json_encode($agreement_fields_data);

                    $agreement_fields_output = $client->request('PUT', $agreement_fields_url, [
                                \GuzzleHttp\RequestOptions::JSON => $agreement_fields_data,
                                'headers' => [
                                    'Authorization' => "Bearer " . $access_token,
                                    'Content-Type' => 'application/json',
                                    'If-Match' => $etag
                                ]
                                    ]
                            )->getBody()->getContents();
                    print_R(json_decode($agreement_fields_output));
                    die;
                }
                die;
            }
        }
    }

    protected function _sendDocumentForSigning($client_id, $document_name, $document_data) {
        $user_account_info = UserAccountInfo::where('user_id', $client_id)->first()->toArray();


        $api_access_point = config('constant.adobe_eSignature.api_access_point');
//
        $client = new \GuzzleHttp\Client(['base_uri' => $api_access_point]);
        $refresh_token = config('constant.adobe_eSignature.REFRESH_TOKEN');

//
        $refresh_token_data = $client->request('POST', '/oauth/refresh', [
                    'form_params' => [
                        'grant_type' => 'refresh_token',
                        'client_id' => config('constant.adobe_eSignature.CLIENT_ID'),
                        'client_secret' => config('constant.adobe_eSignature.CLIENT_SECRET'),
                        'refresh_token' => $refresh_token
                    ]
                ])->getBody()->getContents();
        $refresh_token_data = json_decode($refresh_token_data, true);
        $access_token = $refresh_token_data['access_token'];

        $upload_document_url = 'api/rest/v6/transientDocuments';
        $agreement_send_url = 'api/rest/v6/agreements';


        $opts = array(
            "ssl" => array(
                "verify_peer" => false,
                "verify_peer_name" => false,
            ),
        );

        $file_path = config('constant.adobe_eSignature.document_upload_url') . $document_name;

        $output = $client->request('POST', $upload_document_url, [
                    'multipart' => [
                        [
                            'name' => "File",
                            'contents' => fopen($file_path, 'rb', false, stream_context_create($opts)),
                            'headers' => [
                                'Content-Type' => 'multipart/form-data',
                                'Content-Transfer-Encoding' => "binary",
                            ]
                        ],
                    ],
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                    ]
                        ]
                )->getBody()->getContents();

        $transientDocumentResponse = json_decode($output, true);
        $transientDocumentId = $transientDocumentResponse['transientDocumentId'];
        $user = User::where('id', $client_id)->first();
        $email = $user->email;
        $agreement_data = [
            'fileInfos' =>
            [
                [
                    'transientDocumentId' => $transientDocumentId,
                ]
            ],
            'name' => 'Agreement',
            'participantSetsInfo' =>
            [
                [
                    'memberInfos' =>
                    [
                        [
                            'email' => $email
                        ]
                    ],
                    'order' => 1,
                    'role' => 'SIGNER',
                ],
                [
                    'memberInfos' =>
                    [
                        [
                            'email' => config('constant.adobe_eSignature.approver_email')
                        ]
                    ],
                    'order' => 2,
                    'role' => 'APPROVER',
                ]
            ],
            'signatureType' => 'ESIGN',
            'message' => "Please review and sign this document.",
            'state' => 'AUTHORING',
        ];
        $json_array = json_encode($agreement_data);
        $output_agreement = $client->request('POST', $agreement_send_url, [
                    \GuzzleHttp\RequestOptions::JSON => $agreement_data,
                    'headers' => [
                        'Authorization' => "Bearer " . $access_token,
                        'Content-Type' => 'application/json',
                    ]
                        ]
                )->getBody()->getContents();
        $output_agreement_decoded = json_decode($output_agreement, true);
        if (isset($output_agreement_decoded['id']) && !empty($output_agreement_decoded['id'])) {

            $output_agreement_decoded_id = $output_agreement_decoded['id'];
            sleep(5);
            $agreement_get_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';

            $agreement_get_url = $api_access_point . $agreement_get_url;
            $agreeement_data_adobe = $client->request('GET', $agreement_get_url, [
                'headers' => [
                    'Authorization' => "Bearer " . $access_token,
                ]
            ]);
            $etag = $agreeement_data_adobe->getHeader('ETag')[0];









            $agreement_member_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/members';

            $agreement_member_url = $api_access_point . $agreement_member_url;
            $agreeement_member_data = $client->request('GET', $agreement_member_url, [
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                        ]
                    ])->getBody()->getContents();
            $agreeement_member_data_decoded = json_decode($agreeement_member_data, true);
            foreach ($agreeement_member_data_decoded['participantSets'] as $single) {
                if ($single['role'] == 'SIGNER')
                    $participant_id = $single['id'];
                if ($single['role'] == 'APPROVER')
                    $approver_id = $single['id'];
            }




            if (isset($user_account_info) && !empty($user_account_info)) {
                $third_party_data = $user_account_info['third_party_data'];
                $third_party_data_decoded = json_decode($third_party_data, true);
                $accounts_data = $user_account_info['accounts_data'];
                $accounts_data_decoded = json_decode($accounts_data, true);
            }





            $agreement_fields_url = 'api/rest/v6/agreements/' . $output_agreement_decoded_id . '/formFields';
            $fields_array = [];
            foreach ($document_data as $single) {
                if (isset($single['is_approver']) && !empty($single['is_approver'])) {
                    $single['assignee'] = $approver_id;
                    unset($single['is_approver']);
                } else {
                    $single['assignee'] = $participant_id;
                    if (isset($single['name']) && !empty($single['name']) && $single['name'] == 'client_email') {
                        $single['defaultValue'] = $email;
                    }
                    if (isset($single['displayFormatType']) && !empty($single['displayFormatType']) && $single['displayFormatType'] == 'DATE') {
                        $single['defaultValue'] = date('d/m/Y');
                    }

                    if (in_array($document_name, ['TWPMAgreementAmendmentandUpdate.docx', 'EmployerPlans.docx', 'IACTemplateVersion4-10-18.docx'])) {
                        switch ($single['name']) {
                            case 'third_party_account1':
                                $single['defaultValue'] = @$third_party_data_decoded[0]['authorized_third_party'];
                                break;
                            case 'third_party_address1':
                                $single['defaultValue'] = @$third_party_data_decoded[0]['mailing_address'];
                                break;
                            case 'third_party_telephone1':
                                $single['defaultValue'] = @$third_party_data_decoded[0]['telephone'];
                                break;
                            case 'third_party_account2':
                                $single['defaultValue'] = @$third_party_data_decoded[1]['authorized_third_party'];
                                break;
                            case 'third_party_address2':
                                $single['defaultValue'] = @$third_party_data_decoded[1]['mailing_address'];
                                break;
                            case 'third_party_telephone2':
                                $single['defaultValue'] = @$third_party_data_decoded[1]['telephone'];
                                break;
                            case 'account_number':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['account_number'];
                                break;
                            case 'registration_name':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['registration_name'];
                                break;
                            case 'custodian_name':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['custodian_name'];
                                break;
                            case 'fee':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['fee'];
                                break;
                            case 'discrtion':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['discrtion'];
                                break;
                            case 'non_discrtion':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['non_discrtion'];
                                break;
                            case 'debit':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['debit'];
                                break;
                            case 'bill':
                                $single['defaultValue'] = @$accounts_data_decoded[0]['bill'];
                                break;

                            default:
                                break;
                        }
                    }
                }
                $fields_array[] = $single;
            }



            foreach ($accounts_data_decoded as $key => $single) {
                if ($key == 0)
                    continue;
                if (isset($single) && !empty($single)) {
                    switch ($document_name) {
                        case 'TWPMAgreementAmendmentandUpdate.docx':
                        case 'EmployerPlans.docx':
                        case 'IACTemplateVersion4-10-18.docx':
                            $pageNumber = 12;
                            if ($document_name == 'TWPMAgreementAmendmentandUpdate.docx')
                                $pageNumber = 3;

                            $new_account_number = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 80,
                                        'width' => 60,
                                        'height' => 19
                                    ]],
                                'name' => 'account_number_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['account_number']
                            ];
                            $new_registration_name = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 160,
                                        'width' => 110,
                                        'height' => 19
                                    ]],
                                'name' => 'registration_name_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['registration_name']
                            ];
                            $new_custodian_name = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 280,
                                        'width' => 90,
                                        'height' => 19
                                    ]],
                                'name' => 'custodian_name_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['custodian_name']
                            ];
                            $new_fee = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 380,
                                        'width' => 40,
                                        'height' => 19
                                    ]],
                                'name' => 'fee_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['fee']
                            ];
                            $new_discrtion = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 430,
                                        'width' => 25,
                                        'height' => 19
                                    ]],
                                'name' => 'discrtion_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['discrtion']
                            ];
                            $new_non_discrtion = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 460,
                                        'width' => 20,
                                        'height' => 19
                                    ]],
                                'name' => 'non_discrtion_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['non_discrtion']
                            ];
                            $new_debit = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 490,
                                        'width' => 15,
                                        'height' => 19
                                    ]
                                ],
                                'name' => 'debit_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['debit']
                            ];
                            $new_bill = [
                                'locations' => [
                                    [
                                        'pageNumber' => $pageNumber,
                                        'top' => 500 - $key * 30,
                                        'left' => 510,
                                        'width' => 15,
                                        'height' => 19
                                    ]
                                ],
                                'name' => 'bill_' . $key,
                                'inputType' => 'TEXT_FIELD',
                                'assignee' => $participant_id,
                                'fontSize' => 12,
                                'alignment' => 'LEFT',
                                'required' => 1,
                                'readOnly' => 1,
                                'defaultValue' => $single['bill']
                            ];
                            array_push($fields_array, $new_account_number, $new_registration_name, $new_custodian_name, $new_fee, $new_discrtion, $new_non_discrtion, $new_debit, $new_bill);

                            break;


                        default:
                            break;
                    }
                }
            }


            $agreement_fields_data = [
                'fields' =>
                $fields_array
            ];
            $json_array = json_encode($agreement_fields_data);

            $agreement_fields_output = $client->request('PUT', $agreement_fields_url, [
                        \GuzzleHttp\RequestOptions::JSON => $agreement_fields_data,
                        'headers' => [
                            'Authorization' => "Bearer " . $access_token,
                            'Content-Type' => 'application/json',
                            'If-Match' => $etag
                        ]
                            ]
                    )->getBody()->getContents();
        }
        return true;
    }

    public function userAccountInfo(Request $request) {
//        echo'<PRE>';
        $answer = $request->input('answer');
        $client_id = $request->input('client_id');
        if (isset($answer) && !empty($answer) && isset($answer['third_party_data']) && !empty($answer['third_party_data']) && (isset($answer['accounts_data'])) && !empty($answer['accounts_data'])) {
            $userAccountInfo = UserAccountInfo::where('user_id', $client_id)->first();
            if (empty($userAccountInfo)) {
                $userAccountInfo = new UserAccountInfo;
            }
            $userAccountInfo->third_party_data = $answer['third_party_data'];
            $userAccountInfo->accounts_data = $answer['accounts_data'];
            $userAccountInfo->user_id = $client_id;
            $userAccountInfo->save();
            $serviceRecentlyPurchasedByUser = ServiceUser::whereIn('service_id', config('constant.questions.services_containing_select_inputs'))->where(['user_id' => $client_id, 'status' => 1])->orderBy('updated_at', 'desc')->first();
            if (isset($serviceRecentlyPurchasedByUser) && !empty($serviceRecentlyPurchasedByUser)) {

                if ($serviceRecentlyPurchasedByUser->count() > 0) {
                    $serviceRecentlyPurchasedByUser = $serviceRecentlyPurchasedByUser->toArray();
                    $serviceDocuments = ServiceDocument::where('service_id', $serviceRecentlyPurchasedByUser['service_id'])->orderBy('document_name', 'desc')->get()->toArray();
                    if (isset($serviceDocuments) && !empty($serviceDocuments)) {
                        foreach ($serviceDocuments as $single_document) {
                            if (isset($single_document['document_name']) && !empty($single_document['document_name'])) {
                                $document_name = $single_document['document_name'];
                                $document_data = json_decode($single_document['data'], true);
                                $this->_sendDocumentForSigning($client_id, $document_name, $document_data);
                            }
                        }
                    }

                    return redirect()->back()->withFlashSuccess('Documents Sent For Signature Successfuly');
                }
            }
        }
        return redirect()->back();
    }

}
