<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Events\Frontend\Auth\UserRegistered;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Requests\Frontend\Auth\RegisterRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Models\Backend\Service;
use Session;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Frontend\Auth
 */
class RegisterController extends Controller {

    use RegistersUsers;

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * RegisterController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        // Where to redirect users after registering
        $this->redirectTo = route('frontend.index');

        $this->user = $user;
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    /**
     * @param RegisterRequest $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function register(RegisterRequest $request)
    {
        if (!(Service::where('id', $request->serviceId)->exists())) {
            return redirect()->route('frontend.services');
        }

//		if (config('access.users.confirm_email')) {
        $user = $this->user->create($request->all());
        if (!$user) {
            return redirect()->back()->withFlashDanger("Oops! Something went wrong.");
        }
        event(new UserRegistered($user));
        $request->session()->put('userId', $user->id);
        //saving client service
        if ($user->hasRole(config('access.users.default_role'))) {
            $user->services()->attach([$request->serviceId => ['status' => 1]]);
        }

        return redirect()->route('frontend.client.signupStepTwo', config('constant.subdomain'));
//		} 
    }

}
