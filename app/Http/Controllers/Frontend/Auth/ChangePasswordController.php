<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\ChangePasswordRequest;
use App\Repositories\Frontend\Access\User\UserRepository;
use Response;

/**
 * Class ChangePasswordController
 * @package App\Http\Controllers\Frontend\Auth
 */
class ChangePasswordController extends Controller {

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ChangePasswordController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }

    /**
     * @param ChangePasswordRequest $request
     * @return mixed
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $changePassword = $this->user->changePassword($request->all());
        if ($request->ajax()) {
            if ($changePassword) {
                return Response::json(['message' => trans('strings.frontend.user.password_updated')], 200);
            }
            return Response::json(['message' => trans('exceptions.frontend.auth.password.change_mismatch')], 500);
        }
        if ($changePassword) {
            return redirect()->route('frontend.user.account')->withFlashSuccess(trans('strings.frontend.user.password_updated'));
        }
        throw new GeneralException(trans('exceptions.frontend.auth.password.change_mismatch'));
    }

}
