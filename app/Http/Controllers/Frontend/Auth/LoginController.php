<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Helpers\Auth\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use App\Helpers\Frontend\Auth\Socialite;
use App\Events\Frontend\Auth\UserLoggedIn;
use App\Events\Frontend\Auth\UserLoggedOut;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Profile;
use App\Models\OneTimePassword;
use Session;
use Twilio;
//footer
use App\Models\Backend\Footer;
/* Contact */
use App\Models\Backend\Contact;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller {

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login
     * @return string
     */
    public function redirectPath() {
        if (access()->allow('view-backend') && access()->hasRole('access.roles.admin_role')) {
            return route('admin.dashboard');
        }
        if (access()->hasRole(config('access.roles.employee_role'))) {
            return route('frontend.employee.allClients');
        }
        return route('frontend.client.dashboard', config('constant.subdomain'));
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm() {
//           die('showLoginForm');
        return view('frontend.auth.login')->with(['footer' => Footer::first(), 'contact' => Contact::first()]);
    }

    /**
     * @param Request $request
     * @param $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws GeneralException
     */
    protected function authenticated(Request $request, $user) {
        /**
         * Check to see if the users account is confirmed and active
         */
        if (config('access.users.confirm_email')) {
            if (!$user->isConfirmed()) {
                access()->logout();
                throw new GeneralException(trans('exceptions.frontend.auth.confirmation.resend', ['user_id' => $user->id]));
            } elseif (!$user->isActive()) {
                access()->logout();
                throw new GeneralException(trans('exceptions.frontend.auth.deactivated'));
            }
        }
//        event(new UserLoggedIn($user));
    }

    /**
     * Log the user out of the application.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {
        /**
         * Boilerplate needed logic
         */
        /**
         * Remove the socialite session variable if exists
         */
        if (app('session')->has(config('access.socialite_session_name'))) {
            app('session')->forget(config('access.socialite_session_name'));
        }

        /**
         * Remove any session data from backend
         */
        app()->make(Auth::class)->flushTempSession();

        /**
         * Fire event, Log out user, Redirect
         */
//        event(new UserLoggedOut($this->guard()->user()));
        /**
         * Laravel specific logic
         */
        $this->guard()->logout();
        $request->session()->flush();
        $request->session()->regenerate();

        return redirect('/');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logoutAs() {
        //If for some reason route is getting hit without someone already logged in
        if (!access()->user()) {
            return redirect()->route("frontend.auth.login");
        }

        //If admin id is set, relogin
        if (session()->has("admin_user_id") && session()->has("temp_user_id")) {
            //Save admin id
            $admin_id = session()->get("admin_user_id");

            app()->make(Auth::class)->flushTempSession();

            //Re-login admin
            access()->loginUsingId((int) $admin_id);

            //Redirect to backend user page
            return redirect()->route("admin.access.user.index");
        } else {
            app()->make(Auth::class)->flushTempSession();

            //Otherwise logout and redirect to login
            access()->logout();
            return redirect()->route("frontend.auth.login");
        }
    }

    /**
     * Genrate login otp
     * @param type $userId
     * @param type $phoneNumber
     * @return boolean
     */
    private function _generateOtp($userId, $phoneNumber) {
        $otp = mt_rand(111111, 999999);
        $one_time_password = new OneTimePassword;
        $one_time_password->user_id = $userId;
        $one_time_password->phone_number = $phoneNumber;
        $one_time_password->otp = $otp;

        $message = trans('alerts.frontend.client.loginOtp') . $otp;

        //expiring all previous OTP's active for the user 
        OneTimePassword::where('user_id', $userId)->update(['active' => 0]);
        //sending message and saving otp and saving mobile number in profile table
        if ((Twilio::message(env('SMS_COUNTRY_PHONE_CODE') . $phoneNumber, $message)) && $one_time_password->save()) {
            return true;
        }
        return false;
    }

    /**
     * Verufy OTP and login user to dashboard
     * @param Request $request
     * @return type
     */
    public function verifyLoginOtp(Request $request) {
        $data = $request->all();

        if (isset($data['userId']) && isset($data['mobileNumber']) && isset($data['otp']) && !empty($data['userId']) && !empty($data['mobileNumber']) && !empty($data['otp'])) {

            $profile = Profile::where('user_id', $data['userId'])->where('mobile_number', $data['mobileNumber'])->first(['first_name', 'last_name']);

            if ($profile && (OneTimePassword::where('user_id', $data['userId'])->where('phone_number', $data['mobileNumber'])->where('otp', $data['otp'])->where('active', 1)->first()) && (OneTimePassword::where('user_id', $data['userId'])->update(['active' => 0])) && (\Auth::loginUsingId($data['userId'], true))) {
                Session::set('loggedInUserName', $profile->first_name . ' ' . $profile->last_name);

                if (isset($request->serviceId) && !empty($request->serviceId)) {

                    Session::set('selectedServiceId', $request->serviceId);
                }
                return response()->json(['route' => $this->redirectPath()], 200);
            }
        }
        return response()->json(['message' => trans('alerts.frontend.client.invalidOtp')], 500);
    }

    public function login(Request $request) {
        $this->validateLogin($request);

        /** If the class is using the ThrottlesLogins trait, we can automatically throttle
          the login attempts for this application. We'll key this by the username and
          the IP address of the client making these requests into this application. */
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if (\Auth::once(['email' => $request->email, 'password' => $request->password])) {
            $user = $this->guard()->user();
            if ($user->status == 1) {
                return $this->sendLoginResponse($request, $this->guard()->user());
            } else {
                return $this->sendAccountDeactivatedResponse($request, $this->guard()->user());
            }
        }

        /** If the login attempt was unsuccessful we will increment the number of attempts
          to login and redirect the user back to the login form. Of course, when this
          user surpasses their maximum number of attempts they will get locked out. */
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * ajax response functions to send Login success Response
     * @param Request $request
     * @return type
     */
    protected function sendLoginResponse(Request $request, $user) {
        if ($request->ajax()) {
            if (!$user->isConfirmed()) {
                return response()->json(['message' => trans('exceptions.frontend.auth.confirmation.resend', ['user_id' => $user->id]), 'route' => route('frontend.auth.account.confirm.resend', $user->id), 'click_here' => trans('exceptions.frontend.auth.confirmation.click_here')], 500);
            } elseif (!$user->isActive()) {
                return response()->json(['message' => trans('exceptions.frontend.auth.deactivated')], 500);
            }
            if ($user->hasRole(config('access.users.default_role'))) {

                $profile = Profile::where('user_id', $user->id)->first(['profile_complete_step', 'first_name', 'last_name', 'mobile_number']);

                if ($profile && $user->isTwoFactorAuth() && !empty($profile->mobile_number)) {
                    if ($this->_generateOtp($user->id, $profile->mobile_number)) {
                        return response()->json(['message' => trans('exceptions.frontend.auth.confirmation.twoFactorAuth'), 'status' => '1', 'userId' => $user->id, 'mobileNumber' => $profile->mobile_number], 200);
                    } else {
                        return response()->json(['message' => trans('alerts.frontend.client.otpFail')], 500);
                    }
                }

                \Auth::login($user);
                $this->clearLoginAttempts($request);

                if ($profile) {
                    if (isset($profile->first_name)) {
                        Session::set('loggedInUserName', $profile->first_name . ' ' . $profile->last_name);
                    } else {
                        Session::set('loggedInUserName', $user->name);
                    }
                }

                if (isset($request->serviceId) && !empty($request->serviceId)) {
                    Session::set('selectedServiceId', $request->serviceId);
                }
                return response()->json(['route' => $this->redirectPath()], 200);
            } elseif (access()->hasRole(config('access.roles.employee_role'))) {
                return response()->json(['route' => $this->redirectPath()], 200);
            }
        }
      \Auth::login($user);
        $this->clearLoginAttempts($request);
        $profile = Profile::where('user_id', $user->id)->first(['first_name', 'last_name']);
        if (!empty($profile) && isset($profile->first_name)) {
            Session::set('loggedInUserName', $profile->first_name . ' ' . $profile->last_name);
        } else {
            Session::set('loggedInUserName', $user->name);
        }

        return $this->authenticated($request, $this->guard()->user()) ?: redirect()->intended($this->redirectPath());
    }

    /**
     * ajax response functions to send Login fail Response
     * @param Request $request
     * @return type
     */
    protected function sendFailedLoginResponse(Request $request) {
        if ($request->ajax()) {
            return response()->json(['message' => trans('auth.failed')], 500);
        }
        return redirect()->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors([$this->username() => trans('auth.failed'),
        ]);
    }

    protected function sendLockoutResponse(Request $request) {
        $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
        );

        $message = trans('auth.throttle', ['seconds' => $seconds]);

        if ($request->ajax()) {
            return response()->json([
                        'error' => $message
                            ], 401);
        }

        return redirect()->back()
                        ->withInput($request->only($this->username(), 'remember'))
                        ->withErrors([$this->username() => $message]);
    }

    protected function sendAccountDeactivatedResponse(Request $request, $user) {
        if ($request->ajax()) {
            return response()->json(['message' => trans('auth.failed')], 500);
        }
        return redirect()->back()
                        ->withErrors([$this->username() => 'Your Account is Deactivated.'
        ]);
    }

}
