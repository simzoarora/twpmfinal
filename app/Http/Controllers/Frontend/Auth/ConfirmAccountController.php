<?php

namespace App\Http\Controllers\Frontend\Auth;

use Mail;
use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Frontend\Access\User\UserRepository;
use App\Notifications\Frontend\Auth\UserNeedsConfirmation;
use Illuminate\Support\Facades\Auth;
use App\Models\Access\Role\Role;
use Illuminate\Support\Facades\DB;

/**
 * Class ConfirmAccountController
 * @package App\Http\Controllers\Frontend\Auth
 */
class ConfirmAccountController extends Controller {

    /**
     * @var UserRepository
     */
    protected $user;

    /**
     * ConfirmAccountController constructor.
     * @param UserRepository $user
     */
    public function __construct(UserRepository $user) {
        $this->user = $user;
    }

    /**
     * @param $token
     * @return mixed
     */
    public function confirm($token) {
//        Auth::logout();
        $route = 'frontend.auth.login';
        $user = User::where('confirmation_code', $token)->first();
        $info = $this->user->confirmAccount($token);
        if (!Auth::guest()) {
            $role_user = DB::table('role_user')
                    ->select(DB::raw('*'))
                    ->where('user_id', Auth::user()->id)
                    ->first();
            switch ($role_user->role_id) {
                case '2':
                    $route = 'frontend.employee.allClients';
                    break;
                case '3':
                    $route = 'frontend.client.dashboard';
                    break;
                default:
                    Auth::logout();
                    $route = 'frontend.auth.login';
                    break;
            }
            if ($user->id != Auth::id()) {
                $route = 'frontend.auth.login';
                Auth::logout();
            }
        }
        if ($info == 'already_confirmed') {
            return redirect()->route($route, config('constant.subdomain'))->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.already_confirmed'));
        } elseif ($info == 'mismatch') {
            return redirect()->route($route, config('constant.subdomain'))->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.mismatch'));
        } else {
            
        }
        // send welcome email to user
        Mail::send('frontend.emails.welcomeEmail', ['user' => $user], function($message) use($user) {
            $message->to($user->email)->subject('Thank you for setting up your account.');
        });
        return redirect()->route($route, config('constant.subdomain'))->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.success'));
    }

    /**
     * @param $user
     * @return mixed
     */
    public function sendConfirmationEmail(User $user) {
        $user->notify(new UserNeedsConfirmation($user->confirmation_code));
        return redirect()->route('frontend.auth.login')->withFlashSuccess(trans('exceptions.frontend.auth.confirmation.resent'));
    }

}
