<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Models\DocumentType;
use App\Http\Controllers\Controller;
use Auth;
use App\Services\FileUpload;
use App\Services\Unisharp;
use App\Models\Notification;
use App\Models\Notification_data;
use App\Models\Receiver;
use App\Models\Notify;
use App\Models\Client_Document;
use App\Models\Notification_room;
use App\Events\RoomEvents;
use App\Events\PublicNotification;
use App\Events\NotificationEvent;
use App\Events\PrivateNotificationEvent;
use App\Models\Access\User\User;
//use LRedis;
use App\Events\PrivateEvent;
use Carbon\Carbon;
use App\Mail\MailNotify;
use Mail;
use File;

class DocumentController extends Controller {

public function __construct() {
$this->middleware('auth');
}

//save Document 
public function saveDocument(Request $request) {
// echo'<PRE>';print_R($request->all());die;
$docuementCategoryId = $request->category;
$documentDiscription = $request->description;
$isNotified = $request->notify_client;
$clientId = $request->client_id;
$file = $request->filedata;
$fileOriginalName = $request->original_name;
$userInfo = User::where('id', $clientId)->with('profile')->first();
$dirUniSharp = 'files' . '/' . $clientId;
$fileName = Unisharp::uploadFile($file, 'uploadedClientDocuments', $dirUniSharp);
$name = "";
if ($userInfo->name != '') {
$name = $userInfo->name;
} else {
$name = $userInfo->profile->first_name;
}
$text = "Hi " . $name . " Your " . $fileName . " is successfully added to your profile";
if ($isNotified == "true") {
$email = User::find($clientId)->email;
Mail::send(new MailNotify($name, $email, $fileOriginalName));
$senderId = auth()->user()->id;
$receiverId = $request->client_id;
$notificationData = new Notification_data;
$notificationData->notification_room_id = $request->notification_room_id;
$notificationData->sender_id = $senderId;
$notificationData->notification = $text;
$notificationData->receiver_id = $receiverId;
$notificationData->is_read = 0;
$notificationData->save();
$notification = Notification_data::with('sender')->find($notificationData->id);
// dd( broadcast(new PrivateEvent($notification))->toOthers());
broadcast(new PrivateEvent($notification))->toOthers();
}
$document = new Client_Document;
$document->user_id = $clientId;
$document->document__categories_id = $docuementCategoryId;
$document->description = $documentDiscription;
$document->original_name = $fileOriginalName;
$document->document_path = $fileName;
if ($document->save()) {

return response()->json(['Docuemnt Uploaded', 'data' => $document], 200);
} else {
return response('Error in upload', 405);
}
}

//edit Document
public function editDocument($id) {
$editData = Client_Document::findOrFail($id);
return response()->json(['data' => $editData], 200);
}

//update Document
public function updateDocument(Request $request, $id) {

$docuementCategoryId = $request->category;
$documentDiscription = $request->description;
$isNotified = $request->notify_client;
$employeeId = $request->employee_id;
$clientId = $request->client_id;
$fileOriginalName = $request->original_name;
$dirUniSharp = 'files' . '/' . $clientId;

if ($request->filedata) {
$file = $request->filedata;
// $fileName = FileUpload::uploadFile($file, 'uploadedClientDocuments');
$fileName = Unisharp::uploadFile($file, 'uploadedClientDocuments', $dirUniSharp);

// $fileName = FileUpload::uploadFile($file, 'clientDocuments');

Client_Document::where('id', $id)->update(['user_id' => $clientId, 'document__categories_id' => $docuementCategoryId, 'description' => $documentDiscription, 'original_name' => $fileOriginalName, 'document_path' => $fileName]);
} else {

Client_Document::where('id', $id)->update(['user_id' => $clientId, 'document__categories_id' => $docuementCategoryId, 'description' => $documentDiscription, 'original_name' => $fileOriginalName]);
}
$data = $request->all();
$current_time = Carbon::now()->toDateTimeString();
return response()->json(['data' => $data, 'update_at' => $current_time, 'updated Successfully'], 200);
}

//Delete Document
public function DeleteDocument($id) {
$deleteDocuemnt = Client_Document::findOrFail($id);
$image_path = public_path("clientDocuments/{$deleteDocuemnt->document_path}");
if (File::exists($image_path)) {
unlink($image_path);
}
$deleteDocuemnt->delete();
return response('deleted Successfully', 200);
}

//For Redis Data
public function getConnectionData(Request $request) {
$employeeId = $request->employee_id;
$clientId = $request->client_id;
$roomMembers = [$clientId, $employeeId];
$roomMembers = implode($roomMembers, ',');
$notificationRoom = Notification_room::where('user_ids', $roomMembers)->first();
if (is_null($notificationRoom)) {
$notificationRoom = new Notification_room;
$notificationRoom->room_type = 'private';
$notificationRoom->user_ids = $roomMembers;
$notificationRoom->save();
}
return response()->json(['added Successfully', 'notificationRoomData' => $notificationRoom, 'receiverId' => $clientId], 200);
}

public function notification(Request $request) {
$clientIdRead = Notify::findOrFail($request->id)->where('is_read', 1)->pluck('id', 'text');
$clientIdUnRead = Notify::findOrFail($request->clientId)->where('is_read', 0)->pluck('id', 'text');
return response()->json(['dataRead' => $clientIdRead, 'dataUnRead' => $clientIdUnRead], 200);
}

public function generatePdf() {
return view('frontend.employee.services.financial_consulting');

// ado 
}

}