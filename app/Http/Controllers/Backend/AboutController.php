<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Backend\About;
use Redirect;
use Session;
use App\Http\Requests\Backend\SaveAboutRequest;

class AboutController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $aboutPageContent = About::all()->first();
        
        return View::make('backend.about.index', ['aboutPageContent' => $aboutPageContent]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveAboutRequest $request) {
        $data = $request->all();
        $aboutPageContent = About::all()->first();
        $fileArray = (!empty($request->file())) ? $this->_uploadImages($request->file()) : [];
        if ($aboutPageContent) {
            if ($data['sponsor_logo']['data'] != '') {
                $data['sponsor_logo'] = $this->_uploadBase64Image(public_path() . '/img/backend/aboutpage', $data['sponsor_logo']['name'], $data['sponsor_logo']['data']);
            } else {
                $data['sponsor_logo'] = $data['sponsor_logo']['name'];
            }
            $data['seo_img'] = (isset($fileArray['seo_img'])) ? $fileArray['seo_img'] : '';
            $data['slide_1_image'] = (isset($fileArray['slide_1_image'])) ? $fileArray['slide_1_image'] : '';
            $aboutPageContent->update($data);
            return Redirect::back()->with('updateMessage', 'Successfully Updated!');
        } else {
            if ($data['sponsor_logo']['data'] != '') {
                $data['sponsor_logo']=$this->_uploadBase64Image(public_path() . '/img/backend/aboutpage', $data['sponsor_logo']['name'], $data['sponsor_logo']['data']);
            }
            $data['seo_img'] = (isset($fileArray['seo_img'])) ? $fileArray['seo_img'] : '';
            $data['slide_1_image'] = (isset($fileArray['slide_1_image'])) ? $fileArray['slide_1_image'] : '';
            About::create($data);
            return Redirect::back()->with('saveMessage', 'Successfully Saved!');
        }
    }

    private static function _uploadBase64Image($path, $name, $imgdata) {
        $name=date('d-m-y-h-i-s-').$name;
        $imgdata = base64_decode(explode(',', $imgdata)[1]);
        $handle = fopen($path . DS . $name, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return $name;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    
    private static function _uploadImages($files)
    {
        $fileArray = [];
        foreach ($files as $key => $file) {
            if ($key == 'sponsor_logo_file')
                continue;
            $fileName = date('d-m-y-h-i-s-') . $file->getClientOriginalName();
            $file->move('img/backend/aboutpage', $fileName);
            $fileArray[$key] = $fileName;
        }
        return $fileArray;
    }

}
