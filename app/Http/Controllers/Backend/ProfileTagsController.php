<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests\Backend\StoreProfileTagRequest;
use App\Http\Requests\Backend\UpdateProfileTagRequest;
use App\Http\Controllers\Controller;
use App\Models\Backend\ProfileTag;
Use DB;

class ProfileTagsController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = ProfileTag::get();
        return view('backend.profileTags.index', compact('tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.profileTags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProfileTagRequest $request)
    {
        $data = $request->all();
        DB::beginTransaction();

        $profileTag = new ProfileTag();
        $profileTag->name = $data['name'];

        if ($profileTag->save()) {
            DB::commit();
            return redirect()->route('admin.profiletags.index')->withFlashSuccess('Profile Type Tag successfully saved.');
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger('Failed To Save Profile Type Tag.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = ProfileTag::find($id);
        return view('backend.profileTags.create', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileTagRequest $request, $id)
    {
        $profileTag = ProfileTag::find($request->id);
        if ($profileTag && $profileTag->update([
                    'name' => $request->name,
                ])) {
            return redirect()->route('admin.profiletags.index')->withFlashSuccess('Profile Type Tag successfully updated.');
        }
        return redirect()->back()->withFlashDanger('Failed To Update Profile Type Tag.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        if (ProfileTag::find($request->id)->delete()) {
            return $request->session()->flash('flash_success', 'Profile Type tag Deleted Successfully.');
        } else {
            return $request->session()->flash('flash_warning', 'Profile Type tag deletion Failed.');
        }
    }

}
