<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Backend\Employee;
use Redirect;
use Session;
use App\Http\Requests\Backend\SaveEmployeeRequest;

class EmployeeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        return View::make('backend.employee.index', ['employees' => $employees]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.employee.create', ['employee' => 0]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveEmployeeRequest $request)
    {
        $data = $request->all();
        if ($data['image']['data'] != '') {
            $data['image'] = $this->_uploadBase64Image(public_path() . '/img/backend/aboutpage/employees/', $data['image']['name'], $data['image']['data']);
        } else {
            $data['image'] = $data['image']['name'];
        }
        Employee::create($data);
        return redirect()->route('admin.employee.index')->with('saveMessage', 'Successfully Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::findOrFail($id);
        return view('backend.employee.create', ['employee' => $employee]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SaveEmployeeRequest $request, $id)
    {
        $employee = Employee::findOrFail($id);
        $data = $request->all();
        if ($data['image']['data'] != '') {
            $data['image'] = $this->_uploadBase64Image(public_path() . '/img/backend/aboutpage/employees/', $data['image']['name'], $data['image']['data']);
        } else {
            $data['image'] = $data['image']['name'];
        }
        $employee->update($data);
        return redirect()->route('admin.employee.index')->with('saveMessage', 'Successfully Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();
        return Redirect::back()->withFlash(['saveMessage', 'Successfully Deleted']);
    }

    private static function _uploadBase64Image($path, $name, $imgdata)
    {
        $name = date('d-m-y-h-i-s-') . $name;
        $imgdata = base64_decode(explode(',', $imgdata)[1]);
        $handle = fopen($path . DS . $name, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return $name;
    }

    public function editOrder(Request $request)
    {
        $input = $request->input();

        $orders_array = Employee::get()->pluck('order')->toArray();
        if (isset($input['employee_id'])) {
            $employee = Employee::where('id', $input['employee_id'])->first();
            $current_order = $employee->order;
        }

        if (in_array($input['order'], $orders_array)) { //duplicate
            if (!empty($employee)) { //existing employee
                if ($current_order != $input['order']) {
                    return response()->json(['message' => 'This order is in use!', 'status' => '0'], 200);
                } else {
                    if ($input['form']) {
                        return response()->json(['message' => 'This order number is available.', 'status' => '1'], 200);
                    } else {
                        $employee->update(['order' => $input['order']]);
                        return response()->json(['message' => 'The order has been updated.', 'status' => '1'], 200);
                    }
                }
            } else { //new employee
                return response()->json(['message' => 'This order is in use!', 'status' => '0'], 200);
            }
        } else { //not duplicate
            if ($input['form']) { 
                return response()->json(['message' => 'This order number is available.', 'status' => '1'], 200);
            } else { //existing employee
                $employee->update(['order' => $input['order']]);
                return response()->json(['message' => 'The order has been updated.', 'status' => '1'], 200);
            }
        }
    }

}
