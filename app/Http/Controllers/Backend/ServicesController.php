<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backend\ServiceCategory;
use App\Models\Backend\Service;
use App\Models\Backend\ServiceServiceCategory;
use App\Models\Backend\ServiceFile;
use App\Models\Backend\ServiceQuestion;
use App\Models\Backend\ServiceQuestionOption;
use App\Models\Backend\ProfileTag;
use App\Models\Backend\ServiceSlideData;
use Redirect;
use App\Http\Requests\Backend\SaveServiceCategoryRequest;
use App\Http\Requests\Backend\SaveServiceRequest;
use DB;
use Response;
use App\Services\FileUpload;

class ServicesController extends Controller
{
    /* SERVICES */

    public function index()
    {
        $services = Service::orderBy('id', 'desc')->get();
        return view('backend.services.index', compact('services'));
    }

    public function createService()
    {
        $serviceCategories = ServiceCategory::get();
        return view('backend.services.create', compact('serviceCategories'));
    }

    public function saveService(SaveServiceRequest $request)
    {
        $files      = $request->file();
        $input      = $request->input();
        $image      = $this->_uploadImages($files['image'],
            'img/backend/services');
        $service    = Service::create([
                'title' => $input['title'],
                'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color']
                    : '#002d62',
                'order' => $input['order'],
                'image' => $image,
                'description' => $input['description'],
                'recommended_for' => $input['recommended_for'],
                'cost' => $input['cost'],
                'amount' => $input['amount'],
                'fee_schedule' => $input['fee_schedule']
        ]);
        $service_id = $service->id;

        foreach ($input['service_category_id'] as $serviceCategoryId) {
            $service->serviceCategories()->attach(['service_category_id' => $serviceCategoryId]);
        }

        $this->_saveUploadedFiles($request, $service_id);
        return redirect()->route('admin.services')->withFlashSuccess('Service successfully created.');
    }

    private function _uploadImages($file, $directory)
    {
        $fileName = date('d-m-y-h-i-s-').$file->getClientOriginalName();
        $file->move($directory, $fileName);
        return $fileName;
    }

    private function _saveUploadedFiles($request, $service_id)
    {
        $fileArray            = [];
        $files                = $request->file();
        $input                = $request->input();
        $serviceFile          = ServiceFile::where('service_id', $service_id);
        $existingServiceFiles = $serviceFile->get()->count();

        if (isset($files['upload_file'])) {
            $fileArray = $this->_createFileArray($input, $files);
            if ($existingServiceFiles) {
                $serviceFile->delete();
            }
            $this->_createServiceFile($fileArray, $service_id);
        } else {
            if (!empty($input['upload_file_old'])) {
                $fileArray = $this->_updateFileArray($input);
                $serviceFile->delete();
                $this->_createServiceFile($fileArray, $service_id);
            } else {
                $serviceFile->delete();
            }
        }
    }

    private function _createFileArray($input, $files)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text'] = $fileText;
            if (isset($files['upload_file'][$key])) {
                $fileArray[$key]['upload_file'] = $this->_uploadImages($files['upload_file'][$key],
                    'img/backend/services/file_uploads');
            } else {
                $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
            }
        }
        return $fileArray;
    }

    private function _updateFileArray($input)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text']   = $fileText;
            $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
        }
        return $fileArray;
    }

    private function _createServiceFile($fileArray, $service_id)
    {
        foreach ($fileArray as $serviceFile) {
            ServiceFile::create([
                'service_id' => $service_id,
                'file_text' => $serviceFile['file_text'],
                'upload_file' => $serviceFile['upload_file'],
            ]);
        }
    }

    public function editService($id)
    {
//        dd('edit service');
        $serviceCategories         = ServiceCategory::get();
        $service                   = Service::where('id', $id)->with(['serviceFiles',
                'serviceCategories'])->first();
        $selectedServiceCategories = [];
        foreach ($service->serviceCategories as $selectedServiceCategory) {
            $selectedServiceCategories[] = $selectedServiceCategory->id;
        }
        return view('backend.services.create',
            compact('service', 'serviceCategories', 'selectedServiceCategories'));
    }

    public function updateService(SaveServiceRequest $request)
    {
        $files   = $request->file();
        $input   = $request->input();
        $service = Service::find($input['id']);

        $image = (isset($files['image'])) ? $this->_uploadImages($files['image'],
                'img/backend/services') : $service->image;
        $service->update([
            'title' => $input['title'],
            'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color']
                    : '#002d62',
            'order' => $input['order'],
            'image' => $image,
            'description' => $input['description'],
            'recommended_for' => $input['recommended_for'],
            'cost' => $input['cost'],
            'amount' => $input['amount'],
            'fee_schedule' => $input['fee_schedule']
        ]);

        $service->serviceCategories()->sync($input['service_category_id']);
        $this->_saveUploadedFiles($request, $service->id);

        return redirect()->route('admin.services')->withFlashSuccess('Service successfully updated.');
    }

    public function deleteService(Request $request)
    {
        $input   = $request->input();
        $service = Service::find($input['id']);
        $service->serviceCategories()->detach();
        $service->serviceFiles()->delete();
        $service->delete();
        return $request->session()->flash('flash_success',
                'The Service has been successfully deleted.');
    }

    public function editOrder(Request $request)
    {
        $input = $request->input();

        $orders_array = Service::get()->pluck('order')->toArray();
        if (isset($input['service_id'])) {
            $service       = Service::where('id', $input['service_id'])->first();
            $current_order = $service->order;
        }

        if (in_array($input['order'], $orders_array)) { //duplicate
            if (!empty($service)) { //existing service
                if ($current_order != $input['order']) {
                    return response()->json(['message' => 'This order is in use!',
                            'status' => '0'], 200);
                } else {
                    if ($input['form']) {
                        return response()->json(['message' => 'This order number is available.',
                                'status' => '1'], 200);
                    } else {
                        $service->update(['order' => $input['order']]);
                        return response()->json(['message' => 'The order has been updated.',
                                'status' => '1'], 200);
                    }
                }
            } else { //new service
                return response()->json(['message' => 'This order is in use!', 'status' => '0'],
                        200);
            }
        } else { //not duplicate
            if ($input['form']) {
                return response()->json(['message' => 'This order number is available.',
                        'status' => '1'], 200);
            } else { //existing service
                $service->update(['order' => $input['order']]);
                return response()->json(['message' => 'The order has been updated.',
                        'status' => '1'], 200);
            }
        }
    }
    /* SERVICE CATEGORIES */

    public function categories()
    {
        $categories = ServiceCategory::orderBy('id', 'desc')->get();
        return view('backend.services.categories.index', compact('categories'));
    }

    public function createCategory()
    {
        return view('backend.services.categories.create');
    }

    public function saveCategory(SaveServiceCategoryRequest $request)
    {
        $image_name = $this->_handleImage($request->input());
        ServiceCategory::create([
            'name' => $request->name,
            'logo' => $image_name
        ]);
        return redirect()->route('admin.services.categories')->withFlashSuccess('Service Category successfully created.');
    }

    private function _handleImage($input)
    {
        if ($input['logo']['name'] != "") {
            $image_name = date('d-m-y-h-i-s-').$input['logo']['name'];
            $img        = explode(',', $input['logo']['data'])[1];
            $this->_uploadBase64Image(public_path().'/img/backend/services/categories',
                $image_name, base64_decode($img));
        } else {
            $image_name = $input['logo_old'];
        }
        return $image_name;
    }

    private function _uploadBase64Image($path, $file, $imgdata)
    {
        $handle = fopen($path.DS.$file, 'w');
        fwrite($handle, $imgdata);
        fclose($handle);
        return TRUE;
    }

    public function editCategory($id)
    {
        $category = ServiceCategory::find($id);
        return view('backend.services.categories.create', compact('category'));
    }

    public function updateCategory(SaveServiceCategoryRequest $request)
    {
        $category   = ServiceCategory::find($request->id);
        $image_name = $this->_handleImage($request->input());
        $category->update([
            'name' => $request->name,
            'logo' => $image_name
        ]);
        return redirect()->route('admin.services.categories')->withFlashSuccess('Service Category successfully updated.');
    }

    public function deleteCategory(Request $request)
    {
        $input           = $request->input();
        $inUse           = $this->_isCategoryInUse($input['id']);
        $totalCategories = ServiceCategory::get()->count();
        $message         = ($totalCategories == 1) ? 'You cannot delete all Service Categories.'
                : 'This Service Category is in use. You cannot delete it.';
        if ($inUse || $totalCategories == 1) {
            return $request->session()->flash('flash_warning', $message);
        } else {
            $category = ServiceCategory::find($input['id']);
            $category->delete();
            return $request->session()->flash('flash_success',
                    'The Service Category has been successfully deleted.');
        }
    }

    private function _isCategoryInUse($id)
    {
        return ServiceServiceCategory::where('service_category_id', $id)->count();
    }

    public function createQuestion($id)
    {
        $tag         = ProfileTag::get(['id', 'name']);
        $profileTags = [];
        foreach ($tag as $value) {
            $profileTags[$value['id']] = $value['name'];
        }
        return view('backend.services.questions.create')->with(['serviceId' => $id,
                'profileTags' => $profileTags]);
    }

    public function saveQuestion(Request $request)
    {
        $data = $request->all();
        DB::beginTransaction();

        $question             = new ServiceQuestion();
        $question->service_id = $data['service_id'];
        $question->question   = $data['question'];
        $question->type       = $data['type'];
        $question->order      = $data['order'];
        if (isset($data['profile_tag_id']) && !empty($data['profile_tag_id'])) {
            $question->profile_tag_id = $data['profile_tag_id'];
        }

        if ($question->save()) {

            if (in_array($data['type'],
                    config('constant.service_question_types_options'))) {

                foreach ($data['option_name'] as $optionName) {

                    if (!$this->_saveServiceQuestionOption($question->id,
                            $optionName)) {
                        DB::rollBack();
                        return redirect()->back()->withFlashDanger(trans('alerts.backend.questions.saveFail'));
                    }
                }
            }

            DB::commit();
            return redirect()->route('admin.services.questions',
                    $data['service_id'])->withFlashSuccess(trans('alerts.backend.questions.saveSuccess'));
        }
        DB::rollBack();
        return redirect()->back()->withFlashDanger(trans('alerts.backend.questions.saveFail'));
    }

    public function allServiceQuestions($id)
    {
        $questions = ServiceQuestion::where('service_id', $id)->orderBy('id',
                'desc')->get();
        return view('backend.services.questions.index', compact('questions'))->with([
                'serviceId' => $id]);
    }

    public function editQuestion($serviceId, $questionId)
    {
        $question = ServiceQuestion::with(['serviceQuestionOption' => function($q) {
                    $q->select('id', 'service_question_id', 'name');
                }])->where('id', $questionId)->first(['id', 'service_id', 'question',
            'type', 'order', 'profile_tag_id']);

        $tag         = ProfileTag::get(['id', 'name']);
        $profileTags = [];
        foreach ($tag as $value) {
            $profileTags[$value['id']] = $value['name'];
        }

        return view('backend.services.questions.create', compact('question'))->with([
                'serviceId' => $serviceId, 'profileTags' => $profileTags]);
    }

    public function updateQuestion(Request $request)
    {
        $data                = $request->all();
        $questionUpdateArray = [
            'question' => $data['question'],
            'type' => $data['type']
        ];

        DB::beginTransaction();

        if (ServiceQuestion::where('id', $data['id'])->update($questionUpdateArray)) {
            if (isset($data['option_name'])) {
                $updateOptions = $this->_updateServiceQuestionOption($data['option_name'],
                    $data['id']);

                if (!$updateOptions) {
                    DB::rollBack();
                    return redirect()->route('admin.services.questions.edit',
                            [$data['service_id'], $data['id']])->withFlashDanger(trans('alerts.backend.questions.updateFail'));
                }
            }

            DB::commit();
            return redirect()->route('admin.services.questions.edit',
                    [$data['service_id'], $data['id']])->withFlashSuccess(trans('alerts.backend.questions.updateSuccess'));
        }

        DB::rollBack();
        return redirect()->route('admin.services.questions.edit',
                [$data['service_id'], $data['id']])->withFlashDanger(trans('alerts.backend.questions.updateFail'));
    }

    public function deleteServiceQuestion(Request $request)
    {
        $data = $request->all();
        if (ServiceQuestion::destroy($data['id'])) {
            return Response::json(['route' => route('admin.services.questions',
                        $data['serviceId'])], 200);
        }
        return Response::json(['route' => route('admin.services.questions',
                    $data['serviceId'])], 500);
    }

    public function checkQuestionOrderStatus(Request $request)
    {
        $data     = $request->all();
        $response = $this->_commonCheckQuestionStatus($data);
        return response()->json($response, 200);
    }

    public function editServiceQuestionOrder(Request $request)
    {
        $data     = $request->all();
        $response = $this->_commonCheckQuestionStatus($data);
        return response()->json($response, 200);
    }

    private function _commonCheckQuestionStatus($data)
    {
        $ordersArray  = ServiceQuestion::where('service_id', $data['service_id'])->get()->pluck('order')->toArray();
        $service      = '';
        $currentOrder = '';

        if (isset($data['question_id']) && !empty($data['question_id'])) {
            $service      = ServiceQuestion::where('id', $data['question_id'])->first([
                'id', 'order']);
            $currentOrder = $service->order;
        }

        return $this->_commonOrderEdit($ordersArray, $data, $service,
                $currentOrder);
    }

    private function _commonOrderEdit($ordersArray, $data, $service = null,
                                      $currentOrder = null)
    {
        if (in_array($data['order'], $ordersArray)) { //duplicate
            if (!empty($service)) { //existing service
                if ($currentOrder != $data['order']) {
                    return ['message' => 'This order is in use!', 'status' => '0'];
                } else {
                    if ($data['form']) {
                        return ['message' => 'This order number is available.', 'status' => '1'];
                    } else if ($service->update(['order' => $data['order']])) {
                        return['message' => 'The order has been updated.', 'status' => '1',
                            'status' => '1'];
                    } else {
                        return ['message' => 'Oops!! something went wrong. Please try again.'];
                    }
                }
            } else { //new service
                return ['message' => 'This order is in use!', 'status' => '0'];
            }
        } else { //not duplicate
            if ($data['form']) {
                return ['message' => 'This order number is available.', 'status' => '1'];
            } else { //existing service
                $service->update(['order' => $data['order']]);
                return ['message' => 'The order has been updated.', 'status' => '1'];
            }
        }
    }

    private function _saveServiceQuestionOption($serviceQuestionId, $name)
    {
        $question_option                      = new ServiceQuestionOption();
        $question_option->service_question_id = $serviceQuestionId;
        $question_option->name                = $name;
        return $question_option->save();
    }

    private function _updateServiceQuestionOption($optionsArray,
                                                  $serviceQuestionId)
    {
        foreach ($optionsArray as $options) {

            if (isset($options['is_deleted'])) {
                $serviceQuestion = ServiceQuestionOption::where('id',
                        $options['id'])->delete();
            } else if (isset($options['id'])) {
                $serviceQuestion = ServiceQuestionOption::where('id',
                        $options['id'])->update($options);
            } else {
                $serviceQuestion = $this->_saveServiceQuestionOption($serviceQuestionId,
                    $options);
            }

            if (!$serviceQuestion) {
                return FALSE;
            }
        }
        return TRUE;
    }

    public function createSlides()
    {
        $checkEmpty = ServiceSlideData::get()->count();

        if (!$checkEmpty) {
            $data = $checkEmpty;
        } else {
            $data = ServiceSlideData::first()->toArray();
        }

        return view('backend.services.service_slide_data', compact('data'));
    }

    public function saveSlides(Request $request)
    {
//        dd(public_path('app/public/img/backend/services'));
        if ($request->hasFile('image_1')) {
            $fileName = FileUpload::uploadFile($request->image_1,'img/backend/services');
        }
//        dd('here1');
        $input = $request->input();

        if (ServiceSlideData::get()->count()) {
            $currentContent = ServiceSlideData::first();
            $currentContent->update([
                'heading_1' => $input['heading_1'],
                'subheading_1' => $input['subheading_1'],
                'image_1' => (isset($fileName)) ? $fileName: $currentContent['image_1'],
                'heading_1' => $input['heading_1'],
                'heading_2' => $input['heading_2'],
                'subheading_2' => $input['subheading_2'],
                'heading_3' => $input['heading_3'],
                'subheading_3' => $input['subheading_3']
            ]);

            return Redirect::back()->withFlashSuccess('The Resource page content has been Updated.');
        } else {
            ServiceSlideData::create([
                'heading_1' => $input['heading_1'],
                'subheading_1' => $input['subheading_1'],
                'image_1' => $fileName,
                'heading_2' => $input['heading_2'],
                'subheading_2' => $input['subheading_2'],
                'heading_3' => $input['heading_3'],
                'subheading_3' => $input['subheading_3']
            ]);
            return Redirect::back()->withFlashSuccess('The Resource page content has been saved.');
        }
    }
}