<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Backend\Article;
use App\Models\Backend\BlogCategory;
use App\Models\Backend\ArticleBlogCategory;
use App\Models\Access\User\User;
use App\Http\Requests\Backend\SaveArticleRequest;
use App\Http\Requests\Backend\SaveBlogCategoryRequest;

class BlogController extends Controller {
    /* ARTICLES */

    public function index() {
        $articles = Article::orderBy('publish_at', 'desc')->get();
        return view('backend.blogs.articles.index', compact('articles'));
    }

    public function createArticle() {
        $categories = BlogCategory::get()->pluck('title', 'id')->toArray();
        $users = User::whereHas('roles', function($q) {
                    $q->whereIn('name', ['Administrator','Employee']);
                }) -> get()->pluck('name','id')->toArray();
        return view('backend.blogs.articles.create', compact('categories', 'users'));
    }

    public function saveArticle(SaveArticleRequest $request) {
        $files = $request->file();
        $input = $request->input();
        $image = $this->_uploadImages($files['image'], 'img/backend/blogs/articles');
        $slug = $this->_validateSlug($input['slug']);
        $article = Article::create([
                    'author' => $input['author'],
                    'slug' => $slug,
                    'title' => $input['title'],
                    'image' => $image,
                    'excerpt' => $input['excerpt'],
                    'body' => $input['body'],
                    'status' => (isset($input['status'])) ? $input['status'] : config('constant.backend.blogs.default_status'),
                    'publish_at' => (!empty($input['publish_at']) ? $input['publish_at'] : date('Y-m-d'))
        ]);
        $article_id = $article->id;
        foreach ($input['blog_category_id'] as $blogCategoryId) {
            $article->blogCategories()->attach(['blog_category_id' => $blogCategoryId]);
        }
        return redirect()->route('admin.blogs.articles')->withFlashSuccess('Article successfully created.');
    }

    private function _validateSlug($slug) {
        $count = Article::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        return $count ? ($slug . '-' . ($count + 1)) : $slug;
    }

    public function makeSlugFromTitle($title) {
        $slug = Str::slug($title);
        $count = Article::whereRaw("slug RLIKE '^{$slug}(-[0-9]+)?$'")->count();
        return $count ? ($slug . '-' . ($count + 1)) : $slug;
    }

    public function autoGenerateSlug(Request $request) {
        $input = $request->input();
        $slug = $this->makeSlugFromTitle($input['title']);
        return response()->json(['slug' => $slug], 200);
    }

    public function checkSlugDuplicacy(Request $request) /* slug editing disabled */ {
        $input = $request->input();
        $duplicate = Article::where('slug', $input['slug'])->count();
        $message = ($duplicate) ? 'This slug is already in use. Please choose another slug.' : 'This slug is available.';
        $status = ($duplicate) ? 'duplicate' : 'unique';
        return response()->json(['message' => $message, 'status' => $status], 200);
    }

    private function _uploadImages($file, $directory) {
        $fileName = date('d-m-y-h-i-s-') . $file->getClientOriginalName();
        $file->move($directory, $fileName);
        return $fileName;
    }

    public function editArticle($id) {
        $categories = BlogCategory::get()->pluck('title', 'id')->toArray();
        $article = Article::where('id', $id)->with(['blogCategories', 'articleAuthor'])->first();
        $users = User::get()->pluck('name', 'id')->toArray();
        return view('backend.blogs.articles.create', compact('categories', 'article', 'users'));
    }

    public function updateArticle(SaveArticleRequest $request) {
        $files = $request->file();
        $input = $request->input();
        $article = Article::find($input['id']);
        $image = (isset($files['image'])) ? $this->_uploadImages($files['image'], 'img/backend/blogs/articles') : $article->image;
        $slug = $input['slug'];
        $article->update([
            'author' => $input['author'],
            'slug' => $slug,
            'title' => $input['title'],
            'image' => $image,
            'excerpt' => $input['excerpt'],
            'body' => $input['body'],
            'status' => (isset($input['status'])) ? $input['status'] : config('constant.backend.blogs.default_status'),
            'publish_at' => (!empty($input['publish_at']) ? $input['publish_at'] : date('Y-m-d'))
        ]);
        $article->blogCategories()->sync($input['blog_category_id']);
        return redirect()->route('admin.blogs.articles')->withFlashSuccess('Article successfully updated.');
    }

    public function deleteArticle(Request $request) {
        $input = $request->input();
        $article = Article::find($input['id']);
        $article->blogCategories()->detach();
        $article->delete();
        return $request->session()->flash('flash_success', 'The Article has been successfully deleted.');
    }

    /* CATEGORIES */

    public function categories() {
        $categories = BlogCategory::orderBy('id', 'desc')->get();
        return view('backend.blogs.categories.index', compact('categories'));
    }

    public function createCategory() {
        return view('backend.blogs.categories.create');
    }

    public function saveCategory(SaveBlogCategoryRequest $request) {
        $input = $request->input();
        BlogCategory::create([
            'title' => $input['title'],
            'description' => $input['description']
        ]);
        return redirect()->route('admin.blogs.categories')->withFlashSuccess('Blog Category successfully created.');
    }

    public function editCategory($id) {
        $category = BlogCategory::find($id);
        return view('backend.blogs.categories.create', compact('category'));
    }

    public function updateCategory(SaveBlogCategoryRequest $request) {
        $input = $request->input();
        $category = BlogCategory::find($input['id']);
        $category->update([
            'title' => $input['title'],
            'description' => $input['description']
        ]);
        return redirect()->route('admin.blogs.categories')->withFlashSuccess('Blog Category successfully updated.');
    }

    public function deleteCategory(Request $request) {
        $input = $request->input();
        $inUse = $this->_isCategoryInUse($input['id']);
        $totalCategories = BlogCategory::get()->count();
        $message = ($totalCategories == 1) ? 'You cannot delete all Blog Categories.' : 'This Blog Category is in use. You cannot delete it.';
        if ($inUse || $totalCategories == 1) {
            return $request->session()->flash('flash_warning', $message);
        } else {
            $category = BlogCategory::find($input['id']);
            $category->delete();
            return $request->session()->flash('flash_success', 'The Blog Category has been successfully deleted.');
        }
    }

    private function _isCategoryInUse($id) {
        return ArticleBlogCategory::where('blog_category_id', $id)->count();
    }

}
