<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SaveHomepageRequest;
use App\Models\Backend\Homepage;
use App\Models\Backend\LogoBox;
use App\Models\Backend\Seo;
use Redirect;

class HomeController extends Controller
{

    public function index()
    {
        $checkEmpty = Homepage::get()->count();

        if (!$checkEmpty) {
            $homePageContent = $checkEmpty;
        } else {
            $data      = Homepage::first()->toArray();
            $logoBoxes = LogoBox::get()->toArray();
            foreach ($logoBoxes as $key => $logoBox) {
                $data['logoBox'][$key]['box_logo']        = $logoBox['box_logo'];
                $data['logoBox'][$key]['box_heading']     = $logoBox['box_heading'];
                $data['logoBox'][$key]['box_description'] = $logoBox['box_description'];
            }
            $seoContent                   = Seo::first()->toArray();
            $data['home_seo_title']       = $seoContent['home_seo_title'];
            $data['home_seo_description'] = $seoContent['home_seo_description'];
            $data['home_seo_img']         = $seoContent['home_seo_img'];
            $homePageContent              = $data;
        }
        return view('backend.home.index', compact('homePageContent'));
    }

    public function saveHomepage(SaveHomepageRequest $request)
    {
        $input = $request->input();
        if (Homepage::get()->count()) {
            $currentContent = Homepage::first();
            $fileArray      = (!empty($request->file())) ? $this->_uploadImages($request->file())
                    : [];
            $currentContent->update([
                'seo_img' => (isset($fileArray['seo_img'])) ? $fileArray['seo_img']
                        : $currentContent['seo_img'],
                'seo_title' => $input['seo_title'],
                'seo_description' => $input['seo_description'],
                'bg_image_1' => (isset($fileArray['bg_image_1'])) ? $fileArray['bg_image_1']
                        : $currentContent['bg_image_1'],
                'heading_1' => $input['heading_1'],
                'subheading_1' => $input['subheading_1'],
                'heading_3' => $input['heading_3'],
                'subheading_3' => $input['subheading_3'],
                'image_3' => (isset($fileArray['image_3'])) ? $fileArray['image_3']
                        : $currentContent['image_3'],
                'image_4' => (isset($fileArray['image_4'])) ? $fileArray['image_4']
                        : $currentContent['image_4'],
                'heading_4' => $input['heading_4'],
                'description_4' => $input['description_4'],
                'linktext_4' => $input['linktext_4'],
                'bg_image_6' => (isset($fileArray['bg_image_6'])) ? $fileArray['bg_image_6']
                        : $currentContent['bg_image_6'],
                'heading_6' => $input['heading_6'],
                'description_6' => $input['description_6'],
                'linktext_6' => $input['linktext_6'],
                'heading_7' => $input['heading_7']
            ]);

            if (!empty($input['box_logo']) && !empty($input['box_heading']) && !empty($input['box_description'])) {
                $saveLogoBoxes = LogoBox::saveContent($input);
            }
//            $this->_updateSeoContent($request);
            return Redirect::back()->withFlashSuccess('The home page content has been updated.');
        } else {
            $fileArray     = $this->_uploadImages($request->file());
            //save homepage content
            $saveHomePage  = Homepage::create([
                    'seo_img' => $fileArray['seo_img'],
                    'seo_title' => $input['seo_title'],
                    'seo_description' => $input['seo_description'],
                    'bg_image_1' => $fileArray['bg_image_1'],
                    'heading_1' => $input['heading_1'],
                    'subheading_1' => $input['subheading_1'],
                    'heading_3' => $input['heading_3'],
                    'subheading_3' => $input['subheading_3'],
                    'image_3' => $fileArray['image_3'],
                    'image_4' => $fileArray['image_4'],
                    'heading_4' => $input['heading_4'],
                    'description_4' => $input['description_4'],
                    'linktext_4' => $input['linktext_4'],
                    'bg_image_6' => $fileArray['bg_image_6'],
                    'heading_6' => $input['heading_6'],
                    'description_6' => $input['description_6'],
                    'linktext_6' => $input['linktext_6'],
                    'heading_7' => $input['heading_7']
            ]);
            //save logoboxes
            $saveLogoBoxes = LogoBox::saveContent($input);
//            $this->_updateSeoContent($request);
            return Redirect::back()->withFlashSuccess('The home page content has been saved.');
        }
    }

    private static function _uploadImages($files)
    {
        $fileArray = [];
        foreach ($files as $key => $file) {
            if ($key == 'box_logo') continue;
            $fileName        = date('d-m-y-h-i-s-').$file->getClientOriginalName();
            $file->move('img/backend/homepage', $fileName);
            $fileArray[$key] = $fileName;
        }
        return $fileArray;
    }

    public function uploader()
    {
        return view('backend.uploader');
    }

    public function saveUploader(Request $request)
    {
        $input = $request->all();
        if ($input['filepath']) {
//            $file = explode('.',basename($input['filepath']));
//            $link = '<a href="'.$input['filepath'].'" target="_blank">'.$file[0].'</a>';
            $link   = $input['filepath'];
            $status = 1;
            return response()->json(['link' => $link, 'status' => $status], 201);
        } else {
            $link   = '';
            $status = 0;
            return response()->json(['link' => $link, 'status' => $status], 500);
        }
    }
}