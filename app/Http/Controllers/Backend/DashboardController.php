<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Backend\ServiceUser;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller {

    /**
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('backend.dashboard')->with(['subscriptionHistory' => ServiceUser::with(['service' => function($subQuery) {
                            $subQuery->select('id', 'title');
                        }, 'user' => function($subQuery) {
                            $subQuery->select('id', 'email');
                        }])->orderBy('updated_at', 'desc')->take(20)->get()]);
    }

}
