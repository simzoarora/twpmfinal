<?php

namespace App\Http\Controllers\Backend\Access\User;

use App\Models\Access\User\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\User\UserRepository;
use App\Repositories\Backend\Access\Role\RoleRepository;
use App\Http\Requests\Backend\Access\User\StoreUserRequest;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserRequest;
use Illuminate\Http\Request;
use App\Models\Backend\ClientToEmployeeUser;
use Illuminate\Database\QueryException;

/**
 * Class UserController
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * @param UserRepository $users
     * @param RoleRepository $roles
     */
    public function __construct(UserRepository $users, RoleRepository $roles) {
        $this->users = $users;
        $this->roles = $roles;
    }

    /**
     * @param ManageUserRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(ManageUserRequest $request) {
        return view('backend.access.index');
    }

    /**
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function create(ManageUserRequest $request) {
        return view('backend.access.create')
                ->withRoles($this->roles->getAll());
    }

    /**
     * @param StoreUserRequest $request
     * @return mixed
     */
    public function store(StoreUserRequest $request) {
        $this->users->create(['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);
        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.created'));
    }

    /**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function show(User $user, ManageUserRequest $request) {
        return view('backend.access.show')
                ->withUser($user);
    }

    /**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request) {
        return view('backend.access.edit')
                ->withUser($user)
                ->withUserRoles($user->roles->pluck('id')->all())
                ->withRoles($this->roles->getAll());
    }

    /**
     * @param User $user
     * @param UpdateUserRequest $request
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request) {
        $this->users->update($user, ['data' => $request->except('assignees_roles'), 'roles' => $request->only('assignees_roles')]);
        return redirect()->route('admin.access.user.index')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param User $user
     * @param ManageUserRequest $request
     * @return mixed
     */
    public function destroy(User $user, ManageUserRequest $request) {
        $this->users->delete($user);
        return redirect()->route('admin.access.user.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted'));
    }

    public function assignClientsToEmployee() {
        return view('backend.access.assignClientsToEmployee');
    }

    public function searchEmployee(Request $request) {
        $queryString = $request->queryString;

        return response()->json(User::select('id', 'email', 'name')
                    ->where('users.confirmed', 1)
                    ->where('users.status', 1)
                    ->whereHas('roles',
                        function ($subQuery) {
                        $subQuery->where('name',
                            config('access.roles.employee_role'));
                    })
                    ->where(function ($subQuery) use ($queryString) {
                        $subQuery
                        ->orWhere('email', 'LIKE', "%$queryString%")
                        ->orWhere('name', 'LIKE', "%$queryString%");
                    })
                    ->get(), 200);
    }

    public function searchClient(Request $request) {
        $queryString = $request->queryString;

        return response()->json(User::select('id', 'email')
                                ->where('users.confirmed', 1)
                                ->where('users.status', 1)
                                ->whereNotIn('id', function($query) {
                                    $query->select('client_user_id')
                                    ->from('client_to_employee_users');
                                })->has('profile')
                                ->with(['profile' => function($subQuery) {
                                        $subQuery->select('id', 'user_id', 'first_name', 'last_name');
                                    }])->where(function ($subQuery) use ($queryString) {
                            $subQuery->where('email', 'LIKE', "%$queryString%")
                                    ->orWhereHas('profile', function ($query) use ($queryString) {
                                        $query->where('first_name', 'LIKE', "%$queryString%")
                                        ->orWhere('last_name', 'LIKE', "%$queryString%");
                                    });
                        })->get(), 200);
    }

    public function storeClientsToEmployee(Request $request) {
        try {
            $employeeClient = new ClientToEmployeeUser();
            $employeeClient->employee_user_id = $request->employee_user_id;
            $employeeClient->client_user_id = $request->client_user_id;
            if ($employeeClient->save()) {
                return redirect()->back()->withFlashSuccess('Client sucessfully assigned to employee');
            }
        } catch (QueryException $ex) {

//            return redirect()->back()->withFlashDanger($ex->getMessage());
            return redirect()->back()->withFlashDanger('Failed to assign Client to Employee');
        }
    }

    public function allclientEmployeeList() {
        $users = ClientToEmployeeUser::with(['employeeUser', 'clientUsers' => function($q) {
                        $q->with(['profile' => function($q) {
                                $q->select(['id', 'user_id', 'first_name', 'middle_name',
                                    'last_name']);
                            }]);
                    }
                ])->get();
        return view('backend.access.clientEmployeeList', compact('users'));
    }

    public function removeClientEmployeeRelation($id) {
        if (isset($id) && !empty($id)) {
            $delted = ClientToEmployeeUser::destroy($id);
            if ($delted) {
                return redirect()->back()->withFlashSuccess('Successfuly Deleted');
            }
        }
        return redirect()->back()->withFlashWarning('An Error Occured');
    }

}
