<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SaveContactRequest;
use App\Models\Backend\Contact;
use Redirect;

class ContactController extends Controller
{

    public function index()
    {
        $checkEmpty = Contact::get()->count();
        if (!$checkEmpty) {
            $contactContent = $checkEmpty;
        } else {
            $contactContent = Contact::first()->toArray();
        }
        return view('backend.contact.index', compact('contactContent'));
    }

    public function saveContact(SaveContactRequest $request)
    {
        $input = $request->input();
        if (Contact::get()->count()) {
            $contact = Contact::first();
            $contact->update($input);
            return Redirect::back()->withFlashSuccess('The contact content has been updated.');
        } else {
            Contact::create($input);
            return Redirect::back()->withFlashSuccess('The contact content has been saved.');
        }
    }

}
