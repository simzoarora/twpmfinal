<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\SaveLegalDisclosuresRequest;
use App\Http\Requests\Backend\SavePrivacyRequest;
use App\Http\Requests\Backend\SaveTermsRequest;
use App\Models\Backend\LegalDisclosure;
use App\Models\Backend\Privacy;
use App\Models\Backend\Term;
use Redirect;

class PagesController extends Controller
{
    /* Legal Disclosures */
    public function legalDisclosures()
    {
        $checkEmpty = LegalDisclosure::get()->count();
        if (!$checkEmpty) {
            $content = $checkEmpty;
        } else {
            $content = LegalDisclosure::first()->toArray();
        }
        return view('backend.pages.legal_disclosures', compact('content'));
    }

    public function saveLegalDisclosures(SaveLegalDisclosuresRequest $request)
    {
        $input = $request->input();
        if (LegalDisclosure::get()->count()) {
            $content = LegalDisclosure::first();
            $content->update($input);
            return Redirect::back()->withFlashSuccess('The content has been updated.');
        } else {
            LegalDisclosure::create($input);
            return Redirect::back()->withFlashSuccess('The content has been saved.');
        }
    }
    
    /* Privacy & Security */
    public function privacy()
    {
        $checkEmpty = Privacy::get()->count();
        if (!$checkEmpty) {
            $content = $checkEmpty;
        } else {
            $content = Privacy::first()->toArray();
        }
        return view('backend.pages.privacy', compact('content'));
    }

    public function savePrivacy(SavePrivacyRequest $request)
    {
        $input = $request->input();
        if (Privacy::get()->count()) {
            $content = Privacy::first();
            $content->update($input);
            return Redirect::back()->withFlashSuccess('The content has been updated.');
        } else {
            Privacy::create($input);
            return Redirect::back()->withFlashSuccess('The content has been saved.');
        }
    }
    
    /* Terms of Use */
    public function terms()
    {
        $checkEmpty = Term::get()->count();
        if (!$checkEmpty) {
            $content = $checkEmpty;
        } else {
            $content = Term::first()->toArray();
        }
        return view('backend.pages.terms', compact('content'));
    }

    public function saveTerms(SaveTermsRequest $request)
    {
        $input = $request->input();
        if (Term::get()->count()) {
            $content = Term::first();
            $content->update($input);
            return Redirect::back()->withFlashSuccess('The content has been updated.');
        } else {
            Term::create($input);
            return Redirect::back()->withFlashSuccess('The content has been saved.');
        }
    }

}
