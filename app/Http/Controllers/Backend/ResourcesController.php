<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Redirect;
use App\Models\Backend\ResourceCategory;
use App\Models\Backend\ResourceTopic;
use App\Models\Backend\Resource;
use App\Models\Backend\ResourceTopicFile;
use App\Http\Requests\Backend\SaveResourceCategoryRequest;
use App\Http\Requests\Backend\SaveResourceTopicRequest;

class ResourcesController extends Controller
{
    /* TOPICS */

    public function topics()
    {
        $topics = ResourceTopic::with('resourceCategories')->orderBy('id',
                'desc')->get();
        return view('backend.resources.topics.index', compact('topics'));
    }

    public function createTopic()
    {
        $categories = ResourceCategory::get()->pluck('title', 'id')->toArray();
        return view('backend.resources.topics.create', compact('categories'));
    }

    public function saveTopic(SaveResourceTopicRequest $request)
    {
        $files = $request->file();
        $input = $request->input();

        $resource_topic    = ResourceTopic::create([
                'title' => $input['title'],
                'order' => $input['order'],
//                    'is_client' => $input['is_client'],  // no idea about this field
                'image' => '',
                'title_bg_color' => (isset($input['title_bg_color'])) ? $input['title_bg_color']
                    : '#002d62',
                'resource_category_id' => $input['resource_category_id'],
                'content' => $input['content']
        ]);
        $resource_topic_id = $resource_topic->id;

        $this->_saveUploadedFiles($request, $resource_topic_id);
        return redirect()->route('admin.resources.topics')->withFlashSuccess('Resource Topic successfully created.');
    }

    private function _saveUploadedFiles($request, $resource_topic_id)
    {
        $fileArray                  = [];
        $files                      = $request->file();
        $input                      = $request->input();
        $resourceTopicFile          = ResourceTopicFile::where('resource_topic_id',
                $resource_topic_id);
        $existingResourceTopicFiles = $resourceTopicFile->get()->count();

        if (isset($files['upload_file'])) {
            $fileArray = $this->_createFileArray($input, $files);
            if ($existingResourceTopicFiles) {
                $resourceTopicFile->delete();
            }
            $this->_createResourceTopicFile($fileArray, $resource_topic_id);
        } else {
            if (!empty($input['upload_file_old'])) {
                $fileArray = $this->_updateFileArray($input);
                $resourceTopicFile->delete();
                $this->_createResourceTopicFile($fileArray, $resource_topic_id);
            } else {
                $resourceTopicFile->delete();
            }
        }
    }

    private function _createFileArray($input, $files)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text'] = $fileText;
            if (isset($files['upload_file'][$key])) {
                $fileArray[$key]['upload_file'] = $this->_uploadImages($files['upload_file'][$key],
                    'img/backend/resources/file_uploads');
            } else {
                $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
            }
        }
        return $fileArray;
    }

    private function _updateFileArray($input)
    {
        foreach ($input['file_text'] as $key => $fileText) {
            $fileArray[$key]['file_text']   = $fileText;
            $fileArray[$key]['upload_file'] = $input['upload_file_old'][$key];
        }
        return $fileArray;
    }

    private function _createResourceTopicFile($fileArray, $resource_topic_id)
    {
        foreach ($fileArray as $resourceTopicFile) {
            ResourceTopicFile::create([
                'resource_topic_id' => $resource_topic_id,
                'file_text' => $resourceTopicFile['file_text'],
                'upload_file' => $resourceTopicFile['upload_file'],
            ]);
        }
    }

    public function editTopic($id)
    {
        $topic      = ResourceTopic::find($id);
        $categories = ResourceCategory::get()->pluck('title', 'id')->toArray();
        return view('backend.resources.topics.create',
            compact('topic', 'categories'));
    }

    public function updateTopic(SaveResourceTopicRequest $request)
    {
//        dd('update topic');
        $files = $request->file();
        $input = $request->input();
        $topic = ResourceTopic::find($input['id']);

        $topic->update([
            'title' => $input['title'],
//            'is_client' => (int) $input['is_client'], //no idea about this field
            'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color']
                    : '#002d62',
            'order' => $input['order'],
            'image' => '',
            'resource_category_id' => $input['resource_category_id'],
            'content' => $input['content']
        ]);

        $this->_saveUploadedFiles($request, $topic->id);
        return redirect()->route('admin.resources.topics')->withFlashSuccess('Resource Topic successfully updated.');
    }

    public function deleteTopic(Request $request)
    {
        dd('delete');
        $input = $request->input();
        $topic = ResourceTopic::find($input['id']);
        $topic->delete();
        return $request->session()->flash('flash_success',
                'The Resource Topic has been successfully deleted.');
    }

    public function editOrder(Request $request)
    {
        $input = $request->input();

        $orders_array = ResourceTopic::get()->pluck('order')->toArray();
        if (isset($input['topic_id'])) {
            $topic         = ResourceTopic::where('id', $input['topic_id'])->first();
            $current_order = $topic->order;
        }

        if (in_array($input['order'], $orders_array)) { //duplicate
            if (!empty($topic)) { //existing topic
                if ($current_order != $input['order']) {
                    return response()->json(['message' => 'This order is in use!',
                            'status' => '0'], 200);
                } else {
                    if ($input['form']) {
                        return response()->json(['message' => 'This order number is available.',
                                'status' => '1'], 200);
                    } else {
                        $topic->update(['order' => $input['order']]);
                        return response()->json(['message' => 'The order has been updated.',
                                'status' => '1'], 200);
                    }
                }
            } else { //new topic
                return response()->json(['message' => 'This order is in use!', 'status' => '0'],
                        200);
            }
        } else { //not duplicate
            if ($input['form']) {
                return response()->json(['message' => 'This order number is available.',
                        'status' => '1'], 200);
            } else { //existing topic
                $topic->update(['order' => $input['order']]);
                return response()->json(['message' => 'The order has been updated.',
                        'status' => '1'], 200);
            }
        }
    }
    /* CATEGORIES */

    public function categories()
    {
        $categories = ResourceCategory::orderBy('id', 'desc')->get();
        return view('backend.resources.categories.index', compact('categories'));
    }

    public function createCategory()
    {
        return view('backend.resources.categories.create');
    }

    public function saveCategory(SaveResourceCategoryRequest $request)
    {
        $files = $request->file();
        $input = $request->input();
        if (!empty($files)) {
            $image = $this->_uploadImages($files['image'],
                'img/backend/resources/categories');
        }
        ResourceCategory::create([
            'title' => $input['title'],
            'order' => $input['order'],
            'title_bg_color' => (isset($input['title_bg_color'])) ? $input['title_bg_color']
                    : '#002d62',
            'image' => (isset($image)) ? $image : ''
        ]);
        return redirect()->route('admin.resources.categories')->withFlashSuccess('Resource Category successfully created.');
    }

    private function _uploadImages($file, $directory)
    {
        $fileName = date('d-m-y-h-i-s-').$file->getClientOriginalName();
        $file->move($directory, $fileName);
        return $fileName;
    }

    public function editCategory($id)
    {
        $category = ResourceCategory::find($id);
        return view('backend.resources.categories.create', compact('category'));
    }

    public function updateCategory(SaveResourceCategoryRequest $request)
    {
        $files    = $request->file();
        $input    = $request->input();
        $category = ResourceCategory::find($input['id']);

        $image = (isset($files['image'])) ? $this->_uploadImages($files['image'],
                'img/backend/resources/categories') : $category->image;
        $category->update([
            'title' => $input['title'],
            'order' => $input['order'],
            'title_bg_color' => ($input['title_bg_color']) ? $input['title_bg_color']
                    : '#002d62',
            'image' => $image
        ]);

        return redirect()->route('admin.resources.categories')->withFlashSuccess('Resource Category successfully updated.');
    }

    public function deleteCategory(Request $request)
    {
        $input           = $request->input();
        $inUse           = $this->_isCategoryInUse($input['id']);
        $totalCategories = ResourceCategory::get()->count();
        $message         = ($totalCategories == 1) ? 'You cannot delete all Resource Categories.'
                : 'This Resource Category is in use. You cannot delete it.';
        if ($inUse || $totalCategories == 1) {
            return $request->session()->flash('flash_warning', $message);
        } else {
            $category = ResourceCategory::find($input['id']);
            $category->delete();
            return $request->session()->flash('flash_success',
                    'The Resource Category has been successfully deleted.');
        }
    }

    private function _isCategoryInUse($id)
    {
        return ResourceTopic::where('resource_category_id', $id)->count();
    }

    public function editResourceCategoryOrder(Request $request)
    {
        $input = $request->input();

        $orders_array = ResourceCategory::get()->pluck('order')->toArray();
        if (isset($input['category_id'])) {
            $category      = ResourceCategory::where('id', $input['category_id'])->first();
            $current_order = $category->order;
        }

        if (in_array($input['order'], $orders_array)) { //duplicate
            if (!empty($category)) { //existing category
                if ($current_order != $input['order']) {
                    return response()->json(['message' => 'This order is in use!',
                            'status' => '0'], 200);
                } else {
                    if ($input['form']) {
                        return response()->json(['message' => 'This order number is available.',
                                'status' => '1'], 200);
                    } else {
                        $category->update(['order' => $input['order']]);
                        return response()->json(['message' => 'The order has been updated.',
                                'status' => '1'], 200);
                    }
                }
            } else { //new category
                return response()->json(['message' => 'This order is in use!', 'status' => '0'],
                        200);
            }
        } else { //not duplicate
            if ($input['form']) {
                return response()->json(['message' => 'This order number is available.',
                        'status' => '1'], 200);
            } else { //existing category
                $category->update(['order' => $input['order']]);
                return response()->json(['message' => 'The order has been updated.',
                        'status' => '1'], 200);
            }
        }
    }

    public function createResource()
    {
        $checkEmpty = Resource::get()->count();

        if (!$checkEmpty) {
            $data = $checkEmpty;
        } else {
            $data = Resource::first()->toArray();
        }

        return view('backend.resources.create', compact('data'));
    }

    public function saveResource(Request $request)
    {
//        dd($request->all());
        $input = $request->input();
        if (Resource::get()->count()) {
            $currentContent = Resource::first();
            $currentContent->update([
                'heading_1' => $input['heading_1'],
                'subheading_1' => $input['subheading_1'],
                'heading_2' => $input['heading_2'],
                'subheading_2' => $input['subheading_2']
            ]);

            return Redirect::back()->withFlashSuccess('The Resource page content has been Updated.');
        } else {
            Resource::create([
                'heading_1' => $input['heading_1'],
                'subheading_1' => $input['subheading_1'],
                'heading_2' => $input['heading_2'],
                'subheading_2' => $input['subheading_2']
            ]);
            return Redirect::back()->withFlashSuccess('The Resource page content has been saved.');
        }
    }
}