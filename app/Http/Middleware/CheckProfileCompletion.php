<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Models\Profile;

class CheckProfileCompletion {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $profile = Profile::where('user_id', Auth::id())->first(['profile_complete_step']);
            if (!$profile) {
                return redirect()->route('frontend.client.signupStepTwo', config('constant.subdomain'));
            } else if ($profile->profile_complete_step < 4) {
                return redirect()->route(config('constant.profile_completion_steps_routes')[$profile->profile_complete_step + 1], config('constant.subdomain'));
            }
        }
        return $next($request);
    }

}
