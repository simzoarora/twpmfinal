<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Access\User\User;

class NotificationEvent implements ShouldBroadcast
{

    use InteractsWithSockets,
        SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     * 
     */
//    public $user; 
//    public $clinetId;
//    public $notificationRoom;

//    public function __construct(User $user)
//    public function __construct(User $user)
////    public function __construct($notificationRoom)
//    {
//        $this->user = $user;
////        $this->notificationRoom=$notificationRoom;
//    }

    public $user;
//public function __construct(User $user)
public function __construct()
{
//    $this->user = $user;
}
    
    
    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
//        return new PrivateChannel('user.'. $this->user->id);
        return new Channel('test-event');
//        return new Channel('test-event');
    }

    public function broadcastWith()
    {
        return [
//            'data' => $this->clinetId 
            'data' => 'key'
        ];
    }

}
