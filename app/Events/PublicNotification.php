<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Notification_data;
use App\Models\Notification_room;

class PublicNotification implements ShouldBroadcast {

    use SerializesModels,
        InteractsWithSockets;

//    public $notification;
//    public $notificationRoom;

    /**
     * Create a new event instance.
     *
     * @return void
     */
//    public function __construct(Notification_room $notificationRoom) {
//        $this->notificationRoom = $notificationRoom;
//    }
    public function __construct() {
        
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn() {
//        return new PrivateChannel('channel-name');
//        return new Channel('public-notification-room-' . $this->notificationRoom->id);
        return new Channel('test-event');
    }

    public function broadcastWith() {
        return [
            'data' => 'key'
        ];
    }

}
