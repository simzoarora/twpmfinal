<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Notification_room;

class RoomEvents implements ShouldBroadcast
{
    use SerializesModels, InteractsWithSockets;
    public $notificationRoom;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Notification_room $notificationRoom)
    {
        //
         $this->notificationRoom = $notificationRoom;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
          return new PresenceChannel('room-events-' . $this->notificationRoom->id);
    }
}
