<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\Models\Access\User\User;
use App\Models\Notification_data;

class PrivateEvent implements ShouldBroadcast
{

    use InteractsWithSockets,
        SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
//    public $user;
    public $notification;

//    public function __construct(User $user,Notification_data $notificationData)
    public function __construct(Notification_data $notification)
    {
//        $this->user = $user;
        $this->notification = $notification;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
//         return new PrivateChannel('private-event-'.$this->user->id);
         return new PrivateChannel('private-event-'.$this->notification->receiver_id);
    }
}
