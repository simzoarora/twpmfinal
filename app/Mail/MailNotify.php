<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailNotify extends Mailable
{

    use Queueable,
        SerializesModels;

    public $name;
    public $email;
    public $user_message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $fileOriginalName)
    {
        $this->name = $name;
        $this->email = $email;
        $this->user_message = $fileOriginalName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pagalbuddy.lz@gmail.com', 'TWPM')
                        ->to($this->email, $this->name)
                        ->subject('Document Uploaded')
                        ->view('frontend.employee.email');
    }

}
