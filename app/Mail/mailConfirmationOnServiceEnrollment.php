<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class mailConfirmationOnServiceEnrollment extends Mailable
{

    use Queueable,
        SerializesModels;

    public $serviceName;
    public $userName;
    public $userEmail;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($serviceName,$userName,$userEmail)
    {
        $this->serviceName=$serviceName;
        $this->userName=$userName;
        $this->userEmail=$userEmail;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->from('pagalbuddy.lz@gmail.com', 'TWPM')
                        ->to($this->userEmail, $this->userName)
                        ->subject('Service Enrollment Completed')
                        ->view('frontend.emails.serviceEnrollment');
    }

}
