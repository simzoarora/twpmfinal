<?php

namespace App\Services;

class FileUpload {

    public static function uploadFile($file, $directoryPath) {
        $directory = public_path('/') . $directoryPath;
//        dd($directory);
        $fileName = date('d-m-y-h-i-s-') . preg_replace('/\s+/', '-', trim($file->getClientOriginalName()));
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        } if ($file->move($directory, $fileName)) {
            return $fileName;
        } return false;
    }

}
