<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services;

use App\Models\AttemptedQuestionAnswer;
Use Auth;
use DB;

/**
 * Description of CheckEnvelopeStatus
 *
 * @author pc8
 */
class CheckEnvelopeStatus
{

    //put your code here
    public static function checkStatus()
    {
        $docusign_email    = 'mk4677070@gmail.com';
        $docusign_password = 'manpreet@docusign';
        $integratorKey     = '2045e376-db32-4fbe-b292-c274718e65af';
//        $docisign_email    = env('DOCUSIGN_USERNAME');
//        $docisign_password = env('DOCUSIGN_PASSWORD');
//        $integratorKey     = env('DOCUSIGN_INTEGRATOR_KEY');
        $header            = "<DocuSignCredentials><Username>".$docusign_email."</Username><Password>".$docusign_password."</Password><IntegratorKey>".$integratorKey."</IntegratorKey></DocuSignCredentials>";
        $url               = "https://demo.docusign.net/restapi/v2/login_information";
        $curl              = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            array("X-DocuSign-Authentication: $header"));

        $json_response = curl_exec($curl);
        $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if ($status != 200) {
            return 'error';
        }
        $response     = json_decode($json_response, true);
        $accountId    = $response["loginAccounts"][0]["accountId"];
        $baseUrl      = $response["loginAccounts"][0]["baseUrl"];
        $envelop_data = AttemptedQuestionAnswer::where('envelope_id', '!=', null)->where('status', 1)->get()->toArray();
        if ($envelop_data != null) {
            foreach ($envelop_data as $info) {
                $envelopeId = $info['envelope_id'];
                $curl       = curl_init($baseUrl."/envelopes/".$envelopeId."/recipients");
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER,
                    array(
                    "X-DocuSign-Authentication: $header")
                );

                $json_response = curl_exec($curl);
                $status        = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                if ($status != 200) {
                    return response()->json($json_response, $status);
                }

                $result = json_decode($json_response, true);
                curl_close($curl);
                if ($result['signers'][0]['status'] == 'completed') {

                    DB::beginTransaction();
                    if (AttemptedQuestionAnswer::where('id', $info['id'])->update([
                            'status' => config('services.envelope_status.completed')])) {
                        DB::commit();
                    }
                    DB::rollBack();
                }
            }
        } else {
            return response()->json('no envelope exists', 401);
        }
        return response()->json('Invalid', 401);
    }
}