<?php

namespace App\Services;

class Unisharp
{

    public static function uploadFile($file, $directoryPath,$dirUniSharp=Null)
    {
        $directory = public_path('/') . $directoryPath;
        $directoryUni = public_path('/') . $dirUniSharp;
//        dd($directoryUni);
        $fileName = date('d-m-y-h-i-s-') . preg_replace('/\s+/', '-', trim($file->getClientOriginalName()));
        if (!file_exists($directory)) {
            mkdir($directory, 0777, true);
        } 
        if (!file_exists($directoryUni)) {
            mkdir($directoryUni, 0777, true);
        } 
        if ($file->move($directory, $fileName)) {
//            dump($directory.'/'.$fileName);
//            \File::copy($directory .'/'. $fileName,$directoryUni .'/'. $fileName);
            copy($directory .'/'. $fileName, $directoryUni .'/'. $fileName);
            return $fileName;
        } return false;
    }

}
