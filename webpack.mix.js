let mix = require('laravel-mix');

/*
|--------------------------------------------------------------------------
| Mix Asset Management
|--------------------------------------------------------------------------
|
| Mix provides a clean, fluent API for defining some Webpack build steps
| for your Laravel application. By default, we are compiling the Sass
| file for your application, as well as bundling up your JS files.
|
*/
//mix.setPublicPath('public');
//mix.setResourceRoot('resources/assets/');
mix.js('resources/assets/js/app.js', 'public/js/');
mix.js('resources/assets/js/sockets.js', 'public/js/sockets.js');



mix
// .scripts(
// [
// 'resources/assets/js/plugin/jquery-3.1.1.min.js',
// 'resources/assets/js/plugin/bootstrap.3.3.7.min.js',
// 'resources/assets/js/frontend/common.js'
//], 'resources/assets/js/master.js')
// .scripts([
// 'resources/assets/js/plugin/steps.min.js',
// 'resources/assets/js/plugin/validate.min.js',
// 'resources/assets/js/plugin/jquery-ui-selectBoxIt.min.js',
// 'resources/assets/js/plugin/jquery.selectBoxIt.min.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/plugin//additonal_methods.min.js'
// ], 'resources/assets/js/master-plugin.js')
// .scripts([
// 'resources/assets/js/plugin/jquery-ui-selectBoxIt.min.js',
// 'resources/assets/js/plugin/jquery.selectBoxIt.min.js'
// ], 'resources/assets/js/master-selectBoxIt.js')
//
// .scripts([
// 'resources/assets/js/plugin/underscore.min.js',
// 'resources/assets/js/plugin/moment.min.js',
// 'resources/assets/js/plugin/datePicker.min.js',
// ], 'resources/assets/js/master-services-plugin.js')
//
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/underscore.min.js',
// 'resources/assets/js/plugin/file-uploader.js',
// 'resources/assets/js/frontend/client/life-insurance.js',
// ], 'public/js/life-insurance.js') 
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/master-services-plugin.js',
// 'resources/assets/js/frontend/client/services-questions.js'
// ], 'public/js/services-questions.js')

// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/master-services-plugin.js',
// 'resources/assets/js/plugin/file-uploader.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/frontend/client/annual-portfolio-review.js'
// ], 'public/js/annual-portfolio-review.js')
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/underscore.min.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/frontend/client/educational-consultation.js'
// ], 'public/js/educational-consultation.js')
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/master-services-plugin.js',
// 'resources/assets/js/frontend/client/goal-specific-financial-planning.js'
// ], 'public/js/goal-specific-financial-planning.js')
//
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/master-services-plugin.js',
// 'resources/assets/js/plugin/ion.rangeSlider.js',
// 'resources/assets/js/plugin/ion.rangeSlider.min.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/frontend/client/investment-management.js'
// ], 'public/js/investment-management.js')
//
//
//.scripts([
//'resources/assets/js/master-plugin.js',
//'resources/assets/js/frontend/common.js',
//'resources/assets/js/master-services-plugin.js',
//'resources/assets/js/frontend/client/comprehensive-planning.js'
//], 'public/js/comprehensive-planning.js')
//
//
// .scripts([
// 'resources/assets/js/dist/frontend.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// ], 'public/js/frontend.js')
//
// .scripts([
//
// 'resources/assets/js/plugin/jquery.min.js',
// 'resources/assets/js/plugin/bootstrap.3.3.7.min.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/frontend/home.js'
// ], 'public/js/home.js')
//
 .scripts([
 'resources/assets/js/master-plugin.js',
 'resources/assets/js/frontend/common.js',   
 'resources/assets/js/frontend/client/financial-information.js'
 ], 'public/js/financial-information.js')
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/client/contact-information.js'
// ], 'public/js/contact-information.js')
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/plugin/jquery-ui.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/underscore.min.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/frontend/services.js'
// ], 'public/js/services.js')
//
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/plugin/jquery-ui.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/frontend/about.js',
// ], 'public/js/about.js')
//
// .scripts([
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/plugin/isotope.pkgd.min.js',
// 'resources/assets/js/plugin/imagesloaded.pkgd.min.js',
//// https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.3/imagesloaded.pkgd.min.js
//// 'plugin/moment.min.js', 
// 'resources/assets/js/frontend/blog.js'
// ], 'public/js/blog.js')
//
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/underscore.min.js',
// 'resources/assets/js/frontend/resources.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// ], 'public/js/resources.js') 

// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/plugin/sharrre.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/single-blog.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// ], 'public/js/single-blog.js')
//
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// ], 'public/js/privacy.js')
//
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js'
// ], 'public/js/legal.js')
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/includes/navbar.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js'
// ], 'public/js/terms.js')
//
// .scripts([
// 'resources/assets/js/plugin/jquery-3.1.1.min.js',
// 'resources/assets/js/frontend/client/client.js',
// 'resources/assets/js/frontend/client/client-login.js'
// ], 'public/js/client-login.js')
//
// .scripts([
// 'resources/assets/js/frontend/client/client.js'
// ], 'public/js/step-one.js')
// .scripts([
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/client/client.js',
// 'resources/assets/js/frontend/client/step-two.js'
// ], 'public/js/step-two.js')
//
// .scripts([
// 'resources/assets/js/master-selectBoxIt.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/plugin/owl.carousel.min.js',
// 'resources/assets/js/frontend/client/dashboard.js'
// ], 'public/js/dashboard.js')
//
// //client mobile verification(step 2 page) js
//// .scripts([
//// 'resources/assets/js/frontend/client/client.js',
//// 'resources/assets/js/frontend/client/step-three.js'
//// ], 'public/js/step-three.js')
//// .scripts([x
//// 'resources/assets/js/frontend/client/client.js',
//// ], 'public/js/step-four.js')
////
//// .scripts([
//// 'resources/assets/js/plugin/underscore.min.js',
//// 'resources/assets/js/frontend/client/client.js',
//// 'resources/assets/js/frontend/client/step-five.js'
//// ], 'public/js/step-five.js')
//
//.scripts([
//'resources/assets/js/master-selectBoxIt.js',
//'resources/assets/js/frontend/common.js',
//'resources/assets/js/frontend/client/dashboard.js',
//'resources/assets/js/frontend/client/client-profile.js'
//], 'public/js/client-profile.js')

//.scripts([
//'resources/assets/js/frontend/client/dashboard.js',
//'resources/assets/js/master-selectBoxIt.js',
//'resources/assets/js/plugin/validate.min.js',
//'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
//'resources/assets/js/frontend/common.js',
//'resources/assets/js/plugin/dropzone.js',
//'resources/assets/js/plugin/toastr/toastr.min.js',
//'resources/assets/js/frontend/client/documents.js',
//'resources/assets/js/plugin/owl.carousel.min.js',
//'resources/assets/js/plugin/moment.min.js',
//'resources/assets/js/plugin/datePicker.min.js',
//'resources/assets/js/frontend/employee.js',
//'resources/assets/js/plugin/file-uploader.js',
//], 'public/js/employee.js')
// .scripts([
// 'resources/assets/js/frontend/client/client.js',
// 'resources/assets/js/frontend/client/step-payment.js'
// ], 'public/js/step-payment.js')
 .scripts([
 'resources/assets/js/master-plugin.js',
 'resources/assets/js/frontend/common.js'
 ], 'public/js/signUp.js')
//
.scripts([ 
'resources/assets/js/master-plugin.js',
'resources/assets/js/frontend/common.js',
'resources/assets/js/frontend/client/sign-up-questions.js'
], 'public/js/sign-up-questions.js')
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/plugin/dropzone.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/frontend/client/documents.js'
// ], 'public/js/documents.js')
//
// .scripts([
// 'resources/assets/js/master-plugin.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/owl.carousel.min.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/frontend/client/service-enrollment.js'
// ], 'public/js/service-enrollment.js')
//
//.scripts([
//'resources/assets/js/plugin/owl.carousel.min.js',
//'resources/assets/js/plugin/jquery-ui-selectBoxIt.min.js',
//'resources/assets/js/plugin/jquery.selectBoxIt.min.js',
//'resources/assets/js/frontend/client/dashboard.js',
//'resources/assets/js/plugin/toastr/toastr.min.js',
//'resources/assets/js/frontend/common.js',
//'resources/assets/js/frontend/client/recommended-services.js'
//], 'public/js/recommended-services.js')
// 
// .scripts([
// 'resources/assets/js/dist/backend.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/backend/custom.js'
// ], 'public/js/backend.js')
//
/**
//// * Home page SCSS stylesheets
//// */
// .sass('resources/assets/sass/frontend/pages/home2.scss', 'public/css/home2.css').options({
// processCssUrls: false
//})
// /**
// // * Process frontend SCSS stylesheets
// // */
// .sass('resources/assets/sass/frontend/pages/frontend.scss', 'public/css/frontend.css').options({
// processCssUrls: false
//})
// /**
// // * Services page SCSS stylesheets
// // */
// .sass('resources/assets/sass/frontend/pages/services.scss', 'public/css/services.css').options({
// processCssUrls: false
//})
////// * About page SCSS stylesheets
// .sass('resources/assets/sass/frontend/pages/about.scss', 'public/css/about.css').options({
// processCssUrls: false
//})
//
//
//
////// * Blog page SCSS stylesheets
////
// .sass('resources/assets/sass/frontend/blog/blog.scss', 'public/css/blog.css').options({
// processCssUrls: false
//})
//
//
//
//// * Resources page SCSS stylesheets
// .sass('resources/assets/sass/frontend/pages/resources.scss', 'public/css/resources.css').options({
// processCssUrls: false
//})
//
//// * Single-blog page SCSS stylesheets
//// 
// .sass('resources/assets/sass/frontend/blog/single-blog.scss', 'public/css/single-blog.css').options({
// processCssUrls: false
//})
//
////* Privacy SCSS stylesheets
// .sass('resources/assets/sass/frontend/pages/privacy.scss', 'public/css/privacy.css').options({
// processCssUrls: false
//})
// * Legal SCSS stylesheets
.sass('resources/assets/sass/frontend/pages/legal.scss', 'public/css/legal.css').options({
processCssUrls: false
})
//
// * Terms SCSS stylesheets
//.sass('resources/assets/sass/frontend/pages/terms.scss', 'public/css/terms.css').options({
//processCssUrls: false
//})
//
//// * Client Login SCSS stylesheets
// .sass('resources/assets/sass/frontend/client/clientLogin.scss', 'public/css/clientLogin.css').options({
// processCssUrls: false
//})
//
//
////* Client Step1 SCSS stylesheets
// .sass('resources/assets/sass/frontend/client/client.scss', 'public/css/client.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/dashboard.scss', 'public/css/dashboard.css').options({
// processCssUrls: false
//})
//
.sass('resources/assets/sass/frontend/client/service-questions.scss', 'public/css/service-questions.css').options({
processCssUrls: false
})
// .sass('resources/assets/sass/frontend/client/profile.scss', 'public/css/profile.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/documents.scss', 'public/css/documents.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/employee/employee-dashboard-services.scss', 'public/css/employee-dashboard-services.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/preview.scss', 'public/css/preview.css').options({
// processCssUrls: false  
//})
.sass('resources/assets/sass/frontend/client/signUpOne.scss', 'public/css/signUpOne.css').options({
processCssUrls: false
})
// .sass('resources/assets/sass/frontend/client/signUpSix.scss', 'public/css/signUpSix.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/signUpTwelve.scss', 'public/css/signUpTwelve.css').options({
// processCssUrls: false
//})
.sass('resources/assets/sass/frontend/client/servicesQuestion.scss', 'public/css/servicesQuestion.css').options({
processCssUrls: false
})
// .sass('resources/assets/sass/frontend/client/selectedService.scss', 'public/css/selectedService.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/signUpSixteen.scss', 'public/css/signUpSixteen.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/twpmDashboard.scss', 'public/css/twpmDashboard.css').options({
// processCssUrls: false
//})
// .sass('resources/assets/sass/frontend/client/finish-account.scss', 'public/css/finish-account.css').options({
// processCssUrls: false
//})
 .sass('resources/assets/sass/frontend/client/single-service-questions.scss', 'public/css/single-service-questions.css').options({
 processCssUrls: false
})
// .sass('resources/assets/sass/backend/backend.scss', 'public/css/backend.css').options({
// processCssUrls: false
//});








//mix.js([
// 'resources/assets/js/plugin/jquery-3.1.1.min.js',
// 'resources/assets/js/plugin/bootstrap.3.3.7.min.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/plugin/owl.carousel.min.js',
// 'resources/assets/js/frontend/client/dashboard.js'
// ], 'public/js/dashboard.js');
//mix.js([
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/plugin/jquery-ui-selectBoxIt.min.js',
// 'resources/assets/js/plugin/jquery.selectBoxIt.min.js',
// 'resources/assets/js/plugin/sweetalert/sweetalert.min.js',
// 'resources/assets/js/frontend/common.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/frontend/client/documents.js',
// 'resources/assets/js/frontend/employee.js'
// ], 'public/js/employee.js');
// mix.js([
// 'resources/assets/js/plugin/jquery-3.1.1.min.js',
// 'resources/assets/js/plugin/bootstrap.3.3.7.min.js',
// 'resources/assets/js/plugin/owl.carousel.min.js',
// 'resources/assets/js/frontend/client/dashboard.js',
// 'resources/assets/js/plugin/toastr/toastr.min.js',
// 'resources/assets/js/frontend/client/recommended-services.js'
// ], 'public/js/recommended-services.js');


// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.preact(src, output); <-- Identical to mix.js(), but registers Preact compilation.
// mix.coffee(src, output); <-- Identical to mix.js(), but registers CoffeeScript compilation.
// mix.ts(src, output); <-- TypeScript support. Requires tsconfig.json to exist in the same folder as webpack.mix.js
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.fastSass('src', output); <-- Alias for mix.standaloneSass().
// mix.less(src, output);
// mix.stylus(src, output);
// mix.postCss(src, output, [require('postcss-some-plugin')()]);
// mix.browserSync('my-site.test');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.babelConfig({}); <-- Merge extra Babel configuration (plugins, etc.) with Mix's default.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.extend(name, handler) <-- Extend Mix's API with your own components.
// mix.options({
// extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
// globalVueStyles: file, // Variables file to be imported in every component.
// processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
// purifyCss: false, // Remove unused CSS selectors.
// uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
// postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });