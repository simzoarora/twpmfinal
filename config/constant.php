<?php

return [
    'backend' => [
        'blogs' => [
            'default_status' => 0,
            'active' => 1
        ]
    ],
    'phone_type' => [
        'Mobile' => 1,
        'Home' => 2,
        'Work' => 3,
        'Other' => 4
    ],
    'phone_type_inverse' => [
        1 => 'Mobile',
        2 => 'Home',
        3 => 'Work',
        4 => 'Other'
    ],
    'sex' => [
        'Male' => 1,
        'Female' => 2
    ],
    'sex_inverse' => [
        1 => 'Male',
        2 => 'Female'
    ],
    'profile_completion_steps_routes' => [
        1 => 'frontend.client.signupStepTwo',
        2 => 'frontend.client.signupStepThree',
        3 => 'frontend.client.signupStepFour',
        4 => 'frontend.client.signupStepFive',
    ],
    'profile_completion_steps_routes_inverse' => [
        'signupStepTwo' => 1,
        'signupStepThree' => 2,
        'signupStepFour' => 3,
        'signupStepFive' => 4,
    ],
    'service_question_types' => [
        1 => 'Multiple Choice Multiple Answer',
        2 => 'Multiple Choice Single Answer',
        3 => 'Single Line Answer',
        4 => 'Multiple Line Answer',
        5 => 'Upload a Single File',
        6 => 'Upload Multiple Files',
        7 => 'Dropdown'
    ],
    'service_question_types_options' => [
        'mcma' => 1, //Multiple Choice Multiple Answer
        'mcsa' => 2, //Multiple Choice Single Answer
    ],
    'service_question_types_options_inverse' => [
        'mcma' => 1, //Multiple Choice Multiple Answer
        'mcsa' => 2, //Multiple Choice Single Answer
        'sla' => 3, //Single Line Answer
        'mla' => 4, //Multiple Line Answer
        'usl' => 5, //Upload a Single File
        'uml' => 6, //Uplaod Multiple Files
        'dpdwn' => 7 //Dropdown
    ],
    'client_file_upload_path' => 'files/client/',
    'subdomain' => 'client',
    'base_url' => 'https://client.'. env('MAIN_DOMAIN').'/',
    'question_type' => [
        'dropdown' => 1,
        'mcma' => 2,
        'mcsa' => 3,
        'number' => 4,
        'select_box' => 5,
        'text' => 6
    ],
    'employement_status' => [
        NULL => 'Please select one',
        1 => 'Employed (full-time)',
        2 => 'Employed (part-time)',
        3 => 'Self-employed',
        4 => 'Student',
        5 => 'Retired',
        6 => 'Homemaker',
        7 => 'Not employed'
    ],
    'tax_filing_status' => [
        NULL => 'Please select one',
        1 => 'Single',
        2 => 'Head of Household',
        3 => 'Qualifying Widow(er)',
        4 => 'Married Filing jointly',
        5 => 'Married Filing Separately',
        6 => 'Married Filing Separately (living apart)',
    ],
    'tax_filing_status_inverse' => [
        'single' => 1,
        'hoh' => 2, //Head of Household
        'qw' => 3, //Qualifying Widow(er)
        'mfj' => 4, // Married Filing jointly
        'mfs' => 5, //'Married Filing Separately'
        'mfsla' => 6 //'Married Filing Separately (living apart)',
    ],
    'security_question_primary' => [
        1 => "What is your best friend's name?",
        2 => "What was the name of your childhood pet?",
        3 => "What is your favorite holiday?"
    ],
    'security_question_secondary' => [
        1 => "What is the name of the street you grew up on?",
        2 => "What was your high school mascot?",
        3 => "What was the model of your first car?"
    ],
    'security_question_inverse' => [
        'best_friend' => 1,
        'childhood_pet' => 2,
        'favorite_holiday' => 3
    ],
    'security_question_type' => [
        'primary' => 1,
        'secondary' => 2
    ],
    'services' => [
        1 => 'Comprehensive Financial Planning & Management',
        2 => 'Investment Management',
        3 => 'One-Time Comprehensive Financial Planning',
        4 => 'Goal-Specific Financial Planning',
        5 => 'Annual Portfolio Review',
        6 => 'Semi-Annual 401(k) Review',
        7 => 'Stock Option Exercise',
        8 => 'Life Insurance or Long-Term Care Insurance Review',
        9 => 'Educational Consultation',
        10 => 'Debt Reduction Consultation'
    ],
    'pre_defined_options_for_service_recommended_questions' => [
        1 => 'Managing my cashflow and budget',
        2 => 'Reducing my debt',
        3 => 'Do I have enough Life Insurance',
        4 => 'Is my will properly alingned with my wishes and assets?',
        5 => 'Paying for education',
        6 => 'Am I properly investing',
        7 => 'I need to understand financial matters better',
        8 => 'I would like to learn more about investing',
        9 => 'I need to know how to make my investments make more income for me',
        10 => 'Should I lease or buy a car',
        11 => 'Should I rent or buy a house',
        12 => 'What kind of mortgage is best for me?',
        13 => 'I need help saving for a specific goal (house, car, vacation, home improvement, etc.)',
        14 => 'I need help reducing my taxes',
        15 => "Should I contribute to my employer's retirement plan",
        16 => 'How should I invest my 401(k) or other retirement plan',
        17 => 'Will I have enough money in retirement',
        18 => 'How should I manage my employment Stock Options, Restricted Stock and/or Stock Purchase Plan',
        19 => "Should I take advantage of my employer's Deferred Compensation plan",
        20 => 'I need help planning for a special needs child',
        21 => 'I need help managing my investments',
        22 => 'I would like a second opinion on how I am investing in my portfolio'
    ],
    'services_ids'=>['1','2','3','4','5','6','7','8','9','10'],
    'questions' => [
        'general_life' => 'general_life',
        'general' => 'general',
        'personal' => 'personal',
        'comprehensive_life_insurance_id' => 88,
        'comprehensive_spouse_life_insurance_id' => 94,
        'comprehensive_disablity_insurance_id' => 89,
        'comprehensive_spouse_disablity_insurance_id' => 95,
        'your_liabilities' => 'your liabilities',
        'new' => 'new',
        'states' => 'states',
        'income_tax_topic_id' => 47,
        'goal_topic_id' => 41,
        'topic_ids_to_skip_add_icon' => [91, 97],
        'life_insurance_policy_topic_id' => 27,
        'stock_service_id' => 9,
        'annual_service_id' => 7,
        'semi_annual_service_id' => 8,
        'life_insurance_service_id' => 3,
        'spourse_name_question_id' => 1575,
        'services_containing_select_inputs' => [7, 8, 3, 2],
        'read_only_question_ids' => [401, 402, 1575, 1576, 1565, 1569, 1570, 1571, 1573, 1566, 15, 117, 353, 364, 386, 1407, 18, 26, 356, 365, 389,1567,1568],
        'not_required_question_ids' => [403, 404, 1578, 1579,917,945,925,952,918,1211,1220],
        'disabled_dropdown_question_ids' => [1574, 357, 121, 17, 355, 363, 388, 1572,431],
        'spouse_name_autofill_question_ids' => [1575, 1576],
        'tax_filling_status_ids' => [17,355,363,388],
        'tax_filling_status_group_ids' => [121,357,363,1574],
        'household_income_question_ids' => [15,117,353,364,386,1407],
        'taxable_income_question_ids' => [122,387,16,354],
        'tax_bracket_question_ids' => [18,29,356,365,389],
    ]
    , 'adobe_eSignature' => [
//        'API_URL' => 'https://secure.na2.echosign.com/',
//        'CLIENT_ID'=>'CBJCHBCAABAAcZcnLoxTow7ah_rJBOOAH9z6IKymTB25',
//        'CLIENT_SECRET'=>'OgjSjDIiao8UD5SDQFh5iNzXA5dJLlZs',
        'API_URL' => 'https://secure.na1.echosign.com/',
        'CLIENT_ID' => env('ADOBE_CLIENT_ID'),
        'CLIENT_SECRET' => env('ADOBE_CLIENT_SECRET'),
        'REFRESH_TOKEN' => env('ADOBE_REFRESH_TOKEN'),
        'approver_email' => env('ADOBE_APPROVER_EMAIL'),
        'api_access_point' => 'https://api.na1.echosign.com/',
        'document_upload_url' => 'uploadedClientDocuments/'
    ]
];
