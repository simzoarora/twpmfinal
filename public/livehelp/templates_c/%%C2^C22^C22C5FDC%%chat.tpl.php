<?php /* Smarty version 2.6.27, created on 2018-02-13 13:46:28
         compiled from default/chat.tpl */ ?>
<div id="chatstack-container" class="chatstack-container app closed">
	<div class="chatstack-app">
		<div class="logo"></div>
		<div class="tab"></div>
		<div class="status text"><?php echo $this->_tpl_vars['LOCALE']['online']; ?>
</div>
		<div class="campaign"></div>
		<div class="close parent">
			<div class="close button CloseButton sprite embed expand" title="Close"></div>
		</div>
		<div class="notification"><span></span></div>
		<div class="operator container">
			<div class="image"></div>
			<div class="foreground"></div>
			<div class="background">
				<div class="name"></div>
				<div class="department"></div>
			</div>
		</div>
		<div class="body">
			<div class="details button collapse sprite Expand" title="Expand"></div>
			<div class="chatting">
				<div class="scroll">
					<div class="waiting" data-lang-key="thankyoupatience"><?php echo $this->_tpl_vars['LOCALE']['thankyoupatience']; ?>
</div>
					<div class="messages">
						<div class="initiate chat">
							<div class="flex initiate">
								<div class="name"></div>
								<div class="avatar"></div>
								<div class="message" data-id="-1"></div>
							</div>
						</div>
					</div>
					<div class="end">
						<div class="rating parent"><?php echo $this->_tpl_vars['LOCALE']['rateyourexperience']; ?>
:<br/>
							<div class="rating container">
								<div class="rating option"></div>
								<div class="rating option neutral" title="Neutral" data-rating="2"></div>
								<div class="rating option"></div>
							</div>
						</div>
						<div class="restart"><?php echo $this->_tpl_vars['LOCALE']['closedusermessage']; ?>
 <a href="#"><?php echo $this->_tpl_vars['LOCALE']['restartchat']; ?>
</a></div>
					</div>
				</div>
			</div>
			<div class="prechat">
				<div class="welcome title"><?php echo $this->_tpl_vars['LOCALE']['welcome']; ?>
</div>
				<div class="welcome details"><?php echo $this->_tpl_vars['LOCALE']['enterguestdetails']; ?>
</div>
				<div class="flex left grouped">
					<div class="avatar"></div>
					<div class="name"></div>
					<div class="message bubble received left">
						<div></div>
					</div>
				</div>
				<div class="inputs" <?php echo $this->_tpl_vars['rtl']; ?>
>
					<div class="name container">
						<input class="name input" type="text" tabindex="100" placeholder="<?php echo $this->_tpl_vars['LOCALE']['name']; ?>
" <?php echo $this->_tpl_vars['dir']; ?>
/>
						<div class="name error sprite" title="Name Required"></div>
					</div>
					<div class="email container">
						<input class="email input" type="email" tabindex="101" placeholder="<?php echo $this->_tpl_vars['LOCALE']['email']; ?>
" <?php echo $this->_tpl_vars['dir']; ?>
/>
						<div class="email error sprite" title="Email Required"></div>
					</div>
					<label class="department label" <?php echo $this->_tpl_vars['rtl']; ?>
><?php echo $this->_tpl_vars['LOCALE']['department']; ?>
<br/>
						<div class="department container">
							<select class="department input" tabindex="102" placeholder="<?php echo $this->_tpl_vars['LOCALE']['department']; ?>
" <?php echo $this->_tpl_vars['dir']; ?>
></select>
							<div class="department error sprite" title="Department Required"></div>
						</div>
					</label>
					<label class="question label" <?php echo $this->_tpl_vars['rtl']; ?>
><?php echo $this->_tpl_vars['LOCALE']['question']; ?>
<br/>
						<div class="question container">
							<textarea class="question input" tabindex="103" placeholder="<?php echo $this->_tpl_vars['LOCALE']['question']; ?>
" <?php echo $this->_tpl_vars['dir']; ?>
></textarea>
							<div class="question error sprite" title="Question Required"></div>
						</div>
					</label>
					<div class="connect parent">
						<div class="connect button" tabindex="104"><?php echo $this->_tpl_vars['LOCALE']['connect']; ?>
</div>
					</div>
					<div class="blocked details"><?php echo $this->_tpl_vars['LOCALE']['chatsessionblocked']; ?>
</div>
					<div class="blocked container">
						<div style="margin-top:5px; left:15px">
							<div class="description"><?php echo $this->_tpl_vars['LOCALE']['accessdenied']; ?>
<br/><?php echo $this->_tpl_vars['LOCALE']['blockedchatsession']; ?>
</div>
							<div style="text-align: center; margin: 10px 0">
								<div class="blocked close button"><?php echo $this->_tpl_vars['LOCALE']['closechat']; ?>
</div>
							</div>
						</div>
					</div>
				</div>
				<a href="https://www.chatstack.com" target="_blank" class="brand" style="display: <?php echo $this->_tpl_vars['style']; ?>
">
					<div class="icon"></div>
				</a>
				<div class="connecting">
					<div class="loading-inner ball-clip-rotate">
						<div></div>
					</div>
					<div class="text"><?php echo $this->_tpl_vars['LOCALE']['connecting']; ?>
</div>
				</div>
			</div>
			<div class="alert">
				<div class="message"></div>
			</div>
			<div class="message container">
				<div class="toolbar">
					<div title="<?php echo $this->_tpl_vars['LOCALE']['emailchat']; ?>
" class="icon email"></div>
					<div title="<?php echo $this->_tpl_vars['LOCALE']['togglesound']; ?>
" class="icon sound"></div>
					<div title="<?php echo $this->_tpl_vars['LOCALE']['switchpopupwindow']; ?>
" class="icon maximise"></div>
					<div title="<?php echo $this->_tpl_vars['LOCALE']['feedback']; ?>
" class="icon rating"></div>
					<div title="<?php echo $this->_tpl_vars['LOCALE']['disconnect']; ?>
" class="icon disconnect"></div>
				</div>
				<div class="typing">
					<div class="sprite Typing"></div>
					<span></span>
				</div>
				<textarea class="message textarea" type="submit" placeholder="<?php echo $this->_tpl_vars['LOCALE']['enteryourmessage']; ?>
" <?php echo $this->_tpl_vars['dir']; ?>
></textarea>
				<div title="Smilies" class="icon smilies"></div>
				<a href="https://www.chatstack.com" target="_blank" class="brand" style="display: <?php echo $this->_tpl_vars['style']; ?>
"><div class="icon"></div></a>
			</div>
			<div id="smilies-tooltip" class="smilies tooltip"></div>
			<iframe id="chatstack-download"></iframe>
			<div class="disconnect dialog">
				<div class="title"><?php echo $this->_tpl_vars['LOCALE']['disconnecttitle']; ?>
</div>
				<div class="description"><?php echo $this->_tpl_vars['LOCALE']['disconnectdescription']; ?>
</div>
				<div class="success button" title="<?php echo $this->_tpl_vars['LOCALE']['disconnect']; ?>
">
					<div class="success icon"></div>
				</div>
				<div class="cancel button" title="<?php echo $this->_tpl_vars['LOCALE']['cancel']; ?>
">
					<div class="cancel icon"></div>
				</div>
			</div>
		</div>
	</div>
</div>