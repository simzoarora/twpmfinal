/*global Chatstack*/
(function(d, prefix, undefined) { // eslint-disable-line no-unused-vars

	// Container
	var id = prefix + '-container';
	var c = d.createElement('div');
	c.id = id;
	c.className = id;
	c.setAttribute('style', 'display: block !important; fixed !important; width: 0 !important; height: 0 !important; bottom: 0 !important; right: 0 !important; z-index: 2147483647 !important');
	d.body.appendChild(c);

	function iframe(id, parent, scrolling) {
		var i = d.createElement('iframe');
		i.id = id;
		i.name = id;
		i.className = id;
		i.scrolling = (scrolling) ? 'yes' : 'no';
		i.setAttribute('allowFullScreen', '');
		i.setAttribute('style', 'border: none !important');
		parent.appendChild(i);

		var doc = i.contentWindow.document;
		doc.open('text/html', 'replace');
		doc.write('<!doctype html><head></head><body></body></html>');
		doc.close();

		return i;
	}

	// JavaScript
	var i = iframe(prefix + '-js-frame', c);

	// Launcher
	iframe(prefix + '-launcher-frame', c);

	var ap = d.createElement('div');
	ap.id = prefix + '-app';
	ap.className = prefix + '-app parent';
	ap.setAttribute('style', 'z-index: 2147483647 !important');
	c.appendChild(ap);

	// App
	iframe(prefix + '-app-frame', ap);

	var s = d.createElement('script');
	s.type = 'text/javascript';
	s.charset = 'utf-8';
	s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + Chatstack.server + '/livehelp/scripts/app.js';

	var w = i.contentWindow;
	w.Chatstack = Chatstack;
	w.document.head.appendChild(s);
})(document, 'chatstack');