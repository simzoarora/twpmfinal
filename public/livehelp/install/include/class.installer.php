<?php
/*
Chatstack - https://www.chatstack.com
Copyright - All Rights Reserved - Stardevelop Pty Ltd

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from Stardevelop Pty Ltd (https://www.chatstack.com)

You may make modifications, but only for your own
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior
written consent is expressly forbidden. Obtain
permission before redistributing this program over
the Internet or in any other medium.  In all cases
copyright and header must remain intact.
*/
namespace stardevelop\chatstack;

use PDO;


class Installer {

  public $prefix = '';
  public $directory = './';
  public $config = '';
  public $error = false;

  private $connected = false;
  private $writable = false;


  public function __construct($host, $port, $database, $username, $password) {
    // Open MySQL Connection
    $this->connected = true;
    try {

      $connection = sprintf('mysql:host=%s;port=%d;dbname=%s;charset=utf8', $host, $port, $database);
      $db = new PDO($connection, $username, $password);

      ORM::set_db($db);

    } catch (\PDOException $ex) {
      $this->error = $ex->getMessage();
      $this->connected = false;
    }
  }

  public function install() {
    if ($hits->connected) {
      $schema = file($this->directory . 'mysql.schema.txt');
      $dump = '';
      foreach ($schema as $key => $line) {
        if (trim($line) != '' && substr(trim($line), 0, 1) != '#') {
          $line = str_replace('prefix_', $this->prefix, $line);
          $dump .= trim($line);
        }
      }

      $dump = trim($dump, ';');
      $tables = explode(';', $dump);

      foreach ($tables as $key => $sql) {
        $result = ORM::raw_execute($sql);
        if ($result == false) {
          $statement = ORM::get_last_statement();
          $errorinfo = $statement->errorInfo();
          if (!empty($errorinfo) && is_array($errorinfo)) {
            $this->error = 'Unable to create the MySQL database schema ( MySQL Error: [' . $errorinfo[0] . '] ' . $errorinfo[2] . ' ).  Please contact technical support.';
          } else {
            $this->error = 'Unable to create the MySQL database schema.  Please contact technical support.';
          }
          break;
        }
      }
      unset($dump);
      unset($tables);

      if (empty($this->error)) {

        // Truncate settings
        $query = 'TRUNCATE ' . $this->prefix . 'settings';
        ORM::raw_execute($query);

        // Remove .www. if at the start of string
        $domain = $_SERVER['SERVER_NAME'];
        if (substr($domain, 0, 4) == 'www.') {
          $domain = substr($domain, 4);
        }

        // Insert / Update Settings
        $settings = file_get_contents($this->directory . 'mysql.data.settings.txt');
        $settings = json_decode($settings, true);
        foreach ($settings as $key => $value) {

          // Update Settings
          if ($value['name'] == 'Email') {
            $value['value'] = $offlineemail;
          } else if ($value['name'] == 'Domain') {
            $value['value'] = $domain;
          } else if ($value['name'] == 'URL') {
            $value['value'] = $address;
          } else if ($value['name'] == 'OnlineLogo') {
            $value['value'] = $address . '/livehelp/locale/en/images/Online.png';
          } else if ($value['name'] == 'OfflineLogo') {
            $value['value'] = $address . '/livehelp/locale/en/images/Offline.png';
          } else if ($value['name'] == 'BeRightBackLogo') {
            $value['value'] = $address . '/livehelp/locale/en/images/BeRightBack.png';
          } else if ($value['name'] == 'AwayLogo') {
            $value['value'] = $address . '/livehelp/locale/en/images/Away.png';
          } else if ($value['name'] == 'AuthKey') {

            $key = '';
            $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ`~!@#$%^&*()-_=+[{]}\|:\'",<.>/?';
            for ($index = 0; $index < 255; $index++) {
              $number = rand(1, strlen($chars));
              $key .= substr($chars, $number - 1, 1);
            }

            $value['value'] = $key;
          } else if ($value['name'] == 'LastUpdated') {
            $value['value'] = date('Y-m-d H:i:s', time());
          }

          // Insert Setting
          ORM::configure('logging', true);
          $setting = Setting::create();
          $setting->name = $value['name'];
          $setting->value = $value['value'];
          $result = $setting->save();

          if ($result == false) {
            $this->error = 'Unable to insert the Live Help settings.  Please contact technical support.';
            break;
          }

        }
        unset($settings);

        if (empty($this->error)) {

          // Countries / Telephone Codes
          $countries = file_get_contents($this->directory . 'mysql.data.countries.txt');
          $countries = json_decode($countries, true);
          foreach ($countries as $key => $value) {

            // Insert Country
            $country = Country::create();
            $country->code = $value['code'];
            $country->country = $value['country'];
            $country->dial = $value['dial'];
            $result = $country->save();

            if ($result == false) {
              $this->error = 'Unable to insert the Live Help country data.  Please contact technical support.';
              break;
            }
          }
          unset($countries);

          if (empty($this->error)) {

            // Operator Password
            $algo = 'sha512';
            if (function_exists('hash') && in_array($algo, hash_algos())) {
              $password = hash($algo, $password);
            } else if (function_exists('mhash') && mhash_get_hash_name(MHASH_SHA512) != false) {
              $password = bin2hex(mhash(MHASH_SHA512, $password));
            } else {
              $password = sha1($password);
            }

            // Insert Operator Account
            if (!empty($username)) {
              $user = Operator::create();
              $user->username = $username;
              $user->password = $password;
              $user->firstname = $firstname;
              $user->lastname = $lastname;
              $user->email = $email;
              $user->department = 'Sales / Technical Support';
              $user->image = '';
              $user->datetime = date('Y-m-d H:i:s', time());
              $user->updated = date('Y-m-d H:i:s', time());
              $user->privilege = -1;
              $user->status = -1;
              $result = $user->save();

              if ($result == false) {
                $this->error = 'Unable to create Live Help operator account, username may already exist.';
              }
            }

            if (empty($this->error)) {
              // Save Database Configuration
              $this->writable = false;
              $configuration = '../include/database.php';
              if (empty($this->error)) {
                if (file_exists($configuration)) {
                  if (is_writable($configuration)) {

                    // Configuration File
                    if (file_exists($this->config)) {
                      $content = file_get_contents($this->config);
                    } else {
                      $content = $this->config;
                    }
                    if ($handle = fopen($configuration, 'w')) {
                      if (fwrite($handle, $content)) {
                        $this->writable = true;
                        fclose($handle);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } else {
      // Unexpected Error
      $this->error = 'MySQL Connection Error. ' . $this->error . '  Please contact technical support.';
    }

    return (empty($this->error) && isset($this->writable) && $this->writable) ? true : false;
  }

}

?>
