<?php
/*
Chatstack - https://www.chatstack.com
Copyright - All Rights Reserved - Stardevelop Pty Ltd

You may not distribute this program in any manner,
modified or otherwise, without the express, written
consent from Stardevelop Pty Ltd (https://www.chatstack.com)

You may make modifications, but only for your own
use and within the confines of the License Agreement.
All rights reserved.

Selling the code for this program without prior
written consent is expressly forbidden. Obtain
permission before redistributing this program over
the Internet or in any other medium.  In all cases
copyright and header must remain intact.
*/
namespace stardevelop\chatstack;

use Smarty;
use DateTime;

if ((isset($_PLUGINS['WHMCS']) || defined('WHMCS')) && !defined('CHATSTACK')) {
	return;
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
		header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
		header('Access-Control-Allow-Headers: X-Requested-With');
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 1728000');
		header('Content-Length: 0');
		header('Content-Type: text/plain');
		exit();
	} else {
		header('HTTP/1.1 403 Access Forbidden');
		header('Content-Type: text/plain');
		exit();
	}
}

$installed = false;
$database = require_once('./include/database.php');
if ($database) {
	// Smarty Template
	require_once('./include/smarty/Smarty.class.php');

	require_once('./include/spiders.php');
	require_once('./include/class.aes.php');
	require_once('./include/class.cookie.php');
	require_once('./include/class.session.php');
	$installed = require_once('./include/core.config.php');
	require_once('./include/class.models.php');
} else {
	$installed = false;
}

require_once('./include/core.functions.php');
$version = require_once('./include/version.php');

// Fix Suhosin Request Data / Visitor Tracking Issue
// suhosin.get.max_value_length=512
$_REQUEST = array();
$params = explode('&', html_entity_decode($_SERVER['QUERY_STRING']));
foreach ($params as $pair) {
	list($key, $value) = explode('=', $pair);
	$_REQUEST[urldecode($key)] = urldecode($value);
}

// HTTP Request Array
$_REQUEST = array_change_key_case($_REQUEST, CASE_UPPER);


if ($installed == false) {

	// Hooks
	require_once dirname(__FILE__) . '/include/class.hooks.php';

	// Settings Failure Hook
	$hooks->run('SettingsFailure');

	// Initialise Settings
	$_SETTINGS = array();

} else if (!empty($_REQUEST['DATA'])) {

	if (!isset($_SERVER['HTTP_REFERER'])){ $_SERVER['HTTP_REFERER'] = ''; }
	if (!isset($_REQUEST['DATA'])){ $_REQUEST['DATA'] = ''; }
	if (!isset($_REQUEST['DEPARTMENT'])){ $_REQUEST['DEPARTMENT'] = ''; }
	if (!isset($_REQUEST['SERVER'])){ $_REQUEST['SERVER'] = ''; }
	if (!isset($_REQUEST['PLUGIN'])){ $_REQUEST['PLUGIN'] = ''; }
	if (!isset($_REQUEST['CUSTOM'])){ $_REQUEST['CUSTOM'] = ''; }
	if (!isset($_REQUEST['NAME'])){ $_REQUEST['NAME'] = ''; }

	// Copyright Removal
	if ($version == false && !isset($_LOCALE['stardevelopcopyright'])) {
		$_LOCALE['stardevelopcopyright'] = 'International Copyright &copy; 2003 - ' . date('Y') . ' <a href="http://livehelp.stardevelop.com" target="_blank" class="normlink">Live Help Messenger</a> All Rights Reserved';
	}

	// HTTP Request Data
	$data = base64_decode($_REQUEST['DATA']);
	$data = prepare_json($data);
	$data = json_decode($data, true);

	// JSON DATA
	$data = array_change_key_case($data, CASE_UPPER);

	$_REQUEST['INITIATE'] = (isset($data['INITIATE']) && is_numeric($data['INITIATE'])) ? (int)$data['INITIATE'] : false;
	$_REQUEST['CLOUD'] = (isset($data['CLOUD'])) ? 2 : 0;
	$_REQUEST['TITLE'] = (isset($data['TITLE'])) ? $data['TITLE'] : '';
	$_REQUEST['URL'] = (isset($data['URL'])) ? $data['URL'] : '';
	$_REQUEST['REFERRER'] = (isset($data['REFERRER'])) ? $data['REFERRER'] : '';
	$_REQUEST['USERAGENT'] = (isset($data['USERAGENT'])) ? $data['USERAGENT'] : '';
	$_REQUEST['WIDTH'] = (isset($data['WIDTH'])) ? $data['WIDTH'] : '';
	$_REQUEST['HEIGHT'] = (isset($data['HEIGHT'])) ? $data['HEIGHT'] : '';
	$_REQUEST['TIME'] = (isset($data['TIME'])) ? $data['TIME'] : '';
	$_REQUEST['PLUGIN'] = (isset($data['PLUGIN'])) ? $data['PLUGIN'] : '';
	$_REQUEST['CUSTOM'] = (isset($data['CUSTOM'])) ? $data['CUSTOM'] : '';
	$_REQUEST['NAME'] = (isset($data['NAME'])) ? $data['NAME'] : '';
	$_REQUEST['DEPARTMENT'] = (isset($data['DEPARTMENT'])) ? $data['DEPARTMENT'] : '';
	$_REQUEST['SESSION'] = (isset($data['SESSION'])) ? $data['SESSION'] : '';
	$_REQUEST['WEBSOCKETS'] = (isset($data['WEBSOCKETS'])) ? $data['WEBSOCKETS'] : '';
	$_REQUEST['POPUP'] = (isset($data['POPUP'])) ? $data['POPUP'] : false;

	$cloud = $_REQUEST['CLOUD'];

	$department = trim(htmlspecialchars($_REQUEST['DEPARTMENT']));
	$callback = (isset($_REQUEST['CALLBACK'])) ? true : false;
	$websocket = (isset($_REQUEST['WEBSOCKETS']) && !empty($_REQUEST['WEBSOCKETS'])) ? $_REQUEST['WEBSOCKETS'] : false;

	if (!isset($_REQUEST['TITLE'])){ $_REQUEST['TITLE'] = ''; }
	if (!isset($_REQUEST['URL'])){ $_REQUEST['URL'] = ''; }
	if (!isset($_REQUEST['REFERRER'])){ $_REQUEST['REFERRER'] = ''; }
	if (!isset($_REQUEST['INITIATE'])){ $_REQUEST['INITIATE'] = false; }
	if (!isset($_REQUEST['POPUP'])){ $_REQUEST['POPUP'] = false; }

	$title = urldecode(substr($_REQUEST['TITLE'], 0, 150));
	$url = urldecode($_REQUEST['URL']);
	$referrer = urldecode($_REQUEST['REFERRER']);
	$initiate = $_REQUEST['INITIATE'];
	$popup = $_REQUEST['POPUP'];

	$totalpages = 0;
	$initiatejson = array('enabled' => false, 'delay' => (isset($_SETTINGS['INITIATECHATDELAY'])) ? (int)$_SETTINGS['INITIATECHATDELAY'] : -1);

	// Output Settings
	$output = (!empty($url) || $popup) ? true : false;

	// Initialise Session
	$session = new Session($_REQUEST['SESSION'], $_SETTINGS['AUTHKEY'], true);

	// Visitor
	$visitor = $session->request;

	// Chat
	$chat = $session->chat;

	// Location
	$location = false;

	// Visitor Session Hook
	$result = $hooks->run('VisitorSession', $session);
	if (is_array($result) && isset($result['visitor']) && isset($result['session'])) {
		$visitor = ($result['visitor'] !== false) ? $result['visitor'] : $visitor;
		$session->visitor = $result['session']->visitor;
	}


	// Add / Update Visitor
	$guid = false;
	if ($visitor !== false) {

		$initiateflag = $visitor->initiate;
		$status = $visitor->status;
		$date = date('Y-m-d H:i:s', time());

		if (!empty($url)) {

			// Current Page from URL
			$page = $url;
			for ($i = 0; $i < 3; $i++) {
				$pos = strpos($page, '/');
				if ($pos === false) {
					$page = '';
					break;
				}
				if ($i < 2) {
					$page = substr($page, $pos + 1);
				}
				elseif ($i >= 2) {
					$page = substr($page, $pos);
				}
			}

			$page = trim(addslashes($page));
			if (is_array($visitor->path)) {
				$path = $visitor->path;
				$previouspath = $path;
			} else {
				$path = addslashes($visitor->path);
				$previouspath = explode('; ', $path);
			}

			if ($page != trim(end($previouspath))) {

				$visitor->request = $date;
				if (is_array($visitor->path)) {
					$path[] = $page;
					$visitor->path = $path;
				} else {
					$visitor->path = $path . '; ' . $page;
				}
				$visitor->url = $page;
				$visitor->status = $cloud;

				$totalpages = count($previouspath);

				if ($_SETTINGS['TRANSCRIPTVISITORALERTS'] == true && $chat !== false) {

					if ($chat->id > 0 && $chat->session()->count() > 0) {
						$message = Message::create();
						$message->chat = $chat->id;
						$message->username = '';
						$message->datetime = $date;
						$message->message = sprintf('%s has just visited %s',  $chat->name, $_SERVER['HTTP_REFERER']);
						$message->align = 2;
						$message->status = -2;
						$message->save();

						// Visitor Alert Hook
						$hooks->run('VisitorAlert', $message);
					}
				}

			} else {

				$visitor->url = $url;
				$visitor->status = $cloud;

				$totalpages = count($previouspath);
			}
		}

		// Initiate Chat
		if ($initiateflag > 0 || $initiateflag == -1) { $initiatejson['enabled'] = true; }
		if (isset($_SETTINGS['INITIATECHATAUTO']) && $_SETTINGS['INITIATECHATAUTO'] > 0) {
			if (($initiateflag == 0 || $initiateflag == -1) && count(Operators::$online) > 0 && $totalpages >= $_SETTINGS['INITIATECHATAUTO']) {
				$initiatejson['enabled'] = true;
			}
		}

		// Custom Initiate Chat
		if ($initiateflag > -2) {
			$custominitiate = InitiateChat::where('request', (int)$session->request->id)->find_one();
			if ($custominitiate !== false) {
				$avatar = false;

				// Operator
				$operator = Operator::where_id_is($custominitiate->user)->find_one();
				if ($operator !== false) {
					$avatar = md5($operator->email);
					$image = $operator->image;
				}
				$initiatejson['enabled'] = true;
				$initiatejson['message'] = $custominitiate->message;
				$initiatejson['avatar'] = $avatar;
				$initiatejson['image'] = $image;
			}
		}

		$initiatejson['status'] = (int)$visitor->initiate;

		// IP Address
		$ipaddress = ip_address(false);
		if ($ipaddress !== false && $visitor->ipaddress !== $ipaddress) {
			$visitor->ipaddress = $ipaddress;
		}

		// User Agent
		$useragent = (isset($_SERVER['HTTP_USER_AGENT'])) ? substr($_SERVER['HTTP_USER_AGENT'], 0, 200) : '';

		if (isset($_REQUEST['USERAGENT']) && !empty($_REQUEST['USERAGENT'])) {
			$useragent = $_REQUEST['USERAGENT'];
		}
		$visitor->useragent = $useragent;

		// Update Initiate Status
		if (!empty($initiate)) {
			if ($initiate < 0) {
				// Initiate Opened
				$visitor->initiate = $initiate;
			}
		}

		// Update Current Page and Title
		if (!empty($url) && !empty($title)) {
			$visitor->url = $url;
			$visitor->title = $title;
		}

		$visitor->refresh = $date;

		// Save Visitor
		if ($session->db !== false && method_exists($visitor, 'save')) {
			$visitor->save();
		}

		// Visitor Updated
		if ($visitor !== false) {
			$visitor->websocket = $websocket;
			$visitor->chat = $session->chat;
			$hooks->run('VisitorUpdated', $visitor);
		}

	} else {

		if (!isset($_REQUEST['WIDTH'])){ $_REQUEST['WIDTH'] = ''; }
		if (!isset($_REQUEST['HEIGHT'])){ $_REQUEST['HEIGHT'] = ''; }

		$width = $_REQUEST['WIDTH'];
		$height = $_REQUEST['HEIGHT'];

		$ipaddress = ip_address();
		$useragent = (isset($_SERVER['HTTP_USER_AGENT'])) ? substr($_SERVER['HTTP_USER_AGENT'], 0, 200) : '';

		if (isset($_REQUEST['USERAGENT']) && !empty($_REQUEST['USERAGENT'])) {
			$useragent = $_REQUEST['USERAGENT'];
		}

		if (!empty($width) && !empty($height) && !empty($url)) {

			$page = $_REQUEST['URL'];
			for ($i = 0; $i < 3; $i++) {
				$pos = strpos($page, '/');
				if ($pos === false) {
					$page = '';
					break;
				}
				if ($i < 2) {
					$page = substr($page, $pos + 1);
				}
				elseif ($i >= 2) {
					$page = substr($page, $pos);
				}
			}
			if (empty($page)) { $page = '/'; }
			$page = urldecode(trim($page));

			if (empty($referrer)) { $referrer = 'Direct Visit / Bookmark'; }
			$date = date('Y-m-d H:i:s', time());

			if ($session->db !== false) {
				$visitor = Visitor::create();
			} else {
				$visitor = $session->visitor;
			}

			$visitor->ipaddress = $ipaddress;
			$visitor->useragent = $useragent;
			$visitor->resolution = sprintf('%s x %s', $width, $height);
			$visitor->city = '';
			$visitor->state = '';
			$visitor->country = '';
			$visitor->datetime = $date;
			$visitor->request = $date;
			$visitor->url = $url;
			$visitor->title = $title;

			if (empty($visitor->referrer)) {
				$visitor->referrer = $referrer;
			}

			$visitor->initiate = 0;
			$visitor->status = $cloud;

			if ($session->db !== false) {
				$visitor->refresh = $date;
				$visitor->path = $page;
			} else {
				$visitor->path = array($page);
			}

			$visitor->status = $cloud;

			// MaxMind Geo IP Location Plugin
			$geolocation = false;
			if (file_exists('./plugins/maxmind/GeoLiteCity.dat') && (float)$_SETTINGS['SERVERVERSION'] >= 3.90) {
				// Note that you must download the new format of the MaxMind GeoIP City (GEO-133).
				// The old format (GEO-132) will not work.
				require_once('./plugins/maxmind/geoipcity.php');

				// Shared Memory Support
				// geoip_load_shared_mem('./plugins/maxmind/GeoLiteCity.dat');
				// $gi = geoip_open('./plugins/maxmind/GeoLiteCity.dat', GEOIP_SHARED_MEMORY);

				$gi = geoip_open('./plugins/maxmind/GeoLiteCity.dat', GEOIP_STANDARD);
				$record = geoip_record_by_addr($gi, ip_address());
				if (!empty($record)) {
					if (!empty($record->country_name)) { $country = convert_utf8($record->country_name); } else { $country = ''; }
					if (isset($GEOIP_REGION_NAME[$record->country_code][$record->region])) { $state = convert_utf8($GEOIP_REGION_NAME[$record->country_code][$record->region]); } else { $state = ''; }
					if (!empty($record->city)) { $city = convert_utf8($record->city); } else { $city = ''; }

					$visitor->city = $city;
					$visitor->state = $state;
					$visitor->country = $country;

					$geolocation = array(
						'city' => $city,
						'state' => $state,
						'country' => $country
					);

					// Save Visitor
					if ($session->db !== false) {
						$visitor->save();
					}

					// Insert Geolocation
					if ((float)$_SETTINGS['SERVERVERSION'] >= 4.10) {
						$latitude = $record->latitude;
						$longitude = $record->longitude;

						// Location Save
						if ($session->db !== false) {
							// Save Geolocation
							$location = Geolocation::create();
							$location->request = $visitor->id;
							$location->city = $city;
							$location->state = $state;
							$location->country = $country;
							$location->latitude = $latitude;
							$location->longitude = $longitude;
							$location->save();
						} else {
							$visitor->city = $city;
							$visitor->state = $state;
							$visitor->country = $country;
							$visitor->latitude = $latitude;
							$visitor->longitude = $longitude;
						}

						$geolocation = array(
							'city' => $city,
							'state' => $state,
							'country' => $country,
							'latitude' => $latitude,
							'longitude' => $longitude
						);
					}

				} else {
					// Save Visitor
					if ($session->db !== false) {
						$visitor->save();
					}
				}
				geoip_close($gi);

			} else {
				// Save Visitor
				if ($session->db !== false) {
					$visitor->save();
				}
			}

			// GUID
			$guid = guidv4();

			if ($session->db === false) {
				$visitor->id = 'visitor:' . ACCOUNT . ':' . $guid;
			}

			$visitor->guid = $guid;
			$visitor->location = $geolocation;

			// Visitor Added Hook
			$visitor->websocket = $websocket;
			$visitor->chat = $session->chat;
			$hooks->run('VisitorAdded', $visitor);

		}
	}

	// Custom Integration
	$plugin = htmlspecialchars(urldecode($_REQUEST['PLUGIN']));
	$custom = htmlspecialchars(urldecode($_REQUEST['CUSTOM']));
	$name = htmlspecialchars(urldecode($_REQUEST['NAME']));

	// Custom Plugin / Integration Data
	if ($visitor !== false && ($visitor->id > 0 || is_string($visitor->id)) && !empty($plugin) && !empty($custom)) {
		if ($plugin !== 'Internal') {

			// Integration Hook
			$hooks->run('VisitorCustomDetailsInitialised', array('request' => $visitor->id, 'custom' => $custom, 'name' => $name, 'plugin' => $plugin));

		} else if ($plugin === 'Internal') {

			$result = $hooks->run('VisitorCustomDetailsInitialised', array('visitor' => $visitor, 'custom' => $custom, 'name' => $name, 'plugin' => $plugin));

			// Internal Integration
			if (!is_bool($result)) {
				$integration = Custom::where('request', $visitor->id)->find_one();
				if ($integration === false) {
					$integration = Custom::create();
					$integration->request = $visitor->id;
				}
				$integration->custom = $custom;
				$integration->name = $name;
				$integration->reference = $plugin;
				$integration->save();
			}
		}
	}

	// Hidden Departments
	$hiddendepts = false;
	if ((float)$_SETTINGS['SERVERVERSION'] >= 5.0) {
		$hiddendepts = Department::where('status', 1)->find_many();
	}

	$excludedepts = array();
	if ($hiddendepts !== false) {
		foreach ($hiddendepts as $key => $value) {
			$excludedepts[] = $value->name;
		}
	}

	// Operators
	$users = Operator::find_many();

	$type = false;
	foreach ($users as $key => $user) {

		$id = (int)$user->id;

		switch ($user->status()) {
			case 0: // Offline - Hidden
				$type = &Operators::$hidden;
				break;
			case 1: // Online
				$type = &Operators::$online;
				break;
			case 2: // Be Right Back
				$type = &Operators::$brb;
				break;
			case 3: // Away
				$type = &Operators::$away;
				break;
		}

		if (!empty($department) && $type !== false) {

			$depmnts = explode(';', $user->department);
			if (is_array($depmnts)) {
				foreach ($depmnts as $key => $depart) {
					if (!in_array($id, $type)) {
						$depart = trim($depart);
						if ($depart == $department && !in_array($depart, $excludedepts)) {
							$type[] = $id;
						}
					}
				}
			}
			else {
				if (!in_array($id, $type)) {
					$depmnt = trim($user->department);
					if ($depmnt == $department && !in_array($depmnt, $excludedepts)) {
						$type[] = $id;
					}
				}
			}

		} else {
			$type[] = $id;
		}

	}

	// Status Mode
	$status = 'Offline';
	if (count(Operators::$online) > 0) {
		$status = 'Online';
	} elseif (count(Operators::$brb) > 0 && count(Operators::$brb) >= count(Operators::$away)) {
		$status = 'BRB';
	} elseif (count(Operators::$away) > 0) {
		$status = 'Away';
	}

	// Away Disabled
	if ($status == 'Away' && isset($_SETTINGS['AWAYMODE']) && $_SETTINGS['AWAYMODE'] === false) {
		$status = 'Offline';
	}

	// BRB Disabled
	if ($status == 'BRB' && isset($_SETTINGS['BRBMODE']) && $_SETTINGS['BRBMODE'] === false) {
		$status = 'Offline';
	}

	// Auto Initiate Chat
	$initiate = false;
	if ($visitor !== false && $status === 'Online') {

		$initiate = (int)$visitor->initiate;
		if (is_string($visitor->path)) {
			$path = explode('; ', $visitor->path);
		} else {
			$path = $visitor->path;
		}
		$totalpages = count($path);

		$initiatejson['status'] = $initiate;

		if ($initiate > 0 || $initiate == -1 || (isset($_SETTINGS['INITIATECHATAUTO']) && $_SETTINGS['INITIATECHATAUTO'] > 0 && $initiate == 0 && count(Operators::$online) > 0 && $totalpages >= $_SETTINGS['INITIATECHATAUTO'])) {
			$initiate = true;
		} else {
			$initiate = false;
		}
	}
	$initiatejson['enabled'] = $initiate;

	/*
	if ($initiate && $embeddedoperator !== false) {
		$initiatejson['avatar'] = md5($embeddedoperator->email);
		$initiatejson['image'] = $embeddedoperator->image;
	}
	*/

	// Offline Email Redirection
	if (!empty($_SETTINGS['OFFLINEEMAILREDIRECT'])) {
		if (preg_match('/^[\-!#$%&\'*+\\\\.\/0-9=?A-Z\^_`a-z{|}~]+@[\-!#$%&\'*+\\\\\/0-9=?A-Z\^_`a-z{|}~]+\.[\-!#$%&\'*+\\\\.\/0-9=?A-Z\^_`a-z{|}~]+$/', $_SETTINGS['OFFLINEEMAILREDIRECT'])) {
			$_SETTINGS['OFFLINEEMAILREDIRECT'] = 'mailto:' . $_SETTINGS['OFFLINEEMAILREDIRECT'];
		}
	}

	// Operators
	$embeddedoperator = false;

	// Departments
	$departments = array();
	$operators = array();
	$unavailable = array();

	$excludedepts = array();
	if ($hiddendepts !== false) {
		foreach ($hiddendepts as $key => $value) {
			$excludedepts[] = $value->name;
		}
	}

	// Unavailable Operators
	Operators::$unavailable = array_merge(Operators::$hidden, Operators::$brb, Operators::$away);

	if (Operators::$online !== false && count(Operators::$online) > 0) {
		foreach (Operators::$online as $id) {
			$user = Operator::where_id_is($id)->find_one();

			$depmnts = explode(';', $user->department);
			if (is_array($depmnts)) {
				foreach ($depmnts as $key => $depart) {
					$depart = trim($depart);
					if (!in_array($depart, $departments) && !in_array($depart, $excludedepts)) {
						$departments[] = $depart;
					}
					if (empty($department) || (!empty($department) && $depart == $department && !in_array($depart, $excludedepts))) {
						$operators[] = $user;
					}
				}
			}
			else {
				$depmnt = trim($user->department);
				if (!in_array($depmnt, $departments) && !in_array($depmnt, $excludedepts)) {
					$departments[] = $depmnt;
				}
				if (empty($department) || (!empty($department) && $depmnt == $department && !in_array($depmnt, $excludedepts))) {
					$operators[] = $user;
				}
			}
		}

		if (count($operators) > 0) {
			$embeddedoperator = $operators[array_rand($operators)];
		}

		$total = count($departments);
		sort($departments);
	} else if (Operators::$unavailable !== false && count(Operators::$unavailable) > 0) {
		foreach (Operators::$unavailable as $id) {
			$user = Operator::where_id_is($id)->find_one();

			$depmnts = explode(';', $user->department);
			if (is_array($depmnts)) {
				foreach ($depmnts as $key => $depart) {
					$depart = trim($depart);
					if (!in_array($depart, $departments) && !in_array($depart, $excludedepts)) {
						$departments[] = $depart;
					}
					if (empty($department) || (!empty($department) && $depart == $department && !in_array($depart, $excludedepts))) {
						$unavailable[] = $user;
					}
				}
			}
			else {
				$depmnt = trim($user->department);
				if (!in_array($depmnt, $departments) && !in_array($depmnt, $excludedepts)) {
					$departments[] = $depmnt;
				}
				if (empty($department) || (!empty($department) && $depmnt == $department && !in_array($depmnt, $excludedepts))) {
					$unavailable[] = $user;
				}
			}
		}

		if (count($unavailable) > 0) {
			$embeddedoperator = $unavailable[array_rand($unavailable)];
		}
	}

	// Departments Loaded Hook
	if (is_array($departments) && count($departments) > 0) {
		$departments = $hooks->run('DepartmentsLoaded', $departments);
	}

	// Disable Departments
	if ($_SETTINGS['DEPARTMENTS'] == false) {
		$departments = false;
	}

	if ($embeddedoperator !== false) {
		$photo = (!empty($embeddedoperator->image)) ? 'data:image/png;base64,' . $embeddedoperator->image : false;
		$embeddedinitiate = array('id' => (int)$embeddedoperator->id, 'name' => $embeddedoperator->firstname . ' ' . $embeddedoperator->lastname, 'department' => $embeddedoperator->department, 'avatar' => md5($embeddedoperator->email), 'photo' => $photo);
	} else {
		$embeddedinitiate = array('id' => -1);
	}

	// Chat State
	$chatstate = array('idle' => true);

	// Auto Open Chat
	$name = '';
	$email = false;
	$depmnt = '';
	$blocked = 0;

	$channel = '';
	if ($chat !== false) {
		$chatstatus = (int)$chat->status;
		// Chat Status
		switch ($chatstatus) {
			case -3:
				$chatstate = array('blocked' => true);
				break;
			case -2:
				$chatstate = array('connected' => true);
				break;
			case -1:
				$chatstate = array('completed' => true);
				break;
			case 0:
				$chatstate = array('waiting' => true);
				break;
			case 1:
				$chatstate = array('connected' => true);
				break;
		}

		if (!empty($url) && !empty($chatstate['completed'])) {
			$chatstate = array('idle' => true);
		}

		if ($chatstatus == 1 || $chatstatus == 0 || $chatstatus == -2) {

			$name = $chat->name;
			$email = $chat->email;
			$depmnt = $chat->department;

			// Introduction Name / Department
			$_SETTINGS['INTRODUCTION'] = preg_replace("/({name})/", $name, $_SETTINGS['INTRODUCTION']);
			$_SETTINGS['INTRODUCTION'] = preg_replace("/({department})/", $depmnt, $_SETTINGS['INTRODUCTION']);

			// Override Embedded Initiate Chat Operator
			$chatsession = $chat->session()->order_by_desc('requested')->find_one();
			if ($chatsession !== false) {
				$account = $chatsession->operator()->find_one();
				if ($account !== false) {
					$photo = (!empty($account->image)) ? 'data:image/png;base64,' . $account->image : false;
					$embeddedinitiate = array('id' => (int)$account->id, 'name' => $account->firstname . ' ' . $account->lastname, 'department' => $account->department, 'avatar' => md5($account->email), 'photo' => $photo);

					// Introduction Operator
					$_SETTINGS['INTRODUCTION'] = preg_replace("/({firstname})/", $account->firstname, $_SETTINGS['INTRODUCTION']);
					$_SETTINGS['INTRODUCTION'] = preg_replace("/({lastname})/", $account->lastname, $_SETTINGS['INTRODUCTION']);
				}
			}

			// Remove Optional Brackets
			$_SETTINGS['INTRODUCTION'] = preg_replace("/(\[|\])/", '', $_SETTINGS['INTRODUCTION']);

			if (isset($_SETTINGS['CLOUDSOCKETSCHANNELSALT'])) {
				$channel = sha1((int)$chat->id . $_SETTINGS['CLOUDSOCKETSCHANNELSALT']);
			}

		} else if ($chat->status == -3) {
			$blocked = 1;
		}
	}

	// Remove Introduction Variables
	if (!empty($chatstatus['connected'])) {
		if (strpos($_SETTINGS['INTRODUCTION'], '{name}') !== false) {
			$_SETTINGS['INTRODUCTION'] = preg_replace("/\s*({name})\s*/", '', $_SETTINGS['INTRODUCTION']);
		}

		if (strpos($_SETTINGS['INTRODUCTION'], '{firstname}') !== false) {
			$_SETTINGS['INTRODUCTION'] = preg_replace("/\s*({firstname})\s*|(?=\[).+({firstname}).+?(\])/", '', $_SETTINGS['INTRODUCTION']);
		}

		if (strpos($_SETTINGS['INTRODUCTION'], '{lastname}') !== false) {
			$_SETTINGS['INTRODUCTION'] = preg_replace("/\s*({lastname})\s*|(?=\[).+({lastname}).+?(\])/", '', $_SETTINGS['INTRODUCTION']);
		}

		if (strpos($_SETTINGS['INTRODUCTION'], '{department}') !== false) {
			$_SETTINGS['INTRODUCTION'] = preg_replace("/\s*({department})\s*|(?=\[).+({department}).+?(\])/", '', $_SETTINGS['INTRODUCTION']);
		}
	}

	// Encrypt Session
	$data = array();
	if ($visitor !== false) {
		$data['visitor'] = (int)$visitor->id;

		// Visitor Session Hook
		$result = $hooks->run('VisitorSessionData', array('guid' => false, 'session' => $session));
		if ($result !== false && is_string($result)) {
			$data = array('visitor' => $result);
			$data['visitor'] = $result;
		} else if (is_array($result) && is_array($result['session']) && isset($result['session']['request'])) {
			$data['visitor'] = (int)$result['session']['request'];
		}
	}

	if ($chat !== false) {
		$data['chat'] = (int)$chat->id;
		$data['hash'] = $chat->hash;
		$data['datetime'] = $chat->datetime;
		$data['name'] = $chat->name;
		$data['email'] = $chat->email;
		$data['department'] = $chat->department;

		if ($_SETTINGS['DATABASEVERSION'] >= 17 && !empty($chat->hash)) {
			$data['hash'] = $chat->hash;
		}

		// Chat Blocked
		if ($chat->status == -3) {
			$blocked = 1;
		}
	} else {
		$data['chat'] = false;
	}

	$encrypted = $session->encrypt($data);

}

header('Content-type: text/html; charset=utf-8');

if (defined('LANGUAGE')) {
	include('./locale/' . LANGUAGE . '/guest.php');
} else {
	include('./locale/en/guest.php');
}

if (LANGUAGE !== $_SETTINGS['LANGUAGE'] && !empty($_LOCALE['introduction'])) {
	$_SETTINGS['INTRODUCTION'] = $_LOCALE['introduction'];
	$_SETTINGS['INTRODUCTION'] = preg_replace("/(\r\n|\r|\n)/", '<br />', $_SETTINGS['INTRODUCTION']);
}

// Templates
$templates = array();
if (isset($_SETTINGS['TEMPLATES']) && is_array($_SETTINGS['TEMPLATES'])) {
	foreach ($_SETTINGS['TEMPLATES'] as $key => $template) {
		$templates[] = $template['value'];
	}
}

if (empty($templates)) {
	$templates[] = 'default';
}

// Visitor Language Hook
$hooks->run('VisitorLanguage');

// Language
$language = array();
$language['welcome'] = $_LOCALE['welcome'];
$language['enterguestdetails'] = $_LOCALE['enterguestdetails'];
$language['says'] = $_LOCALE['says'];
$language['pushedurl'] = $_LOCALE['pushedurl'];
$language['opennewwindow'] = $_LOCALE['opennewwindow'];
$language['sentfile'] = $_LOCALE['sentfile'];
$language['startdownloading'] = $_LOCALE['startdownloading'];
$language['rightclicksave'] = $_LOCALE['rightclicksave'];
$language['disconnecttitle'] = $_LOCALE['disconnecttitle'];
$language['disconnectdescription'] = $_LOCALE['disconnectdescription'];
$language['thankyoupatience'] = $_LOCALE['thankyoupatience'];
$language['emailchat'] = $_LOCALE['emailchat'];
$language['togglesound'] = $_LOCALE['togglesound'];
$language['feedback'] = $_LOCALE['feedback'];
$language['disconnect'] = $_LOCALE['disconnect'];
$language['collapse'] = $_LOCALE['collapse'];
$language['expand'] = $_LOCALE['expand'];
$language['invalidemail'] = $_LOCALE['invalidemail'];
$language['name'] = $_LOCALE['name'];
$language['email'] = $_LOCALE['email'];
$language['department'] = $_LOCALE['department'];
$language['question'] = $_LOCALE['question'];
$language['send'] = $_LOCALE['send'];
$language['enteryourmessage'] = $_LOCALE['enteryourmessage'];
$language['switchpopupwindow'] = $_LOCALE['switchpopupwindow'];
$language['rateyourexperience'] = $_LOCALE['rateyourexperience'];
$language['copyright'] = $_LOCALE['stardevelopcopyright'];
$language['thankyoumessagesent'] = $_LOCALE['thankyoumessagesent'];
$language['cancel'] = $_LOCALE['cancel'];
$language['pleasewait'] = $_LOCALE['pleasewait'];
$language['telephonecallshortly'] = $_LOCALE['telephonecallshortly'];
$language['telephonethankyoupatience'] = $_LOCALE['telephonethankyoupatience'];
$language['connect'] = $_LOCALE['connect'];
$language['connecting'] = $_LOCALE['connecting'];
$language['closechat'] = $_LOCALE['closechat'];
$language['chatsessionblocked'] = $_LOCALE['chatsessionblocked'];
$language['accessdenied'] = $_LOCALE['accessdenied'];
$language['blockedchatsession'] = $_LOCALE['blockedchatsession'];
$language['istyping'] = $_LOCALE['istyping'];
$language['online'] = $_LOCALE['online'];
$language['offline'] = $_LOCALE['offline'];
$language['brb'] = $_LOCALE['brb'];
$language['away'] = $_LOCALE['away'];
$language['offlineerrortitle'] = $_LOCALE['offlineerrortitle'];
$language['closedusermessage'] = $_LOCALE['closedusermessage'];
$language['contactus'] = $_LOCALE['contactus'];
$language['restartchat'] = $_LOCALE['restartchat'];
$language['password'] = $_LOCALE['password'];
$language['retypepassword'] = $_LOCALE['retypepassword'];
$language['chatwith'] = $_LOCALE['chatwith'];
$language['feedbackintroduction'] = $_LOCALE['feedbackintroduction'];;
$language['enteryourfeedbackemail'] = $_LOCALE['enteryourfeedbackemail'];
$language['enteryourfeedback'] = $_LOCALE['enteryourfeedback'];
$language['close'] = $_LOCALE['close'];
$language['needsupport'] = $_LOCALE['needsupport'];
$language['clickhere'] = $_LOCALE['clickhere'];
$language['chattingwith'] = $_LOCALE['chattingwith'];
$language['offlinemessagetitle'] = $_LOCALE['offlinemessagetitle'];
$language['offlinemessagedescription'] = $_LOCALE['offlinemessagedescription'];
$language['enteryourofflinename'] = $_LOCALE['enteryourofflinename'];
$language['enteryourofflineemail'] = $_LOCALE['enteryourofflineemail'];
$language['enteryourofflinemessage'] = $_LOCALE['enteryourofflinemessage'];
$language['retry'] = $_LOCALE['retry'];
$language['thankyou'] = $_LOCALE['thankyou'];


if (isset($_LOCALE['departments'])) {
	$language['departments'] = $_LOCALE['departments'];
} else {
	$language['departments'] = array();
}

// Smarty Templates
$smarty = false;
if ($output) {
	$smarty = new Smarty;

	$smarty->debugging = false;
	$smarty->caching = false;
	$smarty->template_dir = './templates';
	$smarty->compile_dir = './templates_c';
	$smarty->cache_dir = './templates/cache';
	$smarty->config_dir = './includes/smarty';
}

// Default Template
if (empty($_SETTINGS['TEMPLATE']) || ($smarty && !file_exists($smarty->template_dir . '/' . $_SETTINGS['TEMPLATE']) && file_exists($smarty->template_dir . '/default/chat.tpl'))) {
	$_SETTINGS['TEMPLATE'] = 'default';
}

if (isset($_REQUEST['TEMPLATE'])) {
	$chattemplatepath = $_REQUEST['TEMPLATE'] . '/chat.tpl';
	if ($smarty && !file_exists($smarty->template_dir . '/' . $chattemplatepath)) {
		$chattemplatepath = $_SETTINGS['TEMPLATE'] . '/chat.tpl';
	}
	$buttontemplatepath = $_REQUEST['TEMPLATE'] . '/button.tpl';
	if ($smarty && !file_exists($smarty->template_dir . '/' . $buttontemplatepath)) {
		$buttontemplatepath = $_SETTINGS['TEMPLATE'] . '/button.tpl';
	}
} else {
	$chattemplatepath = $_SETTINGS['TEMPLATE'] . '/chat.tpl';
	$buttontemplatepath = $_SETTINGS['TEMPLATE'] . '/button.tpl';
}


// AJAX Cross-site Headers
if (isset($_SERVER['HTTP_ORIGIN'])) {
	header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
	header('Access-Control-Allow-Credentials: true');
}

// HTTP/1.1
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);

// HTTP/1.0
header('Pragma: no-cache');


$json = array();
if (!empty($_SETTINGS)) {

	// Default Settings
	$json['session'] = $encrypted;

	// Channel Settings
	$settings = array();
	$settings['channel'] = array();

	if ($_SETTINGS['SECURITYCODE']) {
		// Security Code
		$chars = array('1','2','3','4','5','6','7','8','9','0');
		$ascii = array();

		$code = '';
		for ($i = 0; $i < 4; $i++) {
			$char = $chars[rand(0, count($chars) - 1)];
			$ascii[$i] = ord($char);
			$code .= $char;
		}

		$verify = sha1(strtoupper($code));
		$aes = new AES256($_SETTINGS['AUTHKEY']);
		$captcha = $aes->iv . $verify . $aes->encrypt($code);
		$settings['captcha'] = array();
		$settings['captcha']['code'] = $captcha;

		// Font
		$path = dirname(__FILE__);
		if (substr($path, 0, 2) == '\\\\') { $path = '//' . substr($path, 2); }
		if (substr($path, -1) == '/') {
			$font = $path . 'styles/fonts/Lato-Regular-Digits.svg';
		} else {
			$font = $path . '/styles/fonts/Lato-Regular-Digits.svg';
		}

		// SVG
		require_once('./include/lib/svg.php');
		$svg = new EasySVG();
		$svg->setFontSVG($font);
		$svg->setFontSize(30);

		$securityarray = str_split($code, 1);
		$attributes = array('fill' => '#ffffff');
		$textwidth = 5;
		$angles = array('-12', '-11', '-10', '-9', '-8', '-5', '5', '6', '7', '8', '9', '10', '11', '12');

		foreach ($securityarray as $key => $value) {
			$def = $svg->textDef($value);
			$angle = $angles[rand(0, 13)];
			$def = $svg->defRotate($def, $angle);
			$def = $svg->defTranslate($def, $textwidth, 0);
			$svg->addPath($def, $attributes);
			$textwidth = $textwidth + 17;
		}

		$svg->addAttribute('width', '80px');
		$svg->addAttribute('height', '30px');

		$settings['captcha']['image'] = $svg->asXML();
	} else {
		$settings['captcha'] = array();
		$settings['captcha']['code'] = false;
	}

	if ($output) {
		// Settings
		$settings['name'] = $_SETTINGS['NAME'];
		$settings['channel']['chat'] = (!empty($channel)) ? $channel : false;
		$settings['initiate'] = $initiatejson;
		$settings['popupSize'] = array('width' => (int)$_SETTINGS['CHATWINDOWWIDTH'], 'height' => (int)$_SETTINGS['CHATWINDOWHEIGHT']);
		$settings['status'] = $status;
		$settings['offlineRedirect'] = $_SETTINGS['OFFLINEEMAILREDIRECT'];
		$settings['offlineEmail'] = (int)$_SETTINGS['OFFLINEEMAIL'];
		$settings['chatstate'] = $chatstate;
		$settings['smilies'] = (int)$_SETTINGS['SMILIES'];
		$settings['department'] = $depmnt;
		$settings['departments'] = $departments;
		$settings['locale'] = LANGUAGE;
		$settings['language'] = $language;
		$settings['user'] = $name;
		$settings['email'] = $email;
		$settings['monitoring'] = array('enabled' => (int)$_SETTINGS['VISITORTRACKING'], 'refresh' => (int)$_SETTINGS['VISITORREFRESH']);
		$settings['requireGuestDetails'] = (int)$_SETTINGS['REQUIREGUESTDETAILS'];
		$settings['loginDetails'] = (int)$_SETTINGS['LOGINDETAILS'];
		$settings['loginEmail'] = (int)$_SETTINGS['LOGINEMAIL'];
		$settings['loginQuestion'] = (int)$_SETTINGS['LOGINQUESTION'];
		$settings['embeddedinitiate'] = $embeddedinitiate;
		$settings['templates'] = $templates;
		$settings['template'] = $_SETTINGS['TEMPLATE'];
		$settings['blocked'] = $blocked;
		$settings['rtl'] = (isset($_SETTINGS['DIRECTION']) && $_SETTINGS['DIRECTION'] == 'rtl') ? true : false;
		$settings['plugins'] = (isset($_SETTINGS['PLUGINS'])) ? $_SETTINGS['PLUGINS'] : false;
		$settings['images'] = array('online' => $_SETTINGS['ONLINELOGO'], 'offline' => $_SETTINGS['OFFLINELOGO'], 'brb' => $_SETTINGS['BERIGHTBACKLOGO'], 'away' => $_SETTINGS['AWAYLOGO'], 'initiatechat' => $_SETTINGS['INITIATECHATLOGO'], 'feedback' => (isset($_SETTINGS['FEEDBACKLOGO'])) ? $_SETTINGS['FEEDBACKLOGO'] : '', 'logo' => (isset($_SETTINGS['LOGO'])) ? $_SETTINGS['LOGO'] : '', 'button' => (isset($_SETTINGS['CHATBUTTONIMAGE'])) ? $_SETTINGS['CHATBUTTONIMAGE'] : 'default');
		$settings['introduction'] = $_SETTINGS['INTRODUCTION'];
		$settings['styles'] = array('app' => (file_exists('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/styles.min.css')) ? file_get_contents('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/styles.min.css') : false, 'parent' => (file_exists('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/parent.min.css')) ? file_get_contents('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/parent.min.css') : false);
		$settings['layout'] = (isset($_SETTINGS['LAYOUT'])) ? $_SETTINGS['LAYOUT'] : 'tab';
		$settings['supportaddress'] = (isset($_SETTINGS['SUPPORTADDRESS'])) ? $_SETTINGS['SUPPORTADDRESS'] : '';
		$settings['fonts'] = (file_exists('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/fonts.min.css')) ? file_get_contents('./templates/' . $_SETTINGS['TEMPLATE'] . '/styles/fonts.min.css') : false;
		$settings['campaign'] = array('image' => (isset($_SETTINGS['CAMPAIGNIMAGE'])) ? $_SETTINGS['CAMPAIGNIMAGE'] : '', 'link' => (isset($_SETTINGS['CAMPAIGNLINK'])) ? $_SETTINGS['CAMPAIGNLINK'] : '');
		$settings['theme'] = $_SETTINGS['THEME'];
		$settings['alert'] = (isset($_SETTINGS['ALERT'])) ? $_SETTINGS['ALERT'] : false;

		$json['update'] = true;
	} else {

		// Settings
		$settings['channel']['chat'] = $channel;
		$settings['chatstate'] = $chatstate;
		$settings['initiate'] = $initiatejson;

		// Updated JSON
		$json['status'] = $status;
		$json['departments'] = $departments;
		$json['update'] = false;
	}

	// Visitor Hash
	if ($visitor !== false && isset($_SETTINGS['CLOUDSOCKETSVISITORSALT'])) {
		// Visitor Hash
		$settings['channel']['visitor'] = sha1((int)$visitor->id . $_SETTINGS['CLOUDSOCKETSVISITORSALT']);
	}

	// Chat Session
	$hooks->run('ChatSessionData', array('chat' => $chat, 'visitor' => $visitor));

	// Visitor Monitoring Server Override
	if (defined('VISITORSERVER')) {
		$json['visitor']['server'] = VISITORSERVER;
	}

}

// HTML Output
if ($output) {
	$smarty->assign('LOCALE', $_LOCALE, true);

	$dir = (isset($_SETTINGS['DIRECTION']) && $_SETTINGS['DIRECTION'] == 'rtl') ? 'dir="rtl"' : '';
	$rtl = (isset($_SETTINGS['DIRECTION']) && $_SETTINGS['DIRECTION'] == 'rtl') ? 'style="text-align:right"' : '';
	$style = (strlen($_LOCALE['stardevelopcopyright']) > 0) ? 'block' : 'none';
	$smarty->assign('dir', $dir, true);
	$smarty->assign('rtl', $rtl, true);
	$smarty->assign('style', $style, true);

	$html = $smarty->fetch($chattemplatepath);
	$json['html'] = array();
	$json['html']['app'] = $html;

	$button = $smarty->fetch($buttontemplatepath);
	$json['html']['button'] = $button;
}

$json['settings'] = $settings;

$result = $hooks->run('VisitorSettings', array('settings' => $json['settings']));
if (is_array($result) && isset($result['settings']) && $output) {
	$json['settings'] = $result['settings'];
}

if ($installed == false) {
	$json['error'] = true;
} else {
	$json['error'] = false;
}

$json = json_encode($json);
if (!isset($_GET['callback'])) {
	header('Content-Type: application/json; charset=utf-8');
	exit($json);
} else {
	if (is_valid_callback($_GET['callback'])) {
		header('Content-Type: application/javascript; charset=utf-8');
		exit($_GET['callback'] . '(' . $json . ')');
	} else {
		header('HTTP/1.1 400 Bad Request');
		exit();
	}
}

?>
