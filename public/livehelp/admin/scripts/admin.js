/*global Symbol, Chatstack, NProgress*/

// Third Party Libraries
var $ = require('jquery');
var	_ = require('lodash');

var buzz = require('buzz');
var director = require('director');
var moment = require('moment');

// UUID.js
require('uuid');
import uuid from 'uuid';

// Storage
import Storage from 'storage';

//require('./jquery.zeroclipboard.js');

require('jquery.ui.widget');
require('jquery.ajaxq');
require('jquery.calendar');
require('jquery.caret');
require('jquery.event.drag');
require('jquery.fileupload');
require('jquery.hotkeys');
require('jquery.iframe-transport');
require('jquery.json');
require('jquery.lavalamp');
require('jquery.metro');
require('jquery.minicolors');
require('jquery.pulse');
require('jquery.scrollTo');
require('jquery.storage');
require('jquery.bubbletip');
require('bootstrap');
require('intro');
require('intercom');
require('qrcode');
require('slick.core');
require('slick.dataview');
require('slick.grid');
require('slick.groupitemmetadataprovider');
require('slick.rowselectionmodel');

// Enterprise
if (__ENTERPRISE__) { // eslint-disable-line no-undef
  require('enterprise/admin');
}

// Plugins
require('twitter-widget');
require('fullcontact');
require('totp');

// Components
require('statistics.js');
require('history.js');
require('responses.js');
require('accounts.js');
require('settings.js');

var api = require('api');
var core = require('core');
var chats = require('chats');
var visitors = require('visitors');

// JSON Web Service Access Details
var shortcuts = true;
var autoSignIn = true;
var saveExpanded = true;

// jQuery object for-in iterator
$.prototype[Symbol.iterator] = Array.prototype[Symbol.iterator];

// Initialise Storage
var storage = new Storage();

// Electron App
if (__ELECTRON__) { // eslint-disable-line no-undef
  require('../electron.js');
}

$.preloadImages = function () {
  for (var i = 0; i < arguments.length; i++) {
    $('<img>').attr('src', arguments[i]);
  }
};

var chatResponsesOpen = false;

function updateTypingStatus(currentlyTyping) {
  var chats = $('.chat-stack .chat.focussed');
  var id = chats.data('id');
  var operator = chats.data('operator');
  var typing;

  var chat = _.find(core.activechats, function (chat) {
    return chat.id === id && chat.type == operator;
  });

  $(document).trigger('LiveHelp.UpdateTyping', { id: id, operator: operator, typing: currentlyTyping });

  if (chat !== undefined) {
    if (currentlyTyping) {
      switch (chat.typing) {
        case 0: // None
          typing = 2;
          break;
        case 1: // Guest Only
          typing = 3;
          break;
        case 2: // Operator Only
          typing = 2;
          break;
        case 3: // Both
          typing = 3;
          break;
      }
    } else {
      switch (chat.typing) {
        case 0: // None
          typing = 0;
          break;
        case 1: // Guest Only
          typing = 1;
          break;
        case 2: // Operator Only
          typing = 0;
          break;
        case 3: // Both
          typing = 1;
          break;
      }
    }

    chat.typing = typing;
    return false;
  }

}

$(document).on('LiveHelp.UpdateTags', function (event, response) {
  for (var tag of response.tags) {
    tag = tag.toLowerCase();
    if ($.inArray(tag, core.tags) === -1) {
      core.tags.push(tag);
    }
  }
});

var initialiseRoute = function () {
  var route = window.location.hash.slice(2);
  if (route.length > 0) {
    storage.set('route', route);
  } else {
    storage.set('route', '');
  }
};

// Document Ready
$(document).ready(function () {

  // Initialise
  core.initialiseDocumentReady();

  // Chat Sections
  chats.sections = {
    chatting: { element: $('.chatting.list'), height: 44 },
    other: { element: $('.other-chatting.list'), height: 44 },
    pending: { element: $('.pending.list'), height: 44 },
    transferred: { element: $('.transferred.list'), height: 44 },
    operators: { element: $('.operators.list'), height: 34 },
    none: { height: 0, enabled: false }
  };

  // Progress
  NProgress.configure({ showSpinner: false });

  $(document).ajaxStart(function() {
    NProgress.set(0.3);
    NProgress.start();
  });

  $(document).ajaxStop(function() {
    setTimeout(function () {
      NProgress.done();
    }, 2000);
  });

  var routes = {
    '/home': function () {
      core.switchMenu('home');
    },

    '/accounts': function () {
      core.switchMenu('accounts');
    },

    '/settings': function () {
      $(document).trigger('LiveHelp.OpenSettings');
    },

    '/settings/:menu': function (section) {
      $(document).trigger('LiveHelp.OpenSettings', section);
    },

    '/tour': function () {
      if (core.tour !== false) {
        core.tour.setCurrentStep(0);
        core.tour.start(true);
      }
    },

    '/chat/:chat': function (id) {
      var chats = require('chats');
      chats.openChat(id);
    }
  };
  core.router = director.Router(routes);

  // Initialise Route
  initialiseRoute();

  // Web Sockets
  $(document).bind('LiveHelp.WebSocketStateChanged', function (event, data) {
    core.websockets = data;
    if (core.websockets !== false) {
      window.clearTimeout(core.usersTimer);
      $(document).trigger('LiveHelp.ClearVisitorsTimer');
    }
  });

  // Reset Chatting List Height Data
  var lists = $('.chat-list .list');
  $.each(lists, function(key, value) {
    var section = $(value);
    if (section.find('.visitor').length === 0) {
      if (chats.sections.none.enabled) {
        section.data('height', chats.sections.none.height);
      } else {
        section.find('no-visitor').hide();
        section.data('height', 0);
      }
    } else {
      var height = chats.sectionHeight(section);
      section.data('height', height);
    }
  });

  $('.chat-list').on({
    mouseenter: function () {
      $(this).find('.image, .close, .dropdown-toggle.options').addClass('hover');
    },
    mouseleave: function () {
      $(this).find('.image, .close, .dropdown-toggle.options').removeClass('hover');
    }
  }, '.visitor');

  $('.chat-list').on({
    mouseenter: function () {
      $('.chat-list').find('.visitor.selected, .channel.selected').addClass('previous');
      $('.chat-list').find('.visitor, .channel').removeClass('selected');
    },
    mouseleave: function () {
      $('.chat-list .previous').removeClass('previous').addClass('selected');
    }
  }, '.visitor, .channel');

  $('.VisitorsTotal, #visitortotal').click(function () {
    core.switchMenu('home');
  });

  // Sidebar Menu
  $('.sidebar-icons .menu').click(function (e) {
    var type = $(this).data('type');

    // Menu
    core.switchMenu(type);

    if (e.preventDefault) {
      e.preventDefault();
    }

    if (e.stopPropagation) {
      e.stopPropagation();
    }
  });

  // Notification Events
  $('.notification .close').click(function () {
    chats.closeNotification();
    return false;
  });

  if (core.dropdown) {
    $('.dropdown-toggle').dropdown();
  }

  var chatstack = $('.chat-stack');
  chatstack.find('.dialog .btn.unblock').click(function () {
    var chats = chatstack.find('.chat');
    var dialog = chatstack.find('.dialog');
    var chat = false;

    $.each(chats, function(key, value) {
      if ($(value).position().left === 0) {
        chat = $(value);
        return false;
      }
    });

    if (chat !== undefined && chat.length > 0) {
      chat = chat.data('id');
      core.unblockChat(chat, dialog);
    }
  });

  // Chats Stack
  chatstack.on('click', '.chat', function () {
    var obj = $(this);
    var id = obj.data('id');
    var pos = { top: parseInt(obj.css('top'), 10), left: parseInt(obj.css('left'), 10), bottom: parseInt(obj.css('bottom'), 10) };
    var focussed = obj.is('.focussed');
    var scroll = obj.find('.scroll');
    var total = obj.find('.message-alert').data('total');

    if (!focussed) {

      // Save Scroll Position
      $('.chat-stack .chat').each(function (index, value) {
        var chat = $(value);
        var left = parseInt(chat.position().left, 10);

        if (left === 0) {
          $(chat).data('scroll', chat.find('.scroll').scrollTop());
        }
      });

      // Animate Front Position
      obj.animate({ 'z-index':80, 'left':0, 'bottom':0, 'top':0, 'backgroundColor': '#fffff' }, 150, 'easeInOutBack', function () {
        obj.find('.inputs, input').fadeIn();
        obj.find('.inputs input').focus();

        var scroll = obj.data('scroll');

        if (total > 0) {
          scroll = obj.find('.scroll .end');
        }

        // Check Blocked Chat
        core.checkBlocked(id);

        if (scroll !== undefined) {
          obj.find('.scroll').scrollTo(scroll);
        }
      });

      // Close Smilies
      $('.chat-stack .smilies.button').close();

      // Hide Name / Alert
      obj.find('.name').hide();
      chats.resetMessageAlert(obj);

      // Stack Chats
      $('.chat-stack .chat').each(function (index, value) {
        var chat = $(value);
        var left = parseInt(chat.css('left'), 10);
        var zindex = parseInt(chat.css('z-index'), 10);
        var top = 0;
        var bottom = 2;
        var color = '#fafafa';

        if (left < pos.left) {
          if (left > 0) {
            top = 0;
            bottom = 4;
            color = '#f6f6f6';
          }

          chat.animate({
            'z-index': zindex - 10,
            'top': top,
            'left': left + 35,
            'bottom': bottom,
            'backgroundColor': color
          }, 150, 'easeInOutBack');

          chat.find('.inputs, input').fadeOut();
          chat.find('.name').fadeIn();
        }
      });

      scroll.scrollTo(scroll.find('.end'));

    }
  });

  chatstack.on('click', '.chat .scrollalert a, .chat .scrollalert .scrollmessage, .chat .scrollalert .arrow', function () {
    var chat = $('.chat-stack .chat.focussed');
    var id = chat.data('id');
    var visitor = $('.chat-list .visitor[data-id="' + id + '"]');
    var operator = $('.chat.focussed').data('operator');
    var alert = visitor.find('.message-alert');
    var total = alert.data('total');

    if (total > 0) {
      var last = chat.find('.messages .flex.left:nth-last-child(' + total + ')');
      var typing = chat.find('.messages .flex.typing:visible');

      if (typing.length > 0) {
        last = typing;
      }

      chat.find('.scroll').scrollTo(last);
    } else {
      chats.scrollToBottom(id, operator, true);
    }

    chats.resetMessageAlert(visitor);
    $('.chat-stack .chat[data-id="' + id + '"] .scrollalert').hide();
  });

  // Keyboard Shortcuts
  var keyselector = 'body, input, textarea, .chat-stack #message';
  $(keyselector).on('keydown', 'esc', function () {
    if (shortcuts) {
      processEscKeyDown();
    }
  });

  $(keyselector).on('keydown', 'ctrl+shift+s', function () {
    if (shortcuts) {
      $(document).trigger('LiveHelp.OpenSettings');
    }
  });

  $(keyselector).on('keydown', 'ctrl+shift+a', function () {
    if (shortcuts) {
      $(document).trigger('LiveHelp.OpenAccounts');
    }
  });

  $(keyselector).on('keydown', 'ctrl+shift+r', function () {
    if (shortcuts) {
      $(document).trigger('LiveHelp.OpenResponses');
    }
  });

  $(document).on('keydown', keyselector, 'ctrl+space', function () {
    if (shortcuts) {
      var popover = $('.popover.responses');
      var button = $('.chat-stack .search.button');

      if (popover.length) {
        button.removeClass('active').popover('hide');
      } else {
        button.addClass('active').popover('show');
        $(document).trigger('LiveHelp.UpdateResponsesPopover');
        $('.popover.responses #search').focus();
      }
    }
  });

  $('.chat-stack .smilies.button').on({
    mouseenter: function () {
      $(this).removeClass('Smilies').addClass('SmiliesHover');
    },
    mouseleave: function () {
      $(this).removeClass('SmiliesHover').addClass('Smilies');
    },
    click: function () {
      $(this).bubbletip($('#SmiliesTooltip'), { calculateOnShow: true }).open();
    }
  });

  var responsesSearch = $('.chat-stack .search.button');
  $('.chat-stack').on({
    focus: function () {
      $('.chat-stack .smilies.button').close();
      responsesSearch.addClass('focus');
    },
    blur: function () {
      responsesSearch.removeClass('focus');
    }
  }, '#message');

  $('#SmiliesTooltip span').click(function () {
    var smilie = $(this).attr('class').replace(/sprite | Small/g, '');
    var text = '';
    var textarea = $('.chat-stack #message');
    var val = textarea.val();

    switch (smilie) {
      case 'Laugh':
        text = ':D';
        break;
      case 'Smile':
        text = ':)';
        break;
      case 'Sad':
        text = ':(';
        break;
      case 'Money':
        text = '$)';
        break;
      case 'Impish':
        text = ':P';
        break;
      case 'Sweat':
        text = ':\\';
        break;
      case 'Cool':
        text = '8)';
        break;
      case 'Frown':
        text = '>:L';
        break;
      case 'Wink':
        text = ';)';
        break;
      case 'Surprise':
        text = ':O';
        break;
      case 'Woo':
        text = '8-)';
        break;
      case 'Tired':
        text = 'X-(';
        break;
      case 'Shock':
        text = '8-O';
        break;
      case 'Hysterical':
        text = 'xD';
        break;
      case 'Kissed':
        text = ':-*';
        break;
      case 'Dizzy':
        text = ':S';
        break;
      case 'Celebrate':
        text = '+O)';
        break;
      case 'Angry':
        text = '>:O';
        break;
      case 'Adore':
        text = '<3';
        break;
      case 'Sleep':
        text = 'zzZ';
        break;
      case 'Stop':
        text = ':X';
        break;
    }
    textarea.val(val + text).keyup();
  });

  // Close Chat
  var confirmclose = $('.chat-stack .confirm-close.dialog');
  confirmclose.find('.cancel-button').on('click', function () {
    confirmclose.show().animate({ bottom: -90 }, 250, 'easeInOutBack', function () {
      confirmclose.hide();
    });
  });

  confirmclose.find('.accept-button').on('click', function () {
    chats.closeChat();
  });

  // Operator Status Dropdown
  $('.operator .name, .operator .photo').on('click', function (e) {
    $('.operator .dropdown-toggle').click();
    e.stopPropagation();
  });

  // Send Button
  $('.chat-stack .send').on({
    click: function () {
      // Send Message
      sendMessage();

      // Close Pre-typed Responses
      var menu = $('.responses-menu');
      if (menu.css('display') != 'none') {
        menu.height(0).hide();
      }

      return false;
    },
    mouseenter: function () {
      $(this).css({ 'opacity': 1.0 }).removeClass('sprite SendButton').addClass('sprite SendButtonHover');
    },
    mouseleave: function () {
      $(this).css({ 'opacity': 0.4 }).removeClass('sprite SendButtonHover').addClass('sprite SendButton');
    }
  });

  var textarea = $('.chat-stack #message');
  textarea.bind('keydown', 'return', function () {
    sendMessage();
    return false;
  }).bind('keyup', 'return', function () {
    updateTypingStatus(false);
  }).bind('focusout', function () {
    updateTypingStatus(false);
  }).bind('keydown', 'ctrl+return shift+return', function () {
    var input = $(this);
    var value = input.val();
    var start = input.range().start;
    var end = input.range().end;

    input.val(value.substr(0, start) + '\n' + value.substr(end)).caret(start + 1);
    return false;
  }).bind('keydown', function () {
    updateTypingStatus(true);
  });

  $('.chat-list-heading').on('click', function () {
    var menu = $(this).next();
    var height = menu.height();
    var expanded = false;

    if (height === 0) {
      height = menu.data('height');
      expanded = true;
    } else {
      menu.data('height', height);
      height = 0;
    }

    $(this).attr('aria-expanded', expanded);
    toggleChatMenu(menu, height, true);
  });

  $('.chat-list-heading').on({
    mouseenter: function () {
      $(this).find('.expander').css('opacity', 1.0);
    },

    mouseleave: function () {
      $(this).find('.expander').css('opacity', 0);
    }
  });

  $('.reset.password').on('click', function () {
    $('.login .signin.form, .btn-toolbar.signin, .login .error').hide();
    $('.login .reset.form, .btn-toolbar.reset').show();
    $('.login .reset.form #username').focus();
  });

  var twofactor = $('.twofactor.form');
  twofactor.find('a.disable.twofactor').on('click', function () {
    twofactor.find('div.twofactor.signin').hide();
    twofactor.find('div.twofactor.disable').show();
  });

  function hideResetPassword() {
    $('.login .reset.form, .btn-toolbar.reset, .login .error').hide();
    $('.login .signin.form, .btn-toolbar.signin').show();
    $('.login .signin.form #username').focus();
  }

  var resettoolbar = $('.btn-toolbar.reset');
  resettoolbar.find('.btn.back').on('click', function () {
    hideResetPassword();
  });

  resettoolbar.find('.btn.reset').on('click', function () {
    var reset = $('.reset.form');
    var username = reset.find('#username').val();
    var email = reset.find('#email').val();
    var post = { 'Username': username, 'Email': email };

    api.apiRequest({
      url: api.apiEndpoint.resetpassword,
      data: post,
      success: function () {
        // Reset Password JSON
        hideResetPassword();
      },
      error: function () {
        var login = $('.login, .inputs');
        $('.login .signin.form .error .text').text('Incorrect Username or Email');

        login.find('.error').fadeIn();
        login.effect('shake', { times:3, distance: 10 }, 150);
      }
    });
  });

  // Initalise Administration
  $(document).on('LiveHelp.LocalSettingsLoaded', function () {

    (function initSettings() {

      // Remove jStorage
      if (typeof migrateLocalStorage === 'undefined' && localStorage !== undefined && localStorage.jStorage !== undefined) {
        localStorage.removeItem('jStorage');
      }

      if (!storage.isSet('session')) { storage.set('session', ''); }
      if (!storage.isSet('server')) { storage.set('server', ''); }
      if (!storage.isSet('protocol')) { storage.set('protocol', 'http://'); }
      if (!storage.isSet('locale')) { storage.set('locale', {}); }
      if (!storage.isSet('html5-notifications')) { storage.set('html5-notifications', false); }
      if (!storage.isSet('accounts')) { storage.set('accounts', []); }
      if (!storage.isSet('account')) { storage.set('account', null); }
      if (!storage.isSet('route')) { storage.set('route', 'home'); }
      if (!storage.isSet('history-date')) { storage.set('history-date', ''); }
      if (!storage.isSet('viewed')) { storage.set('viewed', chats.viewed); }

      var size = { width: 900, height: 650 };
      if (!storage.isSet('size')) { storage.set('size', size); } else { size = storage.get('size'); }
      if (!storage.isSet('position')) { storage.set('position', {
          top: (window.screen.height - size.height) / 2,
          left: (window.screen.width - size.width) / 2
        });
      }

      if (!storage.isSet('visitorsColumns')) {
        storage.set('visitorsColumns', core.visitorsColumns);
      }

      var language = storage.get('locale');
      if (!$.isEmptyObject(language)) {
        core.locale = language;

        $(document).trigger('LiveHelp.LocaleUpdated');
      }

    })();

    // Operator
    core.config.operator = storage.get('operator');

    if (core.dropdown) {
      $('.operators.list').on('click', '.visitor .status', function () {

        var menu = '<ul class="dropdown-menu statusmode"> \
      <li><a href="#" class="Online" data-lang-key="online">Online</a></li> \
      <li><a href="#" class="Offline" data-lang-key="offline">Offline</a></li> \
      <li><a href="#" class="BRB" data-lang-key="brb">Be Right Back</a></li> \
      <li><a href="#" class="Away" data-lang-key="away">Away</a></li>';

        var element = $(this);
        var visitor = element.closest('.visitor');
        var account = (element.is('.parent')) ? element : visitor.find('.status.parent');
        var id = visitor.data('id');

        if (id === core.config.operator.id) {
          menu += '	<li class="divider"></li> \
      <li><a href="#" class="Signout" data-lang-key="signout">Sign Out</a></li>';
        }

        menu += '</ul>';

        if (!visitor.find('.dropdown-menu').length) {
          $(menu).insertAfter(account);
        }
        account.dropdown();

      });
    }

    // Status Mode Menu
    $('.operators.list').on('click', '.dropdown-menu.statusmode li a', function () {
      var element = $(this);
      var status = element.attr('class');
      var visitor = element.closest('.visitor');
      var id = visitor.data('id');

      if (id === core.config.operator.id) {
        id = undefined;
      }

      if (status !== 'Signout') {
        core.changeStatus(status, $(this).text(), id);
      } else {
        core.signOut();
      }

      if (core.dropdown) {
        visitor.find('.status.parent').dropdown();
      }
    });

    // Administration
    initAdmin();

  });

  $(document).trigger('LiveHelp.Initialised');

});

var showLogin = function () {
  // Hide Loading
  $('.loading').fadeOut(250, function () {
    $('.loading').hide();
  });

  // Show Login
  $('.login').show().fadeIn();
};

function initAdmin() {

  // Sign In / Saved Session
  var autoLogin = storage.get('session');
  if (autoLogin !== undefined && autoLogin.length > 0) {
    // Update Session
    core.config.session = autoLogin;
    if (autoSignIn) {
      signIn();
      $(document).trigger('LiveHelp.AutoSignIn', true);
    } else {
      $(document).trigger('LiveHelp.AutoSignIn', false);
    }
  } else {

    // Show Login
    showLogin();

    $(document).trigger('LiveHelp.AutoSignIn', false);
  }

  // Metro Pivot
  $('div.metro-pivot').metroPivot({ selectedItemChanged: function () {
      // Update Charts
      $(document).trigger('LiveHelp.UpdateCharts');
    }, controlInitialized: function () {
      // Initialise amCharts
      //loadStatisticsChartData();
    }
  });

  // Images
  $.preloadImages('images/bubbletip/bubbletip.png');

  // Two Factor Authentication
  $('.twofactor .factor').hover(function () {
    $(this).find('span, img').animate({ opacity: 1 }, 250);
  }, function () {
    if (!$(this).data('selected')) {
      $(this).find('span').animate({ opacity: 0 }, 250);
      $(this).find('img').animate({ opacity: 0.5 }, 250);
    }
  }).click(function () {
    var twofactor = $('.twofactorcode');
    factor = $(this).data('factor');
    $(this).parent().find('.factor').each(function (key, value) {
      var element = $(value);
      if (element.data('factor') !== factor) {
        element.data('selected', false).find('img').animate({ opacity: 0.5 }, 250);
        element.find('span').animate({ opacity: 0 }, 250);
      }
    });

    $(this).data('selected', true).find('span, img').animate({ opacity: 1 }, 250);
    twofactor.fadeIn();

    if (factor === 'push') {
      twofactor.find('.code').fadeOut(function () {
        twofactor.find('.status span').text('Authenticate to send Duo PUSH request');
        twofactor.find('.status, .status img').fadeIn();
      });
    } else if (factor === 'sms' || factor === 'token') {
      if (factor === 'sms') {
        twofactor.find('.code label').text('SMS Code');
        twofactor.find('.hint-token').hide();
        twofactor.find('.hint-sms').show();
      } else {
        twofactor.find('.code label').text('Token Code');
        twofactor.find('.hint-sms').hide();
        twofactor.find('.hint-token').text('Enter your hardware token code or Duo Mobile code').show();
      }

      twofactor.find('.status').fadeOut(function () {
        twofactor.find('.code').fadeIn(function () {
          $('#twofactor').focus();
        });
      });
    }
    $('#twofactor').focus();
  });

  $('.login input').keypress(function (e) {
    if (e.which === 13) {
      signIn();
      e.preventDefault();
    }
  });

  // Sign In
  $('.login .signin.btn').click(function () {
    signIn();
  });

  $('.login #server').blur(function () {
    var input = $(this);
    var server = input.val();
    var ssl = $('.login #ssl');

    if (server.indexOf('https://') > -1) {
      server = server.replace('https://', '');
      ssl.prop('checked', 'checked');
      core.protocol = 'https://';
      storage.set('protocol', core.protocol);
    } else if (server.indexOf('http://') > -1) {
      server = server.replace('http://', '');
      ssl.prop('checked', '');
      core.protocol = 'http://';
      storage.set('protocol', core.protocol);
    }

    var endings = ['/', '/livehelp', '/livehelp/admin', '/livehelp/admin/index.php'];
    for (var value of endings) {
      var pos = server.indexOf(value, server.length - value.length);
      if (pos > -1) {
        server = server.substring(0, pos);
      }
    }
    input.val(server);

  });

  // Clear
  $('.login .clear').click(function () {
    var inputs = $('.login .inputs');
    inputs.find('.server input, .username input, .password input').val('');
    inputs.find('.server input').focus();
  });

  function closeDropdown() {
    if (core.dropdown) {
      $('.operator .dropdown-toggle').dropdown('toggle');
    }
  }

  // Status Mode Menu
  $('.operator').on('click', '.dropdown-menu.statusmode li a', function () {
    var status = $(this).attr('class');
    if (status === 'Accounts') {
      core.switchMenu('accounts');
    } else if (status === 'History') {
      core.switchMenu('history');
    } else if (status === 'Responses') {
      core.switchMenu('responses');
    } else if (status === 'Settings') {
      core.switchMenu('settings');
      return false;
    } else if (status === 'Integrations') {
      closeDropdown();
      core.switchMenu('integrations');
      return false;
    } else if (status === 'Billing') {
      $(document).trigger('LiveHelp.OpenBilling');
      closeDropdown();
      return false;
    } else if (status === 'Signout') {
      core.signOut();
    } else {
      core.changeStatus(status, $(this).text());
    }
  });

  // Accept Pending Chat
  $('.pending.list, .transferred.list').on('click', '.visitor', $(this), function (e) {
    var chat = ($(e.target).is('.visitor')) ? $(e.target) : $(e.target).parent();
    var id = chat.data('id');

    chats.acceptChat(id);
    chats.closeNotification();
  });

  // Pending / Chatting Click Events
  $(document).on('click', '.chatting.list .visitor, .other-chatting.list .visitor, .operators.list .visitor', chats.chattingVisitorClickCallback);

  // Chat Close Event
  $('.chat-stack').on('click', '.chat .inputs > .close', function () {
    // Close Chat
    chats.closeChats();

    // Close Chat Responses
    if (chatResponsesOpen) {
      $(document).trigger('LiveHelp.CloseResponses');
      chatResponsesOpen = false;
    }
  });

}

// Intercom.js
if (core.intercom !== false) {

  core.intercom.on('message-alert', function (data) {
    if (data.id > 0 && data.count !== undefined) {
      if (data.count === 0) {
        var element = $('.chat-list .visitor[data-id=' + data.id + '][data-operator=' + data.operator + ']');
        chats.resetMessageAlert(element, false);
      } else {
        var viewedchat = _.find(chats.viewed, function (chat) { return chat.id === data.id && chat.operator == data.operator; });
        if (viewedchat !== undefined) {
          viewedchat.count = data.count;
          chats.updateMessageAlert(data.id, data.operator, data.count - viewedchat.count, data.viewed);
        }
      }
    }
  });
}

$.fn.chatScrollHandler = function () {
  $(this).on('scroll', _.debounce(function () {
    var chat = $(this).closest('.chat');
    var scrollalert = chat.find('.scrollalert');
    var visitor = $('.chat-list .visitor[data-id="' + chat.data('id') + '"][data-operator=' + chat.data('operator') + ']');

    if (scrollalert.is(':visible')) {
      var scroll = chat.find('.scroll');
      var bottom = scroll[0].scrollTop + scroll.height();
      var last = chat.find('.messages .message:not(.typing):last:parent');
      var element = scroll[0].scrollTop + last.position().top;

      if (bottom > element) {
        if (chat.is('.focussed')) {
          chats.resetMessageAlert(visitor);
        }
        scrollalert.hide();
      }
    } else {
      if (chat.is('.focussed')) {
        chats.resetMessageAlert(visitor);
      }
    }
  }, 250));
};

$.fn.getCursorPosition = function () {
  var el = $(this).get(0);
  var pos = 0;
  if ('selectionStart' in el) {
    pos = el.selectionStart;
  } else if ('selection' in document) {
    el.focus();
    var Sel = document.selection.createRange();
    var SelLength = document.selection.createRange().text.length;
    Sel.moveStart('character', -el.value.length);
    pos = Sel.text.length - SelLength;
  }
  return pos;
};

$.fn.selectRange = function (start, end) {
  if (!end) {
    end = start;
  }
  return this.each(function () {
    if (this.setSelectionRange) {
      this.focus();
      this.setSelectionRange(start, end);
    } else if (this.createTextRange) {
      var range = this.createTextRange();
      range.collapse(true);
      range.moveEnd('character', end);
      range.moveStart('character', start);
      range.select();
    }
  });
};

function sendMessage(message, id) {
  var stack = $('.chat-stack');
  var textarea = stack.find('#message');

  if (textarea.is('.disconnected') || !textarea.val().length) {
    return false;
  }

  var chat = stack.find('.chat.focussed');
  var everyone = stack.find('.channel.everyone');
  var internal = $('.chat-stack .messages.input').is('.internal');
  var content = message || textarea.val();

  core.messagesAjax = false;

  if (id !== undefined || chat.length > 0) {
    var staff = 0;
    var post = { 'Message': content };

    if (chat.length > 0 && !id) {
      id = chat.data('id');
      staff = (chat.data('operator') !== false) ? 1 : 0;
    }

    if ((typeof id === 'number' && id > 0) || (typeof id === 'string' && id.length === 36)) {
      $(document).trigger('LiveHelp.SendMessage', { from: core.config.operator.id, to: id, message: content });

      var account = false;
      for (var value of core.accounts) {
        if (value.id === core.config.operator.id) {
          account = value;
          break;
        }
      }

      // Display Message
      var unique = uuid.v4();

      var chatmessage = {
        id: unique,
        from: core.config.operator.id,
        username: core.config.operator.username,
        firsname: account.firstname,
        email: core.config.operator.email,
        content: content,
        date: new moment().utc().format(),
        status: (!internal) ? 1 : 8
      };
      chats.messageHTML(id, chatmessage, false, false);

      var sentSuccess = function (chat, uuid) {
        return function (data) {

          var id = (typeof data.id === 'number') ? data.id : parseInt(data.id, 10);
          var message = chat.find('.message[data-id="' + uuid + '"]');
          var visitor = $('.chat-list .visitor[data-id=' + chat.data('id') + '][data-operator=false]');

          if (message.length > 0) {
            message.data('id', id).attr('data-id', id).removeClass('sending');
          }

          // Update Last Message
          var previous = parseInt(visitor.data('messages'), 10);
          if (id > 0 && visitor.length > 0 && id > previous) {
            visitor.data('messages', id);
          }

          core.messagesAjax = true;
        };
      };

      if (status > 1) {
        $.extend(post, { Status: status });
      }
      $.extend(post, { ID: id, UUID: unique, Staff: staff });

      api.apiRequest({
        url: api.apiEndpoint.send,
        data: post,
        success: sentSuccess(chat, unique)
      });
    } else if (typeof id === 'number' && id < 0) {
      $(document).trigger('LiveHelp.SendMessage', { from: core.config.operator.id, id: id, message: content });
    }

  } else if (everyone.length > 0) {
    $(document).trigger('LiveHelp.SendMessage', { from: core.config.operator.id, channel: 'everyone', message: content });
  }

  textarea.val('');
  textarea.keyup();

  return false;
}

function processEscKeyDown() {
  var sliders = $('.slider.right');
  var account = $('#account-details');
  var settings = $('#settings');
  var accountgrid = $('.accounts-grid');
  var zindex = 0;
  var top = null;

  // Close Settings
  if (settings.height() > 0 && settings.width() > 0) {
    $(document).trigger('LiveHelp.CloseSettings');
    return;
  }

  // Close Right Sliders
  $.each(sliders, function(key, value) {
    var element = $(value);
    var i = parseInt(element.css('z-index'), 10);

    if (element.width() > 0 && i > zindex) {
      top = element;
      zindex = i;
    }
  });

  // Close Slider
  if (top !== null) {
    var id = top.attr('id');
    var menu = '';

    switch (id) {
      case 'responses':
        $(document).trigger('LiveHelp.CloseResponses');
        menu = id;
        break;
      case 'visitor-details':
        $(document).trigger('LiveHelp.CloseVisitor');
        break;
      case 'account-details':
        if (accountgrid.is(':visible') === false) {
          $(document).trigger('LiveHelp.ShowAccounts');
        }
        if (account.is(':visible') && account.width() > 0) {
          $(document).trigger('LiveHelp.CloseAccount');
        }
        menu = 'accounts';
        break;
      case 'history-chat':
        $(document).trigger('LiveHelp.CloseHistory');
        menu = 'history';
        break;
    }

    $(document).trigger('LiveHelp.CloseSlider', { menu: menu, previousMenu: core.previousMenu });

    return;
  }

  // Close Chats
  if (parseInt($('.chat-stack').css('top'), 10) === 0) {
    chats.closeChats();
    return;
  }
}

function toggleChatMenu(menu, height, manual) {
  var css = 'expander sprite sort-desc';
  var id = menu.attr('id');

  if (height > 0) {
    css = 'expander sprite sort-asc';
  }
  storage.set(id + '.height', height);

  if (height > 0 && saveExpanded || manual) {
    menu.animate({ height: height }, 250, 'easeOutBack', function () {
      var expanded = false;
      if (height > 0) {
        menu.show();
        expanded = true;
      } else {
        menu.hide();
      }
      $(this).prev().attr('aria-expanded', expanded);
      $(this).prev().find('.expander').removeAttr('class').addClass(css);
    });
  } else {
    menu.prev().attr('aria-expanded', true);
    menu.prev().find('.expander').removeAttr('class').addClass(css);
  }

}

// Settings
function loadLocalSettings() {

  // Viewed Chats
  chats.viewed = storage.get('viewed');

  // Popout
  var route = storage.get('route');
  if (route.length > 0) {
    switch (route) {
      case 'responses':
        $(document).trigger('LiveHelp.OpenResponses');
        break;
      case 'accounts':
        $(document).trigger('LiveHelp.OpenAccounts');
        break;
      case 'settings':
        $(document).trigger('LiveHelp.OpenSettings');
        break;
      case 'history':
        $(document).trigger('LiveHelp.OpenHistory');
        break;
    }

    if (route.indexOf('history/') > -1) {
      var chat = parseInt(route.replace('history/', ''), 10);
      $(document).trigger('LiveHelp.OpenHistoryRoute', chat);
    }

    if (route.indexOf('settings/') > -1) {
      var section = route.replace('settings/', '');
      $(document).trigger('LiveHelp.OpenSettings', section);
    }

    core.route = route;
    core.router.setRoute(route);
    $(document).trigger('LiveHelp.RouteChanged');
  }
}

var factor = '';

function signInComplete() {

  $('.content').filter(':not(.loading)').show().animate({ opacity: 1.0 }, 250);

  // Notification
  $(document).trigger('LiveHelp.SignInCompleted');

  // Setup Sounds
  if (!core.messageSound) {
    core.messageSound = new buzz.sound(core.address + '/sounds/New Message', {
      formats: ['ogg', 'mp3', 'wav'],
      volume: 100
    });
  }
  if (!core.pendingSound) {
    core.pendingSound = new buzz.sound(core.address + '/sounds/Pending Chat', {
      formats: ['ogg', 'mp3', 'wav'],
      volume: 100
    });
  }

  // Update Operator Details
  var operator = storage.get('operator');

  if (operator !== undefined) {
    $(document).trigger('LiveHelp.UpdateOperatorImage');
    $('.operator .name').text(operator.name);
  }

  // Operator Access
  if (operator.access > 3) {
    $('.menu a[data-type=statistics]').parent().hide();
    $('.menu a[data-type=history]').parent().hide();
    $('.menu a[data-type=accounts]').parent().hide();
  }

  $(document).trigger('LiveHelp.SignInComplete', { account: operator, server: core.server });

  // Local Settings
  loadLocalSettings();

  // Settings AJAX
  api.apiRequest({
    url: api.apiEndpoint.settings,
    timeout: 15000,
    success: function (data) {
      // Settings JSON
      data = (data.settings) ? data.settings : data;
      if (data) {

        // Settings
        core.settings = data;
        storage.set('settings', core.settings);

        // Update Visitors
        visitors.updateVisitorsGrid();

        // Update Users
        core.updateUsers({}, function () {

          // Update Messages
          chats.updateMessages(function () {
            // Complete Login
            var login = $('.login');
            $('.login, .loading').fadeOut(250, function () {
              login.hide();
            });

            // Clear Login
            login.find('input[type!=reset][type!=submit], select').val('');
            login.find('.ball-clip-rotate').hide();
            login.find('.signin.btn').show();
          }, function (xhr, textStatus, thrownError) {
            core.signInError(xhr, textStatus, thrownError);
          });

        }, function (xhr, textStatus, thrownError) {
          core.signInError(xhr, textStatus, thrownError);
        });

        $(document).trigger('LiveHelp.SettingsInitialised', data);
      }
    },
    error: function (xhr, textStatus, thrownError) {
      core.signInError(xhr, textStatus, thrownError);
    }
  });

  // World Map / Locations
  try {
    visitors.initWorldMap();
  } catch (e) {
    console.log(e);
  }

}

var signInCallback = function () { return true; };

var signInTwoFactorCallback = function () {
    // Show Two Factor Security Code
    if ($('.login #username').val().length > 0 && $('.login #password').val().length > 0) {
      $('.login .signin.form, .login .error, .loading').hide();
      $('.login, .login .twofactor.form').show().find('#security').focus();
    } else {
      $('.login, .login .signin.form').show();
    }
  };

function signIn() {

  var result = signInCallback();
  if (result) {

    var login = $('.login');
    var path = window.location.pathname;
    var serverfield = login.find('#server');
    var serverinput = (serverfield.length) ? serverfield.val() : '';

    core.protocol = login.find('#ssl').is(':checked') ? 'https://' : 'http://';
    login.find('.logo').addClass('loading');

    if (storage.get('server').length > 0) {
      core.server = storage.get('server');
      core.protocol = storage.get('protocol');
    }

    if (document.location.protocol == 'https:') {
      core.protocol = 'https://';
      storage.set('protocol', core.protocol);
    }

    core.server = (core.server !== undefined && core.server.length > 0) ? core.server : document.location.host;
    if (serverinput.length > 0) {
      core.server = serverinput;
    }

    var modulefolder = document.location.pathname.indexOf('/modules/livehelp/admin');
    var subfolder = document.location.pathname.indexOf('/livehelp/admin');

    if (modulefolder > -1) {
      path = document.location.pathname.substring(0, modulefolder);
      core.address = core.protocol + core.server + path + '/modules' + core.directoryPathName;
    } else if (subfolder > -1) {
      path = document.location.pathname.substring(0, subfolder);
      core.address = core.protocol + core.server + path + core.directoryPathName;
    } else {
      core.address = core.protocol + core.server + core.directoryPathName;
    }

    // Router
    core.router.init();

    $(document).trigger('LiveHelp.SignIn');

    var user = (login.find('#username').length) ? login.find('#username').val() : '';
    var pass = (login.find('#password').length) ? login.find('#password').val() : '';
    var status = (login.find('#status').length) ? login.find('#status').val() : '';
    var security = login.find('#security');
    var backupcode = login.find('#backupcode');
    var post = {};

    if (security.is(':visible')) {
      if (security.val().length == 6) {
        post = $.extend(post, { 'Data': $.toJSON({ 'code': security.val() }) });
        $('.login .twofactor.form .error').hide();
      } else {
        $('.login .twofactor.form .error').fadeIn();
        $('.login .inputs, .login .logo').effect('shake', { times: 3, distance: 10 }, 150);
        return;
      }
    } else if (backupcode.is(':visible')) {
      user = login.find('.twofactor.form #username').val();
      pass = login.find('.twofactor.form #password').val();
      if (backupcode.val().length == 24) {
        $.extend(post, { 'Data': $.toJSON({ 'backupcode': backupcode.val() }) });
        $('.login .twofactor.form .error').hide();
      } else {
        $('.login .twofactor.form .backup.error').fadeIn();
        $('.login .inputs, .login .logo').effect('shake', { times: 3, distance: 10 }, 150);
        return;
      }
    }

    if (user.length > 0 && pass.length > 0) {
      $.extend(post, { 'Username': user, 'Password': pass, 'Action': status, 'Version': '5.0' });
    } else {
      core.server = storage.get('server');
      core.protocol = storage.get('protocol');
    }

    if (core.server.length) {
      var pathDirectory = core.directoryPathName.replace(/[/]/g, '');
      var regEx = new RegExp('/(/)??' + pathDirectory + '(/)??$/g');

      core.server = core.server.replace(regEx, '');
    } else {
      var pathname = document.location.pathname;
      path = (pathname.indexOf(core.directoryPath) > 0) ? pathname.substring(0, pathname.indexOf(core.directoryPath)) : '';
      core.server = document.location.host + path;
    }

    if (core.cache) {
      $.extend(post, { 'cache': '' });
    }

    login.find('.signin.btn').hide();
    login.find('.ball-clip-rotate').show();

    api.apiRequest({
      url: api.apiEndpoint.login,
      data: post,
      timeout: 15000,
      success: function (data) {
        // Hide Loading
        $('.login .logo').removeClass('loading');

        // Login JSON
        data = (data.login) ? data.login : data;
        if (data && data.session) {
          // Operator Session
          core.config.session = data.session;

          if (core.config.session !== false && core.config.session.length > 0) {

            // Image
            var image = core.operatorGravatar(data.image, data.email);

            // Username / Password
            core.config.operator = {
              id: data.id,
              username: user,
              name: data.name,
              email: data.email,
              image: image,
              access: parseInt(data.access, 10),
              status: data.status
            };

            if (data.datetime !== undefined) {
              core.config.operator.datetime = data.datetime;
            }

            if (data.customer !== undefined) {
              core.config.operator.customer = {
                company: data.customer.company,
                plan: data.customer.plan,
                datetime: data.customer.datetime
              };
            }

            if (data.users !== undefined) { core.config.operator.users = data.users; }
            if (data.messages !== undefined) { core.config.operator.messages = data.messages; }
            if (data.teammessages !== undefined) { core.config.operator.teammessages = data.teammessages; }
            if (data.chats !== undefined) { core.config.operator.chats = data.chats; }
            if (data.devices !== undefined) { core.config.operator.devices = data.devices; }
            if (data.hash !== undefined) { core.config.operator.hash = data.hash; }

            // Account
            if (data.account !== undefined) {
              core.config.operator.account = data.account;
            }

            $(document).trigger('LiveHelp.LoginComplete', [core.config.session, core.config.operator]);

            // Electron App
            if (__ELECTRON__) { // eslint-disable-line no-undef
              Chatstack.introduction = 'Hey, how are you going? Do you require help or have any feeback for us?';
              Chatstack.name = core.config.operator.name;
              Chatstack.email = core.config.operator.email;
              Chatstack.custom = 'Chatstack';
              Chatstack.useragent = 'Chatstack App/' + packagejson.version + ' (' + platform + ' ' + os.release() + '; electron.js ' + process.versions.electron + ') Chrome/' + process.versions.chrome; // eslint-disable-line no-undef
              Chatstack.url = core.protocol + core.server;
              Chatstack.referrer = 'Direct Visit / Bookmark';

              (function (d) {
                // JavaScript
                Chatstack.e = []; Chatstack.ready = function (c) { Chatstack.e.push(c); }
                var b = d.createElement('script'); b.type = 'text/javascript'; b.async = true;
                b.src = 'https://' + Chatstack.server + '/livehelp/scripts/js.js';
                var s = d.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(b, s);
              })(document);
            }

            // Sign In / Saved Session
            storage.set('protocol', core.protocol);
            storage.set('server', core.server);
            storage.set('session', core.config.session);
            storage.set('operator', core.config.operator);

            // Complete Sign In
            signInComplete();

            // Update Status Mode
            var status = 'Offline';
            switch (data.status) {
              case 1:
                status = 'Online';
                break;
              case 2:
                status = 'BRB';
                break;
              case 3:
                status = 'Away';
                break;
            }

            status = (status === 'BRB') ? 'Be Right Back' : status;
            $('.operator .dropdown-toggle .status').text(status);

            if (status === 'Be Right Back') {
              status = 'BRB';
            } else if (status === 'Offline') {
              status = 'Offline';
            }
            $('.operator .mode').removeClass('online offline hidden brb away').addClass(status.toLowerCase());

          } else if (data.otp !== false) {

            // Two Factor Callback
            signInTwoFactorCallback();

            $(document).trigger('LiveHelp.LoginTwoFactor');

          } else if (data.otp === false) {
            var element = $('.login .signin .backup.error .text');
            core.signInError(false, false, false, element);
          } else {
            core.signInError();
          }
        }
      },
      error: function (xhr, textStatus, thrownError) {
        core.signInError(xhr, textStatus, thrownError);
      }
    });
  }
}
