<!DOCTYPE html>
<html>
<head>
<title>{$SETTINGS.NAME}</title>
{literal}
<script type="text/javascript">
<!--
	var Chatstack = {};
	Chatstack.server = document.location.host + document.location.pathname.substring(0, document.location.pathname.indexOf('/livehelp'));
	Chatstack.visitorTracking = false;
	Chatstack.popup = true;
	Chatstack.initiate = false;
	Chatstack.css = false;
	Chatstack.session = {/literal}'{$session|escape:quotes}'{literal};
	Chatstack.template = {/literal}'{$template|escape:quotes}'{literal};
	Chatstack.department = {/literal}'{$department|escape:quotes}'{literal};
	Chatstack.security = {/literal}'{$captcha|escape:quotes}'{literal};
	Chatstack.locale = {/literal}'{$language|escape:quotes}'{literal};
{/literal}{if $connected}{literal}	Chatstack.connected = {/literal}{$connected}{literal};{/literal}{/if}{literal}

	(function(d, undefined) {
		// JavaScript
		Chatstack.e = []; Chatstack.ready = function (c) { Chatstack.e.push(c); }
		Chatstack.server = Chatstack.server.replace(/[a-z][a-z0-9+\-.]*:\/\/|\/livehelp\/*(\/|[a-z0-9\-._~%!$&'()*+,;=:@\/]*(?![a-z0-9\-._~%!$&'()*+,;=:@]))|\/*$/g, '');
		var b = document.createElement('script'); b.type = 'text/javascript'; b.async = true;
		b.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + Chatstack.server + '{/literal}{$jspath}{literal}';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(b, s);
	})(document);
-->
</script>
{/literal}
</head>
<body style="background-color: {$SETTINGS.BACKGROUNDCOLOR};" class="LiveHelpPopup">
<div id="LiveHelpContent">
