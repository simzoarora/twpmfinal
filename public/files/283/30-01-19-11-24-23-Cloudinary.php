<?php

namespace Cloudinary;
use \Cloudinary\Uploader as CloudinaryUploader;

class Cloudinary
{
    public function resizeImage($imageUrl)
    {
        \Log::info( compact('imageUrl') );
        $cloudinaryDimensions = config('constant.cloudinary');
        $files = [];
        try{
            $files['final_image'] = CloudinaryUploader::upload( $imageUrl, ['eager' => [ 
                "transformation" => [ 
                    [
                        'width' => "iw_add_" + $cloudinaryDimensions['shadow_margin'],
                        'height' => "ih_add_" + $cloudinaryDimensions['shadow_margin'],
                        'crop' => $cloudinaryDimensions['lpad'],
                        'effect' => $cloudinaryDimensions['effect'], 
                        'color' => $cloudinaryDimensions['color'], 
                        'x' => $cloudinaryDimensions['x-offset'], 
                        'y' => $cloudinaryDimensions['y-offset'] 
                    ], [
                        'width' => $cloudinaryDimensions['carousel_image_width'], 
                        'height' => $cloudinaryDimensions['carousel_image_height'], 
                        'crop' => $cloudinaryDimensions['lpad']
                    ]
                ], "format" => "png"
            ] ] );
            $finalImage = $files['final_image']['eager'][0]['secure_url'];
            \Log::info('Cloudinary API accessed.');
            \Log::info( compact('finalImage') );
            return [ 'image' => $finalImage];  
        }catch(\Exception $exception){
            \Log::info("Cloudinary failed for image ($imageUrl) : ".$exception);
            return $imageUrl;
        }
        
    }
}