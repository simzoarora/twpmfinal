if (!String.prototype.trim) {
    (function () {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function () {
            return this.replace(rtrim, '');
        };
    })();
}
$(document).ready(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    //Input add class  
    $(document).on('keyup change', 'input.input__field', function () {
        var $this = $(this);
        if ($this.val().trim() !== '') {
            $this.parent().addClass('input--filled');
        } else {
            $this.parent().removeClass('input--filled');
        }
    });
    //END

    var inputHasValue = $('input.input__field').val();
    if (inputHasValue) {
        $('input.input__field').trigger('change');
    }
});
$(document).ready(function () {
    //Stripe payment event handler
    $('#stripe-payment-form').submit(function (e) {
        var $this = $(this);
        //Remove error messages
        $this.find("#form-response").text('').hide();

        $.ajax({
            type: "POST",
            url: $this.attr('action'),
            data: $this.serialize(),
            dataType: 'json',
            success: function (resp) {
                $("#form-response").text(resp.message).show();

                setTimeout(function () {
                    $("#form-response").text('').hide();
                    $('.inner-content').fadeOut();
                    setTimeout(function () {
                        $('#settingup-dash').fadeIn();

                        setTimeout(function () {
                            endRegisterCall();
                        }, 5000);
                    }, 1000);
                }, 4000);
            },
            error: function (err) {
                var error = JSON.parse(err.responseText);
                $("#form-response").text(error.message).show();
            }
        });
        e.preventDefault();
    });
    //END
    //Saving user if all questions has been submitted
    function endRegisterCall() {
        $.ajax({
            type: "POST",
            url: endRegister,
            data: {
                user_id: $('#user-id').val()
            },
            dataType: 'json',
            success: function (resp) {
                location.href = dashboardUrl;
            },
            error: function () {
                $('#end-register-error').show();
                setTimeout(function () {
                    location.href = loginUrl;
                }, 2000);
            }
        });
    }
    //END


});