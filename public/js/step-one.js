if (!String.prototype.trim) {
    (function () {
        // Make sure we trim BOM and NBSP
        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
        String.prototype.trim = function () {
            return this.replace(rtrim, '');
        };
    })();
}
$(document).ready(function () {
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    //Input add class  
    $(document).on('keyup change', 'input.input__field', function () {
        var $this = $(this);
        if ($this.val().trim() !== '') {
            $this.parent().addClass('input--filled');
        } else {
            $this.parent().removeClass('input--filled');
        }
    });
    //END

    var inputHasValue = $('input.input__field').val();
    if (inputHasValue) {
        $('input.input__field').trigger('change');
    }
});