// Typography
$fa-font-path: "../fonts/vendor/font-awesome/";
$icon-font-path: "../fonts/vendor/bootstrap-sass/bootstrap/";

// Datatables
.dataTables_wrapper label{
        font-weight: 400;
}
.dataTables_paginate, .dataTables_filter {
  float: right;
}


https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.16/js/dataTables.bootstrap.min.js