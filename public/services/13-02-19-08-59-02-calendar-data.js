[{
    'calendarId': 1,
    'userData': [{
        'id': '5b8684c682188b2f40a53c72',
        'title': 'Елена  Минина',
        'events': [{
            'eventId': '5c2e4d5c2d996a337d8818e0',
            'title': 'Укладка',
            'description': '1',
            'start': '2019-01-03T03:00:00.000Z',
            'end': '2019-01-03T03:19:59.000Z',
            'allDay': false,
            'resourceId': '5b8684c682188b2f40a53c72'
        }, {
            'eventId': '5c2e631ce8310e5eb4c8f223',
            'title': 'Макияж вечерний',
            'description': '2',
            'start': '2019-01-04T03:00:00.000Z',
            'end': '2019-01-04T03:29:59.000Z',
            'allDay': false,
            'resourceId': '5b8684c682188b2f40a53c72'
        }, {
            'eventId': '5c2e80140d801b1553143d29',
            'title': 'Макияж дневной',
            'description': 'No description or notes...',
            'start': '2019-01-04T03:30:00.000Z',
            'end': '2019-01-04T03:59:59.000Z',
            'allDay': false,
            'resourceId': '5b8684c682188b2f40a53c72'
        }, {
            'eventId': '5c2e89e9ff06f91e882c7cd7',
            'title': 'Окрашивание корней +тонирование',
            'description': 'asd',
            'start': '2019-01-04T03:30:00.000Z',
            'end': '2019-01-04T04:14:59.000Z',
            'allDay': false,
            'resourceId': '5b8684c682188b2f40a53c72'
        }, {
            'eventId': '5c2e89f4ff06f91e882c7cd8',
            'title': 'Стрижка + Укладка',
            'description': 'No description or notes...',
            'start': '2019-01-04T04:45:00.000Z',
            'end': '2019-01-04T05:29:59.000Z',
            'allDay': false,
            'resourceId': '5b8684c682188b2f40a53c72'
        }]
    }, {
        'id': '5b8683ae82188b2f40a53c6e',
        'title': 'Елена  Рич',
        'events': [{
            'eventId': '5c2e80060d801b1553143d28',
            'title': 'Макияж дневной',
            'description': 'No description or notes...',
            'start': '2019-01-04T03:15:00.000Z',
            'end': '2019-01-04T03:44:59.000Z',
            'allDay': false,
            'resourceId': '5b8683ae82188b2f40a53c6e'
        }, {
            'eventId': '5c2f45f5ff06f91e882c7d01',
            'title': 'Окрашивание корней +тонирование',
            'description': 'No description or notes...',
            'start': '2019-01-10T03:00:00.000Z',
            'end': '2019-01-10T03:44:59.000Z',
            'allDay': false,
            'resourceId': '5b8683ae82188b2f40a53c6e'
        }, {
            'eventId': '5c2f48f7ff06f91e882c7d08',
            'title': 'Стрижка + Укладка',
            'description': 'No description or notes...',
            'start': '2019-01-10T04:00:00.000Z',
            'end': '2019-01-10T04:44:59.000Z',
            'allDay': false,
            'resourceId': '5b8683ae82188b2f40a53c6e'
        }, {
            'eventId': '5c2f61e9e8989a04cc50365a',
            'title': 'Makeup and Nails service',
            'description': 'No description or notes...',
            'start': '2019-01-04T03:00:00.000Z',
            'end': '2019-01-04T03:29:59.000Z',
            'allDay': false,
            'resourceId': '5b8683ae82188b2f40a53c6e'
        }]
    }, {
        'id': '5b8682f382188b2f40a53c6c',
        'title': 'Евгений  Головачёв',
        'events': [{
            'eventId': '5c2f0652ff06f91e882c7cdd',
            'title': 'Маникюр классический',
            'description': 'Bugssssssssssss',
            'start': '2019-01-04T03:30:00.000Z',
            'end': '2019-01-04T04:14:59.000Z',
            'allDay': false,
            'resourceId': '5b8682f382188b2f40a53c6c'
        }, {
            'eventId': '5c2f458dff06f91e882c7cff',
            'title': 'Makeup and Nails service',
            'description': 'No description or notes...',
            'start': '2019-01-10T03:00:00.000Z',
            'end': '2019-01-10T03:29:59.000Z',
            'allDay': false,
            'resourceId': '5b8682f382188b2f40a53c6c'
        }, {
            'eventId': '5c2f480fff06f91e882c7d03',
            'title': 'Маникюр классический',
            'description': 'No description or notes...',
            'start': '2019-01-10T03:30:00.000Z',
            'end': '2019-01-10T04:14:59.000Z',
            'allDay': false,
            'resourceId': '5b8682f382188b2f40a53c6c'
        }, {
            'eventId': '5c2f484dff06f91e882c7d04',
            'title': 'Маникюр классический',
            'description': 'No description or notes...',
            'start': '2019-01-04T04:15:00.000Z',
            'end': '2019-01-04T04:59:59.000Z',
            'allDay': false,
            'resourceId': '5b8682f382188b2f40a53c6c'
        }]
    }, {
        'id': '5b86844182188b2f40a53c70',
        'title': 'Олег  Игнатьев',
        'events': [{
            'eventId': '5c2f48d9ff06f91e882c7d07',
            'title': 'Укладка',
            'description': 'No description or notes...',
            'start': '2019-01-10T03:00:00.000Z',
            'end': '2019-01-10T03:19:59.000Z',
            'allDay': false,
            'resourceId': '5b86844182188b2f40a53c70'
        }, {
            'eventId': '5c2f4907ff06f91e882c7d09',
            'title': 'Окрашивание корней +тонирование',
            'description': 'No description or notes...',
            'start': '2019-01-13T03:00:00.000Z',
            'end': '2019-01-13T03:44:59.000Z',
            'allDay': false,
            'resourceId': '5b86844182188b2f40a53c70'
        }, {
            'eventId': '5c2f4a5dff06f91e882c7d0b',
            'title': 'Makeup and Nails service',
            'description': 'No description or notes...',
            'start': '2019-01-04T11:30:00.000Z',
            'end': '2019-01-04T11:59:59.000Z',
            'allDay': false,
            'resourceId': '5b86844182188b2f40a53c70'
        }]
    }, {'title': 'All Staff', 'id': 3, 'events': []}]
}]