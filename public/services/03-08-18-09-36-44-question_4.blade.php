<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Details about your mortgage on your non-rental property.</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[4][questionName]','Details about your mortgage on your non-rental property') }}
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'For Which Property') }}
            {{ Form::input('text','data[4][answer][property]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Address']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What was the original mortgage amount') }}
            {{ Form::input('text','data[4][answer][amount]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'When did the mortgage begin') }}
            {{  Form::date('data[4][answer][mortgage begin]', null, ['class'  => 'form-control'], 'd/m/Y')}}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'What is the current mortgage balance') }}
            {{ Form::input('text','data[4][answer][mortgage balance]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Type of mortgage') }}
            {{ Form::select('data[4][answer][Type of mortgage]', ['SELECT','Fixed', 'Adjustable', 'Interest-Only','Other'],null,['class'=>'mortgage-type']) }}
        </div>
        <div class="col-sm-8 inner-left other-type" style="display: none;">
            {{ Form::label('label', 'Other') }}
            {{ Form::input('text','data[4][answer][Type of mortgage other]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
        </div>

        <div class="row">
            <div class="col-sm-8  adjustable-type" style="display: none;">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'In how many years from beginning do the adjustments begin') }}
                    {{ Form::input('text','data[4][answer][adjustments begin]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'How often does it adjust?') }}
                    {{ Form::input('text','data[4][answer][adjustments ]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'What is the interest rate cap(if any)?') }}
                    {{ Form::input('text','data[4][answer][adjustments begin]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-8  interest-type" style="display: none;">
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'For what number of years is t he loan interest-only?') }}
                    {{ Form::input('text','data[4][answer][interest-period]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Is the interest rate fixed during the interest-only period?') }}
                    {{ Form::input('text','data[4][answer][adjustments ]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
                <div class="col-sm-12 inner-left ">
                    {{ Form::label('label', 'Upon what is the interest rate  based') }}
                    {{ Form::select('data[4][answer][interest-rate]', ['SELECT','10 Year Treasury, ', 'PRIME', 'LIBOR','Unsure','Other'],null,['class'=>'interest-rate']) }}
                </div>
                <div class="col-sm-12 inner-left interest-other" style="display: none;">
                    {{ Form::label('label', 'Other') }}
                    {{ Form::input('text','data[4][answer][interest-rate]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control interest-rate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>

                <div class="col-sm-12 inner-left">
                    {{ Form::label('label', 'Does this loan negatively amortize?') }}
                    {{ Form::input('text','data[4][answer][adjustments begin]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
                </div>
            </div>
        </div>

        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Mortgage Term') }}
            {{ Form::select('data[4][answer][mortgage term]', ['SELECT','15yr', '30yr','Other'],null ,[ 'class' => 'mortgage-term']) }}
        </div>
        <div class="col-sm-8 inner-left mortgage-other" style="display: none;">
            {{ Form::label('label', 'Other') }}
            {{ Form::input('text','data[4][answer][mortgage term other]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control ', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Please Describe']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Interest') }}
            {{ Form::input('number','data[4][answer][interest]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Do you have Private Mortgage Insurance on this loan(PMI)') }}
            <label class="radio-custom-label"> {{ Form::radio('data[4][answer][Private Mortgage Insurance]', 'yes',false,[ 'class' => 'radio-value']) }} Yes
            <span class="radio-icon"></span>
            </label>
            <label class="radio-custom-label"> {{ Form::radio('data[4][answer][Private Mortgage Insurance]', 'no',false,[ 'class' => 'radio-value']) }}<p>No</p>
                <span class="radio-icon"></span>
            </label>
        </div>
        <div class="col-sm-8 inner-left insurance-amount"  style="display: none;">
            {{ Form::label('label', 'Amount(Enter amount ONLY if it is separate itemized payment on your mortgage statement)') }}
            {{ Form::input('text','data[4][answer][mortgage insurance amount]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
        <div class="col-sm-8 inner-left">
            {{ Form::label('label', 'Annual Property taxes(only enter amount if property taxes are not included in your monthly payment)') }}
            {{ Form::input('text','data[4][answer][mortgage insurance amount]',null,['data-validation'=>'' , 'class'=>'custom-validation form-control', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
        </div>
    </div>
</div>