<div class="col-sm-12 ">
    <div class="col-md-4 col-xs-11 section-left">
        <h2>Do you have any other mortgage on non-rental property?</h2>
        <p>This does not include Home Equity Lines of Credit(HELOC). we will cover HELOC next.</p>
    </div>
    <div class="col-sm-6 col-sm-offset-1 section-right">
        {{ Form::input('hidden','data[6][questionName]','Do you have any other mortgage on non-rental property?') }}
        <div class="col-sm-8 inner-left">
            <label class="radio-custom-label"> 
                {{ Form::radio('data[6][answer][mortgage on non-rental property HELOC]', 'yes',false,['class'=>'other-non-rental']) }}Yes
                <span class="radio-icon"></span>
            </label>

            <label class="radio-custom-label"> 
                {{ Form::radio('data[6][answer][mortgage on non-rental property HELOC]', 'no',false,['class'=>'other-non-rental']) }}No
                <span class="radio-icon"></span>
            </label>
        </div>
    </div>
</div>

 <div class="col-sm-6 col-sm-offset-1 section-right" id='otherNonRentalForm'> 

        
</div>