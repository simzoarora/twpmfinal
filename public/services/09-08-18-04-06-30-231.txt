@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(elixir('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title = $currentService->title;
                ?>
                @include('frontend.includes.client_header')
                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}
                    <h3></h3> 
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <div class="annual-portfolio">
                                        <p style="margin-bottom: 0;">LIABILITIES</p>
                                    </div>
                                    <h2>Equity Lines of Credit (HELOC)</h2>
                                    <p>Textarea for explanation of HELOC.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right">
                                    {{ Form::input('hidden','data[200][questionName]','Do you have any home equity lines of credit?') }}
                                    <div class="col-sm-8 inner-left">
                                        {{ Form::label('label', 'Do you have any home equity lines of credit?') }}
                                        <label class="radio-custom-label">{{ Form::radio('data[200][answer][semiAnnuaHELOCConfirmation]', 'yes',false,['class'=>'semiAnnuaHELOCConfirmation']) }}Yes
                                            <span class="radio-icon"></span>
                                        </label>
                                        <label class="radio-custom-label">{{ Form::radio('data[200][answer][semiAnnuaHELOCConfirmation]', 'no',false,['class'=>'semiAnnuaHELOCConfirmation']) }}No
                                            <span class="radio-icon"></span>
                                        </label>
                                    </div>
                                    <!-- info table starts -->
                                    <div class="col-sm-12 paddingLeft0 inner-left SemiAnnuaHELOC-details-table" style="display: none;" >
                                        <div class="col-sm-12 ">
                                            <div class="row"> 
                                                <div class="col-sm-12 ">
                                                    <div class="row">
                                                        <div class="add-child">
                                                            <h6> List all home equity lines of credit..</h6>

                                                            <table id='SemiAnnual-HELOCInfo'>
                                                                <thead>
                                                                    <tr> 
                                                                        <td>Property address</td>
                                                                        <td> </td>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr class="children-info">
                                                                    </tr> 
                                                                </tbody>
                                                            </table> 
                                                            <div class="col-md-4 ">
                                                                <div class="row">
                                                                    <button type='button' class="SemiAnnualAddHELOCBtn add-dependent" data-toggle="modal" data-target="#SemiAnnualHELOCModal">Add HELOC</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- info table finsih -->

                                </div>
                            </div>
                        </section>
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>


<!-- modal starts -->
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <section class="sections">
                <div id="SemiAnnualHELOCModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header" style= "background-color: #0972bd; color:#fff; border: none;">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">ADD HELOC</h4>
                            </div>
                            <div class="modal-body" style="overflow: hidden;">
                                <div class="row">
                                    <!-- Steps starts here -->
                                    <div class="requestwizard hide">
                                        <div class="requestwizard-row setup-panel">
                                            <div class="requestwizard-step">
                                                <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                                            </div>
                                            <div class="requestwizard-step">
                                                <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- steps form starts -->
                                    <form role="form">
                                        <!-- first step starts -->
                                        <div class=" setup-content" id="step-1">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right">
                                                    <!-- first step content starts here -->
                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label', 'For which property?') }}
                                                        {{ Form::input('text','data[200][answer][HELOC Property Name]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCPropertyName', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Street Address']) }}
                                                    </div>

                                                    <div class="col-sm-12 inner-left">
                                                        <i class="fa fa-dollar"></i>
                                                        {{ Form::label('label', 'What is the total line of credit Amount?') }}
                                                        {{ Form::input('number','data[200][answer][HELOC Credit Amount]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCreditAmount', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
                                                    </div>


                                                    <div class="col-sm-12 inner-left">
                                                        <i class="fa fa-dollar"></i>
                                                        {{ Form::label('label', 'What is the current balance?') }}
                                                        {{ Form::input('number','data[200][answer][HELOC Current Balance]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCurrentBalance', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
                                                    </div>

                                                    <div class="col-sm-12 inner-left">
                                                        <i class="fa fa-dollar"></i>
                                                        {{ Form::label('label', 'What is your current monthly payment?') }}
                                                        {{ Form::input('number','data[200][answer][HELOC Current Monthly Payment]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCCurrentMonthPayment', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'$']) }}
                                                    </div>


                                                    <div class="col-sm-9 paddingRight0">
                                                        <button class="nextBtn" style="border-radius: 3px; color: #fff; font-family: Lato; font-size: 18px; line-height: 24px; text-align: center; width: 150px;  padding: 12px 35px; background-color: #2179EE; text-decoration: none;  border: none; margin-left: 62px; margin-top: 40px; float: right; margin-bottom: 40px;" type="button" >Next</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- first step finsih -->

                                        <!-- second step starts -->
                                        <div class=" setup-content" id="step-2" style="display: none">
                                            <div class="row">
                                                <div class="col-sm-6 col-sm-offset-3 section-right">
                                                    <div class="col-sm-12 inner-left">
                                                        <i class="fa fa-dollar"></i>
                                                        {{ Form::label('label', 'What is the interest rate??') }}
                                                        {{ Form::input('number','data[200][answer][HELOC Interest Rate]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCInterestRate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'%']) }}
                                                    </div>

                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label', 'Upon what is the interest rate based?') }}
                                                        <i class="fa fa-angle-down" aria-hidden="true" style="position:absolute; right: 28%; z-index: 1; top: 42px; font-size: 16px;"></i>
                                                        <select class="status valid semiAnnualHELOCInterestRateBased" id="helocothers" name="data[3][answer][semiAnnual HELOC interest rate based]" required="" aria-invalid="false" style="display: none;">
                                                            <option selected="" disabled="" value="">SELECT</option>
                                                            <option value="1">10-year Treasury</option>
                                                            <option value="2">Prime</option>
                                                            <option value="3">Libor</option>
                                                            <option value="3">Unsure</option>
                                                            <option value="4">Other</option>
                                                        </select>
                                                    </div>

                                                    <div class="col-sm-12 inner-left helocinterestrateothers" style="display: none;">
                                                        {{ Form::label('label', 'Other?') }}
                                                        {{ Form::input('text','data[200][answer][semiAnnual HELOC Other Option]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCOtherOption', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'please describe']) }}
                                                    </div>


                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label', 'When did the loan originate?') }}
                                                        {{ Form::input('text','data[200][answer][HELOC Loan Originate Date]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control datetimepicker semiAnnualHELOCLoanOriginate', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'MM/DD/YYYY']) }}
                                                    </div>

                                                    <div class="col-sm-12 inner-left">
                                                        {{ Form::label('label', 'What is the term of the loan (in years)?') }}
                                                        {{ Form::input('number','data[200][answer][semiAnnual cc credit limit]',null,['data-validation'=> '' , 'class'=>'custom-validation form-control semiAnnualHELOCLoanTerm', 'data-rule-regex'=>"false", 'required'=>true , 'placeholder'=>'Year']) }}
                                                    </div>


                                                    <div class="col-sm-9 paddingRight0">
                                                        <!--id='SemiAnnualAddHELOCBtn'-->  
                                                        <button  class='SemiAnnualAddHELOCBtn finish-last pull-right' type="button" style="border-radius: 3px; color: #fff;
                                                                 font-family: Lato;
                                                                 font-size: 18px;
                                                                 line-height: 24px;
                                                                 text-align: center;
                                                                 width: 150px;
                                                                 padding: 12px 35px;
                                                                 background-color: #2179EE;
                                                                 text-decoration: none;
                                                                 border: none;
                                                                 margin-top: 40px;
                                                                 margin-bottom: 40px;">
                                                            Finish
                                                        </button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <!-- second step finsih -->
                                        <!-- steps form finsih -->
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>  
    </div>
</div>  
<!-- modal finsih -->

<!-- modal section finish -->
@stop
@section('after-scripts')
{{ Html::script(elixir('js/annual-portfolio-review.js')) }}
<script>
    $(document).ready(function () {

        var navListItems = $('div.setup-panel div a'),
                allWells = $('.setup-content'),
                allNextBtn = $('.nextBtn');

        allWells.hide();

        navListItems.click(function (e) {
            e.preventDefault();
            var $target = $($(this).attr('href')),
                    $item = $(this);

            if (!$item.hasClass('disabled')) {
                navListItems.removeClass('btn-primary').addClass('btn-default');
                $item.addClass('btn-primary');
                allWells.hide();
                $target.show();
                $target.find('input:eq(0)').focus();
            }
        });

        allNextBtn.click(function () {
            var curStep = $(this).closest(".setup-content"),
                    curStepBtn = curStep.attr("id"),
                    nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                    curInputs = curStep.find("input[type='text'],input[type='url']"),
                    isValid = true;

            $(".inner-left").removeClass("has-error");
            for (var i = 0; i < curInputs.length; i++) {
                if (!curInputs[i].validity.valid) {
                    isValid = false;
                    $(curInputs[i]).closest(".inner-left").addClass("has-error");
                }
            }

            if (isValid)
                nextStepWizard.removeAttr('disabled').trigger('click');
        });

        $('div.setup-panel div a.btn-primary').trigger('click');
    });
</script>

<script>
    $(document).ready(function () {
        $("select").selectBoxIt();
        $('.datetimepicker').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $(document).on('change', '.semiAnnuaHELOCConfirmation', function () {
            if ($(this).val() === 'yes') {
                $('.SemiAnnuaHELOC-details-table').show();
            } else {
                $('.SemiAnnuaHELOC-details-table').hide();
            }
        });

        $(document).on('change', '.semiAnnual-cashback-confirmation', function () {
            if ($(this).val() == 'yes' || $(this).prop('checked', true).val() == 'yes') {
                $('.CashBackYesOption ').show();
            } else {
                $('.CashBackYesOption, .exchangeforCasgYesOption').hide();
            }
        });

        $(document).on('change', '.semiAnnualexchange-point-cash', function () {
            if ($(this).val() == 'yes') {
                $('.exchangeforCasgYesOption ').show();
            } else {
                $('.exchangeforCasgYesOption').hide();
            }
        });

        var SemiAnnualHELOCDetails = [];
        $(document).on('click', '.SemiAnnualAddHELOCBtn', function () {
            console.log('1');
            $('#SemiAnnualHELOCModal #step-1').show();
            $('#SemiAnnualHELOCModal #step-2').hide();
            $('#SemiAnnualHELOCModal').find('.error-alert').remove();
            console.log('2');
            if ($('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() && $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val()) {
                console.log('3');
                if ($(this).hasClass('edit-form')) {
                    console.log('4');
                    SemiAnnualHELOCDetails[$(this).attr('data-index')] = {
                        semiAnnualHELOCPropertyName: $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(),
                        semiAnnualHELOCCreditAmount: $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(),
                        semiAnnualHELOCCurrentBalance: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(),
                        semiAnnualHELOCCurrentMonthPayment: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(),
                        semiAnnualHELOCInterestRate: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(),
                        semiAnnualHELOCOtherOption: $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(),
                        semiAnnualHELOCLoanOriginate: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(),
                        semiAnnualHELOCLoanTerm: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(),
                        semiAnnualHELOCInterestRateBased: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val()
                    }
                    console.log('5');
                    $('#SemiAnnual-HELOCInfo tbody tr[data-index=' + $(this).attr('data-index') + ']').html('<td>' + $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() + '</td>  <td><i class="fa fa-pencil" aria-hidden="true"></i></td> <td><i class="fa fa-trash"  aria-hidden="true"></i></td>');
                } else {
                    console.log('6');
                    $('#SemiAnnual-HELOCInfo tbody').append('<tr data-index=' + SemiAnnualHELOCDetails.length + '><td>' + $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val() + '</td>   <td> <i class="fa fa-pencil" aria-hidden="true"></i></td><td><i class="fa fa-trash"  aria-hidden="true"></i></td></tr>');
                    console.log('7');
                    SemiAnnualHELOCDetails.push({
                        semiAnnualHELOCPropertyName: $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(),
                        semiAnnualHELOCCreditAmount: $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(),
                        semiAnnualHELOCCurrentBalance: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(),
                        semiAnnualHELOCCurrentMonthPayment: $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(),
                        semiAnnualHELOCInterestRate: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(),
                        semiAnnualHELOCOtherOption: $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(),
                        semiAnnualHELOCLoanOriginate: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(),
                        semiAnnualHELOCLoanTerm: $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(),
                        semiAnnualHELOCInterestRateBased: $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val()
                    });
                }
                $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName, #SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val('');
                $('#SemiAnnualHELOCModal .semiAnnuaHELOCConfirmation').prop('checked', false);
            } else {
                console.log('8');
                $('#SemiAnnualHELOCModal input').each(function (i, elem) {
                    if (!$(elem).val()) {
                        $(elem).parent().append('<label class="error-alert">This field is required.</label>');
                    }
                });
                if (!$('#SemiAnnualHELOCModal input[type="radio"]').is(':checked')) {
                    $('#SemiAnnualHELOCModal input[type="radio"]').last().closest('.inner-left').append('<label class="error-alert">This field is required.</label>');
                }
            }
        });
        $('#SemiAnnualHELOCModal input').on('keyup change', function () {
            $(this).parent().find('.error-alert').remove();
            if (!$(this).val() || $(this).val() == '0') {
                $(this).parent().append('<label class="error-alert">This field is required.</label>');
            }
        });
        $(document).on('click', '#SemiAnnual-HELOCInfo .fa-pencil', function () {
            $('#SemiAnnualHELOCModal').modal();
            $('#SemiAnnualHELOCModal #step-1').show();
            $('#SemiAnnualHELOCModal #step-2, #SemiAnnualHELOCModal #step-3').css('display', 'none');

            var index = $(this).closest('tr').attr('data-index');
            SemiAnnualCCDetails = SemiAnnualHELOCDetails[index];
            $('#SemiAnnualHELOCModal .semiAnnualHELOCPropertyName').val(SemiAnnualCCDetails.semiAnnualHELOCPropertyName);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCreditAmount').val(SemiAnnualCCDetails.semiAnnualHELOCCreditAmount);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentBalance').val(SemiAnnualCCDetails.semiAnnualHELOCCurrentBalance);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCCurrentMonthPayment').val(SemiAnnualCCDetails.semiAnnualHELOCCurrentMonthPayment);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRate').val(SemiAnnualCCDetails.semiAnnualHELOCInterestRate);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCOtherOption').val(SemiAnnualCCDetails.semiAnnualHELOCOtherOption);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanOriginate').val(SemiAnnualCCDetails.semiAnnualHELOCLoanOriginate);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCLoanTerm').val(SemiAnnualCCDetails.semiAnnualHELOCLoanTerm);
            $('#SemiAnnualHELOCModal .semiAnnualHELOCInterestRateBased').val(SemiAnnualCCDetails.semiAnnualHELOCInterestRateBased);
            $('#SemiAnnualAddHELOCBtn').addClass('edit-form');
            $('#SemiAnnualAddHELOCBtn').attr('data-index', index);
        });
        $(document).on('click', '.SemiAnnualAddHELOCBtn', function () {
            $('#SemiAnnualAddHELOCBtn').removeClass('edit-form');
            $('#SemiAnnualHELOCModal input[type="text"], input[type="number"]').val('');
            $('#SemiAnnualHELOCModal input[type="radio"]').prop('checked', false);
        });
        $(document).on('click', '.fa-trash', function () { // <-- changes
            //alert("aa");
            $(this).closest('tr').remove();
            return false;
        });

        $(document).on('click', '.finish-last', '.fa-pencil', function () { // <-- changes
            $('#SemiAnnualHELOCModal').modal('hide');
            $('#step-1').show();
            $('#step-2, #step-3').hide();
        });

        $("#helocothers").on('change', function () {
            if ($(this).val() === '4') {
                $('.helocinterestrateothers').show();
            } else {
                $('.helocinterestrateothers').hide();
            }
        });
        $(document).on('click', 'SemiAnnualAddHELOCBtn', function () { // <-- changes
            $('#SemiAnnualHELOCModal').find('.error-alert').remove();
        });
    });

</script>
@stop