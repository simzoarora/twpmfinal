@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(elixir('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    <div class="col-sm-12 recommended-service-div liabilities">
                        <!--{{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                        {{ Form::input('hidden','serviceId',$currentService->id) }}-->
                        <h3></h3>
                        <fieldset>
                            <section class="sections">
                                <div class="col-sm-12 ">
                                    <div class="col-md-4 col-xs-12 section-left">
                                        <h6>Main Menu</h6>
                                        <h2>Let's talk about your goal.</h2>
                                        <p>Having this information helps us provide you with the best guidance and support possible. If you need help, simply  <a href="#"> contact us</a>. </p>
                                         <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">As user completes each section, direct them back to this main Liabilities screen, so they can select which section to work on next.</p>-->
                                         <!--<p style="color:#ff0016; font-size: 20px; margin: 80px 0px; line-height: 26px;">User cannot submit info and proceed to payment/sc heduling until they have completed all sections.</p>-->
                                    </div>
                                    <div class="col-sm-6 col-sm-offset-1 col-xs-12 section-right section-size">

                                        <div class="categories">
                                            <div class="headings col-sm-8">
                                                <h4>About you and your family</h4>
                                            </div>
                                            <div class="status col-sm-4">
                                                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,201,43])}}"><?php
                                                    if (!empty($topicsInfo) && array_key_exists(201,
                                                            $topicsInfo)) {
                                                        echo $topicsInfo[201];
                                                    }
                                                    ?></a>
                                            </div>
                                        </div>
                                        <div class="categories">
                                            <div class="headings col-sm-8">
                                                <h4>income & investments</h4>
                                            </div>
                                            <div class="status col-sm-4">
                                                <a href="{{route('frontend.client.fetchAnswers',[   config('constant.subdomain'),$currentService->id,202,42])}}"><?php
                                                    if (!empty($topicsInfo) && array_key_exists(202,
                                                            $topicsInfo)) {
                                                        echo $topicsInfo[202];
                                                    }
                                                    ?></a>
                                            </div>
                                        </div>

                                        <div class="categories">
                                            <div class="headings col-sm-8">
                                                <h4>Real estate assets</h4>
                                            </div>
                                            <div class="status col-sm-4">
                                                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,203,41])}}"><?php
                                                    if (!empty($topicsInfo) && array_key_exists(203,
                                                            $topicsInfo)) {
                                                        echo $topicsInfo[203];
                                                    }
                                                    ?></a>
                                            </div>
                                        </div>

                                        <div class="categories">
                                            <div class="headings col-sm-8">
                                                <h4>goal details</h4>
                                            </div>
                                            <div class="status col-sm-4">
                                                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,204,40])}}"><?php
                                                    if (!empty($topicsInfo) && array_key_exists(204,
                                                            $topicsInfo)) {
                                                        echo $topicsInfo[204];
                                                    }
                                                    ?></a>
                                            </div>
                                        </div>

                                        <div class="categories">
                                            <div class="headings col-sm-8">
                                                <h4>additional notes</h4>
                                            </div>
                                            <div class="status col-sm-4">
                                                <a href="{{route('frontend.client.fetchAnswers',[config('constant.subdomain'),$currentService->id,205,39])}}"><?php
                                                    if (!empty($topicsInfo) && array_key_exists(205,
                                                            $topicsInfo)) {
                                                        echo $topicsInfo[205];
                                                    }
                                                    ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </fieldset>

                        <!-- {{ Form::close() }}-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@section('after-scripts')
{{ Html::script(elixir('js/goal-specific-financial-planning.js')) }}