<!--
# ingramquery0_.php
# Ingram Web API php script
# 
-->

<html>
<head>
<title>00iquery.php</title>
</head>
<body>

<?

// Use the NuSOAP php library
require_once('c:\inetpub\wwwroot\nusoap.php');

// Set parameters
$userName    = '';
$password    = '';
$queryType   = 1;
$query       = 'kw=dog';
$startRecord = 2;
$endRecord   = 5;
$sortField   = 'ti';
$liveUpdate  = 'Y';

// Build SOAP message
$body = '<?xml version="1.0" encoding="ISO-8859-1"?>
<SOAP-ENV:Envelope SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/" 
	xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" 
	xmlns:xsd="http://www.w3.org/2001/XMLSchema" 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/">
	<SOAP-ENV:Header>
		<UserInfo xmlns="http://ingrambook.com/CompDataAccess/companion">
			<UserName>' . $userName . '</UserName>
			<Password>' . $password . '</Password>
		</UserInfo>
	</SOAP-ENV:Header>
	<SOAP-ENV:Body>
		<SearchRequestTypes1234 xmlns="http://ingrambook.com/CompDataAccess/companion">	
			<queryType>' . $queryType . '</queryType>
			<query>' . $query . '</query>
			<startRecord>' . $startRecord . '</startRecord>
			<endRecord>' . $endRecord . '</endRecord>
			<sortField>' . $sortField . '</sortField>
			<liveUpdate>' . $liveUpdate . '</liveUpdate>
		</SearchRequestTypes1234>			
	</SOAP-ENV:Body>
</SOAP-ENV:Envelope>';

// Set up web service path and SOAP action variable
$serverpath = "https://idswebtest.ingramcontent.com/cws/companion.asmx";
$soapaction = "http://ingrambook.com/CompDataAccess/companion/SearchRequestTypes1234";

// Create new SOAP client object instance
$client = new soapclient($serverpath);

$results = $client->send($body, $soapaction);

// Display output
echo "<h2>Request</h2>";
echo "<pre>" . htmlspecialchars($client->request,
ENT_QUOTES) . "</pre>";
echo "<h2>Response</h2>";
echo "<pre>" . htmlspecialchars($client->response,
ENT_QUOTES) . "</pre>";

// Dispose SOAP client
unset($client);

?> 

</body>
</html>
