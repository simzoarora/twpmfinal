@extends('frontend.layouts.client')

@section('title')
@stop

@section('meta_description')
@stop
@section('after-styles')
{{ Html::style(elixir('css/single-service-questions.css')) }}
@stop
@section('content')
<div class="dashboard ">
    <div id="dashboard-content">
        <div class="container-fluid wrapper">
            <?php $activeClass4 = 'active'; ?>
            @include('frontend.includes.client_sidebar')
            <div class="right-content back-div">
                <?php
                $page_title   = $currentService->title;
                ?>
                @include('frontend.includes.client_header')

                <div class="col-sm-12 recommended-service-div">
                    {{ Form::open([ 'route' => 'frontend.client.saveServiceQuestionAnswer', 'id' => 'services-question-form' , 'class' => 'col-sm-11 col-xs-11']) }}
                    {{ Form::input('hidden','serviceId',$currentService->id) }}

                    <h3></h3>
                    <fieldset>
                        <section class="sections">
                            <div class="col-sm-12 ">
                                <div class="col-md-4 col-xs-11 section-left">
                                    <p style="margin-bottom: 0;">{{session::get('loggedInUserName')}} INVESTMENT EXPERIENCE</p>
                                    <h2>Review your information.</h2>
                                    <p>Click on any section to review your answers.</p>
                                </div>
                                <div class="col-sm-6 col-sm-offset-1 section-right section-size">

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>INDIVIDUAL STOCKS</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,26,96])}}"><?php if(!empty($subTopicInfo) && array_key_exists(26, $subTopicInfo)){ echo $subTopicInfo[26];}?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>BONDS</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,27,97])}}"><?php if(!empty($subTopicInfo) && array_key_exists(27, $subTopicInfo)){ echo $subTopicInfo[27];}?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>OPTIONS ON STOCKS</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,28,98])}}"><?php if(!empty($subTopicInfo) && array_key_exists(28, $subTopicInfo)){ echo $subTopicInfo[28];}?></a>
                                        </div>
                                    </div>

                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>COMMODITIES AND FUTURES</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,29,99])}}"><?php if(!empty($subTopicInfo) && array_key_exists(29, $subTopicInfo)){ echo $subTopicInfo[29];}?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>MUTUAL FUNDS</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,30,100])}}"><?php if(!empty($subTopicInfo) && array_key_exists(30, $subTopicInfo)){ echo $subTopicInfo[30];}?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>ANNUITIES</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,31,101])}}"><?php if(!empty($subTopicInfo) && array_key_exists(31, $subTopicInfo)){ echo $subTopicInfo[31];}?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>MARGIN</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,32,102])}}"><?php if(!empty($subTopicInfo) && array_key_exists(32, $subTopicInfo)){ echo $subTopicInfo[32];}?></a>
                                        </div>
                                    </div>
                                    <div class="categories">
                                        <div class="headings col-sm-8">
                                            <h4>REAL ESTATE</h4>
                                        </div>
                                        <div class="status col-sm-4">
                                            <a href="{{route('frontend.client.fetchAnswersSubTopic',[config('constant.subdomain'),$currentService->id,10,33,103])}}"><?php if(!empty($subTopicInfo) && array_key_exists(33, $subTopicInfo)){ echo $subTopicInfo[33];}?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </fieldset>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
